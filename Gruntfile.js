// Generated on 2014-11-19 using generator-angular 0.10.0
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Routing requests
  var rewriteRulesSnippet = require('grunt-connect-rewrite/lib/utils').rewriteRequest;

  // Configurable paths for the application
  var appConfig = {
    app: require('./bower.json').appPath || 'app',
    dist: 'dist'
  };

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    yeoman: appConfig,

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      js: {
        /**
         * @TODO
         */
        files: ['app/config/**/*.js', 'app/components/**/*.js', 'app/shared/**/*.js', '!endpoint_mocks/**/*.js'],
        tasks: ['newer:jshint:all'],
        options: {
          livereload: '<%= connect.options.livereload %>'
        }
      },
      jsTest: {
        /**
         * @TODO
         */
        files: ['test/spec/**/*.js'],
        tasks: ['newer:jshint:test', 'karma']
      },
      compass: {
        files: ['app/assets/styles/**/*.{scss,sass}'],
        tasks: ['compass:server', 'autoprefixer']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        /**
         * @TODO
         */
        files: [
          'app/**/*.html',
          '.tmp/**/*.*',
          'app/assets/images/**/*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: '0.0.0.0',
        livereload: 35729
      },
      // Rewrite rules
      rules: [
        // redirecting everything to root
        {from: '^[^\\.]*$', to: '/index.html'}
      ],
      livereload: {
        options: {
          middleware: function (connect) {
            return [
              rewriteRulesSnippet,
              connect.static('.tmp'),
              connect().use(
              '/bower_components',
              connect.static('./bower_components')
              ),
              connect.static(appConfig.app)
            ];
          }
        }
      },
      test: {
        options: {
          port: 9001,
          middleware: function (connect) {
            return [
              rewriteRulesSnippet,
              connect.static('.tmp'),
              connect().use(
              '/bower_components',
              connect.static('./bower_components')
              ),
              connect.static(appConfig.app)
            ];
          }
        }
      },
      dist: {
        options: {
          open: true,
          base: '<%= yeoman.dist %>'
        }
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js',
          'app/scripts/**/*.js',
          'app/components/**/*.js',
          'app/shared/**/*.js',
          '!app/**/tests/**/*.js',
          '!app/**/test/**/*.js',
          '!endpoint_mocks/**/*.js'
        ]
      },
      test: {
        options: {
          jshintrc: 'app/config/test/.jshintrc'
        },
        src: ['test/spec/**/*.js']
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= yeoman.dist %>/**/*',
            '!<%= yeoman.dist %>/.git/**/*'
          ]
        }]
      },
      server: '.tmp'
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['last 1 version']
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '**/*.css',
          dest: '.tmp/styles/'
        }]
      }
    },

    // Compiles Sass to CSS and generates necessary files if requested
    compass: {
      options: {
        sassDir: 'app/assets/styles',
        cssDir: '.tmp/styles',
        generatedImagesDir: '.tmp/images/generated',
        /**
         * @TODO
         */
        imagesDir: 'app/assets/images',
        /**
         * @TODO
         */
        javascriptsDir: ['.tmp/'],
        fontsDir: 'assets/fonts',
        importPath: './bower_components',
        httpImagesPath: '/images',
        httpGeneratedImagesPath: '/images/generated',
        httpFontsPath: '/app/assets/fonts',
        relativeAssets: false,
        assetCacheBuster: false,
        raw: 'Sass::Script::Number.precision = 10\n'
      },
      dist: {
        options: {
          generatedImagesDir: '<%= yeoman.dist %>/images/generated'
        }
      },
      server: {
        options: {
          debugInfo: true
        }
      }
    },

    // Renames files for browser caching purposes
    filerev: {
      dist: {
        src: [
          '<%= yeoman.dist %>/scripts/**/*.js',
          '<%= yeoman.dist %>/styles/**/*.css'
        ]
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: '<%= yeoman.app %>/index.html',
      options: {
        dest: '<%= yeoman.dist %>',
        flow: {
          html: {
            steps: {
              js: ['concat', 'uglifyjs'],
              css: ['cssmin']
            },
            post: {}
          }
        }
      }
    },

    // Performs rewrites based on filerev and the useminPrepare configuration
    usemin: {
      html: ['<%= yeoman.dist %>/**/*.html'],
      css: ['<%= yeoman.dist %>/assets/styles/**/*.css'],
      options: {
        assetsDirs: ['<%= yeoman.dist %>','<%= yeoman.dist %>/images']
      }
    },

    // The following *-min tasks will produce minified files in the dist folder
    // By default, your `index.html`'s <!-- Usemin block --> will take care of
    // minification. These next options are pre-configured if you do not wish
    // to use the Usemin blocks.
    // cssmin: {
    //   dist: {
    //     files: {
    //       '<%= yeoman.dist %>/styles/main.css': [
    //         '.tmp/styles/{,*/}*.css'
    //       ]
    //     }
    //   }
    // },
    // uglify: {
    //   dist: {
    //     files: {
    //       '<%= yeoman.dist %>/scripts/scripts.js': [
    //         '<%= yeoman.dist %>/scripts/scripts.js'
    //       ]
    //     }
    //   }
    // },
    // concat: {
    //   dist: {}
    // },

    //imagemin: {
    //  dist: {
    //    files: [{
    //      expand: true,
    //      cwd: '<%= yeoman.app %>/images',
    //      src: '{,*/}*.{png,jpg,jpeg,gif}',
    //      dest: '<%= yeoman.dist %>/images'
    //    }]
    //  }
    //},

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/assets/images',
          src: '{,*/}*.svg',
          dest: '<%= yeoman.dist %>/assets/images'
        }]
      }
    },

    ngtemplates: {
      frontendApp: {
        options: {
          url:     function (url) {
            return url.replace('.tmp', '');
          },
          htmlmin: {
            collapseBooleanAttributes: true,
            collapseWhitespace:        true,
            removeAttributeQuotes:     true,
            removeComments:            true,
            removeEmptyAttributes:     true
          }
        },
        src:     '.tmp/**/*.html',
        dest:    '.tmp/js/templates.js'
      }
    },

    // ng-annotate tries to make the code safe for minification automatically
    // by using the Angular long form for dependency injection.
    ngAnnotate: {
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/concat/scripts',
          src: ['*.js', '!oldieshim.js'],
          dest: '.tmp/concat/scripts'
        }]
      }
    },

    // Replace Google CDN references
    //cdnify: {
    //  dist: {
    //    html: ['<%= yeoman.dist %>/*.html']
    //  }
    //},

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: '<%= yeoman.dist %>',
          src: [
            '*.{ico,png,txt}',
            '.htaccess',
            '*.html',
            'views/**/*.html',
            'assets/images/**/*.*',
            'assets/fonts/**/*.*'
          ]
        }, {
          expand: true,
          dot: true,
          cwd: '.',
          dest: '<%= yeoman.dist %>',
          src: [
            'bower_components/please-wait/build/*.*',
            'bower_components/spinkit/css/spinkit.css'
          ]
        }, {
          expand: true,
          cwd: '.tmp/images',
          dest: '<%= yeoman.dist %>/images',
          src: ['generated/*']
        }, {
          expand: true,
          cwd: '.',
          src: 'bower_components/bootstrap-sass-official/assets/fonts/bootstrap/*',
          dest: '<%= yeoman.dist %>'
        }, {
          expand: true,
          cwd: 'bower_components/font-awesome/fonts',
          src: 'fontawesome-webfont.*',
          dest: '<%= yeoman.dist %>/fonts/'
        }]
      },
      styles: {
        expand: true,
        cwd: '<%= yeoman.app %>/assets/styles',
        dest: '.tmp/styles/',
        src: '**/*.css'
      }, html: {
        expand: true,
        cwd: '<%= yeoman.app %>',
        dest: '.tmp/',
        src: [
          'components/**/*.html',
          'shared/**/*.html',
          '../bower_components/ascope-shared/**/*.html'
        ]
      }
    },

    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: [
        'compass:server'
      ],
      test: [
        'compass'
      ],
      dist: [
        'compass:dist',
        //'imagemin',
        'svgmin'
      ]
    },

    // Test settings
    karma: {
      unit: {
        configFile: 'app/config/test/karma.conf.js',
        singleRun: true
      }
    },
    // Run protractor tests
    protractor: {
      options: {
        configFile: 'app/config/test/protractor-conf.js',
        // Keep alive on failure
        keepAlive: true,
        // Make things pretty
        noColor: false,
        args: {
          // Any additional arguments
        },
        debug: false
      },
      all: {}
    },
    // Create the e2e database before running protractor tests
    shell: {
      createDb: {
        command: [
          'cd ../backend',
          'phpunit --configuration phpunit.xml --testsuite "Application Test Suite"'
        ].join('&&')
      }
    },
    /**
     * Run both Dyson and watch in parallel
     */
    parallel: {
      dysonWatch: {
        options:{
          stream: true
        },
        tasks: [
          {
            cmd: 'node_modules/dyson/bin/dyson.js', args:['./endpoint_mocks/']
          },
          {
            grunt: true,
            args: ['watch']
          }
        ]
      },
      dysonE2e: {
        options:{
          stream: true
        },
        tasks: [
          {
            cmd: 'node_modules/dyson/bin/dyson.js', args:['./endpoint_mocks/']
          },
          {
            grunt: true,
            args: ['protractor']
          }
        ]
      },
      /**
       * I find it helpful to keep the server running when developing protractor tests. This is what I use. -- Logan
       */
      dyson: {
        options:{
          stream: true
        },
        tasks: [
          {
            cmd: 'node_modules/dyson/bin/dyson.js', args:['./endpoint_mocks/']
          }
        ]
      }
    },
    // Automatically inject Bower components into the app
    wiredep: {
      app: {
        src: ['<%= yeoman.app %>/index.html'],
        ignorePath:  /\.\./,
        exclude: [
          '/bootstrap/',
          '/eonasdan-bootstrap-datetimepicker/build/',
          '/knockout-sortable.js', // Disable Sortable package dependencies...
          '/react-sortable-mixin.js' // ...for unused frameworks
        ]
      },
      // For e2e testing, we need to include the ng-mocks libraries
      dev: {
        devDependencies: true,
        src: ['<%= yeoman.app %>/index.html'],
        ignorePath:  /\.\./,
        exclude: [
          '/bootstrap/',
          '/eonasdan-bootstrap-datetimepicker/build/',
          '/knockout-sortable.js', // Disable Sortable package dependencies...
          '/react-sortable-mixin.js' // ...for unused frameworks
        ]
      },
      sass: {
        src: ['<%= yeoman.app %>/styles/**/*.{scss,sass}'],
        ignorePath: /(\.\/){1,2}bower_components\//
      }
    },
    // Inject project script files into index template, then copy to app/index.html
    injector: {
      options: {
        template: 'app/config/index-template.html',
        // Remove app/ from all injected paths
        ignorePath: ['app', '.tmp']
      },
      defaults: {
        files: {
          'app/index.html': [
            'app/config/**/*.js',
            '.tmp/js/templates.js',
            'app/**/*.js',
            '!app/**/test/**/*.js',
            '!app/**/tests/**/*.js',
            '!endpoint_mocks/**/*.js'
          ]
        }
      }

    }
  });

  /**
   * Standard serve
   */
  grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'clean:server',
      'injector',
      'wiredep:app',
      'wiredep:sass',
      'concurrent:server',
      'autoprefixer',
      'configureRewriteRules',
      'connect:livereload',
      'parallel:dysonWatch'
      //'watch'
    ]);
  });

  /**
   * Create e2e database, bring up server, run protractor tests
   */
  grunt.registerTask('e2e', 'Start server and run protractor tests', function () {
    grunt.task.run([
      //'shell:createDb',
      'clean:server',
      'injector',
      'wiredep:dev',
      'wiredep:sass',
      'concurrent:server',
      'autoprefixer',
      'connect:test',
      'configureRewriteRules',
      'parallel:dysonE2e'
      //'parallel:dyson'
    ]);
  });

  grunt.registerTask('server', 'DEPRECATED TASK. Use the "serve" task instead', function (target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve:' + target]);
  });

  grunt.registerTask('test', [
    'clean:server',
    'concurrent:test',
    'autoprefixer',
    'connect:test',
    'karma'
  ]);

  grunt.registerTask('build', [
    'clean:dist',
    'copy:html',
    'ngtemplates',
    'injector',
    'wiredep:app',
    'wiredep:sass',
    'useminPrepare',
    'concurrent:dist',
    'autoprefixer',
    'concat',
    'ngAnnotate',
    'copy:dist',
    //'cdnify',
    'cssmin',
    'uglify',
    'filerev',
    'usemin'
  ]);

  grunt.registerTask('default', [
    'newer:jshint',
    'test',
    'build'
  ]);

  /**
   * For windows users. After running this task, open another terminal window, go to the frontend/ directory, then run this:
   * node node_modules/dyson/bin/dyson.js endpoint_mocks/
   */
  grunt.registerTask('windows', 'Compile then start a connect web server', function (target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'clean:server',
      'injector',
      'wiredep:app',
      'wiredep:sass',
      'concurrent:server',
      'autoprefixer',
      'configureRewriteRules',
      'connect:livereload',
      //'parallel:dysonWatch'
      'watch'
    ]);
  });

  // on watch events configure jshint:all to only run on changed file
  grunt.event.on('watch', function(action, filepath) {
    grunt.config('jshint.all.src', filepath);
  });
};
