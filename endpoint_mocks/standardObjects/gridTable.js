var _             = require('lodash');
var utils         = require('./utils.js');
var paginator     = require('./paginator.js');
var reports       = require('./admin/reports/reports.js');
var aggregators   = require('./admin/reports/aggregator.js');

module.exports = function(gridState, allRows){
  var groupBy,orderBy,filter, pagination, sorters, filtered, filteredBeforePagination, hasAggregators;

  groupBy = utils.filter('asGroupBy');
  orderBy = utils.filter('orderBy');
  filter = utils.filter('filter');
  pagination = gridState.pagination;
  sorters = undefined;

  filtered = {
    "data" : [],
    "meta" : {}
  };

  if(gridState.limitRows){
    allRows = allRows.splice(0, gridState.limitRows);
  }

  filtered.data = gridState.filters ? filter(allRows, gridState.filters) : allRows;
  if (Array.isArray(gridState.groupers) && gridState.groupers.length) {
    filtered.data = groupBy(filtered.data, gridState.groupers);
    sorters = utils.addGroupersToSorts(gridState.groupers, gridState.sorters);
  }


  if ((Array.isArray(sorters) && !_.isEmpty(sorters))
    || (Array.isArray(gridState.sorters) && !_.isEmpty(gridState.sorters)) ) {
    filtered.data = orderBy(filtered.data, sorters || gridState.sorters);
  }
  filteredBeforePagination = filtered.data;

  if(pagination){
    filtered = paginator(pagination, filtered);
  }

  hasAggregators = aggregators.hasAggregators(gridState);
  if (hasAggregators.any) {
    filtered.meta = filtered.meta || {};
    filtered.meta.aggregators = aggregators.aggregate(filteredBeforePagination,gridState);
    filtered.meta.hasAggregators = hasAggregators;
  }

  return filtered;
};