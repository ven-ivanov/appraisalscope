var _ = require('lodash');
var faker = require('faker');

/**
 * This is where object conflicts go until their resolution
 */
var Objects = {
  // Standard user object
  // @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-user-object
  //user: function (id) {
  //  var firstName = faker.name.firstName(),
  //  lastName = faker.name.lastName();
  //  return {
  //    id: id || _.random(1, 100),
  //    firstName: firstName,
  //    lastName: lastName,
  //    fullName: firstName + ' ' + lastName,
  //    username: 'User' + id,
  //    email: firstName + '@' + lastName + '.com',
  //    type: _.random(1, 3),
  //    // Currently listed as an issue
  //    isActive: _.sample([true, false])
  //  }
  //},
  //// Standard client company branch object
  //// @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Branches#standard-client-company-branch-object
  //branch: function (id) {
  //  return {
  //    id: id || _.random(1, 100),
  //    name: faker.company.companyName(),
  //    isActive: _.sample([true, false]),
  //    location: adminObjects.location(),
  //    employees: []
  //  };
  //},
  //// Standard client company object
  //// @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Companies#standard-client-company-object
  //// Persistable - https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Companies#standard-client-company-persistable-object
  //clientCompany: function (id) {
  //  return {
  //    id: id || _.random(1, 100),
  //    name: faker.name.firstName(),
  //    email: faker.internet.email(),
  //    isPending: _.sample([true, false]),
  //    accountType: _.random(1, 5),
  //    ordersPerMonth: _.random(1, 1000),
  //    contact: {
  //      firstName: faker.name.firstName(),
  //      lastName: faker.name.lastName(),
  //      location: Objects.location(),
  //      phones: {
  //        primary: {
  //          number: faker.phone.phoneNumber(),
  //          ext: _.sample([null, 'x100'])
  //        },
  //        fax: faker.phone.phoneNumber()
  //      }
  //    },
  //    ucdp: {
  //      lender: faker.company.companyName(),
  //      businessUnit: 'Unit 1',
  //      fannieMaeSerialNumber: _.random(100000, 9999999),
  //      fraddieMacIdentification: _.random(100000, 999999)
  //    }
  //  }
  //}
};

module.exports = Objects;