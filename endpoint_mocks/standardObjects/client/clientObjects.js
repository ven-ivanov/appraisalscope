var faker = require('faker');
var _ = require('lodash');
var Admin = require('../admin/objects');
var General = require('../generalObjects');
var AppraiserList = require('../../standardObjects/admin/appraisersList/objects');
// Cache for persistence between requests
var cache = {
  // Employees
  employee: {},
  // Branches
  branches: {}
};

var getInt = function () {
  return _.random(1, 100);
};
var getFloat = function () {
  return _.random(1, 100, true);
};
var getBool = function () {
  return _.sample([true, false]);
};

// Store companies in memory so they can be attached to user correctly
var createdCompanies = {};
/**
 * Client objects
 * @type {{}}
 */
var Client = {
  // Standard client company object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Companies#standard-client-company-object
  // 8/2
  clientCompany: function(id, branchNull) {
    // Retrieve if already created
    if (typeof createdCompanies[id] !== 'undefined') {
      return createdCompanies[id];
    }
    var company = General.company(id);

    var clientCompany = {
      accountType: Client.accountTypeEnum(),
      contact: General.companyContact(),
      ucdp: {
        lender: faker.company.companyName(),
        businessUnit: _.random(10000000, 99999999),
        fannieMaeSerialNumber: _.random(1000000000, 9999999999),
        freddieMacIdentification: _.random(1000000000, 9999999999)
      },
      branches: typeof branchNull !== 'undefined' ? branchNull : Client.branch(null, null, true, true)
    };
    // Store for retrieval later
    createdCompanies[company.id] = company;
    return _.assign(company, clientCompany);
  },
  // Account type enum
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Companies#account-type-enum
  // 8/2
  accountTypeEnum: function () {
    return _.sample(['attorney' , 'bank' , 'correspondent-lender' , 'credit-union' , 'home-owner' , 'mortgage-broker' ,
                     'lender' , 'real-estate-agent' , 'wholesales-lender' , 'other']);
  },
  // Standard employee object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees#standard-employee-object
  // 8/2
  employee: function (id, companyId, excludeBranch, excludeCompany) {
    // Inherit from user
    var user = General.user(id);

    var employee = {
      role: Client.employeeRoleEnum(),
      joinedAt: new Date().toISOString(),
      ordersPerMonth: _.random(100, 1000),
      location: Admin.location(),
      phones: {
        primary: {
          number: faker.phone.phoneNumber(),
          ext: 'x100'
        },
        fax: faker.phone.phoneNumber()
      }
    };
    // Include branch, unless explicitly excluded
    if (!excludeBranch) {
      employee.branch = Client.branch(null, companyId, true);
    }
    if (!excludeCompany) {
      employee.company = Client.clientCompany(companyId);
    }
    // Extend user
    employee = _.assign(user, employee);
    // Add to cache
    cache.employee[employee.id] = employee;
    // Return generated
    return employee;
  },
  // Employee Rule Enum
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees#employee-role-enum
  // 8/10
  employeeRoleEnum: function () {
    return _.sample(['processor', 'manager', 'loan-officer']);
  },
  // Standard client company branch object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Branches#standard-client-company-branch-object
  // 8/9
  branch: function (id, companyId, branchExcludedFromEmployees, companyExcludedFromEmployees) {
    var employees = [], i = ((id - 1) * 5) + 1, end = i + 5, branch;
    // Generate some employees
    for (i; i < end; i = i + 1) {
      employees.push(Client.employee(i, companyId, branchExcludedFromEmployees, companyExcludedFromEmployees));
    }
    branch = {
      id: id || _.random(1, 100),
      name: faker.company.companyName(),
      isActive: _.sample([true, false]),
      location: Admin.location(),
      employees: employees,
      company: Client.clientCompany(id, null)
    };
    // If we have one cached, return that
    if (typeof cache.branches[branch.id] !== 'undefined') {
      return cache.branches[branch.id];
    }

    // Add to cache
    cache.branches[branch.id] = branch;

    return branch;
  },
  // Standard Instruction Document Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client#standard-instruction-document-object
  // 8/6
  instructionDocument: function (id) {
    var document = {
      title: faker.hacker.phrase()
    };
    return _.assign(document, Admin.document(id))
  },
  // Standard Instruction Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client#standard-instruction-object
  // 8/6
  instruction: function (id) {
    return {
      id: id,
      title: faker.hacker.phrase(),
      content: faker.hacker.phrase(),
      documents: [Client.instructionDocument(((id - 1) * 2) + 1), Client.instructionDocument(((id - 1) * 2) + 2)],
      visibility: {
        jobTypes: [1, 2, 3],
        states: [General.state(), General.state()]
      }
    }
  },
  // Standard Auto Assign Criteria Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Companies/AutoAssign#standard-auto-assign-criteria-object--standard-auto-assign-criteria-persistable-object
  // 8/7
  autoAssign: function () {
    return {
      // Max distance
      maxDistance: {
        isEnabled: getBool(),
        value: getInt()
      },
      // Min rating
      minRating: {
        isEnabled: getBool(),
        value: _.random(1, 5)
      },
      // Max pending orders
      maxPendingOrders: {
        isEnabled: getBool(),
        value: getInt()
      },
      isOpenOrdersLimit: getBool(),
      isApprovedList: getBool(),
      isCoverage: getBool(),
      fhaApproved: {
        isEnabled: getBool(),
        value: Client.fhaApprovedAutoAssignOptions()
      },
      generalOnly: {
        isEnabled: getBool(),
        // Options are 1-3
        priority: 1
      },
      certifiedOnly: {
        isEnabled: getBool(),
        // Options are 1-3
        priority: 2
      },
      licensedOnly: {
        isEnabled: getBool(),
        // Options are 1-3
        priority: 3
      },
      isJobtypePresence: getBool(),
      // Displays a list of job types and allows them to be selected or deselected
      // @todo Waiting on the backend
      // @link https://github.com/ascope/manuals/issues/46#issuecomment-128852516
      jobTypesRange: {
        isEnabled: getBool(),
        items: [1, 2, 3]
      },
      /**
       * @todo Waiting on the backend for this
       * @link https://github.com/ascope/manuals/issues/46#issuecomment-123748767
       */
      prioritizeApprovedLists: {
        isEnabled: getBool(),
        lists: [Client.prioritizedAppraiserList(1), Client.prioritizedAppraiserList(2),
                Client.prioritizedAppraiserList(3)]
      },
      profitMargin: {
        isEnabled: getBool(),
        value: getInt()
      },
      isPreventAutoAssignWhenComment: getBool(),
      // General options
      options: {
        enabledForAllOrders: getBool(),
        enabledPerOrder: getBool(),
        notifyClient: getBool()
      }
    };
  },
  /**
   * Standard Prioritized Appraiser List Object
   * @todo Not currently in backend, just proposed by Igor here: https://github.com/ascope/manuals/issues/46#issuecomment-123748767
   */
  prioritizedAppraiserList: function (id) {
    return {
      "priority": getInt(),
      "isEnabled": getInt(),
      "appraiserList": AppraiserList.appraisersList(id || getInt(), 'approved')
    };
  },
  /**
   * FHA Approved Auto Assign Options Enum
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Companies/AutoAssign#fha-approved-auto-assign-options-enum
   * 8/7
   */
  fhaApprovedAutoAssignOptions: function () {
    return _.sample(["all", "fha-only"]);
  },
  /**
   * Standard Job Type List Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/JobTypeLists#standard-job-type-list-object
   * @todo The endpoint does not currently return job types within the list. Igor is making a relationship object to link job types and job type lists together. Need to wait on that.
   * @link https://github.com/ascope/manuals/issues/203
   * @todo Actually, I think my issue is resolved here.
   * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/JobTypeLists/JobTypes.md#retrieves-all-job-types-of-a-specific-job-type-list
   * 8/12
   */
  jobTypeList: function (id) {
    // init cache
    if (!cache.jobTypeList) {
      cache.jobTypeList = {};
    }
    // Create jobtype list
    var jobTypeList = {
      id: id || _.random(1, 100),
      title: faker.hacker.phrase()
    };
    // Return cached item, if available
    if (cache.jobTypeList[id]) {
      return cache.jobTypeList[id];
    }
    // Store in cache
    cache.jobTypeList[jobTypeList.id] = jobTypeList;
    return jobTypeList;
  },
  /**
   * Standard Job Type Relation Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/JobTypeLists#standard-job-type-relation-object
   * 8/7
   */
  jobTypeRelation: function () {
    return {
      "jobType": Admin.jobType(),
      "zone": Client.jobTypeZoneEnum(),
      "fees": Client.jobTypeFees()
    };
  },
  /**
   * Standard Job Type Zip Fees Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/JobTypeLists#standard-job-type-fees-object
   * 8/7
   */
  jobTypeFees: function () {
    return  {
      "client": getFloat(),
      "appraiser": {
        "value": getFloat(),
        "isEnabled": getBool()
      },
      "management": {
        "value": getFloat(),
        "isEnabled": getBool()
      }
    };
  },
  /**
   * Job Type Zone Enum
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/JobTypeLists#job-type-zone-enum
   * 8/7
   */
  jobTypeZoneEnum: function () {
    return _.sample(["default", "county", "zip", "state"]);
  },
  /**
   * Standard Client Aging Client
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Agings/Clients#standard-client-aging-object
   */
  agingClient: function (id) {
    return _.assign(General.aging(id), {client: Client.clientCompany()});
  },
  /**
   * Client Member Enum
   * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Members/README.md#client-member-enum
   */
  clientMemberEnum: function (type) {
    return type || _.sample(['company', 'employee']);
  }
};

module.exports = Client;
