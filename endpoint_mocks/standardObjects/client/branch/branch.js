var faker = require('faker');
var _ = require('lodash');
/**
 * Branch objects
 */
var Objects = {
  // Standard Branch Settings Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Branches/Settings#standard-branch-settings-object
  // 8/9
  branchSettings: function () {
    return {
      payment: {
        enabled: _.sample([true, false]),
        methods: [1, 3],
        canCapture: _.sample([false, true])
      }
    };
  }
};

module.exports = Objects;