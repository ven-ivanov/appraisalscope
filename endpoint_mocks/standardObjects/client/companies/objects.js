var faker = require('faker');
var _ = require('lodash');

// Client company objects
var objects = {
  /**
   * Standard Client Company Settings Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Companies/Settings#standard-client-company-settings-object
   */
  settings: function () {
    return {
      payment: {
        enabled: _.sample([true, false]),
        methods: [1, 2],
        canCapture: _.sample([true, false])
      },
      appraisals: {
        putOnHoldByDefault: _.sample([true, false])
      },
      reports: {
        merging: {
          point: _.random(1, 5),
          placement: _.random(1, 5),
          templates: [1, 2, 3]
        },
        sendAs: _.random(1, 5),
        emailBorrowerByDefault: _.sample([true, false]),
        requireAppraisedValue: _.sample([true, false]),
        filenameTemplate: faker.hacker.phrase(),
        autoGenerate: _.sample([true, false])
      },
      messages: {
        restrictedRoles: [1, 2, 3]
      },
      commissions: {
        requirement: {
          amount: _.random(1, 100, true),
          unit: _.random(1, 10)
        }
      },
      eo: {
        minCoverageAmount: _.random(1, 100, true)
      }
    };
  }
};

module.exports = objects;