var faker = require('faker');
var _ = require('lodash');
var company = require('../companies/objects');
var Branch = require('../../../standardObjects/client/branch/branch');
var Client = require('../../../standardObjects/client/clientObjects');
var AppraiserList = require('../../../standardObjects/admin/appraisersList/objects');
var Admin = require('../../../standardObjects/admin/objects');
var ClientOnReport = require('../../../standardObjects/clientOnReport');

var getBool = function () {
  return _.sample([true, false]);
};
var getInt = function () {
  return _.random(0, 10);
};

var Objects = {
  // Standard Employee Settings Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/Settings#standard-employee-settings-object
  // @todo Waiting on backend
  // @link https://github.com/ascope/manuals/issues/57
  // 8/6
  employeeSettings: function () {
    return {
      // Instructions
      instructions: {
        // @todo This is a guess, might be incorrect
        source: Objects.paymentSourceEnum()
      },
      appraiserList: {
        // @todo This is a guess, might be incorrect
        source: Objects.paymentSourceEnum()
      },
      // Checklist
      checklist: {
        // @todo This is a guess, might be incorrect
        source: Objects.paymentSourceEnum()
      },
      // Client on report
      clientsOnReport: {
        // @todo This is a guess, might be incorrect
        source: Objects.paymentSourceEnum()
      },
      // Fee schedule
      jobtypesList: {
        // @todo This is a guess, might be incorrect
        source: Objects.paymentSourceEnum()
      },
      "payment": {
        "enabled": getBool(),
        "source": Objects.paymentSourceEnum(),
        "method": [Objects.paymentMethodEnum(), Objects.paymentMethodEnum()],
        // @todo This is just a guess
        creditCardOptions: _.sample(['authorize-only', 'capture', 'save-payment']),
        "canCapture": getBool()
      },
      // Corresponds with notification permissions
      "events": {
        "internal": {
          "allowable": [1, 2],
          "canEdit": getBool()
        },
        "broker": {
          "allowable": [1, 2],
          "canEdit": getBool()
        }
      },
      "appraisals": {
        "canCreate": getBool(),
        "documents": {
          "canUpload": getBool()
        }
      }
    };
  },
  /**
   * @todo I'm just guessing at this
   * @link https://github.com/ascope/manuals/issues/57
   */
  paymentSourceEnum: function () {
    return _.sample(['company', 'branch', 'individual']);
  },
  /**
   * @todo I'm just guessing at this
   * @link https://github.com/ascope/manuals/issues/57
   */
  paymentMethodEnum: function () {
    return _.sample(['credit-card', 'bank-account', 'send-payment-request', 'bill-me', 'split-payment', 'invoice-partial-payment']);
  },
  /**
   * Settings source enum
   * @link https://github.com/ascope/manuals/blob/SergeiMe-issue/38-Confusion-with-client-employee-settings-object/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/Settings/README.md#settings-source-enum
   */
  settingsSourceEnum: function () {
    return _.sample(['company', 'branch', 'individual']);
  },
  /**
   * Settings payment source enum
   * @link https://github.com/ascope/manuals/blob/SergeiMe-issue/38-Confusion-with-client-employee-settings-object/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/Settings/README.md#settings-payment-source-enum
   */
  settingsPaymentSourceEnum: function (type) {
    return type || _.sample([Objects.settingsSourceEnum(), 'no-payment']);
  },
  /**
   * Settings credit card options
   * @link https://github.com/ascope/manuals/blob/SergeiMe-issue/38-Confusion-with-client-employee-settings-object/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/Settings/README.md#settings-credit-card-options
   */
  settingsCreditCardOptions: function () {
    return ["authorize-only", "show-capture-on-client", "capture", "capture-later"];
  },
  /**
   * Event type enum
   * @link https://github.com/ascope/manuals/blob/SergeiMe-issue/38-Confusion-with-client-employee-settings-object/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/Settings/README.md#event-type-enum
   */
  eventTypeEnum: function () {
    return _.sample(["inspection-sheduled", "inspection-complete", "on-hold", "additional-status",
                     "completed-appraisal", "in-review", "accepted", "email-office", "declined", "assigned",
                     "emailed-documents", "accepted-with-conditions", "new-order-confirmation"]);
  },
  /**
   * Standard employee settings payment object
   * @link https://github.com/ascope/manuals/blob/SergeiMe-issue/38-Confusion-with-client-employee-settings-object/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/Settings/README.md#standard-employee-settings-payment-object
   */
  standardEmployeeSettingsPayment: function () {
    return {
      "creditCard": Objects.settingsCreditCardOptions(),
      "isBankAccount": getBool(),
      "sendInvoice": getBool(),
      "billMe": getBool(),
      "splitPayment": getBool(),
      "partialPayment": getBool()
    }
  }
};

module.exports = Objects;