var g = require('../../generator/generator');
var f = require('faker');
var paidStatuses = [
  {id: 1, label: 'Paid'},
  {id: 2, label: 'Paid (Credit Card) '},
  {id: 3, label: 'Partial Paid'},
  {id: 4, label: 'Split Payment'},
  {id: 5, label: 'Unpaid'},
  {id: 6, label: 'Unpaid (Authorized)'},
  {id: 7, label: 'Unpaid (Invoice sent to borrower)'},
  {id: 8, label: 'Unpaid (Invoiced)'},
  {id: 9, label: 'Unpaid (Payment info saved)'}
];

var PaidStatus = {
  // Standard paidStatus Object
  // @Todo
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/PaidStatus#standard-paid-status-object
  paidStatus: function() {
    var id = g.random(0, 8);
    return {
      id: id,
      label: paidStatuses[id].label,
      fha: g.bool()
    };
  }
};

module.exports = PaidStatus;
