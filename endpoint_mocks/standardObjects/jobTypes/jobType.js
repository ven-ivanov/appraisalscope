var g = require('../../generator/generator');
var f = require('faker');

var JobType = {
  // Standard JobType Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/JobTypes#standard-jobtype-object
  jobType: function() {
    return {
      id: g.randomNumber(),
      label: f.lorem.words()[0],
      fha: g.bool()
    };
  }
};

module.exports = JobType;
