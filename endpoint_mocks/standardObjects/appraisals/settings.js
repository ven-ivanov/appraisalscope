var g = require('../../generator/generator');

var Settings = {
  // Standard Settings Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Settings#standard-appraisal-settings-object
  settings: function() {
    return {
      unacceptedTimeout: g.randomNumber()
    };
  }
};

module.exports = Settings;
