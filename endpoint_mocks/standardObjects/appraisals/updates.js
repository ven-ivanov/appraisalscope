var g = require('../../generator/generator');
var f = require('faker');
var assignmentObjects = require('./assignment');
var generalObjects    = require('../generalObjects');

var Updates = {
  // Standard Update Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Updates#standard-update-object
  update: function() {
    return {
      id: g.randomChars(10),
      status: g.random(14),
      document: g.bool()? generalObjects.document(): null,
      assignment: g.bool()? assignmentObjects.assignment(): null
    };
  }
};

module.exports = Updates;
