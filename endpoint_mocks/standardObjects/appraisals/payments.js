var g = require('../../generator/generator');
var f = require('faker');
var generalObjects    = require('../generalObjects');
var _ = require('lodash');

var Payments = {
  // Standard Payment object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Payment#standard-payment-object
  payment: function() {
    return {
      receivable: Payments.receivable(),
      payable: Payments.payable(),
      amount: {
        pl: g.randomFloat(),
        commission: g.randomFloat()
      },
      fee: {
        reviewer:   g.randomFloat(),
        supervisor: g.randomFloat(),
        admin:      g.randomFloat(),
        management: g.randomFloat()
      }
    };
  },

  // Standard Receivable Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Payment/Receivable#standard-receivable-object
  receivable: function(paid, invoiced) {
    invoiced = _.isUndefined(invoiced) ? g.bool() : invoiced;
    paid = _.isUndefined(paid) ? g.bool() : paid;

    return {
      paid: paid,
      method: g.random(4),
      invoiced: invoiced,
      requested: g.bool(),
      fee:  g.randomFloat(),
      paidAt: paid ? g.date.past(): null,
      amount: {
        "total": g.randomFloat(),
        "paid": g.randomFloat(),
        "due": g.randomFloat()
      },
      profile: {
        cc: g.bool()? null: {
          number: g.randomChars(10)
        },
        bank: g.bool()? null: {
          name:    f.finance.accountName(),
          account: f.finance.account(),
          routing: f.finance.mask(),
          type:    f.finance.transactionType(),
          owner:   f.name.firstName() +' '+ f.name.lastName()
        },
        check: g.bool()? null : {
          number: f.finance.account()
        }
      },
      invoice: invoiced ? Payments.invoice(): null
    };
  },

  // Standard Payable Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Payment/Payable#standard-payable-object
  payable: function(payablePaid) {
    return {
      paid: typeof payablePaid !== 'undefined' ? payablePaid : g.bool(),
      method: g.random(4),
      fee:  g.randomFloat(),
      amount: {
        total: g.randomFloat(),
        paid: g.randomFloat(),
        due: g.randomFloat()
      },
      profile: {
        check: g.bool()? null : {
          number: f.finance.account()
        }
      },
      paidAt: g.bool()? g.date.past(): null
    };
  },

  // Standard Receivable Invoice Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Payment/Receivable/Invoice#standard-receivable-invoice-object
  invoice: function() {
    return {
      number: g.randomChars(10),
      description: f.lorem.paragraph(),
      createdAt: g.date.past(),
      document: generalObjects.document()
    };
  }
};

module.exports = Payments;
