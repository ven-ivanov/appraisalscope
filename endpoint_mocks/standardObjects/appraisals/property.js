var GeneralObjects = require('../generalObjects');
var Enums = require('../enums');
var g = require('../../generator/generator');
var f = require('faker');
var _ = require('lodash');

var PropertyObjects = {
  // Standard Property Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Property#standard-property-object
  property: function(id) {
    return {
      id:              id || _.random(1, 10000),
      legal: _.random(10000000, 99999999),
      type: Enums.propertyTypes(true),
      location:        GeneralObjects.location(),
      document:        GeneralObjects.document(),
      occupancy: _.random(1, 100),
      personToContact: PropertyObjects.propertyPersonToContactEnum(),
      contacts: {
        borrower: PropertyObjects.contact(),
        coBorrower: PropertyObjects.contact(),
        owner: PropertyObjects.contact(),
        other: PropertyObjects.contact()
      }
    }
  },

  // Standard Contact Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Property#standard-contact-object
  contact: function() {
    var firstName, lastName;
    firstName = f.name.firstName();
    lastName = f.name.lastName();
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: firstName + ' ' + lastName,
      phones: {
        home: g.phone(),
        work: g.phone(),
        cell: g.phone()
      },
      email: f.internet.email(),
      middleName: f.lorem.words()[0]
    };
  },
  /**
   * Property Person to Contact Enum
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals#property-person-to-contact-enum
   */
  propertyPersonToContactEnum: function () {
    return _.sample(["borrower" , "co-borrower" , "owner" , "other"]);
  }
};

module.exports = PropertyObjects;
