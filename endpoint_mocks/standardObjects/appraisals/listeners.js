var generalObjects    = require('../generalObjects');
var g = require('../../generator/generator');
var f = require('faker');

var Listeners = {
  // Standard Listener Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Listeners#standard-listener-object
  listener: function(count) {
    var data = [];
    var listener = function() {
      return {
        id: g.randomNumber(),
        user: generalObjects.user(),
        events: g.bool()? f.lorem.words(): null
      }
    };

    if (count) {
      for(var i = 1; i <= count; i ++) {
        data.push(listener());
      }
    } else {
      data = listener();
    }

    return data;
  }
};

module.exports = Listeners;
