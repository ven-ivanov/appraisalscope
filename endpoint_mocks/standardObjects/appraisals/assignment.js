var g = require('../../generator/generator');
var f = require('faker');
var _ = require('lodash');
var appraisementObjects = require('../appraisement/appraisementObjects');
var revisions = require('./revisions');

var Assignment = {
  // Standard Assignment Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Assignment
  assignment: function(forceRevision) {
    return             {
      assignee: appraisementObjects.appraiser(g.id(), g.bool()),
      status: g.bool(),
      condition: g.bool()? Assignment.condition(): null,
      reason: _.sample([null, {
        title: f.hacker.phrase(),
        message: f.hacker.phrase()
      }]),
      revision: g.bool() || forceRevision? revisions.revision(): null,
      assignedAt: g.date.past(),
      acceptedAt: g.bool()? g.date.past(): null
    };
  },

  // Standard Assignment Condition Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Assignment#standard-assignment-condition-object
  condition: function() {
    return {
      fee: f.finance.amount(),
      dueDate: {
        client: g.date.future(),
        appraiser: g.date.future()
      },
      message: g.bool()? f.hacker.phrase(): null
    }
  }
};

module.exports = Assignment;
