var g = require('../../generator/generator');
var f = require('faker');
var generalObjects   = require('../generalObjects');

/**
 * Generate comps for revision
 *
 * @param num
 * @returns {Array}
 */
function comps (num) {
  var data = [];

  function createObject() {
    return {
      id: g.bool(),
      address: f.address.streetAddress(),
      salesPrice: f.finance.amount(),
      closedDate: g.date.past(),
      livingArea: f.lorem.words()[0],
      siteSize: f.lorem.words()[0],
      actualAge: f.lorem.words()[0],
      distanceToSubject: f.lorem.words()[0],
      sourceData: f.lorem.words()[0],
      comment: f.lorem.sentence()
    };
  }

  if (typeof num === 'number') {
    for (var i = 1; i <= num; i ++) {
      data.push(createObject());
    }
  } else {
    data = createObject();
  }

  return data;
}

var Revisions = {
  // Standard Revision Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Revisions#standard-revision-object
  revision: function() {
    return {
      id: g.randomNumber(),
      owner: generalObjects.user(),
      appraiser: g.bool()? generalObjects.user(): null,
      content: g.bool()? comps(g.random(5)): {
        name: 'revision',
        type: g.random(1),
        message: f.lorem.sentence(),
        isChecklist: g.bool()
      },
      createdAt: g.date.past(),
      uploadedAt: g.bool()? g.date.past(): null,
      readAt: g.bool()? g.date.past(): null
    };
  }

};

module.exports = Revisions;
