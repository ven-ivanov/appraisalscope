var g = require('../../generator/generator');
var f = require('faker');
var appraisementObjects = require('../appraisement/appraisementObjects');

var Bids = {
  // Standard Bid Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Bids#standard-bid-object
  bid: function() {
    return {
      id: g.id(),
      appraiser: appraisementObjects.appraiser(),
      amount: f.finance.amount(),
      bidAt: g.bool()? g.date.past(): null,
      comment: g.bool()? f.lorem.sentence(): null,
      isSubmitted: g.bool()
    };
  },

  // Standard Bids Summary Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Bids#standard-bids-summary-object
  bidSummary: function() {
    return {
      status: g.random(4),
      total: g.randomNumber(),
      lowest: f.finance.amount(),
      submittedAt: g.date.past()
    };
  }
};

module.exports = Bids;
