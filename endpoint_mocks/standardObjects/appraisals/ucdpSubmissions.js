var g = require('../../generator/generator');
var f = require('faker');

var UCDPSubmission = {
  // Standard Ucdp Submission Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/UcdpSubmission#standard-ucdp-submission-object
  ucdpSubmission: function() {
    return {
      lender: f.name.firstName(),
      orderId: g.randomChars(10),
      status: g.randomChars(10),
      type: g.random(5),
      submittedAt: g.date.past(),
      documents: g.bool()? null: {
        fannieMae: g.bool()? null: {
          file: f.lorem.words()[0],
          url: f.image.imageUrl()
        },
        freddieMac: g.bool()? null: {
          file: f.lorem.words()[0],
          url: f.image.imageUrl()
        }
      }
    }
  }
};

module.exports = UCDPSubmission;
