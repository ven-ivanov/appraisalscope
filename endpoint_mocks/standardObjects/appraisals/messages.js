var g = require('../../generator/generator');
var f = require('faker');
var generalObjects = require('../generalObjects');

var Messages = {
  // Standard Message Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Messages
  message: function() {
    return {
      id:       g.randomNumber(),
      sender:   generalObjects.user(),
      receiver: g.bool() ? generalObjects.user() : null,
      text:     f.lorem.paragraph(),
      visibility: {
        client:    g.bool(),
        appraiser: g.bool()
      },
      createdAt: g.date.past()
    };
  }
};

module.exports = Messages;
