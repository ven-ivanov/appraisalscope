/*
 * https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals
 */

var g = require('../../generator/generator');
var f = require('faker');
var generalObjects    = require('../generalObjects');
var propertyObjects   = require('./property');
var assignmentObjects = require('./assignment');
var ClientObjects     = require('../client/clientObjects');
var clientOnReport    = require('../clientOnReport');
var settingsObject    = require('./settings');
var listenersObject   = require('./listeners');
var jobTypesObject    = require('../jobTypes/jobType');
var paymentsObject    = require('./payments');
var _ = require('lodash');

var Appraisals = {
  // Standard Appraisal Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals#standard-appraisal-object
  appraisal: function(include, filters) {
    var data = {};
    var update = g.bool();
    filters = filters || [];

    // Break down include into an array if it's set
    include = include ? include.split(',') : null;

    data.id          = g.id();
    data.file        = f.lorem.words()[0];
    data.hasPriority = g.bool();
    data.specialCare = g.bool()? f.lorem.paragraph(): null;
    data.fha         = g.bool()? f.lorem.words()[0]: null;
    data.comment     = g.bool()? f.lorem.paragraph(): null;
    data.warning     = g.bool()? f.lorem.paragraph(): null;
    data.parent      = g.bool()? f.helpers.randomNumber(): null;

    if (include && include.indexOf('amc') !== -1) {
      data.amc = {
        licenses: generalObjects.amcLicence()
      };
    }

    data.loan       = generalObjects.loan();
    data.creator    = g.bool()? generalObjects.staff(): ClientObjects.employee(g.id(), g.id(), g.bool());
    data.owner      = ClientObjects.employee(g.id(), g.id(), g.bool());
    data.property   = propertyObjects.property();
    data.assignment = assignmentObjects.assignment();
    data.inspection = Appraisals.inspection();
    data.client     = ClientObjects.clientCompany(g.id());

    if (include && include.indexOf('clientOnReport') !== -1) {
      data.clientOnReport = clientOnReport.clientOnReport();
    }

    data.reviewer = generalObjects.staff();

    if (include && include.indexOf('supervisor') !== -1) {
      data.supervisor = generalObjects.user();
    }

    if (include && include.indexOf('salesperson') !== -1) {
      data.salesperson = generalObjects.staff();
    }

    if (include && include.indexOf('admin') !== -1) {
      data.admin = generalObjects.user();
    }

    data.settings        = settingsObject.settings();
    data.listeners       = listenersObject.listener();
    data.listenerByEmail = f.internet.email();
    data.jobType         = jobTypesObject.jobType();
    data.payment         = paymentsObject.payment();
    data.reason          = g.bool()? f.lorem.sentence(): null;
    data.status          = g.random(14);
    data.customStatus    = g.randomNumber();
    data.createdAt       = g.date.past();
    data.updatedAt       = update? g.date.past(): null;

    data.reason = g.bool()? f.lorem.sentence(): null;
    data.status = g.random(14);
    data.customStatus = g.randomNumber();
    data.createdAt   = g.date.past();
    data.updatedAt = update? g.date.past(): null;
    data.dueDate   = g.bool()? null: {
      client: g.date.future(),
      appraiser: g.date.future()
    };
    data.completedAt = filters.indexOf('completedAt') !== -1 ? g.date.past(): _.sample([g.date.past(), null]);

    data.uploadedAt  = g.bool()? g.date.past(): null;
    data.reviewedAt  = g.bool()? g.date.past(): null;

    if (include && include.indexOf('instruction') !== -1) {
      data.instruction = ClientObjects.instruction(g.id());
    }

    data.staff = generalObjects.staff();

    return data;
  },
  inspection: function() {
    return {
      id: g.randomChars(10),
      startedAt: g.date.past(),
      estimatedCompletionDate: g.bool()? g.date.future(): null,
      completed: g.bool(),
      settings: {
        reminderSent: g.bool()
      }
    };
  }
};


module.exports = Appraisals;

