var _ = require('lodash');
var Cache = {
  cache: {},
  /**
   * Update balance due
   */
  updateBalance: function (id, amount) {
    // See if the appraisal is in cache
    if (!_.isUndefined(Cache.cache[id])) {
      // Update amount due
      Cache.cache[id].payment.receivable.amount.due = Cache.cache[id].payment.receivable.amount.due + (amount * -1);
      // Update amount paid
      Cache.cache[id].payment.receivable.amount.paid = Cache.cache[id].payment.receivable.amount.paid + amount;
    }
  }
};
module.exports = Cache;