var faker = require('faker');
var _ = require('lodash');
var g = require('../generator/generator');
/**
 * General objects, typically found in https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md
 */
var getInt = function () {
  return _.random(1, 100);
};
var getFloat = function () {
  return _.random(1, 100, true);
};
var getBool = function () {
  return _.sample([true, false]);
};

var states = [
  {id:  1, code: 'AL', name: 'Alabama'},
  {id:  2, code: 'AK', name: 'Alaska'},
  {id:  3, code: 'AZ', name: 'Arizona'},
  {id:  4, code: 'AR', name: 'Arkansas'},
  {id:  5, code: 'CA', name: 'California'},
  {id:  6, code: 'CO', name: 'Colorado'},
  {id:  7, code: 'CT', name: 'Connecticut'},
  {id:  8, code: 'DE', name: 'Delaware'},
  {id:  9, code: 'DC', name: 'District of Columbia'},
  {id: 10, code: 'FL', name: 'Florida'},
  {id: 11, code: 'GA', name: 'Georgia'},
  {id: 12, code: 'HI', name: 'Hawaii'},
  {id: 13, code: 'ID', name: 'Idaho'},
  {id: 14, code: 'IL', name: 'Illinois'},
  {id: 15, code: 'IN', name: 'Indiana'},
  {id: 16, code: 'IA', name: 'Iowa'},
  {id: 17, code: 'KS', name: 'Kansas'},
  {id: 18, code: 'KY', name: 'Kentucky'},
  {id: 19, code: 'LA', name: 'Louisiana'},
  {id: 20, code: 'ME', name: 'Maine'},
  {id: 21, code: 'MD', name: 'Maryland'},
  {id: 22, code: 'MA', name: 'Massachusetts'},
  {id: 23, code: 'MI', name: 'Michigan'},
  {id: 24, code: 'MN', name: 'Minnesota'},
  {id: 25, code: 'MS', name: 'Mississippi'},
  {id: 26, code: 'MO', name: 'Missouri'},
  {id: 27, code: 'MT', name: 'Montana'},
  {id: 28, code: 'NE', name: 'Nebraska'},
  {id: 29, code: 'NV', name: 'Nevada'},
  {id: 30, code: 'NH', name: 'New Hampshire'},
  {id: 31, code: 'NJ', name: 'New Jersey'},
  {id: 32, code: 'NM', name: 'New Mexico'},
  {id: 33, code: 'NY', name: 'New York'},
  {id: 34, code: 'NC', name: 'North Carolina'},
  {id: 35, code: 'ND', name: 'North Dakota'},
  {id: 36, code: 'OH', name: 'Ohio'},
  {id: 37, code: 'OK', name: 'Oklahoma'},
  {id: 38, code: 'OR', name: 'Oregon'},
  {id: 39, code: 'PA', name: 'Pennsylvania'},
  {id: 40, code: 'RI', name: 'Rhode Island'},
  {id: 41, code: 'SC', name: 'South Carolina'},
  {id: 42, code: 'SD', name: 'South Dakota'},
  {id: 43, code: 'TN', name: 'Tennessee'},
  {id: 44, code: 'TX', name: 'Texas'},
  {id: 45, code: 'UT', name: 'Utah'},
  {id: 46, code: 'VT', name: 'Vermont'},
  {id: 47, code: 'VA', name: 'Virginia'},
  {id: 48, code: 'WA', name: 'Washington'},
  {id: 49, code: 'WV', name: 'West Virginia'},
  {id: 50, code: 'WI', name: 'Wisconsin'},
  {id: 51, code: 'WY', name: 'Wyoming'},
  {id: 52, code: 'PR', name: 'Puerto Rico'},
  {id: 55, code: 'VI', name: 'Virgin Islands'}
];

var GeneralObjects = {
  /**
   * Standard Document Object
   * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-document-object
   * 8/10
   */
  document: function (id) {
    return {
      "id": id || getInt(),
      "file": faker.hacker.phrase(),
      "url": '//www.pdf995.com/samples/pdf.pdf',
      "size": getFloat(),
      "format": 'pdf',
      "uploadedAt": (new Date()).toISOString()
    };
  },

  // Standard Loan Object
  // @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-loan-object
  loan: function() {
    return {
      number: g.randomChars(10),
      amount: faker.finance.amount(),
      reference: g.randomChars(10),
      type: getInt(),
      purchasePrice: g.bool()? faker.finance.amount(): null
    }
  },

  // Standard State Object
  // @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-state-object
  // 8/10
  state: function() {
    return g.pickRandom(states);
  },

  // Standard Amc Licence
  // @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-amc-license-object
  amcLicence: function() {
    return {
      id: g.randomNumber(),
      state: this.state(),
      number: g.randomNumber(),
      document: g.bool()? this.document(): null,
      expireAt: g.date.future()
    }
  },

  // Standard user object
  // @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-user-object
  // 8/10
  user: function (id) {
    var firstName = faker.name.firstName(),
    lastName = faker.name.lastName();
    return {
      id: id || _.random(1, 100),
      firstName: firstName,
      lastName: lastName,
      fullName: firstName + ' ' + lastName,
      username: 'User' + id,
      email: firstName + '@' + lastName + '.com',
      type: GeneralObjects.userTypeEnum(),
      isActive: _.sample([true, false])
    }
  },
  /**
   * User Type Enum
   * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#user-type-enum
   * 8/2
   */
  userTypeEnum: function () {
    return _.sample(["appraiser", "client", "admin", "staff", "amc"]);
  },

  // Standard Staff Object
  // @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-staff-object
  staff: function() {
    return this.user();
  },

  // Standard Location Object
  // @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-location-object
  // 8/10
  location: function() {
    return {
      address1: faker.address.streetAddress(),
      address2: faker.address.secondaryAddress(),
      city: faker.address.city(),
      state: GeneralObjects.state(),
      valid: _.sample([true, false, null]),
      zip: _.random(10000, 99999),
      geo: {
        latitude: faker.address.latitude(),
        longitude: faker.address.longitude()
      }
    }
  },
  /**
   * Standard pagination object
   * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-pagination-object
   */
  pagination: function (page, perPage, totalPages) {
    return {
      "total": getInt(),
      "perPage": perPage || getInt(),
      "page": page || getInt(),
      "totalPages": totalPages || getInt()
    };
  },
  /**
   * Standard Aging Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Agings#standard-aging-object
   */
  aging: function (id) {
    return  {
      "id": id || getInt(),
      "balance": getFloat(),
      "current": getFloat(),
      "over30": getFloat(),
      "over60": getFloat(),
      "over90": getFloat()
    };
  },
  /**
   * Standard Transaction Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Payment/Transactions#transactions
   */
  transaction: function (id) {
    return  {
      "id": id || getInt(),
      "createdAt": new Date().toISOString(),
      "reference": g.lorem.short(),
      "ccn": _.sample([null, _.random(100000000000, 999999999999)]),
      "type": getInt(),
      "credit": getFloat(),
      "debit": getFloat(),
      "remarks": _.sample([null, faker.hacker.phrase()])
    };
  },
  /**
   * Standard Company Object
   * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-company-object
   * 8/16
   */
  company: function (id) {
    return {
      "id": id || getInt(),
      "name": faker.company.companyName(),
      "joinedAt": new Date().toISOString(),
      "isPending": getBool(),
      "ordersPerMonth": getInt()
    };
  },
  /**
   * Standard Company Contact Object
   * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-company-contact-object
   * 8/16
   */
  companyContact: function () {
    var companyContact = {
      location: GeneralObjects.location()
    };

    return _.assign(GeneralObjects.contact(), companyContact);
  },
  // Standard Contact Object
  // @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-contact-object
  // 8/10
  contact: function() {
    var firstName, lastName;
    firstName = faker.name.firstName();
    lastName = faker.name.lastName();
    return {
      firstName: firstName,
      lastName: lastName,
      // @todo Waiting on backend for this property
      // @link https://github.com/ascope/manuals/issues/89
      fullName: firstName + ' ' + lastName,
      phones: {
        primary: {
          number: g.phone(),
          ext: 'x100'
        },
        fax: g.phone()
      },
      email: faker.internet.email()
    };
  },
  /**
   * Standard Score Object
   * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-score-object
   * 8/4
   */
  score: function (date) {
    date = typeof date !== 'undefined' ? new Date(date).toISOString() : new Date().toISOString();
    return {
      "totals": {
        "reviewerRevisions": getInt(),
        "clientRevisions": getInt(),
        "completedAppraisals": getInt(),
        "reconsiderationRequests": getInt()
      },
      "rates": {
        "reviewerRevision": getFloat(),
        "clientRevision": getFloat()
      },
      "periods": {
        "acceptanceToCompletion": getFloat(),
        "assignedToAccepted": getFloat(),
        "inspectionToCompletion": getFloat()
      },
      "revisionTurnaroundTime": getInt(),
      "onTime": getFloat(),
      "date": date
    };
  },
  /**
   * Standard Merge Field Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Platform/MergeFields#standard-merge-field-object
   * 8/6
   */
  mergeFields: function () {
    var label = faker.hacker.phrase();
    return {
      "placeholder": label.split(' ')[0],
      "label": label
    };
  },
  /**
   * Standard Instruction Template Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Platform/InstructionTemplates#standard-instruction-template-object
   * 8/6
   */
  instructionTemplate: function (id) {
    return {
      "id": id || getInt(),
      "title": faker.hacker.phrase(),
      "body": faker.hacker.phrase()
    };
  },
  /**
   * Certificate Type Enum
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/Certificates#certificate-type-enum
   * 8/16
   */
  certificateTypeEnum: function (type) {
    return type || _.sample(['licensed-appraiser', 'general-appraiser', 'certified-appraiser', 'trainee-license', 'amc']);
  },
  /**
   * Standard County Object
   * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-county-object
   * 8/10
   */
  county: function (number) {
    var names = ['Harris County', 'Hamilton County', 'Montgomery County'];
    var unchangeable = ['harris', 'hamilton', 'montgomery'];
    var random = typeof number === 'number' ? number : _.random(0, 2);
    return {
      state: GeneralObjects.state(),
      // Unchaneable name
      name: unchangeable[random],
      // Display name
      displayName: names[random]
    };
  }
};

module.exports = GeneralObjects;
