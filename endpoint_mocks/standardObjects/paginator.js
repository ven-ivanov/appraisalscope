function Pagination(start, perPage, data){
  this.start = start || 1;
  this.page = 1;
  this.perPage = perPage || 10;
  this.data = data || [];
}

Pagination.prototype.updatePage = function(){
  this.page = Math.round(this.start / this.perPage + 1) || 1;
};

Pagination.prototype.getData = function(){
  var start = Math.max((this.page-1)*this.perPage, 0);
  return this.data.slice(start, start + this.perPage);
};

Pagination.prototype.getTotal = function(){ return this.data.length; };

Pagination.prototype.calculatePages = function(){
  var extraPage = this.getTotal() % this.perPage === 0 ? 0 : 1;
  return Math.floor(this.getTotal()/this.perPage) + extraPage;
};

module.exports = function(paginationInfo, filtered){
  paginationInfo.perPage = paginationInfo.perPage > filtered.data.length ? filtered.data.length : paginationInfo.perPage;
  var pagination = new Pagination(paginationInfo.start, paginationInfo.perPage, filtered.data);
  pagination.updatePage();

  filtered.data = pagination.getData();
  filtered.meta.pagination = {
    page : pagination.page,
    perPage : pagination.perPage,
    total : pagination.getTotal(),
    totalPages : pagination.calculatePages()
  };

  return filtered;
};