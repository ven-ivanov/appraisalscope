var _ = require('lodash');
var faker = require('faker');
var General = require('../../standardObjects/generalObjects');
/**
 * Standard Email Object
 * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Emails#standard-email-object
 */
var Emails = {
  email: function (id) {
    return {
      "id": id || _.random(1, 1999),
      // @todo Not sure of the actual statuses
      // @link https://github.com/ascope/manuals/issues/new
      "status": _.sample(['success', 'failed']),
      "sender": General.user(),
      "receiver": General.user(),
      "message": faker.hacker.phrase(),
      "subject": faker.hacker.phrase(),
      "sentAt": new Date().toISOString()
    }
  }
};

module.exports = Emails;