/*
 * DEPRECATED use appraisal/objects
 */

var g = require('../../generator/generator');
var faker = require('faker');
var _ = require('lodash');

var getBool = function () {
  return _.sample([true, false]);
};

// Cache for holding data for multiple requests
var cache = {};

var states = [
  {id:  1, code: 'AL', name: 'Alabama'},
  {id:  2, code: 'AK', name: 'Alaska'},
  {id:  3, code: 'AZ', name: 'Arizona'},
  {id:  4, code: 'AR', name: 'Arkansas'},
  {id:  5, code: 'CA', name: 'California'},
  {id:  6, code: 'CO', name: 'Colorado'},
  {id:  7, code: 'CT', name: 'Connecticut'},
  {id:  8, code: 'DE', name: 'Delaware'},
  {id:  9, code: 'DC', name: 'District of Columbia'},
  {id: 10, code: 'FL', name: 'Florida'},
  {id: 11, code: 'GA', name: 'Georgia'},
  {id: 12, code: 'HI', name: 'Hawaii'},
  {id: 13, code: 'ID', name: 'Idaho'},
  {id: 14, code: 'IL', name: 'Illinois'},
  {id: 15, code: 'IN', name: 'Indiana'},
  {id: 16, code: 'IA', name: 'Iowa'},
  {id: 17, code: 'KS', name: 'Kansas'},
  {id: 18, code: 'KY', name: 'Kentucky'},
  {id: 19, code: 'LA', name: 'Louisiana'},
  {id: 20, code: 'ME', name: 'Maine'},
  {id: 21, code: 'MD', name: 'Maryland'},
  {id: 22, code: 'MA', name: 'Massachusetts'},
  {id: 23, code: 'MI', name: 'Michigan'},
  {id: 24, code: 'MN', name: 'Minnesota'},
  {id: 25, code: 'MS', name: 'Mississippi'},
  {id: 26, code: 'MO', name: 'Missouri'},
  {id: 27, code: 'MT', name: 'Montana'},
  {id: 28, code: 'NE', name: 'Nebraska'},
  {id: 29, code: 'NV', name: 'Nevada'},
  {id: 30, code: 'NH', name: 'New Hampshire'},
  {id: 31, code: 'NJ', name: 'New Jersey'},
  {id: 32, code: 'NM', name: 'New Mexico'},
  {id: 33, code: 'NY', name: 'New York'},
  {id: 34, code: 'NC', name: 'North Carolina'},
  {id: 35, code: 'ND', name: 'North Dakota'},
  {id: 36, code: 'OH', name: 'Ohio'},
  {id: 37, code: 'OK', name: 'Oklahoma'},
  {id: 38, code: 'OR', name: 'Oregon'},
  {id: 39, code: 'PA', name: 'Pennsylvania'},
  {id: 40, code: 'RI', name: 'Rhode Island'},
  {id: 41, code: 'SC', name: 'South Carolina'},
  {id: 42, code: 'SD', name: 'South Dakota'},
  {id: 43, code: 'TN', name: 'Tennessee'},
  {id: 44, code: 'TX', name: 'Texas'},
  {id: 45, code: 'UT', name: 'Utah'},
  {id: 46, code: 'VT', name: 'Vermont'},
  {id: 47, code: 'VA', name: 'Virginia'},
  {id: 48, code: 'WA', name: 'Washington'},
  {id: 49, code: 'WV', name: 'West Virginia'},
  {id: 50, code: 'WI', name: 'Wisconsin'},
  {id: 51, code: 'WY', name: 'Wyoming'},
  {id: 52, code: 'PR', name: 'Puerto Rico'},
  {id: 55, code: 'VI', name: 'Virgin Islands'}
];

var accountTypes = [
  {id:  1, name: 'Mortgage Broker'},
  {id:  2, name: 'Lender'},
  {id:  3, name: 'Real estate agent'},
  {id:  4, name: 'Home owner'},
  {id:  5, name: 'Attorney'},
  {id:  6, name: 'Bank'},
  {id:  7, name: 'Credit Union'},
  {id:  8, name: 'Correspondent Lender'},
  {id:  9, name: 'Wholesale Lender'},
  {id:  99, name: 'Other'},
];

var primaryLicenses = [
  {name:  'licensed', title: 'Licensed Appraiser'},
  {name:  'certified', title: 'Certified Appraiser'},
  {name:  'general', title: 'General Appraiser'}
];

var Objects = {
  user: function() {
    return {
      id: g.randomNumber(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      name: faker.internet.userName(),
      email: faker.internet.email(),
      phone: this.phone(),
      fax: g.bool()? this.fax(): null,
      type: g.user.type(),
      certifiedState: this.state().code,
      licenseNumber: g.randomChars(10)
    }
  },
  client: function() {
    var client = this.user();
  },
  phone: function() {
    var phone = faker.phone.phoneNumber().split('x');

    return {
      number: phone[0],
      ext: phone[1]? phone[1]: null
    };
  },
  fax: function() {
    return faker.phone.phoneNumber().split('x')[0];
  },
  clientCompany: function() {
    var object = this.user();

    object.location = this.location();

    return object;
  },
  loan: function() {
    return {
      id: faker.lorem.words()[0],
      number: g.randomChars(10),
      amount: faker.finance.amount(),
      reference: g.randomChars(10)
    }
  },
  // Standard Document Object
  // @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-document-object
  // 8/6
  document: function(id) {
    return {
      id: id || g.randomNumber(),
      file: faker.lorem.words()[0],
      url: faker.image.imageUrl(),
      size: g.randomFloat(),
      format: g.fileFormat(),
      uploadedAt: g.date.past()
    }
  },
  // Standard location object
  // @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-location-object
  // 8/7
  location: function() {
    return {
      address1: faker.address.streetAddress(),
      address2: faker.address.secondaryAddress(),
      city: faker.address.city(),
      state: Objects.state(),
      valid: _.sample([true, false, null]),
      zip: _.random(10000, 99999),
      geo: {
        latitude: faker.address.latitude(),
        longitude: faker.address.longitude()
      }
    }
  },
  // Standard state object
  // @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-state-object
  // Good - 8/2
  state: function() {
    return g.pickRandom(states);
  },
  allStates: function() {
    return states;
  },
  accountTypes: function() {
    return accountTypes;
  },
  primaryLicenses: function() {
    return primaryLicenses;
  },
  amcLicence: function() {
    return {
      id: g.randomNumber(),
      state: this.state(),
      number: g.randomNumber(),
      document: g.bool()? this.document: null,
      expireAt: g.date.future()
    }
  },
  /**
   * Standard Job Type Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/JobTypes#standard-job-type-object
   * 8/12
   */
  jobType: function(id, addOnsExcluded) {
    var jobType = {
      id: id || g.randomNumber(),
      title: faker.lorem.words()[0],
      isFha: getBool(),
      isCommercial: getBool(),
      isDisplayed: getBool(),
      isAppraisedValue: getBool(),
      formats: [Objects.jobTypeDocumentFormatEnum(), Objects.jobTypeDocumentFormatEnum()],
      addons: addOnsExcluded || [Objects.jobType(false, true), Objects.jobType(false, true)]
    };
    if (!cache.jobType) {
      cache.jobType = {};
    }
    // return cached
    if (typeof cache.jobType[jobType.id] !== 'undefined') {
      return cache.jobType[jobType.id];
    }
    // Cache and return new object
    cache.jobType[jobType.id] = jobType;

    return jobType;
  },
  /**
   * Job Type Document Format Enum
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/JobTypes#job-type-document-format-enum
   * 8/4
   */
  jobTypeDocumentFormatEnum: function () {
    return _.sample(["pdf", "xml", "aci", "zap", "env"]);
  },
  // Standard Checklist Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Checklists#standard-checklist-object
  // 8/4
  checklist: function (id, title) {
    // Init cache
    if (!('checklist' in cache)) {
      cache.checklist = {};
    }
    // Create this checklist item
    var checklist = {
      id: id || _.random(1, 1000),
      title: title || faker.hacker.phrase()
    };
    // If we have one cached, return that
    if (cache.checklist[checklist.id]) {
      return cache.checklist[checklist.id];
    }
    // Cache
    cache.checklist[checklist.id] = checklist;
    // Otherwise return newly created checklist
    return checklist;
  },
  // Standard Checklist Question Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Checklists#standard-checklist-question-object
  // 8/4
  checklistQuestion: function (id, text) {
    return {
      id: id || _.random(1, 1000),
      text: text || faker.hacker.phrase()
    }
  }
};

module.exports = Objects;
