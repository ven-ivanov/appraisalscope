var General = require('../../../standardObjects/generalObjects');
var faker = require('faker');
var _ = require('lodash');
/**
 * Standard notes objects
 */
var Notes = {
  /**
   * Standard client note object
   * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/README.md#standard-client-note-object
   * 8/16
   */
  clientNote: function (id) {
    var clientNote = {
      isWholesaler: _.sample([true, false])
    };

    return _.assign(Notes.note(id), clientNote);
  },
  /**
   * Standard appraiser note object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/Notes#standard-appraiser-note-object
   * 8/16
   */
  appraiserNote: function (id) {
    var appraiserNote = {
      isDisplayed: _.sample([true, false])
    };

    return _.assign(Notes.note(id), appraiserNote);
  },
  /**
   * Standard Note Object
   * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-note-object
   * 8/16
   */
  note: function (id) {
    return {
      id: id || _.random(1, 1000),
      author: General.user(),
      editor: General.user(),
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString(),
      text: faker.hacker.phrase()
    }
  }
};

module.exports = Notes;