var _ = require('lodash');
var faker = require('faker');
var cache = {
  approved: {},
  'do-not-use': {}
};
var Objects = {
  // Standard Appraiser List Object
  // @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/AppraiserLists/README.md#standard-appraisers-list-object
  // 8/10
  appraisersList: function (id, type) {
    var list = {
      id: id,
      type: type || _.sample(['approved', 'do-not-use']),
      title: faker.hacker.phrase()
    };

    // Retrieve from cache
    if (cache[list.type][list.id]) {
      return cache[list.type][list.id];
    }
    // Add to cache
    cache[list.type][list.id] = list;

    return list;
  }
};

module.exports = Objects;