var g = require('../../generator/generator');
var f = require('faker');
var _ = require('lodash');
var standardAdminObjects = require('./objects');
var ClientObjects = require('../client/clientObjects');


var Objects = {
  client: function() {
    var client = standardAdminObjects.user();

    client.role = 1;
    client.joinedAt = g.date.past();
    client.location = standardAdminObjects.location();
    client.company = g.bool()? standardAdminObjects.clientCompany(): null;
    client.branch = g.bool()? this.branch(): null;

    return client;
  },
  branch: function() {
    var clients = [];
    var count = g.random(5);

    for (var i = 1; i <= count; i++) {
      clients.push(g.randomNumber())
    }

    return {
      id: g.randomNumber(),
      name: f.company.companyName(),
      isActive: _.sample([true, false]),
      location: standardAdminObjects.location(),
      clients: clients,
      employees: []
    }
  },
  // Standard client on report object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees#standard-client-on-report-object
  clientOnReport: function(id) {
    return {
      id: id || g.randomNumber(),
      name: f.company.companyName(),
      isActive: _.sample([true, false]),
      // This is unfinished, but might end up as a standard object -- Logan 6/8/15
      settings: [
        {id: 1, setting: 'Checklist', isActive: _.sample([true, false])},
        {id: 2, setting: 'Instructions', isActive: _.sample([true, false])},
        {id: 3, setting: 'Approved Panel', isActive: _.sample([true, false])},
        {id: 4, setting: 'Do Not Use Panel', isActive: _.sample([true, false])},
        {id: 5, setting: 'Fee Schedule', isActive: _.sample([true, false])},
        {id: 6, setting: 'UCDP', isActive: _.sample([true, false])},
        {id: 7, setting: 'Payment Options', isActive: _.sample([true, false])},
        {id: 8, setting: 'Merge Documents', isActive: _.sample([true, false])},
        {id: 9, setting: 'Email Borrower Appraisal', isActive: _.sample([true, false])},
        {id: 10, setting: 'Auto Assign Setting', isActive: _.sample([true, false])},
        {id: 11, setting: 'E&O Coverage', isActive: _.sample([true, false])},
        {id: 12, setting: 'Job Type Fee Margin', isActive: _.sample([true, false])}
      ]
    }
  }
};

module.exports = Objects;
