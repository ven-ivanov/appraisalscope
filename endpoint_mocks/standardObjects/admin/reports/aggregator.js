'use strict';
var totalAggregator   = require('./aggregators/totalAggregator');
var averageAggregator = require('./aggregators/averageAggregator.js');
var emptyAggregator   = require('./aggregators/emptyAggregator.js');
var utils             = require('../../utils.js');
var _                 = require('lodash');


module.exports = {
  
  available : ['total','average'],
  
  getAggregator : function(aggName, colName){
    switch(aggName){
      case 'total':
        return new totalAggregator(colName);
        break;
      case 'average':
        return new averageAggregator(colName);
        break;
      case 'empty':
        return new emptyAggregator(colName);
        break;
    }
    return false;
  },
  
  aggregate : function (aggregated, params){
    var aggregators = {}, row, r, rl, c, cl, col, sgColumns, a,al,aggType, groupName,grandTotal, autoIncreaseFirstColumn;
    grandTotal = 'grandTotal';
    sgColumns = _.cloneDeep(params.columns);
    autoIncreaseFirstColumn = params.autoIncreaseFirstColumn;

    if(autoIncreaseFirstColumn){
      sgColumns.unshift({name : '_no', nameAs : "No", type : 'AI'});
    }

    r  = 0;
    rl = aggregated.length;
    for(r; r<rl;r++){
      row = aggregated[r];
      groupName = row._groupByClass;

      c  = 0;
      cl = sgColumns.length;
      for(c; c<cl;c++){
        col = sgColumns[c];

        a=0;
        al = this.available.length;
        for(a;a<al;a++){
          aggType = this.available[a];
          aggregators[aggType] = aggregators[aggType] || {};
          if(groupName){
            aggregators[aggType][groupName] = aggregators[aggType][groupName] || [];
          }
          aggregators[aggType][grandTotal] = aggregators[aggType][grandTotal] || [];

          if(col[aggType]){
            if(groupName){
              aggregators[aggType][groupName][c] = aggregators[aggType][groupName][c] || this.getAggregator(aggType, col.name);
              aggregators[aggType][groupName][c].next(parseFloat(row[col.name]) || 0);
            }

            // grandTotal
            aggregators[aggType][grandTotal][c] = aggregators[aggType][grandTotal][c] || this.getAggregator(aggType, col.name);
            aggregators[aggType][grandTotal][c].next(parseFloat(row[col.name]) || 0);
          }else if(col.name === '_no'){
            if(groupName){
              aggregators[aggType][groupName][c] = this.getAggregator('empty', '_no');
            }
            // grandTotal
            aggregators[aggType][grandTotal][c] = this.getAggregator('empty', '_no');
          }else{
            if(groupName){
              aggregators[aggType][groupName][c] = this.getAggregator('empty', col.name);
            }
            //grandTotal
            aggregators[aggType][grandTotal][c] = this.getAggregator('empty', col.name);
          }
        }
      }
    }
    return aggregators;
  },
  hasAggregators : function(gridState){
    var i, l, aggType, index, hasAggregators;
    hasAggregators = {any : false};
    i = 0;
    l = this.available.length;
    for(i;i<l;i++){
      aggType = this.available[i];
      index = utils.indexOf(gridState.columns, true, aggType);
      if(index > -1) {
        hasAggregators[aggType] = true;
        hasAggregators.any = true;
      }
    }
    return hasAggregators;
  }
};


