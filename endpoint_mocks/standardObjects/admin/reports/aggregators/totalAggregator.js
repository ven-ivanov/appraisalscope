'use strict';

function SgTotalAggregator(colName){
  this.colName = colName;
  this.name = 'total';
  this.nameAs = 'Total';
  this.sum = 0;
  this.count = 0;
}

SgTotalAggregator.prototype.next = function(nextVal){
  this.sum += nextVal;
};

module.exports = SgTotalAggregator;

