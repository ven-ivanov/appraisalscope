'use strict';

function SgEmptyAggregator(colName){
  this.colName = colName;
  this.name = 'empty';
  this.nameAs = 'Empty';
}

SgEmptyAggregator.prototype.next = function(){};

module.exports = SgEmptyAggregator;

