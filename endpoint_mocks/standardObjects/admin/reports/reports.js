module.exports = {

  listReports: function(){
    return [
      {id: '32', 'data1': 'Appraisal: General', 'data2': 'November Orders', 'data3': 'This report shows all orders place in November.', 'data4': 'ascopesuper', 'data5': '08\/05\/2013', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '104', 'data1': 'Appraisal: General', 'data2': 'TESTING REPORT', 'data3': '', 'data4': 'ascopesuper', 'data5': '12\/18\/2013', 'data6': '12\/24\/2014 04:05:05 pm', 'data7': 'Permission'},
      {id: '105', 'data1': 'Appraisal: General', 'data2': 'Average Turn Time by state', 'data3': '', 'data4': 'ascopesuper', 'data5': '12\/18\/2013', 'data6': '12\/24\/2014 04:35:03 am', 'data7': 'Permission'},
      {id: '111', 'data1': 'Appraisal: General', 'data2': 'ss-test-d', 'data3': '', 'data4': 'ascopesuper', 'data5': '12\/30\/2013', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '115', 'data1': 'Appraisal: General', 'data2': 'Test_showtime1', 'data3': '', 'data4': 'ascopesuper', 'data5': '01\/22\/2014', 'data6': '12\/25\/2014 01:05:04 am', 'data7': 'Permission'},
      {id: '118', 'data1': 'Appraisal: General', 'data2': 'test_pastdays', 'data3': '', 'data4': 'ascopesuper', 'data5': '01\/28\/2014', 'data6': '01\/31\/2014 04:05:39 am', 'data7': 'Permission'},
      {id: '121', 'data1': 'Appraisal: General', 'data2': 'test_pastweek', 'data3': '', 'data4': 'ascopesuper', 'data5': '01\/28\/2014', 'data6': '01\/28\/2014 02:23:08 am', 'data7': 'Permission'},
      {id: '124', 'data1': 'Appraisal: General', 'data2': 'test_months', 'data3': '', 'data4': 'ascopesuper', 'data5': '01\/28\/2014', 'data6': '01\/28\/2014 02:24:20 am', 'data7': 'Permission'},
      {id: '128', 'data1': 'Appraisal: General', 'data2': 'Test Company Name', 'data3': '', 'data4': 'ascopesuper', 'data5': '02\/05\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '133', 'data1': 'Appraisal: General', 'data2': 'test_cancelled', 'data3': '', 'data4': 'ascopesuper', 'data5': '02\/07\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '136', 'data1': 'Appraisal: General', 'data2': 'Turn Time Test', 'data3': '', 'data4': 'ascopesuper', 'data5': '02\/07\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '138', 'data1': 'Appraisal: General', 'data2': 'TW APP', 'data3': '', 'data4': 'ascopesuper', 'data5': '02\/10\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '139', 'data1': 'Appraisal: General', 'data2': 'Mortgage Unlimite - Completion Report', 'data3': '', 'data4': 'ascopesuper', 'data5': '02\/11\/2014', 'data6': '02\/11\/2014 03:33:14 am', 'data7': 'Permission'},
      {id: '143', 'data1': 'Appraisal: General', 'data2': 'Open Orders', 'data3': '', 'data4': 'ascopesuper', 'data5': '02\/14\/2014', 'data6': '03\/30\/2015 05:08:05 pm', 'data7': 'Permission'},
      {id: '145', 'data1': 'Appraisal: General', 'data2': 'Test Checking Report', 'data3': '', 'data4': 'jeffcolbert!', 'data5': '02\/16\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '148', 'data1': 'Appraisal: General', 'data2': 'Test Checking Report11', 'data3': '', 'data4': 'ascopesuper', 'data5': '02\/16\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '151', 'data1': 'Appraisal: General', 'data2': 'Test Report 123', 'data3': '', 'data4': 'jeffcolbert!', 'data5': '02\/16\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '156', 'data1': 'Appraisal: General', 'data2': 'Test Report', 'data3': '', 'data4': 'mehulss', 'data5': '02\/19\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '157', 'data1': 'Appraisal: General', 'data2': 'test_new1', 'data3': '', 'data4': 'mehulss', 'data5': '02\/19\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '159', 'data1': 'Appraisal: General', 'data2': 'Completed Orders', 'data3': '', 'data4': 'ascopesuper', 'data5': '02\/21\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '161', 'data1': 'Appraisal: General', 'data2': 'Amount Due', 'data3': '', 'data4': 'ascopesuper', 'data5': '02\/21\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '162', 'data1': 'Appraisal: General', 'data2': 'Testing', 'data3': '', 'data4': 'ascopesuper', 'data5': '02\/21\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '163', 'data1': 'Appraisal: General', 'data2': 'rpt_custom_fields', 'data3': '', 'data4': 'ascopesuper', 'data5': '02\/25\/2014', 'data6': '02\/25\/2014 01:16:46 am', 'data7': 'Permission'},
      {id: '164', 'data1': 'Appraisal: General', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '02\/25\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '166', 'data1': 'Appraisal: General', 'data2': 'Test report latest notes', 'data3': '', 'data4': 'ascopesuper', 'data5': '02\/27\/2014', 'data6': '02\/27\/2014 07:44:45 pm', 'data7': 'Permission'},
      {id: '169', 'data1': 'Appraisal: General', 'data2': 'Test_summary', 'data3': '', 'data4': 'ascopesuper', 'data5': '02\/27\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '172', 'data1': 'Appraisal: General', 'data2': 'Test report for reference no', 'data3': '', 'data4': 'ascopesuper', 'data5': '03\/02\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '175', 'data1': 'Appraisal: General', 'data2': 'January Orders', 'data3': '', 'data4': 'ascopesuper', 'data5': '03\/03\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '179', 'data1': 'Appraisal: General', 'data2': 'SSN', 'data3': '', 'data4': 'jeffcolbert!', 'data5': '03\/12\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '181', 'data1': 'Appraisal: General', 'data2': 'Test', 'data3': '', 'data4': 'ascopesuper', 'data5': '03\/12\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '184', 'data1': 'Appraisal: General', 'data2': 'Appraiser Paid', 'data3': '', 'data4': 'ascopesuper', 'data5': '03\/17\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '187', 'data1': 'Appraisal: General', 'data2': 'Test report RM 503', 'data3': '', 'data4': 'ascopesuper', 'data5': '03\/17\/2014', 'data6': '03\/17\/2014 07:42:32 pm', 'data7': 'Permission'},
      {id: '190', 'data1': 'Appraisal: General', 'data2': 'Test RM-504', 'data3': '', 'data4': 'ascopesuper', 'data5': '03\/17\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '192', 'data1': 'Appraisal: General', 'data2': 'Testing', 'data3': '', 'data4': 'ascopesuper', 'data5': '03\/20\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '194', 'data1': 'Appraisal: General', 'data2': 'Testing Contact Name', 'data3': '', 'data4': 'ascopesuper', 'data5': '04\/21\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '198', 'data1': 'Appraisal: General', 'data2': 'Appraisal Completed', 'data3': '', 'data4': 'ascopesuper', 'data5': '05\/01\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '201', 'data1': 'Appraisal: General', 'data2': 'Summary- Appraisal Completed', 'data3': '', 'data4': 'ascopesuper', 'data5': '05\/01\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '204', 'data1': 'Appraisal: General', 'data2': 'Susquehanna Client Report', 'data3': '', 'data4': 'ascopesuper', 'data5': '05\/01\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '215', 'data1': 'Appraisal: General', 'data2': 'Staff', 'data3': '', 'data4': 'ascopesuper', 'data5': '05\/29\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '216', 'data1': 'Appraisal: General', 'data2': 'TEST CANCELLED', 'data3': '', 'data4': 'ascopesuper', 'data5': '06\/02\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '223', 'data1': 'Appraisal: General', 'data2': 'Staff Names', 'data3': '', 'data4': 'ascopesuper', 'data5': '06\/04\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '226', 'data1': 'Appraisal: General', 'data2': 'TEST GROUPS', 'data3': '', 'data4': 'ascopesuper', 'data5': '06\/05\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '234', 'data1': 'Appraisal: General', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '06\/09\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '237', 'data1': 'Appraisal: General', 'data2': 'Inspection Create Date', 'data3': '', 'data4': 'ascopesuper', 'data5': '06\/11\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '245', 'data1': 'Appraisal: General', 'data2': 'Revision Counts Test', 'data3': '', 'data4': 'ascopesuper', 'data5': '06\/16\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '249', 'data1': 'Appraisal: General', 'data2': 'total order count', 'data3': '', 'data4': 'ascopesuper', 'data5': '06\/22\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '261', 'data1': 'Appraisal: General', 'data2': 'Name Filter', 'data3': '', 'data4': 'ascopesuper', 'data5': '06\/29\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '266', 'data1': 'Appraisal: General', 'data2': 'Copy - November Orders', 'data3': 'This report shows all orders place in November.', 'data4': 'eric', 'data5': '08\/05\/2013', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '268', 'data1': 'Appraisal: General', 'data2': 'Completed Test', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/01\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '273', 'data1': 'Appraisal: General', 'data2': 'Appraiser Registered Test', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/02\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '282', 'data1': 'Appraisal: General', 'data2': 'Ad-On Test', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/10\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '294', 'data1': 'Appraisal: General', 'data2': 'test_vendorstatus1', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/13\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '304', 'data1': 'Appraisal: General', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/16\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '307', 'data1': 'Appraisal: General', 'data2': 'Priority Test', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/22\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '315', 'data1': 'Appraisal: General', 'data2': 'Appraised Value', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/23\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '323', 'data1': 'Appraisal: General', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/31\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '325', 'data1': 'Appraisal: General', 'data2': 'Turn Time - Order Completion', 'data3': '', 'data4': 'ascopesuper', 'data5': '08\/04\/2014', 'data6': '08\/06\/2014 02:50:10 am', 'data7': 'Permission'},
      {id: '327', 'data1': 'Appraisal: General', 'data2': 'Turn Time - Report Upload', 'data3': '', 'data4': 'ascopesuper', 'data5': '08\/04\/2014', 'data6': '12\/24\/2014 02:35:09 am', 'data7': 'Permission'},
      {id: '331', 'data1': 'Appraisal: General', 'data2': 'Profit\/Loss', 'data3': '', 'data4': 'ascopesuper', 'data5': '08\/11\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '338', 'data1': 'Appraisal: General', 'data2': 'Copy - rpt_custom_fields', 'data3': '', 'data4': 'debapriya', 'data5': '02\/25\/2014', 'data6': '02\/25\/2014 01:16:46 am', 'data7': 'Permission'},
      {id: '349', 'data1': 'Appraisal: General', 'data2': 'PL', 'data3': '', 'data4': 'debapriya', 'data5': '08\/13\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '352', 'data1': 'Appraisal: General', 'data2': 'Test', 'data3': '', 'data4': 'ascopesuper', 'data5': '08\/14\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '363', 'data1': 'Appraisal: General', 'data2': 'Copy - TESTING REPORT', 'data3': '', 'data4': 'debapriya', 'data5': '12\/18\/2013', 'data6': '08\/13\/2014 03:05:05 pm', 'data7': 'Permission'},
      {id: '369', 'data1': 'Appraisal: General', 'data2': '', 'data3': '', 'data4': 'agni', 'data5': '08\/28\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '371', 'data1': 'Appraisal: General', 'data2': 'Test Virat', 'data3': '', 'data4': 'ascopesuper', 'data5': '09\/01\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '375', 'data1': 'Appraisal: General', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '09\/09\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '379', 'data1': 'Appraisal: General', 'data2': '', 'data3': '', 'data4': 'kellycopani', 'data5': '09\/15\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '380', 'data1': 'Appraisal: General', 'data2': 'TEST', 'data3': '', 'data4': 'ascopesuper', 'data5': '09\/15\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '387', 'data1': 'Appraisal: General', 'data2': 'RSA fields', 'data3': '', 'data4': 'ascopesuper', 'data5': '09\/22\/2014', 'data6': '09\/22\/2014 10:22:13 pm', 'data7': 'Permission'},
      {id: '388', 'data1': 'Appraisal: General', 'data2': 'Fee Teset', 'data3': '', 'data4': 'ascopesuper', 'data5': '09\/23\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '390', 'data1': 'Appraisal: General', 'data2': 'Test Pipeline', 'data3': '', 'data4': 'agni', 'data5': '09\/23\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '393', 'data1': 'Appraisal: General', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '09\/29\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '394', 'data1': 'Appraisal: General', 'data2': 'test report!!', 'data3': '', 'data4': 'ascopesuper', 'data5': '10\/02\/2014', 'data6': '10\/02\/2014 11:26:38 am', 'data7': 'Permission'},
      {id: '395', 'data1': 'Appraisal: General', 'data2': 'Appraiser Due Date', 'data3': '', 'data4': 'ascopesuper', 'data5': '10\/06\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '397', 'data1': 'Appraisal: General', 'data2': 'Profit Report', 'data3': '', 'data4': 'ascopesuper', 'data5': '10\/10\/2014', 'data6': '12\/22\/2014 04:05:05 am', 'data7': 'Permission'},
      {id: '399', 'data1': 'Appraisal: General', 'data2': 'SS_Daily report', 'data3': '', 'data4': 'ascopesuper', 'data5': '10\/13\/2014', 'data6': '11\/03\/2014 08:35:04 pm', 'data7': 'Permission'},
      {id: '400', 'data1': 'Appraisal: General', 'data2': 'TEST REPORT', 'data3': '', 'data4': 'ascopesuper', 'data5': '10\/13\/2014', 'data6': '12\/22\/2014 01:05:04 pm', 'data7': 'Permission'},
      {id: '402', 'data1': 'Appraisal: General', 'data2': 'Bank Lion Profit', 'data3': '', 'data4': 'ascopesuper', 'data5': '10\/23\/2014', 'data6': '12\/22\/2014 04:05:06 am', 'data7': 'Permission'},
      {id: '403', 'data1': 'Appraisal: General', 'data2': 'Revisions', 'data3': '', 'data4': 'rhonda', 'data5': '11\/03\/2014', 'data6': '11\/03\/2014 09:47:29 am', 'data7': 'Permission'},
      {id: '404', 'data1': 'Appraisal: General', 'data2': 'Performance Report', 'data3': 'Management Report Reflecting Turn Times and Value', 'data4': 'rhonda', 'data5': '11\/07\/2014', 'data6': '11\/07\/2014 11:39:06 am', 'data7': 'Permission'},
      {id: '407', 'data1': 'Appraisal: General', 'data2': 'Test - S', 'data3': '', 'data4': 'agni', 'data5': '11\/17\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '409', 'data1': 'Appraisal: General', 'data2': 'Payroll', 'data3': '', 'data4': 'ascopesuper', 'data5': '11\/20\/2014', 'data6': '12\/19\/2014 12:35:05 am', 'data7': 'Permission'},
      {id: '419', 'data1': 'Appraisal: General', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '12\/09\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '420', 'data1': 'Appraisal: General', 'data2': 'Paid Status', 'data3': '', 'data4': 'ascopesuper', 'data5': '12\/19\/2014', 'data6': '12\/23\/2014 08:44:33 pm', 'data7': 'Permission'},
      {id: '422', 'data1': 'Appraisal: General', 'data2': 'Testing On Hold Orders - VR', 'data3': 'Testing Report# 21 - On Hold Orders - VR', 'data4': 'ascopesuper', 'data5': '12\/21\/2014', 'data6': '12\/21\/2014 08:50:58 pm', 'data7': 'Permission'},
      {id: '423', 'data1': 'Appraisal: General', 'data2': 'Paid Status Report', 'data3': '', 'data4': 'ascopesuper', 'data5': '12\/23\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '425', 'data1': 'Appraisal: General', 'data2': 'test paid status', 'data3': '', 'data4': 'ascopesuper', 'data5': '12\/23\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '197', 'data1': 'Appraisal: Status Log', 'data2': 'Status', 'data3': '', 'data4': 'ascopesuper', 'data5': '04\/22\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '127', 'data1': 'Appraisal: Comments & Notification', 'data2': 'Comment_Rpt1', 'data3': '', 'data4': 'ascopesuper', 'data5': '01\/30\/2014', 'data6': '03\/27\/2015 09:45:12 am', 'data7': 'Permission'},
      {id: '58', 'data1': 'Appraisal: Revisions\/Stipulations', 'data2': 'Ratings', 'data3': 'This report shows quality, service, and turn-around time ratings for all client companies.', 'data4': 'ascopesuper', 'data5': '08\/16\/2013', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '178', 'data1': 'Appraisal: Revisions\/Stipulations', 'data2': 'Report testing', 'data3': '', 'data4': 'ascopesuper', 'data5': '03\/03\/2014', 'data6': '03\/03\/2014 11:29:55 pm', 'data7': 'Permission'},
      {id: '225', 'data1': 'Appraisal: Revisions\/Stipulations', 'data2': 'Reviewer Type Test', 'data3': '', 'data4': 'ascopesuper', 'data5': '06\/05\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '283', 'data1': 'Appraisal: Revisions\/Stipulations', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/10\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '286', 'data1': 'Appraisal: Revisions\/Stipulations', 'data2': 'TEST', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/11\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '311', 'data1': 'Appraisal: Revisions\/Stipulations', 'data2': 'Priority Test', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/22\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '312', 'data1': 'Appraisal: Revisions\/Stipulations', 'data2': 'Client Submitted Revision', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/23\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '153', 'data1': 'Appraisal: Documents', 'data2': 'Report Delivered', 'data3': '', 'data4': 'johnsmith', 'data5': '02\/19\/2014', 'data6': '02\/19\/2014 03:03:01 am', 'data7': 'Permission'},
      {id: '95', 'data1': 'Client: General', 'data2': 'Client Order Volume', 'data3': 'This report provides a list of orders submitted by date by all clients.', 'data4': 'lionsmith', 'data5': '10\/31\/2013', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '101', 'data1': 'Client: General', 'data2': 'Copy - Client Order Volume', 'data3': 'This report provides a list of orders submitted by date by all clients.', 'data4': 'ascopesuper', 'data5': '10\/31\/2013', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '126', 'data1': 'Client: User List', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '01\/28\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '132', 'data1': 'Client: Client on Reports', 'data2': 'Test Client on Report', 'data3': '', 'data4': 'ascopesuper', 'data5': '02\/06\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '262', 'data1': 'Client: Approve Appraiser List', 'data2': 'approve appraiser list', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/01\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '270', 'data1': 'Client: Approve Appraiser List', 'data2': 'Test_AAList', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/02\/2014', 'data6': '07\/02\/2014 04:53:49 am', 'data7': 'Permission'},
      {id: '210', 'data1': 'Client: Instructions', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '05\/13\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '206', 'data1': 'Client: Fee Schedules', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '05\/13\/2014', 'data6': '06\/05\/2014 10:05:13 pm', 'data7': 'Permission'},
      {id: '224', 'data1': 'Client: Fee Schedules', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '06\/05\/2014', 'data6': '06\/05\/2014 01:59:22 am', 'data7': 'Permission'},
      {id: '365', 'data1': 'Client: Fee Schedules', 'data2': '', 'data3': '', 'data4': 'debapriya', 'data5': '08\/19\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '356', 'data1': 'Client: Documents', 'data2': 'Client Document', 'data3': '', 'data4': 'ascopesuper', 'data5': '08\/14\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '86', 'data1': 'Appraiser: General', 'data2': 'Appraiser List', 'data3': 'This report shows all appraisers and their respective contact information and license numbers.', 'data4': 'ascopesuper', 'data5': '09\/27\/2013', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '112', 'data1': 'Appraiser: General', 'data2': 'test_appr1', 'data3': '', 'data4': 'ascopesuper', 'data5': '01\/20\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '131', 'data1': 'Appraiser: General', 'data2': 'Appraisers', 'data3': '', 'data4': 'ascopesuper', 'data5': '02\/05\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '221', 'data1': 'Appraiser: General', 'data2': 'Appraiser Disabled', 'data3': '', 'data4': 'ascopesuper', 'data5': '06\/04\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '229', 'data1': 'Appraiser: General', 'data2': 'Appraiser Counties', 'data3': '', 'data4': 'ascopesuper', 'data5': '06\/05\/2014', 'data6': '06\/05\/2014 01:53:48 pm', 'data7': 'Permission'},
      {id: '276', 'data1': 'Appraiser: General', 'data2': 'Appraiser Registered Test', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/02\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '298', 'data1': 'Appraiser: General', 'data2': 'test_vendor1', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/13\/2014', 'data6': '07\/13\/2014 10:08:47 pm', 'data7': 'Permission'},
      {id: '300', 'data1': 'Appraiser: General', 'data2': 'Vendor Status', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/14\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '406', 'data1': 'Appraiser: General', 'data2': 'Appraisers by State', 'data3': '', 'data4': 'rhonda', 'data5': '11\/07\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '416', 'data1': 'Appraiser: General', 'data2': 'Appraiser Approved List', 'data3': '', 'data4': 'ascopesuper', 'data5': '12\/08\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '208', 'data1': 'Appraiser: Fee Schedule', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '05\/13\/2014', 'data6': '05\/13\/2014 03:55:43 am', 'data7': 'Permission'},
      {id: '99', 'data1': 'Appraisal: Borrower Report History', 'data2': 'Borrower History', 'data3': 'This report shows the borrower history.', 'data4': 'ascopesuper', 'data5': '12\/11\/2013', 'data6': '09\/20\/2014 12:05:09 pm', 'data7': 'Permission'},
      {id: '277', 'data1': 'Appraiser: Scorecard', 'data2': 'Appraiser Scorecard', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/07\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '414', 'data1': 'Appraiser: Scorecard', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '12\/04\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '418', 'data1': 'Appraiser: Scorecard', 'data2': 'Appraiser Scorecard', 'data3': '', 'data4': 'ascopesuper', 'data5': '12\/08\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '212', 'data1': 'Appraiser: Tech Fee', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '05\/21\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '213', 'data1': 'Appraiser: Tech Fee', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '05\/29\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '242', 'data1': 'Client: Scorecard', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '06\/11\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '412', 'data1': 'Client: Scorecard', 'data2': '', 'data3': '', 'data4': 'ascopesuper', 'data5': '12\/04\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '417', 'data1': 'Client: Scorecard', 'data2': 'Bank Lion Scorecard', 'data3': '', 'data4': 'ascopesuper', 'data5': '12\/08\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '292', 'data1': 'Staff: Staff Productivity', 'data2': 'Staff Productivity - Detail', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/11\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '293', 'data1': 'Staff: Staff Productivity', 'data2': 'Staff Productivity - Summary', 'data3': '', 'data4': 'ascopesuper', 'data5': '07\/11\/2014', 'data6': '09\/29\/2014 02:19:59 am', 'data7': 'Permission'},
      {id: '342', 'data1': 'Staff: Staff Productivity', 'data2': 'Productivity stat', 'data3': '', 'data4': 'debapriya', 'data5': '08\/13\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '359', 'data1': 'Staff: Staff Productivity', 'data2': 'Staff productivity', 'data3': 'Staff productivity', 'data4': 'debapriya', 'data5': '08\/18\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '367', 'data1': 'Staff: Staff Productivity', 'data2': '', 'data3': '', 'data4': 'agni', 'data5': '08\/27\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '382', 'data1': 'Staff: Staff Productivity', 'data2': 'Appraisal Nation - Al', 'data3': '', 'data4': 'ascopesuper', 'data5': '09\/19\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '383', 'data1': 'Staff: Staff Productivity', 'data2': 'Appraisal Nation - Ken', 'data3': '', 'data4': 'ascopesuper', 'data5': '09\/19\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '401', 'data1': 'Staff: Staff Productivity', 'data2': 'Revision Sent', 'data3': '', 'data4': 'ascopesuper', 'data5': '10\/14\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '328', 'data1': 'Appraisal: Commissions', 'data2': 'COMMISSION REPORT', 'data3': '', 'data4': 'ascopesuper', 'data5': '08\/07\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '334', 'data1': 'Appraisal: Commissions', 'data2': 'P & L statement', 'data3': 'P & L', 'data4': 'debapriya', 'data5': '08\/12\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '343', 'data1': 'Appraisal: Commissions', 'data2': 'Commision_BankLion', 'data3': '', 'data4': 'agni', 'data5': '08\/13\/2014', 'data6': 'N\/A', 'data7': 'Permission'},
      {id: '345', 'data1': 'Appraisal: Commissions', 'data2': 'commissions', 'data3': '', 'data4': 'debapriya', 'data5': '08\/13\/2014', 'data6': '08\/19\/2014 08:40:40 am', 'data7': 'Permission'},
      {id: '411', 'data1': 'Appraisal: Form Data', 'data2': 'Form Data', 'data3': '', 'data4': 'ascopesuper', 'data5': '11\/21\/2014', 'data6': 'N\/A', 'data7': 'Permission'}
    ];
  },

  getAvailableStaffPermission : function(){
    return [
      {"id":"4377024","name":"Agni Basu"},
      {"id":"4376718","name":"Anniemac API"},
      {"id":"4377342","name":"Arturo"},
      {"id":"4377673","name":"azio"},
      {"id":"4377201","name":"David Vitale"},
      {"id":"4377027","name":"Debapriya Mitra"},
      {"id":"4377572","name":"designer"},
      {"id":"2675463","name":"Jeff Colbert"},
      {"id":"2675378","name":"John Smith"},
      {"id":"1355732","name":"John Stone"},
      {"id":"4377660","name":"Keyur"},
      {"id":"4377179","name":"Lion Smith"},
      {"id":"4377661","name":"Mahavir Jain"},
      {"id":"2675704","name":"Mehul Batliwala"},
      {"id":"4377036","name":"Pragnesh Smith"},
      {"id":"2675466","name":"Rich Sargo"},
      {"id":"2675327","name":"Sam Jones"},
      {"id":"4377476","name":"Todd Loomis"},
      {"id":"2675503","name":"Tom Smith"},
      {"id":"4377659","name":"Virat Gaywala"}
    ];
  },

  getStaffPermission : function(){
    return [ "4377024", "4376718", "4377342", "4377673", "4377201", "4377027", "4377572" ];
  },

  getAvailableClientPermission : function(){
    return [
      {"id":"4377406","name":"AAAA Company","userLevels" : {}},
      {"id":"104743","name":"AALAND APPRAISALS","userLevels":{"3" : true, "1" : true}},
      {"id":"104742","name":"AALAND APPRAISALS","userLevels" : {}},
      {"id":"104744","name":"AALAND APPRAISALS","userLevels":{"1" : true, "3" : true}},
      {"id":"4377460","name":"ABC - Media Bank","userLevels":{"3" : true, "2" : true}},
      {"id":"1355730","name":"ABC's Bank","userLevels" : {}},
      {"id":"4377578","name":"ABC-Media Bank Branch 1","userLevels" : {}},
      {"id":"131492","name":"Acceptance Capital Mortgage Corp","userLevels":{"1" : true, "2" : true}},
      {"id":"4377232","name":"Ace Bank","userLevels" : {}},
      {"id":"4377355","name":"All Right Bank","userLevels" : {}},
      {"id":"131493","name":"Allied First Bank","userLevels" : {}},
      {"id":"2675524","name":"Amcap Mortgage Ltd.","userLevels" : {}},
      {"id":"131494","name":"American Financial Funding","userLevels" : {}},
      {"id":"131495","name":"American Financial Resources Inc.","userLevels" : {}},
      {"id":"131496","name":"American Financial Resources, Inc.","userLevels" : {}},
      {"id":"2675527","name":"America\\'s Choice Home Loans","userLevels" : {}},
      {"id":"4377433","name":"Amy's Broker Company","userLevels" : {}},
      {"id":"2675622","name":"asdasd","userLevels" : {}},
      {"id":"131497","name":"Associated Mortgage Bankers, Inc.","userLevels" : {}},
      {"id":"131498","name":"At Your Service","userLevels" : {}},
      {"id":"2675336","name":"Bank Lion","userLevels" : {}},
      {"id":"4377300","name":"Bank RJB","userLevels" : {}},
      {"id":"4377242","name":"Batacompany Inc.","userLevels" : {}},
      {"id":"4377576","name":"BBBB Company","userLevels" : {}},
      {"id":"4377263","name":"BC Company","userLevels" : {}},
      {"id":"2675290","name":"Big Test Bank","userLevels" : {}},
      {"id":"131499","name":"Bluegrass Appraisal Management, Inc","userLevels" : {}},
      {"id":"131500","name":"BofI Federal Bank","userLevels" : {}},
      {"id":"2675380","name":"Brand New Company","userLevels" : {}},
      {"id":"4377374","name":"Broker JAB","userLevels" : {}},
      {"id":"2675339","name":"Broker Lion","userLevels" : {}},
      {"id":"4377376","name":"Broker RJB","userLevels" : {}},
      {"id":"131501","name":"CAA Real Property Service","userLevels" : {}},
      {"id":"131502","name":"CalCon Mutual","userLevels" : {}},
      {"id":"2675530","name":"Capstar Lending","userLevels" : {}},
      {"id":"131503","name":"Carrington Mortgage Services, LLC","userLevels" : {}},
      {"id":"131504","name":"CashCall, Inc.","userLevels" : {}},
      {"id":"131505","name":"Castle Arch","userLevels" : {}},
      {"id":"4377595","name":"CCCC Company","userLevels" : {}},
      {"id":"4377222","name":"Churchill Mortgage","userLevels" : {}},
      {"id":"131506","name":"City 1st Mortgage Services","userLevels" : {}},
      {"id":"131507","name":"Clear Point Funding","userLevels" : {}},
      {"id":"131508","name":"ClearVision Funding, LLC","userLevels" : {}},
      {"id":"131509","name":"Cliffco Inc","userLevels" : {}},
      {"id":"2675533","name":"Colina Homes","userLevels" : {}},
      {"id":"131510","name":"Command Capital Mortgage Services, LLC","userLevels" : {}},
      {"id":"2675421","name":"Company Name","userLevels" : {}},
      {"id":"131511","name":"Contour Mortgage Corporation","userLevels" : {}},
      {"id":"4378800","name":"Crest Infosystems","userLevels" : {}},
      {"id":"131512","name":"Customized Mortgage Solutions","userLevels" : {}},
      {"id":"4376793","name":"Demo","userLevels" : {}},
      {"id":"4376798","name":"Demo","userLevels" : {}},
      {"id":"4376804","name":"Demo xyz","userLevels" : {}},
      {"id":"2675536","name":"DHI Mortgage","userLevels" : {}},
      {"id":"131513","name":"Eagle Home Mortgage","userLevels" : {}},
      {"id":"131514","name":"Eagle National Bank","userLevels" : {}},
      {"id":"131515","name":"Eagle National Mortgage Corporation","userLevels" : {}},
      {"id":"131516","name":"Eagle Nationwide Mortgage Company","userLevels" : {}},
      {"id":"131517","name":"El Paso Area Teachers Federal Credit Union","userLevels" : {}},
      {"id":"131518","name":"Emery Federal Credit Union","userLevels" : {}}
    ];
  },

  getClientPermissionUserLevels : function(){
    return [
      {"id" : '1', "name":'processor', "nameAs":'Processor'},
      {"id" : '2', "name":'manager', "nameAs":'Manager'},
      {"id" : '3', "name":'loanOfficer', "nameAs":'Loan Officer'}
    ];
  },

  dynamicFilters : function(){
    return [
      {"fieldName":"col_2810_1","fieldLabel":"Completed Date","fieldType":"datefield","condition":">="},
      {"fieldName":"col_2856_2","fieldLabel":"Borrower's Cell #","fieldType":"textfield","condition":"<>"}
    ];
  },
  reportAlerts : function(){
    return [
      {"id":"56","name":"aaaaa"},
      {"id":"58","name":"ababa"},
      {"id":"57","name":"bbbb"},
      {"id":"17","name":"Borrower Report History"},
      {"id":"24","name":"New Report Alert"},
      {"id":"55","name":"NewAler"},
      {"id":"16","name":"test"},
      {"id":"20","name":"test"},
      {"id":"21","name":"Test"},
      {"id":"49","name":"testA"}
    ];
  },

  reportAlert : function(){
    return {
      "alertDate":"03/25/2015",
      "weekdays":[true,true,true,true,null,null,null],
      "monthDate":"07",
      "Emails":"email@email.com",
      "cmbHour":"08",
      "cmbMeridian":"PM",
      "col_2810_1":"03/20/2015",
      "col_2810_1_cnd":">=",
      "col_2856_2":"16",
      "col_2856_2_cnd":"<>"
    }
  },

  getReportTypes : function(){
    return [
      { id: 1, groupCategory: 'Appraisal', name: 'General', is_set: true },
      { id: 2, groupCategory: 'Appraisal', name: 'Status Log', is_set: true },
      { id: 3, groupCategory: 'Appraisal', name: 'Comments & Notification', is_set: true },
      { id: 4, groupCategory: 'Appraisal', name: 'Revisions\/Stipulations', is_set: true },
      { id: 5, groupCategory: 'Appraisal', name: 'Documents', is_set: true },
      { id: 33, groupCategory: 'Appraisal', name: 'Borrower Report History', is_set: true },
      { id: 39, groupCategory: 'Appraisal', name: 'Commissions', is_set: true },
      { id: 40, groupCategory: 'Appraisal', name: 'Form Data', is_set: true },
      { id: 11, groupCategory: 'Client', name: 'General', is_set: true },
      { id: 12, groupCategory: 'Client', name: 'User List', is_set: true },
      { id: 13, groupCategory: 'Client', name: 'Notes', is_set: true },
      { id: 14, groupCategory: 'Client', name: 'Client on Reports', is_set: true },
      { id: 15, groupCategory: 'Client', name: 'Checklists', is_set: true },
      { id: 16, groupCategory: 'Client', name: 'Approve Appraiser List', is_set: true },
      { id: 17, groupCategory: 'Client', name: 'Instructions', is_set: true },
      { id: 18, groupCategory: 'Client', name: 'Fee Schedules', is_set: true },
      { id: 19, groupCategory: 'Client', name: 'Documents', is_set: true },
      { id: 36, groupCategory: 'Client', name: 'Scorecard', is_set: true },
      { id: 21, groupCategory: 'Appraiser', name: 'General', is_set: true },
      { id: 22, groupCategory: 'Appraiser', name: 'Notes', is_set: true },
      { id: 23, groupCategory: 'Appraiser', name: 'Fee Schedule', is_set: true },
      { id: 34, groupCategory: 'Appraiser', name: 'Scorecard', is_set: true },
      { id: 35, groupCategory: 'Appraiser', name: 'Tech Fee', is_set: true },
      { id: 38, groupCategory: 'Appraiser', name: 'Pending Appraisers', is_set: true },
      { id: 31, groupCategory: 'Job', name: 'Job Lists', is_set: true },
      { id: 32, groupCategory: 'Job', name: 'Fee Schedule', is_set:false },
      { id: 37, groupCategory: 'Staff', name: 'Staff Productivity', is_set: true }
    ];
  },

  getReportColumns: function () {
    return [{
      id: 77001,
      label: 'Appraisal',
      children: [{
        id: 2432,
        label: 'Address1',
        datatype: 'varchar'
      }, {
        id: 2433,
        label: 'Address2',
        datatype: 'varchar'
      }, {
        id: 2442,
        label: 'Appraisal Form',
        datatype: 'varchar'
      }, {
        id: 2447,
        label: 'Appraiser Fee',
        datatype: 'int'
      }, {
        id: 2444,
        label: 'Borrower\'s name',
        datatype: 'varchar'
      }]
    }, {
      id: 77002,
      label: 'Customer',
      children: [{
        id: 2434,
        label: 'City',
        datatype: 'varchar'
      }, {
        id: 2446,
        label: 'Client Fee',
        datatype: 'int'
      }, {
        id: 2445,
        label: 'Completed Date',
        datatype: 'date'
      }, {
        id: 2437,
        label: 'File #',
        datatype: 'varchar'
      }, {
        id: 2443,
        label: 'Order Date',
        datatype: 'date'
      }, {
        id: 2961,
        label: 'Paid Status',
        datatype: 'varchar'
      }, {
        id: 2451,
        label: 'PL Amount',
        datatype: 'float'
      }]
    }, {
      id: 77003,
      label: 'Orders',
      children: [{
        id: 2449,
        label: 'Process Status',
        datatype: 'varchar'
      }, {
        id: 2435,
        label: 'State',
        datatype: 'varchar'
      }, {
        id: 2436,
        label: 'Zip',
        datatype: 'varchar'
      }, {
        id: 2441,
        label: 'Company name',
        datatype: 'varchar'
      }, {
        id: 2438,
        label: 'Company name',
        datatype: 'varchar'
      }, {
        id: 2439,
        label: 'Branch name',
        datatype: 'varchar'
      }, {
        id: 2450,
        label: 'Commission Amount',
        datatype: 'float'
      }, {
        id: 2430,
        label: 'Name',
        datatype: 'varchar'
      }, {
        id: 2440,
        label: 'Branch name',
        datatype: 'varchar'
      }]
    }];
  },

  generateReport : function(){
    return [
      { "col_2437": "teste4", "col_2430": "bob sue", "col_2438": "Bank of America ", "col_2442": "ewrwes3312222", "col_2444": "test lname", "col_2432": "test", "col_2443": "01-31-2013", "col_2445": "", "col_2446": "22.00", "col_2447": "0.00", "col_2451": "22.00", "col_2450": "5.00", "col_2961": "Paid (Check)", "col_2449": "Ready for Review"},
      { "col_2437": "teste20", "col_2430": "bob sue", "col_2438": "Wells Fargo", "col_2442": "big form", "col_2444": "Sam A Dan", "col_2432": "Bla", "col_2443": "02-24-2013", "col_2445": "", "col_2446": "17.00", "col_2447": "0.00", "col_2451": "17.00", "col_2450": "0.00", "col_2961": "Paid (Credit Card)", "col_2449": "Ready for Review"},
      { "col_2437": "teste42", "col_2430": "bob sue", "col_2438": "Jordan company", "col_2442": "Test job 2", "col_2444": "hello hi roth", "col_2432": "123 abc lane", "col_2443": "03-13-2013", "col_2445": "", "col_2446": "171.00", "col_2447": "0.00", "col_2451": "171.00", "col_2450": "5.00", "col_2961": "Paid (Check)", "col_2449": "Ready for Review"},
      { "col_2437": "tes89", "col_2430": "bob sue", "col_2438": "gcufexmmle", "col_2442": "job 1", "col_2444": "ttttttttttaDASFSAF ttttttttttSFASFASF tttttttttt", "col_2432": "jhjhj", "col_2443": "11-05-2012", "col_2445": "03-14-2013", "col_2446": "1.00", "col_2447": "2.00", "col_2451": "-1.00", "col_2450": "0.00", "col_2961": "Paid (Credit Card)", "col_2449": "Ready for Review"},
      { "col_2437": "teste46", "col_2430": "bob sue", "col_2438": "Wells Fargo", "col_2442": "job 1", "col_2444": "dfsdf ", "col_2432": "dfdsfsd", "col_2443": "03-18-2013", "col_2445": "", "col_2446": "1.00", "col_2447": "0.00", "col_2451": "1.00", "col_2450": "0.00", "col_2961": "Paid (Credit Card)", "col_2449": "Ready for Review"},
      { "col_2437": "tes84", "col_2430": "bob sue", "col_2438": "gcufexmmle", "col_2442": "job 2", "col_2444": "rereeeeeeeeeeeeeee jk jk", "col_2432": "jhjhj", "col_2443": "11-01-2012", "col_2445": "", "col_2446": "3.00", "col_2447": "15.00", "col_2451": "-12.00", "col_2450": "0.00", "col_2961": "Paid (Credit Card)", "col_2449": "Ready for Review"},
      { "col_2437": "tes83", "col_2430": "bob sue", "col_2438": "gcufexmmle", "col_2442": "job 1", "col_2444": "jk jk jk", "col_2432": "jhjhj", "col_2443": "10-31-2012", "col_2445": "", "col_2446": "1.00", "col_2447": "12.00", "col_2451": "-11.00", "col_2450": "0.00", "col_2961": "Paid (Check)", "col_2449": "Ready for Review"},
      { "col_2437": "teste59", "col_2430": "bob sue", "col_2438": "Wells Fargo", "col_2442": "job 1", "col_2444": "Bla A Nai", "col_2432": "Bla", "col_2443": "04-08-2013", "col_2445": "", "col_2446": "1.00", "col_2447": "0.00", "col_2451": "1.00", "col_2450": "0.00", "col_2961": null, "col_2449": "Ready for Review"},
      { "col_2437": "teste60", "col_2430": "bob sue", "col_2438": "Wells Fargo", "col_2442": "ewrwes3312222", "col_2444": "alb a nai", "col_2432": "Bla B", "col_2443": "04-08-2013", "col_2445": "", "col_2446": "32.00", "col_2447": "0.00", "col_2451": "32.00", "col_2450": "5.00", "col_2961": null, "col_2449": "Ready for Review"},
      { "col_2437": "teste61", "col_2430": "bob sue", "col_2438": "Wells Fargo", "col_2442": "ewrwes3312222", "col_2444": "Bla A Test", "col_2432": "Bla", "col_2443": "04-15-2013", "col_2445": "", "col_2446": "32.00", "col_2447": "0.00", "col_2451": "32.00", "col_2450": "5.00", "col_2961": "Paid (Credit Card)", "col_2449": "Ready for Review"},
      { "col_2437": "teste63", "col_2430": "bob sue", "col_2438": "Wells Fargo", "col_2442": "ewrwes3312222", "col_2444": "Bla A Test", "col_2432": "Bla", "col_2443": "04-15-2013", "col_2445": "", "col_2446": "32.00", "col_2447": "0.00", "col_2451": "32.00", "col_2450": "5.00", "col_2961": "Paid (Credit Card)", "col_2449": "Ready for Review"},
      { "col_2437": "teste71", "col_2430": "bob sue", "col_2438": "Wells Fargo", "col_2442": "green eggs and ham", "col_2444": "test entry", "col_2432": "test", "col_2443": "04-15-2013", "col_2445": "", "col_2446": "40.00", "col_2447": "0.00", "col_2451": "40.00", "col_2450": "5.00", "col_2961": "Paid (Check)", "col_2449": "Ready for Review"},
      { "col_2437": "teste66", "col_2430": "bob sue", "col_2438": "Wells Fargo", "col_2442": "ewrwes3312222", "col_2444": "test entry", "col_2432": "test", "col_2443": "04-15-2013", "col_2445": "", "col_2446": "62.00", "col_2447": "0.00", "col_2451": "62.00", "col_2450": "5.00", "col_2961": "Paid (Credit Card)", "col_2449": "Ready for Review"},
      { "col_2437": "teste72", "col_2430": "bob sue", "col_2438": "Wells Fargo", "col_2442": "green eggs and ham", "col_2444": "sdfdf dfdf", "col_2432": "123 abc lane", "col_2443": "04-16-2013", "col_2445": "", "col_2446": "55.00", "col_2447": "0.00", "col_2451": "55.00", "col_2450": "5.00", "col_2961": null, "col_2449": "Ready for Review"},
      { "col_2437": "teste10", "col_2430": "bob sue", "col_2438": "Bank of America ", "col_2442": "ewrwes3312222", "col_2444": "test lname", "col_2432": "test", "col_2443": "02-04-2013", "col_2445": "05-23-2013", "col_2446": "32.00", "col_2447": "21.00", "col_2451": "11.00", "col_2450": "5.00", "col_2961": "Paid (Credit Card)", "col_2449": "Ready for Review"},
      { "col_2437": "teste114", "col_2430": "bob sue", "col_2438": "Jordan company", "col_2442": "Test job 2", "col_2444": "hello hi roth", "col_2432": "123 abc lane", "col_2443": "06-11-2013", "col_2445": "", "col_2446": "171.00", "col_2447": "0.00", "col_2451": "171.00", "col_2450": "5.00", "col_2961": "Paid (Check)", "col_2449": "Ready for Review"},
      { "col_2437": "tes59", "col_2430": "bob sue", "col_2438": "gcufexmmle", "col_2442": "job 1", "col_2444": "lkm kuri lasi", "col_2432": "sa 78", "col_2443": "10-03-2012", "col_2445": "", "col_2446": "1.00", "col_2447": "2.00", "col_2451": "-1.00", "col_2450": "0.00", "col_2961": "Paid (Check)", "col_2449": "Ready for Review"},
      { "col_2437": "tes32", "col_2430": "bob sue", "col_2438": "gcufexmmle", "col_2442": "job 1", "col_2444": "asdf asdf df", "col_2432": "232", "col_2443": "09-25-2012", "col_2445": "", "col_2446": "1.00", "col_2447": "0.00", "col_2451": "1.00", "col_2450": "5.00", "col_2961": "Paid (Check)", "col_2449": "Ready for Review"},
      { "col_2437": "ewve2224", "col_2430": "bob sue", "col_2438": "bubba", "col_2442": "(1004) URAR Full Single Family + 1004MC", "col_2444": "gggggggg gggggggg gggggggg", "col_2432": "4354 abc lane", "col_2443": "08-18-2012", "col_2445": "", "col_2446": "501.00", "col_2447": "600.00", "col_2451": "-99.00", "col_2450": "0.00", "col_2961": "Unpaid", "col_2449": "Ready for Review"},
      { "col_2437": "ewve2222", "col_2430": "bob sue", "col_2438": "Wells Fargo", "col_2442": "(1004) URAR Full Single Family + 1004MC", "col_2444": "Billy Robert Joel", "col_2432": "4444444 wobble lane", "col_2443": "09-18-2012", "col_2445": "10-04-2012", "col_2446": "501.00", "col_2447": "700.00", "col_2451": "-199.00", "col_2450": "0.00", "col_2961": "Unpaid", "col_2449": "Ready for Review"}
    ]
  },

  getReport : function(){
    return {
      id: 328,
      typeId: 39,
      name: 'COMMISSION REPORT',
      headerAlignment: 0,
      pageBreak: 0,
      description: '',
      header: '',
      detailType: 0,
      filters: [
        {
          id: '2430',
          name: 'first_name',
          table: 'salesperson',
          tabledisplaylabel: 'Salesperson',
          fieldlabel: 'name',
          fieldType: 'varchar(50)',
          operation: '=',
          condition: 'bob sue'},
        {
          id: '2447',
          name: 'appraisal_info_fee_2',
          table: 'info',
          tabledisplaylabel: 'Appraisal',
          fieldlabel: 'Appraiser Fee',
          fieldType: 'int(5)',
          operation: '<',
          condition: '@@DYNAMIC_DATA@@'},
        {
          id: '2445',
          name: 'Completed Date',
          table: 'appraisal',
          tabledisplaylabel: 'Appraisal',
          fieldlabel: 'Completed Date',
          fieldType: 'date',
          operation: 'LAST COMPLETED DAY',
          condition: 'asd'
        }
      ],
      'columns': [
        {nameAs: 'File #', name: 'col_2437'},
        {nameAs: 'First Name', name: 'col_2430'},
        {nameAs: 'Company Name', name: 'col_2438'},
        {nameAs: 'Appraisal Form', name: 'col_2442'},
        {nameAs: 'Borrower\'s Name', name: 'col_2444'},
        {nameAs: 'Address1', name: 'col_2432'},
        {nameAs: 'Order Date', name: 'col_2443'},
        {nameAs: 'Completed Date', name: 'col_2445'},
        {nameAs: 'Client Fee', name: 'col_2446', total: 'true'},
        {nameAs: 'Appraiser Fee', name: 'col_2447', total: 'true'},
        {nameAs: 'PL Amount', name: 'col_2451', total: 'true'},
        {nameAs: 'Commission Amount', name: 'col_2450', total: 'true'},
        {nameAs: 'Paid Status', name: 'col_2961'},
        {nameAs: 'Process Status', name: 'col_2449'}
      ],
      'groupers': [
        {name : 'col_2430', nameAs:'First Name', reverse : false}
      ],
      'sorters': []
    };
  }
};