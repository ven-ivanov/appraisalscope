/**
 * DEPRECATED Use appraisals/appraisal
 */

var g = require('../../generator/generator');
var f = require('faker');
var adminObjects = require('./objects');
var adminClientObjects = require('./clients');
var ClientObjects = require('../client/clientObjects');

module.exports = {
  appraisal: function(include){
    var data = {};
    var update = g.bool();

    // Break down include into an array if it's set
    include = include? include.split(','): null;

    data.id          = g.id();
    data.hasPriority = g.bool();
    data.specialCare = g.bool()? f.lorem.paragraph(): null;
    data.fha         = g.bool()? f.lorem.words()[0]: null;
    data.comment     = g.bool()? f.lorem.paragraph(): null;
    data.warning     = g.bool()? f.lorem.paragraph(): null;
    data.parent      = g.bool()? f.helpers.randomNumber(): null;

    if (include && include.indexOf('amc') !== -1) {
      data.amc = {
        licenses: adminObjects.amcLicence()
      };
    }

    data.order      = this.order();
    data.loan       = adminObjects.loan();
    data.borrower   = adminObjects .user();
    data.property   = this.property();
    data.assignment = g.bool()? this.assignment(): null;
    data.inspection = g.bool()? this.inspection(): null;
    data.client     = adminObjects.clientCompany();

    if (include && include.indexOf('clientOnReport') !== -1) {
      data.clientOnReport = adminClientObjects.clientOnReport();
    }

    data.reviewer = adminObjects.user();

    if (include && include.indexOf('supervisor') !== -1) {
      data.supervisor = adminObjects.user();
    }

    if (include && include.indexOf('admin') !== -1) {
      data.admin = adminObjects.user();
    }

    data.settings = this.appraisalSettings();
    data.listeners = this.listeners(g.random(5));
    data.listenerByEmail = f.internet.email();
    data.jobType = adminObjects.jobType();
    data.payment = this.payment();

    data.reason = g.bool()? f.lorem.sentence(): null;
    data.status = g.random(14);
    data.customStatus = g.randomNumber();
    data.updatedAt = update? g.date.past(): null;
    data.lastUpdate = update? this.update(): null;
    data.dueDate   = g.bool()? null: {
      client: g.date.future(),
      appraiser: g.date.future()
    };
    data.completedAt = g.bool()? g.date.past(): null;
    data.uploadedAt = g.bool()? g.date.past(): null;
    data.reviewedAt = g.bool()? g.date.past(): null;

    if (include && include.indexOf('instruction')) {
      data.instruction = this.instructions();
    }

    data.staff = g.bool()? adminObjects.user(): null;

    return data;
  },
  order: function () {
    return {
      id: g.randomNumber(),
      file: f.lorem.words(g.random(5)).join(' '),
      createdAt: g.date.past(),
      createdBy: g.bool()? adminObjects.user(): adminClientObjects.client(),
      createdFor: adminClientObjects.client()
    }
  },
  inspection: function () {
    return {
      id: g.randomChars(10),
      startedAt: g.date.past(),
      estimatedCompletionDate: g.bool()? g.date.future(): null,
      completed: g.bool(),
      settings: {
        reminderSent: g.bool()
      }
    }
  },
  appraisalSettings: function() {
    return {
      unacceptedTimeout: g.randomNumber()
    }
  },
  listeners: function(count) {
    var data = [];
    var listener = function() {
      return {
        id: g.randomNumber(),
        user: adminObjects.user(),
        events: g.bool()? f.lorem.words(): null
      }
    };

    if (count) {
      for(var i = 1; i <= count; i ++) {
        data.push(listener());
      }
    } else {
      data = listener();
    }

    return data;
  },
  payment: function(paid) {
    paid = typeof paid !== 'undefined' ? paid : g.bool();
    var paidAmount = g.randomFloat();
    var dueAmount  = !paid ? g.randomFloat(): 0.0;
    var totalAmount = paidAmount + dueAmount;

    return {
      paid: paid,
      method: g.random(4),
      profile: {
        cc: g.bool()? null: {
          number: g.randomChars(10)
        },
        bank: g.bool()? null: {
          name:    f.finance.accountName(),
          account: f.finance.account(),
          routing: f.finance.mask(),
          type:    f.finance.transactionType(),
          owner:   f.name.firstName() +' '+ f.name.lastName()
        }
      },
      amount: {
        total: paidAmount + dueAmount,
        paid: paidAmount,
        due: dueAmount
      },
      fee: {
        client:     Math.round(totalAmount * 0.15 * 100) / 100,
        appraiser:  Math.round(totalAmount * 0.25 * 100) / 100,
        reviewer:   Math.round(totalAmount * 0.3 * 100) / 100,
        supervisor: Math.round(totalAmount * 0.2 * 100) / 100,
        admin:      Math.round(totalAmount * 0.1 * 100) / 100
      },
      paidAt: paid? g.date.past(): null
    }
  },
  // Standard Property Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Property#standard-property-object
  property: function() {
    return {
      id:              g.randomChars(10),
      legal:           g.randomChars(10),
      type:            g.random(5),
      location:        adminObjects.location(),
      document:        adminObjects.document(),
      occupancy:       g.random(7),
      personToContact: g.randomNumber(),
      contacts: {
        borrower: this.contact(),
        coBorrower: this.contact(),
        owner: this.contact(),
        other: this.contact()
      }
    }
  },
  contact: function() {
    return {
      firstName: f.name.firstName(),
      lastName: f.name.lastName(),
      phones: {
        home: adminObjects.phone(),
        work: adminObjects.phone(),
        cell: adminObjects.phone()
      },
      email: f.internet.email(),
      mi: f.internet.userName()
    }
  },
  assignment: function() {
    return {
      appraiser: adminObjects.user(),
      status: g.random(14),
      condition: g.bool()? this.assigmentCondition(): null,
      reason: g.bool()? null: {
        title: f.lorem.sentence(),
        message: f.lorem.paragraph()
      },
      assignedAt: g.date.past(),
      acceptedAt: g.bool()? g.date.past(): null
    }
  },
  assigmentCondition: function() {
    return {
      fee: f.finance.amount(),
      dueDate: {
        client: g.date.future(),
        appraiser: g.date.future()
      },
      message: g.bool()? f.lorem.paragraph(): null
    }
  },
  instructions: function() {
    return {
      id: g.randomNumber(),
      title: f.lorem.words()[0],
      instruction: f.lorem.paragraph()
    }
  },
  bidSummary: function() {
    return {
      status: g.random(4),
      total: g.randomNumber(),
      lowest: f.finance.amount(),
      submittedAt: g.date.past()
    };
  },
  message: function() {
    return {
      id:       g.randomNumber(),
      sender:   adminObjects.user(),
      receiver: g.bool() ? adminObjects.user() : null,
      text:     f.lorem.paragraph(),
      visibility: {
        client:    g.bool(),
        appraiser: g.bool()
      },
      createdAt: g.date.past()
    }
  },
  revision: function() {
    return {
      id: g.randomNumber(),
      owner: adminObjects.user(),
      appraiser: g.bool()? adminObjects.user(): null,
      content: g.bool()? this.comps(g.random(5)): {
        name: 'revision',
        type: g.random(1),
        message: f.lorem.sentence(),
        isChecklist: g.bool()
      },
      createdAt: g.date.past(),
      uploadedAt: g.bool()? g.date.past(): null,
      readAt: g.bool()? g.date.past(): null
    };
  },
  comps: function(num) {
    var data = [];

    function createObject() {
      return {
        id: g.bool(),
        address: f.address.streetAddress(),
        salesPrice: f.finance.amount(),
        closedDate: g.date.past(),
        livingArea: f.lorem.words()[0],
        siteSize: f.lorem.words()[0],
        actualAge: f.lorem.words()[0],
        distanceToSubject: f.lorem.words()[0],
        sourceData: f.lorem.words()[0],
        comment: f.lorem.sentence()
      };
    }

    if (typeof num === 'number') {
      for (var i = 1; i <= num; i ++) {
        data.push(createObject());
      }
    } else {
      data = createObject();
    }

    return data;
  },
  ucdpSubmission: function() {
    return {
      lender: f.name.firstName(),
      orderId: g.randomChars(10),
      status: g.randomChars(10),
      type: g.random(5),
      submittedAt: g.date.past(),
      documents: g.bool()? null: {
        fannieMae: g.bool()? null: {
          file: f.lorem.words()[0],
          url: f.image.imageUrl()
        },
        freddieMac: g.bool()? null: {
          file: f.lorem.words()[0],
          url: f.image.imageUrl()
        }
      }
    }
  },
  update: function() {
    return {
      id: g.randomChars(10),
      status: g.random(14),
      document: g.bool()? adminObjects.document(): null,
      assignment: g.bool()? this.assignment(): null
    }
  },
  appraisalPersistable: function () {

  }
};
