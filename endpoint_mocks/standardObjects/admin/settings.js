var g = require('../../generator/generator');
var f = require('faker');

module.exports = {
  notification: function() {
    return {
      id: g.randomNumber(),
      label: f.lorem.words()[0],
      message: g.randomChars(20),
      isSetForAppraiser: g.bool()
    }
  }
};