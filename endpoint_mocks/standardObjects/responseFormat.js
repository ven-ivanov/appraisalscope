
module.exports = {
  pagination: function(request, response, createEntity) {
    var perPage = request.query.perPage? request.query.perPage: 10;
    var page    = request.query.page? request.query.page: 1;
    var resData = [];

    for (var i = 1; i <= perPage; i ++) {
      resData.push(createEntity());
    }

    return response.json({
      data: resData,
      meta: {
        pagination: {
          total:      1337,
          perPage:    perPage,
          page:       page,
          totalPages: Math.ceil((1337 / perPage))
        }
      }
    });
  }
};
