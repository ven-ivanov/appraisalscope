// Get all AMC certificates
var Appraisement = require('../appraisementObjects');
module.exports = {
  path: '/v2.0/appraisement/amcs/:amcId/certificates',
  callback: function (req, res) {
    var response = {data: []}, i;

    for (i = 1; i < 10; i = i + 1) {
      response.data.push(Appraisement.certificate(i));
    }

    return res.json(response);
  }
};