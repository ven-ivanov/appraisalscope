var faker = require('faker');
var General = require('../../../standardObjects/generalObjects');
var Appraisement = require('../appraisementObjects');
var _ = require('lodash');
var getInt = function () {
  return _.random(1, 100);
};
var getBool = function () {
  return _.sample([true, false]);
};
/**
 * AMC section objects
 */
var Amc = {
  /**
   * Standard AMC Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Amcs
   * 8/16
   */
  amc: function (id) {
    return _.assign(General.company(id), {
      contact: General.companyContact(),
      "ratingSummary": Appraisement.ratingsSummary(),
      "certificates": [Appraisement.certificate(), Appraisement.certificate()]
    });
  }
};

module.exports = Amc;