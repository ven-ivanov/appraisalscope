// Get all AMC certificates
var Appraisement = require('../appraisementObjects');
module.exports = {
  path: '/v2.0/appraisement/amcs/:amcId/certificates/:certificateId',
  callback: function (req, res) {
    return res.json(Appraisement.certificate(req.params.certificateId));
  }
};