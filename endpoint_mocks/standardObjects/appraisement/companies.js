var g = require('../../generator/generator');
var f = require('faker');

module.exports = {
  ach: function() {
    return {
      name: f.internet.userName(),
      accountNumber: g.randomChars(10),
      routing: g.randomChars(9)
    }
  }
};
