var Notes = require('../../standardObjects/admin/notes/notes');
var Admin = require('../../standardObjects/admin/objects');
var Appraisal = require('../../standardObjects/admin/appraisals');
var GeneralObjects = require('../../standardObjects/generalObjects');
var Client = require('../../standardObjects/client/clientObjects');
var Property = require('../../standardObjects/appraisals/property');
var Listener = require('../../standardObjects/appraisals/listeners');
var JobType = require('../../standardObjects/jobTypes/jobType');
var ClientOnReport = require('../../standardObjects/clientOnReport');
var Payment = require('../../standardObjects/appraisals/payments');
var _ = require('lodash');
var faker = require('faker');

/**
 * Functions to return random integers or floats
 * A present for Alex
 */
var getInt = function (includeNull) {
  if (includeNull) {
    return _.sample([null, getInt()])
  }
  return _.random(1, 100);
};
var getFloat = function (includeNull) {
  if (includeNull) {
    return _.sample([null, getFloat()]);
  }
  return _.random(1, 100, true);
};
var getBool = function () {
  return _.sample([true, false]);
};
var nullOrPhrase = function () {
  return _.sample([null, faker.hacker.phrase()]);
};
var getDate = function (canBeNull) {
  if (canBeNull) {
    return _.sample([null, getDate()])
  }
  return (new Date()).toISOString();
};
var getPercentage = function () {
  return _.random(1, 100, true)
};

/**
 * Conditionally include an item in an appraisal
 */
var conditionalInclude = function (appraisal, type, includeObj) {
  if (include.indexOf(type)) {
    _.assign(appraisal, includeObj);
  }
};

var appraisalCache = require('../../standardObjects/appraisals/cache').cache;

/**
 * Small cache for retrieving appraisement members
 */
var cache = {
  company: {},
  manager: {},
  appraiser: {}
};
/**
 * Appraisement objects
 */
var Appraisement = {
  /**
   * Standard Appraisal Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals#standard-appraisal-object
   * 8/13
   */
  appraisal: function (include, id, special) {
    id = id || getInt();
    // Return cached record, if available
    if (typeof appraisalCache[id] !== 'undefined') {
      return appraisalCache[id];
    }
    special = special || {};
    var appraisal = {
      "id": id || getInt(),
      "file": 'File ' + id,
      "hasPriority": getBool(),
      "specialCare": nullOrPhrase(),
      "fha": nullOrPhrase(),
      "comment": nullOrPhrase(),
      "warning": nullOrPhrase(),
      "parent": getInt(true),
      // AMC (conditional)
      "loan": GeneralObjects.loan(),
      "creator": _.sample([GeneralObjects.staff(), Client.employee()]),
      "owner": Client.employee(),
      property: Property.property(),
      /**
       * @todo This is causing a circular dependency
       */
      //assignment: _.sample([null, Assignment.assignment()]),
      inspection: Appraisement.inspection(),
      client: Client.clientCompany(typeof special.clientId !== 'undefined' ? special.clientId : null),
      // Client on report (conditional)
      reviewer: GeneralObjects.staff(),
      supervisor: GeneralObjects.user(),
      salesperson: _.sample([null, GeneralObjects.staff()]),
      admin: _.sample([null, GeneralObjects.user()]),
      settings: Appraisement.appraisalSettings(),
      listeners: [Listener.listener(), Listener.listener()],
      listenerByEmail: 'what@ever.com',
      jobType: JobType.jobType(),
      payment: Appraisement.payment(special.paid, special.invoiced, special.payablePaid),
      "reason": nullOrPhrase(),
      "status": Appraisement.appraisalStatusEnum(),
      "customStatus": getInt(),
      "createdAt": getDate(),
      "updatedAt": getDate(true),
      "dueDate": _.sample([null, {
        "client": getDate(),
        "appraiser": getDate()
      }]),
      "completedAt": getDate(typeof special.completedAt !== 'undefined' ? special.completedAt : true),
      "uploadedAt": getDate(true), // the time when the report was uploaded
      "reviewedAt": getDate(true),
      staff: _.sample([null, GeneralObjects.staff()])
    };

    // If no includes, return
    if (!_.isArray(include)) {
      // Push to cache
      appraisalCache[appraisal.id] = appraisal;
      return appraisal;
    }
    // Include AMC
    conditionalInclude(appraisal, 'amc', {
      "amc": {
        "license": Appraisement.amcLicense()
      }
    });
    // Include client on report
    conditionalInclude(appraisal, 'clientOnReport', {
      clientOnReport: ClientOnReport.clientOnReport()
    });
    // Include supervisor
    conditionalInclude(appraisal, 'supervisor', {
      supervisor: GeneralObjects.user()
    });
    // Include salesperson
    conditionalInclude(appraisal, 'salesperson', {
      salesperson: _.sample([null, GeneralObjects.staff()])
    });
    // Include admin
    conditionalInclude(appraisal, 'admin', {
      admin: GeneralObjects.user()
    });
    // Instruction
    conditionalInclude(appraisal, 'instruction', {
      instruction: Client.instruction()
    });

    // Push to cache
    appraisalCache[appraisal.id] = appraisal;

    return appraisal;
  },
  /**
   * Standard Appraiser Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers#standard-appraiser-object
   * 8/10
   */
  appraiser: function (id, manager, filterParams, excludeCompany, excludeBranch) {
    // Extend user
    return _.assign(GeneralObjects.user(id), {
      isManager: typeof manager !== 'undefined' ? manager : getBool(),
      isOnVacation: getBool(),
      status: Appraisement.appraiserStatusEnum(),
      joinedAt: (new Date()).toISOString(),
      maxOrders: getInt(),
      vendorStatus: Appraisement.vendorStatusEnum(),
      statistics: Appraisement.appraiserStatistic(),
      notes: [Notes.appraiserNote(), Notes.appraiserNote(), Notes.appraiserNote()],
      company: excludeCompany ? {name: faker.company.companyName()} : Appraisement.appraiserCompany(),
      branch: excludeBranch ? null :
              Appraisement.appraiserCompanyBranch(filterParams && filterParams.branchId ? filterParams.branchId : null),
      locations: {
        office: GeneralObjects.location(),
        assignments: GeneralObjects.location()
      },
      phones: {
        primary: {
          number: faker.phone.phoneNumber(),
          ext: 'x100'
        },
        cell: faker.phone.phoneNumber(),
        fax: faker.phone.phoneNumber()
      },
      resume: GeneralObjects.document(),
      eo: {
        carrier: faker.company.companyName(),
        claimAmount: getFloat(),
        aggregateAmount: getFloat(),
        expireAt: new Date().toISOString(),
        insurance: GeneralObjects.document()
      },
      ratingSummary: Appraisement.ratingsSummary(),
      certification: {
        licensedAt: new Date().toISOString(),
        certificate: Appraisement.certificate(),
        isRelocation: getBool(),
        isVa: getBool()
      },
      certificates: [Appraisement.certificate(), Appraisement.certificate()],
      ach: Appraisement.ach(),
      reports: [GeneralObjects.document(), GeneralObjects.document()],
      w9: Appraisement.w9()


      // @todo These were listed previously but are no longer. Need to determine if they're needed
      //isActive: filterParams && filterParams.isActive ? JSON.parse(filterParams.isActive) : getBool(),
      //isPending: filterParams && filterParams.isPending ? JSON.parse(filterParams.isPending) :getBool(),
    });
  },
  /**
   * Appraiser Status Enum
   *
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers#appraiser-status-enum
   * 8/10
   */
  appraiserStatusEnum: function () {
    return _.sample(['pending', 'approved']);
  },
  /**
   * Vendor Status Enum
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers#vendor-status-enum
   * 8/10
   */
  vendorStatusEnum: function () {
    return _.sample(['gold', 'silver', 'bronze', 'trial', 'platinum']);
  },
  /**
   * Standard Appraiser Company Manager Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Managers#standard-appraiser-company-manager-object
   * 8/10
   */
  appraiserCompanyManager: function (id, branchId) {
    return _.assign(GeneralObjects.user(id), {
      company: Appraisement.appraiserCompany(),
      branch: Appraisement.appraiserCompanyBranch(branchId),
      locations: {
        office: GeneralObjects.location(),
        assignments: _.assign(GeneralObjects.location(), {
          distance: _.random(1, 100)
        })
      },
      phones: {
        primary: {
          number: faker.phone.phoneNumber(),
          ext: 'x100'
        },
        cell: faker.phone.phoneNumber(),
        fax: faker.phone.phoneNumber()
      },
      joinedAt: new Date().toISOString()
    });
  },
  /**
   * Standard Appraiser Company Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Companies#standard-appraiser-company-object
   * 8/10
   */
  appraiserCompany: function (id) {
    var company = GeneralObjects.company(id);
    company = _.assign(company, {
      taxId: _.random(1000000, 99999999),
      contact: Appraisement.appraiserCompanyContact()
    });
    // Return from cache
    if (cache.company[id]) {
      return cache.company[id];
    }
    // Add to cache
    cache.company[id] = company;
    return company;
  },
  /**
   * Standard Appraiser Company Contact Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Companies#standard-appraiser-company-contact-object
   * 8/10
   */
  appraiserCompanyContact: function () {
    var contact = GeneralObjects.contact();
    return _.assign(contact, {
      locations: {
        office: GeneralObjects.location(),
        assignments: GeneralObjects.location()
      }
    });
  },
  /**
   * Standard Appraiser Statistic Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/Statistics#standard-appraiser-statistic-object
   * 8/10
   */
  appraiserStatistic: function () {
    return {
      orders: {
        open: {
          percentage: getPercentage(),
          number: _.random(1, 5)
        },
        accepted: {
          percentage: getPercentage(),
          number: _.random(1, 5)
        },
        unaccepted: {
          percentage: getPercentage(),
          number: _.random(1, 5)
        },
        declined: {
          percentage: getPercentage(),
          number: _.random(1, 5)
        },
        onTime: {
          percentage: getPercentage(),
          number: _.random(1, 5)
        },
        late: {
          percentage: getPercentage(),
          number: _.random(1, 5)
        }
      },
      requests: {
        feeIncrease: {
          percentage: getPercentage(),
          number: _.random(1, 5)
        },
        dueDate: {
          percentage: getPercentage(),
          number: _.random(1, 5)
        }
      },
      "activities": {
        "client": _.random(1,5),
        "client_on_report": _.random(1,5)
      },
      "totals": {
        "completed": _.random(1,5),
        "assigned": _.random(1,5),
        "all": _.random(1,5)
      }
    }
  },
  /**
   * Standard Appraiser Company Branch Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Branches#standard-appraiser-company-branch-object
   * 8/10
   */
  appraiserCompanyBranch: function (id) {
    return {
      id: id || _.random(5, 100),
      name: faker.company.companyName(),
      isActive: _.sample([true, false])
    }
  },
  /**
   * Standard Rating Summary Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/Ratings#standard-rating-summary-object
   * 8/16
   */
  ratingsSummary: function (id) {
    return {
      id: id || getInt(),
      appraisal: Appraisement.appraisal(),
      quality: _.random(1, 10),
      service: _.random(1, 10),
      turnAroundTime: _.random(1, 10),
      customerRating: _.random(1, 10),
      cuScore: _.random(1, 10)
    }
  },
  /**
   * Standard Certificate Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/Certificates#standard-certificate-object
   * 8/16
   */
  certificate: function (id, type) {
    return {
      id: id || _.random(1, 100),
      isPrimary: _.sample([true, false]),
      number: _.random(1, 100),
      type: GeneralObjects.certificateTypeEnum(type),
      isFhaApproved: _.sample([true, false]),
      isCommercial: _.sample([true, false]),
      expireAt: new Date().toISOString(),
      document: GeneralObjects.document(),
      coverage: {
        state: GeneralObjects.state(),
        counties: [GeneralObjects.county(1), GeneralObjects.county(2)]
      }
    }
  },
  /**
   * Standard ACH Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement#standard-ach-object
   * 8/15
   */
  ach: function () {
    return {
      bank: faker.company.companyName(),
      account: _.random(10000, 99999),
      routing: _.random(10000, 99999)
    }
  },
  /**
   * Standard Rating Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/Ratings#standard-rating-object
   * 8/10
   */
  ratings: function (id, appraisalId) {
    return {
      "id": id || getInt(),
      "appraisal": Appraisement.appraisal(false, appraisalId),
      "quality": getInt(),
      "service": getInt(),
      "turnAroundTime": getInt(),
      "customerRating": getInt(),
      "cuScore": getFloat()
    };
  },
  /**
   * Standard Appraiser Job Type Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/JobTypes#jobtypes
   * 8/16
   */
  appraiserJobType: function (id) {
    return {
      "id": id || getInt(),
      "amount": getInt(),
      requestedAmount: getFloat(),
      "jobtype": Admin.jobType(id)
    }
  },
  /**
   * Standard Appraiser Settings Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/Settings#settings
   * 8/13
   */
  appraiserSettings: function () {
    return {
      "techFeeEnabled": getBool(),
      "percentageFromClientFees": getFloat(true)
    };
  },
  /**
   * Standard Amc Settings Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Amcs/Settings#standard-amc-settings-object
   * 8/16
   */
  amcSettings: function () {
    return {
      "canSubmitPayment": getBool(),
      "canUseFeeSchedule": getBool()
    };
  },
  /**
   * Standard Appraiser Document Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/Documents#standard-appraiser-document-object
   * 8/13
   */
  appraiserDocument: function (id) {
    var document = GeneralObjects.document(id);
    return _.assign(document, {type: 'Type' + document.id});
  },
  /**
   * Standard Appraiser Company Settings Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Companies/Settings#settings
   */
  appraiserCompanySettings: function () {
    return {
      techFeeEnabled: getBool(),
      percentageFromClientFees: getFloat(true)
    };
  },
  /**
   * Standard AMC License Object
   * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Objects.md#standard-amc-license-object
   */
  amcLicense: function () {
    return {
      "id": getInt(),
      "state": GeneralObjects.state(),
      "number": _.random(10000, 99999),
      "document": _.sample([null, GeneralObjects.document()]),
      "expireAt": new Date().toISOString()
    }
  },
  /**
   * Standard Inspection Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals#standard-inspection-object
   */
  inspection: function (id) {
    return {
      "id": id || _.random(1, 1000),
      "startedAt": new Date().toISOString(),
      "estimatedCompletionDate": _.sample([null, new Date().toISOString()]),
      "completed": getBool(),
      "settings": {
        "reminderSent": getBool()
      }
    };
  },
  /**
   * Standard Appraisal Settings Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Settings#standard-appraisal-settings-object
   */
  appraisalSettings: function () {
    return {
      "unacceptedTimeout": getInt()
    };
  },
  /**
   * Standard Payment Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Payment#standard-payment-object
   */
  payment: function (paid, invoiced, payablePaid) {
    return {
      "receivable": Payment.receivable(paid, invoiced),
      "payable": Payment.payable(payablePaid),
      "amount": {
        "pl": getFloat(),
        "commission": getFloat()
      },
      "fee": {
        "reviewer": getFloat(),
        "supervisor": getFloat(),
        "admin": getFloat(),
        "management": getFloat()
      }
    };
  },
  /**
   * Standard Payment Summary Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Payment#standard-payment-summary-object
   */
  paymentSummary: function () {
    return {
      "receivable": {
        "fee": {
          "total": getFloat(),
          "pageTotal": getFloat()
        },
        "amount": {
          "paid": {
            "total": getFloat(),
            "pageTotal": getFloat()
          },
          "due": {
            "total": getFloat(),
            "pageTotal": getFloat()
          },
          "total": {
            "total": getFloat(),
            "pageTotal": getFloat()
          }
        }
      },
      "payable": {
        "fee": {
          "total": getFloat(),
          "pageTotal": getFloat()
        }
      },
      "amount": {
        "pl": {
          "total": getFloat(),
          "pageTotal": getFloat()
        },
        "commission": {
          "total": getFloat(),
          "pageTotal": getFloat()
        }
      }
    };
  },
  /**
   * Standard Appraiser Aging Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Agings/Appraisers#standard-appraiser-aging-object
   */
  agingAppraiser: function (id) {
    return _.assign(GeneralObjects.aging(id), {appraiser: Appraisement.appraiser()})
  },
  /**
   * Standard Aging Summary Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Agings#standard-aging-summary-object
   */
  agingSummary: function () {
    return {
      "current": {
        "total": getFloat(),
        "pageTotal": getFloat()
      },
      "balance": {
        "total": getFloat(),
        "pageTotal": getFloat()
      },
      "over30": {
        "total": getFloat(),
        "pageTotal": getFloat()
      },
      "over60": {
        "total": getFloat(),
        "pageTotal": getFloat()
      },
      "over90": {
        "total": getFloat(),
        "pageTotal": getFloat()
      }
    };
  },
  /**
   * Standard User W9 Object
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/W9#standard-user-w9-object
   * 8/10
   */
  w9: function () {
    var firstName = faker.name.firstName();
    var lastName = faker.name.lastName();
    return {
      "name": firstName + ' ' + lastName,
      "initials": firstName.charAt(0) + lastName.charAt(0),
      "signature": firstName + ' ' + lastName,
      "signedAt": new Date().toISOString(),
      "business": {
        "name": faker.company.companyName(),
        "type": Appraisement.businessTypeEnum(),
        "customType": _.sample([null, 'custom type']),
        "taxClassification": Appraisement.taxClassificationEnum()
      },
      "exemptions": {
        "payeeCode": _.random(1000, 9999),
        "fatcaCode": _.random(1000, 9999),
        "accountNumber": _.random(1000, 9999),
        "address": _.random(1000, 9999),
        "city": faker.address.city(),
        "state": faker.address.stateAbbr(),
        "zip": _.random(10000, 99999)
      },
      "requester": {
        "name": faker.name.findName(),
        "address": faker.address.streetAddress()
      },
      "taxPayer": {
        "taxId": _.random(10000, 99999),
        "ssn": _.random(100, 999) + '-' + _.random(10, 99) + '-' + _.random(1000, 9999),
        "isCrossOut": getBool()
      }
    };
  },
  /**
   * Business Type Enum
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/W9#business-type-enum
   * 8/10
   *
   * @todo Individual-sole-proprietor is misspelled
   */
  businessTypeEnum: function () {
    return _.sample(['individual-sole-proprietor', 'c-corporation', 's-corporation', 'partnership',
                     'limited-liability-company', 'trust-estate']);
  },
  /**
   * Tax Classification Enum
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/W9#tax-classification-enum
   * 8/10
   */
  taxClassificationEnum: function () {
    return _.sample(['d', 'c', 'p']);
  },
  /**
   * Appraisement Member Enum
   * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/README.md#appraisement-member-enum
   * 8/10
   */
  appraisementMemberEnum: function (type) {
    return type || _.sample(['appraiser', 'company', 'manager']);
  },
  /**
   * Appraisal Status Enum
   * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals#appraisal-status-enum
   * 8/13
   */
  appraisalStatusEnum: function () {
    return _.sample(['new-appraisal', 'assigned', 'accepted', 'declined', 'ready-for-review', 'completed', 'late',
                     'cancelled', 'inspection-scheduled', 'on-hold', 'revision-sent', 'reviewed', 'inspection-complete',
                     'revision-received', 'request-for-bid', 'accepted-with-conditions']);
  }
};

module.exports = Appraisement;
