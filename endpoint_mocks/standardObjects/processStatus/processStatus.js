var g = require('../../generator/generator');
var f = require('faker');
var processStatues = [
  {id: 1, label: 'Ready for Review'},
  {id: 2, label: 'Reviewed'},
  {id: 3, label: 'Revision Received'},
  {id: 4, label: 'Revision Sent'},
  {id: 5, label: 'UW Condition'},
];
var ProcessStatus = {
  // Standard JobType Object
  //@Todo
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/ProcessStatus#standard-process-status-object
  processStatus: function() {
    var id = g.random(0, 4);
    return {
      id: id,
      label: processStatues[id].label,
      fha: g.bool()
    };
  }
};

module.exports = ProcessStatus;
