var faker = require('faker'),
_ = require('lodash');
var Client = require('../standardObjects/client/clientObjects');

var getBool = function () {
  return _.sample([true, false]);
};
/**
 * Client on report
 */
var ClientOnReport = {
  // Standard Client on Report Inheritance Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees#standard-client-on-report-inheritance-object-unfinished
  // 8/4
  clientOnReportInheritance: function () {
    return {
      "checklist": getBool(),
      "instructions": getBool(),
      "appraisersList": {
        "approved": getBool(),
        "doNotUse": getBool()
      },
      "ucdp": getBool(),
      "jobtypesList": getBool(),
      "settings": {
        "reports": {
          "merging": getBool(),
          "emailBorrowerByDefault": getBool()
        },
        "integrations": {

        }
      }
    }
  },
  // Standard Client on Report Object
  // @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees#standard-client-on-report-object
  // 8/7
  clientOnReport: function (id) {
    return {
      company: Client.clientCompany(id),
      inheritance: ClientOnReport.clientOnReportInheritance()
    }
  }
};

module.exports = ClientOnReport;
