'use strict';

var _ = require('lodash');

var filter = {
  asGroupBy : function(rows, groupers) {
    var rowsCp, r, rl, g, gl, name, gName, lastGroupName, gCounter = 0;
    rowsCp = _.cloneDeep(rows || []);
    groupers = groupers || [];
    r = 0;
    rl = rowsCp.length;
    for (r; r < rl; r++) {
      g = 0;
      gl = groupers.length;
      name = [];
      for (g; g < gl; g++) {
        name.push((groupers[g].nameAs || groupers[g].name) + ': ' + rowsCp[r][groupers[g].name]);
      }
      gName = name.join(" | ");
      if(lastGroupName !== gName) gCounter++;
      rowsCp[r]['_groupBy'] = gName;
      rowsCp[r]['_groupByClass'] = 'group_' + gCounter;
      lastGroupName = gName;
    }
    return rowsCp;
  },
  orderBy : function(sorted, sorteree){
    if(!Array.isArray(sorted)|| _.isEmpty(sorted)) return sorted;
    var predicates, orders = [];
    sorteree = _.isPlainObject(sorteree) ? [sorteree] : sorteree;
    predicates = _.map(sorteree, function(item){
      orders.push(!item.reverse);
      return item.predicate;
    });

    return _.sortByOrder(sorted, predicates, orders);
  },
  filter : function(filtered, filteree){
    if(!Array.isArray(filtered) || _.isEmpty(filtered)) return filtered;
    if(!_.isPlainObject(filteree) && typeof filteree !== 'string' ) return filtered;
    filteree = typeof filteree === 'string' ? {$:filteree} : filteree;
    var result;

    function globalFilter(predicate, item){
      var foundAny = false;
      _(item).forEach(function(value){
        if(value === predicate){
          foundAny = true;
          return true;
        }
      });
      return foundAny;
    }

    function filter(filters, item){
      if(_.isEmpty(filters)) return true;
      if(filters.$) return globalFilter(filters.$, item);
      var foundAll = false, index;

      _.forEach(filters,function(value, prop){

        if(!item[prop]) {
          foundAll = false;
          return false;
        }
        index = item[prop].toLowerCase().search(value.toLowerCase());
        foundAll = index > -1;

        if(!foundAll) return false;
      });

      return foundAll;
    }

    result = _.filter(filtered, filter.bind(filtered, filteree));

    return result
  }
};

module.exports = {

  filter: function(type){
    return typeof filter[type] === 'function' ? filter[type] : false;
  },
  // like normal index of but add supports to search within property
  // usage: Utils.indexOf(arr, 5, 'id'), Utils.indexOf(arr, tab, 'id')
  indexOf : function(arr, srch, prop){
    for(var i = 0; i < arr.length; i++){
      if(prop && arr[i][prop] && (arr[i][prop] === srch[prop] || arr[i][prop] === srch)) return i;
      if(arr[i] === srch) return i;
    }
    return -1;
  },

  addGroupersToSorts : function (groupers, sorters) {
    var i, l, tempSorters = [], sorter, savedSorter, index;
    groupers = groupers || [];
    sorters = sorters || [];

    i = 0;
    l = groupers.length;
    for(i; i<l;i++){
      sorter = {name : groupers[i].name, predicate : groupers[i].name, reverse: groupers[i].reverse};
      index = this.indexOf(sorters, groupers[i].name, 'name');
      savedSorter = index > -1 ? sorters[index] : false ;
      tempSorters.push(savedSorter || sorter);
      if(savedSorter) {
        sorters.splice(index,1);
      }
    }

    return [].concat(tempSorters, sorters);
  }
};