var g = require('../generator/generator');


var PropertyTypes = [
  'single-family',
  'multi-family',
  'condominium',
  'duplex',
  'modular-home',
  'vacant-land',
  'industrial',
  'office',
  'raw-land',
  'retail',
  'other'
];


var Enums = {
  propertyTypes: function(random) {
    if (random) {
      return g.pickRandom(PropertyTypes);
    } else {
      return PropertyTypes;
    }
  }
};

module.exports = Enums;
