/**
 * Update payable queue record
 */
module.exports = {
  path: '/v2.0/appraisals/:appraisalId/payment/payable',
  callback: function (req, res) {
    return res.json();
  }
};