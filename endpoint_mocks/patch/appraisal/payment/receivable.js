/**
 * Update appraisal receivable payment
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/appraisals/:appraisalId/payment/receivable',
  callback: function (req, res) {
    return res.json();
  }
};