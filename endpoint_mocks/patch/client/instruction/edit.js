/**
 * Edit instructions
 * @type {{path: RegExp, callback: Function}}
 */
module.exports = {
  path: /instructions/,
  callback: function (req, res) {
    return res.json();
  }
};