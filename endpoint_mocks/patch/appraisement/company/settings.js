// Update appraiser company settings
module.exports = {
  path: '/v2.0/appraisement/companies/:companyId/settings',
  callback: function (req, res) {
    return res.json();
  }
};