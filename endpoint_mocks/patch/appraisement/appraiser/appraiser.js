// Patch an appraiser
module.exports = {
  path: '/v2.0/appraisement/appraisers/:appraiserId/:param?',
  callback: function (req, res) {
    return res.json();
  }
};