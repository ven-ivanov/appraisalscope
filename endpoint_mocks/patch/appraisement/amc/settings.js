// Update AMC settings
module.exports = {
  path: '/v2.0/appraisement/amcs/:amcId/settings',
  callback: function (req, res) {
    return res.json();
  }
};