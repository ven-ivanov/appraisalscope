/**
 * Update note
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/:type/:userId/flag/:id',
  callback: function (req, res) {
    return res.json();
  }
};