/**
 * Update fee schedule
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/fee-schedule',
  callback: function (req, res) {
    return res.json();
  }
};