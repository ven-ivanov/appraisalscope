/**
 * Update appraisal forms
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/appraisal-forms',
  callback: function (req, res) {
    return res.json();
  }
};