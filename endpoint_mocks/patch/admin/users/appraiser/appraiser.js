module.exports = {
  path: '/v2.0/users/appraiser/:appraiserId?',
  callback: function (req, res) {
    return res.json();
  }
};