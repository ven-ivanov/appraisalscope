/**
 * Edit note
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/:type/:userId/note/:noteId',
  callback: function (req, res) {
    return res.json({
      id: req.params.noteId,
      note: req.body.note,
      type: 'Admin',
      sender: 'Editor',
      date: new Date(),
      flag: 1
    });
  }
};