/**
 * Update appraiser
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/coverage/:coverageId',
  callback: function (req, res) {
    return res.json({
      id: req.params.coverageId,
      state: req.body.state,
      primaryState: req.body.primaryState,
      license: req.body.license,
      licenseType: req.body.licenseType,
      fha: req.body.fha,
      commercial: req.body.commercial,
      expirationDate: req.body.expirationDate,
      licenseDoc: req.body.licenseDoc,
      counties: req.body.counties
    });
  }
};