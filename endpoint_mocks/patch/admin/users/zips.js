/**
 * Update selected zips
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/:type/:id/zip',
  callback: function (req, res) {
    return res.json();
  }
};
