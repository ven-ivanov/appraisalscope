/**
 * Enable/disable branch
 */
module.exports = {
  path: '/v2.0/users/client/branch/:branchId',
  callback: function (req, res) {
    return res.json();
  }
};