/**
 * Complete update branch
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches/:branchId',
  callback: function (req, res) {
    return res.json();
  }
};