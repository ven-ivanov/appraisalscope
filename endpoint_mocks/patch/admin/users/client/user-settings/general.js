/**
 * Update general setting
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/client/:userId/general',
  callback: function (req, res) {
    return res.json();
  }
};