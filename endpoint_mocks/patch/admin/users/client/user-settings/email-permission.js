/**
 * Toggle email permission
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/client/:userId/email-permission',
  callback: function (req, res) {
    return res.json();
  }
};