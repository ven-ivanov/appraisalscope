/**
 * Update client on report on click
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: /clients-on-report/,
  callback: function (req, res) {
    return res.json({});
  }
};