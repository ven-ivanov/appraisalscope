/**
 * Patch client employees
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches/:branchId/employees/:employeeId?',
  callback: function (req, res) {
    return res.json();
  }
};