/**
 * Update documents
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/documents',
  callback: function (req, res) {
    return res.json();
  }
};