/**
 * Update acceptance message
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/instructions/acceptance-message',
  callback: function (req, res) {
    return res.json();
  }
};