/**
 * Update fee margin by state
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/settings/fee-margin-by-state/:clientId',
  callback: function (req, res) {
    return res.json();
  }
};