/**
 * Emailed document filename
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/emailed-document-filename',
  callback: function (req, res) {
    return res.json();
  }
};