/**
 * Enable setting
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/enable-setting',
  callback: function (req, res) {
    return res.json();
  }
};