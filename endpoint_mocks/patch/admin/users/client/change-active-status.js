/**
 * Change user active status
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/users/client/change-active-status/',
  callback: function (req, res) {
    return res.json({
      id: req.body.id,
      active: !req.body.active
    });
  }
};