/**
 * Assign job list
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/fee-schedule/assign-job-list/:jobId',
  callback: function (req, res) {
    return res.json();
  }
};