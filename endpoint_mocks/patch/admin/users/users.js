/**
 * Update an individual inspector
 */
module.exports = {
  path: '/v2.0/users/:type/:id',
  callback: function (req, res) {
    return res.json();
  }
};