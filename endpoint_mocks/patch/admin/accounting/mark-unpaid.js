/**
 * Mark as appraiser unpaid
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/accounting/appraiser/mark-unpaid',
  callback: function (req, res) {
    return res.json({});
  }
};