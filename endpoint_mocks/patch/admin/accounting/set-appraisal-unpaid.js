/**
 * Mark a previously paid appraisal as unpaid
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/accounting/set-appraisal-unpaid/:appraisalId',
  callback: function (req, res) {
    res.status(200).send('');
  }
};