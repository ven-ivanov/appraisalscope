/**
 * THIS FILE IS AN EXAMPLE ONLY. IT DOES NOT CORRESPOND TO A REAL API ENDPOINT.
 *
 * Allow users to login via mock API endpoint
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
var determineUser = function(req, res, next) {

  var resBody = {
    patchRequest: 'I am a patch request!'
  };

  return res.json(resBody);
};


module.exports = {
  path: '/v2.0/auth/sign-in',
  callback: determineUser
};
