/**
 * Update user settings
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/Settings/Index.md#updates-some-employee-settings
 */
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches/:branchId/employees/:employeeId/settings',
  callback: function (req, res) {
    return res.json();
  }
};