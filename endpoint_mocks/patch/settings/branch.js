// Update branch settings
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches/:branchId/settings',
  callback: function (req, res) {
    return res.json();
  }
};