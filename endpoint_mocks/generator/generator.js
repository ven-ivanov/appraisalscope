var _ = require('lodash');
var f = require('faker');

var data = {
  alpha: 'abcdefghijklmnopqrstuvwxyz'.split(''),
  firstName: ['John', 'David', 'Jack', 'Aaron', 'Harry', 'Oliver', 'Charlie', 'Amelia', 'Olivia', 'Lily', 'Jessica',
              'Emily', 'Sophie', 'Grace'],
  lastName: ['Barry', 'Bartlett', 'Walker', 'Bass', 'Harris', 'Battle', 'Adams', 'Baxter', 'Beach', 'Johnson', 'Beard',
             'Thompson', 'Beck', 'Ramirez', 'Bell', 'Bender', 'Benjamin', 'Bennett', 'Williams'],
  city: ['London', 'Los Angeles', 'Moscow', 'Beijing', 'Buenos Aires', 'Cairo', 'Istanbul', 'Jakarta', 'Tokyo', 'Seoul', 'Mumbai', 'Shanghai', 'Mexico City'],
  lorem: 'lorem ipsum dolor sit amet consectetur adipiscing elit morbi porttitor nunc neque suspendisse vitae velit eu odio pulvinar facilisis consequat et urna pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas donec ut justo turpis praesent sollicitudin diam convallis aliquet scelerisque ante urna lacinia lacus vel'.split(' '),
  states: ['Alaska', 'Alabama', 'Arkansas',  'American Samoa',  'Arizona',  'California',  'Colorado',  'Connecticut',  'District of Columbia',  'Delaware',  'Florida',  'Georgia',  'Guam',  'Hawaii',  'Iowa',  'Idaho',  'Illinois',  'Indiana',  'Kansas',  'Kentucky',  'Louisiana',  'Massachusetts',  'Maryland',  'Maine',  'Michigan',  'Minnesota',  'Missouri',  'Mississippi',  'Montana',  'North Carolina',  'North Dakota',  'Nebraska',  'New Hampshire',  'New Jersey',  'New Mexico',  'Nevada',  'New York',  'Ohio',  'Oklahoma',  'Oregon',  'Pennsylvania',  'Puerto Rico',  'Rhode Island',  'South Carolina',  'South Dakota',  'Tennessee',  'Texas',  'Utah',  'Virginia',  'Virgin Islands',  'Vermont',  'Washington',  'Wisconsin',  'West Virginia','Wyoming']
};

/**
 * Get random characters
 * @param length
 * @returns {string}
 */
function randomCharacters(length, type) {
  var used;
  var text = "";
  var capitals = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  var lowercase = 'abcdefghijklmnopqrstuvwxyz';
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  // Determine which type of string to return
  switch (type) {
    case 'uppercase':
      used = capitals;
      break;
    case 'lowercase':
      used = lowercase;
      break;
    case 'letters':
      used = capitals.concat(lowercase);
      break;
    default:
      used = possible;
  }
  // Concatenate a string
  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * used.length));
  }

  return text;
}

var pickRandom = function(list) {
  return _.sample(list);
};

var pad = function (amount, width) {
  var padding = "";
  while (padding.length < width - 1 && amount < Math.pow(10, width - padding.length - 1)) {
    padding += "0";
  }
  return padding + amount.toString();
};

var formatDate = function (date) {
  return date? date.toISOString(): new Date().toISOString();
};

var generators = {
  id: function() {
    return _.uniqueId();
  },
  random: _.random,
  // First name
  firstName: function() {
    return pickRandom(data.firstName);
  },
  // Random last name
  lastName: function () {
    return pickRandom(data.lastName)
  },
  // Create a full name
  fullName: function () {
    return this.firstName() + ' ' + this.lastName()
  },
  lorem: {
    short: function() {
      return _.range(6).map(function() { return pickRandom(data.lorem); }).join(' ');
    }
  },
  /**
   * Random string generation
   * @param length
   * @returns {string}
   */
  randomChars: function (length) {
    return randomCharacters(length);
  },
  letters: function (length) {
    return randomCharacters(length, 'letters');
  },
  numbers: function (length) {
    return randomCharacters(length, 'numbers');
  },
  uppercase: function (length) {
    return randomCharacters(length, 'uppercase');
  },
  lowercase: function (length) {
    return randomCharacters(length, 'lowercase');
  },
  /**
   * Generate a random street address
   * @returns {string}
   */
  streetAddress: function () {
    return _.random(1, 9999) + ' ' + this.lastName() + _.sample(['Avenue', 'Street', 'Road', 'Drive']);
  },
  address: {
    city: function() {
      return generators.pickRandom(data.city);
    },
    zipUS: function() {
      return '' + _.random(10000,99999);
    },
    zipNL: function() {
      return _.random(1000,9999) + ' ' + (generators.pickRandom(data.alpha) + generators.pickRandom(data.alpha)).toUpperCase();
    }
  },
  time: {
    quarter: function() {
      return generators.pickRandom([0, 15, 30, 45]);
    },
    hour: function() {
      return _.random(23);
    },
    byQuarter: function() {
      return ('0' + generators.time.hour()).slice(-2) + ':' + ('0' + generators.time.quarter()).slice(-2);
    }
  },
  /**
   * Get random state
   */
  state: function () {
    return _.sample(data.states);
  },
  /**
   * Fake phone number (US)
   */
  phone: function () {
    return _.random(111, 999) + '-' + _.random(111, 999) + '-' + _.random(1111, 9999);
  },
  date: {
    /**
     * Return a random date within a time period.
     *
     * generator.date.between(new Date(2012, 0, 1), new Date())
     *
     * @param start
     * @param end
     * @returns {string}
     */
    between: function (start, end) {
      var date = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
      return date.toISOString();
    },
    past: function () {
      return formatDate(new Date(f.date.past()));
    },
    future: function() {
      return formatDate(new Date(f.date.future()));
    }
  },
  user: {
    type: function() {
      return generators.pickRandom([1, 2, 3, 4, 5])
    }
  },
  bool: function() {
    return generators.pickRandom([true, false]);
  },
  float: function() {
    return _.random() * 1000;
  },
  fileFormat: function() {
    return generators.pickRandom(['pdf', 'doc']);
  },
  randomNumber: function() {
    return Math.floor(Math.random() *  100000);
  },
  randomFloat: function() {
    return +(Math.round(this.randomNumber())  + "e-2");
  },
  pickRandom: function(list) {
    return list[_.random(list.length - 1)];
  }
};

module.exports = generators;
