/**
 * Upload appraisal report
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/report/upload',
  callback: function (req, res) {
    return res.json({
      id: 1000,
      link: 'http://www.analysis.im/uploads/seminar/pdf-sample.pdf',
      name: 'New doc'
    });
  }
};
