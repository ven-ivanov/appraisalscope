var faker = require('faker');
var _ = require('lodash');
/**
 * Create new note
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/note',
  callback: function (req, res) {
    return res.json({
      id: 1000,
      type: 'Admin',
      sender: faker.name.findName(),
      date: new Date(),
      note: req.body.note,
      flag: _.random(0,1)
    });
  }
};