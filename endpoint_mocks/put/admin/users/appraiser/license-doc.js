/**
 * Upload license doc
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/coverage/:coverageId/license-doc/upload',
  callback: function (req, res) {
    return res.json({
      id: req.params.coverageId,
      licenseDoc: 'https://partners.adobe.com/public/developer/en/xml/AdobeXMLFormsSamples.pdf'
    });
  }
};