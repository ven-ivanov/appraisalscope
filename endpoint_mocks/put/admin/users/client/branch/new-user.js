// New user on branch
module.exports = {
  path: '/v2.0/users/client/:clientId/branch/:branchId/new-user',
  callback: function (req, res) {
    return res.json({
      id: 1000,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      phone: req.body.phone,
      username: req.body.username,
      userType: req.body.userType,
      branch: req.params.branchId
    });
  }
};