/**
 * Create a new branch
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/branch',
  callback: function (req, res) {
    return res.json({
      id: 1000,
      branch: req.body.name
    });
  }
};