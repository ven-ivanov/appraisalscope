/**
 * Add list
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/users/client/appraiser-panel/approved-lists/:id',
  callback: function (req, res) {
    return res.json({});
  }
};