/**
 * Create new company
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/users/client/company/new',
  callback: function (req, res) {
    return res.json({id: 1000});
  }
};