var faker = require('faker');
/**
 * Create a new note
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/users/note',
  callback: function (req, res) {
    return res.json({
      id: 1000,
      created: faker.name.findName(),
      date: new Date(),
      content: req.body.content
    });
  }
};