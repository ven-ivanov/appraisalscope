/**
 * Create a new instruction
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/instruction',
  callback: function (req, res) {
    return res.json({
      id: 1000,
      title: req.body.title,
      content: req.body.content,
      tag: req.body.tag
    });
  }
};