var _ = require('lodash');
var faker = require('faker');

// Duplicate checklist
module.exports = {
  path: '/v2.0/users/client/:clientId/checklist/:listId',
  callback: function (req, res) {
    var i, checklist, n;
    // Just create some checklists
    for (i = 0; i <= _.random(1, 25); i = i + 1) {
      n = 1;
      checklist = {
        id: 1000, name: req.body.name, checklist: []
      };
      // Create questions for checklist
      for (n = 1; n < _.random(1, 25); n = n + 1) {
        checklist.checklist.push({
          id: n, question: faker.hacker.phrase()
        });
      }
    }
    return res.json(checklist);
  }
};