module.exports = {
  path: '/v2.0/users/client/:clientId/checklist/new',
  callback: function (req, res) {
    return res.json({
      id: 1000,
      checklist: [
        {
          id: 1,
          question: 'Question'
        }
      ],
      name: req.body.name
    });
  }
};