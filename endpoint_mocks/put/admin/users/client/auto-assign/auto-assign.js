/**
 * Auto assign table data
 */
module.exports = {
  path: '/v2.0/client/companies/:companyId/auto-assign/:param?',
  callback: function (req, res) {
    return res.json({});
  }
};