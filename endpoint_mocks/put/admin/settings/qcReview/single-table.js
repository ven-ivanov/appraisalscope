/**
 * Delete message
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/settings/qcReview/review-tables',  
  callback: function (req, res) {
    return res.json({});
  }
};