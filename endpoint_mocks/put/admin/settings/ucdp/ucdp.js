/**
 * UCDP
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/settings/ucdp/ucdp',
  callback: function (req, res) {
    return res.json({});
  }
};