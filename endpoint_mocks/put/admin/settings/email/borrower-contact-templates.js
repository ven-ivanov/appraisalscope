/**
 * Assign
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/settings/email/borrower-contact-templates',
  callback: function (req, res) {
    return res.json({});
  }
};