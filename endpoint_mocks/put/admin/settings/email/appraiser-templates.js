/**
 * Assign
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/settings/email/appraiser-templates',
  callback: function (req, res) {
    return res.json({});
  }
};