/**
 * amcLicense
 * @type {{path: string, callback: Function}}
 */
module.exports = {
   path: '/v2.0/admin/settings/amcLicense/amcLicense',
  callback: function (req, res) {
    return res.json({});
  }
};