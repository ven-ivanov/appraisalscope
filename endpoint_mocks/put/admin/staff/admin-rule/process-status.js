var ProcessStatus = require('../../../../standardObjects/processStatus/processStatus.js');
var g = require('../../../../generator/generator');
var generateResponse = function(req, res, next) {
  res.body = req.body;
  next();
};
module.exports = {
  path: '/v2.0/admin/admin-rule-profile/:profileId/process-status',
  callback: generateResponse
};
