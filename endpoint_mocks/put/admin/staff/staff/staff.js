var faker = require('faker');
var _ = require('lodash');

/**
 * Retrieve realtors
 */
module.exports = {
  path: '/v2.0/admin/staff',
  callback: function (req, res) {

    //must return Object
    var response_obj = {
      data: req.body
    };
    return res.json(response_obj);
  }
};
