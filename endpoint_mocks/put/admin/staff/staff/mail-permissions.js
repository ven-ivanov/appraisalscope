/**
 * Update user mail permissions for staff
 * @type {exports}
 * @private
 */
var _ = require('lodash'),
  faker = require('faker');

module.exports = {
  path: '/v2.0/admin/staff/email-permissions/',
  callback: function (req, res) {
    return res.json({data: req.body});
  }
};
