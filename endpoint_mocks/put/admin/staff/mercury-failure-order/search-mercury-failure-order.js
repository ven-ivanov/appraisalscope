var _ = require('lodash'),
  faker = require('faker');

/**
 * Search staff mercury failure orders
 * @type {{path: string, callback: Function}}
 * @params
 * :staffId
 * :trackId
 */
module.exports = {
  path: '/v2.0/admin/mercury-failure-orders/search/:trackId',
  callback: function (req, res) {

    var reqParams = [];SS
    // Get parameters
    _.forEach(req.params, function (requestVal) {
      reqParams.push(requestVal);
    });
    // Filter unused parameters
    reqParams = reqParams.filter(function (param) {
      return param;
    });

    var response = [];
    // Make 10
    //rather than using params we just gives subset of orders
    if(reqParams.length === 1) {
      for (var i = 1; i <= 4; i = i + 1) {
        var trackingID = reqParams[0];
        response.push({
          id: i,
          name: faker.name.findName(),
          trackingId: trackingID,
          failureReason: 'mercury order with tracking id' + trackingID+'already exists with ascope file #xxx on Ascopesytem',
          requestDate: new Date()
        });
      }
    }
    //wrapping object
    var response_obj = {
      data: response
    };
    return res.json(response_obj);
  }
};

