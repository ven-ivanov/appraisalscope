var _ = require('lodash'),
  faker = require('faker');
/**
 * Staff realec failure orders
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/realec-failure-orders/',
  callback: function (req, res) {
    var response = [];
    var i;
    // Make 10
    for (i = 1; i <= 10; i = i + 1) {
      response.push({
        id: i,
        name: faker.name.findName(),
        transactionId: 'dsrectest-'+ _.random(100),
        uniqueId: 3,
        failureReason: 'failure reason xxx',
        requestDate: new Date()
      });
    }
    var response_obj = {
      data: response
    };
    return res.json(response_obj);
  }
};
