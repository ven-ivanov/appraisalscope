/**
 * Created by Venelin on 6/24/2015.
 */
var _ = require('lodash'),
  faker = require('faker');

/**
 * User permissions
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/reviewer-rule-profile/',
  callback: function (req, res) {
    var response = [];
    var i;
    // Make 10
    for (i = 1; i <= 7; i = i + 1) {
      var trackingID = _.random(10000);
      response.push({
        id: i,
        name: faker.name.findName()
      });
    }
    //must return Object
    var response_obj = {
      data: response
    };
    return res.json(response_obj);
  }
};
