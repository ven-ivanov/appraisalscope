var Objects = require('../../../../standardObjects/admin/objects.js');
var g = require('../../../../generator/generator');
var generateResponse = function(req, res, next) {
  res.body = req.body;
  next();
};

module.exports = {
  path: '/v2.0/admin/reviewer-rule-profile/:profileId/states',
  callback: generateResponse
};

