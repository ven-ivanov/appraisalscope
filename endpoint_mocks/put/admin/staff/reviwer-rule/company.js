var Client = require('../../../../standardObjects/client/clientObjects');
var g = require('../../../../generator/generator');
var generateResponse = function(req, res, next) {
  res.body = req.body;
  next();
};
module.exports = {
  path: '/v2.0/admin/reviewer-rule-profile/:profileId/companies',
  callback: function (req, res) {
    var i, response = {data: []};
    var total = g.random(1,10);
    for (i = 1; i < total; i = i + 1) {
      response.data.push(Client.clientCompany(i));
    }
    return res.json(response);
  }
};
