'use strict';

module.exports = {
  path: '/v2.0/admin/notifications/:id',

  callback: function (req, res) {
    return res.json({data: req.body});
  }
};