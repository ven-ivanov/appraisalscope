var generateResponse = function(req, res, next) {
    res.body = req.body;
    next();
};

module.exports = {
    path: '/v2.0/admin/job-type/:id?',
    callback: generateResponse
};
