// Update AMC note
module.exports = {
  path: '/v2.0/appraisement/amcs/:amcId/notes/:noteId',
  callback: function (req, res) {
    return res.json();
  }
};