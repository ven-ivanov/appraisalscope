// Update AMC certificate
var Appraisement = require('../../../standardObjects/appraisement/appraisementObjects');
module.exports = {
  path: '/v2.0/appraisement/amcs/:amcId/certificates',
  callback: function (req, res) {
    return res.json();
  }
};