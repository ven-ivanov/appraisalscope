// Update AMC job type
module.exports = {
  path: '/v2.0/appraisement/amcs/:amcId/job-types/:jobTypeId',
  callback: function (req, res) {
    return res.json();
  }
};