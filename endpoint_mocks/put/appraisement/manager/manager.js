// Appraiser manager
module.exports = {
  path: '/v2.0/appraisement/companies/:companyId/branches/:branchId/managers/:managerId',
  callback: function (req, res) {
    return res.json();
  }
};