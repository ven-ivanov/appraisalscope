// Update appraiser company
module.exports = {
  path: '/v2.0/appraisement/companies/:companyId',
  callback: function (req, res) {
    return res.json();
  }
};