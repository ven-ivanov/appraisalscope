var _ = require('lodash'),
faker = require('faker');
var objects = require('../../../standardObjects/clientOnReport');
/**
 * Client on report for companies
 * @type {{path: string, callback: Function}}
 *
 * @todo The client on report object is currently unfinished -- Logan 6/8/15
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Companies/ClientsOnReport/Index.md#get-clientcompaniescompanyidclients-on-report
 */
module.exports = {
  path: '/v2.0/client/companies/:companyId/clients-on-report',
  callback: function (req, res) {
    var response = {data: []}, i;
    // Set default
    response.defaultActive = _.random(0,1);
    // Create clients on report
    for (i = 1; i < _.random(25, 50); i = i + 1) {
      response.data.push(objects.clientOnReport(i));
    }
    return res.json(response);
  }
};