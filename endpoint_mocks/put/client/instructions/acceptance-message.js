/**
 * Update instructions acceptance message
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/Instructions/AcceptanceMessage.md#updates-an-acceptance-message
 */
module.exports = {
  path: /acceptance-message/,
  callback: function (req, res) {
    return res.json();
  }
};