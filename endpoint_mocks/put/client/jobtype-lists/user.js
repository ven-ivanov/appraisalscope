// Assign user job list
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches/:branchId/employees/:employeeId/jobtypes-list/:all?',
  callback: function (req, res) {
    return res.json();
  }
};