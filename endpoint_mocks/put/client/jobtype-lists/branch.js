// Assign job list
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches/:branchId/jobtypes-list',
  callback: function (req, res) {
    return res.json();
  }
};