// Assign company job list
module.exports = {
  path: '/v2.0/client/companies/:companyId/jobtypes-list/:all?',
  callback: function (req, res) {
    return res.json();
  }
};