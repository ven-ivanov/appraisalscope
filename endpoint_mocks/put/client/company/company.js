// Update company
module.exports = {
  path: '/v2.0/client/companies/:companyId',
  callback: function (req, res) {
    return res.json();
  }
};