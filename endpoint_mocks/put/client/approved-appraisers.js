/**
 * Add approved appraisers for admin users - client - user settings
 * @type {{path: string, callback: Function}}
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/AppraisersLists/Approved.md#get-clientcompaniescompanyidbranchesbranchidemployeesemployeeidappraiser-listsapproved
 */
module.exports = {
  path: /appraiser-lists\/approved/,
  callback: function (req, res) {
    return res.json()
  }
};