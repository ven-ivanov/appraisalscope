/**
 * Update note
 * @type {{path: RegExp, callback: Function}}
 * 8/3
 */
module.exports = {
  path: /notes/,
  callback: function (req, res) {
    return res.json();
  }
};