var objects = require('../../../standardObjects/clientOnReport.js');
var _ = require('lodash');

// @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/ClientsOnReport/Index.md#get-clientcompaniescompanyidbranchesbranchidemployeesemployeeidclients-on-report
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches/:branchId/employees/:employeeId/clients-on-report/:id',
  callback: function (req, res) {
    var response = {data: []}, i;
    // Set default
    response.defaultActive = _.random(0,1);
    // Create clients on report
    for (i = 1; i < 4; i = i + 1) {
        response.data.push(objects.clientOnReport(i));
    }

    return res.json(response);
  }
};