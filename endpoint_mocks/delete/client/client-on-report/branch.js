var _ = require('lodash'),
faker = require('faker');
client = require('../../../standardObjects/admin/clients');
/**
 * Client on report for employees
 * @type {{path: string, callback: Function}}
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Branches/ClientsOnReport/Index.md#get-clientcompaniescompanyidbranchesbranchidclients-on-report
 */
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches/:branchId/clients-on-report/:id',
  callback: function (req, res) {
    var response = {data: []}, i;
    // Set default
    response.defaultActive = _.random(0,1);
    // Create clients on report
    for (i = 1; i < _.random(25, 50); i = i + 1) {
      response.data.push(client.clientOnReport(i));
    }
    return res.json(response);
  }
};