// Delete a checklist question
module.exports = {
  path: '/v2.0/checklists/:checklistId',
  callback: function (req, res) {
    return res.json();
  }
};