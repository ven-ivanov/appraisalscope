// Delete a checklist question
module.exports = {
  path: '/v2.0/checklists/:checklistId/questions/:questionId',
  callback: function (req, res) {
    return res.json();
  }
};