/**
 * Add do not use appraisers list for admin users - client - user settings
 * @type {{path: string, callback: Function}}
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/AppraisersLists/DoNotUse.md#get-clientcompaniescompanyidbranchesbranchidemployeesemployeeidappraiser-listsdo-not-use
 */
module.exports = {
  path: /appraiser-lists\/do-not-use/,
  callback: function (req, res) {
    return res.json()
  }
};