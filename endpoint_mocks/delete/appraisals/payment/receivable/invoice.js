/**
 * Delete selected invoices
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/appraisals/payment/receivable/invoice',
  callback: function (req, res) {
    return res.json();
  }
};