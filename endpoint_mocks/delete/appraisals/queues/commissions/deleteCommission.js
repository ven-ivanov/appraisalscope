/**
 * Delete record from commission queue
 */
module.exports = {
  path: '/v2.0/appraisals/queues/commissions/:appraisalId',
  callback: function (req, res) {
    return res.json();
  }
};