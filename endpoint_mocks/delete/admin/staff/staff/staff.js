var faker = require('faker');
var _ = require('lodash');
var Objects = require('../../../../standardObjects/admin/objects.js');
var g = require('../../../../generator/generator');
/**
 * Delete Staff
 */
var generateResponse = function(req, res, next) {
  res.body = req.body;
  next();
};
module.exports = {
  path: '/v2.0/admin/staff/:staffId',
  callback: generateResponse
};
