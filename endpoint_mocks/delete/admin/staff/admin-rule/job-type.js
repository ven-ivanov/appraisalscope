var JobType = require('../../../../standardObjects/jobTypes/jobType.js');
var g = require('../../../../generator/generator');
var generateResponse = function(req, res, next) {
  res.body = req.body;
  next();
};
module.exports = {
  path: '/v2.0/admin/admin-rule-profile/:profileId/job-type/:jobTypeId',
  callback: generateResponse
};

