var _ = require('lodash'),
  faker = require('faker');

/**
 * User permissions
 * @type {{path: string, callback: Function}}
 */
var generateResponse = function(req, res, next) {
  res.body = req.body;
  next();
};

module.exports = {
  path: '/v2.0/admin/admin-rule-profile/:profileId',
  callback: generateResponse
};




