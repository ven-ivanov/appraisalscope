var Client = require('../../../../standardObjects/client/clientObjects');
var g = require('../../../../generator/generator');

var generateResponse = function(req, res, next) {
  res.body = req.body;
  next();
};
module.exports = {
  path: '/v2.0/admin/reviewer-rule-profile/:profileId/companies/:companyId',
  callback: generateResponse
};

