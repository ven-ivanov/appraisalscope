/**
 * Created by Venelin on 6/24/2015.
 */
var _ = require('lodash'),
  faker = require('faker');

var generateResponse = function(req, res, next) {
  res.body = req.body;
  next();
};
/**
 * User permissions
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/reviewer-rule-profile/:profileId',
  callback: generateResponse
};
