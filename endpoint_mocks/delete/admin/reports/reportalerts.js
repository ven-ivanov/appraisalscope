
module.exports = {
  path: '/v2.0/admin/reports/:id/reportalerts/:alertId',
  callback: function (req, res) {
    return res.json({data : req.params.alertId});
  }
};

