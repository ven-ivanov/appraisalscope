/**
 * Created by cssdna on 07/24/15.
 */
module.exports = {
  path: '/v2.0/admin/reports/delete/',
  callback: function (req, res) {
    // Create array ids need to remove and return it
    var removeIDs = ['99', '32', '104', '105', '111'];
    return res.json({data :removeIDs});
  }
};
