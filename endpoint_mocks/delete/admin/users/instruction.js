/**
 * Delete instruction
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/instruction',
  callback: function (req, res) {
    return res.json();
  }
};