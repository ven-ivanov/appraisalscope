/**
 * Delete coverage
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/coverage/:coverageId',
  callback: function (req, res) {
    return res.json();
  }
};