/**
 * Delete document
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/doc/:docId',
  callback: function (req, res) {
    return res.json();
  }
};