/**
 * Delete branch
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/client/branch/:branchId',
  callback: function (req, res) {
    return res.json();
  }
};