/**
 * Delete user or complany
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/users/:type/:id',
  callback: function (req, res) {
    return res.json({});
  }
};