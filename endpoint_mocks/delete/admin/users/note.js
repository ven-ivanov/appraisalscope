/**
 * Delete note
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/note/:noteId',
  callback: function (req, res) {
    return res.json();
  }
};