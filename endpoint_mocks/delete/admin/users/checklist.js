module.exports = {
  path: '/v2.0/users/client/:clientId/checklist/:id/:questionId?',
  callback: function (req, res) {
    return res.json();
  }
};