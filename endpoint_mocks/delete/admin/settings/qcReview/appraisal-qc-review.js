/**
 * Delete message
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/settings/qcReview/appraisal-qc-review',  
  callback: function (req, res) {
    return res.json({});
  }
};