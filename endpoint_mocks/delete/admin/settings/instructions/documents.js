/**
 * Delete document
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/settings/instructions/documents',  
  callback: function (req, res) {
    return res.json({});
  }
};