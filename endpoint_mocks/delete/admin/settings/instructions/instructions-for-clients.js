/**
 * Delete instruction
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/settings/instructions/instructions-for-clients',  
  callback: function (req, res) {
    return res.json({});
  }
};