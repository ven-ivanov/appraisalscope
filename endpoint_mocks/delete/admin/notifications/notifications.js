'use strict';
/**
 * Delete notification
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/notifications/:id?',
  callback: function (req, res) {
    return res.json({});
  }
};