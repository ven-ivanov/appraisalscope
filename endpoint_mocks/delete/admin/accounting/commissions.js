/**
 * Delete commissions
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/accounting/commissions/:appraisalIds',
  callback: function (req, res) {
    return res.json({});
  }
};