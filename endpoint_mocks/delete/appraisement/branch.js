// Delete appraiser company branch
module.exports = {
  path: '/v2.0/appraisement/companies/:companyId/branches/:branchId',
  callback: function (req, res) {
    return res.json();
  }
};