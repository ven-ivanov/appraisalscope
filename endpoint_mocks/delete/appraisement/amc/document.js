// Delete AMC document
module.exports = {
  path: '/v2.0/appraisement/amcs/:amcId/documents/:documentId',
  callback: function (req, res) {
    return res.json();
  }
};