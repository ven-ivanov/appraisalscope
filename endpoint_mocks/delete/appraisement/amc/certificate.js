// Delete AMC certificate
module.exports = {
  path: '/v2.0/appraisement/amcs/:amcId/certificates/:certificateId',
  callback: function (req, res) {
    return res.json();
  }
};