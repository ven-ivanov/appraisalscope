module.exports = {
  path: '/v2.0/appraisement/appraisers/:appraiserId/documents/:documentId',
  callback: function (req, res) {
    return res.json();
  }
};