var _ = require('lodash'),
faker = require('faker');
var objects = require('../../../standardObjects/clientOnReport');
var CompanyCache = require('../companyCache');
/**
 * Client on report for companies
 * @type {{path: string, callback: Function}}
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Companies/ClientsOnReport/Index.md#get-clientcompaniescompanyidclients-on-report
 * 8/4
 */
module.exports = {
  path: /clients-on-report/,
  callback: function (req, res) {
    var response = {data: []}, i;
    // Get companies from cache, if we have one available
    if (CompanyCache.companyIds.length) {
      var length = CompanyCache.companyIds.length;
      // Create clients on report
      for (i = 0; i < length - 1; i = i + 1) {
        response.data.push(objects.clientOnReport(CompanyCache.companyIds[i]));
      }
      return res.json(response);
    }
    // Set default
    response.defaultActive = _.random(0,1);
    // Create clients on report
    for (i = 1; i < _.random(25, 50); i = i + 1) {
      response.data.push(objects.clientOnReport(i));
    }
    return res.json(response);
  }
};