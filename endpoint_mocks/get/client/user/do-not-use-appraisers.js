var appraisersList = require('../../../standardObjects/admin/appraisersList/objects');
/**
 * Get approved appraisers for admin users - client - user settings
 * @type {{path: string, callback: Function}}
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/AppraiserLists/DoNotUse.md#get-clientcompaniescompanyidbranchesbranchidemployeesemployeeidappraiser-listsdo-not-use
 * 8/4
 */
module.exports = {
  path: /(client|appraisers).*?appraiser-lists\/do-not-use/,
  callback: function (req, res) {
    var response = {data: []}, i;

    // Create a few
    for (i = 1; i < 3; i = i + 1) {
        response.data.push(appraisersList.appraisersList(i, 'do-not-use'));
    }

    return res.json(response);
  }
};