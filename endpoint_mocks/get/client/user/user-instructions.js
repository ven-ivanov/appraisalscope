var clientObjects = require('../../../standardObjects/client/clientObjects');
/**
 * User documents
 * @type {{path: string, callback: Function}}
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/Instructions/Index.md
 */
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches/:branchId/employees/:employeeId/instructions',
  callback: function (req, res) {
    var response = {data: []}, i;
    // Create 3 documents
    for (i = 1; i < 4; i = i + 1) {
      response.data.push(clientObjects.instruction(i));
    }
    return res.json(response);
  }
};