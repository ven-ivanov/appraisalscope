var clientObjects = require('../../../standardObjects/client/clientObjects');
/**
 * User documents
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches/:branchId/employees/:employeeId/instructions/documents',
  callback: function (req, res) {
    var response = {data: []}, i;
    // Create 3 documents
    for (i = 1; i < 4; i = i + 1) {
      response.data.push(clientObjects.instructionDocument(i));
    }
    return res.json(response);
  }
};