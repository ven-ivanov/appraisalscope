/**
 * Client employee checklist
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/Checklist/Index.md
 */
var objects = require('../../../standardObjects/admin/objects.js');
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches/:branchId/employees/:employeeId/checklist',
  callback: function (req, res) {
    var response = {data: []}, i;
    for (i = 1; i < 4; i = i + 1) {
      response.data.push(objects.checklist(i));
    }

    return res.json(response);
  }
};