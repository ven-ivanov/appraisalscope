var objects = require('../../../standardObjects/admin/objects');
var Client = require('../../../standardObjects/client/clientObjects');
/**
 * Get job types list
 * @type {{path: string, callback: Function}}
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/JobTypeList/Index.md#get-clientcompaniescompanyidbranchesbranchidemployeesemployeeidjob-type-list-unimplemented
 */
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches/:branchId/employees/:employeeId/jobtypes-list',
  callback: function (req, res) {
    var response = {data: []}, i;

    // Create a few
    for (i = 1; i < 4; i = i + 1) {
        response.data.push(Client.jobTypeList(i));
    }

    return res.json(response);
  }
};