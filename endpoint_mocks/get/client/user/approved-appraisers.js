var appraisersList = require('../../../standardObjects/admin/appraisersList/objects.js');
/**
 * Get approved appraisers for admin users - client - user settings
 * @type {{path: string, callback: Function}}
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/AppraiserLists/Approved.md#get-clientcompaniescompanyidbranchesbranchidemployeesemployeeidappraiser-listsapproved-unimplemented
 * 8/4
 */
module.exports = {
  path: /(client|appraisers).*?appraiser-lists\/approved/,
  callback: function (req, res) {
    var response = {data: []}, i;

    // Create a few
    for (i = 1; i < 3; i = i + 1) {
        response.data.push(appraisersList.appraisersList(i, 'approved'));
    }

    return res.json(response);
  }
};