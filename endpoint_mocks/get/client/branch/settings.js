// Branch settings
var Settings = require('../../../standardObjects/client/branch/branch');
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches/:branchId/settings',
  callback: function (req, res) {
    return res.json(Settings.branchSettings());
  }
};