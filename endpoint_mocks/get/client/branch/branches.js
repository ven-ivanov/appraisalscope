var ClientObjects = require('../../../standardObjects/client/clientObjects');
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches',
  callback: function (req, res) {
    var response = {data: []}, i;
    for (i = 1; i < 5; i = i + 1) {
        response.data.push(ClientObjects.branch(i, null, true));
    }

    return res.json(response);
  }
};