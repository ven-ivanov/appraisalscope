// User instructions
var Objects = require('../../../standardObjects/client/clientObjects');
module.exports = {
  path: /instructions$/,
  callback: function (req, res) {
    var i, response = {data: []};

    for (i = 1; i < 10; i = i + 1) {
        response.data.push(Objects.instruction(i));
    }

    return res.json(response);
  }
};