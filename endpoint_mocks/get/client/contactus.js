module.exports = {
  path: '/v2.0/company/contactus',
  template: {
		'data': [
			{
				'id'    : 1,
 				'post'  : 'Primary Account Manager',
				'phone' : '310-453-9883',
				'mobile': '310-453-9289',
				'email' : 'fake@mail.com',
				'fullName' : 'Jordan Belford'
			},
			{
				'id'    : 2,
 				'post'  : 'Operations Manager',
				'phone' : '315-453-9883',
				'mobile': '315-453-9289',
				'email' : 'otherfake@mail.com',
				'fullName' : 'John Smith'
			}	
		]
	}

};