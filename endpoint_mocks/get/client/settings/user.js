var Employee = require('../../../standardObjects/client/employee/objects');
/**
 * Employee settings
 */
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches/:branchId/employees/:employeeId/settings',
  callback: function (req, res) {
    return res.json(Employee.employeeSettings());
  }
};