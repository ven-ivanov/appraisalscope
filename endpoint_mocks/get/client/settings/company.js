var company = require('../../../standardObjects/client/companies/objects');
module.exports = {
  path: '/v2.0/client/companies/:companyId/settings',
  callback: function (req, res) {
    return res.json(company.settings());
  }
};