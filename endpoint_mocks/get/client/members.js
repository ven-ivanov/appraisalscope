var clientObjects = require('../../standardObjects/client/clientObjects');
var faker = require('faker');
var _ = require('lodash');
var CompanyCache = require('./companyCache');
var i, response = {data: []};

// Create a company and store its ID
var createCompany = function (id) {
  var clientCompany;
  // Create an initial company
  clientCompany = clientObjects.clientCompany(id || 1);
  // Store company
  CompanyCache.companyIds.push(clientCompany.id);
  // Push onto response
  response.data.push(_.assign({memberType: 'company'}, clientCompany));
};

/**
 * Create members
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/client/members',
  callback: function (req, res) {
    // Don't add on to the array again
    if (response.data.length) {
      return res.json(response);
    }
    // Create an initial company
    createCompany(1);
    // Create an initial employee
    response.data.push(_.assign({memberType: 'employee'}, clientObjects.employee(2, 1)));
    // Iterate and create users and companies
    for (i = 3; i < 15; i = i + 1) {
      // Make mostly users
      if (_.random(0, 1, true) < 0.5) {
        // Push new user onto response
        response.data.push(_.assign({memberType: 'employee'}, clientObjects.employee(i, _.sample(CompanyCache.companyIds))));
      } else {
        // Create a company
        createCompany(i);
      }
    }

    return res.json(response);
  }
};