var adminAppraisalObjects = require('../../../standardObjects/admin/appraisals');
var responseFormat = require('../../../standardObjects/responseFormat');

module.exports = {
  path: '/v2.0/client/appraisals',
  callback: function (req, res) {

    function createAppraisal() {
      return adminAppraisalObjects.appraisal(req.query.include);
    }

    return responseFormat.pagination(req, res, createAppraisal);
  }
};
