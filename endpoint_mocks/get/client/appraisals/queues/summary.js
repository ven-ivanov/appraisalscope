var g = require('../../../../generator/generator');

module.exports = {
  path: '/v2.0/client/appraisals/queues/summary',
  callback: function(req, res) {

    return res.json({
      'new':            g.random(99999),
      accepted:         g.random(99999),
      revision:         g.random(99999),
      pending:          g.random(99999),
      completed:        g.random(99999)
    });
  }
};
