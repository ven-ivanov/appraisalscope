/**
 * Retrieve assigned employee checklist
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/Checklist/Index.md
 */
var objects = require('../../../standardObjects/admin/objects');
module.exports = {
  path: '/v2.0/client/companies/:companyId/checklist',
  callback: function (req, res) {
    return res.json(objects.checklist(1));
  }
};