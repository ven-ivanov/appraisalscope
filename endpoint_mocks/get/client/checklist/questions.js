var objects = require('../../../standardObjects/admin/objects');
module.exports = {
  path: '/v2.0/checklists/:checklistId/questions',
  callback: function (req, res) {
    var i, response = {data: []};
    for (i = 1; i < 10; i = i + 1) {
      response.data.push(objects.checklistQuestion(i));
    }

    return res.json(response);
  }
};