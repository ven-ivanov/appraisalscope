/**
 * Retrieve all checklists
 */
var objects = require('../../../standardObjects/admin/objects');
module.exports = {
  path: '/v2.0/checklists',
  callback: function (req, res) {
    var response = {data: []}, i;
    for (i = 1; i < 4; i = i + 1) {
      response.data.push(objects.checklist(i));
    }

    return res.json(response);
  }
};