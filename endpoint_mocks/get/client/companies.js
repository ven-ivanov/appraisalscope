var Client = require('../../standardObjects/client/clientObjects');

module.exports = {
  path: '/v2.0/client/companies',
  callback: function (req, res) {
    var i, response = {data: []};

    for (i = 1; i < 5; i = i + 1) {
        response.data.push(Client.clientCompany(i));
    }

    return res.json(response);
  }
};