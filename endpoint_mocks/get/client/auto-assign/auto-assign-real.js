var _ = require('lodash');
var autoAssign = require('../../../standardObjects/client/clientObjects.js');
/**
 * Top level dropdowns on auto assign view
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: /auto-assign/,
  callback: function (req, res) {
    return res.json(autoAssign.autoAssign());
  }
};