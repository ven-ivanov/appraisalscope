var General = require('../../standardObjects/generalObjects');
/**
 * Company scorecard
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/client/companies/:companyId/scorecard',
  callback: function (req, res) {
    var response = {data: []}, i;

    for (i = 1; i < 10; i = i + 1) {
      response.data.push(General.score(i + '/1/2015'));
    }

    return res.json(response);
  }
};