module.exports = {
  path: '/v2.0/clients/profile/:userId',
  template: {
		'phone' : '555-729-4685',
		'email' : 'some@email.com',
		'firstName' : 'Jordan',
		'lastName'  : 'Standford',
		'phoneExt'  : '265',
		'companyName' : 'Straten Ocmand',
		'fax' : '777-888-9994',
		'joinDate':'01/01/2015',
		'estimatedOrderAmount' : 18,
		'accountTypeId' : 2,
		'addressLine1' : 'Some street 55',
		'addressLine2' : 'Some other street 54',
		'city' : 'New Orleans',
		'state' : 'AL',
		'postalCode' : '15226',
		'username'   : 'client',
		'password'   : 'password'
	}
};