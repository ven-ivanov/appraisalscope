// Company instructions
var Objects = require('../../../standardObjects/client/clientObjects');
module.exports = {
  path: '/v2.0/client/companies/:companyId/jobtypes-list/:all?',
  callback: function (req, res) {
    var i, response = {data: []};
    // Return all
    if (req.params.all) {
      for (i = 1; i < 10; i = i + 1) {
        response.data.push(Objects.jobTypeList(i));
      }
      // Remove first, since it'll be default
      response.data.shift();
      return res.json(response);
    }
    // Assigned jobtype list
    return res.json(Objects.jobTypeList(1));
  }
};