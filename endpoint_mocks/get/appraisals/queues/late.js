// This endpoint is the same as /admin/appraisals so just use that module
var appraisal = require('../appraisals');

module.exports = {
  path: '/v2.0/appraisals/queues/late',
  callback: appraisal.callback
};
