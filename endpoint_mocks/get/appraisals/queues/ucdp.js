var appraisalObjects       = require('../../../standardObjects/appraisals/appraisal');
var ucdpSubmissionsObjects = require('../../../standardObjects/appraisals/ucdpSubmissions');
var responseFormat         = require('../../../standardObjects/responseFormat');

module.exports = {
  path: '/v2.0/appraisals/queues/ucdp',
  callback: function(req, res) {

    function createBid() {
      // Include the assignment object with each appraisal no matter the input
      var appraisal = appraisalObjects.appraisal(req.query.include);

      appraisal.ucdpSubmission = ucdpSubmissionsObjects.ucdpSubmission();

      return appraisal;
    }

    return responseFormat.pagination(req, res, createBid);
  }
};
