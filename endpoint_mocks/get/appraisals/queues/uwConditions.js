var appraisalObjects = require('../../../standardObjects/appraisals/appraisal');
var revisionObjects  = require('../../../standardObjects/appraisals/revisions');
var responseFormat   = require('../../../standardObjects/responseFormat');

module.exports = {
  path: '/v2.0/appraisals/queues/uw-conditions',
  callback: function(req, res) {

    function createUwCondition() {
      // Include the assignment object with each appraisal no matter the input
      var appraisal = appraisalObjects.appraisal(req.query.include);

      appraisal.revision = revisionObjects.revision();

      return appraisal;
    }

    return responseFormat.pagination(req, res, createUwCondition);
  }
};
