// This endpoint is the same as /admin/appraisals/queues/revision-received so just use that module
var revisionReceived = require('./revisionReceived');

module.exports = {
  path: '/v2.0/appraisals/queues/revision-sent',
  callback: revisionReceived.callback
};
