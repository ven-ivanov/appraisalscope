var Appraisement = require('../../../standardObjects/appraisement/appraisementObjects');
var General = require('../../../standardObjects/generalObjects');
/**
 * Pending ACH
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Queues/PendingAch.md#get-appraisalsqueuespending-ach-unimplemented
 */
module.exports = {
  path: '/v2.0/appraisals/queues/pending-ach',
  callback: function (req, res) {
    var response = {data: [], meta: {}}, i, page = 1, perPage = 10, totalPages = 20;
    // Per page
    if (typeof req.query.perPage !== 'undefined') {
      perPage = parseInt(req.query.perPage);
    }
    // Page
    if (typeof req.query.page !== 'undefined') {
      page = parseInt(req.query.page);
    }

    // Create the right number
    for (i = ((page - 1) * perPage) + 1; i <= page * perPage; i = i + 1) {
      response.data.push(Appraisement.appraisal({}, i, {payablePaid: false}));
    }

    response.meta.pagination = General.pagination(page, perPage, totalPages);
    response.meta.summary = Appraisement.paymentSummary();

    return res.json(response);
  }
};