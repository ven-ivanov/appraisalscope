var appraisalObjects = require('../../../standardObjects/appraisals/appraisal');
var bidsObject       = require('../../../standardObjects/appraisals/bids');
var responseFormat   = require('../../../standardObjects/responseFormat');


module.exports = {
  path: '/v2.0/appraisals/queues/bids',
  callback: function(req, res) {

    function createBid() {
      var appraisal = appraisalObjects.appraisal(req.query.include);

      appraisal.bidSummary = bidsObject.bidSummary();

      return appraisal;
    }

    return responseFormat.pagination(req, res, createBid);
  }
};
