/*
 * https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Queues/Summary.md
 */

var g = require('../../../generator/generator');

module.exports = {
  path: '/v2.0/appraisals/queues/summary',
  callback: function(req, res) {
    var i = 0;
    while(i < 1000000000) {
      i ++;
    }

    return res.json({
      all:              g.random(99999),
      pending:          g.random(99999),
      completed:        g.random(99999),
      canceled:         g.random(99999),
      due:              g.random(99999),
      open:             g.random(99999),
      'new':            g.random(99999),
      assigned:         g.random(99999),
      unaccepted:       g.random(99999),
      accepted:         g.random(99999),
      scheduled:        g.random(99999),
      late:             g.random(99999),
      onHold:           g.random(99999),
      messages:         g.random(99999),
      updates:          g.random(99999),
      ucdp:             g.random(99999),
      bids:             g.random(99999),
      uwConditions:     g.random(99999),
      revisionSent:     g.random(99999),
      revisionReceived: g.random(99999),
      readyForReview:   g.random(99999),
      reviewed:         g.random(99999)
    });
  }
};
