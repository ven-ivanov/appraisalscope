var appraisalObjects = require('../../../standardObjects/appraisals/appraisal');
var messagesObjects  = require('../../../standardObjects/appraisals/messages');
var responseFormat   = require('../../../standardObjects/responseFormat');

module.exports = {
  path: '/v2.0/appraisals/queues/messages',
  callback: function(req, res) {

    function createMessage() {
      return {
        appraisal: appraisalObjects.appraisal(req.query.include),
        message: messagesObjects.message()
      };
    }

    return responseFormat.pagination(req, res, createMessage);
  }
};
