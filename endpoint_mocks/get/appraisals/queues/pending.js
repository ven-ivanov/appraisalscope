var Appraisement = require('../../../standardObjects/appraisement/appraisementObjects');
var General = require('../../../standardObjects/generalObjects');
/**
 * Pending appraisal queue
 *
 * @todo This is just a guess. This queue doesn't exist yet
 */
module.exports = {
  path: '/v2.0/appraisals/queues/pending',
  callback: function (req, res) {
    var response = {data: [], meta: {}}, i, page = 1, perPage = 10, totalPages = 20;
    // Per page
    if (typeof req.query.perPage !== 'undefined') {
      perPage = parseInt(req.query.perPage);
    }
    // Page
    if (typeof req.query.page !== 'undefined') {
      page = parseInt(req.query.page);
    }

    // Create the right number
    for (i = ((page - 1) * perPage) + 1; i <= page * perPage; i = i + 1) {
      response.data.push(Appraisement.appraisal({}, i));
    }

    response.meta.pagination = General.pagination(page, perPage, totalPages);
    response.meta.summary = Appraisement.paymentSummary();

    return res.json(response);
  }
};