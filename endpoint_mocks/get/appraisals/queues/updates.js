var appraisalObjects = require('../../../standardObjects/appraisals/appraisal');
var udpdateObjects   = require('../../../standardObjects/appraisals/updates');
var responseFormat   = require('../../../standardObjects/responseFormat');

module.exports = {
  path: '/v2.0/appraisals/queues/updates',
  callback: function(req, res) {

    function createBid() {
      return {
        appraisal: appraisalObjects.appraisal(req.query.include),
        update: udpdateObjects.update()
      };
    }

    return responseFormat.pagination(req, res, createBid);
  }
};
