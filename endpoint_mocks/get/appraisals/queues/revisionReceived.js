var appraisalObjects  = require('../../../standardObjects/appraisals/appraisal');
var assignmentObjects = require('../../../standardObjects/appraisals/assignment');
var responseFormat    = require('../../../standardObjects/responseFormat');

module.exports = {
  path: '/v2.0/appraisals/queues/revision-received',
  callback: function(req, res) {

    function createBid() {
      // Include the assignment object with each appraisal no matter the input
      var appraisal = appraisalObjects.appraisal(req.query.include);

      appraisal.assignment = assignmentObjects.assignment(true);

      return appraisal;
    }

    return responseFormat.pagination(req, res, createBid);
  }
};
