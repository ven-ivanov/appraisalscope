/**
 * Check if appraisals have already been invoiced
 *
 * @todo This is a fake endpoint
 * @link https://github.com/ascope/manuals/issues/273
 */
module.exports = {
  path: '/v2.0/appraisals/:appraisalIds/payment/receivable/invoice/check',
  callback: function (req, res) {
    var appraisalIds = req.params.appraisalIds.split(',');
    var invoiced = [], notInvoiced = [];
    if (appraisalIds.indexOf('1') !== -1) {
      invoiced.push(1);
    }
    appraisalIds.forEach(function (id) {
      if (id !== '1') {
        notInvoiced.push(id);
      }
    });
    return res.json({invoiced: invoiced, notInvoiced: notInvoiced});
  }
};