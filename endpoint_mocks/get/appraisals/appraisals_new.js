/**
 * This is simply here so I don't disrupt the existing endpoint (appraisals.js), which has an incorrect path
 * @type {Appraisals|exports}
 */
var appraisalObjects = require('../../standardObjects/appraisals/appraisal');
var responseFormat   = require('../../standardObjects/responseFormat');
var _ = require('lodash');

module.exports = {
  path: '/v2.0/appraisals',
  callback: function (req, res) {
    var filters = [];
    // Get filters
    if (_.isPlainObject(req.query)) {
      Object.keys(req.query).forEach(function (query) {
        filters.push(query);
      });
    }

    function createAppraisal() {
      return appraisalObjects.appraisal(req.query.include, filters);
    }

    return responseFormat.pagination(req, res, createAppraisal);
  }
};
