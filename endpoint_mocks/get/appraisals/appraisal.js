var appraisalObjects = require('../../standardObjects/appraisals/appraisal');
var responseFormat   = require('../../standardObjects/responseFormat');

module.exports = {
  path: '/v2.0/appraisals/:id',
  callback: function (req, res) {
    var object = appraisalObjects.appraisal(req.query.include);

    object.id = req.params.id;

    return res.json(object);
  }
};
