var g = require('../../generator/generator');
var f = require('faker');

module.exports = {
  path: '/v2.0/admin/appraisals/options',
  callback: function (req, res) {

    function generateOptions(itemGenerator) {
      var count = g.random(20);
      var data  = [];

      for (var i = 0; i < count; i ++) {
        data.push({
          id: g.random(99999),
          value: itemGenerator()
        });
      }

      return data;
    }

    function userName() {
      return f.name.firstName() +' '+ f.name.lastName();
    }

    function randomWords() {
      return f.lorem.words().join(' ');
    }

    function companyName() {
      return f.company.companyName();
    }

    return res.json({
      staff: generateOptions(userName),
      reviewer: generateOptions(userName),
      lastUpdate: generateOptions(randomWords),
      client: generateOptions(companyName),
      clientOnReport: generateOptions(companyName),
      jobType: generateOptions(randomWords)
    });
  }
};
