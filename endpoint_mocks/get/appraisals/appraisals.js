var appraisalObjects = require('../../standardObjects/appraisals/appraisal');
var responseFormat   = require('../../standardObjects/responseFormat');

module.exports = {
  path: '/v2.0/appraisals',
  callback: function (req, res) {

    function createAppraisal() {
      return appraisalObjects.appraisal(req.query.include);
    }

    return responseFormat.pagination(req, res, createAppraisal);
  }
};
