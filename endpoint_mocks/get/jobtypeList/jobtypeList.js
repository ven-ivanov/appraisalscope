// Retrieve all job type lists
// @todo This can't be right. It currently doesn't return the actual job types with the list
// @link https://github.com/ascope/manuals/issues/203
var Client = require('../../standardObjects/client/clientObjects');
module.exports = {
  path: '/v2.0/job-type-lists',
  callback: function (req, res) {
    var i, response = {data: []};

    for (i = 1; i < 10; i = i + 1) {
        response.data.push(Client.jobTypeList(i));
    }

    return res.json(response);
  }
};