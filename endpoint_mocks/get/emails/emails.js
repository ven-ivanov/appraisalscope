var Emails = require('../../standardObjects/emails/email');
module.exports = {
  path: '/v2.0/emails',
  callback: function (req, res) {
    var i, response = {data: []};

    for (i = 1; i < 20; i = i + 1) {
        response.data.push(Emails.email(i));
    }

    return res.json(response);
  }
};