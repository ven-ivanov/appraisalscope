var Appraisement = require('../../standardObjects/appraisement/appraisementObjects');

// Appraiser Company
// @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Companies/Index.md#gets-an-appraisers-company
module.exports = {
  path: '/v2.0/appraisement/companies/:id',
  callback: function (req, res) {

    return res.json(Appraisement.appraiserCompany(req.params.id));
  }
};