var AppraisementObjects = require('../../../standardObjects/appraisement/appraisementObjects');
var cachedManagers = [];
module.exports = {
  path: '/v2.0/appraisement/companies/:companyId/branches/:branchId/managers',
  callback: function (req, res) {
    var response = {data: []}, i;
    var manager;
    var startingLength = cachedManagers.length + 1;

    for (i = startingLength; i < startingLength + 5; i = i + 1) {
      manager = AppraisementObjects.appraiserCompanyManager(i, req.params.branchId);
      cachedManagers.push(manager);
      response.data.push(manager);
    }

    return res.json(response);
  }
};