var companiesObjects = require('../../../standardObjects/appraisement/appraisementObjects');

/**
 * ACH information
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Companies/Ach/Index.md#gets-an-ach-details-of-an-appraisers-company
 */
module.exports = {
  path: /companies.*?ach/,
  callback: function (req, res) {

    return res.json(companiesObjects.ach());
  }
};
