var AppraisementObjects = require('../../../standardObjects/appraisement/appraisementObjects');
/**
 * Appraiser company settings
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Companies/Settings/Index.md#gets-an-appraiser-company-settings
 */
module.exports = {
  path: '/v2.0/appraisement/companies/:companyId/settings',
  callback: function (req, res) {
    return res.json(AppraisementObjects.appraiserCompanySettings());
  }
};