var Appraisement = require('../../../standardObjects/appraisement/appraisementObjects');
var General = require('../../../standardObjects/generalObjects');
var _ = require('lodash');
/**
 * Appraisement company members
 */
module.exports = {
  path: '/v2.0/appraisement/companies/:companyId/members',
  callback: function (req, res) {
    var page = 1, perPage = 10, totalPages = 20, response = {data: [], meta: {}}, i;
    var memberType, branchId = 1;
    // Per page
    if (typeof req.query.perPage !== 'undefined') {
      perPage = parseInt(req.query.perPage);
    }
    // Page
    if (typeof req.query.page !== 'undefined') {
      page = parseInt(req.query.page);
    }

    // Create 25 members
    for (i = 1; i < 20; i = i + 1) {
      // increment branches every 5
      if (i % 5 === 0) {
        branchId = branchId + 1;
      }
      // Get member type
      memberType = i % 2 === 0 ? 'manager' : 'appraiser';
      // Create appraiser
      response.data.push(_.assign({memberType: memberType}, Appraisement.appraiser(i, i % 2 === 0, {branchId: branchId})));
    }

    response.meta.pagination = General.pagination(page, perPage, totalPages);

    return res.json(response);
  }
};