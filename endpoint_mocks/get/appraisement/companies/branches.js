var AppraisementObjects = require('../../../standardObjects/appraisement/appraisementObjects');
module.exports = {
  path: '/v2.0/appraisement/companies/:companyId/branches',
  callback: function (req, res) {
    var response = {data: []}, i;

    for (i = 1; i < 5; i = i + 1) {
        response.data.push(AppraisementObjects.appraiserCompanyBranch(i));
    }

    return res.json(response);
  }
};