var Appraisement = require('../../standardObjects/appraisement/appraisementObjects');

// Appraiser Managers
// @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Managers/Index.md#gets-all-managers
module.exports = {
  path: '/v2.0/appraisement/companies/:companyId/branches/:branchId/managers',
  callback: function (req, res) {

    var response = {data: []}, i;
    for (i = 1; i < 5; i = i + 1) {
      response.data.push(Appraisement.appraiserCompanyManager(i));
    }

    return res.json(response);
  }
};