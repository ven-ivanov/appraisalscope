var Appraisement = require('../../standardObjects/appraisement/appraisementObjects');
var _ = require('lodash');

/**
 * Retrieve appraisement members
 */
module.exports = {
  path: '/v2.0/appraisement/members',
  callback: function (req, res) {
    var response = {data: []};

    // Add appraiser
    response.data.push(_.assign({memberType: Appraisement.appraisementMemberEnum('appraiser')},
    Appraisement.appraiser(1, false)));
    // Add company manager
    response.data.push(_.assign({memberType: Appraisement.appraisementMemberEnum('manager')},
    Appraisement.appraiserCompanyManager(2)));
    // Add company
    response.data.push(_.assign({memberType: Appraisement.appraisementMemberEnum('company')},
    Appraisement.appraiserCompany(3)));
    // Add appraiser/manager
    response.data.push(_.assign({memberType: Appraisement.appraisementMemberEnum('appraiser')},
    Appraisement.appraiser(4, true)));
    // Add company manager
    response.data.push(_.assign({memberType: Appraisement.appraisementMemberEnum('manager')},
    Appraisement.appraiserCompanyManager(5)));
    // Add company
    response.data.push(_.assign({memberType: Appraisement.appraisementMemberEnum('company')},
    Appraisement.appraiserCompany(6)));

    res.json(response);
  }
};