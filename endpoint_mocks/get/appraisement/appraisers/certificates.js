var AppraisementObjects = require('../../../standardObjects/appraisement/appraisementObjects');
// Appraiser certificates
// @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/Certificates/Inedx.md#gets-all-certificates
module.exports = {
  path: '/v2.0/appraisement/appraisers/:appraiserId/certificates',
  callback: function (req, res) {
    var response = {data: []}, i;
    // Primary
    response.data.push(AppraisementObjects.certificate(1, true));
    // Non-primary
    for (i = 2; i < 5; i = i + 1) {
        response.data.push(AppraisementObjects.certificate(i, false));
    }

    return res.json(response);
  }
};