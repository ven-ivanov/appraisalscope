var General = require('../../../standardObjects/generalObjects');
/**
 * County coverage
 */
module.exports = {
  path: '/v2.0/location/states/:stateCode/counties',
  callback: function (req, res) {
    var response = {data: []}, i;

    for (i = 1; i < 4; i = i + 1) {
      response.data.push(General.county(i - 1));
    }

    return res.json(response);
  }
};