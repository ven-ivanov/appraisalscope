var Appraisement = require('../../../standardObjects/appraisement/appraisementObjects');
module.exports = {
  path: '/v2.0/appraisement/appraisers/:appraiserId/settings',
  callback: function (req, res) {
    return res.json(Appraisement.appraiserSettings());
  }
};