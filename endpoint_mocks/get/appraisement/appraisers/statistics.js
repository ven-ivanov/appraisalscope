var AppraisementObjects = require('../../../standardObjects/appraisement/appraisementObjects');
module.exports = {
  path: '/v2.0/appraisement/appraisers/:appraiserId/statistics',
  callback: function (req, res) {
    return res.json(AppraisementObjects.appraiserStatistic());
  }
};
