var AppraisementObjects = require('../../../standardObjects/appraisement/appraisementObjects');
module.exports = {
  path: '/v2.0/appraisement/appraisers/:appraiserId/documents',
  callback: function (req, res) {
    var response = {data: []}, i;
    for (i = 1; i < 5; i = i + 1) {
        response.data.push(AppraisementObjects.appraiserDocument(i));
    }
    return res.json(response);
  }
};