// Appraiser job types
// @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/JobTypes
var Appraisement = require('../../../standardObjects/appraisement/appraisementObjects');
module.exports = {
  path: '/v2.0/appraisement/appraisers/:appraiserId/job-types',
  callback: function (req, res) {
    var i, response = {data: []};

    for (i = 1; i < 5; i = i + 1) {
        response.data.push(Appraisement.appraiserJobType(i));
    }

    return res.json(response);
  }
};