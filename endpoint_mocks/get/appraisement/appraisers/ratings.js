// Appraiser ratings
var Appraisement = require('../../../standardObjects/appraisement/appraisementObjects');
module.exports = {
  path: '/v2.0/appraisement/appraisers/:appraiserId/ratings',
  callback: function (req, res) {
    var response = {data: []}, i;
    for (i = 1; i < 5; i = i + 1) {
        response.data.push(Appraisement.ratings(i, i));
    }
    return res.json(response);
  }
};
