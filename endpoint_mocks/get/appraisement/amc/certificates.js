// Retrieve AMC certificates
// @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Amcs/Certificates/Index.md#get-appraisementamcsamcidcertificates-unimplemented
var Appraisement = require('../../../standardObjects/appraisement/appraisementObjects');
module.exports = {
  path: '/v2.0/appraisement/amcs/:amcId/certificates',
  callback: function (req, res) {
    var response = {data: []}, i;

    for (i = 1; i < 10; i = i + 1) {
        response.data.push(Appraisement.certificate(i, 'amc'));
    }

    return res.json(response);
  }
};