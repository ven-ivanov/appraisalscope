var Appraisement = require('../../../standardObjects/appraisement/appraisementObjects');
// Retrieve all job types for an AMC
// @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Amcs/JobTypes/Index.md#get-appraisementamcsamcidjob-types-unimplemented
module.exports = {
  path: '/v2.0/appraisement/amcs/:amcId/job-types',
  callback: function (req, res) {
    var response = {data: []}, i;

    for (i = 1; i < 10; i = i + 1) {
        response.data.push(Appraisement.appraiserJobType(i));
    }

    return res.json(response);
  }
};