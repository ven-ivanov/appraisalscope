// Get AMC settings
// @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Amcs/Settings/Index.md#get-appraisementamcsamcidsettings-unimplemented
var Appraisement = require('../../../standardObjects/appraisement/appraisementObjects');
module.exports = {
  path: '/v2.0/appraisement/amcs/:amcId/settings',
  callback: function (req, res) {
    return res.json(Appraisement.amcSettings());
  }
};