var Appraisement = require('../../../standardObjects/appraisement/appraisementObjects');
var General = require('../../../standardObjects/generalObjects');
// Get all AMC histories
// @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Amcs/Histories/Index.md#gets-all-amcs-histories
module.exports = {
  path: '/v2.0/appraisement/amcs/:amcId/history',
  callback: function (req, res) {
    var response = {data: []}, i;

    for (i = 1; i < 10; i = i + 1) {
      response.data.push(Appraisement.appraisal(i));
    }

    response.meta = {pagination: General.pagination()};

    return res.json(response);
  }
};