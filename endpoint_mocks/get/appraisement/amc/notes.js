var Note = require('../../../standardObjects/admin/notes/notes');
module.exports = {
  path: '/v2.0/appraisement/amcs/:amcId/notes',
  callback: function (req, res) {
    var response = {data: []}, i;

    for (i = 1; i < 10; i = i + 1) {
        response.data.push(Note.appraiserNote(i));
    }

    return res.json(response);
  }
};