var Amc = require('../../../standardObjects/appraisement/amc/amcs');
// Get a single AMC
// @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Amcs/Index.md#get-appraisementamcsamcid
module.exports = {
  path: '/v2.0/appraisement/amcs/:amcId',
  callback: function (req, res) {
    return res.json(Amc.amc(req.params.amcId));
  }
};