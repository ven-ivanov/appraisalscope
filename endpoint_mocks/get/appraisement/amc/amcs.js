var Amc = require('../../../standardObjects/appraisement/amc/amcs');
/**
 * Get all AMCs
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Amcs/Index.md#get-appraisementamcs
 */
module.exports = {
  path: '/v2.0/appraisement/amcs',
  callback: function (req, res) {
    var response = {data: []}, i;

    for (i = 1; i < 10; i = i + 1) {
        response.data.push(Amc.amc(i));
    }

    return res.json(response);
  }
};