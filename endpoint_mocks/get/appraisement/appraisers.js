var Appraisement = require('../../standardObjects/appraisement/appraisementObjects');
var _ = require('lodash');

// Appraisers
// @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/Index.md#gets-all-appraisers
module.exports = {
  path: '/v2.0/appraisement/appraisers',
  callback: function (req, res) {
    var param, filterParams = {};
    // Get filter params
    for (param in req.query) {
      filterParams = _.assign(filterParams, req.query[param]);
    }
    var undef;
    var response = {data: []}, i;
    for (i = 1; i < 5; i = i + 1) {
        response.data.push(Appraisement.appraiser(i, undef, filterParams));
    }

    return res.json(response);
  }
};