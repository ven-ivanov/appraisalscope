// Query all job types
// @todo Not implemented on backend
// @link https://github.com/ascope/manuals/issues/188
var AdminObjects = require('../../standardObjects/admin/objects');
module.exports = {
  path: '/v2.0/job-types',
  callback: function (req, res) {
    var i, response = {data: []}, filter = req.query.filter;
    for (i = 1; i < 20; i = i + 1) {
      if (i === 5) {
        continue;
      }
      response.data.push(AdminObjects.jobType(i, false, filter));
    }
    return res.json(response);
  }
};