var AppraisersList = require('../../standardObjects/admin/appraisersList/objects');
var Appraisement = require('../../standardObjects/appraisement/appraisementObjects');
var _ = require('lodash');
module.exports = {
  path: '/v2.0/appraiser-lists/appraisers',
  callback: function (req, res) {
    var i, response = {data: []};

    for (i = 1; i < 10; i = i + 1) {
      response.data.push({
        appraiser: Appraisement.appraiser(i),
        list: AppraisersList.appraisersList(_.random(100, 10000))
      });
    }

    return res.json(response);
  }
};