var adminObjects = require('../../standardObjects/admin/objects');

module.exports = {
  path: '/v2.0/handbook/states',
  callback: function (req, res) {
    return res.json({
      data: adminObjects.allStates()
    });
  }
};
