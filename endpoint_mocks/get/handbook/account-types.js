var adminObjects = require('../../standardObjects/admin/objects');

module.exports = {
  path: '/v2.0/handbook/account-types',
  callback: function (req, res) {
    return res.json({
      data: adminObjects.accountTypes()
    });
  }
};
