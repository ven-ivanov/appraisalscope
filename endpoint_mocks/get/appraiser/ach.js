var adminObjects = require('../../standardObjects/admin/objects');

module.exports = {
  path: '/v2.0/appraisers/:userId/ach',
  callback: function (req, res) {
    return res.json({
      data: adminObjects.achInfo()
    });
  }
};
