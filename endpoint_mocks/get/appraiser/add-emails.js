var f = require('faker');

module.exports = {
  path: '/v2.0/appraisers/:userId/add-emails',
  callback: function (req, res) {
    return res.json({
      data: {
      	email1: f.internet.email(),
      	email2: f.internet.email(),
      	email3: f.internet.email(),
      }
    });
  }
};
