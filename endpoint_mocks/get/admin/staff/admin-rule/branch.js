var ClientObjects = require('../../../../standardObjects/client/clientObjects');
module.exports = {
  path: '/v2.0/client/staff/:profileId/admin-rule/branches/:branchId',
  callback: function (req, res) {
    return res.json(ClientObjects.branch(req.params.branchId, req.params.companyId));
  }
};
