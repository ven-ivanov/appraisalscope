var JobType = require('../../../../standardObjects/jobTypes/jobType.js');
var g = require('../../../../generator/generator');


module.exports = {
  path: '/v2.0/admin/admin-rule-profile/:profileId/job-type',
  callback: function (req, res) {
    var i, response = {data: []};
    var total = 2+g.random(1, 10);

    for (i = 1; i < total; i = i + 1) {
      response.data.push(JobType.jobType());
    }
    return res.json(response);
  }
};

