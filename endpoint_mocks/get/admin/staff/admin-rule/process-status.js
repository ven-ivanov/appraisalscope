var ProcessStatus = require('../../../../standardObjects/processStatus/processStatus.js');
var g = require('../../../../generator/generator');
module.exports = {
  path: '/v2.0/admin/admin-rule-profile/:profileId/process-status',
  callback: function (req, res) {
    var i, response = {data: []};
    var total = g.random(1,10);
    for (i = 1; i <= total; i = i + 1) {
      response.data.push(ProcessStatus.processStatus());
    }
    return res.json(response);
  }
};

