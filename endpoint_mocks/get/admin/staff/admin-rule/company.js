var Client = require('../../../../standardObjects/client/clientObjects');
var g = require('../../../../generator/generator');
module.exports = {
  path: '/v2.0/admin/admin-rule-profile/:profileId/companies',
  callback: function (req, res) {
    var i, response = {data: []};
    var total = g.random(1,10);
    for (i = 1; i < 5; i = i + 1) {
      response.data.push(Client.clientCompany(i));
    }
    return res.json(response);
  }
};
