var _ = require('lodash'),
faker = require('faker');

/**
 * Company permissions
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/reviewer-rule/:profileId/brokers',
  callback: function (req, res) {
    var response = [];
    var i;
    // Make 10
    for (i = 1; i <= 5; i = i + 1) {
      response.push({
        id: i,
        active: _.random(0,1),
        name: faker.company.companyName()
      });
    }
    return res.json(response);
  }
};
