var PaidStatus = require('../../../../standardObjects/paidStatus/paidStatus.js');
var g = require('../../../../generator/generator');
module.exports = {
  path: '/v2.0/admin/reviewer-rule-profile/:profileId/paid-status',
  callback: function (req, res) {
    var i, response = {data: []};
    var total = g.random(1,10);
    for (i = 1; i <= total; i = i + 1) {
      response.data.push(PaidStatus.paidStatus());
    }
    return res.json(response);
  }
};

