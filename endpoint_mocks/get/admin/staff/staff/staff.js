/**
 * Created by Venelin on 6/18/2015.
 */
var faker = require('faker');
var _ = require('lodash');

/**
 * Retrieve realtors
 */
module.exports = {
  path: '/v2.0/admin/staff',
  callback: function (req, res) {
    var response = [], i;
    var firstName, lastName;
    for (i = 1; i <= 10; i = i + 1) {
      firstName = faker.name.firstName();
      lastName = faker.name.lastName();
      var staff = {
        id: i,
        name: firstName + ' ' + lastName,
        username: 'user' + i,
        password:'333',
        isSalesPerson : true,
        isVacation: true,
        email: firstName.toLowerCase() + '@' + lastName.toLowerCase() + '.com'
      };
      response.push(staff);
    }
    //must return Object
    var response_obj = {
      data: response
    };
    return res.json(response_obj);
  }
};
