var _ = require('lodash'),
  g = require('../../../../generator/generator'),
  faker = require('faker');

/**
 * Staff Logged in Logs
 */
module.exports = {
  path: '/v2.0/admin/staff/:staffId/logged-in-logs',
  callback: function (req, res) {
    var response = [];
    var total = g.random(1,10);
    // Make
    for (var i = 1; i <= total; i = i + 1) {
      response.push({
        id: i,
        logged_time: g.date.between(new Date(2015, 0, 1), new Date())
      });
    }
    return res.json({data: response});
  }
};
