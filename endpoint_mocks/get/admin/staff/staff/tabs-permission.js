/**
 * Get user mail permissions for staff
 * @type {exports}
 * @private
 */
var _ = require('lodash'),
  faker = require('faker');

module.exports = {
  path: '/v2.0/admin/staff/tabs-permissions/',
  callback: function (req, res) {
    var response = [];
    var i;
    // Create some fake users. Or not.
    for (i = 1; i < 7; i = i + 1) {
      response.push({
        name: faker.name.findName(),
        id: i,
        data: 'xx'
      });
    }
    return res.json({data: response});
  }
};
