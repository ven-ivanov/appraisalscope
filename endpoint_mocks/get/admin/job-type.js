var g = require('../../generator/generator');

/**
 * Retrieve list of job types
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
var getAllJobTypes = function (req, res, next) {
  // Response
  var resBody = [
    {
      'id': g.id(),
      'appraisalForm': 'URAR (1004)',
      'fha': false,
      'commercial': false,
      'display': true,
      'appraisedValue': true,
      'doc': {
        'pdf': true,
        'xml': true,
        'aci': true,
        'zap': true,
        'env': true
      },
      'mappedId': null,
      'addons': [],
      'clientFee': 0.0,
      'managmentFee': 0.0,
      'appraiserFee': 0.0,
      'isManagmentFee': false,
      'isAppraiserFee': true
    },
    {
      'id': g.id(),
      'appraisalForm': 'Trip Fee',
      'fha': true,
      'commercial': true,
      'display': true,
      'appraisedValue': false,
      'doc': {
        'pdf': false,
        'xml': false,
        'aci': true,
        'zap': true,
        'env': true
      },
      'mappedId': 0,
      'addons': [0],
      'clientFee': 0.0,
      'managmentFee': 0.0,
      'appraiserFee': 0.0,
      'isManagmentFee': false,
      'isAppraiserFee': true
    },
    {
      'id': g.id(),
      'appraisalForm': 'SFR 2005',
      'fha': false,
      'commercial': true,
      'display': true,
      'appraisedValue': false,
      'doc': {
        'pdf': false,
        'xml': false,
        'aci': true,
        'zap': true,
        'env': true
      },
      'mappedId': null,
      'addons': [0, 1],
      'clientFee': 0.0,
      'managmentFee': 0.0,
      'appraiserFee': 0.0,
      'isManagmentFee': false,
      'isAppraiserFee': true
    }
  ];

  return res.json(resBody);
};

module.exports = {
  path: '/v2.0/admin/job-type',
  callback: getAllJobTypes
};