module.exports = {
  path: '/v2.0/admin/settings/qcReview/available-elements',
  template: [
  	'Title',
  	'Text Box',	
  	'Text Area',
  	'QC Question',
  	'Dropdown List',
    'Dropdown Multyselect List'
  ]
};