module.exports = {
  path: '/v2.0/admin/settings/qcReview/appraisal-qc-review',
  template: [
    {
      'id': 1,
      'name': 'Appraiser Template 1',
      'title': '',
      'template': 'Template 1',
      'subject': 'Subject 1',
      'from': 'some@email.com'
    },
    {
      'id': 2,
      'name': 'Appraiser Template 2',
      'title': '',
      'template': 'Template 2',
      'subject': 'Subject 1',
      'from': 'some@email.com'
    }, 
    {
      'id': 3,
      'name': 'Appraiser Template 3 ',
      'title': '',
      'template': 'Template 3',
      'subject': 'Subject 1',
      'from': 'some@email.com'
    }
  ]
};