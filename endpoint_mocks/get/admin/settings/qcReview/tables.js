module.exports = {
  path: '/v2.0/admin/settings/qcReview/review-tables',
  template: [
    {
      'id': 1,
      'title': 'some',
      'elementType': '0',
      'status': 'E',
      'selected': '1',
      'dataArray': ['1','2c']
    },
    {
      'id': 2,
      'title': 'other',
      'elementType': 'input',
      'status': 'D',
      'selected': '1',
      'dataArray': ['1','2b']
    }, 
    {
      'id': 3,
      'title': 'thing',
      'elementType': 'dropdown',
      'status': 'E',
      'selected': '1',
      'dataArray': ['1','2s']
    },
    {
      'id': 4,
      'title': 'thing',
      'elementType': 'dropdown',
      'status': 'E',
      'selected': '1',
      'dataArray': ['1','2s']
    },
    {
      'id': 5,
      'title': 'thing',
      'elementType': 'dropdown',
      'status': 'E',
      'selected': '1',
      'dataArray': false 
    },
    {
      'id': 6,
      'title': 'thing',
      'elementType': 'input',
      'status': 'E',
      'selected': '1',
      'dataArray': false
    },
    {
      'id': 7,
      'title': 'thing',
      'elementType': 'dropdown',
      'status': 'E',
      'selected': '1',
      'dataArray': false
    },
    {
      'id': 8,
      'title': 'thing',
      'elementType': 'input',
      'status': 'E',
      'selected': '1',
      'dataArray': false
    }
  ]
};