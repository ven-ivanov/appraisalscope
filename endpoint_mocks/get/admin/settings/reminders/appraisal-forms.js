module.exports = {
  path: '/v2.0/admin/settings/reminders/appraisal-forms/:id',
  template: [
    {
      id: 0,
      appraisalForm: 'testy form'
    },
    {
      id: 1,
      appraisalForm: 'another one'
    },
    {
      id: 2,
      appraisalForm: 'third one'
    }
  ]
};