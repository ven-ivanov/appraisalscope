module.exports = {
  path: '/v2.0/admin/settings/reminders/scheduler',
  template: {
    '0': { 
    	label: '12:00 am',
    	val: false
    },
    '1': { 
    	label: '01:00 am',
    	val: true
    },
    '2': { 
    	label: '02:00 am',
    	val: true
    },
    '3': { 
    	label: '03:00 am',
    	val: true
    },
    '4': { 
    	label: '04:00 am',
    	val: false
    },
    '5': { 
    	label: '05:00 am',
    	val: false
    },
    '6': { 
    	label: '06:00 am',
    	val: false
    },
    '7': { 
    	label: '07:00 am',
    	val: true
    },
    '8': { 
    	label: '08:00 am',
    	val: false
    },
    '9': { 
    	label: '09:00 am',
    	val: false
    },
    '10': { 
    	label: '10:00 am',
    	val: false
    },
    '11': { 
    	label: '11:00 am',
    	val: false
    },
    '12': { 
    	label: '12:00 pm',
    	val: false
    },
    '13': { 
    	label: '01:00 pm',
    	val: false
    },
    '14': { 
    	label: '02:00 pm',
    	val: false
    },
    '15': { 
    	label: '03:00 pm',
    	val: false
    },
    '16': { 
    	label: '04:00 pm',
    	val: false
    },
    '16': { 
    	label: '04:00 pm',
    	val: false
    },
    '17': { 
    	label: '05:00 pm',
    	val: false
    },
    '18': { 
    	label: '06:00 pm',
    	val: false
    },
    '19': { 
    	label: '07:00 pm',
    	val: false
    },
    '20': { 
    	label: '08:00 pm',
    	val: false
    },
    '21': { 
    	label: '09:00 pm',
    	val: false
    },
    '22': { 
    	label: '10:00 pm',
    	val: false
    },
    '23': { 
    	label: '11:00 pm',
    	val: false
    }
  }
};