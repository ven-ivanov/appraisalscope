module.exports = {
  path: '/v2.0/admin/settings/reminders/options',
  template: {
    'hourOptions': {
      '0': 'On cut off time',
      '1': '1 hour',
      '2': '2 hours',
      '3': '3 hours',
      '4': '4 hours',
      '6': '6 hours',
      '12': '12 hours',
      '24': '24 hours',
      '48': '48 hours'
    },
    'dateOptions1': {
      '1': 'Estimated Completion Date',
      '2': 'Appraisal Due Date'
    },
    'dateOptions2': {
      '3': 'Accepted Date',
      '4': 'Assigned Date'
    },
    'cutOffTimeOptions': {
      '0': '12:00 am',
      '1': '01:00 am',
      '2': '02:00 am',
      '3': '03:00 am',
      '4': '04:00 am',
      '5': '05:00 am',
      '6': '06:00 am',
      '7': '07:00 am',
      '8': '08:00 am',
      '9': '09:00 am',
      '10': '10:00 am',
      '11': '11:00 am',
      '12': '12:00 pm',
      '13': '01:00 pm',
      '14': '02:00 pm',
      '15': '03:00 pm',
      '16': '04:00 pm',
      '17': '05:00 pm',
      '18': '06:00 pm',
      '19': '07:00 pm',
      '20': '08:00 pm',
      '21': '09:00 pm',
      '22': '10:00 pm',
      '23': '11:00 pm'
    }
  }
};