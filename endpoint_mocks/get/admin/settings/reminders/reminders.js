module.exports = {
  path: '/v2.0/admin/settings/reminders/reminders',
  template: {
  	'1': {
  		'hours': '2',
  		'date': '1',
  		'cutOffTime': '12',
  		'active': true
  	},
  	'2': {
  		'hours': '2',
			'active': true  		
  	},
  	'3': {
  		'hours': '2',
  		'active': true
  	},
  	'4': {
  		'hours': '2',
  		'date': '1',
  		'cutOffTime': '12',
  		'active': false
  	},
  	'5': {
  		'hours': '2',
  		'active': 0
  	},
  	'7': {
  		'hours': '2',
  		'active': false
  	},
  	'9': {
  		'days': '2',
  		'active': true
  	},
  	'11': {
  		'hours': '2',
  		'date': '4',
  		'active': false
  	},
  	'12': {
  		'hours': '2',
  		'active': false
  	},
  	'13': {
  		'notifyClient': true,
  		'active': false
  	}
  }
};