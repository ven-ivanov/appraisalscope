module.exports = {
  path: '/v2.0/admin/settings/options/vendor',
  template: [
    {
      'id': 1,
      'status': 'Gold'
    },
    {
      'id': 2,
      'status': 'Silver'
    },
    {
      'id': 3,
      'status': 'Bronze'
    },
    {
      'id': 4,
      'status': 'Trial'
    }
  ]
};