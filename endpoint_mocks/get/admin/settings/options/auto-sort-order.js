module.exports = {
  path: '/v2.0/admin/settings/options/auto-sort-order',
  template: [
    'By Minimum Distance',
    'By General/Certified/Licensed',
    'By Minimum Open Order',
    'By Max Rating',
    'By Prioritize Approved Lists'
  ]
};