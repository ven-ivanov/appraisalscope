module.exports = {
  path: '/v2.0/admin/settings/options/manual-sort-order',
  template: [
    'By Max Open Orders',
    'By Name',
    'By Certification',
    'By Proximity',
    'By Turn Time',
    'By FHA',
    'By Open Orders',
    'By Fee Lowest to Highest'
  ]
};