module.exports = {
  path: '/v2.0/admin/settings/options/appraisal-form',
  template: [
    {
      id: 0,
      appraisalForm: 'testy form'
    },
    {
      id: 1,
      appraisalForm: 'another one'
    },
    {
      id: 2,
      appraisalForm: 'third one'
    }
  ]
};