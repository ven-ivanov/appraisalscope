module.exports = {
  path: '/v2.0/admin/settings/options/turn-time-rating',
  template: {
    'star5': 5,
    'star4': 16,
    'star3': 18,
    'star2': 25,
    'star1': 26
  }
};