module.exports = {
  path: '/v2.0/admin/settings/options/updates',
  template: [
    {
      'id': 1,
      'update': 'Accepted',
      'notifyClient': false
    },
    {
      'id': 2,
      'update': 'Declined',
      'notifyClient': true
    },
    {
      'id': 3,
      'update': 'Inspection Scheduled',
      'notifyClient': false
    },
    {
      'id': 4,
      'update': 'Inspection Scheduled',
      'notifyClient': false
    },
    {
      'id': 5,
      'update': 'On Hold',
      'notifyClient': true
    },
    {
      'id': 6,
      'update': 'Ready for Review',
      'notifyClient': false
    },
    {
      'id': 7,
      'update': 'Revision Received',
      'notifyClient': true
    },
    {
      'id': 8,
      'update': 'Additional Statuses',
      'notifyClient': false
    },
  ]
};