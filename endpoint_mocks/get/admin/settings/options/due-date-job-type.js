module.exports = {
  path: '/v2.0/admin/settings/options/due-date-job-type',
  template: [
    {
      'id': 0,
      'appraisalForm': 'testForm 1',
      'days': 0
    },
    {
      'id': 1,
      'appraisalForm': 'test form 2',
      'days': 5
    },
    {
      'id': 2,
      'appraisalForm': 'test form 3',
      'days': 2
    }
  ]
};