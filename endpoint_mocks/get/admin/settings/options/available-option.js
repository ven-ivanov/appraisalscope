module.exports = {
  path: '/v2.0/admin/settings/options/available-option',
  template: {
    'data': {
      unpaidOrders: function () {
        return [
          'Show all unpaid and paid orders in the new queue on the dashboard',
          'Show only orders that have been fully paid in the new queue. All unpaid or partially paid orders will move to the on hold queue',
          'Show orders that have been partially paid or fully paid in the new queue. All orders with a full balance due will be moved to the on hold queue'
        ];
      },
      appraisalDocuments: function () {
        return [
          'Do not show appraisals by default',
          'Show appraisals to all users by default',
          'Show appraisals to Clients by default',
          'Show appraisals to Appraiser by default'
        ];
      },
      additionalDocuments: function () {
        return [
          'Do not show additional documents by default',
          'Only show addiitional documents uploaded by the user by default',
          'Show Additional Documents to all users by default',
          'Show Additional Documents to Clients by default',
          'Show Additional Documents to Appraiser by default'
        ];
      },
      dueDate: function () {
        return [
          'Enable Due date in Admin view only',
          'Enable Due date in all views',
          'Disable Due date in all views'
        ];
      }
    }
  }

};