module.exports = {
  path: '/v2.0/admin/settings/options/revision-type',
  template: [
    {
      'id': 1,
      'revisionType': 'Reviewer'
    },
    {
      'id': 2,
      'revisionType': 'Underwriter'
    },
    {
      'id': 3,
      'revisionType': 'Reviewer Type 2',
      'asClientRevision': true,
      'asReviewerRevision': false
    },
    {
      'id': 4,
      'revisionType': 'Reviewer Type 4',
      'asClientRevision': false,
      'asReviewerRevision': true
    }
  ]
};