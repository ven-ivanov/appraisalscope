module.exports = {
  path: '/v2.0/admin/settings/predefinedMessages/predefinedMessage',
    template: {
    'predefinedMessage': [
      {
        'id':1,
        'Title': 'Titile 1',
        'Message': 'Message 1',
        'type': '1',
      },
      {
        'id':2,
        'Title': 'Titile 3',
        'Message': 'Message 3',
        'type': '0',
      }

    ],
    'PredefinedMessagesForRevision': [
      {
        'id':1,
        'Title': 'Titile 1',
        'Message': 'Message 1',
        'type': '1',
      },
      {
        'id':2,
        'Title': 'Titile 2',
        'Message': 'Message 2',
        'type': '0',
      }
    ]
     
    }
};

