module.exports = {
  path: '/v2.0/admin/settings/document-upload/system-templates',
  template: {
    'data': [
      {
        'id': 1,
        'label': '#1 Template',
        'body': 'Omnis Draco Maledicte'
      },
      {
        'id': 2,
        'label': '#2 Template',
        'body': 'Et Omnis Legio Diabloica'
      }
    ]
  }
};