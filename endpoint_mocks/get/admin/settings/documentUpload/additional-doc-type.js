module.exports = {
  path: '/v2.0/admin/settings/document-upload/additional-doc-type',
  template: {
    'data': [
      {
        'id': 1,
        'label': 'Appraisal',
        'displayToAppraiser': true,
        'displayToClient': true,
        'displayToAdmin': true
      },
      {
        'id': 2,
        'label': 'Condo Questionnaire',
        'displayToAppraiser': true,
        'displayToClient': false,
        'displayToAdmin': true
      },
      {
        'id': 3,
        'label': 'Invoice',
        'displayToAppraiser': false,
        'displayToClient': true,
        'displayToAdmin': true
      },
      {
        'id': 4,
        'label': 'Tax Receipts',
        'displayToAppraiser': true,
        'displayToClient': true,
        'displayToAdmin': true
      }
    ]
  }
};