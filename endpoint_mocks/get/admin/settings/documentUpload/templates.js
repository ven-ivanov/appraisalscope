module.exports = {
  path: '/v2.0/admin/settings/document-upload/templates',
  template: {
    'data': [
      {
        'id': 1,
        'label': '#1 Doc',
        'body': 'Never'
      },
      {
        'id': 2,
        'label': '#2 Doc',
        'body': 'Gonna'
      },
      {
        'id': 3,
        'label': 'Give',
        'body': 'You'
      },
      {
        'id': 4,
        'label': '#4 Dog',
        'body': 'Up'
      }
    ]
  }
};