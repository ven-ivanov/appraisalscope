module.exports = {
  path: '/v2.0/admin/settings/document-upload/system-documents',
  template: {
    'data': [
      {
        'id': 1,
        'label': '#1 Doc',
        'href': '#'
      },
      {
        'id': 2,
        'label': '#2 Doc',
        'href': '#'
      },
      {
        'id': 3,
        'label': '#3 Doc',
        'href': '#'
      },
      {
        'id': 4,
        'label': '#4 Doc',
        'href': '#'
      }
    ]
  }
};