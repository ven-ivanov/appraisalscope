module.exports = {
  path: '/v2.0/admin/settings/document-upload/tags',
  template: {
    'data': [
      {
        'label': 'Client',
        'code': 'client'
      },
      {
        'label': 'Ordered By',
        'code': 'ordered_by'
      },
      {
        'label': 'Client displayed on report',
        'code': 'client_displayed_on_report'
      },
      {
        'label': 'Submitted by',
        'code': 'submitted_by'
      },
      {
        'label': 'Client Fee',
        'code': 'client_fee'
      },
      {
        'label': 'Appraiser Fee',
        'code': 'appraiser_fee'
      },
      {
        'label': 'Job type',
        'code': 'job_type'
      }
    ]
  }
};