module.exports = {
  path: '/v2.0/admin/settings/email/appraiser-templates',
  template: [
    {
      'id': 1,
      'title': 'Appraiser Template 1',
      'template': 'Template 1',
      'subject': 'Subject 1',
      'from': 'some@email.com'
    },
    {
      'id': 2,
      'title': 'Appraiser Template 2',
      'template': 'Template 2',
      'subject': 'Subject 1',
      'from': 'some@email.com'
    }, 
    {
      'id': 3,
      'title': 'Appraiser Template 3 ',
      'template': 'Template 3',
      'subject': 'Subject 1',
      'from': 'some@email.com'
    }
  ]
};