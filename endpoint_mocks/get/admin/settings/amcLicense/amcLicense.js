module.exports = {
  path: '/v2.0/admin/settings/amcLicense/amcLicense',
  template: [
    {
      'id': 1,
      'State': 'Alabama',
      'License': 'License 1',
      'DisplayLicense': '1',
      'ExpirationDate': '06-12-2014',
      'DisplayExpirationDate': '0',
      'DisplayClientFeeToAppraiser': '1',
      'DisplayAppraiserFeeToClient': '0'
    },
    {
      'id': 2,
      'State': 'Alaska',
      'License': 'License 2',
      'DisplayLicense': '0',
      'ExpirationDate': '06-12-2014',
      'DisplayExpirationDate': '0',
      'DisplayClientFeeToAppraiser': '1',
      'DisplayAppraiserFeeToClient': '0'
    },
    {
      'id': 3,
      'State': 'California',
      'License': 'License 3',
      'DisplayLicense': '0',
      'ExpirationDate': '06-12-2014',
      'DisplayExpirationDate': '1',
      'DisplayClientFeeToAppraiser': '1',
      'DisplayAppraiserFeeToClient': '0'
    },
    {
      'id': '4',
      'State': 'Colorado',
      'License': 'License 4',
      'DisplayLicense': '1',
      'ExpirationDate': '06-12-2014',
      'DisplayExpirationDate': '0',
      'DisplayClientFeeToAppraiser': '1',
      'DisplayAppraiserFeeToClient': '0'
    }   
  ]
};

		        
