var settingsObjects = require('../../../standardObjects/admin/settings');

module.exports = {
  path: '/v2.0/admin/notifications',
  callback: function (req, res) {
  	var notifications = [];

  	for (var i = 0; i < 10; i++) {
  		notifications.push(settingsObjects.notification());
  	}

    return res.json({
      data: notifications
    });
  }
};
