var ProcessStatus = require('../../standardObjects/processStatus/processStatus.js');
var g = require('../../generator/generator');
/**
 * Retrieve process Status
 */
module.exports = {
  path: '/v2.0/admin/process-status',
  callback: function (req, res) {
    var i, response = {data: []};
    var total = g.random(1,5);
    for (i = 1; i < total; i = i + 1) {
      response.data.push(ProcessStatus.processStatus());
    }
    return res.json(response);
  }
};
