var _ = require('lodash');

module.exports = {
  path: '/v2.0/admin/accounting/tab-values',
  callback: function (req, res) {
    return res.json({
      unpaid: 186,
      paid: 891,
      invoices: 8941,
      'appraiser-unpaid':  4894,
      'pending-ach': 489,
      'appraiser-paid': 971,
      quickbooks: 981,
      commissions: 1568,
      'aging-balance': 415,
      'transaction-summary': 8644
    });
  }
};