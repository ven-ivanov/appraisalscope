var generator = require('../../../generator/generator'),
    _ = require('lodash');

var dataNumber = 1;

var param, paramArgs, filter, filterArgs;

/**
 * Generate a single record of table data
 */
var randomTableData = function (page) {
  var invoicePaidStatuses = ['', 'paid', 'unpaid', 'partial paid'];
  var qbAction = ['bill add', 'vendor add', 'customer add'];
  var currentBalance;
  // Selecting invoice statuses
  if (param === 'invoice-status' && typeof paramArgs !== 'undefined') {
    invoicePaidStatuses = paramArgs.split(',');
  // Quickbooks
  } else if (filter === 'requestAction' && typeof filterArgs !== 'undefined' && filterArgs !== 'all') {
    qbAction = [filterArgs.replace('_', ' ')];
  }
  var json = {
    id: ((page - 1) * 10) + dataNumber,
    sNo: ((page - 1) * 10) + dataNumber,
    action: _.sample(['No action', 'Some action', 'Other action']),
    // Appraiser info
    appraiser: {
      bank_name: _.sample(['Blah bank', null]),
      bank_account_number: 1,
      bank_routing: 1
    },
    qbMsg: 'qb message' + _.random(0, 10),
    qbStatus: _.sample(['In Progress', 'Some action', 'Other action']),
    qbPriority: _.random(0, 10),
    qbReqData: 'req data ' + _.random(0, 5),
    qbIdent: _.random(1, 1000),
    qbAction: _.sample(qbAction),
    qbQueueId: _.random(1, 1000),
    commission: _.random(0, 5000, true).toFixed(2),
    jobType: _.sample(['appraisal', 'client job']),
    salesPerson: generator.fullName(),
    invoiceDescription: _.sample(['', 'Description 1', 'Description 2']),
    generatedDate: generator.date.between(new Date(2012, 0, 1), new Date()),
    invoiceStatus: _.sample(invoicePaidStatuses),
    invoiceAmount: _.sample([null, _.random(0, 5000, true).toFixed(2)]),
    invoiceNumber: _.sample(['', 'invoice' + _.random(1, 10)]),
    checkNumber: _.random(1, 1000),
    plAmount: _.random(0, 2000, true).toFixed(2),
    appFee: _.random(0, 5000, true).toFixed(2),
    clientFee: _.random(0, 500, true).toFixed(2),
    paidDate: generator.date.between(new Date(2012, 0, 1), new Date()),
    balanceDue: _.random(0, 5000, true).toFixed(2),
    amountPaid: _.random(0, 5000, true).toFixed(2),
    completedDate: generator.date.between(new Date(2012, 0, 1), new Date()),
    orderDate: generator.date.between(new Date(2012, 0, 1), new Date()),
    address: generator.streetAddress(),
    borrower: generator.fullName(),
    client: generator.lastName() + ' Bank',
    appraiserName: generator.fullName(),
    appraisalCompany: generator.lastName() + ' Appraisers',
    state: _.sample(['Texas', 'Ohio', 'Indiana', 'Washington']),
    file: generator.uppercase(3) + _.random(0, 99999),
    orderedFor: generator.fullName(),
    transactionDate: generator.date.between(new Date(2012, 0, 1), new Date()),
    referenceNumber: _.random(0, 999999),
    creditCardNumber: _.random(0, 9).toString() + _.random(0, 9).toString() + _.random(0, 9).toString() +
                      _.random(0, 9).toString() + '-xxxx-xxxx-xxxx',
    paymentType: _.sample(['refund', 'capture']),
    credit: _.random(0, 500, true),
    debit: _.random(0, 500, true),
    transactionRemarks: _.sample(['Remark 1', 'Remark 2'])
  };

  /**
   * Quick and dirty aging balance figures
   */
  json.current = _.random(0, json.balanceDue, true);
  currentBalance = json.balanceDue - json.current;
  json.over90 = _.random(0, currentBalance, true);
  currentBalance = currentBalance - json.over90;
  json.over60 = _.random(0, currentBalance, true);
  currentBalance = currentBalance - json.over60;
  json.over30 = currentBalance;

  return json;
};

/**
 * Retrieve table data via mock API endpoint
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
var generateTableData = function (req, res, next) {
  // Response
  var resBody = [];

  /**
   * Make sure page number is an actual number
   */
  if (!_.isFinite(parseInt(req.params.page))) {
    return res.status(500).send('Page must be a number');
  }
  // If we're working with a param, note it
  if (!_.isUndefined(req.params.type) && _.isString(req.params.type) && req.params.type.length) {
    param = req.params.type;
    paramArgs = req.params.paramArgs;
  }
  // Change QB status (to none found)
  if (param === 'requestStatus' && paramArgs !== 'In_Queue') {
    return res.json({});
  }
  // Same for filter
  if (!_.isUndefined(req.params.filter) && _.isString(req.params.filter) && req.params.filter.length) {
    filter = req.params.filter;
    filterArgs = req.params.filterArgs;
  }

  var i;
  // Get ten pages of random table data
  for (i = 0; i < 10; i = i + 1) {
    resBody.push(randomTableData(req.params.page));
    dataNumber = dataNumber + 1;
  }
  dataNumber = 1;
  // Reset args
  param = paramArgs = null;

  return res.json(resBody);
};

module.exports = {
  path: '/v2.0/admin/accounting/data/:page/:type?/:paramArgs?/:filter?/:filterArgs?',
  callback: generateTableData
};