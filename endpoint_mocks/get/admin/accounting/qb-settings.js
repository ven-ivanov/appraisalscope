/**
 * Retrieve quickbooks settings
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/accounting/qb-settings',
  callback: function (req, res) {
    return res.json({
      incomeAmount: 'Commission Income',
      costGoodsSold: 'Commissions Paid',
      accountsReceivable: 'Accounts Receivable',
      syncInvoice: 'On completion',
      syncCcPayments: 'On',
      syncCheckPayments: 'On',
      syncAppraiserChecks: 'On',
      sync1099: 'On',
      syncMethod: 'invoice'
    });
  }
};