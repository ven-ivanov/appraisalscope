module.exports = {
  path: '/v2.0/admin/accounting/grand-total/:view',
  callback: function (req, res) {
    var json;
    switch (req.params.view) {
      case 'unpaid':
        json = {
          "clientFee": 62626,
          "amountPaid": 266260,
          "balanceDue": 15652620,
          "appFee": 626224,
          "plAmount": 5253260
        };
        break;
      case 'paid':
        json = {
          clientFee: 532532.02,
          appFee: 21543.11,
          plAmount: 42157.01
        };
        break;
      case 'invoices':
        json = {
          invoiceAmount: 512516.16,
          amountPaid: 526261.67,
          balanceDue: 1651684.57
        };
        break;
      case 'appraiser-unpaid':
        json = {
          clientFee: 4894984.52,
          appFee: 464894.56,
          plAmount: 794894.68
        };
        break;
      case 'pending-ach':
        json = {
          clientFee: 489179.68,
          appFee: 88486.65,
          plAmount: 689611.68
        };
        break;
      case 'appraiser-paid':
        json = {
          clientFee: 489179.68,
          appFee: 88486.65,
          plAmount: 689611.68
        };
        break;
      case 'commissions':
        json = {
          clientFee: 894984.98,
          appFee: 46165.98,
          plAmount: 4165165.12,
          commission: 984984.54
        };
        break;
      case 'aging-balance':
        json = {
          current: 52525.51,
          over30: 5215.00,
          over60: 2525.00,
          over90: 51553.00,
          balanceDue: 25626.58
        };
        break;
      case 'transaction-summary':
        json = {
          credit: 15616846.46,
          debit: 1651548.69,
          appFee: 5616846.12
        };
        break;
      default:
        json = {};
    }
    return res.json(json);
  }
};