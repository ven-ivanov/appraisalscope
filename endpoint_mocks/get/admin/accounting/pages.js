var _ = require('lodash');
/**
 * Get total number of pages for a view
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/accounting/pages/:view',
  callback: function (req, res) {
    return res.json({
      pages: _.random(10, 50)
    });
  }
};