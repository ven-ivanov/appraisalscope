module.exports = {
  path: '/v2.0/admin/accounting/get-user-details/:userIds',
  callback: function (req, res) {
    var userIds = req.params.userIds.split(',');
    var resBody = {};
    userIds.forEach(function (userId) {
      resBody[userId] = {
        company: 'Company ' + userId,
        data: [
        {
          label: 'Sub User ' + userId,
          id: userId + '@' + userId + '.com'
        },
        {
          label: 'Sub User ' + userId + userId,
          id: userId + '@' + userId + userId + '.com'
        }],
        appraisalId: parseInt(userId)
      }
    });
    return res.json(resBody);
  }
};