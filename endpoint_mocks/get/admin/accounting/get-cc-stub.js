/**
 * Retrieve stub of credit card for admin accounting view
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/accounting/get-cc-stub/:userId',
  callback: function (req, res) {
    res.json({
      ccNumber: '5262467352631567',
      expiration: '01/2020'
    });
  }
};