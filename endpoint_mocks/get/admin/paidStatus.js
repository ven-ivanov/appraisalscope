var _ = require('lodash');
var PaidStatus = require('../../standardObjects/paidStatus/paidStatus.js');
var g = require('../../generator/generator');
/**
 * Retrieve paid status
 */
module.exports = {
  path: '/v2.0/admin/paid-status',
  callback: function (req, res) {
    var i, response = {data: []};
    var total = g.random(1,10);
    for (i = 1; i < total; i = i + 1) {
      response.data.push(PaidStatus.paidStatus());
    }
    return res.json(response);
  }
};
