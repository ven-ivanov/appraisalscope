// var g = require('../../generator/generator');

/**
 * Retrieve list of job types lists
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
var getAllJobTypeLists = function (req, res, next) {
  // Response
  var resBody = [
    {
      'id': g.id(),
      'name': 'Test List A'
    },
    {
      'id': g.id(),
      'name': 'Test List B'
    },
    {
      'id': g.id(),
      'name': 'Test List C'
    }
  ];

  return res.json(resBody);
};

module.exports = {
  path: '/v2.0/admin/job-type-list',
  callback: getAllJobTypeLists
};