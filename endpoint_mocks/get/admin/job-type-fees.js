// var g = require('../../generator/generator');

/**
 * Retrieve list of fees
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
var getAllFees = function (req, res, next) {

  switch (req.params.type) {
    case "state":
      var resBody = [
        {
          id: 10,
          abbr: 'AL',
          name: 'Alaska',
          clientFee: 0.0,
          managmentFee: 0.0,
          appraiserFee: 0.0,
          isManagmentFee: false,
          isAppraiserFee: true
        },
        {
          id: 11,
          abbr: 'DC',
          name: 'District Columbia',
          clientFee: 0.0,
          managmentFee: 30.0,
          appraiserFee: 10.0,
          isManagmentFee: true,
          isAppraiserFee: true
        },
        {
          id: 12,
          abbr: 'GE',
          name: 'Georgia',
          clientFee: 0.0,
          managmentFee: 30.0,
          appraiserFee: 10.0,
          isManagmentFee: true,
          isAppraiserFee: true
        }
      ];
    break;

    case "zip":
      var resBody = [
        {
          id: 10,
          code: '32154',
          clientFee: 0.0,
          managmentFee: 0.0,
          appraiserFee: 0.0,
          isManagmentFee: false,
          isAppraiserFee: true
        },
        {
          id: 11,
          code: '78894',
          clientFee: 0.0,
          managmentFee: 30.0,
          appraiserFee: 10.0,
          isManagmentFee: true,
          isAppraiserFee: true
        },
        {
          id: 12,
          code: '87944',
          clientFee: 0.0,
          managmentFee: 30.0,
          appraiserFee: 10.0,
          isManagmentFee: true,
          isAppraiserFee: true
        }
      ];
    break;

    case "county":
      var resBody = [
        {
          id: 10,
          name: 'APPLING',
          clientFee: 0.0,
          managmentFee: 0.0,
          appraiserFee: 0.0,
          isManagmentFee: false,
          isAppraiserFee: true
        },
        {
          id: 11,
          name: 'BROOK',
          clientFee: 0.0,
          managmentFee: 30.0,
          appraiserFee: 10.0,
          isManagmentFee: true,
          isAppraiserFee: true
        },
        {
          id: 12,
          name: 'DRUM',
          clientFee: 0.0,
          managmentFee: 30.0,
          appraiserFee: 10.0,
          isManagmentFee: true,
          isAppraiserFee: true
        }
      ];
    break;

    default:
      var resBody = [];
  }

  return res.json(resBody);
};

module.exports = {
  path: '/v2.0/admin/job-type-fees/:type?',
  callback: getAllFees
};
