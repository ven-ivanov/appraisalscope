var g = require('../../generator/generator');
var f = require('faker');

var messageResponses = function (req, res, next) {
  var resBody = [];
  var total = g.random(1, 10);

  for (var i = 1; i <= total; i ++) {
    resBody.push({
      id: g.randomNumber(),
      title: f.lorem.words(g.random(1, 5)).join(' '),
      text: f.lorem.sentence()
    });
  }

  return res.json({
    data: resBody
  });
};

module.exports = {
  path: '/v2.0/admin/predefined-messages',
  callback: messageResponses
};
