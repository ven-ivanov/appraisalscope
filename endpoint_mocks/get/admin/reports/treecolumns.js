var reports       = require('../../../standardObjects/admin/reports/reports.js');

module.exports = {
  path: '/v2.0/admin/reports/treecolumns/:type?',
  callback: function (req, res) {
    return res.json({data : reports.getReportTreeColumns()});
  }
};

