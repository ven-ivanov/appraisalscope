var gridTable     = require('../../../standardObjects/gridTable.js');
var reports       = require('../../../standardObjects/admin/reports/reports.js');

module.exports = {
  path: '/v2.0/admin/reports/listreports/:gridState?',
  callback: function (req, res) {
    var params;

    try{ params = JSON.parse(req.params.gridState); }
    catch(e){ console.log(e); return res.json({}); }

    return res.json(gridTable(params, reports.listReports()));
  }
};

