var _             = require('lodash');
var utils         = require('../../../standardObjects/utils.js');
var paginator     = require('../../../standardObjects/paginator.js');
var reports       = require('../../../standardObjects/admin/reports/reports.js');

module.exports = {
  path: '/v2.0/admin/reports/:id/dynamicfilters/',
  callback: function (req, res) {
    return res.json(reports.dynamicFilters());
  }
};

