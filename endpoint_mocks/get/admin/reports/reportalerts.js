var _             = require('lodash');
var utils         = require('../../../standardObjects/utils.js');
var paginator     = require('../../../standardObjects/paginator.js');
var reports       = require('../../../standardObjects/admin/reports/reports.js');

module.exports = {
  path: '/v2.0/admin/reports/:id/reportalerts/:alertId?',
  callback: function (req, res) {

    console.log(req.params);
    if(req.params.alertId){
      return res.json({data : reports.reportAlert()});
    }else{
      return res.json({data : reports.reportAlerts()});
    }
  }
};

