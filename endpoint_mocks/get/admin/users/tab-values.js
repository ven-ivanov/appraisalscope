var _ = require('lodash');

/**
 * Simple tab values for admin users view
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/users/tab-values',
  callback: function (req, res) {
    return res.json({
      client: 186,
      appraiser: 891,
      inspector: 8941,
      realtor:  484,
      amc: 489,
      'appraiser-directory': 971,
      'amc-directory': 418,
      asc: 981,
      disabled: 1568,
      'appraiser-panel': 415,
      'pending-users': 8644,
      'email-users': 861
    });
  }
};