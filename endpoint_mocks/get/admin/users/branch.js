var faker = require('faker');
var ClientObjects = require('../../../standardObjects/client/clientObjects');
/**
 * Get branch settings
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/client/company/:id/branch/:branchId',
  callback: function (req, res) {
    // Return standard branch
    return res.json(ClientObjects.branch(req.params.branchId));
  }
};