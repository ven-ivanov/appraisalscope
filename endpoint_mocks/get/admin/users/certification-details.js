/**
 * Appraiser certification details
 * @type {exports}
 */
var faker = require('faker'),
_ = require('lodash');
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/certification-details',
  callback: function (req, res) {
    var response = [], i;
    for (i = 1; i < 5; i = i + 1) {
      response.push({
        id: i,
        state: faker.address.state(),
        licenseNumber: _.random(10000, 99999) + '-R',
        licenseType: _.sample(['General ' + req.params.type, 'Licensed ' + req.params.type]),
        fha: _.random(0, 1),
        commercial: _.random(0, 1),
        expirationDate: new Date(),
        licenseDoc: _.sample(['', 'http://www.analysis.im/uploads/seminar/pdf-sample.pdf'])
      });
    }
    return res.json(response);
  }
};