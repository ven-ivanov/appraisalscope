var faker = require('faker'),
_ = require('lodash');

/**
 * Appraiser coverage
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/coverage',
  callback: function (req, res) {
    // Create one with a known state as primary state
    var response = [
      {
        id: 1,
        state: 'OH',
        primaryState: 'OH',
        license: _.random(1000, 9999) + '-R',
        licenseType: _.random(1,2),
        fha: _.random(0, 1),
        commercial: _.random(0, 1),
        expirationDate: new Date(),
        licenseDoc: 'http://www.analysis.im/uploads/seminar/pdf-sample.pdf',
        counties: [1],
        countyNames: [{name: 'Harris'}, {name: 'Hamilton'}, {name: 'Kenton'}]
      }], i;
    for (i = 2; i < 4; i = i + 1) {
      response.push({
        id: i,
        state: faker.address.stateAbbr(),
        primaryState: faker.address.stateAbbr(),
        license: _.random(1000, 9999) + '-R',
        licenseType: _.random(1,2),
        fha: _.random(0,1),
        commercial: _.random(0, 1),
        expirationDate: new Date(),
        licenseDoc: _.sample(['', 'http://www.analysis.im/uploads/seminar/pdf-sample.pdf']),
        counties: [1],
        countyNames: [{name: 'Harris'}, {name: 'Hamilton'}, {name: 'Kenton'}]
      });
    }

    return res.json(response);
  }
};