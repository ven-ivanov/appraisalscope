var _ = require('lodash');
/**
 * AMC settings
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/amc/:id/settings',
  callback: function (req, res) {
    return res.json({
      submitPayment: _.random(0,1),
      useAmcFeeSchedule: _.random(0,1)
    });
  }
};