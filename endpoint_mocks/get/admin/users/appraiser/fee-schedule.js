/**
 * Retrieve fee schedule
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/fee-schedule',
  callback: function (req, res) {
    return res.json({
      calculateBasedOnPercentage: 1,
      percentageClientFee: 57.2,
      techFee: 1
    });
  }
};