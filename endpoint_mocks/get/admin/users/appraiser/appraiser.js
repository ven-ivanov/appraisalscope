var faker = require('faker');
var _ = require('lodash');

/**
 * Retrieve appraisers
 */
module.exports = {
  path: '/v2.0/users/appraiser/',
  callback: function (req, res) {
    var active = true;
    if (typeof req.query.disabled !== 'undefined' && req.query.disabled === 'true') {
      active = false;
    }
    var response = [], i;
    var firstName, lastName;
    for (i = 1; i <= 20; i = i + 1) {
      firstName = faker.name.firstName();
      lastName = faker.name.lastName();
      response.push({
        id: i,
        firstName: firstName,
        lastName: lastName,
        certification: _.random(1, 3),
        resume: 'http://www.analysis.im/uploads/seminar/pdf-sample.pdf',
        w9: 'http://www.analysis.im/uploads/seminar/pdf-sample.pdf',
        eoInsuranceLink: 'http://www.analysis.im/uploads/seminar/pdf-sample.pdf',
        fha: _.random(0,1),
        rating: _.random(0, 5),
        username: 'user' + i,
        vacation: _.random(0,1),
        email: firstName.toLowerCase() + '@' + lastName.toLowerCase() + '.com',
        phone: faker.phone.phoneNumber(),
        cell: faker.phone.phoneNumber(),
        fax: faker.phone.phoneNumber(),
        vendorStatus: 'Definitely a vendor',
        maxOrders: _.random(0, 100000),
        application: _.random(0, 1),
        taxId: _.random(10000, 99999),
        ssn: _.random(100, 999) + '-' + _.random(100,999) + '-' + _.random(1000, 9999),
        registered: new Date(),
        companyName: faker.company.companyName(),
        active: active,
        // Office address
        officeAddress1: faker.address.streetAddress(),
        officeAddress2: '',
        officeCity: faker.address.city(),
        officeState: faker.address.state(),
        officeZip: faker.address.zipCode(),
        // Assign address
        assignmentAddress1: faker.address.streetAddress(),
        assignmentAddress2: '',
        assignmentCity: faker.address.city(),
        assignmentState: faker.address.state(),
        assignmentZip: faker.address.zipCode(),
        // E&O details
        eoCarrier: faker.company.companyName(),
        eoExpirationDate: new Date(),
        eoPerClaimAmount: Number(_.random(0, 10000)),
        eoPerAggregateAmount: Number(_.random(0, 10000)),
        eoInsurance: Number(_.random(0, 10000)),
        // ACH Info
        bankName: faker.company.companyName(),
        accountNumber: _.random(1000000, 9999999),
        routing: _.random(100000, 999999),
        accountType: 'Bank account',
        // Other certification info
        yearsLicensed: _.random(1,5),
        primaryState: 'OH',
        certifiedRelocationProfession: _.random(0,1),
        va: _.random(0,1)
      });
    }
    return res.json(response);
  }
};