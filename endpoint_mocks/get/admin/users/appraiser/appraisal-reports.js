/**
 * Appraisal reports
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/reports',
  callback: function (req, res) {
    var response = [], i;
    for (i = 1; i < 3; i = i + 1) {
      response.push({
        id: 1,
        link: 'http://www.analysis.im/uploads/seminar/pdf-sample.pdf',
        name: 'Report ' + i
      });
    }
    return res.json(response);
  }
};