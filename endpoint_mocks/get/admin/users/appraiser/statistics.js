/**
 * Appraiser statistics
 * @type {{path: string, callback: Function}}
 */
var generator = require('../../../../generator/generator'),
_ = require('lodash');

var prevMonth, nextMonth, prevYear, nextYear, response = [], date;

// Set months
prevMonth = 0;
nextMonth = 1;
prevYear = 2014;
nextYear = 2014;

/**
 * Iterate months
 */
var iterateDate = function () {
  prevMonth = prevMonth + 1;
  nextMonth = nextMonth + 1;
  // If exceeding months, iterate year
  if (prevMonth === 12) {
    prevYear = prevYear + 1;
    prevMonth = 0;
  }
  if (nextMonth === 12) {
    nextYear = nextYear + 1;
    nextMonth = 0;
  }
};

/**
 * Random integer
 * @returns {*}
 */
var intStat = function () {
  return _.random(0, 1000);
};

/**
 * Random float between 0.0 and 100.0 or 1000.0
 * @returns {*}
 */
var floatStat = function (thousand) {
  var endVal = thousand ? 1000 : 100;
  return _.random(0, endVal, true);
};

var record = {};
// Types of stats
var statType = {
  // Total number of completed appraisals - Int
  totalCompletedAppraisals: intStat,
  // Percent completed on time - Float (0-100)
  onTimePercent: floatStat,
  // Acceptance to completion days - Float
  acceptanceToCompletionDays: floatStat,
  // Assigned to accepted days - Float
  assignedToCompletedDays: floatStat,
  // Inspection to completion days - Float
  inspectionToCompletionDays: floatStat,
  // Acceptance to upload days
  acceptanceToUploadDays: floatStat,
  // Inspection to upload days
  inspectionToUploadDays: floatStat,
  // Total number of reviewer revisions - Int
  totalReviewerRevisions: intStat,
  // Total number of client revisions - Int
  totalClientRevisions: intStat,
  // Total number of reconsideration requests - Int
  totalReconsiderationRequests: intStat,
  // Reviewer revision rate - Float (0-100)
  reviewerRevisionRate: floatStat,
  // Client revision rate - Float (0-100)
  clientRevisionRate: floatStat,
  // Revision TAT - Int
  revisionTat: intStat
};

module.exports = {
  path: '/v2.0/users/appraiser/:appraiserId/statistics',
  callback: function (req, res) {
    var i, prop;
    // If we have a response, just return that
    if (response.length) {
      return res.json(response);
    }
    // Create a set of 15 dates
    for (i = 0; i < 15; i = i + 1) {
      record = {};
      record.id = i;
      // Create date
      record.date = generator.date.between(new Date(prevYear, prevMonth), new Date(nextYear, nextMonth));
      // Generate fake values for each of the items needed for stats
      for (prop in statType) {
        // Totally unnecessary, but phpstorm is complaining
        if (!statType.hasOwnProperty(prop)) {
          return;
        }
        record[prop] = statType[prop]();
      }
      // Push onto complete record set
      response.push(record);
      iterateDate();
    }
    return res.json(response);
  }
};