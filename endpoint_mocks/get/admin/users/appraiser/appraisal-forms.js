var faker = require('faker');
var _ = require('lodash');
/**
 * Appraisal forms
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/appraisal-forms',
  callback: function (req, res) {
    var response = [], i;
    for (i = 1; i < 11; i = i + 1) {
      response.push({
        id: i,
        title: faker.hacker.phrase(),
        selected: _.random(0,1),
        fee: Number(_.random(0, 1000, true).toFixed(2)),
        requestedFee: Number(_.random(0, 1000, true).toFixed(2))
      });
    }
    return res.json(response);
  }
};