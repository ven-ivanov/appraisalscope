/**
 * Additional docs
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/additional-docs',
  callback: function (req, res) {
    var response = [], i;
    for (i = 1; i < 7; i = i + 1) {
      response.push({
        id: i,
        date: new Date(),
        docType: 'Type ' + i,
        file: 'http://www.analysis.im/uploads/seminar/pdf-sample.pdf',
        fileName: 'File ' + i
      });
    }
    return res.json(response);
  }
};