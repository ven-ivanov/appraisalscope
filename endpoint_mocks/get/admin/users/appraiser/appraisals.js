var faker = require('faker');
var _ = require('lodash');
/**
 * Pending orders
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/appraisals/:type',
  callback: function (req, res) {
    var response = [], i;
    for (i = 1; i < 11; i = i + 1) {
      response.push({
        id: i,
        file: 'XY' + _.random(1000, 9999),
        borrower: faker.name.findName(),
        address: faker.address.streetAddress(),
        city: faker.address.city(),
        state: faker.address.stateAbbr(),
        client: faker.company.companyName(),
        status: _.random(1, 7),
        dateCompleted: new Date(),
        appFee: Number(_.random(0, 1000, true).toFixed()),
        jobType: _.random(1, 3)
      });
    }
    return res.json(response);
  }
};