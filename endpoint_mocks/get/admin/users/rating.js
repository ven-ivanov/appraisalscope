var faker = require('faker');
var _ = require('lodash');
/**
 * Appraiser quality info
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/quality',
  callback: function (req, res) {
    var response = [], i;
    for (i = 1; i <= 5; i = i + 1) {
      response.push({
        id: i,
        borrowerName: faker.name.findName(),
        streetAddress: faker.address.streetAddress(),
        dateOrdered: new Date(),
        dateCompleted: new Date(),
        quality: _.random(0, 5),
        service: _.random(0, 5),
        turnAround: _.random(0, 5),
        customerRating: _.random(0, 5),
        cuScore: _.random(0, 5)
      });
    }
    return res.json(response);
  }
};