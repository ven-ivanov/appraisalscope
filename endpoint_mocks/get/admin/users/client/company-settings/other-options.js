var _ = require('lodash');

/**
 * Other company settings
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/settings/other',
  callback: function (req, res) {
    return res.json([
      {
        option: 'mergeDocuments', value: _.random(1, 2)
      },
      {
        option: 'mergedDocPlacement', value: _.random(1, 2)
      },
      {
        option: 'setPlZero', value: _.random(1, 2)
      },
      {
        option: 'lockEmailOffice', value: _.random(1, 2)
      },
      {
        option: 'sendDocumentAs', value: _.random(1, 2)
      },
      {
        option: 'adminAs', value: _.random(1, 2)
      },
      {
        option: 'clientAs', value: _.random(1, 2)
      },
      {
        option: 'minEoCoverageAmount', value: Number(_.random(0, 100, true).toFixed(2))
      },
      {
        option: 'setPlZeroAmount', value: Number(_.random(0, 100, true).toFixed(2))
      }
    ]);
  }
};