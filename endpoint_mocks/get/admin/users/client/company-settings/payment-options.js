var _ = require('lodash');
/**
 * Get payment options
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/settings/payment-options',
  callback: function (req, res) {
    var response = [
      {
        id: 'cc',
        name: 'Credit Card',
        enabled: _.random(0,3),
        // Credit card options
        options: [
          {
            id: 1,
            name: 'Authorize Only'
          },
          {
            id: 2,
            name: 'Capture'
          },
          {
            id: 3,
            name: 'Save payment to capture later'
          },
          {
            id: 4,
            name: 'Show capture on client',
            enabled: _.random(0, 1)
          }
        ]
      },
      {
        id: 'bankAccount',
        name: 'Bank account',
        enabled: _.random(0, 1)
      },
      {
        id: 'sendPaymentRequest',
        name: 'Send payment request to customer',
        enabled: _.random(0, 1)
      },
      {
        id: 'billMe',
        name: 'Bill me',
        enabled: _.random(0, 1)
      },
      {
        id: 'splitPayment',
        name: 'Split payment',
        enabled: _.random(0, 1)
      },
      {
        id: 'invoicePartialPayment',
        name: 'Invoice partial payment',
        enabled: _.random(0, 1)
      }
    ];
    return res.json(response);
  }
};