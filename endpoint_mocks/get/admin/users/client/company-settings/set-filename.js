module.exports = {
  path: '/v2.0/users/client/:clientId/settings/set-filename',
  callback: function (req, res) {
    return res.json([
      {
        id: 'lastName',
        name: 'Borrower last name'
      },
      {
        id: 'firstName',
        name: 'Borrower first name'
      },
      {
        id: 'fileNo',
        name: 'File number'
      },
      {
        id: 'loanNumber',
        name: 'Loan number'
      },
      {
        id: 'address1',
        name: 'Address 1'
      },
      {
        id: 'orderDate',
        name: 'Order date'
      },
      {
        id: 'completedDate',
        name: 'Completed date'
      }
    ]);
  }
};