var _ = require('lodash');
/**
 * Merged document templates
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/settings/merged-document-templates',
  callback: function (req, res) {
    return res.json([
      {
        id: 'invoice',
        name: 'Invoice',
        enabled: _.random(0, 1)
      },
      {
        id: 'testDocFormat',
        name: 'Test doc format',
        enabled: _.random(0, 1)
      },
      {
        id: 'complianceCertificate',
        name: 'ComplianceCertificate',
        enabled: _.random(0, 1)
      },
      {
        id: 'air',
        name: 'AIR',
        enabled: _.random(0, 1)
      },
      {
        id: 'testIt',
        name: 'Test it',
        enabled: _.random(0, 1)
      },
      {
        id: 'residential',
        name: 'Residential',
        enabled: _.random(0, 1)
      },
      {
        id: 'testingEditor',
        name: 'Testing editor',
        enabled: _.random(0, 1)
      },
      {
        id: 'airCert',
        name: 'AIR cert',
        enabled: _.random(0, 1)
      },
      {
        id: 'propertyAddressTags',
        name: 'Property address tags',
        enabled: _.random(0, 1)
      }
    ]);
  }
};