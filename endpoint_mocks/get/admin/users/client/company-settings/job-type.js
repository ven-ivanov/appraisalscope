var _ = require('lodash');
/**
 * Get job types
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/settings/job-types',
  callback: function (req, res) {
    var response = [];
    var i;
    for (i = 1; i <= 10; i = i + 1) {
      response.push({
        id: i,
        name: 'Job type ' + i,
        percentageFlat: _.random(0,1),
        feeMargin: Number(_.random(0, 100, true).toFixed(2))
      });
    }
    return res.json(response);
  }
};