var _ = require('lodash');
/**
 * Third party integrations for client company settings
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/settings/integrations',
  callback: function (req, res) {
    return res.json([
      {
        id: 'blitzdoc',
        name: 'Blitzdoc sync',
        enabled: _.random(0, 1)
      },
      {
        id: 'uwm',
        name: 'UWM sync',
        enabled: _.random(0, 1)
      },
      {
        id: 'mercury',
        name: 'Mercury Network interaction',
        enabled: _.random(0, 1)
      },
      {
        id: 'fnc',
        name: 'FNC system interaction',
        enabled: _.random(0, 1)
      },
      {
        id: 'realEc',
        name: 'RealEC system interaction',
        enabled: _.random(0, 1)
      },
      {
        id: 'lendingQb',
        name: 'LendingQB system interaction',
        enabled: _.random(0, 1)
      },
      {
        id: 'fics',
        name: 'FICS system interaction',
        enabled: _.random(0, 1)
      },
      {
        id: 'nbss',
        name: 'NBSS integration',
        enabled: _.random(0, 1)
      },
      {
        id: 'pdsOptiVal',
        name: 'PDS OptiVal',
        enabled: _.random(0, 1)
      },
      {
        id: 'pdsRealViewWithData',
        name: 'PDS RealView (with data)',
        enabled: _.random(0, 1)
      },
      {
        id: 'pdsRealViewWithoutData',
        name: 'PDS RealView (without data)',
        enabled: _.random(0, 1)
      }
    ]);
  }
};