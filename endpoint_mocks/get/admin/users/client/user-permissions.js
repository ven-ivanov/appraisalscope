var _ = require('lodash'),
faker = require('faker');

/**
 * User permissions
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/users/client/:clientId/permissions/user-permissions-permissions/',
  callback: function (req, res) {
    var response = [];
    var i;
    // Make 5
    for (i = 1; i <= 5; i = i + 1) {
      response.push({
        id: i,
        name: faker.name.findName()
      });
    }
    return res.json(response);
  }
};