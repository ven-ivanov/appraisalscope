var _ = require('lodash');
var faker = require('faker');

// Possible company types
var companyTypes = ['attorney', 'bank', 'correspondent lender', 'credit union', 'home owner', 'lender',
                    'mortgage broker', 'real estate agent', 'wholesale lender', 'other'];
/**
 * Generate users and companies for client sub-view
 */
module.exports = {
  path: '/v2.0/users/client',
  callback: function (req, res) {
    var response = [], i, firstName, lastName, users = [], usersSplit, splitLength, branch = '';
    // Generate 5 companies
    for (i = 1; i <= 5; i = i + 1) {
      firstName = faker.name.firstName();
      lastName = faker.name.lastName();
      response.push({
        id: i,
        companyType: _.sample(companyTypes),
        contactFirstName: firstName,
        contactLastName: lastName,
        companyName: faker.company.companyName(),
        email: firstName + '@' + lastName + '.com',
        created: new Date(),
        address: faker.address.streetAddress(),
        address2: '',
        city: faker.address.city(),
        state: faker.address.state(),
        zip: faker.address.zipCode(),
        country: faker.address.country(),
        phone: faker.phone.phoneNumber(),
        fax: faker.phone.phoneNumber(),
        company: 0,
        firstName: '',
        lastName: '',
        levelId: 0,
        type: 'Company',
        accountType: 'Mortgage broker',
        additionalDetails: 'I love bananas',
        ordersPerMonth: _.random(10, 1000),
        lenderName: faker.company.companyName(),
        businessUnit: _.random(1, 100),
        fannieMaeSn: _.random(1, 99999999),
        freddieMacId: _.random(1, 99999999),
        branches: [
          {
            id: (i - 1) * 2 + 1,
            value: 'branch ' + (i - 1) * 2 + 1
          }, {
            id: ((i - 1) * 2) + 2,
            value: 'branch ' + ((i - 1) * 2) + 2
          }]
      });
    }
    // Generate 20 users
    for (i = 6; i < 26; i = i + 1) {
      firstName = faker.name.firstName();
      lastName = faker.name.lastName();
      users.push({
        id: i,
        email: firstName + '@' + lastName + '.com',
        created: new Date(),
        phone: faker.phone.phoneNumber(),
        fax: faker.phone.phoneNumber(),
        firstName: firstName,
        lastName: lastName,
        levelId: 0,
        additionalDetails: 'I love bananas',
        ordersPerMonth: _.random(10, 1000),
        accountType: 'Mortgage broker',
        type: 'User',
        userType: _.sample(['processor', 'manager', 'loan officer']),
        username: 'user' + i,
        branchPermission: 'Not assigned',
        branch: _.random(0,1),
        active: true
      });
    }

    // Split users in 5 groups
    usersSplit = _.chunk(users, 4);
    splitLength = usersSplit.length;
    // Place each user into a company
    for (i = 0; i < splitLength; i = i + 1) {
      _.forEach(usersSplit[i], function (user) {
        user.companyName = response[i].companyName;
        user.address = response[i].address;
        user.address2 = response[i].address2;
        user.city = response[i].city;
        user.state = response[i].state;
        user.zip = response[i].zip;
        user.country = response[i].country;
        user.company = response[i].id;
        user.contactFirstName = response[i].contactFirstName;
        user.contactLastName = response[i].contactLastName;
        if (response[i] === 1) {
          user.branch = _.sample(['branch1', 'branch2']);
        }
        // Add to response
        response.push(user);
      })
    }


    return res.json(response);
  }
};