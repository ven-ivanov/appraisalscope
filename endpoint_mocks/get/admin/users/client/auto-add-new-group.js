var _ = require('lodash');
module.exports = {
  path: '/v2.0/admin/users/client/:clientId/permissions/auto-add-new-group',
  callback: function (req, res) {
    return res.json({
      status: _.random(0, 1)
    });
  }
};