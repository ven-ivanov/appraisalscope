/**
 * Retrieve download link
 */
module.exports = {
  path: '/v2.0/admin/users/client/document/download/:id',
  callback: function (req, res) {
    return res.status(500).send('');
  }
};