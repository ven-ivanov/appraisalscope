/**
 * Client branches
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/:id/branches/',
  callback: function (req, res) {
    return res.json([
      {id: 1, branch: 'Main', enabled: true},
      {id: 2, branch: 'Austin', enabled: true},
      {id: 3, branch: 'Cincinati', enabled: true},
      {id: 4, branch: 'Alaska', enabled: true},
      {id: 5, branch: 'Papua New Guinea', enabled: true}
    ]);
  }
};
/*
 var response = [];
 var i;
 for (i = 1; i < 6; i = i + 1) {
 response.push({
 id: i,
 name: faker.company.companyName(),
 isActive: _.random(0,1),
 location: {
 "address1": faker.address.streetAddress(),
 "address2": '',
 "city": faker.address.city(),
 "state": {
 "id": 1,
 "code": faker.address.stateAbbr(),
 "name": faker.address.state()
 }
 },
 "valid": _.random(0,1),
 "geo": {
 "latitude": _.random(10000, 99999),
 "longitude": _.random(10000, 99999)
 }
 });
 }
 */