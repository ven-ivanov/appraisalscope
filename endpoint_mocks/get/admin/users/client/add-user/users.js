/**
 * Users on load
 * @type {exports}
 * @private
 */
var _ = require('lodash');
var faker = require('faker');

module.exports = {
  path: '/v2.0/users/client/:id/users',
  callback: function (req, res) {
    var response = [];
    var i;
    for (i = 6; i < _.random(16,36); i = i + 1) {
      response.push({
        id: i,
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        username: 'User' + i,
        phone: faker.phone.phoneNumber(),
        userType: _.random(1,3),
        branch: _.random(1,5)
      });
    }
    return res.json(response);
  }
};