/**
 * User types
 */
module.exports = {
  path: '/v2.0/users/user-type/:id',
  callback: function (req, res) {
    return res.json([
      {
        id: 1,
        type: 'Processor'
      },
      {
        id: 2,
        type: 'Manager'
      },
      {
        id: 3,
        type: 'Loan Officer'
      }
    ]);
  }
};