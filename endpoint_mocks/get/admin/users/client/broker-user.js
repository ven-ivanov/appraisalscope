var _ = require('lodash'),
faker = require('faker');

module.exports = {
  path: '/v2.0/admin/users/client/:clientId/permissions/user-permissions-broker-user',
  callback: function (req, res) {
    var response = [];
    var i;
    for (i = 1; i <= 5; i = i + 1) {
      response.push({
        id: i,
        name: faker.name.findName() + '(' + faker.company.companyName() + ')',
        active: _.random(0,1)
      });
    }
    return res.json(response);
  }
};