var _ = require('lodash');
/**
 * General user setings
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/client/:userId/general',
  callback: function (req, res) {
    return res.json([
      {
        // Lock email permissions
        id: 1,
        name: 'lockEmailPermissions',
        active: _.random(0,1)
      },
      // Lock documents
      {
        id: 2,
        name: 'lockDocuments',
        active: _.random(0,1)
      },
      // Lock new appraisal request
      {
        id: 3,
        name: 'lockNewAppraisalRequest',
        active: _.random(0,1)
      },
      // Instructions
      {
        id: 4,
        name: 'instructions',
        active: _.random(1,3)
      },
      // Approved appraiser
      {
        id: 5,
        name: 'approveAppraiser',
        active: _.random(1,3)
      },
      // Checklist
      {
        id: 6,
        name: 'checklist',
        active: _.random(1,3)
      },
      // Client on report
      {
        id: 7,
        name: 'clientOnReport',
        active: _.random(1,3)
      },
      // Fee schedule
      {
        id: 8,
        name: 'feeSchedule',
        active: _.random(1,3)
      },
      // Payment form
      {
        id: 9,
        name: 'paymentForm',
        active: _.random(1,4)
      },
    ]);
  }
};