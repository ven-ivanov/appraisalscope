var _ = require('lodash');

module.exports = {
  path: '/v2.0/users/client/:userId/email-notification-permissions',
  callback: function (req, res) {
    return res.json([
      {
        id: 1,
        name: 'Inspection scheduled',
        active: Number(_.random(0,1))
      },
      {
        id: 2,
        name: 'Inspection complete',
        active: Number(_.random(0,1))
      },
      {
        id: 3,
        name: 'On hold',
        active: Number(_.random(0,1))
      },
      {
        id: 4,
        name: 'Additional status',
        active: Number(_.random(0,1))
      },
      {
        id: 5,
        name: 'Completed appraisal',
        active: Number(_.random(0,1))
      },
      {
        id: 6,
        name: 'In review',
        active: Number(_.random(0,1))
      },
      {
        id: 7,
        name: 'Accepted',
        active: Number(_.random(0,1))
      },
      {
        id: 8,
        name: 'Email office',
        active: Number(_.random(0,1))
      },
      {
        id: 9,
        name: 'Declined',
        active: Number(_.random(0,1))
      },
      {
        id: 10,
        name: 'Assigned',
        active: Number(_.random(0,1))
      },
      {
        id: 11,
        name: 'Emailed documents',
        active: Number(_.random(0,1))
      },
      {
        id: 12,
        name: 'Accepted with conditions',
        active: Number(_.random(0,1))
      },
      {
        id: 13,
        name: 'New order confirmation',
        active: Number(_.random(0,1))
      }
    ]);
  }
};