/**
 * Get disallowed appraisers for admin users - client - user settings
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/users/client/user-settings/disallowed-appraisers/client/:clientId/user/:userId',
  callback: function (req, res) {
    return res.json([
      {
        id: 1,
        name: 'Bad Appraiser'
      },
      {
        id: 2,
        name: 'Worse Appraiser'
      }
    ]);
  }
};