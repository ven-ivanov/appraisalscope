var _ = require('lodash'),
    faker = require('faker');

/**
 * Checklist
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/users/client/user-settings/client-on-report/client/:clientId/user/:userId',
  callback: function (req, res) {
    var response = [];
    var i;
    // Make 10
    for (i = 1; i <= 10; i = i + 1) {
      response.push({
        id: i,
        name: faker.company.companyName()
      });
    }
    return res.json(response);
  }
};