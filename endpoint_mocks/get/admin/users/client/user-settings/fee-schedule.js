var _ = require('lodash'),
    faker = require('faker');

module.exports = {
  path: '/v2.0/admin/users/client/user-settings/fee-schedule/client/:clientId/user/:userId',
  callback: function (req, res) {
    var response = [];
    var i;
    // Create 10 entries
    for (i = 1; i <= 10; i = i + 1) {
      response.push({
        id: i,
        jobTitle: faker.name.firstName() + ' Job',
        FHA: _.sample(['Yes', 'No'])
      });
    }
    return res.json(response);
  }
};