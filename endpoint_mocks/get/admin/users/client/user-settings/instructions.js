module.exports = {
  path: '/v2.0/admin/users/client/user-settings/user-instructions/client/:clientId/user/:id',
  callback: function (req, res) {
    return res.json([
      {
        id: 1,
        title: 'Title 1',
        instruction: 'Instruction 1'
      },
      {
        id: 2,
        title: 'Title 2',
        instruction: 'Instruction 2'
      },
      {
        id: 3,
        title: 'Title 3',
        instruction: 'Instruction 3'
      }
    ]);
  }
};