var _ = require('lodash');
/**
 * Payment types
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/users/client/user-settings/payment-options/client/:clientId/user/:userId',
  callback: function (req, res) {
    // Return payment types
    return res.json({
      // Credit card
      cc: {
        authorizeOnly: _.random(0, 1),
        capture: _.random(0, 1)
      },
      showCapture: _.random(0, 1),
      bankAccount: _.random(0, 1),
      sendInvoice: _.random(0, 1),
      billMe: _.random(0, 1),
      splitPayment: _.random(0, 1),
      invoicePartial: _.random(0, 1)
    });
  }
};