/**
 * Get user mail permissions
 * @type {exports}
 * @private
 */
var _ = require('lodash'),
faker = require('faker');

module.exports = {
  path: '/v2.0/admin/users/client/:clientId/permissions/broker-permissions-mail',
  callback: function (req, res) {
    var response = [];
    var i;
    // Create some fake users. Or not.
    for (i = 1; i < 4; i = i + 1) {
      response.push({
        name: faker.company.companyName(),
        id: i,
        data: {
          inspectionScheduled: _.random(0,1),
          inspectionComplete: _.random(0,1),
          onHold: _.random(0,1),
          additionalStatus: _.random(0,1),
          completedAppraisal: _.random(0,1),
          inReview: _.random(0,1),
          accepted: _.random(0,1),
          emailOffice: _.random(0,1),
          declined: _.random(0,1),
          assigned: _.random(0,1),
          emailedDocuments: _.random(0,1),
          acceptedWithConditions: _.random(0,1),
          newOrderConfirmation: _.random(0,1)
        }
      });
    }
    return res.json(response);
  }
};