/**
 * Generate result set, which can be manipulated and will cache between requests
 * @type {generators|exports}
 */
var generator = require('../../../../generator/generator'),
    _ = require('lodash');

var i, response, user, firstName, lastName, cachedResults;

// Number of users to return
var usersRequested = 60;

/**
 * Generate a user or company
 */
var generateUser = function (i, company) {
  var branch = '';
  firstName = generator.firstName();
  lastName = generator.lastName();
  // Set branch for first company
  if (company.id === 1) {
    branch = _.sample(['branch1', 'branch2']);
  }
  return {
    id: i,
    type: 'user',
    // Get a company
    companyName: company.companyName,
    contact: company.contact,
    companyType: company.companyType,
    email: firstName.toLowerCase() + '@' + lastName.toLowerCase() + '.com',
    phone: generator.phone(),
    fax: generator.phone(),
    created: generator.date.between(new Date(2012, 0, 1), new Date()),
    address: generator.streetAddress(),
    address2: '',
    city: generator.address.city(),
    state: generator.state(),
    zip: generator.address.zipUS(),
    // If it's a company, set parent to 0. Otherwise, attach to a company
    company: company.id,
    firstName: firstName,
    lastName: lastName,
    accountType: 'Mortgage Broker',
    ordersPerMonth: _.random(0, 1000),
    // Company is 0, others are 1
    userType: _.sample(['processor', 'manager', 'loan officer']),
    username: 'user' + i,
    branch: branch,
    branchPermission: 'Not assigned',
    active: true
  };
};

/**
 * Generate companies. Note that the first company will always be the same for unit testing
 */
var generateCompanies = function () {
  // Array of companies
  var companies = [
    {
      id: 1,
      type: 'company',
      company: 0,
      companyName: "Walker Bank",
      contact: "David Bass",
      companyType: "attorney",
      email: "jessica@johnson.com",
      phone: '891-716-9865',
      fax: '891-156-7217',
      created: "2013-11-02T04:08:33.200Z",
      address: "7629 Baxter Drive",
      address2: "Suite 2",
      city: 'Columbia',
      state: 'Oklahoma',
      zip: '83825',
      country: _.sample(['United States', 'Canada']),
      additionalDetails: 'I like bananas',
      firstName: "Jessica",
      lastName: "Johnson",
      levelId: 0,
      accountType: 'Mortgage Broker',
      ordersPerMonth: 6282,
      branches: [{
                   value: 'branch1'
                 }, {
                   value: 'branch2'
                 }]
    }
  ];

  // Possible company types
  var companyTypes = ['attorney', 'bank', 'correspondent lender', 'credit union', 'home owner', 'lender',
                      'mortgage broker', 'real estate agent', 'wholesale lender', 'other'];

  var i,
  totalCompanies = 20,
  firstName, lastName;

  // Generate companies, leaving us with 20 total
  for (i = 1; i < totalCompanies; i = i + 1) {
    firstName = generator.firstName();
    lastName = generator.lastName();
    // Create a company
    var company = {
      id: i + 1,
      type: 'company',
      companyName: lastName + ' ' + _.sample(['Co', 'Inc', 'LLC']),
      contact: {
        firstName: firstName,
        lastName: lastName
      },
      // We want two companies of each type
      companyType: companyTypes[Math.floor(i/2)],
      email: firstName.toLowerCase() + '@' + lastName.toLowerCase() + '.com',
      phone: generator.phone(),
      fax: generator.phone(),
      created: generator.date.between(new Date(2012, 0, 1), new Date()),
      address: _.random(111, 9999) + ' ' + generator.lastName() + ' Drive',
      address2: '',
      city: generator.address.city(),
      state: generator.state(),
      zip: generator.address.zipUS(),
      country: _.sample(['United States', 'Canada']),
      additionalDetails: 'I like bananas',
      company: 0,
      firstName: firstName,
      lastName: lastName,
      levelId: 0,
      accountType: 'Mortgage Broker',
      ordersPerMonth: _.random(0, 1000),
      lenderName: generator.lastName() + ' Lending, Inc',
      businessUnit: _.random(1, 100),
      fannieMaeSn: _.random(1, 99999999),
      freddieMacId: _.random(1, 99999999)
    };
    // Concat to array of companies
    companies.push(company);
  }
  return companies;
};

/**
 * Search in client view
 */
module.exports = {
  //path: /\/v2\.0\/admin\/users\/client\/([^/-]*)\/([a-zA-z]*)\/?(\d*)/,
  path: '/v2.0/users/clientOld',
  callback: function (req, res) {
    var reqParams = [];
    // Get parameters
    _.forEach(req.params, function (requestVal) {
      reqParams.push(requestVal);
    });
    // Filter unused parameters
    reqParams = reqParams.filter(function (param) {
      return param;
    });
    var requestedType = reqParams.length === 1 ? req.params[0].toLowerCase() : 'all accounts';
    /**
     * Init data set
     * On first load, we want to create a set of 300 users
     */
    if (!response) {
      // Generate companies (the first one is not generated)
      var companies = generateCompanies();
      var userCompany;
      response = companies.slice();
      /**
       * Generate users for remaining companies
       */
      for (i = companies.length; i <= companies.length + usersRequested; i = i + 1) {
        userCompany = _.sample(companies);
        // Create first 3 records as companies
        user = generateUser(i, userCompany);
        _.forEach(user.company, function (companyItem, key) {
          // Don't overwrite parent user ID
          //if (key !== 'company') {
            user[key] = companyItem;
          //}
        });
        //delete user.company;
        response = response.concat(user);
      }
      // Cache the result set
      cachedResults = JSON.parse(JSON.stringify(response));
    }
    /**
    * Limit result set to account type, if necessary
    */
    // Create copy to avoid messing with the "db"
    response = JSON.parse(JSON.stringify(cachedResults));
    // If selecting a specific type
    if (!_.isUndefined(requestedType) && requestedType !== 'all accounts') {
      var keptCompanies = [];
      // Separate users and companies
      companies = response.splice(0, 20);
      // Only return companies of the requested type
      companies = companies.map(function (company) {
        if (company.companyType !== requestedType) {
          return null;
        }
        // Keep an array of company ids
        keptCompanies.push(company.id);
        return company;
      }).filter(function (company) {
        return company;
      });
      // Get users that are in the companies remaining
      response = response.map(function (user) {
        // If this user belongs to a company we're using
        if (keptCompanies.indexOf(user.company) !== -1) {
          return user;
        }
        return null;
      }).filter(function (user) {
        return user;
      });
      // And only return users that belong to those companies
      response = companies.concat(response);
    }
    /**
     * If returning a specific user or company
     */
    // Get request ID
    var reqId = parseInt(req.params[1]);
    // If searching for a user or company
    if (reqParams.length === 2 && req.params[0].match(/user|company/) && _.isNumber(reqId)) {
      switch (reqParams[0]) {
        case 'user':
          response = response.slice(reqId, reqId + 1);
          break;
        case 'company':
          response = response.slice(reqId - 1, reqId);
          break;
        default:
          throw Error('The valid request types are "user" and "company"');
      }
    }
    res.json(response);
  }
};