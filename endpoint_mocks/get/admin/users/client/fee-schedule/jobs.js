var _ = require('lodash');
/**
 * Fee schedule jobs
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/job/:jobId/fee-schedule',
  callback: function (req, res) {
    var i, response = [];
    for (i = 1; i <= 10; i = i + 1) {
      response.push({
        id: i,
        title: 'Job Title ' + i,
        fha: _.random(0,1)
      });
    }
    return res.json(response);
  }
};