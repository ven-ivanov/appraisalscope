var _ = require('lodash');
/**
 * Retrieve zips for a particular county
 *
 * This will take filters for specific counties
 */
module.exports = {
  path: '/v2.0/location/zips',
  callback: function (req, res) {
    var response = {data: []}, i;
    for (i = 1; i < 100; i = i + 1) {
      response.data.push({
        id: i,
        zip: _.random(10000, 99999)
      });
    }
    return res.json(response);
  }
};