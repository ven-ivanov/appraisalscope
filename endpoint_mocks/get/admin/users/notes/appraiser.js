var _ = require('lodash'),
faker = require('faker'),
notes = require('../../../../standardObjects/admin/notes/notes');

/**
 * Generate notes
 * @type {{path: string, callback: Function}}
 *
 * Type is company or user
 */
module.exports = {
  path: '/v2.0/appraisement/:type/:appraiserId/notes',
  callback: function (req, res) {
    var i;
    var response = {data: []};
    for (i = 1; i <= 6; i = i + 1) {
      response.data.push(notes.appraiserNote(i));
    }
    return res.json(response);
  }
};