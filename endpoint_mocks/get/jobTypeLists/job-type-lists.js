var Client = require('../../standardObjects/client/clientObjects');
/**
 * Retrieve all job type lists
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/JobTypeLists/Index.md#get-job-type-lists-unimplemented
 * 8/7
 */
module.exports = {
  path: '/v2.0/job-type-lists',
  callback: function (req, res) {
    var response = {data: []}, i;

    for (i = 1; i < 10; i = i + 1) {
      response.data.push(Client.jobTypeList(i));
    }

    return res.json(response);
  }
};