var appraisersList = require('../../standardObjects/admin/appraisersList/objects');

/**
 * Get all do not use appraiser lists
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/AppraiserLists/DoNotUse.md#get-appraiser-listsdo-not-use
 * 8/4
 */
module.exports = {
  path: '/v2.0/appraiser-lists/do-not-use',
  callback: function (req, res) {
    var response = {data: []}, i;

    for (i = 1; i < 10; i = i + 1) {
      response.data.push(appraisersList.appraisersList(i, 'do-not-use'));
    }

    return res.json(response);
  }
};