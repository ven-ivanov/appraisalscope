var appraisersList = require('../../standardObjects/admin/appraisersList/objects');

/**
 * Get all approved appraiser lists
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/AppraiserLists/Approved.md#get-clientcompaniescompanyidbranchesbranchidemployeesemployeeidappraiser-listsapproved-unimplemented
 * 8/4
 */
module.exports = {
  path: '/v2.0/appraiser-lists/approved',
  callback: function (req, res) {
    var response = {data: []}, i;

    for (i = 1; i < 10; i = i + 1) {
      response.data.push(appraisersList.appraisersList(i, 'approved'));
    }

    return res.json(response);
  }
};