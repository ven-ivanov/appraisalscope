var General = require('../../standardObjects/generalObjects');
module.exports = {
  path: '/v2.0/platform/merge-fields',
  callback: function (req, res) {
    var response = {data: []}, i;

    for (i = 1; i < 10; i = i + 1) {
      response.data.push(General.mergeFields());
    }

    return res.json(response);
  }
};