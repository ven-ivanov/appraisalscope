var General = require('../../standardObjects/generalObjects');
/**
 * Get instruction templates
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Platform/InstructionTemplates/Index.md#get-platforminstruction-templates-unimplemented
 */
module.exports = {
  path: '/v2.0/platform/instruction-templates',
  callback: function (req, res) {
    var response = {data: []}, i;

    for (i = 1; i < 10; i = i + 1) {
      response.data.push(General.instructionTemplate(i));
    }

    return res.json(response);
  }
};