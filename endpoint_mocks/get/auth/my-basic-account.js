var g = require('../../generator/generator');

/**
 * Retrieve account information from mock API endpoint
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
var determineUser = function (req, res, next) {
  // Response
  var resBody;

  switch (req.headers.token) {
    case 'admin':
      resBody = {
        "id": 2,
        "firstName": "Jordan",
        "lastName": "Rothstein",
        "type": 3,
        "status": "0",
        "staffPermissions": null,
        "level": 0,
        "token": "admin",
        "typeName": "admin",
        "asAppraiser": {},
        "asClient": {}
      };
      break;
    case 'appraiser':
      resBody = {
        "id": 4,
        "firstName": "sunil",
        "lastName": "kumar",
        "type": 1,
        "status": "1",
        "staffPermissions": null,
        "level": 1,
        "token": "appraiser",
        "typeName": "appraiser",
        "asAppraiser": {},
        "asClient": {}
      };
      break;
    case 'client':
      resBody = {
        "id": 103823,
        "firstName": "Saa",
        "lastName": "dd",
        "type": 2,
        "status": "0",
        "staffPermissions": null,
        "level": 1,
        "token": "client",
        "typeName": "client",
        "asAppraiser": {},
        "asClient": {}
      };
      break;
    default:
      resBody = {};
      break;
  }
  return res.json(resBody);
};

module.exports = {
  path: '/v2.0/auth/my-basic-account',
  callback: determineUser
};