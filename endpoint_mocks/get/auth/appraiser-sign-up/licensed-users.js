var adminObjects = require('../../../standardObjects/admin/objects');

module.exports = {
  path: '/v2.0/auth/appraiser-sign-up/licensed-users',
  callback: function (req, res) {
    return res.json({
      data: [
      	adminObjects.user(),
      	adminObjects.user(),
      	adminObjects.user()
      ]
    });
  }
};
