var f = require('faker');

var createData = function () {
	var arr = [];

	for(var i = 0; i < 30; i++) {
		var obj = {county: f.address.county()};

		arr.push(obj);
	}
	return arr;
};

module.exports = {
  path: '/v2.0/auth/appraiser-sign-up/counties',
  callback: function (req, res) {
    return res.json({
      data: createData()
    });
  }
};
