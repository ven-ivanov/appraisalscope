module.exports = {
  path: '/v2.0/auth/appraiser-sign-up/settings',
  callback: function (req, res) {
    return res.json({
      data: {
      	requireAch: true
      }
    });
  }
};
