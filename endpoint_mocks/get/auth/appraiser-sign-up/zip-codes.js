var _ = require('lodash');

var createData = function (county, state) {
	var arr = [];

	for(var i = 0; i < 30; i++) {
		var obj = {
			id: i,
      zipCode: _.random(10000, 99999),
      county: county,
      state: state
    };

		arr.push(obj);
	}
	return arr;
};

module.exports = {
  path: '/v2.0/auth/appraiser-sign-up/zip-codes/:county?/:state?',
  callback: function (req, res) {
  	console.log(req.params);
    return res.json({
      data: createData(req.params.county, req.params.state)
    });
  }
};
