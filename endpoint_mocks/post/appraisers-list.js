var AppraisersList = require('../standardObjects/admin/appraisersList/objects');
// Add new appraisers list
module.exports = {
  path: '/v2.0/appraiser-lists/:type',
  callback: function (req, res) {
    return res.json(AppraisersList.appraisersList(1000, req.params.type));
  }
};