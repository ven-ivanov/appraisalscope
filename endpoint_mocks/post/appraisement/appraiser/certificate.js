var Appraisement = require('../../../standardObjects/appraisement/appraisementObjects');
/**
 * Create a new appraiser certificate
 */
module.exports = {
  path: '/v2.0/appraisement/appraisers/:appraiserId/certificates',
  callback: function (req, res) {
    return res.json(Appraisement.certificate(1000));
  }
};