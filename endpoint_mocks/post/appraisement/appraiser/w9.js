/**
 * Update appraiser w9
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/W9/Index.md#updates-a-appraisers-user-w9
 */
module.exports = {
  path: '/v2.0/appraisement/appraisers/:appraiserId/w9',
  callback: function (req, res) {
    return res.json();
  }
};