var AppraisementObjects = require('../../../standardObjects/appraisement/appraisementObjects');
// Upload appraiser docs
module.exports = {
  path: '/v2.0/appraisement/appraisers/:appraiserId/documents',
  callback: function (req, res) {
    return res.json(AppraisementObjects.appraiserDocument(1000));
  }
};