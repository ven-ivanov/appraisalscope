/**
 * Send login details to appraiser
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/SendLoginDetails.md#sends-login-details-to-a-appraiser
 * 8/10
 */
module.exports = {
  path: '/v2.0/appraisement/appraisers/:appraiserId/send-login-details',
  callback: function (req, res) {
    return res.json();
  }
};