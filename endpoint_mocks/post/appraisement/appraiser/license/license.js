// New certificate
module.exports = {
  path: '/v2.0/appraisement/appraisers/:appraiserId/certificates/:certificateId',
  callback: function (req, res) {
    return res.json();
  }
};