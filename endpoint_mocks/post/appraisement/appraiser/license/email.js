// Email license doc
module.exports = {
  path: '/v2.0/appraisement/appraisers/:appraiserId/certificates/:certificateId/email',
  callback: function (req, res) {
    return res.json();
  }
};