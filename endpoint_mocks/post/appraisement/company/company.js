var Appraisement = require('../../../standardObjects/appraisement/appraisementObjects');
// New appraisement company
module.exports = {
  path: '/v2.0/appraisement/companies',
  callback: function (req, res) {
    return res.json(Appraisement.appraiserCompany(1000));
  }
};