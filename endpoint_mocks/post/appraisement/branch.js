var AppraisementObjects = require('../../standardObjects/appraisement/appraisementObjects');
// New appraiser company branch
module.exports = {
  path: '/v2.0/appraisement/companies/:companyId/branches/',
  callback: function (req, res) {
    return res.json(AppraisementObjects.appraiserCompanyBranch(1000));
  }
};