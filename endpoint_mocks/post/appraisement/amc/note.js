// Create new AMC note
var Note = require('../../../standardObjects/admin/notes/notes');
module.exports = {
  path: '/v2.0/appraisement/amcs/:amcId/notes',
  callback: function (req, res) {
    return res.json(Note.appraiserNote(1000));
  }
};