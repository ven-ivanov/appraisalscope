var Appraisement = require('../../../standardObjects/appraisement/appraisementObjects');
// Create a new AMC job type
module.exports = {
  path: '/v2.0/appraisement/amcs/:amcId/job-types',
  callback: function (req, res) {
    return res.json(Appraisement.appraiserJobType(1000));
  }
};