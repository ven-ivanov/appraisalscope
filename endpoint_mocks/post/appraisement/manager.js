var AppraisementObjects = require('../../standardObjects/appraisement/appraisementObjects');
// Create new appraiser company manager
module.exports = {
  path: '/v2.0/appraisement/companies/:companyId/branches/:branchId/managers',
  callback: function (req, res) {
    return res.json(AppraisementObjects.appraiserCompanyManager(1000, req.param.branchId));
  }
};