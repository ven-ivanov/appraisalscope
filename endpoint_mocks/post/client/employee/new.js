// Create a new employee
var Client = require('../../../standardObjects/client/clientObjects');
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches/:branchId/employees',
  callback: function (req, res) {
    return res.json(Client.employee(1000, req.params.companyId));
  }
};