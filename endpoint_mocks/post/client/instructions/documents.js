var Objects = require('../../../standardObjects/client/clientObjects');
// Create new instructions document
module.exports = {
  path: /instructions\/documents/,
  callback: function (req, res) {
    return res.json(Objects.instructionDocument(1000));
  }
};