var Client = require('../../../standardObjects/client/clientObjects');
// Create a new branch
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches',
  callback: function (req, res) {
    return res.json(Client.branch(1000, req.params.companyId, null, true));
  }
};