// New checklist question
var objects = require('../../../standardObjects/admin/objects');
module.exports = {
  path: '/v2.0/checklists/:checklistId/questions',
  callback: function (req, res) {
    var question = objects.checklistQuestion(1000, req.body.text);

    return res.json(question);
  }
};