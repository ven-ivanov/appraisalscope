/**
 * Retrieve assigned employee checklist
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/Checklist/Index.md
 */
var objects = require('../../../standardObjects/admin/objects');
module.exports = {
  path: '/v2.0/checklists/:checklistId/clone',
  callback: function (req, res) {
    // Clone a checklist
    var checklist = objects.checklist(req.params.checklistId, req.body.title);
    // Change ID
    checklist.id = 1000;

    return res.json(checklist);
  }
};