/**
 * Retrieve all checklists
 */
var objects = require('../../../standardObjects/admin/objects');
module.exports = {
  path: '/v2.0/checklists',
  callback: function (req, res) {
    return res.json(objects.checklist(1000, req.body.title));
  }
};