var company = require('../../../standardObjects/client/clientObjects');
// Create a new client company
// @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Companies/Index.md#create-a-new-client-company
// Expects a Standard Client Company Persistable Object  @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Companies#standard-client-company-persistable-object
module.exports = {
  path: '/v2.0/client/companies',
  callback: function (req, res) {
    // Return standard client company object
    return res.json(company.clientCompany(1000));
  }
};