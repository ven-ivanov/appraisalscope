/**
 * Pay appraisal
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/appraisals/:appraisalId/payment/payable/pay',
  callback: function (req, res) {
    return res.json();
  }
};