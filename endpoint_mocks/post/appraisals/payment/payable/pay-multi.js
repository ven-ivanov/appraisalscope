/**
 * Pay multiple appraisals
 */
module.exports = {
  path: '/v2.0/appraisals/payment/payable/pay',
  callback: function (req, res) {
    return res.json();
  }
};