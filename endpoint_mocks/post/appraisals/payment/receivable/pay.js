var Appraisement = require('../../../../standardObjects/appraisement/appraisementObjects');
var AppraisalCache = require('../../../../standardObjects/appraisals/cache');
var cache = AppraisalCache.cache;
/**
 * Update appraisal receivable pay attribute
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Payment/Receivable/Pay.md#post-appraisalsappraisalidpaymentreceivablepay-unimplemented
 */
module.exports = {
  path: '/v2.0/appraisals/:appraisalId/payment/receivable/pay/',
  callback: function (req, res) {
    var amount = parseFloat(req.body.amount);
    // Update balance
    AppraisalCache.updateBalance(req.params.appraisalId, amount);
    // Return updated cache
    return res.json(cache[req.params.appraisalId]);
  }
};