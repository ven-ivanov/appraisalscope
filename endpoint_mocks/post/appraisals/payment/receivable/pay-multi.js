/**
 * Update appraisal in receivable unpaid queue
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Payment/Receivable/Pay.md#post-appraisalspaymentreceivablepay-unimplemented
 */
module.exports = {
  path: '/v2.0/appraisals/payment/receivable/pay',
  callback: function (req, res) {
    return res.json();
  }
};