/**
 * Send invoices by email
 */
module.exports = {
  path: '/v2.0/appraisals/payment/receivable/invoice/email',
  callback: function (req, res) {
    return res.json();
  }
};