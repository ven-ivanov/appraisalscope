/**
 * @todo Waiting on backend to accept multiple invoices
 * @link https://github.com/ascope/manuals/issues/271
 */
module.exports = {
  path: '/v2.0/appraisals/:appraisalId/payment/receivable/invoice',
  callback: function (req, res) {
    return res.json();
  }
};