var faker = require('faker');
var _ = require('lodash');
var notes = require('../../../standardObjects/admin/notes/notes');
// New note
module.exports = {
  path: /.*?notes/,
  callback: function (req, res) {
    return res.json(notes.appraiserNote(1000));
  }
};