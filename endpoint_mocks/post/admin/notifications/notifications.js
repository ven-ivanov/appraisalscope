var settingsObjects = require('../../../standardObjects/admin/settings');

/**
 * Store new job type
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
var storeNotification = function (req, res, next) {
  // Response

  var resBody = settingsObjects.notification();

  resBody.label = req.body.label;
  resBody.message = req.body.message;

  return res.json({data: resBody});
};

module.exports = {
  path: '/v2.0/admin/notifications',
  callback: storeNotification
};