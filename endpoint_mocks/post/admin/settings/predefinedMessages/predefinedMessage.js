var g = require('../../../../generator/generator');

/**
 * Store new Message
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
var storeMessage = function (req, res, next) {
  // Response

  var resBody = {
      'id': 3,
      'Title': 'New Title',
      'Message': 'New Message',
      'type': 1,
       
  };

  return res.json(resBody);
};

module.exports = {
  path: '/v2.0/admin/settings/predefinedMessages/predefinedMessage',
  callback: storeMessage
};