'use strict';
/**
 * Store new notification
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
var storeRevisionType = function (req, res, next) {
  // Response
  
  var resBody = {
    'id': 180,
    'message': req.body.message,
    'title': req.body.title
  };

  return res.json(resBody);
};

module.exports = {
  path: '/v2.0/admin/settings/notification/notifications',
  callback: storeRevisionType
};