var g = require('../../../../generator/generator');

/**
 * Store new revision type
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
var storeRevisionType = function (req, res, next) {
  // Response

  var resBody = {
    'id': 10,
    'revisionType': req.body.revisionType,
    'asClientRevision': req.body.asClientRevision,
		'asReviewerRevision': req.body.asReviewerRevision
  };

  return res.json(resBody);
};

module.exports = {
  path: '/v2.0/admin/settings/options/revision-type',
  callback: storeRevisionType
};