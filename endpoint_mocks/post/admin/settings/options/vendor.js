var g = require('../../../../generator/generator');

/**
 * Store new vendor status
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
var storeVendor = function (req, res, next) {
  // Response

  var resBody = {
    'id': 10,
    'status': req.body.status,
  };

  return res.json(resBody);
};

module.exports = {
  path: '/v2.0/admin/settings/options/vendor',
  callback: storeVendor
};