var g = require('../../../../generator/generator');

/**
 * Store new QC Review
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
var storeReview = function(req,res,next){
  return res.json();
}


module.exports = {
  path: '/v2.0/admin/settings/qcReview/appraisal-qc-review',
  callback: storeReview
};