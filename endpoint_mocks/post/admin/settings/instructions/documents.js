var g = require('../../../../generator/generator');

/**
 * Store new document
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
var storeDocument = function (req, res, next) {
  // Response

  var resBody = {
      'id': 3,
      'Document': 'Doc 3',
      'href': '#'    
  };

  return res.json(resBody);
};

module.exports = {
  path: '/v2.0/admin/settings/instructions/documents',
  callback: storeDocument
};