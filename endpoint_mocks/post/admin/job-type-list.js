// var g = require('../../generator/generator');

/**
 * Store new job type list
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
var storeJobTypeList = function (req, res, next) {
  // Response

  var resBody = {
    'id': g.id(),
    'name': req.body.a
  };

  return res.json(resBody);
};

module.exports = {
  path: '/v2.0/admin/job-type-list',
  callback: storeJobTypeList
};