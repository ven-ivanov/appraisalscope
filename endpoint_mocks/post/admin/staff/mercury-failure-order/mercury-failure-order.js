var _ = require('lodash'),
  faker = require('faker');

/**
 * Staff mercury failure orders
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/mercury-failure-orders/',
  callback: function (req, res) {
    var response = [];
    var i;
    // Make 10
    for (i = 1; i <= 10; i = i + 1) {
      var trackingID = _.random(10000);
      response.push({
        id: i,
        name: faker.name.findName(),
        trackingId: trackingID,
        failureReason: 'mercury order with tracking id' + trackingID+'already exists with ascope file #xxx on Ascopesytem',
        requestDate: new Date()
      });
    }
    //must return Object
    var response_obj = {
      data: response
    };
    return res.json(response_obj);
  }
};

