var JobType = require('../../../../standardObjects/jobTypes/jobType.js');
var g = require('../../../../generator/generator');


module.exports = {
  path: '/v2.0/admin/job-type/admin-rule-profile/:staffId',
  callback: function (req, res) {
    var i, response = {data: []};
    var total = g.random(1, 10);

    for (i = 1; i < total; i = i + 1) {
      response.data.push(JobType.jobType());
    }
    return res.json(response);
  }
};

