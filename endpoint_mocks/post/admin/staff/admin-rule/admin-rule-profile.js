var _ = require('lodash'),
  faker = require('faker');

/**
 * User permissions
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/admin-rule-profile/',
  callback: function (req, res) {
    console.log('put method.....');
    console.log(req);
    var response = [];
    var i;
    // Make 10
    for (i = 1; i <= 10; i = i + 1) {
      response.push({
        id: i,
        name: faker.name.findName()
      });
    }
    //must return Object
    var response_obj = {
      data: response
    };
    return res.json(response_obj);
  }
};
