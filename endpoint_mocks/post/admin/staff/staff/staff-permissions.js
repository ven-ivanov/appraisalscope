
var _ = require('lodash'),
  faker = require('faker');

/**
 * User permissions for staff
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/staff/permissions/',
  callback: function (req, res) {
    var response = [];
    var i;
    // Make 5
    for (i = 1; i <= 5; i = i + 1) {
      response.push({
        id: i,
        name: faker.name.findName()
      });
    }
    return res.json({data: response});
  }
};
