var faker = require('faker');
var _ = require('lodash');

/**
 * Retrieve realtors
 */
module.exports = {
  path: '/v2.0/admin/staff',
  callback: function (req, res) {
    console.log(req);
    var response = _.extend(req.body, {id: 11});
    //must return Object
    var response_obj = {
      data: response
    };
    return res.json(response_obj);
  }
};
