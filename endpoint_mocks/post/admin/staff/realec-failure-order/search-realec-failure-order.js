var _ = require('lodash'),
  faker = require('faker');

/**
 * Search staff realeC failure orders
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/realec-failure-orders/search/:transactionId/:uniqueId',
  callback: function (req, res) {

    var reqParams = [];
    // Get parameters
    _.forEach(req.params, function (requestVal) {
      reqParams.push(requestVal);
    });
    // Filter unused parameters
    reqParams = reqParams.filter(function (param) {
      return param;
    });
    /**
     * @TODO
     * Cache result
     * @type {Array}
     */
    var response = [];
    if(reqParams.length == 2)
    {
      // Make 4
      for (var i = 1; i <= 4; i = i + 1) {
        response.push({
          id: i,
          name: faker.name.findName(),
          transactionId: 'dsrectest-'+ reqParams[0],
          uniqueId: reqParams[1],
          failureReason: 'failure reason xxx',
          requestDate: new Date()
        });
      }
    }
    //wrapping
    var response_obj = {
      data: response
    };
    res.json(response_obj);
  }
};
