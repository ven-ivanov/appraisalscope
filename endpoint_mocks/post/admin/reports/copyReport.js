var reports = require('../../../standardObjects/admin/reports/reports.js');
var _ = require('lodash');
module.exports = {
  path: '/v2.0/admin/reports/:id/copy',
  callback: function (req, res) {
    var itemCopy = _.findWhere(reports.listReports(), {id: req.params.id});
    return res.json({data :itemCopy});
  }
};