var _ = require('lodash');
var faker = require('faker');
var reports = require('../../../standardObjects/admin/reports/reports');

var FIELD_TYPES = {
  varchar: 'varchar',
  int: 'int',
  float: 'float',
  date: 'date'
};

var addressColumn = new RegExp('address', 'i');
var nameColumn = new RegExp('name', 'i');
var cityColumn = new RegExp('city', 'i');

function valueForColumn(column) {
  if (column.datatype === FIELD_TYPES.varchar) {
    if (column.label.match(addressColumn)) {
      return faker.address.streetAddress();
    } else if (column.label.match(cityColumn)) {
      return faker.address.city();
    } else if (column.label.match(nameColumn)) {
      return faker.name.findName();
    } else {
      return faker.lorem.sentence();
    }
  } else if (column.datatype === FIELD_TYPES.int) {
    return _.random(3000);
  } else if (column.datatype === FIELD_TYPES.float) {
    return _.random(3000)/100;
  } else if (column.datatype === FIELD_TYPES.date) {
    return faker.date.past();
  }
}

function sortRows(columns) {
  return function (a, b) {
    var sorting = 0;
    columns.some(function (column) {
      if (column.sort) {
        var aLabel = a[column.id];
        var bLabel = b[column.id];
        if (aLabel > bLabel) {
          sorting = column.sort; // to break out forEach
          return true;
        } else if (aLabel < bLabel) {
          sorting = -column.sort; // to break out forEach
          return true;
        }
      }
    });
    return sorting;
  }
}

// Fake sample data generator for reports constructor
module.exports = {
  path: '/v2.0/admin/reports/constructor/data',
  callback: function (req, res) {
    var reqColumns = req.body.report.columns;
    if (!reqColumns) {
      res.status(400).send('At least single columns should be set')
    }
    var rows = [];
    // get columns description
    var columnsData = reports.getReportColumns();

    // Get column data from resource by id
    // TODO Return all columns to not trigger when changed
    var columns = req.body.report.columns.map(function(column) { // for each requested column...
      var columnData;
      columnsData.some(function(item) { // ...check every backend column group...
          var results = item.children.filter(function (child) { // ...for matches
            return child.id === column.id;
          });
          if (results.length) {
            columnData = results[0];
            return true;
          }
      });
      // add found column data to requested column object
      _.extend(column, columnData);
      return column;
    });

    // Generate fake data for 15 rows
    for (var i = 0; i < 15; i++) {
      var row = {
        id: i
      };
      for (var columnIndex = 0; columnIndex < columns.length; columnIndex++) {
        var column = columns[columnIndex];
        row[column.id] = valueForColumn(column);
      }
      rows.push(row);
    }

    rows.sort(sortRows(columns));

    res.json({
      rows: rows
    });
  }
};