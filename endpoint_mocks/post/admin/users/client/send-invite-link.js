/**
 * Send invite link to selected recipients
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/client/companies/:companyId/email-signup-links',
  callback: function (req, res) {
    return res.json({});
  }
};