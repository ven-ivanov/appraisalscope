/**
 * Send login info to selected user
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/send-login-info',
  callback: function (req, res) {
    return res.json();
  }
};