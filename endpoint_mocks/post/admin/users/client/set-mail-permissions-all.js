// Apply mail permissions to all users or brokers
module.exports = {
  path: '/v2.0/admin/users/client/set-mail-permissions-all',
  callback: function (req, res) {
    return res.json({});
  }
};