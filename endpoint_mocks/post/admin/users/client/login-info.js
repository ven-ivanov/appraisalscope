/**
 * Send user login info
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/client/companies/:companyId/branches/:branchId/employees/send-login-details',
  callback: function (req, res) {
    return res.json({});
  }
};