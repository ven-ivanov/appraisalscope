/**
 * Assign instruction
 */
module.exports = {
  path: '/v2.0/users/client/:clientId/instruction/:instructionId',
  callback: function (req, res) {
    return res.json();
  }
};