/**
 * Initiate Quickbooks sync
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/client/companies/:companyId/qb-sync/',
  callback: function (req, res) {
    // Fake nothing to sync for id 1
    if (req.body.id === 1) {
      return res.json({
        syncing: false
      });
    }
    // Success for all others
    return res.json({
      syncing: true
    });
  }
};