/**
 * Send login credentials
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/send-login',
  callback: function (req, res) {
    return res.json();
  }
};