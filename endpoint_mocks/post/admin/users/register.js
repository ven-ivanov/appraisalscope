/**
 * Register
 */
module.exports = {
  path: '/v2.0/users/:type/:id/register',
  callback: function (req, res) {
    return res.json();
  }
};