/**
 * Send invite to appraiser
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/users/appraiser/:appraiserId/invite',
  callback: function (req, res) {
    return res.json();
  }
};