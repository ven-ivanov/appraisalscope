module.exports = {
  path: '/v2.0/users/appraiser/:appraiserId/qb-sync',
  callback: function (req, res) {
    var response = {syncing: true};
    // Fake unneeded for 1
    if (req.params.appraiserId === '1') {
      response.syncing = false;
    }
    return res.json(response);
  }
};