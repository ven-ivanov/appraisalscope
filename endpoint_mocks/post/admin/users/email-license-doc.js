/**
 * Email appraiser license doc
 */
module.exports = {
  path: '/v2.0/users/:type/:appraiserId/license-doc/:docId/email',
  callback: function (req, res) {
    return res.json();
  }
};