// var g = require('../../generator/generator');

/**
 * Store new job type
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
var storeJobType = function (req, res, next) {
  // Response

  var resBody = {
    'id': g.id(),
    'appraisalForm': req.body.appraisalForm,
    'fha': req.body.fha,
    'commercial': req.body.commercial,
    'display': true,
    'appraisedValue': true,
    'doc': {
      'pdf': req.body.pdf,
      'xml': req.body.xml,
      'aci': true,
      'zap': true,
      'env': true
    },
    'mappedId': null,
    'addons': [],
    'clientFee': 0.0,
    'managmentFee': 0.0,
    'appraiserFee': 0.0,
    'isManagmentFee': false,
    'isAppraiserFee': false
  };

  return res.json(resBody);
};

module.exports = {
  path: '/v2.0/admin/job-type',
  callback: storeJobType
};