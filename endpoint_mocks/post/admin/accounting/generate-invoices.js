/**
 * Generate invoices
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/accounting/generate-invoices/',
  callback: function (req, res) {
    // Make sure we have some invoices
    if (!req.body.appraisals.length) {
      return res.status(500).send('No invoices selected');
    }
    // Mock return invoice number 1 always
    return res.json({
      appraisalIds: req.body.appraisals,
      invoiceId: 'NEW_INVOICE',
      invoiceDescription: req.body.description,
      invoiceStatus: 'unpaid',
      invoiceAmount: 1000,
      invoiceNumber: 'INVOICE1'
    });
  }
};