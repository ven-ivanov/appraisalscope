/**
 * Rebill a paid appraisal
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/accounting/rebill-appraisal/:appraisalId',
  callback: function (req, res) {
    // In production, this will have the ability to change credit card info. Ignored for now
    if (req.body.creditCardNumber) {
      return res.json({
        ccUpdated: true
      });
    }
    return res.status(200).send('');
  }
};