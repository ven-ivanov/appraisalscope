module.exports = {
  path: '/v2.0/admin/accounting/appraiser/pay',
  callback: function (req, res) {
    res.json({
      appraisalPaid: true
    });
  }
};