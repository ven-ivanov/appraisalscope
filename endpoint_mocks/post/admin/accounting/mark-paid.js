module.exports = {
  path: '/v2.0/admin/accounting/mark-paid/:type/:id',
  callback: function (req, res) {
    res.json({
      appraisalPaid: true
    });
  }
};