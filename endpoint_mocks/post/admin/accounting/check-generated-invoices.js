/**
 * Allow users to login via mock API endpoint
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
var determineUser = function(req, res, next) {

  // If nothing was selected, don't continue
  if (!req.body.length) {
    return res.status(500).send('No appraisals selected');
  }
  // If only invoice 3 is selected, pretend all appraisals have invoices
  if (req.body.length === 1 && req.body[0] === 3) {
    // No appraisals need invoices
    return res.json({
      appraisals: [],
      invoices: req.body
    });
  }
  // Simulate all other requests, only passing the first in
  // So, for a request with more than one appraisal, this is a subset. For a single appraisal, complete set.
  return res.json({
    // Array of appraisals that still need invoices
    appraisals: req.body.slice(0, 1),
    // Array of appraisals which already have invoices
    invoices: req.body.slice(1)
  });
};


module.exports = {
  path: '/v2.0/admin/accounting/check-generated-invoices',
  callback: determineUser
};
