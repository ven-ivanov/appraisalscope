/**
 * Save Quickbooks settings
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/accounting/qb-settings',
  callback: function (req, res) {
    return res.json({});
  }
};