/**
 * Sync to Quickbooks
 * @type {{path: string, callback: Function}}
 */
module.exports = {
  path: '/v2.0/admin/accounting/sync-to-qb/:checks?',
  callback: function (req, res) {
    return res.json({});
  }
};