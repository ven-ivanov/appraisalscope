This repository contains the source code of the front- and back-end applications. If this is your first visit, please go through the [wiki pages](https://github.com/ascope/ascopev2/wiki).

ALL CODE IN THIS REPOSITORY IS &copy;2008-2015 APPRAISAL SCOPE INC.  
THIS REPOSITORY MAY NOT BE REDISTRIBUTED IN WHOLE OR IN PART.