'use strict';

/**
 * @ngdoc service
 * @name frontendApp.SmartTableDecorator
 * @description
 * # SmartTableDecorator
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('SmartTableDecorator', [ '$timeout',
    function ($timeout) {

      return {
        decorate : function(deco, def){

          /**
           * Holds Defaults.
           *
           * @type {Object}
           * @api public
           */
          def = def || {};

          /**
           * Holds data.
           *
           * @type {Array}
           * @api public
           */
          deco.data = [];

          /**
           * Holds isLoading
           *
           * @type {Boolean}
           * @api public
           */
          deco.isLoading = false;

          /**
           * Holds tableState
           *
           * @type {Object}
           * @api public
           */
          deco.tableState = {};

          /**
           * Holds how many rows per page
           *
           * @type {Number}
           * @api public
           */
          deco.perPage = def.perPage || 10;

          /**
           * RetrieveData
           *
           * @api public
           */
          deco.retrieveData = function(){ };

          /**
           * loading table data
           *
           * @param {Object} tableState
           * @api public
           */
          deco.loadData = function(tableState){

            // Flag to prevent smart-table from calling loadData twice
            if(deco.isLoading) {
              return false;
            }

            // setting loading true to indicate some action happened
            deco.isLoading = true;

            // setting tableState
            deco.tableState = tableState;

            var params = {}, success, fail, start, number;

            // setting pagination start and per page
            start = tableState.pagination.start || 0;
            number = tableState.pagination.number || 10;

            // setting orderBy and order for sorting to sent to server
            if(tableState.sort.predicate){
              params.orderBy = tableState.sort.predicate;
              params.order   = tableState.sort.reverse? 'DESC': 'ASC';
            }

            // setting perPage and page to send to server
            params.perPage = number;
            params.page = Math.round(start / deco.perPage + 1) || 1;

            // query succeeded.
            success = function(response){
              deco.data = response.data;
              tableState.pagination.numberOfPages = response.meta.pagination.totalPages;

              // UGLY workaround - @toDO: look deeper into smart-table to solve double call
              $timeout(function(){
                deco.isLoading = false;
              }, 100);
            };

            // query failed.
            fail = function(e){

              // UGLY workaround - @toDO: look deeper into smart-table to solve double call
              $timeout(function(){
                deco.isLoading = false;
              }, 100);

              // logging errors
              console.log(e);
            };

            // retrieving data.
            return deco.retrieveData(params).then(success,fail).catch(fail);
          };

          /**
           * Add row to table
           *
           * @param {Object} row
           * @param {Number} index
           * @api public
           */
          deco.addRow = function(row, index){
            if(index >= deco.data.length-1) {
              throw new Error('Your index is bigger then table size');
            }

            if(index){
              deco.data.splice(index, 0, row);
            }else{
              deco.data.unshift(row);
            }

            deco.data.pop();
          };

          /**
           * Save RecordNumber details
           *
           * @param {Number} index
           * @api public
           */
          deco.removeRow = function(index){
            deco.data.splice(index, 1);
          };

          return deco;
        }
      };
    }
  ]);