'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:autofill
 * @description
 * #Autofiller
 *
 * Usage:
 * <input autofill="phone" />
 *
 */
angular.module('frontendApp')
  .directive('autofill', [function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
          // zip code autofill function
          var zipMask = function(value){

            var patt = /\d{5}/;

            if(value !== undefined){
              if(value.match(patt)){
                ngModelCtrl.$setViewValue(value.match(patt)[0]);
                ngModelCtrl.$render();
                return value.match(patt)[0];
              }

              ngModelCtrl.$setViewValue(value.replace(/\D+/,''));
              ngModelCtrl.$render();
              return value.replace(/\D+/,'');
            }
          };

          var phoneMask = function(value){

            var patt1 = /(\d{3}).*(\d{3}).*(\d{4})/;

            var patt2 = /^(\d{3})-(\d{3})-(\d{4})/;

            var result;
            if(value !== undefined){
              if(value.match(patt2)){
                ngModelCtrl.$setViewValue(value.match(patt2)[0]);
                ngModelCtrl.$render();
                return value.match(patt2)[0];
              }
              else
              if (value.match(/(\d{10})/)) {
                result = value.match(patt1);
                value = result[1] + '-' + result[2] + '-' + result[3];
                ngModelCtrl.$setViewValue(value);
                ngModelCtrl.$render();
                return value;
              }
              else{
                ngModelCtrl.$setViewValue(value.replace(/\D+/,''));
                ngModelCtrl.$render();
                return value.replace(/\D+/,'');
              }
            }
          };

          var ssnMask = function(value){
            var patt1 = /(\d{3}).*(\d{2}).*(\d{4})/;

            var patt2 = /^(\d{3})-(\d{2})-(\d{4})/;

            var result;
            if(value !== undefined){
              if(value.match(patt2)){
                ngModelCtrl.$setViewValue(value.match(patt2)[0]);
                ngModelCtrl.$render();
                return value.match(patt2)[0];
              }
              else
              if (value.match(/(\d{9})/)) {
                result = value.match(patt1);
                value = result[1] + '-' + result[2] + '-' + result[3];
                ngModelCtrl.$setViewValue(value);
                ngModelCtrl.$render();
                return value;
              }
              else{
                ngModelCtrl.$setViewValue(value.replace(/\D+/,''));
                ngModelCtrl.$render();
                return value.replace(/\D+/,'');
              }
            }
          };

          var taxIdMask = function(value){
            var patt1 = /(\d{2}).*(\d{7})/;

            var patt2 = /^\d{2}\-\d{7}/;

            var result;
            if(value !== undefined){
              if(value.match(patt2)){
                ngModelCtrl.$setViewValue(value.match(patt2)[0]);
                ngModelCtrl.$render();
                return value.match(patt2)[0];
              }
              else
              if (value.match(/(\d{9})/)) {
                result = value.match(patt1);
                value = result[1] + '-' + result[2];
                ngModelCtrl.$setViewValue(value);
                ngModelCtrl.$render();
                return value;
              }
              else{
                ngModelCtrl.$setViewValue(value.replace(/\D+/,''));
                ngModelCtrl.$render();
                return value.replace(/\D+/,'');
              }
            }
          };

          var routingMask = function(value)
          {
            var patt = /\d{9}/;

            if(value !== undefined){
              if(value.match(patt)){
                ngModelCtrl.$setViewValue(value.match(patt)[0]);
                ngModelCtrl.$render();
                return value.match(patt)[0];
              }

              ngModelCtrl.$setViewValue(value.replace(/\D+/,''));
              ngModelCtrl.$render();
              return value.replace(/\D+/,'');
            }
          };

          switch (attrs.autofill)
          {
            case 'phone':
              ngModelCtrl.$parsers.unshift(phoneMask);
              break;
            case 'zip':
              ngModelCtrl.$parsers.unshift(zipMask);
              break;
            case 'ssn':
              ngModelCtrl.$parsers.unshift(ssnMask);
              break;
            case 'taxId':
              ngModelCtrl.$parsers.unshift(taxIdMask);
              break;
            case 'routing':
            ngModelCtrl.$parsers.unshift(routingMask);
              break;
          }
        }
      };
    }]);
