'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:modal
 * @description
 * Wrap provided html in bootrap modal
 *
 * Usage:
 * <modal visible="aModel">...</modal>
 */
angular.module('frontendApp')
  .directive('modal', function () {
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      scope: {
        visible: '='
      },
      template: '<div class="new-window-overlay">' +
                  '<div class="modal fade">' +
                    '<div class="modal-dialog {{size}}">' +
                      '<div class="modal-content">' +
                        '<div class="modal-body clearfix" ng-transclude></div>' +
                      '</div>' +
                    '</div>' +
                  '</div>' +
                '</div>',
      link: function ($scope, element, attrs) {
        var modal = element.contents();

        switch (attrs.size) {
          case 'large':
            $scope.size = 'modal-lg';
            break;

          case 'small':
            $scope.size = 'modal-sm';
            break;
        }

        $scope.$watch('visible', function(value) {
                  
          if(value) {
            angular.element(modal).modal('show');
          } else {
            angular.element(modal).modal('hide');
          }
        }, true);

        modal.on('shown.bs.modal', function(){
          $scope.$digest(function(){
            $scope.visible = true;
          });
        });

        modal.on('hidden.bs.modal', function(){
          $scope.$apply(function(){
             $scope.visible = false;
          });
        });
      
      }
    };
  });
