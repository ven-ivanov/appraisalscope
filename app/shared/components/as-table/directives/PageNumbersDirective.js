'use strict';

var app = angular.module('frontendApp');

/**
 * Page number directive
 */
app.directive('pageNumbers',
['$timeout', function ($timeout) {
  return {
    templateUrl: '/shared/components/as-table/views/page-numbers.html',
    replace: true,
    scope: {
      // Get data for current page
      getRequestedPage: '=',
      // The number of pages
      pages: '=',
      // Object for calculations and page display
      page: '='
    },
    controller: 'PageNumbersCtrl',
    controllerAs: 'pageNumberCtrl',
    link: function (scope) {
      $timeout(function () {
        // Set width of page numbers row
        var cells = angular.element('.page-numbers td');
        angular.element(cells[1]).width(angular.element('.page-numbers').prev().width() -
                                        angular.element(cells[0]).width());

        /**
         * Create array of page numbers
         * @type {number}
         */
        var pageNumber = 1;
        var thisPage;
        scope.$watchCollection(function () {
          return scope.pages;
        }, function (newVal) {
          if (angular.isUndefined(newVal)) {
            return;
          }
          // Start at page one
          pageNumber = 1;
          // Create an array of numbers incrementing
          scope.pageNumbers = Array.apply(0, new Array(newVal)).map(function() {
            thisPage = pageNumber;
            pageNumber = pageNumber + 1;
            return thisPage;
          });
        });

        /**
         * Watch current page and set it to active
         */
        scope.$watchCollection(function () {
          return scope.page;
        }, function (newVal) {
          scope.currentPage = newVal.displayPage;
        });
      });
    }
  };
}]);