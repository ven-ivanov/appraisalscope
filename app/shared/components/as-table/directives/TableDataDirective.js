'use strict';

var app = angular.module('frontendApp');

/**
 * Create a table data cell
 *
 * Currently, this will conditionally set filtering based on the configuration. Further optimization is in order
 */
app.directive('tableData', ['$compile', function ($compile) {
  return {
    scope: {
      data: '=',
      heading: '=',
      linkFn: '&',
      //multiselect handles all multi select data
      multiSelect: '=',
      // Special individual cells, rather than columns
      specialCells: '=?',
      special: '=?',
      tableInputs: '=?'
    },
    compile: function () {
      return {
        pre: function (scope, elem) {
          var htmlString;
          /**
           * Creates an input element\
           */
          var compileInput = function (model) {
            model = model || 'special';
            return $compile('<div><input type="text" class="form-control" ng-model="' + model +
                            '[data.id]" placeholder="{{heading.placeholder}}"/></div>')(scope);
          };
          /**
           * Create dropdown
           */
          var compileDropdown = function (i) {
            // If each has an ID, use that. Otherwise, use the value to track
            var id = true;
            angular.forEach(scope.specialCells[i].options, function (option) {
              if (angular.isUndefined(option.id)) {
                id = false;
              }
            });
            // Track by id
            if (id) {
              return $compile('<div><select class="selectpicker form-control" id="approvedLists" ng-model="special[data.id]" ng-options=\'option.id as option.value for option in ' + JSON.stringify(scope.specialCells[i].options) + '\' as-select></select></div>')(scope);
            } else {
              // Track by value
              return $compile('<div><select class="selectpicker form-control" id="approvedLists" ng-model="special[data.id]" ng-options=\'option.value as option.value for option in ' + JSON.stringify(scope.specialCells[i].options) + '\' as-select></select></div>')(scope);
            }
          };
          /**
           * Compile a multiselect cell
           */
          var compileMultiSelect = function (i) {
            return $compile('<div ng-dropdown-multiselect options="specialCells[' + i + '].options" selected-model="special[data.id]" extra-settings=\'' + JSON.stringify(scope.specialCells[i].settings) + '\'></div>')(scope);
          };
          /**
           * Compile a function cell
           */
          var compileFunction = function (i) {
            return $compile('<div><a ng-click="special[data.id](); $event.stopPropagation();" class="table-function">' + scope.specialCells[i].label + '</a></div>')(scope);
          };
          /**
           * Compile a custom template string
           */
          var compileCustom = function (template) {
            var regexp = /linkFn\((.*?)\)/g;

            template = template.replace(regexp, function (functionCall) {
              var functionName = "'" + functionCall.substring(7, functionCall.length - 1) + "'"; // jshint ignore:line

              return 'linkFn({id: data.id, name: '+ functionName +', sortedIdentifier: data.sortedIdentifier}); $event.stopPropagation();';
            });

            return $compile('<div>' + template + '</div>')(scope);
          };

          /**
           * Compile a custom template string
           */
          var compileCustomSpecial = function (i) {
            var template = scope.specialCells[i].template;
            return compileCustom(template);
          };

          // Function
          if (angular.isDefined(scope.heading.fn)) {
            htmlString = $compile('<div><table-link-function></table-link-function></div>')(scope);
            // Input
          } else if (angular.isDefined(scope.heading.input) && scope.heading.input) {
            htmlString = compileInput('tableInputs');
            // Checkbox
          } else if (angular.isDefined(scope.heading.checkbox) && scope.heading.checkbox) {
            htmlString = '<div><input type="checkbox" ng-model="multiSelect[data.id]" checkable=""';
            // Disabled
            if (scope.heading.disabled) {
              htmlString = htmlString + ' disabled="disabled"';
            }
            htmlString = htmlString + '></div>';
            htmlString =
            $compile(htmlString)(scope);
            // Dropdown menu
          } else if (scope.heading.dropdown) {
            htmlString =
            $compile('<div><select class="selectpicker form-control" id="approvedLists" ng-model="multiSelect[data.id]" ng-options=\'option.value as option.value for option in ' + JSON.stringify(scope.heading.selectOptions) + '\' as-select></select></div>')(scope);

          } else if (scope.heading.custom && scope.heading.template) {
            //custom template column
            htmlString = compileCustom(scope.heading.template);

            // Special cells (for cells which do NOT conform to a column)
          } else if (scope.heading.special) {

            var specialCellLength = scope.specialCells.length;
            var i;
            // Iterate special cells
            for (i = 0; i < specialCellLength; i = i +1) {
              if (scope.specialCells[i].id === scope.data.id) {
                // Input box
                if (scope.specialCells[i].type === 'input') {
                  htmlString = compileInput();
                  // Special function for a particular row
                } else if (scope.specialCells[i].type === 'fn') {
                  htmlString = compileFunction(i);
                  // Dropdown
                } else if (scope.specialCells[i].type === 'dropdown') {
                  htmlString = compileDropdown(i);
                  // Dropdown multiselect
                } else if (scope.specialCells[i].type === 'dropdown-multiselect') {
                  htmlString = compileMultiSelect(i);
                } else if (scope.specialCells[i].type === 'custom') {
                  htmlString = compileCustomSpecial(i);
                  // custom column template
                }
              }
            }
            // Normal data columns
          } else if (angular.isString(scope.heading.data)) {
            /**
             * Limit watchers -- Only interpolate values which are not static
             */
            var dataString = '{{angular.isUndefined(data[heading.data]) || data[heading.data] === \'\' ? \'--\' : data[heading.data]';
            /**
             * Dynamically filter
             */
            if (angular.isDefined(scope.heading.isStatic) && !scope.heading.isStatic && angular.isDefined(scope.heading.filter)) {
              // Create final html string
              htmlString = '<div>' + dataString + ' | ' + scope.heading.filter + '}}</div>';
              /**
               * Already filtered, or doesn't need filtering
               */
            } else {
              dataString = dataString + '}}';
              htmlString = '<div>' + dataString + '</div>';
            }
            htmlString = $compile(htmlString)(scope);
            // Checkbox
          }
          // Create and append actual template
          elem.append(htmlString);
        }
      };
    }
  };
}]);
