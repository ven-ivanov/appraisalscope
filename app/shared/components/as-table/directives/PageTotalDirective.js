'use strict';

var app = angular.module('frontendApp');

app.directive('pageTotal', ['DomLoadService', function (DomLoadService) {
  return {
    templateUrl: '/shared/components/as-table/views/page-total.html',
    replace: true,
    link: function () {
      DomLoadService.load().then(function () {
        var pageTotalCells = angular.element('.page-total td');
        angular.element('as-table table tbody tr').eq(0).find('td').each(function (key, elem) {
          angular.element(pageTotalCells[key]).width(angular.element(elem).width() + 'px');
        });
      });
    }
  };
}]);