'use strict';

var app = angular.module('frontendApp');

app.directive('tableOrder', [function () {
  return {
    scope: {
      headingData: '='
    },
    restrict: 'A',
    replace: true,
    templateUrl: '/shared/components/as-table/views/table-order.html'
  };
}]);
