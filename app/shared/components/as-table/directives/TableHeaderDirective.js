'use strict';

var app = angular.module('frontendApp');

/**
 * Create table header
 */
app.directive('tableHeader', ['$compile', function ($compile) {
  return {
    scope: {
      noHeading: '=',
      tableCtrl: '='
    },
    link: function (scope, elem) {
      var template;
      // Static heading
      if (!angular.isDefined(scope.tableCtrl.headingStatic) || scope.tableCtrl.headingStatic) {
        template = ['<tr ng-if="::!noHeading">',
                        '<th ng-repeat="heading in ::tableCtrl.heading track by $index">',
                        '<div class="form-group">',
                        '<label>{{::heading.label}}</label>',
                        '<input type="text" class="form-control" id="{{::heading.label}}" placeholder="Search"',
                        'ng-if="::heading.search" ng-model="tableCtrl.filterByProp[heading.data]"/>',
                        '</div>',
                        '</th>',
                        '</tr>',
                        ',<tr class="text-center" ng-if="::!noHeading">',
                        '<th class="text-center" ng-repeat="heading in ::tableCtrl.heading track by $index" ng-if="::!heading.noSort" ng-click="::tableCtrl.sort(heading.data)">',
                        '<img src="/assets/images/icons/arrows.svg" class="svg-small" alt="">', '</th>', '</tr>'];
      } else {
        // Non-static heading (can be reordered)
        template = ['<tr ng-if="::!noHeading">',
                    '<th ng-repeat="heading in tableCtrl.heading track by $index">',
                    '<div class="form-group">',
                    '<label>{{heading.label}}</label>',
                    '<input type="text" class="form-control" id="{{heading.label}}" placeholder="Search"',
                    'ng-if="::heading.search" ng-model="tableCtrl.filterByProp[heading.data]"/>',
                    '</div>',
                    '</th>',
                    '</tr>',
                    ',<tr class="text-center" ng-if="::!noHeading">',
                    '<th class="text-center" ng-repeat="heading in tableCtrl.heading track by $index" ng-if="::!heading.noSort" ng-click="tableCtrl.sort(heading.data)">',
                    '<img src="/assets/images/icons/arrows.svg" class="svg-small" alt="">', '</th>', '</tr>'];
      }
      template = $compile(template.join(''))(scope);
      angular.element(elem).parents('tr').replaceWith(template);
    }
  };
}]);