'use strict';

var app = angular.module('frontendApp');

app.directive('grandTotal', ['DomLoadService', function (DomLoadService) {
  return {
    templateUrl: '/shared/components/as-table/views/grand-total.html',
    replace: true,
    link: function () {
      DomLoadService.load().then(function () {
        var grandTotalCells = angular.element('.grand-total td');
        angular.element('as-table table tbody tr').eq(0).find('td').each(function (key, elem) {
          angular.element(grandTotalCells[key]).width(angular.element(elem).width() + 'px');
        });
      });
    }
  };
}]);