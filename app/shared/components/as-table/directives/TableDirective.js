/*global Headroom:false */
'use strict';

var app = angular.module('frontendApp');

/**
 * General table component that is created as a result of the configuration and data passed to it
 *
 * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/Guidelines/Front-end/components/as-table.md
 */
app.directive('asTable', ['$compile', '$timeout', '$window', 'DomLoadService', '$interval', function ($compile, $timeout, $window, DomLoadService, $interval) {
  return {
    templateUrl: '/shared/components/as-table/views/table.html',
    transclude: true,
    link: function (scope, element, attrs) {
      // Length (in px) from screen top/bottom before infinite scroll is triggered
      var infiniteScrollOffset = 50;
      scope.newElementCount = 0;
      // Whether to allow data to be loaded on scroll up
      scope.loadDataUp = false;
      // Header is static by default
      scope.staticHeader = true;

      // Get empty cells for items which will not take up a complete row (such as totals at the bottom)
      scope.getEmptyCells = function () {
        return new Array(scope.newElementCount);
      };

      // Determine whether to show page totals
      scope.showTotals = angular.isUndefined(attrs.totals) || attrs.totals !== 'false';
      // Show page numbers
      scope.hidePageNumbers = angular.isDefined(attrs.hidePageNumbers) && attrs.hidePageNumbers === 'true';
      // Set table ID
      scope.tableId = attrs.tableId;

      // If specifying only specific columns to bold
      if (attrs.boldColumns) {
        // Wait for the table to load
        var boldInterval = $interval(function () {
          var tableCell = angular.element('#' + attrs.tableId + ' tbody tr td');
          if (angular.element(tableCell).length) {
            // Once the table has loaded, remove all bolding
            angular.element(tableCell).css('color', '#a09696').css('font-family', 'Proxima Nova regular');
            // Add bold to each specified column
            attrs.boldColumns.split(',').forEach(function (column) {
              angular.element('#' + attrs.tableId + ' tbody tr td:nth-child(' + column + ')').css('color', '#4a585d').css('font-family',
              'Proxima Nova semibold');
            });
            // Cancel interval
            $interval.cancel(boldInterval);
          }
        });
      }
      // Set overflow-x
      if (attrs.overflowX) {
        $timeout(function () {
          // Select by ID
          if (attrs.tableId) {
            angular.element('#' + attrs.tableId).css('overflow-x', attrs.overflowX);
          } else {
            angular.element('as-table table').css('overflow-x', attrs.overflowX);
          }
        });
      }

      /**
       * Algin cells left
       */
      if (attrs.alignCells && attrs.alignCells === 'left') {
        scope.$watchCollection(function () {
          return scope.tableCtrl.tableData;
        }, function () {
          $timeout(function () {
            angular.element('#' + attrs.tableId + ' tbody td').css('padding-left', '35px').addClass('text-left');
          });
        });
      }

      // If noHeading is specified, display table without a heading
      if (attrs.noHeading) {
        scope.noHeading = true;
      }

      // Wait for all data - If no records, replace table
      if (attrs.noEmpty) {
        DomLoadService.load().then(function () {
          if (angular.isArray(scope.tableCtrl.tableData) && !scope.tableCtrl.tableData.length) {
            angular.element('#' + attrs.tableId).replaceWith('<h4>No Records Available</h4>');
          }
        });
      }
      // If using non-static headers, as in, they can be changed (reordered, whatever)
      if (angular.isDefined(attrs.headerStatic) && attrs.headerStatic === 'false') {
        scope.staticHeader = false;
      }

      /**
       * Disable infinite scroll -- IF INFINITE SCROLL IS DISABLED, NONE OF THIS CODE BELOW WILL RUN
       * Disabled always for now
       */
      if (angular.isDefined(attrs.infiniteScroll) || angular.isUndefined(attrs.infiniteScroll)) {
        return;
      }
      /**
       * Scroll to the correct location when slicing data on infinite load
       */
      scope.scrollOnLoad = function (data) {
        // Scroll direction
        var direction = arguments.length === 2 ? arguments[1] : 'down';
        // Remove headroom before scrolling for the user
        if (scope.useHeadroom) {
          scope.headroomItems.forEach(function (item) {
            // Make sure the items stay shown if scrolling up
            if (direction === 'up' && (typeof angular.element(item).attr('class') === 'undefined' ||
                                       angular.element(item).attr('class').indexOf(scope.headroomStartingOptions.classes.pinned) === 1)) {
              angular.element(item).addClass(scope.headroomStartingOptions.classes.pinned);
            }
            item.headroom.destroy();
          });
        }
        // Load data both up and down
        scope.loadDataUp = true;
        var rowHeight = angular.element('as-table table tbody tr').eq(0).height();
        if (direction === 'down') {
          /**
           * If there is a timing issue and the rows is not rendered yet, get it on the next digest cycle. We would prefer
           * to keep it in *this* digest cycle, but it's preferable to have it be a bit jumpy rather than not happen
           * at all -- Logan
           */
          if (!rowHeight) {
            $timeout(function () {
              // Scroll up to the position where it would keep the screen in the same place
              angular.element(window).scrollTop(angular.element(window).scrollTop() -
                                                angular.element('as-table table tbody tr').eq(0).height() *
                                                data.length);
            });
          } else {
            angular.element(window).scrollTop(angular.element(window).scrollTop() - rowHeight * data.length);
          }
          // Don't show headers when reattaching headroom
          if (scope.useHeadroom) {
            scope.headroomItems.forEach(function (item) {
              angular.element(item.elem).hide();
              item.headroom = new Headroom(item.elem, scope.headroomHideOptions);
            });
            // Display previously hidden items
            $timeout(function () {
              scope.headroomItems.forEach(function (item) {
                angular.element(item.elem).show();
              });
            }, 600);
          }
        } else {
          // If we don't have a row height, wait for next digest cycle
          if (!rowHeight) {
            $timeout(function () {
              // Scroll to a reasonable position
              angular.element(window).scrollTop(angular.element('as-table table tbody tr').eq(0).height() *
                                                data.length);
            });
          } else {
            angular.element(window).scrollTop(rowHeight * data.length);
          }
          // Create new headroom element
          if (scope.useHeadroom) {
            scope.headroomItems.forEach(function (item) {
              item.headroom = new Headroom(item.elem, scope.headroomStartingOptions);
            });
          }
        }
        // Init headroom
        if (scope.useHeadroom) {
          // Init each headroom element
          scope.headroomItems.forEach(function (item) {
            item.headroom.init();
          });
        }
      };

      var enableScrollDown = true, enableScrollUp = true;
      // Disable infinite scroll
      scope.$on('disable-infinite-scroll', function () {
        enableScrollDown = enableScrollUp = false;
      });
      // Enable infinite scroll
      scope.$on('enable-infinite-scroll', function () {
        enableScrollDown = enableScrollUp = true;
      });
      // Bind scroll event to controller functions
      angular.element($window).bind('scroll', function() {
        // Remove data from the end
        if (this.pageYOffset <= infiniteScrollOffset && scope.loadDataUp) {
          if (enableScrollUp) {
            enableScrollUp = false;
            // Trigger loading of previous page
            scope.tableCtrl.prevPage().then(function () {
              enableScrollUp = true;
            });
          }
        // Trigger load next page
        } else if (angular.element(document).height() - this.innerHeight - this.scrollY <= infiniteScrollOffset) {
          // Don't allow more than one load event to occur at any given time
          if (enableScrollDown) {
            enableScrollDown = false;
            // Get next page, then enable infinite scroll
            scope.tableCtrl.nextPage().then(function () {
              enableScrollDown = true;
            });
          }
        }
      });
    }
  };
}]);