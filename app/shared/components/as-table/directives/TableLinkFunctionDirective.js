'use strict';

var app = angular.module('frontendApp');

/**
 * Set the function for data cells which contain functions
 */
app.directive('tableLinkFunction', ['$compile', function ($compile) {
  return function (scope, elem) {
    var template;
    /**
     * Determine whether an optional function should be shown
     */
    var optionalFnHidden = function () {
      // If not an optional function
      if (angular.isUndefined(scope.heading.optionalFn) || !scope.heading.optionalFn) {
        return false;
      }
      /**
       * @todo This logic is wrong! The $eval call is doing exactly the opposite of what it should be doing. =/
       */
      // If data for comparison is defined
      if (angular.isDefined(scope.heading.data)) {
        // If data is defined but it is falsy, don't display
        if (angular.isDefined(scope.data[scope.heading.data]) && !scope.data[scope.heading.data]) {
          return true;
        }
        // If we're doing a comparison, return whether it's true
        if (scope.$eval(scope.heading.data) === true) {
          return true;
        }
      }
      return false;
    };
    // If this is an optional function and there is no data, don't create the link function
    if (optionalFnHidden()) {
      template = '';
    } else {
      // If we have an object of potential label values, choose the correct one
      if (angular.isObject(scope.heading.linkLabel) && angular.isDefined(scope.heading.data)) {
        // If this is a non-static data cell, look for the truth value of linkLabel:
        // linkLabel: {false: '', true: 'Function Title'}
        if (angular.isDefined(scope.heading.isStatic) && !scope.heading.isStatic) {
          template =
          '<a ng-click="linkFn({id: data.id, name: heading.fn, sortedIdentifier: data.sortedIdentifier}); $event.stopPropagation();" class="table-function">{{heading.linkLabel[!!data[heading.data]]}}</a>';
        } else {
          // Create link function
          template =
          '<a ng-click="linkFn({id: data.id, name: heading.fn, sortedIdentifier: data.sortedIdentifier}); $event.stopPropagation();" class="table-function">{{heading.linkLabel[data[heading.data]]}}</a>';
        }
        // Variable link title based on another piece of data: linkLabel: {data: 'fileName'}, variableLabel: true
      } else if (angular.isObject(scope.heading.linkLabel) && angular.isDefined(scope.heading.variableLabel) &&
                 scope.heading.variableLabel) {
        // Create link function
        template = '<a ng-click="linkFn({id: data.id, name: heading.fn, sortedIdentifier: data.sortedIdentifier}); $event.stopPropagation();" class="table-function">{{data[heading.linkLabel.data]}}</a>';
      } else {
        // Create link function
        template = '<a ng-click="linkFn({id: data.id, name: heading.fn, sortedIdentifier: data.sortedIdentifier}); $event.stopPropagation();" class="table-function">{{heading.linkLabel}}</a>';
      }
    }
    // Compile and replace
    template = $compile(template)(scope);
    elem.replaceWith(template);
  };
}]);