'use strict';

var app = angular.module('frontendApp');

/**
 * Create additional rows for data when table data is grouped
 *
 * Template must not be a separate file.
 * @link https://github.com/angular/angular.js/issues/2151
 */
app.directive('groupedData', ['$compile', function ($compile) {
  return {
    link: function (scope, element) {
      var template;
      if (scope.$parent.tableCtrl.tableData[0].sortedIdentifier) {
        template = [
          '<tr ng-repeat="data in data.data | filter:tableCtrl.filterAll track by $index" ng-click="tableCtrl.rowFn(data.id)">',
          '<td ng-repeat="heading in ::tableCtrl.heading track by heading.label" ng-class="::{\'text-uppercase\': heading.uc}">',
          '<table-data heading="heading" data="data" link-fn="tableCtrl.linkFn(id, name, sortedIdentifier)" multi-select="tableCtrl.multiSelect" special-cells="tableCtrl.specialCellsConfig" special="tableCtrl.specialCellsInit" table-inputs="tableCtrl.tableInputs"></table-data>',
          '</td>',
          '</td>',
          '</tr>'].join('');
      } else {
        template = '';
      }
      angular.element(element).replaceWith($compile(template)(scope));
    }
  };
}]);