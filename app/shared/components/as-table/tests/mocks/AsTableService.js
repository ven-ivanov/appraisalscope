var AsTableServiceMock = function ($q) {
  return {
    applyFilters: function (data) {
      return $q(function (resolve) {
        resolve(data);
      });
    }
  }
};