'use strict';

var app = angular.module('frontendApp');

/**
 * Helper for page numbers
 */
app.factory('PageNumbersService', ['$state', function ($state) {
  return {
    changeUrl: function (page) {
      $state.transitionTo('.', {page: page}, { location: true, inherit: true, relative: $state.$current, notify: false });
    }
  };
}]);