'use strict';

var app = angular.module('frontendApp');

app.factory('AsTableService', ['$q', '$filter', function ($q, $filter) {
  return {
    /**
     * Filter data before passing it to the table
     */
    applyFilters: function (data, headers) {
      // For iterating header as array
      var i, recordLength;
      return $q(function (resolve) {
        if (angular.isUndefined(headers)) {
          throw Error('Headers must be defined for applyFilters');
        }
        // Iterate rows
        angular.forEach(data, function (row, key) {
          // Make sure we're dealing with a plain object
          if (Object.getPrototypeOf(row) !== Object.prototype) {
            recordLength = Object.keys(row).length;
            // Iterate each data cell, see if there's a filter and apply it if necessary
            for (i = 0; i < recordLength; i = i + 1) {
              // Only run if we have a header, and the data is not going to change. If the data is going to be updated,
              // let Angular handle filtering in the view
              if (angular.isDefined(headers[i]) && angular.isDefined(headers[i].filter) &&
                  (!angular.isDefined(headers[i].isStatic) || headers[i].isStatic)) {
                // Filter with args
                // @todo I need to handle multiple args function here
                if (headers[i].filter.indexOf(':') !== -1) {
                  var filterArgs = headers[i].filter.split(':');
                  data[key][headers[i].data] = $filter(filterArgs[0])(data[key][headers[i].data], filterArgs[1]);
                  // Filter without args
                } else {
                  data[key][headers[i].data] = $filter(headers[i].filter)(data[key][headers[i].data]);
                }
              }
            }
          }
        });
        return resolve(data);
      });
    },
    // @todo Move getHeaders, getSorters, getPageTotals, and anything else from admin accounting section that could be useful
    /**
     * Retrieve headers to be used in the current table view
     * @param requestedHeaders
     * @returns {Array}
     */
    getHeaders: function (requestedHeaders) {
      var usedHeaders = [];
      // Iterate headers, and search for existing data points
      angular.forEach(requestedHeaders, function (header) {
        if (requestedHeaders[header]) {
          usedHeaders.push(requestedHeaders[header]);
        } else {
          // If no header exists, use it as a label only
          usedHeaders.push({
            label: header,
            data: null
          });
        }
      });
      return usedHeaders;
    },
    /**
     * Retrieve sorters for each table view
     * @param sorters
     */
    getSorters: function (sorters) {
      var sorterObject = {};
      angular.forEach(sorters, function (sorter) {
        // Throw an error if an invalid sorter is requested
        if (!sorters[sorter]) {
          throw Error('Could not find data to sort');
     }
     sorterObject[sorter] = function (value) {
          return value[sorter];
        };
     });
     return sorterObject;
     },
     /**
     * Calculate totals for this set of data
     * @param rowData
     * @param columnsToCalculate
     * @param limit
     * @returns {*}
     */
    getPageTotal: function (rowData, columnsToCalculate, limit) {
      // If nothing to work with, return empty object
      if (!rowData.length) {
        return {};
      }
      if (limit) {
        rowData = rowData.slice(0, limit);
      } else {
        // Reset totals (for infinite scroll)
        angular.forEach(columnsToCalculate, function (total, key) {
          columnsToCalculate[key] = 0;
        });
      }
      // Iterate row data and recalculate page total
      angular.forEach(rowData, function (thisRow) {
        // Calculate page totals
        angular.forEach(thisRow, function (cell, rowKey) {
          // See if this data point is one that will be totaled
          if (columnsToCalculate.hasOwnProperty(rowKey) && cell) {
            // Make sure to not process strings
            if (typeof cell === 'string') {
              cell = parseFloat(cell);
            }
            // Add to column total
            columnsToCalculate[rowKey] = columnsToCalculate[rowKey] + cell;
          }
        });
      });
      return columnsToCalculate;
    }
  };
}]);
