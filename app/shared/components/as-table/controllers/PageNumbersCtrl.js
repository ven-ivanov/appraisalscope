'use strict';

var app = angular.module('frontendApp');

app.controller('PageNumbersCtrl', ['$scope', function ($scope) {
  /**
   * Retrieve a specific page
   * @param page
   */
  this.goToPage = function (page) {
    // Get the current page
    $scope.getRequestedPage(page).then(function () {
      $scope.page.displayPage = page;
    });
  };
}]);