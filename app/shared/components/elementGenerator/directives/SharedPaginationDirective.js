'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.directive:sharedPagination
 * @description
 *
 * Table pagination
 */
app.directive('sharedPagination', [
  function() {
    return {
      scope: {
        paginationMeta: '=',
        changePage: '&'
      },
      templateUrl: '/shared/components/elementGenerator/views/pagination.html',
      link: function(scope) {
        scope.changePageCallback = function(page) {
          scope.changePage({newPage: page});
        };

        scope.$watch('paginationMeta', function() {
          var page;
          var currentPage;

          scope.pages = [];

          if (typeof scope.paginationMeta === 'undefined') {
            return null;
          }

          currentPage = scope.paginationMeta.page = parseInt(scope.paginationMeta.page);

          // Create a list of page numbers to display to the user
          for (var i = -3; i <= 3; i++) {
            page = currentPage + i;

            if (page >= currentPage && page <= scope.paginationMeta.totalPages || currentPage > page && page > 0) {
              scope.pages.push(page);
            }
          }
        });
      }
    };
  }
]);
