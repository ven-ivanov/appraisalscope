'use strict';

/**
 * @ngdoc service
 * @name frontendApp.service:SharedElementGeneratorService
 * @description
 * Generate valid HTML element based on type with attributes and inner content where applicable. Check specific methods
 * for more information.
 */
angular.module('frontendApp')
  .factory('SharedElementGeneratorService',

    function () {

      /**
       * Converts key: value from provided object into HTML attributes in the form of key="value"
       *
       * @param attributes {Object} Key value set of strings to be converted
       * @returns {string} HTML string in the format key="value" create from input object
       */
      function attrToString(attributes) {
        var text = '';

        for (var key in attributes) {
          text += ' '+ key +'="'+ attributes[key] +'"';
        }

        return text;
      }

      /**
       * Create a valid HTML element from provided input.
       *
       * @param name {string} HTML element name (div, input, label, etc)
       * @param innerHTML {string} Inner content of the element.
       * @param attributes {Object,optional} Element attributes in key: value Object form.
       * @param selfCancel {bool,optional} Optional flag describing the way an element should end. Used for Inputs
       * @returns {string} Generated HTML string
       */
      function generateElement(name, innerHTML, attributes, selfCancel) {
        // If only name is provided just return an empty element
        if (!innerHTML && !attributes && !selfCancel) {
          return '<'+ name +'></'+ name +'>';
        }

        if (selfCancel) {
          return '<'+ name + attrToString(attributes) +' />';
        } else {
          return '<'+ name + attrToString(attributes) +'>'+ innerHTML +'</'+ name +'>';
        }
      }

      /**
       * Public wrapper for generateElement() function. Used primarily for creating directive invoking HTML
       *
       * @param name {string} Directive HTML name
       * @param attributes {Object,optional} Directive settings in key: value Object form.
       * @returns {string} Generated HTML
       */
      this.directive = function(name, innerHTML, attributes) {
        return generateElement(name, innerHTML, attributes);
      };

      /**
       * Generate <input> HTML element
       *
       * @param attributes {Object,optional} List of input field attributes in key: value Object form.
       * @returns {string} Generated HTML
       */
      this.input = function(attributes) {
        return generateElement('input', null, attributes, true);
      };

      /**
       * Generate <label> HTML
       *
       * @param label {string} Inner text of the label element.
       * @param attributes {Object,optional} Element attributes in key: value Object form.
       * @returns {string} Generated HTML
       */
      this.label = function(label, attributes) {
        return generateElement('label', label, attributes);
      };

      /**
       * Create <div> HTML element
       *
       * @param contents {string} Inner content of the div element
       * @param attributes {Object,optional} Element attributes in key: value Object form.
       * @returns {string} Generated HTML
       */
      this.div = function(contents, attributes) {
        return generateElement('div', contents, attributes);
      };

      /**
       * Generate <form> HTML element
       *
       * @param contents {string} Inner content of the div element
       * @param attributes {Object,optional} Element attributes in key: value Object form.
       * @returns {string} Generated HTML
       */
      this.form = function(contents, attributes) {
        return generateElement('form', contents, attributes);
      };

      /**
       * Generate <select> HTML element with default option
       *
       * @param defaultOption {Object} Value and label for a default option when nothing else is selected
       * @param attributes {Object,optional} Element attributes in key: value Object form.
       * @returns {string} Generated HTML
       */
      this.select = function(defaultOption, attributes) {

        if (defaultOption) {
          defaultOption = generateElement('option', defaultOption, {value: ''});
        } else {
          defaultOption = generateElement('option', null, null);
        }

        return generateElement('select', defaultOption, attributes);
      };

      /**
       * Generate <table> HTML element
       *
       * @param contents {string} Inner content of the div element
       * @param attributes {Object,optional} Element attributes in key: value Object form.
       * @returns {string} Generated HTML
       */
      this.table = function(contents, attributes) {
        return generateElement('table', contents, attributes);
      };

      /**
       * Generate <thead> HTML element
       *
       * @param contents {string} Inner content of the div element
       * @param attributes {Object,optional} Element attributes in key: value Object form.
       * @returns {string} Generated HTML
       */
      this.thead = function(contents, attributes) {
        return generateElement('thead', contents, attributes);
      };

      /**
       * Generate <tr> HTML element
       *
       * @param contents {string} Inner content of the div element
       * @param attributes {Object,optional} Element attributes in key: value Object form.
       * @returns {string} Generated HTML
       */
      this.tr = function(contents, attributes) {
        return generateElement('tr', contents, attributes);
      };

      /**
       * Generate <th> HTML element
       *
       * @param contents {string} Inner content of the div element
       * @param attributes {Object,optional} Element attributes in key: value Object form.
       * @returns {string} Generated HTML
       */
      this.th = function(contents, attributes) {
        return generateElement('th', contents, attributes);
      };

      /**
       * Generate <td> HTML element
       *
       * @param contents {string} Inner content of the div element
       * @param attributes {Object,optional} Element attributes in key: value Object form.
       * @returns {string} Generated HTML
       */
      this.td = function(contents, attributes) {
        return generateElement('td', contents, attributes);
      };

      /**
       * Generate <img> HTML element
       *
       * @param attributes {Object,optional} Element attributes in key: value Object form.
       * @returns {string} Generated HTML
       */
      this.img = function(attributes) {
        return generateElement('img', null, attributes, true);
      };

      /**
       * Generate <a> HTML element
       *
       * @param contents {string} Inner content of the div element
       * @param attributes {Object,optional} Element attributes in key: value Object form.
       * @returns {string} Generated HTML
       */
      this.anchor = function(contents, attritbutes) {
        return generateElement('a', contents, attritbutes);
      };

      return this;
    }
  );
