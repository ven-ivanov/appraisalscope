'use strict';

var app = angular.module('frontendApp');

app.directive('asTabs', [function () {
  return {
    templateUrl: '/shared/components/tabs/directives/partials/tabs.html',
    scope: {
      tabs: '=',
      tabConfig: '=',
      selectedTab: '=',
      tabWidth: '@',
      changeTab: '&?'
    },
    link: function (scope, elem, attrs) {
      // Change tab
      scope.switchTab = function (tab) {
        scope.selectedTab = tab;
        // If we need to call something on the controller, do so
        if (attrs.changeTab) {
          scope.changeTab({tab: tab});
        }
      };
      // Default to the first tab, if one isn't specified to be selected
      scope.selectedTab = scope.selectedTab || scope.tabs[0];
    }
  };
}]);