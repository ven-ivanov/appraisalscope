'use strict';

var app = angular.module('frontendApp');

/**
 * Display an information modal - does not have a submit button, only "OK"
 */
app.directive('infoModal', ['ModalInheritanceService', function (ModalInheritanceService) {
  return {
    templateUrl: '/shared/components/info-modal/views/modal.html',
    transclude: true,
    scope: {
      modalTitle: '@',
      modalId: '@',
      buttonText: '@',
      modalClass: '@',
      modalWidth: '@',
      modalHeight: '@'
    },
    link: function (scope, element, attrs) {
      // Dynamically set class
      scope.modalClass = scope.modalClass || 'new-window-overlay';
      // Apply the hide and show modal methods to the scope
      ModalInheritanceService.inheritModal.call(null, scope, element, attrs);
    }
  };
}]);