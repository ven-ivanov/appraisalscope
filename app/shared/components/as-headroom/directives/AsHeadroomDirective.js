/*global Headroom:false */
'use strict';

var app = angular.module('frontendApp');

/**
 * Attach and initiate headroom.js for selected items
 */
app.directive('asHeadroom', ['DomLoadService', function (DomLoadService) {
  return function (scope, element, attrs) {
    /**
     * Wait for all pending request to be completed (template URLs, ng-includes, etc)
     */
    DomLoadService.load().then(function () {
      scope.useHeadroom = true;
      // Headroom starting options
      scope.headroomStartingOptions = {
        offset: 0,
        tolerance: 50,
        classes: {
          initial: 'slide',
          pinned: 'header-shown',
          unpinned: 'header-hidden'
        }
      };
      // Headroom hidden options (for reattaching when scrolling down)
      scope.headroomHideOptions = {
        offset: 0,
        tolerance: 50,
        classes: {
          initial: 'header-hidden',
          pinned: 'header-shown',
          unpinned: 'header-hidden'
        }
      };

      // Make sure we have selectors
      var headroomSelectors = [];
      scope.headroomItems = [];

      // Get all elements
      if (attrs.headroomSelectors.indexOf(',') === -1) {
        // Make sure we have valid selectors
        if (!angular.element(attrs.headroomSelectors).length) {
          throw Error('Invalid headroom selector: ' + attrs.headroomSelectors);
        }
        headroomSelectors.push(attrs.headroomSelectors);
      } else {
        attrs.headroomSelectors.split(',').forEach(function (headroomItem) {
          // Make sure we have valid selectors
          if (!angular.element(headroomItem).length) {
            throw Error('Invalid headroom selector: ' + headroomItem);
          }
          headroomSelectors.push(headroomItem);
        });
      }
      /**
       * Create array of each headroom instances and the elements attached
       */
      headroomSelectors.forEach(function (headroomElement) {
        // Get elements
        scope.headroomItems.push({
          elem: document.querySelector(headroomElement)
        });
        // Initiate headroom for each item, keeping reference to element and Headroom instance
        scope.headroomItems[scope.headroomItems.length - 1].headroom =
        new Headroom(scope.headroomItems[scope.headroomItems.length - 1].elem, scope.headroomStartingOptions);
        scope.headroomItems[scope.headroomItems.length - 1].headroom.init();
      });
    });
  };
}]);