'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:asSortable
 * @description
 * Create a sortable list of values from provided array
 *
 * Usage:
 * <as-sortable ng-model="anArray"/>
 */
angular.module('frontendApp')
  .directive('asSortable', function () {
    return {
    	restrict: 'E',
      require: 'ngModel',
      scope: {
        ngModel: '='
      },
      replace: true,
      templateUrl: '/shared/components/as-sortable/views/list.html',
      link: function (scope, element, attrs, ngModel) {

        var generateId = function () {
          var idNumber = 0;
          while(angular.element('#asSortable' + idNumber).length > 0) {
            idNumber++;
          }
          return idNumber;
        };

        var id = 'asSortable' + generateId();
        element.attr('id', id);

        /* Dragula plugin requires containser ids to be passed as variables. */
        /* global dragula */
        /* jslint evil: true */
        var drake = dragula([eval(id)], {
          direction: 'vertical'
        });

        drake.on('drop', function (el, container) {
        	var list = angular.element(container).context.children;
        	var newArray = [];

        	angular.forEach(list, function (item) {
        		newArray.push(angular.element(item).data('$ngModelController').$viewValue);
        	});

        	ngModel.$setViewValue(newArray);
        });
      }
    };
  });
