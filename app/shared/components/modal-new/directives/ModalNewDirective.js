'use strict';

var app = angular.module('frontendApp');

app.directive('modalNew', ['$timeout', 'ModalInheritanceService', function ($timeout, ModalInheritanceService) {
  return {
    templateUrl: '/shared/components/modal-new/views/modal.html',
    transclude: true,
    scope: {
      submitButton: '&',
      modalTitle: '@',
      modalId: '@',
      modalClass: '@',
      modalWidth: '@',
      modalHeight: '@',
      // Submit disabled function
      disabled: '&'
    },
    link: function (scope, element, attrs) {
      // Dynamically set class
      scope.modalClass = scope.modalClass || 'new-window-overlay';
      // Apply show and hide methods to the scope
      ModalInheritanceService.inheritModal.call(this, scope, element, attrs);
      // Disable submit button on load
      $timeout(function () {
        if ('modalForm' in scope && typeof scope.modalForm !== 'undefined' && 'valid' in scope.modalForm &&
            !scope.modalForm.$valid) {
          angular.element(element).find('button[type="submit"]').attr('disabled', true);
        }
      });
      // When form becomes valid, allow the user to submit
      scope.$watchCollection('modalForm', function (newVal) {
        // Don't run on $destroy
        if (!newVal) {
          return;
        }
        // Disable submit while form is invalid
        if (scope.modalForm.$valid) {
          angular.element(element).find('button[type="submit"]').attr('disabled', false);
        } else {
          angular.element(element).find('button[type="submit"]').attr('disabled', true);
        }
      });
    }
  };
}]);