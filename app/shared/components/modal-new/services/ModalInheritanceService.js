'use strict';

var app = angular.module('frontendApp');

app.factory('ModalInheritanceService', ['$timeout', function ($timeout) {
  return {
    inheritModal: function (scope, element, attrs) {
      /**
       * Show one of the modals
       * @param selector
       */
      if (!angular.isFunction(scope.showModal)) {
        scope.showModal = function (selector, front) {
          // Display modal
          var modal = angular.element('#' + selector);
          modal.modal('show');
          // Move the modal in front of any others
          if (front) {
            var initialZIndex = modal.css('z-index');
            modal.css('z-index', 9999);
            // Restore initial z-index
            modal.on('hidden.bs.modal', function () {
              modal.css('z-index', initialZIndex);
            });
          }
          // Allow cleanup to finish, then broadcast finished
          angular.element('#' + scope.modalId).on('hidden.bs.modal', function (param) {
            $timeout(function () {
              // Target the actual bootstrap element target
              scope.$emit('modal-closed-' + angular.element(param.target).attr('id'));
            });
          });
        };
      }

      /**
       * Hide the modal
       * @param selector
       */
      if (!angular.isFunction(scope.hideModal)) {
        scope.hideModal = function (selector) {
          angular.element('#' + selector).modal('hide');
        };
      }

      /**
       * Adjust modal width if defined
       */
      if (scope.modalWidth) {
        $timeout(function () {
          angular.element(element).find('.modal-dialog:first').width(scope.modalWidth);
        });
      }
      /**
       * Adjust modal height
       */
      if (scope.modalHeight) {
        $timeout(function () {
          if (scope.modalHeight === 'max') {
            angular.element(element).find('.modal-body:first').css('min-height', window.innerHeight + 'px');
          } else {
            angular.element(element).find('.modal-body:first').css('min-height', scope.modalHeight);
          }
        });
      }
      /**
       * Keep the .modal-open class on the document if multiple modals will be displayed
       * @param modalId
       */
      var keepModalOpen = function (modalId) {
        angular.element('#' + modalId).on('hidden.bs.modal', function () {
          // Add class on digest cycle
          scope.$apply(function () {
            angular.element('body').addClass('modal-open');
          });
        });
      };
      /**
       * Directly select only this modal to close, otherwise there's interference with multiple modals
       */
      scope.closeModal = function () {
        scope.$broadcast('hide-modal', scope.modalId);
      };
      // Hide modal
      scope.$on('hide-modal', function (event, modalId) {
        scope.hideModal(modalId);
      });
      // Listen for broadcast to display or hide modal windows
      scope.$on('show-modal', function (event, modalId, front) {
        scope.showModal(modalId, front === true);
        if (front) {
          keepModalOpen(modalId);
        }
      });
      // Add modal-open class to body (for when multiple modals are open and one is closed, in which case Bootstrap removes it)
      scope.$on('keep-modal-open', function (event, modalId) {
        keepModalOpen(modalId);
      });

      /**
       * Conditionally disable submit
       */
      if (!attrs.disabled) {
        scope.disabled = function () {
          return false;
        };
      }
    }
  };
}]);