'use strict';

var app = angular.module('frontendApp');

/**
 * Modal for handling file uploads
 */
app.directive('uploadModal', ['UploadModalService', 'ngProgress', function (UploadModalService, ngProgress) {
  return {
    scope: {
      // Modal ID
      modalId: '@',
      // Modal title
      modalTitle: '@',
      // Upload instructions
      modalInstructions: '@',
      // Model holding reference to the file to be uploaded
      fileModel: '=',
      // URL where the file should get uploaded to
      uploadUrl: '@',
      // Success callback
      success: '&',
      // Request method
      method: '@',
      // Disable
      disabled: '&'
    },
    transclude: true,
    templateUrl: '/shared/components/uploadModal/directives/partials/upload-modal.html',
    link: function (scope, element, attrs) {
      // Set a default for instructions
      if (!attrs.modalInstructions) {
        scope.modalInstructions = 'Drop or click here to upload file';
      }
      // Disabled inactive by default
      scope.disabled = scope.disabled || angular.noop;

      /**
       * Handle file upload
       */
      scope.uploadFile = function (file) {
        // Only run with an actual file
        if (!file) {
          return;
        }
        ngProgress.start();
        // Upload file
        UploadModalService.uploadFile(file, scope.uploadUrl, scope.method).promise.then(function (response) {
          // Response
          scope.$broadcast('hide-modal', scope.modalId);
          scope.$broadcast('show-modal', scope.modalId + '-success', true);
          // Trigger callback
          if (scope.success) {
            scope.success({response: response.data});
          }
          // Remove file model
          scope.fileModel = null;
        })
        .finally(function () {
          ngProgress.complete();
        })
        .catch(function () {
          // Failure
          scope.$broadcast('show-modal', scope.modalId + '-failure', true);
        });
      };
    }
  };
}]);