'use strict';

var app = angular.module('frontendApp');

/**
 * Handle file uploads
 */
app.factory('UploadModalService', ['API_PREFIX', 'FileUploader', function (API_PREFIX, FileUploader) {
  return {
    /**
     * Actual file upload
     */
    uploadFile: function (file, url, method) {
      method = method || 'PUT';
      return FileUploader.upload({
        url: API_PREFIX() + '/v2.0/' + url,
        method: method,
        file: file
      });
    }
  };
}]);