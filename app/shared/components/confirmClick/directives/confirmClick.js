'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:modal
 * @description
 * Wrap provided html in bootrap modal
 *
 * Usage:
 * <modal title="">...</modal>
 */
angular.module('frontendApp')
  .directive('confirmClick', function () {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        
        element.bind('click', function() {
          var message = attrs.ngConfirmMessage || 'Are you sure?';

          /*global confirm */
          if (confirm(message)) {
            scope.$apply(attrs.confirmClick);
          }
        });
      }
    };
  });
