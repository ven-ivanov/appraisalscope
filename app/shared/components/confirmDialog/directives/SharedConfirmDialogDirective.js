'use strict';

/**
 *
 */
angular.module('frontendApp')
  .directive('sharedConfirmDialog', function() {
    return {
      scope: {
        show: '=',
        onConfirm: '=',
        onCancel: '=?'
      },
      transclude: true,
      templateUrl: '/shared/components/confirmDialog/views/confirmDialog.html'
    };
  }
);
