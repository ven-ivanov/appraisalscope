'use strict';

/**
 * @ngdoc function
 * @name as-tree-view.directive:asTreeViewRepeater
 * @description
 * # asTreeViewRepeater
 */
(function (ng, undefined){
  ng.module('as-tree-view')
    .directive('asTreeViewRepeater', [ '$timeout', 'tvConfig', function ($timeout, tvConfig) {
      return {
        restrict: 'A',
        link : function(scope){
          // checking if ng-repeat got to the last element and setting a timeout to queue the emit after other event finished.
          if(scope.$last){
            $timeout(function(){

              // emitting repeaterEnd
              scope.$emit(tvConfig.repeaterEnd,true);
            },0);
          }
        }
      };
    }]);
})(angular);