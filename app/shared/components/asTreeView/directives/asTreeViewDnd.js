'use strict';

/**
 * @ngdoc function
 * @name as-tree-view.directive:asTreeViewDnd
 * @description
 * # asTreeViewDnd
 */

/* global dragula:false */
(function (ng, undefined){
  ng.module('as-tree-view')
    .directive('asTreeViewDnd', ['$rootScope', 'tvConfig', 'Collection',
      function ($rootScope,tvConfig, Collection) {
      return {
        restrict: 'A',
        link : function(scope){

          /**
           * When done is done loading we bind drag and drop.
           *
           * @accepts nothing
           * @returns nothing
           * @returns {boolean}
           */
          function onRepeaterEnd(){
            // defining opts
            var opts = scope.asTreeViewOpts.dndOpts || {};

            // checking if dnd is enabled.
            if(!opts.enable) {
              return false;
            }
            var containers, connectWithSelectors, options, drak, connectWith, disabledClassName;

            // defining disabled class
            disabledClassName = 'dnd-disabled';

            // object caching / defining connectWith
            connectWith = opts.connectWith;

            // checking if connect with is defined and if is not an array we throw an error.
            if(connectWith && !ng.isArray(connectWith)){
              throw new Error('connectWith was found but is not an array');
            }

            /**
             * Check if target we are trying to move has any of the class name
             *
             * @param {node} el
             * @param {node} container
             * @param {node} handle
             * @returns {boolean}
             */
             function isConnectWithContainerMove (el, container, handle){
              var i, l, className;
              i = 0;
              l = connectWith.length;
              for(i;i<l;i++){
                className = connectWith[i].className;
                if(ng.element(handle).hasClass(className) || ng.element(el).hasClass(className) || ng.element(container).hasClass(className)){
                  return true;
                }
              }
              return false;
            }

            /**
             * Just like normal indexOf only this goes trough
             * connection and check if a given node has the connection class name
             *
             * @param {node} lookIn
             * @returns {number}
             */
            function indexOfConnection (lookIn){
              var i, l, className;
              i = 0;
              l = connectWith.length;
              for(i;i<l;i++){
                className = connectWith[i].className;
                if(ng.element(lookIn).hasClass(className)){
                  return i;
                }
              }
              return -1;
            }

            /**
             * Basic dragula options , only change is in move and acceptes.
             *
             */
            options = {
              moves: function (el, container, handle) {
                if(ng.element(handle).hasClass(disabledClassName) || ng.element(el).hasClass(disabledClassName)){
                  return false;
                }

                return !(connectWith.length && isConnectWithContainerMove(el, container, handle));
              },
              accepts: function (el, target) {
                return indexOfConnection(target) > -1;
              },
              direction: 'horizontal',
              copy: true,
              revertOnSpill: true,
              removeOnSpill: false
            };

            /**
             * Holds as string of connect with selectors e.g:
             * '.selector-1, #selector-2..'
             * @type {string}
             */
            connectWithSelectors = '';

            // checking if we have connectWith and if its length is bigger then 0
            if(connectWith && connectWith.length){

              // mapping connectWith to a flat array with only selectors strings - using either selector or className
              // then joining the array into a single string.
              connectWithSelectors = ', ' + (connectWith.map(function(connection){
                return connection.selector || '.' + connection.className ;
              }).join(', '));
            }

            // selecting the above created string and turning it into an array instead of nodelist
            containers = Array.prototype.slice.apply(document.querySelectorAll('.tree-view-file' + connectWithSelectors));

            // instantiating dragula supplying the selected containers and options.
            drak = dragula(containers, options || {});

            /**
             * takes collection change object the name of the collection to alter
             * starts a Collection helper and perform the action specified in collectionChange
             *
             * @param {object} collectionChange
             * @param {string} colName
             * @returns {boolean}
             */
            function changeCollection(collectionChange, colName){
              var defaultCollectionConfig, collectionHelper, colIdProp;
              colIdProp = collectionChange.colIdProp || 'name';
              defaultCollectionConfig = { unique: true, collection: scope.asTreeViewCollectionsList[colName]};
              if(colIdProp){
                defaultCollectionConfig.prop = colIdProp;
              }
              collectionHelper = new Collection(defaultCollectionConfig);
              if(!ng.isFunction(collectionHelper[collectionChange.method])) {
                return false;
              }
              collectionHelper[collectionChange.method].apply(collectionHelper, collectionChange.args);
            }

            /**
             * will be invoked when drop event fires.
             *
             * @param {node} el
             * @param {node} container
             * @returns {boolean}
             */
            function drop(el, container){
              var elm, column, index, connection, requiredChange;

              // wrapping el node in angular element.
              elm = ng.element(el);

              // getting the leaf object from the element we just dropped.
              column = elm.data('leaf');

              // getting the index of connectWith so we can use its other properties.
              index = indexOfConnection(container);

              // index wasn't found we return false.
              if(index < -1){
                return false;
              }

              // caching connectWith.
              connection = connectWith[index];

              // if we dropped into sorters add sorters properties.
              if(connection.name === 'sorters'){
                column.predicate = column.name;
                column.reverse = false;
              }

              // if we dropped into groupers add grouper properties.
              if(connection.name === 'groupers'){
                column.reverse = false;
              }

              // defining the required change object
              requiredChange = {method: 'add', prop : connection.name, colIdProp: connection.colIdProp , args: [column]};

              // making changes to the collection we dropped on.
              scope.$apply(changeCollection(requiredChange, connection.name));

              // removing the copied element from DOM.
              ng.element(container).find('.tree-view-item').remove();
            }

            // listening to drop event and binging drop function as handler.
            drak.on('drop', drop);

            // on scope destroy we also destroy dragula.
            scope.$on('$destroy', function() {
              drak.destroy();
            });
          }

          // listening to repeaterEnd so we can start dragula.
          scope.$on(tvConfig.repeaterEnd, onRepeaterEnd);

        }
      };
    }]);
})(angular);