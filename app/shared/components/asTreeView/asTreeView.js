'use strict';

/**
 * @ngdoc function
 * @name as-tree-view.directive:asTreeView
 * @description
 * # asTreeView
 */
(function (ng, undefined){
  ng.module('as-tree-view', [])
    .directive('asTreeView', ['tvConfig', function (tvConfig) {
      return {
        restrict: 'EA',
        templateUrl: '/shared/components/asTreeView/views/asTreeView.html',
        scope: {
          asTreeView: '=',
          asTreeViewLoaded: '=?',
          asTreeViewOpts: '=?',
          asTreeViewCollectionsList: '=?'
        },
        link : function(scope){
          // overriding defaults with scope supplied search
          scope.search = ng.extend(tvConfig.search, scope.asTreeViewOpts.search || {});

          // caching predicate.
          var predicate =  scope.search.predicate;

          /**
           * filtering a child branch by given keyword specified in search.
           *
           * @param {Object} child
           * @returns {boolean}
           */
          scope.childFilter = function(child){
            if(predicate){
              if(!child[predicate]){
                return false;
              }
              return child[predicate].toString().toLowerCase().indexOf(scope.search.value.toLowerCase()) > -1;
            }

            for(var prop in child){
              if(!child.hasOwnProperty(prop)){
                return false;
              }
              if(child[prop].toString().toLowerCase().indexOf(scope.search.value.toLowerCase()) > -1){
                return true;
              }
            }

            return false;
          };

          /**
           * filtering parent branches recursively checking if any of the children returns true and exit
           *
           * @param {Object} parent
           * @returns {boolean}
           */
          scope.recursiveChildFilter = function(parent){
            if(!scope.search.value) {
              return true;
            }

            if(!parent.children || !parent.children.length){
              return scope.childFilter(parent);
            }

            for(var i=0; i < parent.children.length; i++ ){
              if(scope.recursiveChildFilter(parent.children[i])){
                return true;
              }
            }
          };
        }
      };
    }]);
})(angular);