'use strict';

/**
 * @ngdoc function
 * @name as-tree-view.constant:tvConfig
 * @description
 * # tvConfig
 */
(function (ng, undefined){
  ng.module('as-tree-view')
    .constant('tvConfig', {
      /**
       * Search object will get overridden if supplied.
       */
      search: {
        delay: 400,
        predicate : 'text',
        value: ''
      },
      // repeaterEnd event and is fire by asTreeViewRepeater when ng-repeat done manipulating DOM.
      repeaterEnd: 'repeaterEnd'
    });
})(angular);
