'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontendApp
 */
app.controller('AuthCtrl', [
  '$state', '$window', '$location', 'AuthService', 'SessionService',

  function($state, $window, $location, AuthService, SessionService) {
    var vm      = this;
    var storage = $window.localStorage;

    vm.showPage = false;

    // Check if user has an active session on app load. If so, automatically sign user in.
    // Otherwise, the router will take care of redirecting the user to the sign in page.
    if (storage.token) {
      // Verify token validity
      AuthService.getUser().then(function (user) {
        SessionService.create(user);

        // User shouldn't be on the main scope. It's just an empty page
        if ($state.current.name === 'main') {
          if (SessionService.user.typeName === 'admin') {
            $state.transitionTo('main.dashboard');
          } else {
            $state.transitionTo('main.orders');
          }
        }

        if (!vm.showPage) {
          vm.showPage = true;
          $window.loadingScreen.finish();
        }
      }, function () {
        $state.transitionTo('auth.sign-in');

        // Invalid token
        if (!vm.showPage) {
          vm.showPage = true;
          $window.loadingScreen.finish();
        }
      });
    } else {
      // No token found in localStorage
      if (!vm.showPage) {
        vm.showPage = true;
        $window.loadingScreen.finish();
      }

      //$window.location.href = '/sign-in';
      $state.transitionTo('auth.sign-in');
    }
  }
]);
