'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontendApp
 */
app.controller('MainCtrl', [
  '$scope', '$rootScope', '$state', 'ROLES', 'AUTH_EVENTS', 'AuthService', 'SessionService', 'TabsProviderService',
  '$location',

  function ($scope, $rootScope, $state, ROLES, AUTH_EVENTS, AuthService, SessionService, TabsProviderService,
            $location) {
    // @TODO This whole thing is wrong and useless
    $scope.currentUser  = null;
    $scope.renderPage   = false;
    $scope.userTypes    = ROLES;
    $scope.isAuthorized = AuthService.isAuthorized;

    $scope.$watch(function () {
      return SessionService.user;
    }, function (user) {
      $scope.tabs = TabsProviderService.get();

      if (typeof user !== 'undefined') {
        $scope.tabWidth = (100 / $scope.tabs.length) + '%';

        var label = $location.url().split('/')[1];

        $scope.tabs.forEach(function (element, index) {
          if (label === element.label.toLowerCase()) {
            $scope.selectedTab = $scope.tabs[index];
          }
        });

        $scope.renderPage = true;
      }
    });

    $scope.setCurrentUser = function (user) {
      $scope.currentUser = user;
    };

    $scope.changeUserVocation = function(){
      if($scope.currentUser.isOnVacation === 1){
        $scope.currentUser.isOnVacation = 0;
      } else {
        $scope.currentUser.isOnVacation = 1;
      }
    };

    $scope.setSelectedTab = function (tab) {
      $scope.selectedTab = tab;
    };

    $scope.tabClass = function (tab) {
      if ($scope.selectedTab === tab) {
        return 'active';
      } else {
        return '';
      }
    };

    $scope.mobileMenu = false;
    $scope.mobileMenuShow = function(){
        $scope.mobileMenu = !$scope.mobileMenu;
        console.log('mobile menu toggle');
        console.log($scope.mobileMenu);
    };

  }
]);
