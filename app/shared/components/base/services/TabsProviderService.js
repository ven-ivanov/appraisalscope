'use strict';

/**
 * @ngdoc service
 * @name frontendApp.TabsProviderService
 * @description
 * # Provides tab settings based on the current user type
 * Creates the "TabsProviderService" service
 */
angular.module('frontendApp')
  .factory('TabsProviderService', ['SessionService', function (SessionService) {
    return {
      get: function () {
        if (typeof SessionService.user === 'undefined') {
          return [];
        }

        if (SessionService.user.isAdmin()) {
          return [
            {link: 'main.dashboard', label: 'Dashboard', icon: 'assets/images/icons/dashboard.svg'},
            {link: 'main.orders', label: 'Orders', icon: 'assets/images/icons/appraisals.svg'},
            {link: 'main.accounting', label: 'Accounting', icon: 'assets/images/icons/accounting.svg'},
            {link: 'main.users({activeTab: \'client\', recordId: 1, details: \'details\', page: 1})', label: 'Users', icon: 'assets/images/icons/navigation-tab-users.svg'},
            {link: 'main.jobType.listing', label: 'Job Type', icon: 'assets/images/icons/navigation-tab-job-type.svg'},
            {link: 'main.reports', label: 'Reports', icon: 'assets/images/icons/navigation-tab-reports.svg'},
            {link: 'main.settings', label: 'Settings', icon: 'assets/images/icons/settings.svg'},
            {link: 'main.staff', label: 'Staff', icon: 'assets/images/icons/navigation-tab-staff.svg'}
          ];
        }

        if (SessionService.user.isClient()) {
          return [
            {link: 'main.orders', label: 'Orders', icon: 'assets/images/icons/appraisals.svg'},
            {link: 'main.profile.show', label: 'Profile', icon: 'assets/images/icons/accounting.svg'},
            {link: 'main.contactus', label: 'Contact Us', icon: 'assets/images/icons/accounting.svg'}
          ];
        }

        return [];
      }
    };
  }]);
