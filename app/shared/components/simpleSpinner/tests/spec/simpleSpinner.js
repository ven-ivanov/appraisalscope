'use strict';

describe('Directive: simpleSpinner', function () {

  // load the directive's module
  beforeEach(module('frontendApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should display spinning animated icon at default size', inject(function ($compile) {
    element = angular.element('<simple-spinner></simple-spinner>');
    element = $compile(element)(scope);
    expect(element.html()).toBe('<i class="fa fa-2x fa-spinner fa-spin"></i>');
  }));

  it('should display spinning animated icon 5x larger', inject(function ($compile) {
    element = angular.element('<simple-spinner size="5x"></simple-spinner>');
    element = $compile(element)(scope);
    expect(element.html()).toBe('<i class="fa fa-5x fa-spinner fa-spin"></i>');
  }));
});
