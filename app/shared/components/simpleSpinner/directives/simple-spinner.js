'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:simpleSpinner
 * @description
 * # Simple spinner show an animated spinner while performing a time consuming operation (AJAX called mostly.)
 * # Usage:
 * # <simple-spinner ng-hide="dataLoaded"></simple-spinner>
 * # Where dataLoaded is a scope variable set inside the success promise.
 * # You can increase icon size relative to their container, use size attribute with lg (33% increase), 2x, 3x, 4x, or 5x values.
 * # <simple-spinner size="4x"></simple-spinner>
 */
angular.module('frontendApp')
  .directive('simpleSpinner', function () {
    return {
      template: '<div></div>',
      restrict: 'E',
      link: function postLink(scope, element, attr) {
        var size = angular.isUndefined(attr.size) ? ' fa-2x' : ' fa-' + attr.size;

        element.html('<i class="fa' + size + ' fa-spinner fa-spin"></i>');
      }
    };
  });
