'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:addressBasedGoogleMap
 * @description
 * Load Google map based on address
 *
 * Usage:
 * <div addressBasedGoogleMap />
 */
angular.module('frontendApp')
.directive('addressBasedGoogleMap', function () {
  return {
    restrict: 'A',
    template: '<div id="addressMap"></div>',
    scope: {
      address: '=',
      zoom: '=',
      height:'=',
      width:'='
    },
    controller: 'AddressBasedGoogleMapCtrl'
  };
});