'use strict';

angular.module('frontendApp')
.controller('AddressBasedGoogleMapCtrl', ['$scope', 'google', function ($scope, google) {
  var geocoder;
  var latlng;
  var map;
  var marker;
  var initialize = function () {
    geocoder = new google.maps.Geocoder();
    latlng = new google.maps.LatLng(-34.397, 150.644);
    var mapOptions = {
      zoom: $scope.zoom,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var regNum = '/^[0-9]+$/';

    // Check if Height
    if($scope.height !== undefined) {
      if(($scope.height).toString().match(regNum)) {
        document.getElementById('addressMap').style.height = 'auto';
      }
      else {
        document.getElementById('addressMap').style.height = $scope.height + 'px';
      }
    }
    else {
      document.getElementById('addressMap').style.height = 'auto';
    }

    // Check if width
    if($scope.width !== undefined) {
      if(($scope.width).toString().match(regNum)) {
        document.getElementById('addressMap').style.width = 'auto';
      }
      else {
        document.getElementById('addressMap').style.width = $scope.width + 'px';
      }
    }
    else {
      document.getElementById('addressMap').style.width = 'auto';
    }
    map = new google.maps.Map(document.getElementById('addressMap'), mapOptions);
  };
  var markAdressToMap = function () {
    geocoder.geocode({ 'address': $scope.address }, function (results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
        });
      }
    });
  };
  $scope.$watch('address', function () {
    if ($scope.address !== undefined) {
      markAdressToMap();
    }
  });
  initialize();
}]);
