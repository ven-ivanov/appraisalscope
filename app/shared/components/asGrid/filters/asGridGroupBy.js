'use strict';

/**
 * @ngdoc function
 * @name as-grid.filter:asGroupBy
 * @description
 * # asGridGroupBy
 */
(function (ng, undefined){
  ng.module('as-grid')
    .filter('asGridGroupBy', function() {
      return function(rows, groupers) {
        var rowsCp, r, rl, g, gl, name, gName, groups = [], groupObj = {};
        rowsCp = ng.copy(rows)|| [];
        groupers = groupers || [];
        r = 0;
        rl = rowsCp.length;
        for (r; r < rl; r++) {
          g = 0;
          gl = groupers.length;
          name = [];
          for (g; g < gl; g++) {
            name.push((groupers[g].nameAs || groupers[g].name) + ': ' + rowsCp[r][groupers[g].name]);
          }


          gName = name.join(' | ');
          if( groups.indexOf(gName) === -1) {
            groupObj[gName] = groups.push(gName);
          }

          rowsCp[r]._groupBy = gName;
          rowsCp[r]._groupByClass = 'group_' + groupObj[gName].toString();
        }
        return rowsCp;
      };
    });
})(angular);
