'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:SgTotalAggregator
 * @description
 * # SgTotalAggregator
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('SgTotalAggregator', [function () {

      function SgTotalAggregator(colName){
        this.colName = colName;
        this.name = 'total';
        this.nameAs = 'Total';
        this.sum = 0;
        this.count = 0;
      }

      SgTotalAggregator.prototype.next = function(nextVal){
        this.sum += nextVal;
      };

      return SgTotalAggregator;
    }]);
})(angular);