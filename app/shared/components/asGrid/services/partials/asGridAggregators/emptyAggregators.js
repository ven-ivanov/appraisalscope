'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:SgEmptyAggregator
 * @description
 * # SgEmptyAggregator
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('SgEmptyAggregator', [function () {

      function SgEmptyAggregator(colName){
        this.colName = colName;
        this.name = 'empty';
        this.nameAs = 'Empty';
      }

      SgEmptyAggregator.prototype.next = function(){};

      return SgEmptyAggregator;
    }]);
})(angular);