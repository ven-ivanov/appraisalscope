'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:SgAverageAggregator
 * @description
 * # SgAverageAggregator
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('SgAverageAggregator', [function () {

      function SgAverageAggregator(colName){
        this.colName = colName;
        this.name = 'average';
        this.nameAs = 'Average';
        this.total = 0;
        this.sum = 0;
        this.count = 0;
      }

      SgAverageAggregator.prototype.next = function(nextVal){
        this.total += nextVal;
        this.count++;
        this.sum = this.total / this.count;
      };

      return SgAverageAggregator;
    }]);
})(angular);