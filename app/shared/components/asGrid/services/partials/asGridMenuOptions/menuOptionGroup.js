'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:MoGroup
 * @description
 * # MoGroup
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('MoGroup', ['MoSuper', 'sgConfig', 'Utils', function (MoSuper, sgConfig, Utils) {

      function MoGroup(){
        MoSuper.apply(this,arguments);
      }
      MoGroup.prototype = Object.create(MoSuper.prototype);
      MoGroup.prototype.constructor = MoGroup;

      MoGroup.prototype.getIconClassName = function(){ return 'check-able-option'; };
      MoGroup.prototype.menuItemSelected = function(selectedOption, scope, sgState, column, selectedKey){
        var msg = [
          this.getGroupQuery(selectedOption, scope, sgState, column, selectedKey)
        ];
        scope.$emit(sgConfig.requestGridStateChange, msg);
      };
      MoGroup.prototype.isChecked = function(selectedOption, scope, sgState, column){
        return Utils.indexOf(sgState.groupers, column.name, 'name') > -1;
      };
      MoGroup.prototype.getSpecificTemplate = function(){
        return false;
      };
      MoGroup.prototype.hasSpecificTemplate = function(){
        return false;
      };

      MoGroup.prototype.getGroupQuery = function(selectedOption, scope, sgState, column, selectedKey){
        var action, grouper;
        action = 'remove';
        grouper = {name : column.name, nameAs:column.nameAs, reverse : false};
        if(!this.isChecked(selectedOption, scope, sgState, column, selectedKey)){
          action = 'add';
        }

        return {method : 'array', args: [{method: action, prop : 'groupers', args: [grouper]}]};
      };

      return MoGroup;
    }]);
})(angular);