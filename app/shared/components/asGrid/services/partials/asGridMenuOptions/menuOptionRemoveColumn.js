'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:MoRemoveColumn
 * @description
 * # MoRemoveColumn
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('MoRemoveColumn', ['MoSuper', 'MoGroup', 'sgConfig', function (MoSuper, MoGroup, sgConfig) {

      function MoRemoveColumn(){
        MoSuper.apply(this,arguments);
      }
      MoRemoveColumn.prototype = Object.create(MoSuper.prototype);
      MoRemoveColumn.prototype.constructor = MoRemoveColumn;

      MoRemoveColumn.prototype.getIconClassName = function(){ return 'remove-column-option'; };
      MoRemoveColumn.prototype.menuItemSelected = function(selectedOption, scope, sgState, column, selectedKey){
        var msg, moGroup;
        msg = [];
        moGroup = new MoGroup();
        if(moGroup.isChecked(selectedOption, scope, sgState, column, selectedKey)){
          msg.push(moGroup.getGroupQuery(selectedOption, scope, sgState, column, selectedKey));
        }
        msg.push({method : 'array', args: [{method: 'remove', prop : 'columns', args: [column]}]});
        scope.$emit(sgConfig.requestGridStateChange, msg);
      };
      MoRemoveColumn.prototype.isChecked = function(){
        return false;
      };
      MoRemoveColumn.prototype.getSpecificTemplate = function(){
        return false;
      };
      MoRemoveColumn.prototype.hasSpecificTemplate = function(){
        return false;
      };

      return MoRemoveColumn;
    }]);
})(angular);