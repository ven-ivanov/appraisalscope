'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:MoColumns
 * @description
 * # MoColumns
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('MoColumns', ['MoSuper', function (MoSuper) {

      function MoColumns(){
        MoSuper.apply(this,arguments);
      }
      MoColumns.prototype = Object.create(MoSuper.prototype);
      MoColumns.prototype.constructor = MoColumns;

      MoColumns.prototype.getIconClassName = function(){ return 'check-able-option'; };
      MoColumns.prototype.menuItemSelected = function(selectedOption, scope, sgState, column){
        column[selectedOption.name] = !column[selectedOption.name];
      };
      MoColumns.prototype.isChecked = function(selectedOption, scope, sgState, column){
        return column[selectedOption.name] ? true : false;
      };
      MoColumns.prototype.getSpecificTemplate = function(){
        return false;
      };
      MoColumns.prototype.hasSpecificTemplate = function(){
        return false;
      };

      return MoColumns;
    }]);
})(angular);