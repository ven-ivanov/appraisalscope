'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:MoSortAsc
 * @description
 * # MoSortAsc
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('MoSortAsc', ['MoSuper', 'sgConfig', 'Utils', function (MoSuper, sgConfig, Utils) {

      function MoSortAsc(){
        MoSuper.apply(this,arguments);
      }
      MoSortAsc.prototype = Object.create(MoSuper.prototype);
      MoSortAsc.prototype.constructor = MoSortAsc;

      MoSortAsc.prototype.getIconClassName = function(){ return 'check-able-option'; };
      MoSortAsc.prototype.menuItemSelected = function(selectedOption, scope, sgState, column, selectedKey){
        var sorter, msg;
        sorter = {name : column.name, nameAs: column.nameAs, predicate : column.name, reverse: false };
        msg = [ {method : 'array', args: [{method: 'remove', prop : 'sorters', args: [sorter]}]} ];
        if(!this.isChecked(selectedOption, scope, sgState, column, selectedKey)){
          msg.push({method : 'array', args: [{method: 'add', prop : 'sorters', args: [sorter]}]});
        }


        scope.$emit(sgConfig.requestGridStateChange, msg);
      };
      MoSortAsc.prototype.isChecked = function(selectedOption, scope, sgState, column){
        var index = Utils.indexOf(sgState.sorters, column.name, 'name');
        if(index <= -1) {
          return false;
        }
        return sgState.sorters[index].reverse === false;
      };
      MoSortAsc.prototype.getSpecificTemplate = function(){
        return false;
      };
      MoSortAsc.prototype.hasSpecificTemplate = function(){
        return false;
      };

      return MoSortAsc;
    }]);
})(angular);