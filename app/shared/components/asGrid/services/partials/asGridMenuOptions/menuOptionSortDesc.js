'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:MoSortDesc
 * @description
 * # MoSortDesc
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('MoSortDesc', ['MoSuper', 'sgConfig', 'Utils', function (MoSuper, sgConfig, Utils) {

      function MoSortDesc(){
        MoSuper.apply(this,arguments);
      }
      MoSortDesc.prototype = Object.create(MoSuper.prototype);
      MoSortDesc.prototype.constructor = MoSortDesc;

      MoSortDesc.prototype.getIconClassName = function(){ return 'check-able-option'; };
      MoSortDesc.prototype.menuItemSelected = function(selectedOption, scope, sgState, column, selectedKey){
        var sorter, msg;
        sorter = {name : column.name, nameAs: column.nameAs, predicate : column.name, reverse: true };
        msg = [ {method : 'array', args: [{method: 'remove', prop : 'sorters', args: [sorter]}]} ];
        if(!this.isChecked(selectedOption, scope, sgState, column, selectedKey)){
          msg.push({method : 'array', args: [{method: 'add', prop : 'sorters', args: [sorter]}]});
        }
        scope.$emit(sgConfig.requestGridStateChange, msg);
      };
      MoSortDesc.prototype.isChecked = function(selectedOption, scope, sgState, column){
        var index = Utils.indexOf(sgState.sorters, column.name, 'name');
        if(index <= -1) {
          return false;
        }
        return sgState.sorters[index].reverse === true;
      };
      MoSortDesc.prototype.getSpecificTemplate = function(){
        return false;
      };
      MoSortDesc.prototype.hasSpecificTemplate = function(){
        return false;
      };

      return MoSortDesc;
    }]);
})(angular);