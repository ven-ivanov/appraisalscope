'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:MoRenameColumn
 * @description
 * # MoRenameColumn
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('MoRenameColumn', ['Utils','MoSuper', function (Utils, MoSuper) {

      function MoRenameColumn(){
        MoSuper.apply(this,arguments);
      }
      MoRenameColumn.prototype = Object.create(MoSuper.prototype);
      MoRenameColumn.prototype.constructor = MoRenameColumn;

      MoRenameColumn.prototype.getIconClassName = function(){ return 'rename-column-option'; };
      MoRenameColumn.prototype.menuItemSelected = function(selectedOption, scope, sgState, column){
        var prevName = column.name, index;
        column.nameAs = scope.renameColumn;

        index = Utils.indexOf(sgState.sorters, prevName, 'name');
        if(index > -1) {
          sgState.sorters[index].nameAs = column.nameAs;
        }

        index = Utils.indexOf(sgState.groupers, prevName, 'name');
        if(index > -1) {
          sgState.groupers[index].nameAs = column.nameAs;
        }

        scope.renameColumn = '';
      };
      MoRenameColumn.prototype.isChecked = function(){
        return false;
      };
      MoRenameColumn.prototype.getSpecificTemplate = function(){
        return '<input class="form-control" ng-model="renameColumn"/>';
      };
      MoRenameColumn.prototype.hasSpecificTemplate = function(){
        return true;
      };

      return MoRenameColumn;
    }]);
})(angular);