'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:MoShowTime
 * @description
 * # MoShowTime
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('MoShowTime', ['MoSuper', function (MoSuper) {

      function MoShowTime(){
        MoSuper.apply(this,arguments);
      }
      MoShowTime.prototype = Object.create(MoSuper.prototype);
      MoShowTime.prototype.constructor = MoShowTime;

      return MoShowTime;
    }]);
})(angular);