'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:MoTotal
 * @description
 * # MoTotal
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('MoTotal', ['MoSuper', function (MoSuper) {

      function MoTotal(){
        MoSuper.apply(this,arguments);
      }
      MoTotal.prototype = Object.create(MoSuper.prototype);
      MoTotal.prototype.constructor = MoTotal;

      return MoTotal;
    }]);
})(angular);