'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:MoSuper
 * @description
 * # MoSuper
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('MoSuper',
    function () {
      function MoSuper(){}
      MoSuper.prototype.getIconClassName = function(){ return 'check-able-option'; };
      MoSuper.prototype.menuItemSelected = function(selectedOption, scope, sgState, column){
        column[selectedOption.name] = !column[selectedOption.name];
      };
      MoSuper.prototype.isChecked = function(selectedOption, scope, sgState, column){
        return column[selectedOption.name] ? true : false;
      };
      MoSuper.prototype.getSpecificTemplate = function(){
        return false;
      };
      MoSuper.prototype.hasSpecificTemplate = function(){
        return false;
      };

      return MoSuper;
    }
  );
})(angular);