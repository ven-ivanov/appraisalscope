'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:MoFilter
 * @description
 * # MoFilter
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('MoFilter', ['MoSuper', function (MoSuper) {

      function MoFilter(){
        MoSuper.apply(this,arguments);
      }
      MoFilter.prototype = Object.create(MoSuper.prototype);
      MoFilter.prototype.constructor = MoFilter;

      MoFilter.prototype.getIconClassName = function(){ return 'check-able-option'; };
      MoFilter.prototype.menuItemSelected = function(selectedOption, scope, sgState, column){
        column[selectedOption.name] = !column[selectedOption.name];
      };
      MoFilter.prototype.isChecked = function(selectedOption, scope, sgState, column){
        return column[selectedOption.name] ? true : false;
      };
      MoFilter.prototype.getSpecificTemplate = function(){
        return false;
      };
      MoFilter.prototype.hasSpecificTemplate = function(){
        return false;
      };

      return MoFilter;
    }]);
})(angular);