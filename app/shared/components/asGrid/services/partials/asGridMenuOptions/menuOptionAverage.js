'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:MoAverage
 * @description
 * # MoAverage
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('MoAverage', ['MoSuper', function (MoSuper) {

      function MoAverage(){
        MoSuper.apply(this,arguments);
      }
      MoAverage.prototype = Object.create(MoSuper.prototype);
      MoAverage.prototype.constructor = MoAverage;

      return MoAverage;
    }]);
})(angular);