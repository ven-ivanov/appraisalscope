'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:asGridDecorator
 * @description
 * # asGridDecorator
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('asGridDecorator', [
      function () {

        return {
          decorate : function(deco, def){

            /**
             * Holds Defaults.
             *
             * @type {Object}
             * @api public
             */
            def = def || {};

            /**
             * Holds data.
             *
             * @type {Array}
             * @api public
             */
            deco.data = [];

            /**
             * Holds isLoading
             *
             * @type {Boolean}
             * @api public
             */
            deco.isLoading = false;

            /**
             * Holds gridState
             *
             * @type {Object}
             * @api public
             */
            deco.gridState = {};

            /**
             * Holds how many rows per page
             *
             * @type {Number}
             * @api public
             */
            deco.perPage = def.perPage || 10;

            /**
             * RetrieveData
             *
             * @api public
             */
            deco.retrieveData = function(){ };

            /**
             * loading table data
             *
             * @param {Object} gridState
             * @api public
             */
            deco.loadData = function(gridState){

              // Flag to prevent smart-table from calling loadData twice
              if(deco.isLoading) {
                return false;
              }

              // setting loading true to indicate some action happened
              deco.isLoading = true;

              // setting tableState
              deco.gridState = gridState;

              var success, fail, pagination;

              // query succeeded.
              success = function(response){
                pagination = response.meta.pagination;
                deco.numberOfPages = pagination.totalPages;
                deco.data = response.data;
                gridState.pagination = pagination;
                gridState.pagination.start = Math.max((pagination.page-1)*pagination.perPage, 0);
                gridState.hasAggregators = response.meta.hasAggregators;
                gridState.aggregators = response.meta.aggregators;
                deco.isLoading = false;
              };

              // query failed.
              fail = function(){ deco.isLoading = false; };

              // retrieving data.
              return deco.retrieveData(gridState).then(success).catch(fail);
            };

            /**
             * getting selected rows
             *
             * @api public
             */
            deco.getSelected = function(){
              deco.selected = deco.data.filter(function(row){ return row.isSelected; });
              return  deco.selected;
            };

            return deco;
          }
        };
      }
    ]);
})(angular);