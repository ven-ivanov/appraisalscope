'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:AvailableMenuOptions
 * @description
 * # AvailableMenuOptions
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('AvailableMenuOptions', [
      'MoAverage',
      'MoColumns',
      'MoFilter',
      'MoGroup',
      'MoRemoveColumn',
      'MoRenameColumn',
      'MoShowTime',
      'MoSortAsc',
      'MoSortDesc',
      'MoTotal',

      function (
        MoAverage,
        MoColumns,
        MoFilter,
        MoGroup,
        MoRemoveColumn,
        MoRenameColumn,
        MoShowTime,
        MoSortAsc,
        MoSortDesc,
        MoTotal
        ){

        function AvailableMenuOptions(){
          this.average = new MoAverage();
          this.columns = new MoColumns();
          this.filter = new MoFilter();
          this.group = new MoGroup();
          this.removeColumn = new MoRemoveColumn();
          this.renameColumn = new MoRenameColumn();
          this.showTime = new MoShowTime();
          this.sortAsc = new MoSortAsc();
          this.sortDesc = new MoSortDesc();
          this.total = new MoTotal();
        }

        AvailableMenuOptions.prototype.invokeFn = function(method, selectedOption, scope, sgState, column, selectedKey){
          if(!method || !ng.isFunction(this[selectedOption.name][method])){
            return false;
          }
          return this[selectedOption.name][method](selectedOption, scope, sgState, column, selectedKey);
        };

        return new AvailableMenuOptions();
      }]);
})(angular);