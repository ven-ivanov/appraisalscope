'use strict';

/**
 * @ngdoc function
 * @name as-grid.factory:sgAggregators
 * @description
 * # sgAggregators
 */
(function (ng, undefined){
  ng.module('as-grid')
    .factory('sgAggregators', [ 'sgConfig', 'SgTotalAggregator', 'SgAverageAggregator', 'SgEmptyAggregator',
      function ( sgConfig , SgTotalAggregator, SgAverageAggregator, SgEmptyAggregator) {

        var availableAgg = ['total','average'];

        function getAggregator(aggName, colName){
          switch(aggName){
            case 'total':
              return new SgTotalAggregator(colName);
            case 'average':
              return new SgAverageAggregator(colName);
            case 'empty':
              return new SgEmptyAggregator(colName);
          }
          return false;
        }

        function aggregate (scope, filtered){
          var table, sgState, aggregators = {}, row, r, rl, c, cl, col, sgColumns, a,al,aggType, groupName,grandTotal, autoIncreaseFirstColumn;
          grandTotal = 'grandTotal';
          table = filtered || scope.sgTable;
          sgState = scope.sgState;
          sgColumns = ng.copy(sgState.columns);
          autoIncreaseFirstColumn = sgState.autoIncreaseFirstColumn;

          if(autoIncreaseFirstColumn){
            sgColumns.unshift({name : '_no', nameAs : 'No', type : 'AI'});
          }

          r  = 0;
          rl = table.length;
          for(r; r<rl;r++){
            row = table[r];
            groupName = row._groupByClass;

            c  = 0;
            cl = sgColumns.length;
            for(c; c<cl;c++){
              col = sgColumns[c];

              a=0;
              al = availableAgg.length;
              for(a;a<al;a++){
                aggType = availableAgg[a];
                aggregators[aggType] = aggregators[aggType] || {};
                if(groupName){
                  aggregators[aggType][groupName] = aggregators[aggType][groupName] || [];
                }
                aggregators[aggType][grandTotal] = aggregators[aggType][grandTotal] || [];

                if(col[aggType]){
                  if(groupName){
                    aggregators[aggType][groupName][c] = aggregators[aggType][groupName][c] || getAggregator(aggType, col.name);
                    aggregators[aggType][groupName][c].next(parseFloat(row[col.name]) || 0);
                  }
                  // grandTotal
                  aggregators[aggType][grandTotal][c] = aggregators[aggType][grandTotal][c] || getAggregator(aggType, col.name);
                  aggregators[aggType][grandTotal][c].next(parseFloat(row[col.name]) || 0);
                }else if(col.name === '_no'){
                  if(groupName){
                    aggregators[aggType][groupName][c] = getAggregator('empty', '_no');
                  }
                  // grandTotal
                  aggregators[aggType][grandTotal][c] = getAggregator('empty', '_no');
                }else{
                  if(groupName){
                    aggregators[aggType][groupName][c] = getAggregator('empty', col.name);
                  }
                  //grandTotal
                  aggregators[aggType][grandTotal][c] = getAggregator('empty', col.name);
                }
              }
            }
          }
          return aggregators;
        }

        return {
          availableAggregators : availableAgg,
          aggregate : aggregate
        };
      }]);
})(angular);