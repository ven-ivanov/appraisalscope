'use strict';

/**
 * @ngdoc function
 * @name as-grid.constant:sgConfig
 * @description
 * # sgConfig
 */
(function (ng, undefined){
  ng.module('as-grid')
    .constant('sgConfig', {
      pagination: {
        template: 'shared/components/asGrid/views/asGridPaginationTemplate.html'
      },
      search: {
        delay: 400 // ms
      },
      select: {
        mode: 'single',
        selectedClass: 'sg-selected'
      },
      sort: {
        ascentClass: 'sg-sort-ascent',
        descentClass: 'sg-sort-descent'
      },
      group: {
        closeClass: 'grouper-state-close',
        openClass: 'grouper-state-open'
      },
      requestGridStateChange: 'requestGridStateChange',
      doneChangeBeforePipe: 'doneChangeBeforePipe',
      doneChangeAfterPipe: 'doneChangeAfterPipe'
    });
})(angular);
