'use strict';

/**
 * @ngdoc function
 * @name as-grid.directive:asGrid
 * @description
 * # asGrid
 */
(function (ng, undefined){
  ng.module('as-grid', [])
    .directive('asGrid', ['$parse','sgConfig', 'OrderAbleColl', 'Utils', function ($parse, sgConfig) {
      return {
        restrict: 'EA',
        controller: 'AsGridController',
        templateUrl: '/shared/components/asGrid/views/asGrid.html',
        scope: {
          sgState: '=',
          sgGridState: '=?',
          sgSafeSrc: '=',
          sgTable: '=',
          sgNumberOfPages: '=',
          sgSelected: '=?',
          sgAfterGridStateChange: '=?'
        },
        link: function(scope, element, attrs, ctrl){
          var gridState, lastSelected, allIsSelected = false;
          gridState = scope.sgState;
          lastSelected = {row:undefined, element: undefined};
          scope.selectedClass = sgConfig.select.selectedClass;

          scope.groupStart = false;


          scope.selectAllRows = function selectAllRows () {
            var rows;
            rows = scope.sgTable;
            rows.map(function(item){
              item.isSelected = !allIsSelected;
            });
            allIsSelected = !allIsSelected;
            scope.getSelected();
          };

          scope.selectRow = function selectRow (event, row) {
            var rows, index, ele, isSelected;
            ele = ng.element(event.currentTarget);
            rows = scope.sgTable;
            index = rows.indexOf(row);

            if (index <= -1 || !gridState.select || !ele) {
              return false;
            }

            isSelected = !row.isSelected;

            if (gridState.select === 'single') {

              if (lastSelected.row) {
                lastSelected.row.isSelected = false;
              }
            }

            row.isSelected = isSelected;
            lastSelected = {row:row, element: ele};
            scope.getSelected();
            return true;
          };

          scope.groupStateDefault = gridState.groupStartOpened ? sgConfig.group.openClass : sgConfig.group.closeClass;

          scope.collapseGroup = function collapseGroup (event) {
            var ele, group, parent, id;
            group = sgConfig.group;
            ele = ng.element(event.currentTarget);
            parent = ele.parent().parent();
            id = parent.attr('id');

            if(ele.hasClass(group.openClass)){
              ele.removeClass(group.openClass).addClass(group.closeClass);
              parent.siblings('.'+ id).hide();
            }else{
              ele.removeClass(group.closeClass).addClass(group.openClass);
              parent.siblings('.'+ id).show();
            }

          };
          scope.isNewGroupStart = function isNewGroupStart (key, row) {
            var prevValue, isNewGroup;
            prevValue = scope.sgTable[key-1] && scope.sgTable[key-1]._groupBy ? scope.sgTable[key-1]._groupBy : undefined;
            isNewGroup = row._groupBy && prevValue !== row._groupBy;
            if(isNewGroup) {
              scope.groupStart = key;
            }
            return isNewGroup;
          };

          scope.isNewGroupEnd = function isNewGroupEnd (key, row) {
            var nextValue = scope.sgTable[key+1] && scope.sgTable[key+1]._groupBy ? scope.sgTable[key+1]._groupBy : undefined;
            return row._groupBy && nextValue !== row._groupBy;
          };

          scope.getColSpan = function getColSpan () {
            var length = gridState.columns.length || 0;
            return gridState.autoIncreaseFirstColumn ? length+1 : length;
          };

          scope.getSelected = function(){
            var rows = scope.sgTable, gridState;
            gridState = ctrl.getGridState();
            gridState.selected = rows.filter(function(row){ return row.isSelected; });
            return gridState.selected;
          };
        }
      };
    }]);
})(angular);