'use strict';

/**
 * @ngdoc function
 * @name as-grid.controller:AsGridController
 * @description
 * # AsGridController
 * controller of the AsGridController
 */
(function (ng, undefined){
  ng.module('as-grid')
    .controller('AsGridController', [
      '$rootScope', '$scope', '$parse', '$filter', '$attrs', 'sgConfig', 'OrderAbleColl', 'List', 'sgAggregators', 'Utils',
      function ($rootScope, $scope, $parse, $filter, $attrs, sgConfig, OrderAbleColl, List, sgAggregators, Utils ) {


        var ctrl = this,
          groupBy = $filter('asGridGroupBy'),
          orderBy = $filter('orderBy'),
          filter = $filter('filter'),
          safeCopy,
          filtered,
          pipeAfterSafeCopy = true;

        function updateGridState(gridState) {
          var newGridState = {
            columns: gridState.columns,
            sorters: gridState.sorters,
            groupers: gridState.groupers,
            filters: gridState.filters,
            pagination: gridState.pagination,
            limitRows: gridState.limitRows,
            autoIncreaseFirstColumn: gridState.autoIncreaseFirstColumn
          };

          ctrl.gridState = ng.copy(newGridState);
          if($scope.sgGridState){
            $scope.sgGridState = ng.extend($scope.sgGridState,ctrl.gridState);
          }
        }

        function updateSafeCopy() {
          safeCopy = copyRefs($scope.sgSafeSrc);
          if (pipeAfterSafeCopy === true) {
            ctrl.pipe();
          }
        }

        function copyRefs(src) {
          return src ? [].concat(src) : [];
        }

        safeCopy = copyRefs($scope.sgTable || []);

        var defaultState  = {
          columns: [],
          sorters: [],
          groupers: [],
          filters: {},
          pagination: { start: 0, perPage: +($scope.sgState.perPage || 10) },
          displayedPages: 5,
          perPage: +($scope.sgState.perPage || 10),
          limitRows: false,
          autoIncreaseFirstColumn: true,
          groupStartOpened: true,
          select: 'multiple',
          dnd: false
        };
        $scope.sgState =  ng.copy(ng.extend(defaultState, $scope.sgState));
        updateGridState($scope.sgState);

        $scope.aggregators = {};

        /**
         * this will chain the operations of sorting and filtering based on the current table state (sort options, filtering, ect)
         */
        this.pipe = function pipe () {
          $scope.aggregators = {};
          var pagination = this.gridState.pagination, sorters;

          // filtering
          filtered = this.gridState.filters ? filter(safeCopy, this.gridState.filters) : safeCopy;

          // grouping
          if(ng.isArray(this.gridState.groupers) && this.gridState.groupers.length){
            filtered = groupBy(filtered, this.gridState.groupers);
            sorters = this.addGroupersToSorts(this.gridState.groupers, this.gridState.sorters);
          }

          sorters = angular.isArray(sorters) && sorters.length ? sorters : this.gridState.sorters;
          if (ng.isArray(sorters) && sorters.length) {
            var sorterses = this.getSort(sorters);
            filtered = orderBy(filtered, sorterses);
          }

          if (pagination.perPage !== undefined) {
            $scope.sgNumberOfPages = filtered.length > 0 ? Math.ceil(filtered.length / pagination.perPage) : 1;
            pagination.start = pagination.start >= filtered.length ? ($scope.sgNumberOfPages - 1) * pagination.perPage : pagination.start;
            filtered = filtered.slice(pagination.start, pagination.start + parseInt(pagination.perPage));
          }

          if (this.hasAggregators().any) {
            $scope.aggregators = sgAggregators.aggregate($scope, filtered);
          }

          $scope.sgTable = filtered;

          //console.log('this.gridState',this.gridState, $scope.sgTable);//, $scope.sgTable
        };

        if ($attrs.sgSafeSrc) {
          $scope.$watch(function () {
            return $scope.sgSafeSrc;
          }, function () {
            updateSafeCopy();
          });
        }

        $scope.$watch(function(){
          return ctrl.gridState.hasAggregators;
        }, function hasAggregators(newVal) {
          $scope.hasAggregators = newVal;
          $scope.aggregators = ctrl.gridState.aggregators;
        }, true);

        /**
         * On gridState change we rebuild grid state export
         * @param {Array} newVal - New column value
         * @param {Array} oldVal - Old column value
         */
        $scope.$watch('sgState', function onGridStateChange(newVal, oldVal) {
          console.log('sgState', newVal);
          updateGridState(newVal);
          $scope.$broadcast(sgConfig.doneChangeBeforePipe, ctrl.gridState);
          ctrl.pipe();
          $scope.$broadcast(sgConfig.doneChangeAfterPipe, ctrl.gridState);
          $scope.getSelected();
          if(newVal !== oldVal && $scope.sgAfterGridStateChange && ng.isFunction($scope.sgAfterGridStateChange)) {
            $scope.sgAfterGridStateChange();
          }
        }, true);

        /**
         * On external change to sgState
         * @param {Object} msg - Action to perform on sgState
         */
        $scope.$on(sgConfig.requestGridStateChange, function requestGridStateChange (event, msg) {
          if(!msg || typeof msg !== 'object') {
            return false;
          }
          msg = ng.isArray(msg) ? msg : [msg];
          ng.forEach(msg, ctrl.changeReq);
          try{
            $scope.$digest();
          }catch(e){
            console.log(e);
          }
          return true;
        });

        this.changeReq = function(msg){
          var changeMethod;

          changeMethod = msg.method + 'ChangeReq';
          if(!ctrl.hasOwnProperty(changeMethod)) {
            return false;
          }

          // invoking one of the changeReq method.
          return ctrl[changeMethod].apply(ctrl,msg.args);
        };

        this.arrayChangeReq =  function(msg){
          var defArrConfig, stateArrHelper, colIdProp;
          colIdProp = msg.colIdProp || 'name';
          defArrConfig = { unique: true, collection: $scope.sgState[msg.prop] };
          if(colIdProp){ defArrConfig.prop = colIdProp; }
          stateArrHelper = new OrderAbleColl(defArrConfig);
          if(!ng.isFunction(stateArrHelper[msg.method])) {
            return false;
          }
          stateArrHelper[msg.method].apply(stateArrHelper, msg.args);
          $scope.sgState[msg.prop]  = stateArrHelper.get();
          return true;
        };

        this.objectChangeReq =  function(msg){
          var stateArrHelper;
          stateArrHelper = new List($scope.sgState[msg.prop]);
          if(typeof stateArrHelper[msg.method] !== 'function') {
            return false;
          }
          stateArrHelper[msg.method].apply(stateArrHelper, msg.args);
          $scope.sgState[msg.prop]  = stateArrHelper.get();
          return true;
        };

        this.propertyChangeReq =  function(msg){
          $scope.sgState[msg.prop] = msg.args[0];
          return $scope.sgState[msg.prop];
        };

        this.filtersChangeReq =  function(msg){
          ctrl.objectChangeReq(msg);
          ctrl.selectPageChangeReq(1);
          return true;
        };

        this.selectPageChangeReq = function(page){
          var gridState = $scope.sgState;
          if (page > 0 && page <= $scope.sgNumberOfPages) {
            gridState.pagination.start = (page - 1) * gridState.perPage;
            gridState.pagination.perPage = gridState.perPage;
          }
        };

        this.hasAggregators = function(){
          var gridState, i, l, aggType, index;
          $scope.hasAggregators = {any: false};
          gridState = $scope.sgState;
          i = 0;
          l = sgAggregators.availableAggregators.length;
          for(i;i<l;i++){
            aggType = sgAggregators.availableAggregators[i];
            index = Utils.indexOf(gridState.columns, true, aggType);
            if(index <= -1) {
              index = Utils.indexOf(gridState.columns, 'true', aggType);
            }
            if(index > -1) {
              gridState.columns[index][aggType] = true;
              $scope.hasAggregators[aggType] = true;
              $scope.hasAggregators.any = true;
            }
          }
          return $scope.hasAggregators;
        };

        /**
         * get Sorters
         * @param {Array} sorters - set sorters
         */
        this.getSort = function getSort (sorters) {
          return sorters.map(function(sort){ return (sort.reverse ? '-' : '') + sort.predicate; });
        };

        /**
         * add groupers to sorters
         * @param {array} groupers - groupers to add
         * @param {array} sorters - sorters to add to
         */
        this.addGroupersToSorts = function addGroupersToSorts (groupers, sorters) {
          var defArrConfig, sortersHelper, i, l, tempSorters = [], sorter, savedSorter;
          if(!groupers.length) {
            return [];
          }
          groupers = groupers || [];
          sorters = sorters || [];
          defArrConfig = { unique: true, prop: 'name', collection: sorters};
          sortersHelper = new OrderAbleColl(defArrConfig);

          i = 0;
          l = groupers.length;
          for(i; i<l;i++){
            sorter = {name: groupers[i].name, predicate: groupers[i].name, reverse: groupers[i].reverse};
            savedSorter = sortersHelper.get(sorter);
            tempSorters.push(savedSorter || sorter);
            if(savedSorter) {
              sortersHelper.remove(savedSorter);
            }
          }

          return [].concat(tempSorters, sorters);
        };

        /**
         * return the current state of the grid
         * @returns Object gridState
         */
        this.getGridState = function getGridState () {
          return this.gridState;
        };

        this.getFilteredCollection = function getFilteredCollection () {
          return filtered || safeCopy;
        };

        this.getSafeCopy = function getSafeCopy () {
          return safeCopy;
        };

        /**
         * Use a different filter function than the angular FilterFilter
         * @param filterName the name under which the custom filter is registered
         */
        this.setFilterFunction = function setFilterFunction (filterName) {
          filter = $filter(filterName);
        };

        /**
         * Use a different function than the angular orderBy
         * @param sortFunctionName the name under which the custom order function is registered
         */
        this.setSortFunction = function setSortFunction (sortFunctionName) {
          orderBy = $filter(sortFunctionName);
        };

        /**
         * Usually when the safe copy is updated the pipe function is called.
         * Calling this method will prevent it, which is something required when using a custom pipe function
         */
        this.preventPipeOnWatch = function preventPipe () {
          pipeAfterSafeCopy = false;
        };
      }]);
})(angular);


