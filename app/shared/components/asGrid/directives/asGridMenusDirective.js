'use strict';

/**
 * @ngdoc function
 * @name as-grid.directive:sgMenu
 * @description
 * # sgMenu
 */
(function (ng, undefined){
  ng.module('as-grid')
    .directive('sgMenu', ['$parse','$document','sgConfig','Utils', 'AvailableMenuOptions', function ($parse, $document,sgConfig, Utils, AvailableMenuOptions) {
      return {
        restrict: 'A',
        require: '^asGrid',
        scope : {
          sgMenu: '='
        },
        templateUrl: '/shared/components/asGrid/views/asGridMenu.html',
        link: function (scope, element) {
          var sgStatePropName, sgStateGetter, column, hoverFlag;
          sgStatePropName = '$parent.sgState';
          sgStateGetter = $parse(sgStatePropName);
          column = scope.sgMenu || undefined;
          hoverFlag = false;

          scope.sgState = sgStateGetter(scope);

          scope.isOpened = false;

          scope.menuOptions = scope.sgState.menuOptions || [];


          ng.element(element).bind('mouseenter', function(){
            hoverFlag = true;
          }).bind('mouseleave', function(){
            hoverFlag = false;
          });

          $document.bind('click', function(){
            if(!hoverFlag) {
              scope.isOpened = false;
              scope.$digest();
            }
          });

          scope.openMenu = function(){
            scope.isOpened = !scope.isOpened;
          };

          scope.menuSelect = function(key, option){
            AvailableMenuOptions.invokeFn('menuItemSelected', option, scope, scope.sgState, column, key);
          };

          scope.isChecked = function(key, option){
            return AvailableMenuOptions.invokeFn('isChecked', option, scope, scope.sgState, column, key);
          };

          scope.getIconClassName = function(key, option){
            return AvailableMenuOptions.invokeFn('getIconClassName', option, scope, scope.sgState, column, key);
          };

          scope.getSpecificTemplate = function(key, option){
            return AvailableMenuOptions.invokeFn('getSpecificTemplate', option, scope, scope.sgState, column, key);
          };

          scope.hasSpecificTemplate = function(key, option){
            return AvailableMenuOptions.invokeFn('hasSpecificTemplate', option, scope, scope.sgState, column, key);
          };
        }
      };
    }]);
})(angular);