'use strict';

/**
 * @ngdoc function
 * @name as-grid.directive:sgPipe
 * @description
 * # sgPipe
 */
(function (ng, undefined){
  ng.module('as-grid')
    .directive('sgPipe', ['$parse', function ($parse) {
      return {
        require: 'asGrid',
        link: {
          pre: function (scope, element, attrs, ctrl) {
            var sgPipeName, sgPipeGetter, sgPipe;
            sgPipeName = attrs.sgPipe;
            sgPipeGetter = $parse(sgPipeName);
            sgPipe = sgPipeGetter(scope);
            if (sgPipe && ng.isFunction(sgPipe)) {
              ctrl.preventPipeOnWatch();
              ctrl.pipe = function () {
                return sgPipe(ctrl.getGridState(), ctrl);
              };
            }
          }
        }
      };
    }]);
})(angular);