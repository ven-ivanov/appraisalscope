'use strict';

/**
 * @ngdoc function
 * @name as-grid.directive:sgMenuSpecificOption
 * @description
 * # sgMenuSpecificOption
 */
(function (ng, undefined){
  ng.module('as-grid')
    .directive('sgMenuSpecificOption', ['$parse', '$compile', 'AvailableMenuOptions',function ($parse, $compile, AvailableMenuOptions) {
      return {
        restrict: 'A',
        link: function (scope, element, attr) {
          var tpl, key;
          key = attr.sgMenuSpecificOption || false;
          if(!key){
            return false;
          }
          tpl = AvailableMenuOptions.invokeFn('getSpecificTemplate',scope.menuOptions[key] );
          element.html($compile(tpl)(scope.$parent.$parent));
        }
      };
    }]);
})(angular);