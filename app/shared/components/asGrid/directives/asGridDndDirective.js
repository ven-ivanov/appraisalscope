'use strict';

/**
 * @ngdoc function
 * @name as-grid.directive:sgDnd
 * @description
 * # sgDnd
 */
/* global dragula:false */
(function (ng, undefined){
  ng.module('as-grid')
    .directive('sgDnd', ['sgConfig', function (sgConfig) {
      return {
        restrict: 'A',
        require: '^asGrid',
        link : function(scope, element, attrs){
          if(!scope.sgState.dnd) {
            return false;
          }

          var containers, options, nodeList, drak, drop;

          options = {
            moves: function (el) {
              return !ng.element(el).hasClass('dnd-disabled');
            },
            accepts: function (el, target, source, sibling) {
              return !ng.element(sibling).hasClass('dnd-disabled');
            },
            direction: 'horizontal', // Y axis is considered when determining where an element would be dropped
            copy: false,           // elements are moved by default, not copied
            revertOnSpill: false,  // spilling will put the element back where it was dragged from, if this is true
            removeOnSpill: false   // spilling will `.remove` the element, if this is true
          };
          containers = attrs.containers;
          if( containers ) {
            nodeList = document.querySelectorAll(containers);
            if( nodeList.length ) {
              containers = Array.prototype.slice.apply(nodeList);
            }
          }else{
            containers = element[0];
          }

          drak = dragula(containers, options || {});

          drop = function(el, container){
            var children, newOrder, contr;

            contr = ng.element(container);
            children = contr.find('th:not(.dnd-disabled)');
            newOrder = [];
            children.each(function(i, item){
              newOrder.push(ng.element(item).data('original-index'));
              ng.element(item).data('original-index',i);
            });
            var msg = [
              {method : 'array', args: [{method: 'setOrder', prop : 'columns', args: [newOrder]}]}
            ];
            scope.$emit(sgConfig.requestGridStateChange, msg);
          };

          drak.on('drop', drop);
        }
      };
    }]);
})(angular);