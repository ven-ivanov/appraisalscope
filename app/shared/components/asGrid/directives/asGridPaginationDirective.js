'use strict';

/**
 * @ngdoc function
 * @name as-grid.directive:sgPagination
 * @description
 * # sgPagination
 */
(function (ng, undefined){
  ng.module('as-grid')
    .directive('sgPagination', ['sgConfig', function (sgConfig) {
      return {
        restrict: 'EA',
        require: '^asGrid',
        templateUrl: function (element, attrs) {
          if (attrs.sgTemplate) {
            return attrs.sgTemplate;
          }
          return sgConfig.pagination.template;
        },
        link: function (scope, element, attrs, ctrl) {

          var oldPerPage, oldPagination, oldDisplayedPages, oldNumberOfPages;
          oldPerPage = undefined;
          oldPagination = undefined;
          oldDisplayedPages = undefined;
          oldNumberOfPages = scope.sgNumberOfPages;

          scope.currentPage = 1;
          scope.pages = [];


          //view -> table state
          scope.selectPage = function (page) {
            if (page > 0 && page <= scope.sgNumberOfPages) {
              var msg = [{method : 'selectPage', args: [page]}];
              scope.$emit(sgConfig.requestGridStateChange, msg);
            }
          };

          function gridStateChange() {
            var gridState = ctrl.getGridState();
            if(typeof oldPerPage !== 'undefined' && oldPerPage !== gridState.perPage){
              ctrl.selectPage(1);
            }

            if(typeof oldDisplayedPages !== 'undefined' &&
              oldDisplayedPages !== gridState.displayedPages) {
              redraw();
            }

            if(typeof oldPagination !== 'undefined' &&
              (oldPagination.start !== gridState.pagination.start ||
                oldPagination.perPage !== gridState.pagination.perPage)) {
              redraw();
            }

            if(typeof oldNumberOfPages !== 'undefined' && oldNumberOfPages !== scope.sgNumberOfPages){
              redraw();
            }


            oldPerPage = gridState.perPage;
            oldPagination = ng.copy(gridState.pagination);
            oldDisplayedPages = gridState.displayedPages;
            oldNumberOfPages = scope.sgNumberOfPages;
          }

          function redraw() {
            var gridState, paginationState, numberOfPages, start, end, i, prevPage;
            gridState = scope.sgState;
            paginationState = gridState.pagination;
            numberOfPages = scope.sgNumberOfPages;
            start = 1;
            prevPage = scope.currentPage;
            scope.currentPage = Math.floor(paginationState.start / paginationState.perPage) + 1;

            start = Math.max(start, scope.currentPage - Math.abs(Math.floor(gridState.displayedPages / 2)));
            end = start + gridState.displayedPages;

            if (end > numberOfPages) {
              end = numberOfPages + 1;
              start = Math.max(1, end - gridState.displayedPages);
            }

            scope.pages = [];

            for (i = start; i < end; i++) {
              scope.pages.push(i);
            }

            if (prevPage !== scope.currentPage && scope.sgPageChange && ng.isFunction(scope.sgPageChange)) {
              scope.sgPageChange({newPage: scope.currentPage});
            }
          }

          scope.$watch(function () {
            return ctrl.getGridState().pagination;
          }, redraw, true);

          /*
          scope.$watch(function () {
            console.log('watching', ctrl.getGridState().pagination);
            return ctrl.getGridState().pagination;
          }, redraw, true);
          */

          scope.$on(sgConfig.doneChangeAfterPipe, gridStateChange);
          gridStateChange(ctrl.getGridState());
          redraw();
        }
      };
    }]);
})(angular);