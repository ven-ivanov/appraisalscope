'use strict';

/**
 * @ngdoc function
 * @name as-grid.directive:sgCellTemplate
 * @description
 * # sgCellTemplate
 */
(function (ng, undefined){
  ng.module('as-grid')
    .directive('sgCellTemplate', ['$compile', function ($compile) {
      return {
        restrict: 'EA',
        require: '^asGrid',
        link: function (scope, element, attrs) {
          element.html($compile(attrs.sgCellTemplate)(scope));
        }
      };
    }]);
})(angular);