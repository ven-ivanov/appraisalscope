'use strict';

/**
 * @ngdoc function
 * @name as-grid.directive:sgSearch
 * @description
 * # sgSearch
 */
(function (ng, undefined){
  ng.module('as-grid')
    .directive('sgSearch', ['sgConfig', '$timeout', '$parse', function (sgConfig, $timeout) {
      return {
        restrict: 'A',
        require: '^asGrid',
        link: function (scope, element, attr, ctrl) {
          var promise = null ,
            throttle = attr.sgDelay || sgConfig.search.delay,
            msg,
            oldGridState = ng.copy(ctrl.getGridState()),
            oldPredicate = attr.sgSearch;

          scope.$on('$destroy',function(){
            ng.element(element).hide();
          });

          attr.$observe('sgSearch', function (newValue) {
            if (newValue !== oldPredicate) {
              var method = 'remove', args = [newValue || '_$'];
              if(element[0].value){
                method = 'add';
                args = [element[0].value, newValue || '_$'];
              }

              msg = [
                {method : 'filters', args: [{method: method, prop : 'filters', args: args}]},
                {method : 'filters', args: [{method: 'remove', prop : 'filters', args: [oldPredicate || '_$']}]}
              ];

              scope.$emit(sgConfig.requestGridStateChange, msg);
              oldPredicate = attr.sgSearch;
            }
          });

          element.bind('input', function (evt) {
            evt = evt.originalEvent || evt;
            if (promise !== null) {
              $timeout.cancel(promise);
            }
            promise = $timeout(function () {
              var predicate = attr.sgSearch || '_$', method = 'remove', args = [predicate];

              if(evt.target.value){
                method = 'update';
                args = [evt.target.value, predicate];
              }
              msg = [
                {method : 'filters', args:[{ method:method, prop : 'filters', args: args}]}
              ];
              scope.$emit(sgConfig.requestGridStateChange, msg);
              promise = null;
            }, throttle);
          });

          function gridStateChange(event, gridState){
            var predicate = attr.sgSearch || '_$';
            fixGlobalSearch(gridState);
            if (oldGridState.filters[predicate] !== gridState.filters[predicate]){
              element[0].value = gridState.filters[predicate] || '';
              oldGridState =  ng.copy(ctrl.getGridState());
            }
          }
          scope.$on(sgConfig.doneChangeBeforePipe, gridStateChange);

          function fixGlobalSearch (gridState){
            var globalSearch = gridState.filters._$;
            if(globalSearch){
              delete gridState.filters._$;
              gridState.filters.$ = globalSearch;
            }
          }
        }
      };
    }]);
})(angular);