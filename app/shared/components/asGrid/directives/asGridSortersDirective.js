'use strict';

/**
 * @ngdoc function
 * @name as-grid.directive:sgSort
 * @description
 * # sgSort
 */
(function (ng, undefined){
  ng.module('as-grid')
    .directive('sgSort', ['$parse','sgConfig','Utils', function ($parse, sgConfig, Utils) {
      return {
        restrict: 'A',
        require: '^asGrid',
        link: function (scope, element, attr, ctrl) {
          var predicate = attr.sgSort,
            getter = $parse(predicate),
            columns = scope.sgState.columns,
            nameAs,
            index = 0,
            classAscent = attr.sgClassAscent || sgConfig.sort.ascentClass,
            classDescent = attr.sgClassDescent || sgConfig.sort.descentClass,
            sorters = [],
            msg = {},
            currentSorter;

          //view --> table state
          function sort () {
            index++;
            predicate = ng.isFunction(getter(scope)) ? getter(scope) : attr.sgSort;
            if (index % 3 === 0 && attr.sgSkipNatural === undefined) {
              //manual reset
              index = 0;
              sorters = [];
            } else {
              var colIndex = Utils.indexOf(columns, predicate, 'name');
              nameAs = columns[colIndex].nameAs || columns[colIndex].name;
              sorters =  [
                {name : predicate, nameAs : nameAs,  predicate : predicate, reverse: index % 2 === 0}
              ];
            }

            return sorters;
          }

          element.bind('click', function sortClick () {
            if (predicate) {
              msg = {method : 'array', args: [{method: 'set', prop : 'sorters', args:[sort()]}]};
              scope.$emit(sgConfig.requestGridStateChange, msg);
              scope.$apply();
            }
          });

          //table state --> view
          scope.$watch(function () {
            return ctrl.getGridState().sorters;
          }, function (sorters) {
            var colIndex = Utils.indexOf(sorters, predicate, 'name');
            if(colIndex === -1){
              return element
                .removeClass(classAscent)
                .removeClass(classDescent);
            }

            currentSorter = sorters[colIndex];
            if (currentSorter.reverse) {
              element
                .removeClass(classAscent)
                .addClass(classDescent);
            } else {
              element
                .removeClass(classDescent)
                .addClass(classAscent);
            }
            return true;
          });
        }
      };
    }]);
})(angular);