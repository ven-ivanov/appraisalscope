'use strict';

var app = angular.module('frontendApp');

/**
 * Simply place a styled submit button, transcluding in whatever text necessary
 *
 * Usage: <as-button icon="apply" class="green-theme">Save</as-button>
 * This will create a button with the text "save"
 */
app.directive('asButton', [function () {
  return {
    templateUrl: '/shared/components/button/views/as-button.html',
    replace: true,
    transclude: true,
    scope: {
      icon: '@',
      type: '@',
      click: '&',
      disabled: '&',
      right: '@'
    },
    controller: function ($scope) {
      $scope.hasIcon = angular.isDefined($scope.icon);
      if ($scope.hasIcon) {
        $scope.src = '../../../assets/images/icons/as-button/btn-' + $scope.icon + '.svg';
      }
    },
    link: function (scope, element, attrs, controller, transcludeFn) {

      transcludeFn(function (clone) {
        scope.hasContent = clone.text().trim().length > 0;
      });

      // Set disabled to false if a function isn't passed in
      if (!attrs.disabled) {
        scope.disabled = function () {
          return false;
        };
      }
      // Convert to boolean for align right
      scope.right = scope.$eval(scope.right);
    }
  };
}]);
