'use strict';

var app = angular.module('frontendApp');

/**
 * Conditionally load section
 *
 * This should be used in place of ng-if, since ng-if will compile the directive and then remove it from the DOM
 */
app.directive('loadSection', ['$compile', function ($compile) {
  return {
    compile: function () {
      return {
        pre: function (scope, elem) {
          // The directive to load
          var directive = '', match, params;
          if (angular.isDefined(scope.directive)) {
            match = scope.directive.match(/([^\s]+)\s?(.+)?/);
            directive = match[1];
            params = match[2] ? match[2] : '';
            directive = $compile('<' + directive + ' ' + params +  '></' + scope.directive + '>')(scope);
          }
          elem.append(directive);
        }
      };
    }
  };
}]);