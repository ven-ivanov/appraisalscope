'use strict';

var app = angular.module('frontendApp');

/**
 * ng-include that replaces the element on which it is defined with the included template
 */
app.directive('includeReplace', [function () {
  return {
    require: 'ngInclude',
    restrict: 'A',
    link: function (scope, element) {
      element.replaceWith(element.children());
    }
  };
}]);