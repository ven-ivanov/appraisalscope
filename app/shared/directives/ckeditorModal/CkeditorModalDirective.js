'use strict';

var app = angular.module('frontendApp');

/**
 * Enforce focus on the correct modal when using ckeditor in a modal, or else users won't be able to insert images
 */
app.directive('ckeditorModal', [function () {
  return function () {
    angular.element.fn.modal.Constructor.prototype.enforceFocus = function () {
      var $modalElement = this.$element;
      var handleFocus = function () {
        angular.element(document).on('focusin.modal', function (e) {
          var $parent = angular.element(e.target.parentNode);
          if ($modalElement[0] !== e.target && !$modalElement.has(e.target).length &&
              !$parent.hasClass('cke_dialog_ui_input_select') && !$parent.hasClass('cke_dialog_ui_input_text')) {
            // Remove event handler to prevent infinite loop
            angular.element(document).off('focusin.modal');
            $modalElement.focus();
            // Reattach event handler
            handleFocus();
          }
        });
      };
      handleFocus();
    };
  };
}]);