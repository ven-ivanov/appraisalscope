/**
 * Created by dvabuzyarov on 6/1/15.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Calculates and sets height of the base view port
 */
app.directive('baseViewPortHeight', ['$document', function ($document) {
  return {
    restrict: 'A',
    link: function ($scope, $element) {

      var navigationHeaderHeight = 70;
      var copyrightPlaceholderHeight = 65;

      var document = $document[0] || {};
      // @TODO Fix this jshint error
      var setHeight = function () { // jshint ignore:line
        var bodyHeight = document.body && document.body.clientHeight;
        $element.css({'height': bodyHeight - (navigationHeaderHeight + copyrightPlaceholderHeight)});
      };
      //$document.on('resize', setHeight);
      //setHeight();
    }
  };
}]);
