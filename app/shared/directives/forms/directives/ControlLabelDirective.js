/**
 * Created by dvabuzyarov on 6/13/15.
 */
'use strict';


var app = angular.module('frontendApp');

/**
 * control label directive. Simplify the html markup for control-labels controls
 */
app.directive('controlLabel', [function () {
  return {
    restrict: 'A',
    require: '^?formGroup',
    compile: function ($template) {
      $template.addClass('control-label');

      return function (scope, element, attr, formGroupCtrl) {
        if (!formGroupCtrl) {
          return;
        }

        //notifies the parent controller about it self
        formGroupCtrl.label = element;
        scope.$on('$destroy', function () {
          formGroupCtrl.label = null;
        });
      };
    }
  };
}]);
