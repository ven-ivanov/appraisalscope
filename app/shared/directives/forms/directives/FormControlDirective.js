/**
 * Created by dvabuzyarov on 6/13/15.
 */
'use strict';


var app = angular.module('frontendApp');

/**
 * form control directive. Simplify the html markup for form-control controls (inputs)
 */
app.directive('formControl', ['Utils', function (Utils) {
  return {
    restrict: 'A',
    require: ['^?formGroup', '?ngModel'],
    compile: function ($template) {

      $template.addClass('form-control');

      if (!$template.attr('id')) {
        $template.attr('id', 'input' + Utils.capitalize($template.attr('name')));
      }

      if ($template[0].tagName.toLowerCase() === 'input' && !$template.attr('type')) {
        throw Error('input type is required');
      }

      return function ($scope, $element, $attrs, ctrls) {
        var formGroupCtrl = ctrls[0];
        var ngModelCtrl = ctrls[1];

        //notifies the parent controller about it self
        formGroupCtrl.input = $element;

        /**
         * checks if the input has value and notifies the form-group controller
         * @param arg
         * @returns {*}
         */
        function ngModelPipelineCheckIfValuePresent(arg) {
          formGroupCtrl.setHasValue(!ngModelCtrl.$isEmpty(arg));
          return arg;
        }

        /**
         * checks if the input has value and notifies the form-group controller
         */
        function inputCheckIfValuePresent() {
          var isBadValue = ($element[0].validity || {}).badInput;
          formGroupCtrl.setHasValue($element.val().length > 0 || isBadValue);
        }

        ngModelCtrl.$parsers.push(ngModelPipelineCheckIfValuePresent);
        ngModelCtrl.$formatters.push(ngModelPipelineCheckIfValuePresent);

        $scope.$watch(function(){
          return ngModelCtrl.$dirty;
        }, function(){
          formGroupCtrl.setDirty(ngModelCtrl.$dirty);
        });

        $element.on('input', inputCheckIfValuePresent);
        $element.on('blur', inputCheckIfValuePresent);


        $scope.$on('$destroy', function () {
          formGroupCtrl.setHasValue(false);
          formGroupCtrl.input = null;
        });
      };
    }
  };
}]);
