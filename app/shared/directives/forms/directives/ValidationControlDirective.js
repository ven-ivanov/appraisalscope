/**
 * Created by dvabuzyarov on 6/15/15.
 */
'use strict';


var app = angular.module('frontendApp');

/**
 * validation control directive. Simplify html markup and displays input validation errors
 */
app.directive('validationControl', ['VALIDATOR_MESSAGES', function (VALIDATOR_MESSAGES) {
  return {
    restrict: 'A',
    require: '^?formGroup',
    templateUrl: '/shared/directives/forms/views/validation-control.html',
    scope: {},
    controller: ['$scope', function ($scope) {
      var vm = this;

      /**
       * gets ngModel controller and watching after $errors
       * @param modelCtrl
       */
      vm.setInputModelCtrl = function (modelCtrl) {
        $scope.$watchCollection(function () {
          return Object.getOwnPropertyNames(modelCtrl.$error);
        }, function () {
          var $error = modelCtrl.$error;
          Object.getOwnPropertyNames($error).forEach(function (key) {
            if (key === 'parse') {
              return;
            }
            var name = key.indexOf('validator') === 0 ? key.substring(9, key.length) : key;
            var lowercaseName = name.toLowerCase();
            $scope.message = VALIDATOR_MESSAGES[lowercaseName];
            return;
          });
        });
      };

    }],
    compile: function ($template) {

      $template.addClass('help-block');

      return function ($scope, $element, $attrs, formGroupCtrl) {

        //notifies the parent controller about it self
        formGroupCtrl.validationControl = $element;

        $scope.$on('$destroy', function () {
          formGroupCtrl.validationControl = null;
        });
      };
    }
  };
}]);
