/**
 * Created by dvabuzyarov on 6/12/15.
 */
'use strict';


var app = angular.module('frontendApp');

/**
 * form group directive. Simplify the html markup for form-group controls
 */
app.directive('formGroup', [function () {
  return {
    restrict: 'A',
    compile: function($template){
      $template.addClass('form-group');
    },
    controller: ['$scope', '$element', function ($scope, $element) {
      var vm = this;

      vm.element = $element;

      /**
       * toggles css class input-has-value
       * @param hasValue
       */
      vm.setHasValue = function (hasValue) {
        $element.toggleClass('input-has-value', !!hasValue);
      };

      /**
       * toggle css class ng-dirty, ng-pristine
       * @param isDirty
       */
      vm.setDirty = function(isDirty){
        $element.toggleClass('ng-dirty', isDirty);
        $element.toggleClass('ng-pristine', !isDirty);
      };

      /**
       * sets up the label control
       * @param hasLabelAndInput
       */
      function setupLabel(hasLabelAndInput)
      {
        if (hasLabelAndInput && !vm.label.attr('for')) {
          vm.label.attr('for', vm.input.attr('id'));
        }
      }

      /**
       * sets up the validation control with ngModel
       * @param hasValidationControlAndInput
       */
      function setupValidationControl(hasValidationControlAndInput){
        if (hasValidationControlAndInput){
          var modelCtrl = vm.input.controller('ngModel');
          var validationCtrl = vm.validationControl.controller('validationControl');
          validationCtrl.setInputModelCtrl(modelCtrl);
        }
      }

      $scope.$on('show-errors-check-validity', function() {
        $element.addClass('force-validation');
      });
      $scope.$on('show-errors-reset', function() {
        $element.removeClass('force-validation');
      });

      $scope.$watch(function() {
        return vm.label && vm.input;
      }, setupLabel);

      $scope.$watch(function() {
        return vm.validationControl && vm.input;
      }, setupValidationControl);
    }]
  };
}]);
