'use strict';

module.exports = function () {
  return {
    /**
     * Test the current URL
     * @param testUrl
     */
    testUrl: function (testUrl) {
      // Generic wait function
      browser.wait(function () {
        var deferred = protractor.promise.defer();
        browser.getCurrentUrl().then(function (url) {
          expect(url).toEqual('http://clientx.appraisalscope.local:9001/#/' + testUrl);
          deferred.fulfill(true);
        });
        return deferred.promise;
      });
    }
  };
};