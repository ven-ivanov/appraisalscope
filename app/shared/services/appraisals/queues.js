'use strict';

angular.module('frontendApp')
  .factory('AppraisalQueuesApiService', [
    'API_PREFIX', '$resource',

    function (API_PREFIX, $resource) {
      var vm = this;

      function getResource(sufix) {
        return $resource(API_PREFIX() + '/v2.0/appraisals/queues/'+ sufix, null, {
          get: {method: 'GET', isArray: false}
        });
      }

      vm.getSummary = function(callback) {
        return getResource('summary').get(callback).$promise;
      };

      vm.getOpen = function(data) {
        return getResource('open').get(data).$promise;
      };

      vm.getNew = function(data) {
        return getResource('new').get(data).$promise;
      };

      vm.getAssigned = function(data) {
        return getResource('assigned').get(data).$promise;
      };

      vm.getUnaccepted = function(data) {
        return getResource('unaccepted').get(data).$promise;
      };

      vm.getAccepted = function(data) {
        return getResource('accepted').get(data).$promise;
      };

      vm.getScheduled = function(data) {
        return getResource('scheduled').get(data).$promise;
      };

      vm.getDue = function(data) {
        return getResource('due').get(data).$promise;
      };

      vm.getLate = function(data) {
        return getResource('late').get(data).$promise;
      };

      vm.getOnHold = function(data) {
        return getResource('on-hold').get(data).$promise;
      };

      vm.getMessages = function(data) {
        return getResource('messages').get(data).$promise;
      };

      vm.getUpdates = function(data) {
        return getResource('updates').get(data).$promise;
      };

      vm.getUCDP = function(data) {
        return getResource('ucdp').get(data).$promise;
      };

      vm.getBids = function(data) {
        return getResource('bids').get(data).$promise;
      };

      vm.getUwConditions = function(data) {
        return getResource('uw-conditions').get(data).$promise;
      };

      vm.getRevisionSent = function(data) {
        return getResource('revision-sent').get(data).$promise;
      };

      vm.getRevisionReceived = function(data) {
        return getResource('revision-received').get(data).$promise;
      };

      vm.getReadyForReview = function(data) {
        return getResource('ready-for-review').get(data).$promise;
      };

      vm.getReviewed = function(data) {
        return getResource('reviewed').get(data).$promise;
      };

      return vm;
    }
  ]
);
