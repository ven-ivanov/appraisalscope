'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.service:SharedAppraisalApiService
 * @description
 */
app.factory('SharedAppraisalApiService', [
  'API_PREFIX', '$resource',

  function (API_PREFIX, $resource) {

    var resource = $resource(API_PREFIX() + '/v2.0/appraisals/:id', null, {
      get: {method: 'GET', isArray: false}
    });

    return {
      /**
       * Get a single appraisal object by ID
       * @param appraisalId
       * @returns {*|Function|promise|n}
       */
      get: function(appraisalId) {
        return resource.get({id: appraisalId}).$promise;
      },

      /**
       * Get search results for all appraisals
       *
       * @param data
       * @returns {*|Function|promise|n}
       */
      getAll: function(data) {
        return resource.get(data).$promise;
      }
    };
  }
]);
