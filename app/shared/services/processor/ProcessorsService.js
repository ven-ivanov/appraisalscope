'use strict';

/**
 * @ngdoc service
 * @name frontendApp.NotifyProcessors
 * @description
 * # NotifyProcessors
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('ProcessorsService', [
    '$resource', 'API_PREFIX',

    function ($resource, API_PREFIX) {
      return $resource(API_PREFIX() + '/v2.0/admin/notify-processors/:userId', null, {
      	'managers':{method:'GET', url:API_PREFIX() + '/v2.0/admin/managers'},
      	'companies':{method:'GET', url:API_PREFIX() + '/v2.0/admin/companies'}
      });
    }
  ]);