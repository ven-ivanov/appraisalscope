'use strict';

var app = angular.module('frontendApp');

app.factory('TriggerDownloadService', [function () {
  return {
    init: function (scope) {
      // Set scope either as parameter or this val (not sure why I'd make it a param)
      if (angular.isUndefined(scope)) {
        scope = this;
      }
      /**
       * Trigger download of exported data
       */
      scope.triggerDownload = function (link, formRetrieve) {
        // Create form and trigger submit
        if (formRetrieve) {
          var form = angular.element('<form method="get" action="' + link + '"></form>');
          angular.element('html').append(form);
          form.trigger('submit');
          angular.element(form).remove();
        } else {
          window.open(link,'_blank');
        }
      };
    }
  };
}]);