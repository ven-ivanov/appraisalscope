'use strict';

/**
 * @ngdoc service
 * @name frontendApp.LoanOfficersService
 * @description Getting all loan officers from server
 * # LoanOfficersService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
.factory('LoanOfficersService', [
  '$resource', 'API_PREFIX',

  function ($resource, API_PREFIX) {
    return $resource(API_PREFIX() + '/v2.0/appraiser/appraisals/loan-officers/');
  }
]);