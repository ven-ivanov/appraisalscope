'use strict';

var app = angular.module('frontendApp');

app.factory('DomLoadService', ['$http', '$interval', '$q', function ($http, $interval, $q) {
  return {
    /**
     * Wait for all pending $http requests to be completed before resolving promise
     */
    load: function () {
      return $q(function (resolve) {
        var complete = $interval(function () {
          // If no requests remain, resolve promise
          if (!$http.pendingRequests.length) {
            resolve();
            $interval.cancel(complete);
          }
        }, 25);
      });
    }
  };
}]);