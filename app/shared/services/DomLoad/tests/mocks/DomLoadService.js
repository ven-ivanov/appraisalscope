/**
 * Mock DomLoadService
 */
var DomLoadServiceMock = function ($q) {
  return {
    load: function () {
      return $q(function (resolve) {
        resolve();
      });
    }
  }
};