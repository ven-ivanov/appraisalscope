  'use strict';

/**
 * @ngdoc service
 * @name frontendApp.HandbookService
 * @description
 * # HandbookService
 * Creates the "HandbookService" service
 */
angular.module('frontendApp')
  .factory('HandbookService', [
    '$resource', 'API_PREFIX', 'HANDBOOK_CATEGORIES',

    function ($resource, API_PREFIX, HANDBOOK_CATEGORIES) {
      var r = $resource(API_PREFIX() + '/v2.0/handbook/:category', {}, {
        'query': {
          method: 'GET',
          isArray: false
        }
      });

      function extend(obj, category) {
        if (category !== HANDBOOK_CATEGORIES.accountTypes) {
          return obj; // Currently, there's no support for  the other categories
        }

        // finds the first object by id
        obj.find = function (id) {
          for (var i in obj.data) {
            var item = obj.data[i];
            if (item.id === id) {
              return item;
            }
          }
          return null;
        };

        return obj;
      }

      return {
        query: function (params) {
          if (angular.isUndefined(params.category)) {
            throw 'Category is required in order to query the Handbook';
          }
          var q = r.query(params);
          q.$promise.then(function (res) {
            // extends the received object with additional functions
            return extend(res, params.category);
          });
          return q;
        }
      };
    }
  ]);
