'use strict';

/**
 * @ngdoc service
 * @name frontendApp.ValidatorsProviderService
 * @description
 * # Service provides the validator functions
 * Service in the frontendApp.
 */
angular.module('frontendApp')
  .factory('ValidatorsProviderService', ['SharedValidatorsProvider', function (SharedValidatorsProvider) {
    return SharedValidatorsProvider;
  }]);
