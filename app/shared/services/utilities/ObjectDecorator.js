'use strict';

/**
 * @ngdoc factory
 * @name frontendApp.AdminReportsTabsService
 * @description
 * # AdminReportsTabsService
 */
angular.module('frontendApp')
  .service('ObjectDecorator', [
    '$injector',
    function ($injector) {

      return {

        /**
         * @description
         * # takes an object and a decorator name and decorate the object
         *
         * @param {Object} decorated
         * @param {String} decoratorName
         *
         * @usage: ObjectDecorator.decorate(scope, 'smartTableDecorator') will add smart table decorator properties and methods to scope.
         */
        decorate : function(decorated, decoratorName){
          if(!decorated || !decoratorName) {
            throw new Error('Missing arguments cannot decorate');
          }
          var decorator, args;
          decorator = $injector.get(decoratorName);
          if (!decorator || !decorator.decorate || 'function' !== typeof decorator.decorate) {
            throw new Error('Service is not a decorator');
          }
          args = Array.prototype.slice.call(arguments);
          args.splice(1,1);
          decorator.decorate.apply(decorated, args);
        }
      };
    }
  ]);

