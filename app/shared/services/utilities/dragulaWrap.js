'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.dragulaWrap
 * @description
 * # dragulaWrap
 */

/* global dragula:false */
angular.module('frontendApp')
  .directive('dragulaWrap',[function(){
    return {
      restrict: 'EA',
      link : function(scope, element, attrs){
        var containers, options, nodeList, drak,
          drag, dragend, drop, cancel,
          remove, shadow, cloned;

        options = attrs.options;
        containers = attrs.containers;
        if( containers ) {
          nodeList = document.querySelectorAll(containers);
          if( nodeList.length ) {
            containers = Array.prototype.slice.apply(nodeList);
          }
        }else{
          containers = element[0];
        }


        drak = dragula(containers, options || {});

        drag = scope.$eval(attrs.onDrag);
        if(angular.isFunction(drag)) {
          drak.on('drag', drag);
        }

        dragend = scope.$eval(attrs.onDragend);
        if(angular.isFunction(dragend)) {
          drak.on('drop', dragend);
        }

        drop = scope.$eval(attrs.onDrop);
        if(angular.isFunction(drop)) {
          drak.on('drop', drop);
        }

        cancel = scope.$eval(attrs.onCancel);
        if(angular.isFunction(cancel)) {
          drak.on('drop', cancel);
        }

        remove = scope.$eval(attrs.onRemove);
        if(angular.isFunction(remove)) {
          drak.on('drop', remove);
        }

        shadow = scope.$eval(attrs.onShadow);
        if(angular.isFunction(shadow)) {
          drak.on('drop', shadow);
        }

        cloned = scope.$eval(attrs.onCloned);
        if(angular.isFunction(cloned)) {
          drak.on('drop', cloned);
        }

      }
    };
}]);