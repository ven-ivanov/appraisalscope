'use strict';

/**
 * @ngdoc factory
 * @name frontendApp.OrderAbleColl
 * @description
 * # OrderAbleColl
 * Model in the frontendApp.
 */
angular.module('frontendApp')
  .factory('OrderAbleColl', [
    'Collection',
    function (Collection) {

      function OrderAbleColl(){
        Collection.apply(this,arguments);
      }
      OrderAbleColl.prototype = Object.create(Collection.prototype);
      OrderAbleColl.prototype.constructor = OrderAbleColl;

      OrderAbleColl.prototype.getOrder = function(prop){
        var self = this;
        return this.collection.map(function(item){
          return (typeof item === 'object') ? item[prop || self._prop] || '': item;
        });
      };

      OrderAbleColl.prototype.setOrder = function(order){
        if(!order || order.length !== this.collection.length) {
          return false;
        }
        var newCollection = [], i, l, collection = this.collection;
        i=0;
        l = order.length;
        for(i;i<l;i++){
          newCollection.push(collection[order[i]]);
        }

        return this.set(newCollection);
      };

      return OrderAbleColl;
    }
  ]);
