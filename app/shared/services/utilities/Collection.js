'use strict';

/**
 * @ngdoc factory
 * @name frontendApp.Collection
 * @description
 * # Collection
 * Model in the frontendApp.
 */
angular.module('frontendApp')
  .factory('Collection', [
    'Utils',
    function (Utils) {

      function Collection(opts){
        opts = opts || {};
        this._unique = opts.unique || false;
        this._prop = opts.prop || undefined;
        this.collection = opts.collection|| [];
      }

      Collection.prototype.add = function(item, prop){
        if(this._unique){
          var index = this.indexOf(item, prop || this._prop);
          if(index > -1) {
            return false;
          }
        }
        return this.collection.push(item);
      };

      Collection.prototype.update = function(item, prop){
        var index = this.indexOf(item, prop || this._prop);
        if(index <= -1) {
          return false;
        }
        this.collection[index] = item;
        return this.collection[index];
      };

      Collection.prototype.remove = function(item, prop){
        var index = this.indexOf(item, prop || this._prop);
        if(index <= -1) {
          return false;
        }
        return this.collection.splice(index,1);
      };

      Collection.prototype.clear =  function(){
        this.collection.splice(0,this.collection.length);
      };

      Collection.prototype.set = function(newCollection){
        if(!Array.isArray(newCollection)) {
          return false;
        }
        this.clear();
        var i,l;
        i = 0;
        l = newCollection.length;
        for(i;i<l;i++){
          this.add(newCollection[i]);
        }

        return this.collection;
      };

      Collection.prototype.get = function(item, prop){
        if(!item) {
          return this.collection;
        }
        var index = this.indexOf(item, prop || this._prop);
        if(index <= -1) {
          return false;
        }
        return this.collection[index];
      };


      Collection.prototype.indexOf = function(item, prop){
        if(!item) {
          return false;
        }
        if(prop) {
          return Utils.indexOf(this.collection, item, prop);
        }
        return this.collection.indexOf(item);
      };

      return Collection;
    }
  ]);
