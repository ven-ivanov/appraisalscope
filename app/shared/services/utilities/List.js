'use strict';
/**
 * Created by Yaniv-Kalfa on.
 */
angular.module('frontendApp')
  .factory('List', [
    function () {

      function ListFactory(list){
        this.list = list || {};
        this.length = 0;
      }

      ListFactory.prototype.add =  function(l, id){
        var self = this, lId;
        lId = id || l.id || false;
        if(!lId) {
          return false;
        }
        if(self.list.hasOwnProperty(lId)) {
          return false;
        }
        this.length++;
        self.list[lId] = l;
        return self.list[lId];
      };

      ListFactory.prototype.update =  function(l, id){
        var self = this, lId;
        lId = id || l.id || false;
        if(!self.list.hasOwnProperty(lId)) {
          return this.add(l, id);
        }
        self.list[lId] = l;
        return self.list[lId];
      };

      ListFactory.prototype.remove =  function(id){
        var self = this;
        if(!self.list[id]) {
          return false;
        }
        this.length--;
        return delete self.list[id];
      };

      ListFactory.prototype.clear =  function(){
        var self = this;
        this.length = 0;
        self.list = {};
        return self.list;
      };

      ListFactory.prototype.get =  function(id){
        var self = this;
        if(!id) {
          return self.list;
        }
        if(!self.list[id]) {
          return false;
        }
        return self.list[id];
      };

      ListFactory.prototype.createUniqueId = function(){
        var genRandomId = function(){
            var start = Math.floor(Math.random()*30000).toString(), dateNow = Date.now().toString();
            return start+dateNow;
          }, self = this, id = genRandomId();

        while(!_.isEmpty(self.list[id])){
          id = genRandomId();
        }
        return id;
      };

      ListFactory.prototype.listLength = function(){
        var self = this, id, length = 0;

        if(this.length === Object.getOwnPropertyNames(this.list).length) {
          return this.length;
        }

        for(id in self.list){
          if(!self.list.hasOwnProperty(id)) {
            continue;
          }
          length++;
        }
        this.length = length;
        return this.length;
      };

      return ListFactory;
    }]
  );