'use strict';

var app = angular.module('frontendApp');

app.factory('DirectiveInheritanceService',
['TriggerDownloadService', '$compile', function (TriggerDownloadService, $compile) {
  return {
    inheritDirective: function (scope) {
      // For calling
      if (angular.isUndefined(scope)) {
        scope = this;
      }
      /**
       * Clear modal models on close
       * @param modalSelector Modal ID
       * @param modal The displayData property holding each of the modal properties
       * @param properties Individual properties within displayData[modal]
       */
      scope.clearModalOnClose = function (modalSelector, modal, properties) {
        // Keep reference to the modals which already have event handlers
        if (angular.isUndefined(scope.modalsCleared)) {
          scope.modalsCleared = [];
        }
        // Don't attach the event handler more than once
        if (scope.modalsCleared.indexOf(modalSelector) !== -1) {
          return;
        }
        // Memoize
        scope.modalsCleared.push(modalSelector);
        // When the modal closes, clear all models
        angular.element('#' + modalSelector).on('hidden.bs.modal', function () {
          angular.forEach(properties, function (property) {
            scope.tableCtrl.displayData[modal][property] = '';
          });
        });
      };
      // Make the trigger download directive method available
      TriggerDownloadService.init(scope);

      /**
       * Watch window width and display sidebar conditionally
       */
        // Sidebar open/close
      scope.sidebarOpen = false;
      /**
       * Open sidebar
       */
      scope.toggleSidebar = function () {
        scope.sidebarOpen = !scope.sidebarOpen;
      };

      var minWidth = 768,
      medWidth = 992,
      maxWidth = 1600;
      /**
       * Determine whether to show sidebar
       * Show it below 768. Don't show it 768 to 992. Show sidebar 992 to 1600. Don't show it above 1600.
       */
      var enableSidebar = function (windowWidth) {
        return (windowWidth < minWidth) || (windowWidth >= medWidth && windowWidth < maxWidth);
      };
      // Get initial window width
      scope.enableSidebar = enableSidebar(window.innerWidth);
      // Watch for resize
      angular.element(window).on('resize.doResize', function (){
        // Show sidebar
        if (scope.enableSidebar !== enableSidebar(window.innerWidth)) {
          scope.$apply(function () {
            scope.enableSidebar = !scope.enableSidebar;
          });
        }
      });
      // Remove event handler
      scope.$on('$destroy',function (){
        angular.element(window).off('resize.doResize');
      });
      // Remove modal-body when closing modals programmatically
      scope.clearModalBody = function () {
        angular.element('body').removeClass('modal-open');
      };

      /**
       * Recompile table on grouping change
       */
      scope.recompileTable = function () {
        angular.element('#table-wrapper').replaceWith($compile('<div id="table-wrapper"><as-table></as-table></div>')(scope));
      };
    }
  };
}]);