'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AppraiserDataService
 * @description
 * # AppraiserDataService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AppraiserDataService', function () {

    var appraiserCounters = {};

    this.getCounters = function (key) {
      switch (key) {
        case 'pending':
          return appraiserCounters.appraiserCountersPending;
        case 'completed':
          return appraiserCounters.appraiserCountersCompleted;
        case 'unpaid':
          return appraiserCounters.appraiserCountersUnpaid;
        case 'paid':
          return appraiserCounters.appraiserCountersPaid;
        case '':
          return appraiserCounters;
        default:
          return 0;
      }
    };

    this.saveCounters = function (params) {

      if (appraiserCounters.appraiserCountersPending !== params.pending) {
        appraiserCounters.appraiserCountersPending = params.pending;
      }
      if (appraiserCounters.appraiserCountersCompleted !== params.completed) {
        appraiserCounters.appraiserCountersCompleted = params.completed;
      }
      if (appraiserCounters.appraiserCountersUnpaid !== params.unpaid) {
        appraiserCounters.appraiserCountersUnpaid = params.unpaid;
      }
      if (appraiserCounters.appraiserCountersPaid !== params.paid) {
        appraiserCounters.appraiserCountersPaid = params.paid;
      }
    };

    return this;
  });
