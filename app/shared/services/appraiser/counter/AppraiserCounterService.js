'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AppraiserCounterService
 * @description
 * # AppraiserCounterService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AppraiserCounterService', function ($http, SessionService, API_PREFIX, AppraiserDataService) {

    var appraiserCounterService = {};

    appraiserCounterService.counters = function (params) {
      return $http
        .get(API_PREFIX() + '/v2.0/appraisers/'+SessionService.user.id+'/appraisals/counters', params)
        .then(function (res) {
          AppraiserDataService.saveCounters(res.data);
        });
    };

    return appraiserCounterService;
  });
