'use strict';

/**
 * @ngdoc service
 * @name frontendApp.Documents
 * @description
 * # Documents - this is a common service to handle document operations for appraiser
 * Creates the "Documents" service
 */
angular.module('frontendApp')
  .factory('AppraiserDocumentsService', [
    'FileUploader', 'API_PREFIX',
    function (FileUploader, API_PREFIX) {

      function uploadResume(id, file, method){
        return FileUploader.upload({
          url: API_PREFIX() + '/v2.0/appraisers/' + id + '/resume' ,
          method: method,
          file: file
        });
      }

      function uploadLicence(id, file, method){
        return FileUploader.upload({
            url: API_PREFIX() + '/v2.0/appraisers/' + id + '/license' ,
            method: method,
            file: file
        });
      }

      return {
        resume: {
          update: function(uid, file){
            return uploadResume(uid, file, 'PUT');
          },

          store: function(uid, file){
            return uploadResume(uid, file, 'POST');
          }
        },

        /* This license upload used on appraiser coverage -> coverage areas - add/edit functionality */
        license: {
          update: function(uid, file){
              return uploadLicence(uid, file, 'PUT');
          },

          store: function(uid, file){
              return uploadLicence(uid, file, 'POST');
          }
        }
      };
  }]);
