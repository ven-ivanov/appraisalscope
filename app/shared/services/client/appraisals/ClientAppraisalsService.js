'use strict';

/**
 * @ngdoc service
 * @name frontendApp.ClientAppraisalsService
 * @description
 * # ClientAppraisalsService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
.factory('ClientAppraisalsService', [
  '$resource', 'API_PREFIX',

  function ($resource, API_PREFIX) {
    return $resource(API_PREFIX() + '/v2.0/client/appraisals/request-new-appraisal/', null, {
      'post': {method: 'POST',url: API_PREFIX() + '/v2.0/email/send-help-msg'}
    });
  }
]);