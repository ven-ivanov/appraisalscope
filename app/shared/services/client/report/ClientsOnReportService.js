'use strict';

/**
 * @ngdoc service
 * @name frontendApp.clientsOnReport
 * @description Getting all job types from server
 * # ClientsOnReportService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
.factory('ClientsOnReportService', [
  '$resource', 'API_PREFIX',

  function ($resource, API_PREFIX) {

    var some = {};

    some.client = function(id){
      return $resource(API_PREFIX() + '/v2.0/appraiser/appraisals/clients-on-report/?id='+id);
    };

    return some;
  }
]);