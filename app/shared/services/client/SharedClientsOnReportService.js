'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.service:SharedClientsOnReportService
 * @description
 */
app.factory('SharedClientsOnReportService', [
  'API_PREFIX', '$resource',

  function (API_PREFIX, $resource) {

    var resource = $resource(API_PREFIX() + '/v2.0/client/companies/:id/clients-on-report', null, {
      get: {method: 'GET', isArray: false}
    });

    return {
      /**
       * Get search results for all clients on report for a company
       *
       * @param companyId {Number}
       * @returns {*|Function|promise|n}
       */
      getAll: function(companyId) {
        return resource.get({id: companyId}).$promise;
      }
    };
  }
]);
