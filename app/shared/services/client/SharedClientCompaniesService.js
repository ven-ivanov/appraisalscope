'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.service:SharedClientCompaniesService
 * @description
 */
app.factory('SharedClientCompaniesService', [
  'API_PREFIX', '$resource',

  function (API_PREFIX, $resource) {

    var resource = $resource(API_PREFIX() + '/v2.0/client/companies/:id', null, {
      get: {method: 'GET', isArray: false}
    });

    return {
      /**
       * Get a single company object by ID
       * @param companyId
       * @returns {*|Function|promise|n}
       */
      get: function(companyId) {
        return resource.get({id: companyId}).$promise;
      },

      /**
       * Get all companies
       *
       * @returns {*|Function|promise|n}
       */
      getAll: function() {
        return resource.get().$promise;
      }
    };
  }
]);
