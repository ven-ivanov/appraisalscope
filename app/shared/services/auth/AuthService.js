'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AuthService
 * @description
 * # AuthService
 * Service in the frontendApp.
 */
angular.module('frontendApp')
.factory('AuthService', [
  '$http', 'SessionService', 'API_PREFIX', 'ROLES', 'UserFeaturesService', '$rootScope',
  function ($http, SessionService, API_PREFIX, ROLES, UserFeaturesService, $rootScope) {
    var authService = {};

    /**
     * Authenticate and login
     * @param credentials
     * @returns Promise
     */
    authService.login = function (credentials) {
      return $http
      .post(API_PREFIX() + '/v2.0/auth/sign-in', credentials)
      .then(function (res) {
        SessionService.create(res.data);
        return SessionService.user;
      });
    };

    /**
     * Destroy session and remove token from localStorage on logout
     */
    authService.logout = function () {
      SessionService.destroy();
      localStorage.removeItem('token');
    };

    /**
     * Query the current user
     * @returns Promise
     */
    authService.getUser = function () {
      return $http
      .get(API_PREFIX() + '/v2.0/auth/my-basic-account')
      .then(function (res) {
        return UserFeaturesService.extend(res.data);
      });
    };

    /**
     * Determine if the current user is authenticated
     * @returns {boolean}
     */
    authService.isAuthenticated = function () {
      return !!SessionService.user;
    };

    authService.isAuthorized = function (authorizedTypes) {
      if (!angular.isArray(authorizedTypes)) {
        authorizedTypes = [authorizedTypes];
      }

      if (!authService.isAuthenticated()) {
        return false;
      }

      var role;

      //finds role of the current user
      if (SessionService.user.isAppraiser()) {
        role = ROLES.appraiser;
      } else if (SessionService.user.isClient()) {
        role = ROLES.client;
      } else if (SessionService.user.isAdmin()) {
        role = ROLES.admin;
      } else {
        throw 'User type is not supported';
      }

      return authorizedTypes.indexOf(role) !== -1;
    };

    /**
     * Redirect to dashboard on login
     * @param userType
     * @returns {string}
     */
    authService.getRedirectStateOnLogin = function (userType) {
      switch (userType) {
        case 2:
          return 'main.orders';
        case 3:
          return 'main.dashboard';
        default:
          return 'main';
      }
    };

    /**
     * Redirect to the requested page on page refresh
     * @returns {*}
     */
    authService.getRedirectStateOnRefresh = function () {
      // Redirect to the state to which the user loaded, if applicable
      if (angular.isDefined($rootScope.loadedAt)) {
        return $rootScope.loadedAt;
      }
      // Return main if no loaded at state
      return 'main';
    };

    return authService;
  }
]);
