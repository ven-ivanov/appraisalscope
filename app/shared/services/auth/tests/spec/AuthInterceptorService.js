'use strict';

describe('Service: AuthInterceptorService', function () {

  // load the service's module
  beforeEach(module('frontendApp'));

  // instantiate service
  var AuthInterceptorService;
  beforeEach(inject(function (_AuthInterceptorService_) {
    AuthInterceptorService = _AuthInterceptorService_;
  }));

  it('should do something', function () {
    expect(!!AuthInterceptorService).toBe(true);
  });

});
