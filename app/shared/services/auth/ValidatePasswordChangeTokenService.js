'use strict';

angular.module('frontendApp')
.factory('ValidatePasswordChangeTokenService', function ($http, API_PREFIX) {
  var validatePasswordChangeTokenService = {};

  validatePasswordChangeTokenService.validate = function (token) {
    return $http
    .post(API_PREFIX() + '/v2.0/auth/validate-reset-token', {token: token})
    .then(function (res) {
      return res.data;
    });
  };

  return validatePasswordChangeTokenService;
});