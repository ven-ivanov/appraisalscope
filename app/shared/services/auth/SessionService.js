'use strict';

/**
 * @ngdoc service
 * @name frontendApp.SessionService
 * @description
 * # SessionService
 * Service in the frontendApp.
 */
angular.module('frontendApp').service('SessionService', ['UserFeaturesService', function (UserFeaturesService) {
  // Set user and ID. Create token and save in localStorage
  this.create = function (user) {
    this.id = user.id;
    this.token = user.token;
    this.user = UserFeaturesService.extend(user);
    if (this.token) {
      localStorage.setItem('token', this.token);
    }
  };

  // Remove user and ID, destroy token in localStorage
  this.destroy = function () {
    this.id = undefined;
    this.token = undefined;
    this.user = undefined;
  };

  return this;
}]);
