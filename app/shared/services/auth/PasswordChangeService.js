'use strict';

angular.module('frontendApp')
.factory('PasswordChangeService', function ($http, API_PREFIX) {
  var passwordChangeService = {};

  passwordChangeService.reset = function (credentials) {

    return $http
    .post(API_PREFIX() + '/v2.0/auth/reset', credentials)
    .then(function (res) {
      return res.data;
    });
  };

  return passwordChangeService;
});