'use strict';

/**
 * @ngdoc service
 * @name frontendApp.UserFeaturesService
 * @description
 * # Adds some handful functionality to the "User" object
 * Service in the frontendApp.
 */
angular.module('frontendApp')
  .service('UserFeaturesService', [
    'USER_TYPES', 'APPRAISER_LEVELS', 'CLIENT_LEVELS',
    function (USER_TYPES, APPRAISER_LEVELS, CLIENT_LEVELS) {
      return {
        extend: function (user) {

          user.isClient = function () {
            return user.type === USER_TYPES.client;
          };

          user.isAppraiser = function () {
            return user.type === USER_TYPES.appraiser;
          };

          user.isAdmin = function () {
            return user.type === USER_TYPES.admin;
          };

          user.isStaff = function () {
            return user.type === USER_TYPES.staff;
          };

          if (user.isAppraiser()) {
            user.typeName = 'appraiser';
          }

          if (user.isClient()) {
            user.typeName = 'client';
          }

          if (user.isAdmin()) {
            user.typeName = 'admin';
          }

          if (user.isStaff()) {
            user.typeName = 'staff';
          }

          //provides some appraiser specific functions
          user.asAppraiser = {
            isJustManager: function () {
              return user.level === APPRAISER_LEVELS.justManager;
            },

            isAlsoManager: function () {
              return user.level === APPRAISER_LEVELS.alsoManager;
            }
          };

          //provides some client specific functions
          user.asClient = {
            isProcessor: function () {
              return user.level === CLIENT_LEVELS.processor;
            },

            isManager: function () {
              return user.level === CLIENT_LEVELS.manager;
            },

            isLoadOfficer: function () {
              return user.level === CLIENT_LEVELS.loanOfficer;
            }
          };

          return user;
        }
      };
    }]);
