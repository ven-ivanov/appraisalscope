'use strict';

angular.module('frontendApp')
.factory('RecoverService', function ($http, API_PREFIX) {
  var recoverService = {};

  recoverService.remind = function (email) {
    return $http
    .post(API_PREFIX() + '/v2.0/auth/remind', {email: email})
    .then(function (res) {
      return res.data;
    });
  };

  return recoverService;
});