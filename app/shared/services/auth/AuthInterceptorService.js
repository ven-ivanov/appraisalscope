'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AuthInterceptorService
 * @description
 * # AuthInterceptorService
 * A factory interceptor that injects the authentication token with each request.
 * It also redirect the user to the appropriate areas in case of errors.
 */
angular.module('frontendApp')
  .factory('AuthInterceptorService', function ($rootScope, $q) {
    return {
      // optional method
      'request': function (config) {
        // do something on success
        config.headers.token = localStorage.token;

        return config;
      },

      // optional method
      'requestError': function (rejection) {
        // do something on error
        //if (canRecover(rejection)) {
        //  return responseOrNewPromise
        //}
        return $q.reject(rejection);
      },


      // optional method
      'response': function (response) {
        // do something on success
        return response;
      },

      // optional method
      'responseError': function (rejection) {
        // do something on error
        //if (canRecover(rejection)) {
        //  return responseOrNewPromise
        //}
        return $q.reject(rejection);
      }
    };
  });
