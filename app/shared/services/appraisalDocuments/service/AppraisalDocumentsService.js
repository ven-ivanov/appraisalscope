'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AppraisalDocumentsService
 * @description
 * # Documents - this is a common service to handle document operations for appraiser
 * Creates the "Documents" service
 */
angular.module('frontendApp')
  .factory('AppraisalDocumentsService', [
    'FileUploader', 'API_PREFIX',
    function (FileUploader, API_PREFIX) {
      return {

        /**
         * uploads a report for new appraisal
         * @param file - information about the uploaded file
         * @param data - additional information required to successfully complete the process
         * @returns promise
         */
        report: {
          store: function(id,file){
            return FileUploader.upload({
              url: API_PREFIX() + '/v2.0/appraiser/appraisals/upload-document/',
              method: 'POST',
              file: file,
              data: {id:id}
            });
          },

          update: function(id, file){
            id   = null;
            file = null;
          }
        }
      };
    }
  ]);
