'use strict';

var app = angular.module('frontendApp');

/**
 * Conditionally load a directive on broadcast
 *
 * Usage:
 * template = '<template-here>'
 * DirectiveConditionalLoadService.init.call(scope, template, elem, 'appraiser-additional-details');
 */
app.factory('DirectiveConditionalLoadService', ['$compile', function ($compile) {
  return {
    init: function (template, elem, listenString) {
      var scope = this;
      // Compile directive when necessary
      var compile = scope.$on('compile-directive', function (event, directiveName) {
        // When the broadcast is received, compile and attach
        if (directiveName === listenString) {
          // Create directive and attach it
          template = $compile(template.join(''))(scope);
          elem.replaceWith(template);
          // Don't compile more than once
          compile();
        }
      });
    }
  };
}]);