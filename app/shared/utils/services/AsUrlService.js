'use strict';

var app = angular.module('frontendApp');

/**
 * Simple URL service for changing URLs
 */
app.factory('AsUrlService', ['$state', function ($state) {
  var Service = {
    /**
     * Change URL to maintain state
     */
    changeUrl: function (state) {
      $state.transitionTo('.', state, {location: true, inherit: true, relative: $state.$current, notify: false});
    }
  };
  return Service;
}]);