'use strict';

var app = angular.module('frontendApp');

/**
 * Inherit common request types in each resource service
 */
app.factory('RequestMethodsService', ['API_PREFIX', '$resource', function (API_PREFIX, $resource) {
  var Service = {
    /**
     * Basic GET request, expecting an object
     */
    getRequest: function (url, params) {
      return $resource(API_PREFIX() + url).get(params).$promise;
    },
    /**
     * POST request, with or without headers
     */
    post: function (url, params, body, headers) {
      if (!headers) {
        return new ($resource(API_PREFIX() + url, params))(body).$save();
      }
      // Specify put request with potential custom headers
      return new ($resource(API_PREFIX() + url, params, {
        postWithHeaders: {
          method: 'POST',
          headers: headers
        }
      }))(body).$postWithHeaders();
    },
    /**
     * PUT request
     */
    put: function (url, params, body, headers) {
      // Specify put request with potential custom headers
      return new ($resource(API_PREFIX() + url, params, {
        update: {
          method: 'PUT',
          headers: headers
        }
      }))(body).$update();
    },
    /**
     * PATCH request
     */
    patch: function (url, params, body, headers) {
      return new ($resource(API_PREFIX() + url, params, {
        update: {
          method: 'PATCH',
          headers: headers
        }
      }))(body).$update();
    },
    /**
     * POST request, sending an array as the body
     */
    postArray: function (url, params, body) {
      return new ($resource(API_PREFIX() + url, params, {
        postArray: {
          method: 'POST',
          transformRequest: function (data) {
            var thisData = angular.copy(data), response = [];
            // Iterate and send as array
            angular.forEach(thisData, function (item) {
              response.push(item);
            });
            return JSON.stringify(response);
          }
        }
      }))(body).$postArray();
    },
    /**
     * DELETE request
     */
    deleteRequest: function (url, params, headers) {
      return new ($resource(API_PREFIX() + url, params, {
        doDelete: {
          method: 'DELETE',
          headers: headers
        }
      }))().$doDelete();
    }
  };
  return Service;
}]);