'use strict';

/**
 *
 * @name frontendApp.PathService
 * @description
 * # Documents - this is a common service to find path of directives,controllers of a module
 */
angular.module('frontendApp')
  .factory('PathService',function(){
  return function(moduleName) {
    var rootPath = '/app/components/' + moduleName;
    return {
      directive: function(directiveName) {
        var directivePath = rootPath + '/directives/' + directiveName;
        return {
          template: function(templateName) {
            templateName = templateName || directiveName;
            return directivePath + '/' + templateName + '.html';
          },
          img: function(imgName) {
            return directivePath + '/images/' + imgName;
          }
        };
      },
      ctrl: function(controllerName) {
        var controllerPath = rootPath + '/controllers/' + controllerName;
        return {
          template: function(templateName) {
            templateName = templateName || controllerName;
            return controllerPath + '/' + templateName + '.html';
          },
          img: function(imgName) {
            return controllerPath + '/images/' + imgName;
          }
        };
      },
      img: function(imgName) {
        return rootPath + '/images/' + imgName;
      }
    };
  };
});
