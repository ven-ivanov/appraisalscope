describe('AsDataService', function () {
  var scope, Service, that = this, data, asTableService, httpBackend;
  beforeEach(function () {
    module('frontendApp');
  });

  beforeEach(inject(function ($rootScope, AsDataService, AsTableService, $httpBackend) {
    scope = $rootScope.$new();
    Service = AsDataService;
    asTableService = AsTableService;
    httpBackend = $httpBackend;

    // Caller service
    that.safeData = {
      test: {},
      heading: []
    };
    that.displayData = {
      test: []
    };
    that.transformers = {
      test: {
        'Test 1': 'blah'
      },
      id: {
        filter: 'currency'
      }
    };

    // Data to be hashed
    data = [{id: 1, test: 'Test 1'}, {id: 2, test: 'Test 2'}];

    // Spies
    spyOn(asTableService, 'applyFilters').and.callThrough();
    spyOn(JSON, 'stringify').and.callThrough();
    spyOn(JSON, 'parse').and.callThrough();
    spyOn(angular, 'copy').and.callThrough();

    httpBackend.whenGET(/receivable-unpaid/).respond({meta: [], data: [1,2,3]});
  }));

  describe('request', function () {

  });

  describe('loadRecords', function () {
    var options;
    var loadRecords = function (options) {
      try {
        Service.loadRecords(options);
      } catch (e) {
        expect(e.message).toEqual('Options, options.type, and options.resourceService must be defined in loadRecords');
      }
    };
    describe('missing params', function () {
      it('should not proceed without options', function () {
        loadRecords();
      });
      it('should not proceed without resource service', function () {
        loadRecords({type: 'Type'})
      });
      it('should not proceed without type', function () {
        loadRecords({resourceService: 'resourceService'})
      });
    });

    describe('immutable', function () {
      beforeEach(function () {
        this.beforeInitFormat = jasmine.createSpy('beforeInitFormat');
        this.safeData = {};
        this.immutable = true;
        this.hashDataImmutable = jasmine.createSpy('hashDataImmutable');
        this.formatDataImmutable = jasmine.createSpy('formatDataImmutable');
        options = {type: 'unpaid', resourceService: 'AccountingResourceService', callback: jasmine.createSpy('callback')};
        Service.loadRecords.call(this, options);
        httpBackend.flush();
        scope.$digest();
      });

      it('should retrieve data', function () {
        expect(this.beforeInitFormat).toHaveBeenCalledWith([ 1, 2, 3 ]);
      });
      it('should hash immutable', function () {
        expect(this.hashDataImmutable).toHaveBeenCalledWith([ 1, 2, 3 ], 'unpaid');
      });
      it('should format immutable', function () {
        expect(this.formatDataImmutable).toHaveBeenCalled();
      });
      it('should call the callback', function () {
        expect(options.callback).toHaveBeenCalled();
      });
    });

    describe('non-immutable', function () {
      beforeEach(function () {
        this.beforeInitFormat = jasmine.createSpy('beforeInitFormat');
        this.safeData = {};
        this.hashData = jasmine.createSpy('hashData');
        this.formatData = jasmine.createSpy('formatData');
        options = {type: 'unpaid', resourceService: 'AccountingResourceService'};
        Service.loadRecords.call(this, options);
        httpBackend.flush();
        scope.$digest();
      });

      it('should retrieve data', function () {
        expect(this.beforeInitFormat).toHaveBeenCalledWith([ 1, 2, 3 ]);
      });
      it('should hash data', function () {
        expect(this.hashData).toHaveBeenCalledWith([ 1, 2, 3 ], 'unpaid');
      });
      it('should format data', function () {
        expect(this.formatData).toHaveBeenCalled();
      });
    });
  });

  describe('hashData', function () {
    beforeEach(function () {
      that.idVal = 'testIdVal';
    });

    it('should throw an error if only data is passed in', function () {
      try {
        Service.hashData.call(that, data);
      } catch (e) {
        expect(e.message).toEqual('Data and type must be passed in to hashData');
      }
    });
    it('should set safe data if only data and type are passed in', function () {
      Service.hashData.call(that, data, 'test');
      expect(that.safeData.test).toEqual({
        1: {
          id: 1,
          test: 'Test 1'
        },
        2: {
          id: 2,
          test: 'Test 2'
        }
      });
    });
    it('should break references', function () {
      Service.hashData.call(that, data, 'test');
      expect(angular.copy).toHaveBeenCalled();
    });
    describe('callback', function () {
      beforeEach(function () {
        that.callback = function () {};
        spyOn(that, 'callback');
      });
      it('should call the callback when it is the third parameter', function () {
        Service.hashData.call(that, data, 'test', that.callback);
        expect(that.callback).toHaveBeenCalled();
      });
      it('should call the callback when it is the fourth parameter', function () {
        Service.hashData.call(that, data, 'test', 'idVal', that.callback);
        expect(that.callback).toHaveBeenCalled();
      });
    });
  });

  describe('hashDataImmutable', function () {
    var data, callback;
    beforeEach(function () {
      this.safeData = {};
      data = [{id:1}, {id:2}];
      spyOn(Service, 'setIdVal').and.callThrough();
      callback = jasmine.createSpy('callback');
      Service.hashDataImmutable.call(this, data, 'test', callback);
    });

    it('should set ID val if one is not specified', function () {
      expect(Service.setIdVal).toHaveBeenCalled();
    });
    it('should set safeData', function () {
      expect(this.safeData.test.toJS()).toEqual({ 1: { id: 1 }, 2: { id: 2 } });
    });
    it('should call callback as third argument', function () {
      expect(callback).toHaveBeenCalled();
    });
  });

  describe('setIdVal', function () {
    var response;
    it('should set ID to specified ID', function () {
      response = Service.setIdVal('Blah');
      expect(response).toEqual('Blah');
    });
    it('should set ID to id if none specified', function () {
      response = Service.setIdVal();
      expect(response).toEqual('id');
    });
  });

  describe('formatData', function () {
    beforeEach(function () {
      spyOn(Service, 'formatEach');
      spyOn(Service, 'applyHeadingFilters');
    });
    describe('error', function () {
      it('should throw an error if no type is specified', function () {
        try {
          Service.formatData.call(that);
        } catch (e) {
          expect(e.message).toEqual('formatData requires a type to be specified');
        }
      });
    });

    describe('no error', function () {
      beforeEach(function () {
        Service.formatData.call(that, 'test');
      });

      it('should break references', function () {
        expect(angular.copy).toHaveBeenCalled();
      });
      it('should format each', function () {
        expect(Service.formatEach).toHaveBeenCalled();
      });
      it('should apply filters according to heading definition', function () {
        expect(Service.applyHeadingFilters).toHaveBeenCalled();
      });
    });
  });

  describe('formatDataImmutable', function () {
    var callback;
    describe('error', function () {
      it('should throw an error if no type is specified', function () {
        try {
          Service.formatDataImmutable();
        } catch (e) {
          expect(e.message).toEqual('formatDataImmutable requires a type to be specified');
        }
      });
    });

    describe('no error', function () {
      beforeEach(function () {
        this.safeData = {test: [1,2]};
        callback = jasmine.createSpy('callback');
        spyOn(Service, 'formatEachImmutable');
        Service.formatDataImmutable.call(this, 'test', callback);
      });

      it('should format each as immutable', function () {
        expect(Service.formatEachImmutable).toHaveBeenCalledWith([ 1, 2 ], 'test');
      });
      it('should call the callback', function () {
        expect(callback).toHaveBeenCalled();
      });
    });
  });

  describe('formatSingle', function () {

  });

  describe('formatSingleImmutable', function () {

  });

  describe('formatEach', function () {
    beforeEach(function () {
      that.transformData = jasmine.createSpy(that, 'transformData');
      Service.formatEach.call(that, [{id:1}, {id:2}], 'test');
    });
    it('should call transform data for each record', function () {
      expect(that.transformData.calls.count()).toEqual(2);
    });
    it('should push records onto displayData', function () {
      expect(that.displayData.test).toEqual([{id:1}, {id:2}]);
    });
  });

  describe('formatEachImmutable', function () {

  });

  describe('applyHeadingFilters', function () {
    beforeEach(function () {
      Service.applyHeadingFilters.call(that, []);
    });

    it('should call applyFilters', function () {
      expect(asTableService.applyFilters).toHaveBeenCalled();
    });
  });

  describe('transformData', function () {
    var transformInputData;
    beforeEach(function () {
      transformInputData = [{
                              test: '',
                              num: '',
                              num2: '',
                              source: {
                                obj: {
                                  test: 'blah'
                                }
                              },
                              normal: 1
                            }, {
                              test: '',
                              num: '',
                              num2: '',
                              source: '',
                              normal: 1
                            }];
      that.transformers = {
        test: {
          filter: 'currency'
        },
        num: {
          filter: 'number'
        },
        num2: {
          filter: 'number:2'
        },
        date: {
          filter: 'date'
        },
        source: {
          source: 'source.obj.test',
          dest: 'test1'
        },
        normal: {
          1: 'test'
        }
      };
    });

    describe('transformData', function () {
      beforeEach(function () {
        that.beforeFormatEach = jasmine.createSpy(that, 'beforeFormatEach');
        spyOn(Service, 'addTransformDestinationSeeds');
        spyOn(Service, 'undoApplyTransformation');
        spyOn(Service, 'applyTransformation');
      });
      describe('apply transformations', function () {
        beforeEach(function () {
          Service.transformData.call(that, transformInputData[0]);
        });
        it('should call addTransformDestinationSeeds', function () {
          expect(Service.addTransformDestinationSeeds).toHaveBeenCalled();
        });
        it('should apply transformations', function () {
          expect(Service.applyTransformation).toHaveBeenCalled();
        });
        it('should call beforeFormatEach if available', function () {
          expect(that.beforeFormatEach).toHaveBeenCalled();
        });
      });

      describe('undo transformations', function () {
        beforeEach(function () {
          Service.transformData.call(that, transformInputData[0], true);
        });
        it('should apply transformations', function () {
          expect(Service.undoApplyTransformation).toHaveBeenCalled();
        });
      });
    });

    describe('undoApplyTransformation', function () {
      beforeEach(function () {
        spyOn(Service, 'undoCurrencyFilter');
        spyOn(Service, 'undoDateFilter');
      });

      it('should undo currency transformation', function () {
        Service.undoApplyTransformation(that, transformInputData[0], 'test', '$100');
        expect(Service.undoCurrencyFilter).toHaveBeenCalled();
      });
      it('should undo date transformations', function () {
        Service.undoApplyTransformation(that, transformInputData[0], 'date', '06/10/2010');
        expect(Service.undoDateFilter).toHaveBeenCalled();
      });
    });

    describe('removePrivateVars', function () {

    });

    describe('applyTransformation', function () {
      beforeEach(function () {
        spyOn(Service, 'deepSearchObjectProperty');
        spyOn(Service, 'filterTransformer');
        that.transformers = _.assign(that.transformers, {stat: {staticVal: 'swooty'}});
      });

      it('should perform a deep object search to find the correct property', function () {
        Service.applyTransformation(that, transformInputData[1], 'source');
        expect(Service.deepSearchObjectProperty).toHaveBeenCalled();
      });
      it('should apply a filter transformation', function () {
        Service.applyTransformation(that, transformInputData[0], 'test');
        expect(Service.filterTransformer).toHaveBeenCalled();
      });
      it('should apply a key-value transformation', function () {
        Service.applyTransformation(that, transformInputData[0], 'normal', 1);
        expect(transformInputData[0].normal).toEqual('test');
      });
      it('should add static values', function () {
        Service.applyTransformation(that, transformInputData[0], 'stat');
        expect(transformInputData[0].stat).toEqual('swooty');
      });
    });

    describe('applyTransformationImmutable', function () {

    });

    describe('addTransformDestinationSeeds', function () {
      var record = {};
      beforeEach(function () {
        that.transformers = {
          complete: {
            source: 'swiggity.swooty',
            dest: 'dat'
          },
          test: {
            source: 'blah.test'
          },
          stat: {
            staticVal: 'whatevs'
          }
        };
        Service.addTransformDestinationSeeds(that, record);
      });

      it('should add a seed when there is a source and destination', function () {
        expect(record.dat).toEqual('');
      });
      it('should add a seed when there is only a source', function () {
        expect(record.test).toEqual('');
      });
      it('should add a seed for static vals', function () {
        expect(record.stat).toEqual('');
      });
    });

    describe('deepSearchObjectProperty', function () {
      describe('with source', function () {
        beforeEach(function () {
          Service.deepSearchObjectProperty(that, transformInputData[0], 'source');
        });
        it('should find the right piece of data', function () {
          expect(transformInputData[0].test1).toEqual('blah');
        });
      });

      describe('no destination specified', function () {
        beforeEach(function () {
          that.transformers = _.assign(that.transformers, {source:{source:'source.obj.test'}});
          Service.deepSearchObjectProperty(that, transformInputData[0], 'source');
        });
        it('should find the right piece of data', function () {
          expect(transformInputData[0].source).toEqual('blah');
        });
      });

      describe('source as function', function () {
        beforeEach(function () {
          that.transformers = _.assign(that.transformers, {source:{source: function () {
            return 'what';
          }}});
          Service.deepSearchObjectProperty(that, transformInputData[0], 'source');
        });
        it('should find the right piece of data', function () {
          expect(transformInputData[0].source).toEqual('what');
        });
      });
    });

    describe('filterTransformer', function () {
      var service, data;
      beforeEach(function () {
        data = {id:1, filter: 'replace-char'};
        service = this;
      });
      describe('single filter', function () {
        beforeEach(function () {
          service.transformers = {filter: {filter: 'replaceChar:-: '}};
          Service.filterTransformer(service, 'filter', data);
        });

        it('should apply a single filter', function () {
          expect(data).toEqual({ id: 1, filter: 'replace char' });
        });
      });

      describe('multiple filters', function () {
        beforeEach(function () {
          service.transformers = {filter: {filter: 'replaceChar:-: |capitalizeFirst'}};
          Service.filterTransformer(service, 'filter', data);
        });

        it('should apply both filters', function () {
          expect(data).toEqual({ id: 1, filter: 'Replace char' });
        });
      });
    });

    describe('filterTransformerImmutable', function () {

    });

    describe('undoDateFilter', function () {
      var response;
      it('should be able to parse display format', function () {
        response = Service.undoDateFilter('Jun 10, 2010');
        expect(response).toEqual('2010-06-10T00:00:00.000Z');
      });

      it('should be able to parse asdatetimepicker format', function () {
        response = Service.undoDateFilter('06/10/2010');
        expect(response).toEqual('2010-06-10T00:00:00.000Z');
      });
    });

    describe('undoCurrencyFilter', function () {
      it('should remove everything thats not a decimal or number', function () {
        var input = '$104a.41';
        input = Service.undoCurrencyFilter(input);
        expect(input).toEqual('104.41');
      });
    });

    describe('getTransformerKeyByValue', function () {
      var transformer, value, result;
      beforeEach(function () {
        transformer = {test: 'blah'};
        value = 'blah';
        result = Service.getTransformerKeyByValue(transformer, value);
      });

      it('should find the appropriate property', function () {
        expect(result).toEqual('test');
      });
    });
  });

  describe('formatPlainObjectData', function () {
    it('should throw an error if not given type', function () {
      try {
        Service.formatPlainObjectData.call(that);
      } catch (e) {
        expect(e.message).toEqual('Type must be passed to formatPlainObject');
      }
    });

    beforeEach(function () {
      that.transformData = function(){};
      spyOn(that, 'transformData');
      that.safeData.test2 = {
        test1: 'Test1',
        currency: 100
      };
      that.safeData.heading = [];
      Service.formatPlainObjectData.call(that, 'test2');
    });

    it('should call transformData', function () {
      expect(that.transformData).toHaveBeenCalled();
    });
    it('should render display data', function () {
      expect(that.displayData.test2).toEqual({ test1: 'Test1', currency: 100 });
    });
  });

  describe('formatPlainObjectDataImmutable', function () {

  });

  describe('deepDynamicReplace', function () {

  });

  describe('undoFilterTransformation', function () {

  });

  describe('transformDataImmutable', function () {

  });

  describe('addTransformDestinationSeedsImmutable', function () {

  });

  describe('computeObjectDiff', function () {
    var prevObj, currObj, diff;

    it('should return the difference for objects structured the same', function () {
      prevObj = {a: 1, b: 2, c: {d: 1, e: {f: 2, g: 1}}};
      currObj = {a: 1, b: 3, c: {d: 4, e: {f: 5, g: 1}}};
      diff = Service.computeObjectDiff(prevObj, currObj);
      expect(diff).toEqual({ b: 3, c: { d: 4, e: { f: 5 } } });
    });
    it('should all the addition of keys', function () {
      prevObj = {a: 1, b: 2};
      currObj = {a: 1, b: 2, e: 3};
      diff = Service.computeObjectDiff(prevObj, currObj);
      expect(diff).toEqual({e: 3});
    });
    it('should throw an error on the deletion of keys', function () {
      prevObj = {a: 1, b: 2};
      currObj = {a: 1};
      try {
        diff = Service.computeObjectDiff(prevObj, currObj);
      } catch (e) {
        expect(e.message).toEqual('Removing object properties is not supported in this method');
      }
    });
  });
});