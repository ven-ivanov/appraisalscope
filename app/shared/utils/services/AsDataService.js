'use strict';

var app = angular.module('frontendApp');

app.factory('AsDataService', ['AsTableService', '$filter', '$injector', function (AsTableService, $filter, $injector) {
  var Service = {
    /**
     * Standard requests
     */
    request: function (resourceService, requestObject, requestFn, type, params) {
      // The service to which this is bound
      var service = this;
      var ResourceService = $injector.get(resourceService);
      // Partial args
      if (arguments.length === 3 && angular.isObject(type)) {
        params = type;
        type = null;
      }
      // Get request type
      if (angular.isFunction(service.getRequestType)) {
        type = service.getRequestType(type);
      }
      // Get $resource configuration
      return ResourceService[requestObject][requestFn](type, params);
    },
    /**
     * Retrieve data on load
     */
    loadRecords: function (options) {
      var service = this, ResourceService, hashValue, formatCallback = service.formatCallback || angular.noop;
      if (angular.isUndefined(options) || angular.isUndefined(options.type) || angular.isUndefined(options.resourceService)) {
        throw Error('Options, options.type, and options.resourceService must be defined in loadRecords');
      }
      // Default to type
      hashValue = angular.isDefined(options) && angular.isString(options.hashValue) ? options.hashValue :  options.type;
      // Get resource service
      ResourceService = $injector.get(options.resourceService);
      // Load records
      return ResourceService[options.type].init(options.params, hashValue)
      .then(function (response) {
        // Extract meta data
        if (angular.isDefined(response.meta)) {
          service.safeData.meta = angular.copy(response.meta);
        }
        // Leave only data
        if (angular.isArray(response.data)) {
          response = response.data;
        }
        // Any operations that need to be performed before hashing/displaying
        if (angular.isFunction(service.beforeInitFormat)) {
          service.beforeInitFormat(response);
        }
        // Immutable
        if (service.immutable) {
          // Hash
          service.hashDataImmutable(response, hashValue);
          // Format
          service.formatDataImmutable(formatCallback);
        // Non-immutable
        } else {
          // Hash
          service.hashData(response, hashValue);
          // Format
          service.formatData(formatCallback);
        }
      }).then(function () {
        // If callback
        if (angular.isFunction(options.callback)) {
          options.callback();
        }
      });
    },
    /**
     * Create data hash
     * Note: This method is intended to be bound to a service using Service.hashData = AsDataService.hashData.bind(Service);
     * @param data Array Data objects
     * @param type String Safe data key
     * @param idVal String ID values - Defaults to 'id'
     * @param callback Function Optional callback function
     */
    hashData: function (data, type, idVal, callback) {
      if (arguments.length < 2) {
        throw Error('Data and type must be passed in to hashData');
      }
      var service = this;
      // Default to id
      idVal = idVal === 'id' ? 'id' : Service.setIdVal(idVal);
      // Break references
      data = angular.copy(data);
      // Reset safe data of this type
      service.safeData[type] = {};
      // Store safe data
      if (angular.isArray(data)) {
        Lazy(data)
        .each(function (record) {
          service.safeData[type][record[idVal]] = record;
        });
        // Plain objects without IDs
      } else {
        Lazy(data)
        .each(function (record, key) {
          service.safeData[type][key] = record;
        });
      }
      // Partial application
      if (arguments.length !== 4 && angular.isFunction(arguments[arguments.length - 1])) {
        callback = arguments[arguments.length - 1];
      }
      // Callback
      if (angular.isDefined(callback)) {
        callback();
      }
    },
    /**
     * Create data hash using immutable data structure
     * Note: This method is intended to be bound to a service using Service.hashData = AsDataService.hashData.bind(Service);
     * @param data Array Data objects
     * @param type String Safe data key
     * @param idVal String ID values - Defaults to 'id'
     * @param callback Function Optional callback function
     */
    hashDataImmutable: function (data, type, idVal, callback) {
      if (arguments.length < 2) {
        throw Error('Data and type must be passed in to hashData');
      }
      var service = this;
      // Default to id
      idVal = idVal === 'id' ? 'id' : Service.setIdVal(idVal);
      // Initial data structure from DB records
      data = Immutable.fromJS(data);
      var safeData = Immutable.Map();
      // Immutable List type
      if (Immutable.List.isList(data)) {
        // Iterate and create safeData as map
        data.map(function (item) {
          safeData = safeData.set(item.get(idVal), item);
        });
        // Plain objects without IDs
        // @todo Need to find an instance and use immutable
      } else {
        Lazy(data).each(function (record, key) {
          service.safeData[type][key] = record;
        });
      }
      // Partial application
      if (arguments.length !== 4 && angular.isFunction(arguments[arguments.length - 1])) {
        callback = arguments[arguments.length - 1];
      }
      // Callback
      if (angular.isDefined(callback)) {
        callback();
      }
      // Set to safeData
      service.safeData[type] = safeData;
    },
    /**
     * Set ID val for hashdata
     * @param idVal
     * @returns {*}
     */
    setIdVal: function (idVal) {
      // If idVal isn't set, or if a callback is passed as the third argument, return 'id'
      if (angular.isUndefined(idVal) || angular.isFunction(idVal)) {
        return 'id';
      }
      // Return value for idVal
      return idVal;
    },
    /**
     * Format data for display
     *
     * Note: This method is intended to be bound to a service using Service.formatData = AsDataService.formatData.bind(Service);
     * @param type Safe data property from which to create data
     * @param callback Function Callback function
     * @param heading Object Heading override
     */
    formatData: function (type, callback, heading) {
      var data, service = this;
      if (!type) {
        throw Error('formatData requires a type to be specified');
      }
      // Heading override without callback
      if (!angular.isFunction(callback) && arguments.length === 2) {
        heading = callback;
      }
      // Reset display data
      service.displayData[type] = [];
      // Break references
      data = angular.copy(service.safeData[type]);
      // Iterate and format
      Service.formatEach.call(service, data, type);
      // Apply filters if heading is defined
      Service.applyHeadingFilters.call(service, heading, type, callback);
    },
    /**
     * Format data for display (immutable records)
     *
     * Note: This method is intended to be bound to a service using Service.formatData = AsDataService.formatData.bind(Service);
     * @param type Safe data property from which to create data
     * @param callback Function Callback function
     */
    formatDataImmutable: function (type, callback) {
      var data, service = this;
      if (!type) {
        throw Error('formatDataImmutable requires a type to be specified');
      }
      // Callback default to noop
      callback = callback || angular.noop;
      // Copy immutable data structure
      data = service.safeData[type];
      // Iterate and format
      Service.formatEachImmutable.call(service, data, type);
      // Callback
      callback();
    },
    /**
     * Format a single record
     */
    formatSingle: function (type, callback, id) {
      var safeDataRecord, service = this, index = null;
      if (!type) {
        throw Error('formatData requires a type to be specified');
      }
      // Accept two arguments
      if (!angular.isFunction(callback) && arguments.length === 2) {
        id = callback;
      }
      // Get the record in question
      safeDataRecord = angular.copy(service.safeData[type][id]);
      // Retrieve index of the item to be formatted
      Lazy(service.displayData[type])
      .each(function (record, key) {
        if (record.id === id) {
          // Kep reference to index
          index = key;
        }
      });
      // If no record, throw error
      if (index === null) {
        throw Error('Could not find record to format');
      }
      // Deleted record
      if (!safeDataRecord) {
        // Delete record
        delete service.displayData[type][index];
        // Reindex display data
        service.displayData[type] = service.displayData[type].filter(function (record) {
          return angular.isDefined(record);
        });
        // Apply transformations
      } else {
        // Transform
        service.transformData.call(service, safeDataRecord);
        // Set for display
        service.displayData[type][index] = safeDataRecord;
      }
    },
    /**
     * Format a single record (immutable)
     */
    formatSingleImmutable: function (type, callback, id) {
      var safeDataRecord, service = this, index = null;
      if (!type) {
        throw Error('formatData requires a type to be specified');
      }
      // Accept two arguments
      if (!angular.isFunction(callback) && arguments.length === 2) {
        id = callback;
      }
      // Get the record in question
      safeDataRecord = service.safeData[type].get(id);
      // Retrieve index of the item to be formatted
      service.displayData[type].forEach(function (record, key) {
        if (record.get('id') === id) {
          index = key;
          // Stop iterating
          return false;
        }
        return record;
      });
      // If no record, throw error
      if (index === null) {
        throw Error('Could not find record to format');
      }
      // Deleted record
      if (!safeDataRecord) {
        // Delete record
        service.displayData[type] = service.displayData[type].delete(index);
        // Apply transformations
      } else {
        // Transform
        service.displayData[type][index] = service.transformDataImmutable.call(service, safeDataRecord);
      }
    },
    /**
     * Format each data item in the record collection
     * @param data Records
     * @param type Property for safeData and displayData
     */
    formatEach: function (data, type) {
      var service = this;
      angular.forEach(data, function (data) {
        if (data) {
          // Transform as necessary
          service.transformData(data);
          // Push onto display
          service.displayData[type].push(data);
        }
      });
    },
    /**
     * Format each data item in the record collection (immutable)
     * @param data Records
     * @param type Property for safeData and displayData
     */
    formatEachImmutable: function (data, type) {
      var service = this;
      data = data.map(function (record) {
        // Transform as necessary
        return service.transformDataImmutable(record);
      });
      // Set for display
      service.displayData[type] = data.toList();
    },
    /**
     * Apply filters as defined in table heading
     * @param heading Table heading
     * @param type Display data property
     * @param callback Callback function
     */
    applyHeadingFilters: function (heading, type, callback) {
      var service = this;
      // Set to noop if not a function
      if (!angular.isFunction(callback)) {
        callback = angular.noop;
      }
      // If table heading is defined
      if (angular.isArray(heading) || angular.isArray(service.safeData.heading)) {
        // Apply any necessary table filters
        AsTableService.applyFilters(service.displayData[type], heading || service.safeData.heading)
        .then(function () {
          // Callback
          callback();
        });
      } else {
        // Callback
        callback();
      }
    },
    /**
     * Apply relevant transformations to data for display
     * @param data Object Record to transform
     * @param undo Boolean Whether to undo the transformation - false by default
     */
    transformData: function (data, undo) {
      var service = this;
      undo = undo || false;
      // If we have a before individual format function to run, now's the time
      if (angular.isFunction(service.beforeFormatEach)) {
        service.beforeFormatEach(data);
      }
      // Don't run if there are no transformers
      if (angular.isUndefined(service.transformers)) {
        return;
      }
      // Find any transformers which are for deep selection and add seed properties
      if (!undo) {
        Service.addTransformDestinationSeeds(service, data);
      }
      // Apply transformers
      Lazy(data)
      .each(function (value, key) {
        // If we have a transformer, apply it
        if (angular.isDefined(service.transformers[key])) {
          if (undo) {
            // Undo transformation for writing to database
            Service.undoApplyTransformation(service, data, key, value);
          } else {
            // Apply a transformation for display
            Service.applyTransformation(service, data, key, value);
          }
        }
      });
    },
    /**
     * Add in seeds for applying source/destination transformers
     * @param service Service as context
     * @param data Record
     */
    addTransformDestinationSeeds: function (service, data) {
      var destination;
      angular.forEach(service.transformers, function (transformer, key) {
        // Determine destination from transformer key, or else hardcoded destination
        destination = transformer.dest || key;
        // Source/destination copying
        if (transformer.source && angular.isUndefined(data[destination])) {
          data[destination] = '';
          // Static value
        } else if (transformer.staticVal) {
          data[destination] = '';
        }
      });
    },
    /**
     * Apply a transformation for display
     * @param service Service as context
     * @param data Complete record
     * @param key Property
     * @param value Property value
     */
    applyTransformation: function (service, data, key, value) {
      // See if we need to make a transformation by source attribute, typically for flattening an array for a table
      if (angular.isDefined(service.transformers[key].source) && data[key] === '') {
        Service.deepSearchObjectProperty(service, data, key);
      }
      // If we have any static values, add those
      if (angular.isDefined(service.transformers[key].staticVal)) {
        data[key] = service.transformers[key].staticVal;
      }
      // Filter transformation
      if (angular.isDefined(service.transformers[key].filter)) {
        // Apply a filter transformation
        Service.filterTransformer(service, key, data);
      }
      // key->value relationship transformation
      if (angular.isDefined(service.transformers[key][value])) {
        data[key] = service.transformers[key][value];
      }
    },
    /**
     * Apply a transformation for display (immutable)
     * @param service Service as context
     * @param data Complete record
     * @param key Property
     */
    applyTransformationImmutable: function (service, data, key) {
      // See if we need to make a transformation by source attribute, typically for flattening an array for a table
      if (angular.isDefined(service.transformers[key].source) && data.get(key) === '') {
        data = data.set(key, Service.deepSearchObjectProperty(service, data, key));
        // If we have any static values, add those
      } else if (angular.isDefined(service.transformers[key].staticVal)) {
        data = data.set(key, service.transformers[key].staticVal);
      }
      // Filter transformation
      if (angular.isDefined(service.transformers[key].filter)) {
        // Apply a filter transformation
        data = data.set(key, Service.filterTransformerImmutable(service, key, data));
      }
      return data.get(key);
    },
    /**
     * Search for dynamically deep object properties and place them into the correct location
     * @param service Service as context
     * @param data Record
     * @param key Property
     */
    deepSearchObjectProperty: function (service, data, key) {
      var destination = service.transformers[key].dest;
      var source = service.transformers[key].source;
      var transformedValue;
      // Use key for destination if one isn't defined
      destination = destination || key;
      // Immutable
      if (Immutable.Iterable.isIterable(data)) {
        // Get by function
        if (angular.isFunction(source)) {
          return source(data);
        }
        return data.getIn(source.split('.'));
        // Non-immutable
      } else {
        // Attach to record
        if (angular.isFunction(source)) {
          // Handle transformations by reference
          transformedValue = source(data);
          // Handle return value
          if (transformedValue) {
            data[destination] = transformedValue;
          }
        } else {
          data[destination] = _.get(data, source) || '';
        }
      }
    },
    /**
     * Apply a transformation based on a filter
     * @param service Service being used as context
     * @param key Data key
     * @param data Data
     */
    filterTransformer: function (service, key, data) {
      var args, filter, filterInput = service.transformers[key].filter, filters;
      // Multiple filters
      if (filterInput.indexOf('|')) {
        filters = filterInput.split('|');
        // Singular filter
      } else {
        filters = [filterInput];
      }
      // iterate filters and apply each
      Lazy(filters)
      .each(function (thisFilter) {
        // Filter with arguments
        if (thisFilter.indexOf(':')) {
          args = thisFilter.split(':');
          // First argument is filter name
          filter = args.shift();
          // Add input string to beginning of array
          args.unshift(data[key]);
          // Apply with args
          data[key] = $filter(filter).apply(null, args);
          // Plain filter
        } else {
          data[key] = $filter(thisFilter)(data[key]);
        }
      });
    },
    /**
     * Apply a transformation based on a filter (immutable)
     * @param service Service being used as context
     * @param key Data key
     * @param data Data
     */
    filterTransformerImmutable: function (service, key, data) {
      var args, filter, filterInput = service.transformers[key].filter, filters;
      // Multiple filters
      if (filterInput.indexOf('|')) {
        filters = filterInput.split('|');
        // Singular filter
      } else {
        filters = [filterInput];
      }
      var input = data.get(key);
      // iterate filters and apply each
      filters.forEach(function (thisFilter) {
        // Filter with arguments
        if (thisFilter.indexOf(':')) {
          args = thisFilter.split(':');
          // First argument is filter name
          filter = args.shift();
          // Add input string to beginning of array
          args.unshift(input);
          // Apply with args
          input = $filter(filter).apply(null, args);
          // Plain filter
        } else {
          input = $filter(thisFilter)(input);
        }
      });
      return input;
    },
    /**
     * Undo a transformation for writing to the database
     * @param service Service as context
     * @param data Complete record
     * @param key Property
     * @param value Property value
     */
    undoApplyTransformation: function (service, data, key, value) {
      var destination;
      // Get transformer, if not by filtered
      if (angular.isUndefined(service.transformers[key].filter)) {
        data[key] = Service.getTransformerKeyByValue(service.transformers[key], value);
      } else {
        // Undo a filter application
        Service.undoFilterTransformation(service, data, key, value);
      }
      // Delete item if null or undefined, since we don't want to be overwriting everything on the backend
      if (angular.isUndefined(value) || value === null) {
        delete data[key];
      }
      // Figure out destination
      destination = service.transformers[key].dest || key;
      if (angular.isDefined(service.transformers[key].source)) {
        // Revert source function
        if (angular.isFunction(service.transformers[key].source)) {
          service.transformers[key].revert(data);
        } else {
          // Undo any dynamically deep transformations
          Service.deepDynamicReplace(data, service.transformers[key].source,
          data[destination]);
        }
      }
      // Remove private vars before writing
      Service.removePrivateVars(data);
    },
    /**
     * Remove private variables from a record before writing
     */
    removePrivateVars: function (data) {
      // Remove any private variables that hold data structures, rather than primitive values
      angular.forEach(data, function (prop, key) {
        if (key.indexOf('_') === 0 && angular.isObject(prop)) {
          delete data[key];
        }
      });
    },
    /**
     * Replace any arbitrarily deep properties
     * This is typically done for flattening deep object elements for display in tables
     * @param obj Record
     * @param prop String Object property location string representation
     * @param value Value to replace property with
     */
    deepDynamicReplace: function (obj, prop, value) {
      var element;
      if (angular.isString(prop)) {
        prop = prop.split('.');
      }

      if (prop.length > 1) {
        element = prop.shift();
        Service.deepDynamicReplace(obj[element] =
                                   Object.prototype.toString.call(obj[element]) === '[object Object]' ? obj[element] :
                                   {}, prop, value);
      } else {
        obj[prop[0]] = value;
      }
    },
    /**
     * Retrieve a transformer key by value
     *
     * @todo - This creates a potential conflict in case there are duplicate values
     *
     * @param transformer
     * @param value
     * @returns {string}
     */
    getTransformerKeyByValue: function (transformer, value) {
      for (var prop in transformer) {
        if (transformer.hasOwnProperty(prop) && transformer[prop] === value) {
          return prop;
        }
      }
      return value;
    },
    /**
     * Undo a filter transformation
     * @param service Service as context
     * @param data Complete record
     * @param key Property
     * @param value Property value
     */
    undoFilterTransformation: function (service, data, key, value) {
      // Date filter
      if (service.transformers[key].filter) {
        // Undo date filter
        if (service.transformers[key].filter === 'date') {
          /**
           * @todo
           */
          data[key] = Service.undoDateFilter(value);
          // Undo currency filter
        } else if (service.transformers[key].filter === 'currency') {
          /**
           * @todo
           */
          data[key] = Service.undoCurrencyFilter(value);
        }
      }
    },
    /**
     * Undo filter for writing to DB
     */
    undoDateFilter: function (dateVal) {
      return (new Date(dateVal + ' UTC')).toISOString();
    },
    /**
     * Simple undo currency filter
     */
    undoCurrencyFilter: function (record) {
      return record.replace(/[^\d.]/g, '');
    },
    /**
     * Handle plain objects for display
     * Note: This method is intended to be bound to a service using Service.formatPlainObjectData = AsDataService.formatPlainObjectData.bind(Service);
     */
    formatPlainObjectData: function (type) {
      var service = this;
      if (angular.isUndefined(type)) {
        throw Error('Type must be passed to formatPlainObject');
      }
      // Set display data
      service.displayData[type] = angular.copy(service.safeData[type]);
      // Apply any transformations
      service.transformData(service.displayData[type]);
    },
    /**
     * Format plain object immutable
     */
    formatPlainObjectDataImmutable: function (type) {
      var service = this;
      if (angular.isUndefined(type)) {
        throw Error('Type must be passed to formatPlainObject');
      }
      // Copy to temp (don't run the watcher yet)
      var temp = service.safeData[type];
      // Apply any transformations
      service.transformDataImmutable(temp);
      // Set to display data
      service.displayData[type] = temp;
    },
    /**
     * Transform data using immutable safe data
     * @param data
     * @param undo
     */
    transformDataImmutable: function (data, undo) {
      var service = this;
      undo = undo || false;
      // If we have a before individual format function to run, now's the time
      if (angular.isFunction(service.beforeFormatEach)) {
        service.beforeFormatEach(data);
      }
      // Don't run if there are no transformers
      if (angular.isUndefined(service.transformers)) {
        return;
      }
      // Find any transformers which are for deep selection and add seed properties
      if (!undo) {
        data = Service.addTransformDestinationSeedsImmutable(service, data);
      }
      // Apply transformers
      data = data.toSeq().map(function (record, key) {
        if (angular.isDefined(service.transformers[key])) {
          if (undo) {
            // Undo transformation for writing to database
            Service.undoApplyTransformation(service, data, key, record);
          } else {
            // Apply a transformation for display
            return Service.applyTransformationImmutable(service, data, key, record);
          }
        }
        return record;
      }).toMap();
      // Return transformed data set
      return data;
    },
    /**
     * Add in seeds for applying source/destination transformers
     * @param service Service as context
     * @param data Record
     */
    addTransformDestinationSeedsImmutable: function (service, data) {
      var destination;
      angular.forEach(service.transformers, function (transformer, key) {
        // Determine destination from transformer key, or else hardcoded destination
        destination = transformer.dest || key;
        // Source/destination copying (or static value preparation) for Immutable collections
        if (Immutable.Iterable.isIterable(data)) {
          if ((transformer.source && angular.isUndefined(data.get(destination))) || transformer.staticVal) {
            data = data.set(destination, '');
          }
          // Non-immutable
        } else if (transformer.source && angular.isUndefined(data[destination]) || transformer.staticVal) {
          data[destination] = '';
        }
      });
      return data;
    },
    /**
     * Computer the difference between two *PLAIN* objects
     *
     * Note that this cannot calculate structural changes. Only updates to properties existing in both objects
     */
    computeObjectDiff: function (prevObj, newObj) {
      var changes = {};
      // If we lose any keys
      if (Object.keys(prevObj).length > Object.keys(newObj).length) {
        throw Error('Removing object properties is not supported in this method');
      }
      // Iterate new object
      Lazy(newObj)
      .each(function (val, key) {
        if (prevObj[key] !== newObj[key]) {
          // Recurse
          if (angular.isObject(newObj[key])) {
            var c = Service.computeObjectDiff(prevObj[key], newObj[key]);
            if (!_.isEmpty(c)) {
              changes[key] = c;
            }
            // Property
          } else {
            changes[key] = newObj[key];
          }
        }
      });
      return changes;
    }
  };
  return Service;
}]);