'use strict';

var app = angular.module('frontendApp');

/**
 * Replace a truth value with "Yes" or "No"
 */
app.filter('boolYesNo', [ function() {
  return function(text) {
    // Empty string, 0, or -- (table special case)
    return (!text || text === '--') ? 'No' : 'Yes';
  };
}]);