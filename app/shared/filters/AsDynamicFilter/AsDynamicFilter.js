'use strict';

var app = angular.module('frontendApp');

/**
 * Dynamically apply a filter (typically useful during ng-repeat)
 */
app.filter('asDynamicFilter', ['$filter', function($filter) {
  var filterSplit;
  return function(value, filterName) {
    // Allow for filtering to be conditionally skipped by passing in a null value
    if (arguments.length === 3 && arguments[2] === null && angular.isUndefined(value)) {
      return value;
    }
    // If have a filter to apply, find it
    if (filterName) {
      // Set default values, if none are passed in
      if (!value) {
        if (filterName === 'currency' || filterName === 'number') {
          value = 0;
        } else {
          value = '--';
        }
      }
      // Handle filter string with arguments
      if (filterName.indexOf(':') !== -1) {
        filterSplit = filterName.split(':');
        return $filter(filterSplit[0]).apply(null, [value, filterSplit[1]]);
      }
      return $filter(filterName)(value);
    }
    // Set unfiltered, null values to '--'
    if (!value) {
      value = '--';
    }
    return value;
  };
}]);