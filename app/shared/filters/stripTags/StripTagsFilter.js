'use strict';

var app = angular.module('frontendApp');

/**
 * Simple strip tags for text display of html
 */
app.filter('stripTags', [ function() {
  return function(text) {
    return String(text).replace(/<[^>]+>/gm, '');
  };
}]);