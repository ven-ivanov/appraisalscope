'use strict';

var app = angular.module('frontendApp');

/**
 * Replace a character with another
 *
 * Usage: {{type | replaceChar:'-':' '}} - This would replace with a hyphen (-) with a space ( )
 */
app.filter('replaceChar', [ function() {
  return function(text, arg1, arg2) {
    // Replace a given character with another
    if (angular.isString(text)) {
      return text.replace(new RegExp(arg1, 'g'), arg2);
    }
    return text;
  };
}]);