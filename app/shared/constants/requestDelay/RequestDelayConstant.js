'use strict';

/**
 * The amount of time to wait before making a request
 */
angular.module('frontendApp')
.constant('REQUEST_DELAY', {
  ms: 500
});