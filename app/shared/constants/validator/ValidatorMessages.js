'use strict';

/**
 * @ngdoc constant
 * @name frontendApp.ValidatorMessages
 * @description
 * # A common place to place messages for the validators
 * Service in the frontendApp.
 */
angular.module('frontendApp')
  .constant('VALIDATOR_MESSAGES', {
    required: 'REQUIRED',
    phone: 'The :field format is incorrect. The correct format is: XXX-XXX-XXXX',
    date: 'Incorrect date format. The correct format is: mm/dd/yyyy',
    yearMonthDate: 'Format for :field is mm/yyyy',
    amount: 'Invalid amount format',
    email: 'This is not a valid email',
    number: ':field must be a number',
    decimal: ':field must be a number that includes two decimal places (100.00)',
    routing: 'Incorrect routing. It should consist of 9 digits only',
    zip: 'Incorrect postal code. It should consist of 5 digits only',
    ssn: 'Incorrect Social Security Number format. The correct format is: XXX-XX-XXXX',
    taxClass: 'Incorrect Tax Classification format. The correct format is: C, D or P',
    taxId: 'Incorrect TAX ID format. The correct format is: XX-XXXXXXX',
    username: 'Invalid username. It must be between 2 and 30 characters long. Allowed characters: digits, letters, "@", ".", "-", "+", "_"',
    password: 'Invalid password. It must be between 6 and 255 characters long',
    compareTo: 'The :fields do not match',
    alpha: 'The :field must contain alphabetic characters only',
    city: 'The city name can contain the following characters only: letters, spaces, "-"',
    cityMax: 'The city must be at most 35 characters long',
    name: 'The :field can contain the following characters only, letters, spaces, "-", " \' "',
    nameMax: 'The :field must be at most 30 characters long',
    ccExpiration: 'Credit card expiration must be a valid expiration date in this format: 00/0000',
    ccNumber: 'The :field must be a valid credit card number, with only numbers (no spaces, dashes, etc)',
    custom: ':custom'
  });
