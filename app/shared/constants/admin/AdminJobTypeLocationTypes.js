'use strict';

/**
 * @ngdoc service
 * @name frontendApp.JOBTYPE_LOCATION_TYPES
 * @description
 * # JOBTYPE_LOCATION_TYPES
 * Constant in the frontendApp.
 */
angular.module('frontendApp')
  .constant('JOBTYPE_LOCATION_TYPES', {
    county: 1,
    zip: 2,
    state: 3
  });
