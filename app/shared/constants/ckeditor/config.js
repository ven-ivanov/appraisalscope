'use strict';

/**
 * Global settings for CKEditor
 *
 * Note: You must include these settings on the $scope for them to work properly.
 *
 * Controller:
 * $scope.editorOptions = CKEDITOR_CONFIG.config;
 * vm.content = '';
 *
 * HTML:
 * <textarea ckeditor="editorOptions" ng-model="ctrl.content"></textarea>
 */
angular.module('frontendApp')
.constant('CKEDITOR_CONFIG', {
  config: {
    toolbar_full: [ // jshint ignore:line
      {name: 'basicstyles', items: ['Bold', 'Italic']},
      {name: 'paragraph', items: ['BulletedList', 'NumberedList', 'Blockquote']},
      {name: 'insert', items: ['Image']},
      {name: 'clipboard', items: ['Undo', 'Redo']},
      {name: 'styles', items: ['Format', 'PasteFromWord', 'RemoveFormat']}
    ]
  }
});
