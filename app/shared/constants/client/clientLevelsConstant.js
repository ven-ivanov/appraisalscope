'use strict';

/**
 * @ngdoc service
 * @name frontendApp.CLIENT_LEVELS
 * @description
 * # CLIENT_LEVELS
 * Constant in the frontendApp.
 */
angular.module('frontendApp')
.constant('CLIENT_LEVELS', {
  processor: 1,
  manager: 2,
  loanOfficer: 3
});