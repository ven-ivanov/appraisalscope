'use strict';

/**
 * @ngdoc service
 * @name frontendApp.HANDBOOK_CATEGORIES
 * @description
 * # HANDBOOK_CATEGORIES
 * A list of available categories in the handbook
 */
angular.module('frontendApp')
  .constant('HANDBOOK_CATEGORIES', {
    states: 'states',
    primaryLicenses: 'primary-licenses',
    accountTypes: 'account-types',
    counties: 'counties',
    zipCodes: 'zip-codes'
  });
