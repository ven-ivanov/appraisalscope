'use strict';

/**
 * @ngdoc service
 * @name frontendApp.USER_STATUSES
 * @description
 * # USER_STATUSES
 * Constant in the frontendApp.
 */
angular.module('frontendApp')
  .constant('USER_STATUSES', {
    declinedInvitation: -3,
    invited: -2,
    pendingApproval: -1,
    unregistered: 0,
    registered: 1,
    unknown: null
  });
