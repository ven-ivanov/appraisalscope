'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AUTH_SCREEN_SIZES
 * @description
 * The size of the viewport at which absolutely positioned CSS classes should be added/removed from auth form
 */
angular.module('frontendApp')
.constant('AUTH_SCREEN_SIZES', {
  width: 992,
  heightSmall: 453,
  heightLarge: 570
});
