'use strict';

/**
 * @ngdoc service
 * @name frontendApp.USER_TYPES
 * @description
 * # USER_TYPES
 * Constant in the frontendApp.
 */
angular.module('frontendApp')
  .constant('USER_TYPES', {
    appraiser: 1,
    client: 2,
    admin: 3,
    staff: 4
  });
