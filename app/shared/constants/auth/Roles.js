'use strict';

/**
 * @ngdoc service
 * @name frontendApp.ROLES
 * @description
 * # ROLES
 * Constant in the frontendApp.
 */
angular.module('frontendApp')
  .constant('ROLES', {
    all: '*',
    admin: 'admin',
    client: 'client',
    appraiser: 'appraiser',
    guest: 'guest'
  });
