'use strict';

/**
 * @ngdoc service
 * @name frontendApp.APPRAISER_LEVELS
 * @description
 * # APPRAISER_LEVELS
 * Constant in the frontendApp.
 */
angular.module('frontendApp')
  .constant('APPRAISER_LEVELS', {
    justManager: 5,
    alsoManager: 6
  });
