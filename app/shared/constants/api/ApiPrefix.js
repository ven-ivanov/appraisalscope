'use strict';
/*global ENV:false */ // <- Avoid getting "'ENV' is not defined" warning by jshint.
/**
 * @ngdoc service
 * @name frontendApp.config
 * @description
 * # config
 * Constant in the frontendApp.
 */
angular.module('frontendApp')
  .constant('API_PREFIX', function (env, host) {
    var environment = env || ENV; // env (function argument) comes from unit test and ENV is the global, real environment variable.
    var full = host || window.location.host; // Same reason as above.
    var parts = full.split('.');
    var subdomain = parts[0];
    var domain = parts[parts.length === 3 ? 1 : 2];
    var extension = parts[parts.length === 3 ? 2 : 3];
    var port = '';

    // Remove port number if found
    if (/:\d{4}$/.test(extension)) {
      extension = extension.slice(0, -5);
    }

    // API local port number
    //if (extension === 'local') {
    //  port = ':8080';
    //}

    // Make sure the environment is explicitly set and append it to the URL.
    if (angular.isUndefined(environment)) {
      console.log('Undefined Environment!');
      return '';
    }

    switch(environment) {
      case 'production':
        environment = '';
        break;
      case 'dyson-ci':
        return '//54.152.199.181:3000';
      case 'mock':
        return '//localhost:3000';
      default:
        environment += '-';
    }

    return '//' + subdomain + '.' + environment + 'api.' + domain + '.' + extension + port;
  });
