'use strict';

describe('Service: config', function () {

  // load the service's module
  beforeEach(module('frontendApp'));

  // instantiate service
  var API_PREFIX;
  beforeEach(inject(function (_API_PREFIX_) {
    API_PREFIX = _API_PREFIX_;
  }));

  it('should return the correct API development prefix', function () {
    expect(API_PREFIX('dev', 'clientx.appraisalscope.local:9000')).toBe('//clientx.dev-api.appraisalscope.local');
  });

  it('should return the correct API staging prefix', function () {
    expect(API_PREFIX('staging', 'clientx.staging.appraisalscope.com')).toBe('//clientx.staging-api.appraisalscope.com');
  });

  it('should return the correct API production prefix', function () {
    expect(API_PREFIX('production', 'clientx.appraisalscope.com')).toBe('//clientx.api.appraisalscope.com');
  });

});
