'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AdminJobTypeService
 * @description
 * # AdminJobTypeService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AdminJobTypeService', [
    '$resource', 'API_PREFIX',

    function ($resource, API_PREFIX) {

      var jobType = API_PREFIX() + '/v2.0/admin/job-type/:jobTypeId';
      var jobTypeList = API_PREFIX() + '/v2.0/admin/job-type-list/:jobTypeListId';
      var jobTypesByList = API_PREFIX() + '/v2.0/admin/job-type-by-list/';
      var fees = API_PREFIX() + '/v2.0/admin/job-type-fees/:type';

      // jobType
      this.jobType = $resource(jobType, {
        id: '@id'
      }, {
        getAll: {method: 'GET', isArray: true},
        store : {method: 'POST', isArray: false},
        update: {method: 'PUT'},
        destroy: {method: 'DELETE'}
      });

      // jobTypeList
      this.jobTypeList = $resource(jobTypeList, {
        id: '@id'
      }, {
        getAll: {method: 'GET', isArray: true},
        getJobTypes: {method: 'GET', isArray: true, url: jobTypesByList},
        store: {method: 'POST', isArray: true},
        update: {method: 'PUT'},
        destroy: {method: 'DELETE'}
      });

      //fees
      this.fees = $resource(fees, {
        state: '@state',
        jobTypeListId: '@jobTypeListId',
        jobTypeId: '@jobTypeId'
      }, {
        getAllStates: {method: 'GET', isArray: true, params: { type: 'state' } },
        getAllZipCodes: {method: 'GET', isArray: true, params: { type: 'zip' } },
        getAllCounties: {method: 'GET', isArray: true, params: { type: 'county' } },

        updateZipCode: {method: 'PUT'},
        updateCounty: {method: 'PUT'},
        updateState: {method: 'PUT'}
      });

      //directory jobType
      this.dirJobType = $resource(jobType, {}, {
        getAll: {method: 'GET', isArray: true}
      });

      return this;
    }
  ]);
