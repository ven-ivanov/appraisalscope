'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminJobTypeCtrl
 * @description
 * # AdminJobTypeCtrl
 * Controller of the frontendApp
 */

angular.module('frontendApp').controller('AdminJobTypeCtrl', [
  '$scope', 'AdminJobTypeService', '$q',

  function ($scope, AdminJobTypeService, $q) {

  	var vm = this;

    vm.dataloaded = false;
    vm.editJobType = {};
    vm.newJobType = {};

    vm.mapModal = false;

    //get jobTypes from main app and directory app
    var jobTypes = AdminJobTypeService.jobType.getAll();
    var dirJobTypes = AdminJobTypeService.dirJobType.getAll();

    $q.all([
      jobTypes.$promise,
      dirJobTypes.$promise
    ]).then(function () {
      vm.jobTypes = {};
      angular.forEach(jobTypes, function (jobType) {
        vm.jobTypes[jobType.id] = jobType;
      });

      vm.dirJobTypes = dirJobTypes;

      vm.dataloaded = true;
    });

    /**
     * Create new jobType
     * @return {Object} newJobType
     */
    vm.addNewJobType = function () {
      //should return newly created jobType
      var newJobType = AdminJobTypeService.jobType.store(vm.newJobType);

      $q.all([
        newJobType.$promise
      ]).then(function  () {
        if(newJobType) {
          vm.jobTypes[newJobType.id] = newJobType;
          vm.newJobTypeModal = false;
        }
      });
    };

    /**
     * Deletes specified jobType
     * @param {Id} jobTypeId
     * @return {Resource} response
     */
    vm.deleteJobType = function (jobTypeId) {
      delete vm.jobTypes[jobTypeId];
      return AdminJobTypeService.jobType.destroy({jobTypeId: jobTypeId});
    };

    /**
     * Open specified jobType addons modal
     * @param {Id} jobTypeId
     */
    vm.openAddonModal = function (jobTypeId) {
      vm.addonModal = true;
      vm.addonJobType = vm.jobTypes[jobTypeId];
    };

    /**
     * Add/delete addons for specified jobType
     * @param {Id} jobTypeId
     * @param {Boolean} checked
     */
    vm.selectAddon = function (jobTypeId, checked) {
      if(checked) {
        vm.addonJobType.addons.push(jobTypeId);
      } else {
        var index = vm.addonJobType.addons.indexOf(jobTypeId);
        delete vm.addonJobType.addons[index];
      }
    };

    /**
     * Open jobType mapping modal
     */
    vm.openMapModal = function () {
      vm.mapModal = true;
    };

    /**
     * Update specified jobType
     * @param {Id} jobTypeId
     * @return {Resource} response
     */
    vm.updateJobType = function (jobTypeId) {
      vm.editJobType[jobTypeId] = false;
      return AdminJobTypeService.jobType.update({jobTypeId: jobTypeId}, vm.jobTypes[jobTypeId]);
    };
  }
]);