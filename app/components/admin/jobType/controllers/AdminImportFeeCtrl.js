'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminImportFeeCtrl
 * @description
 * # AdminImportFeeCtrl
 * Controller of the frontendApp
 */

angular.module('frontendApp').controller('AdminImportFeeCtrl', [
  'JOBTYPE_LOCATION_TYPES',

  function (JOBTYPE_LOCATION_TYPES) {

    var vm = this;

    vm.locationTypes = JOBTYPE_LOCATION_TYPES;

    vm.importTypes = [
      { id: JOBTYPE_LOCATION_TYPES.county, title: 'Import County Fee' },
      { id: JOBTYPE_LOCATION_TYPES.zip, title: 'Import ZIP Fee' },
      { id: JOBTYPE_LOCATION_TYPES.state, title: 'Import State Fee' }
    ];

  }
]);