'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminSetFeesByLocation
 * @description
 * # AdminSetFeesByLocation
 * Controller of the frontendApp
 */
angular.module('frontendApp').controller('AdminSetFeesByLocation', [
  '$scope', 'AdminJobTypeService', 'JOBTYPE_LOCATION_TYPES', '$q', '$stateParams',

  function ($scope, AdminJobTypeService, JOBTYPE_LOCATION_TYPES, $q, $stateParams) {

  	var vm = this;

    vm.editZipCodeFee = {};
    vm.editCountyFee = {};

    //set jobType list and jobType from state params
    var selectedJobTypeList = $stateParams.jobTypeListId;
    var selectedJobType = $stateParams.jobTypeId;

    var jobTypeLists = AdminJobTypeService.jobTypeList.getAll();
    var jobTypes = AdminJobTypeService.jobTypeList.getJobTypes({jobTypeListId: selectedJobTypeList});

    $q.all([
      jobTypeLists.$promise,
      jobTypes.$promise
    ]).then(function () {
    	
    	//get jobType lists
      vm.jobTypeLists = {};
      angular.forEach(jobTypeLists, function (list) {
        vm.jobTypeLists[list.id] = list;
      });

      //save jobTypes
      vm.jobTypes = {};
      angular.forEach(jobTypes, function (jobType) {
        vm.jobTypes[jobType.id] = jobType;
      });

      vm.selectedJobTypeList = selectedJobTypeList;
      vm.selectedJobType = selectedJobType;

      //get state fees
      var stateFees = AdminJobTypeService.fees.getAllStates({jobTypeId: selectedJobType, JobTypeListId: selectedJobTypeList});

      $q.all([
        stateFees.$promise
      ]).then(function () {
        vm.stateFees = stateFees;
      });
    });

    /**
     * Select jobType list and get related jobtypes
     * @param {Id} jobTypeId
     */
    vm.selectList = function (listId) {
      jobTypes = AdminJobTypeService.jobTypeList.getJobTypes({jobTypeListId: listId});

      $q.all([
        jobTypes.$promise
      ]).then(function () {
        vm.jobTypes = jobTypes;
      });
    };

    /**
     * Select jobType and get related fees by states
     * @param {Id} jobTypeId
     * @param {Id} listId
     */
    vm.selectJobType = function (jobTypeId, listId) {
      var stateFees = AdminJobTypeService.fees.getAllStates({jobTypeId: jobTypeId, JobTypeListId: listId});

      $q.all([
        stateFees.$promise
      ]).then(function () {
        vm.stateFees = stateFees;
      });
    };

    /**
     * Get county fees for specified state
     * @param {Id} jobTypeId
     * @param {Id} listId
     * @param {String} state
     */
    vm.setByCounty = function (jobTypeId, listId, state) {
      var countyFees = AdminJobTypeService.fees.getAllCounties({jobTypeId: jobTypeId, JobTypeListId: listId, state: state});

      $q.all([
        countyFees.$promise
      ]).then(function () {
        vm.countyFees = countyFees;
        vm.setByCountyModal = true;
      });
    };

    /**
     * Get zipCode fees for specified state
     * @param {Id} jobTypeId
     * @param {Id} listId
     * @param {String} state
     * @return {Resource} response
     */
    vm.setByZipCode = function (jobTypeId, listId, state) {
      var zipCodeFees = AdminJobTypeService.fees.getAllZipCodes({jobTypeId: jobTypeId, JobTypeListId: listId, state: state});

      $q.all([
        zipCodeFees.$promise
      ]).then(function () {
        vm.zipCodeFees = zipCodeFees;
        vm.setByZipCodeModal = true;
      });
    };

    /**
     * Open state fee edition modal
     * @param {Object} state
     */
    vm.editStateFee = function (state) {
      vm.editStateFeeModal = true;
      vm.selectedState = state;
    };

    /**
     * Update specified state fee
     * @param {Object} state
     * @return {Resource} response
     */
    vm.updateStateFee = function (state) {
      vm.editStateFeeModal = false;
      return AdminJobTypeService.fees.updateState({id: state.abbr}, state);
    };

    /**
     * Update specified zipCode fee
     * @param {Object} zipCode
     * @return {Resource} response
     */
    vm.updateZipCodeFee = function (zipCode) {
      vm.editZipCodeFee[zipCode.id] = false;
      return AdminJobTypeService.fees.updateZipCode({id: zipCode.id}, zipCode);
    };

    /**
     * Update specified county fee
     * @param {Object} county
     * @return {Resource} response
     */
    vm.updateCountyFee = function (county) {
      vm.editCountyFee[county.id] = false;
      return AdminJobTypeService.fees.updateCounty({id: county.id}, county);
    };
  }
]);