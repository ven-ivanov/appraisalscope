'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminJobTypeListCtrl
 * @description
 * # AdminJobTypeListCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp').controller('AdminJobTypeListCtrl', [
  '$scope', 'AdminJobTypeService', 'JOBTYPE_LOCATION_TYPES', '$q', 'HandbookService', 'HANDBOOK_CATEGORIES',

  function ($scope, AdminJobTypeService, JOBTYPE_LOCATION_TYPES, $q, HandbookService, HANDBOOK_CATEGORIES) {

  	var vm = this;

    var jobTypeLists = AdminJobTypeService.jobTypeList.getAll();
    var states = HandbookService.query({category: HANDBOOK_CATEGORIES.states});
    vm.editJobType = {};

    $q.all([
      jobTypeLists.$promise,
      states.$promise
    ]).then(function () {
      vm.jobTypeLists = {};

      angular.forEach(jobTypeLists, function (jobTypeList) {
        vm.jobTypeLists[jobTypeList.id] = jobTypeList;
      });

      vm.locations = [
        { id: JOBTYPE_LOCATION_TYPES.county, title: 'County' },
        { id: JOBTYPE_LOCATION_TYPES.zip, title: 'ZIP' },
        { id: JOBTYPE_LOCATION_TYPES.state, title: 'State' }
      ];

      vm.states = states.data;
    });

    /**
     * Select jobType list and get related jobtypes
     */
    vm.selectList = function () {
      if(vm.selectedJobTypeList) {
        var jobTypes = AdminJobTypeService.jobTypeList.getJobTypes({jobTypeListId: vm.selectedJobTypeList});

        $q.all([
          jobTypes.$promise
        ]).then(function () {
          vm.jobTypes = {};

          angular.forEach(jobTypes, function (jobType) {
            vm.jobTypes[jobType.id] = jobType;
          });
        });
      }
    };

    /**
     * Get jobTypes and open modal window to add jobTypes to list
     * @param {Id} listId
     */
    vm.addJobTypeToList = function (listId) {
      if(listId) {
        var notUsedJobTypes = AdminJobTypeService.jobType.getAll();

        $q.all([
          notUsedJobTypes.$promise
        ]).then(function () {
          vm.jobTypeToList = {};

          //make list of all jobTypes already on the list
          var usedJobTypes = Object.keys(vm.jobTypes);
          vm.notUsedJobTypes = {};

          //make list of all jobTypes which are not on the list
          angular.forEach(notUsedJobTypes, function (jobType) {
            if(usedJobTypes.indexOf('' + jobType.id) === -1) {
              vm.notUsedJobTypes[jobType.id] = jobType;
            }
          });

          //open modal
          vm.addToListModal = true;
        });
      }
    };

    /**
     * Add specified jobType to list
     * @param {Id} jobTypeId
     * @return {Resource} response
     */
    vm.addToList = function (jobTypeId, checked) {
      if(jobTypeId) {

        if(checked) {
          vm.jobTypes[jobTypeId] = vm.notUsedJobTypes[jobTypeId];
        } else {
          delete vm.jobTypes[jobTypeId];
        }

        return AdminJobTypeService.jobType.update({jobTypeId: jobTypeId}, vm.jobTypes[jobTypeId]);
      }
    };

    /**
     * Add all jobTypes to list
     * @return {Resource} response
     */
    vm.addAllToList = function (checked) {

      angular.forEach(vm.notUsedJobTypes, function (jobType) {

        if(checked) {
          vm.jobTypes[jobType.id] = vm.notUsedJobTypes[jobType.id];
        } else {
          delete vm.jobTypes[jobType.id];
        }

        vm.jobTypeToList[jobType.id] = checked;
      });

      return AdminJobTypeService.jobType.update(vm.jobTypes);
    };

    /**
     * Show all jobTypes realted to specified jobType list
     * @param {Id} listId
     */
    vm.showRelatedJobTypes = function (listId) {
      var relatedJobTypes = AdminJobTypeService.jobTypeList.getJobTypes({jobTypeListId: listId});

      $q.all([
        relatedJobTypes.$promise
      ]).then(function () {
        vm.relatedJobTypes = relatedJobTypes;
        vm.relatedJobTypesModal = true;
      });
    };

    /**
     * Create ne jobType list
     * @return {Object} newJobTypeList
     */
    vm.addNewJobTypeList = function () {
      //should return newly created jobTypeList
      var newJobTypeList = AdminJobTypeService.jobTypeList.store(vm.newJobTypeList);

      if(newJobTypeList) {
        vm.jobTypeLists[newJobTypeList.id] = newJobTypeList;
      }
      return newJobTypeList;
    };

    /**
     * Update sepcified jobType
     * @param {Id} jobTypeId
     * @return {Resource} response
     */
    vm.updateJobType = function (jobTypeId) {
      vm.editJobType[jobTypeId] = false;
      return AdminJobTypeService.jobType.update({jobTypeId: jobTypeId}, vm.jobTypes[jobTypeId]);
    };

    /**
     * Open jobType list edition modal for specified list
     * @param {Id} listId
     */
    vm.editJobTypeList = function (listId) {
      if(listId) {
        vm.editJobTypeListModal = true;
      }
    };

    /**
     * Open jobType list edition modal for specified list
     * @param {Id} listId
     * @return {Resource} response
     */
    vm.updateJobTypeList = function (listId) {
      vm.editJobTypeListModal = false;
      return AdminJobTypeService.jobTypeList.update({jobTypeListId: listId}, vm.jobTypeLists[listId]);
    };

    /**
     * Delete specified jobType
     * @param {Id} jobTypeId
     * @return {Resource} response
     */
    vm.deleteJobType = function (jobTypeId) {
      delete vm.jobTypes[jobTypeId];
      return AdminJobTypeService.jobType.destroy({jobTypeId: jobTypeId});
    };

    /**
     * Delete specified jobType list
     * @param {Id} listId
     * @return {Resource} response
     */
    vm.deleteJobTypeList = function (listId) {
      if(listId) {
        vm.selectedJobTypeList = null;
        delete vm.jobTypeLists[listId];
        return AdminJobTypeService.jobTypeList.destroy({jobTypeListId: listId});
      }
    };

    /**
     * Open jobType mass update modal
     * @param {Id} listId
     */
    vm.openMassUpdateModal = function (listId) {

      if(listId) {
        vm.massUpdate = {
          jobTypes: {},
          states: {}
        };

        vm.massUpdateModal = true;
      }
    };

    /*
     * Check/uncheck all states in mass update window
     * @param {Boolean} checked
     */
    vm.selectAllStates = function (checked) {

      angular.forEach(vm.states, function (state, key) {
        vm.massUpdate.states[key] = checked;
      });
    };

    /*
     * Check/uncheck all jobTyeps in mass update window
     * @param {Boolean} checked
     */
    vm.selectAllJobTypes = function (checked) {

      angular.forEach(vm.jobTypes, function (jobType, key) {
        vm.massUpdate.jobTypes[key] = checked;
      });
    };

    vm.massUpdateJobType = function (data) {

      angular.forEach(vm.jobTypes, function (jobType) {
        if(data.jobTypes[jobType.id]) {
          jobType.clientFee = data.jobTypes[jobType.id].clientFee;
          jobType.managmentFee = data.jobTypes[jobType.id].managmentFee;
          jobType.appraiserFee = data.jobTypes[jobType.id].appraiserFee;
          jobType.isManagmentFee = data.jobTypes[jobType.id].isManagmentFee;
          jobType.isAppraiserFee = data.jobTypes[jobType.id].isAppraiserFee;
        }
      });

      return AdminJobTypeService.jobTypeList.update(data);
    };
  }
]);