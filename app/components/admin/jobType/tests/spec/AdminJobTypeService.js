'use strict';

describe('Service: AdminJobTypeService', function () {

  // load the service's module
  beforeEach(module('frontendApp'));

  // instantiate service
  var AdminJobTypeService;
  beforeEach(inject(function (_AdminJobTypeService_) {
    AdminJobTypeService = _AdminJobTypeService_;
  }));

  it('should do something', function () {
    expect(!!AdminJobTypeService).toBe(true);
  });

});
