'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersClientCompanySettings',
['AdminUsersDirectiveInheritanceService', function (AdminUsersDirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/client/directives/company-settings/directives/partials/company-settings.html',
    controller: 'AdminUsersClientCompanySettingsCtrl',
    controllerAs: 'companySettingsCtrl',
    link: function (scope) {
      AdminUsersDirectiveInheritanceService.inherit.call(scope);
    }
  };
}]);