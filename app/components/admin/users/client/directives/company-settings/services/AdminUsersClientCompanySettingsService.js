'use strict';

/********************************************************
 * @todo I CAN'T DO MUCH WITH THIS UNTIL I GET AN ENDPOINT UPDATE
 * @LINK https://github.com/ascope/manuals/issues/53
 * @todo Still waiting 8/7
 ********************************************************/
var app = angular.module('frontendApp');

app.factory('AdminUsersClientCompanySettingsService',
['API_PREFIX', '$http', 'AdminUsersResourceService', 'AdminUsersService',
function (API_PREFIX, $http, AdminUsersResourceService, AdminUsersService) {
  var Service = {
    // Safe data
    safeData: {
      // General settings
      general: {},
      // Third party integrations
      integrations: {},
      // Payment options
      paymentOptions: {},
      // Merged document templates
      mergedDocumentTemplates: {},
      // Set filename
      setFilename: {},
      // Job types
      feeMargin: {},
      // Other options
      other: {}
    },
    // Data for display
    displayData: {
      general: [],
      integrations: [],
      paymentOptions: [],
      mergedDocumentTemplates: [],
      setFilename: [],
      feeMargin: [],
      other: []
    },
    // Credit card options
    ccOptions: {
      options: []
    },
    // Fee margin by state
    feeMarginByState: {},
    /**
     * Company settings requests
     * @param requestFn
     * @param bodyParams
     */
    request: function (requestFn, bodyParams) {
      // I don't really need type here. I just have it so that when I make a reusable method, it's all the same
      return AdminUsersResourceService.companySettings[requestFn](AdminUsersService.record._type, bodyParams);
    },
    /**
     * Retrieve company settings on load
     */
    getSettings: function () {
      //Service.request('getSettings').then(function (response) {
      //  console.log(response);
      //});
      return $http.get(API_PREFIX() + '/v2.0/users/client/' + AdminUsersService.id + '/settings').
      then(function (response) {
        // General settings
        Service.hashData(response.data);
        // Retrieve display data
        Service.setDisplayData();
        // Retrieve third party integrations
        return $http.get(API_PREFIX() + '/v2.0/users/client/' + AdminUsersService.id + '/settings/integrations');
      })
      .then(function (response) {
        // Third party integrations
        Service.hashData(response.data, 'integrations');
        Service.setDisplayData('integrations');
        // Get merged document templates
        return $http.get(API_PREFIX() + '/v2.0/users/client/' + AdminUsersService.id + '/settings/merged-document-templates');
      })
      .then(function (response) {
        // Merged document templates
        Service.hashData(response.data, 'mergedDocumentTemplates');
        Service.setDisplayData('mergedDocumentTemplates');
        // Get payment options
        return $http.get(API_PREFIX() + '/v2.0/users/client/' + AdminUsersService.id + '/settings/payment-options');
      })
      .then(function (response) {
        // Payment options
        Service.hashData(response.data, 'paymentOptions');
        Service.setDisplayData('paymentOptions');
        // Get cc options
        Service.getCcOptions();
        // Get set filename when emailed options
        return $http.get(API_PREFIX() + '/v2.0/users/client/' + AdminUsersService.id + '/settings/set-filename');
      })
      .then(function (response) {
        // Filename options
        Service.hashData(response.data, 'setFilename');
        Service.setDisplayData('setFilename');
        // Get job types
        return $http.get(API_PREFIX() + '/v2.0/users/client/' + AdminUsersService.id + '/settings/job-types');
      })
      .then(function (response) {
        Service.hashData(response.data, 'feeMargin');
        Service.setDisplayData('feeMargin');
        // Get other options
        return $http.get(API_PREFIX() + '/v2.0/users/client/' + AdminUsersService.id + '/settings/other');
      })
      .then(function (response) {
        Service.hashData(response.data, 'other', 'option');
      });
    },
    /**
     * Hash data and store it
     */
    hashData: function (data, type, hashId) {
      type = type || 'general';
      hashId = hashId || 'id';
      // Hash settings settings
      angular.forEach(data, function (item) {
        Service.safeData[type][item[hashId]] = item;
        // Credit card options
        /**
         * @todo
         * I believe this is a problem with Dyson. it's failing to return the proper cc options ever.
         */
        if (type === 'paymentOptions' && item.id === 'cc') {
          angular.forEach(item.options, function (ccOption) {
            Service.safeData.paymentOptions.cc.options[ccOption.id] = ccOption;
          });
        }
      });
    },
    /**
     * Retrieve general settings
     */
    setDisplayData: function (type) {
      // Default to general
      type = type || 'general';
      // Generate display data
      angular.forEach(Service.safeData[type], function (data) {
        Service.displayData[type].push(data);
      });
    },
    /**
     * Get credit card options
     */
    getCcOptions: function () {
      // Store selected option
      Service.ccOptions.selectedOption =
      Service.safeData.paymentOptions.cc.enabled;
      // Store cc options
      angular.forEach(Service.safeData.paymentOptions.cc.options, function (option) {
        Service.ccOptions.options.push(option);
      });
    },
    /**
     * Enable setting
     */
    enableSetting: function (type, setting) {
      // Special case for capture on client
      if (type === 'captureOnClient') {
        // Update display
        Service.ccOptions.captureOnClient =
        !Service.ccOptions.captureOnClient;
        // Update safe data
        /**
         * @todo
         */
        angular.forEach(Service.safeData.paymentOptions.cc, function (options, key) {
          // For cc options
          if (key === 'options') {

          }
        });
      } else {
        setting.enabled = !setting.enabled;
        // Update whether enabled
        Service.safeData[type][setting.id].enabled = setting.enabled;
      }
      // Update enable/disable setting display
      Service.enableSettingDisplay(type, setting);
      // Send request
      return $http.patch(API_PREFIX() + '/v2.0/users/client/clientId/enable-setting', {setting: setting.id, enable: setting.enabled});
    },
    /**
     * Switch display for settings which can just be enabled/disabled
     */
    enableSettingDisplay: function (type, setting) {
      // Keep display data in sync with safe data
      angular.forEach(Service.displayData[type], function (thisSetting, key) {
        if (thisSetting.id === setting.id) {
          Service.displayData[type][key].enabled = setting.enabled;
        }
      });
    },
    /**
     * Create table data from fee margin data
     */
    generateFeeMarginTableData: function () {
      var vm = this;
      // Iterate safe data and create inputs and dropdown values
      angular.forEach(Service.safeData.feeMargin, function (feeMarginData) {
        // Dropdown
        vm.multiSelect[feeMarginData.id] = feeMarginData.percentageFlat ? 'percentage' : 'flat';
        // Inputs
        vm.tableInputs[feeMarginData.id] = feeMarginData.feeMargin;
      });
    },
    /**
     * Generate data for fee margin by state table
     */
    feeMarginByStateTableData: function (jobTypeId) {
      var vm = this;
      vm.tableInputs = {};
      // Retrieve state data
      return $http.get(API_PREFIX() + '/v2.0/users/client/' + AdminUsersService.id + '/settings/fee-margin-by-state/' + jobTypeId)
      .then(function (response) {
        // Make a simple hash
        angular.forEach(response.data, function (stateData) {
          Service.feeMarginByState[stateData.id] = stateData;
          // Set input data
          vm.tableInputs[stateData.id] = Number(stateData.feeMargin);
        });
        // Set display data
        vm.tableData = response.data;
        vm.rowData = vm.tableData.slice();
      });
    },
    /**
     * Update fee margin by state
     */
    updateFeeMarginByState: function (jobTypeId, values) {
      return $http.patch(API_PREFIX() + '/v2.0/users/client/' + AdminUsersService.id + '/settings/fee-margin-by-state/' + jobTypeId, values);
    },
    /**
     * Update fee margin by job type
     */
    updateFeeMarginByJobType: function (type, values) {
      return $http.patch(API_PREFIX() + '/v2.0/users/client/' + AdminUsersService.id + '/settings/fee-margin-by-job-type/', {type: type, values: values});
    },
    /**
     * Store updated filename on backend
     */
    storeUpdatedFilename: function (filename) {
      return $http.patch(API_PREFIX() + '/v2.0/users/client/' + AdminUsersService.id + '/emailed-document-filename', {filename: filename});
    },
    /**
     * Update misc options
     */
    updateOtherOptions: function (values) {
      return $http.patch(API_PREFIX() + '/v2.0/users/client/' + AdminUsersService.id + '/other-options', values);
    }
  };

  return Service;
}]);