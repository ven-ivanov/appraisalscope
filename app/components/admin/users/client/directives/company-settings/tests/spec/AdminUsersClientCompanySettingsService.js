'use strict';

describe('AdminUsersClientCompanySettingsService', function () {
  var adminUsersClientService, adminUsersClientCompanySettingsService, scope, httpBackend, callerObj = {};
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersClientService', AdminUsersClientServiceMock);
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  /**
   * Options
   * @type {{id: string, name: string, enabled: number}[]}
   */
  var general = [
    {"id": "paymentForm", "name": "Payment form", "enabled": 0},
    {"id": "feeDeduction", "name": "Fee deduction", "enabled": 0}];
  var integrations = [{"id":"blitzdoc","name":"Blitzdoc sync","enabled":1},{"id":"uwm","name":"UWM sync","enabled":1}];
  var mergedDocumentTemplates = [{"id":"invoice","name":"Invoice","enabled":1},{"id":"testDocFormat","name":"Test doc format","enabled":0}];
  var paymentOptions = [{"id":"bankAccount","name":"Bank account","enabled":1},{"id":"sendPaymentRequest","name":"Send payment request to customer","enabled":0}];
  var setFilename = [{"id":"lastName","name":"Borrower last name"},{"id":"firstName","name":"Borrower first name"}];
  var feeMargin = [{"id":1,"name":"Job type 1","percentageFlat":1,"feeMargin":58.32},{"id":2,"name":"Job type 2","percentageFlat":0,"feeMargin":92.1}];
  var other = [{"option":"mergeDocuments","value":1},{"option":"mergedDocPlacement","value":1}];
  /**
   * Hashed data
   */
  var safeData = {
    general: {
      paymentForm: {id: 'paymentForm', name: 'Payment form', enabled: 0},
      feeDeduction: {id: 'feeDeduction', name: 'Fee deduction', enabled: 0}
    },
    integrations: {
      blitzdoc: {id: 'blitzdoc', name: 'Blitzdoc sync', enabled: 1},
      uwm: {id: 'uwm', name: 'UWM sync', enabled: 1}
    },
    mergedDocumentTemplates: {
      invoice: {id: 'invoice', name: 'Invoice', enabled: 1},
      testDocFormat: {id: 'testDocFormat', name: 'Test doc format', enabled: 0}
    },
    paymentOptions: {
      bankAccount: {id: 'bankAccount', name: 'Bank account', enabled: 1},
      sendPaymentRequest: {id: 'sendPaymentRequest', name: 'Send payment request to customer', enabled: 0}
    },
    setFilename: {
      lastName: {id: 'lastName', name: 'Borrower last name'},
      firstName: {id: 'firstName', name: 'Borrower first name'}
    },
    feeMargin: {
      1: {id: 1, name: 'Job type 1', percentageFlat: 1, feeMargin: 58.32},
      2: {id: 2, name: 'Job type 2', percentageFlat: 0, feeMargin: 92.1}
    },
    other: {
      mergeDocuments: {option: 'mergeDocuments', value: 1},
      mergedDocPlacement: {option: 'mergedDocPlacement', value: 1}
    }
  };

  beforeEach(inject(function (AdminUsersClientService, AdminUsersClientCompanySettingsService, $rootScope, $httpBackend) {
    adminUsersClientService = AdminUsersClientService;
    adminUsersClientCompanySettingsService = AdminUsersClientCompanySettingsService;
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    // Other settings
    httpBackend.whenGET(/settings\/other/).respond(other);
    // Job types
    httpBackend.whenGET(/settings\/job-types/).respond(feeMargin);
    // Set filename
    httpBackend.whenGET(/settings\/set-filename/).respond(setFilename);
    // Payment options
    httpBackend.whenGET(/settings\/payment-options/).respond(paymentOptions);
    // Merged doc templates
    httpBackend.whenGET(/settings\/merged-document-templates/).respond(mergedDocumentTemplates);
    // Integrations
    httpBackend.whenGET(/settings\/integrations/).respond(integrations);
    // General
    httpBackend.whenGET(/settings$/).respond(general);
    // Fee margin by state
    httpBackend.whenGET(/fee-margin-by-state/).respond([{id: 1, state: "Alabama", feeMargin: '57.1'},
                                                        {id: 2, state: "Alaska", feeMargin: '81'}]);
  }));

  describe('getSettings', function () {
    beforeEach(function () {
      spyOn(adminUsersClientCompanySettingsService, 'hashData');
      spyOn(adminUsersClientCompanySettingsService, 'setDisplayData');
      spyOn(adminUsersClientCompanySettingsService, 'getCcOptions');
      // Make the requests
      adminUsersClientCompanySettingsService.getSettings();
      httpBackend.flush();
      scope.$digest();
    });

    it('should have hashed data for each setting type', function () {
      expect(adminUsersClientCompanySettingsService.hashData.calls.count()).toEqual(7);
    });
    it('should have created display data for each setting type', function () {
      expect(adminUsersClientCompanySettingsService.setDisplayData.calls.count()).toEqual(6);
    });
    it('should have created credit card display data', function () {
      expect(adminUsersClientCompanySettingsService.getCcOptions).toHaveBeenCalled();
    });
  });

  describe('hashData', function () {
    it('should hash general settings', function () {
      adminUsersClientCompanySettingsService.hashData(general, 'general');
      expect(adminUsersClientCompanySettingsService.safeData.general).toEqual(safeData.general);
    });
    it('should hash integrations', function () {
      adminUsersClientCompanySettingsService.hashData(integrations, 'integrations');
      expect(adminUsersClientCompanySettingsService.safeData.integrations).toEqual(safeData.integrations);
    });
    it('should hash mergedDocumentTemplates', function () {
      adminUsersClientCompanySettingsService.hashData(mergedDocumentTemplates, 'mergedDocumentTemplates');
      expect(adminUsersClientCompanySettingsService.safeData.mergedDocumentTemplates).toEqual(safeData.mergedDocumentTemplates);
    });
    it('should hash paymentOptions', function () {
      adminUsersClientCompanySettingsService.hashData(paymentOptions, 'paymentOptions');
      expect(adminUsersClientCompanySettingsService.safeData.paymentOptions).toEqual(safeData.paymentOptions);
    });
    it('should hash filename', function () {
      adminUsersClientCompanySettingsService.hashData(setFilename, 'setFilename');
      expect(adminUsersClientCompanySettingsService.safeData.setFilename).toEqual(safeData.setFilename);
    });
    it('should hash job types', function () {
      adminUsersClientCompanySettingsService.hashData(feeMargin, 'feeMargin');
      expect(adminUsersClientCompanySettingsService.safeData.feeMargin).toEqual(safeData.feeMargin);
    });
    it('should hash other settings', function () {
      adminUsersClientCompanySettingsService.hashData(other, 'other', 'option');
      expect(adminUsersClientCompanySettingsService.safeData.other).toEqual(safeData.other);
    });
  });

  describe('with data', function () {
    beforeEach(function () {
      // @todo CC options need revision
      spyOn(adminUsersClientCompanySettingsService, 'getCcOptions');
      adminUsersClientCompanySettingsService.getSettings();
      httpBackend.flush();
      scope.$digest();
    });

    describe('setDisplayData', function () {
      it('should set display data correctly', function () {
        expect(adminUsersClientCompanySettingsService.displayData.general).toEqual([
          {
            id: 'paymentForm', name: 'Payment form', enabled: 0
          },
          {
            id: 'feeDeduction', name: 'Fee deduction', enabled: 0
          }
        ]);
      });
    });

    // @todo
    describe('getCcOptions', function () {

    });

    describe('enableSetting', function () {
      it('should toggle on', function () {
        adminUsersClientCompanySettingsService.enableSetting('general', general[0]);
        expect(adminUsersClientCompanySettingsService.safeData.general.paymentForm).toEqual({ id: 'paymentForm', name: 'Payment form', enabled: true });
      });
      it('should toggle off', function () {
        general[0].enabled = true;
        adminUsersClientCompanySettingsService.enableSetting('general', general[0]);
        expect(adminUsersClientCompanySettingsService.safeData.general.paymentForm).toEqual({ id: 'paymentForm', name: 'Payment form', enabled: false });
      });
    });

    describe('enableSettingDisplay', function () {
      it('should toggle display data', function () {
        general[0].enabled = false;
        adminUsersClientCompanySettingsService.enableSettingDisplay('general', general[0]);
        expect(adminUsersClientCompanySettingsService.displayData.general).toEqual([
          {
            id: 'paymentForm', name: 'Payment form', enabled: false
          }, {
            id: 'feeDeduction', name: 'Fee deduction', enabled: 0
          }]);
      });
    });

    describe('generateFeeMarginTableData', function () {
      it('should general multiselect and input objects for table', function () {
        callerObj.multiSelect = {};
        callerObj.tableInputs = {};
        adminUsersClientCompanySettingsService.generateFeeMarginTableData.call(callerObj);
        expect(callerObj).toEqual({multiSelect: {1: 'percentage', 2: 'flat'}, tableInputs: {1: 58.32, 2: 92.1}});
      });
    });

    describe('feeMarginByStateTableData', function () {
      it('should generate fee margin table data', function () {
        adminUsersClientCompanySettingsService.feeMarginByStateTableData.call(callerObj, 1);
        httpBackend.flush();
        scope.$digest();
        expect(callerObj).toEqual({
          multiSelect: {1: 'percentage', 2: 'flat'},
          tableInputs: {1: 57.1, 2: 81},
          tableData: [{id: 1, state: 'Alabama', feeMargin: '57.1'}, {id: 2, state: 'Alaska', feeMargin: '81'}],
          rowData: [{id: 1, state: 'Alabama', feeMargin: '57.1'}, {id: 2, state: 'Alaska', feeMargin: '81'}]
        });
      });
    });
  });
});