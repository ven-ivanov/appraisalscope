'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersClientCompanySettingsCtrl',
['$scope', 'AdminUsersClientCompanySettingsService', 'REQUEST_DELAY', '$timeout', function ($scope, AdminUsersClientCompanySettingsService, REQUEST_DELAY, $timeout) {
  var vm = this;
  // Display data
  vm.displayData = AdminUsersClientCompanySettingsService.displayData;
  // Safe data
  vm.safeData = AdminUsersClientCompanySettingsService.safeData;
  // CC options
  vm.ccOptions = AdminUsersClientCompanySettingsService.ccOptions;
  /**
   * Other settings
   */
  vm.other = {
    mergeDocuments: [{id: 1, name: 'At time of completion'}, {id: 2, name: 'At time of upload'}],
    mergedDocPlacement: [{id: 1, name: 'At beginning of report'}, {id: 2, name: 'At end of report'}],
    setPlZero: [{id: 1, name: 'Flat fee'}, {id: 2, name: 'Percentage'}],
    lockEmailOffice: [{id: 1, name: 'Processor'}, {id: 2, name: 'Loan Office'}, {id: 3, name: 'Manager'}],
    sendDocumentAs: [{id: 1, name: 'Link'}, {id: 2, name: 'Attachment'}],
    adminAs: [{id: 1, name: 'Ordered for'}, {id: 2, name: 'Submitted by'}],
    clientAs: [{id: 1, name: 'Ordered for'}, {id: 2, name: 'Submitted by'}]
  };

  /**
   * Retrieve settings on load
   */
  vm.init = function () {
    AdminUsersClientCompanySettingsService.getSettings();
  };
  /**
   * Display general settings
   */
  vm.generalSettings = function () {
    $scope.$broadcast('show-modal', 'general-settings', true);
    $scope.scrollTop();
  };
  /**
   * Enable/disable setting on client
   */
  vm.enableSetting = function (type, setting) {
    AdminUsersClientCompanySettingsService.enableSetting(type, setting);
  };
  /**
   * Display third party integrations
   */
  vm.thirdPartyIntegrations = function () {
    $scope.$broadcast('show-modal', 'third-party-integrations', true);
    $scope.scrollTop();
  };
  /**
   * Display merged document templates
   */
  vm.mergedDocumentTemplates = function () {
    $scope.$broadcast('show-modal', 'merged-document-templates', true);
    $scope.scrollTop();
  };
  /**
   * Display payment options
   */
  vm.paymentOptions = function () {
    $scope.$broadcast('show-modal', 'payment-options', true);
    $scope.scrollTop();
  };

  /**
   * Set filename when emailed
   */
  vm.setFilename = function () {
    $scope.$broadcast('show-modal', 'set-filename-emailed', true);
    $scope.scrollTop();
  };

  /**
   * Set emailed filename
   */
  vm.setFilenameOption = function (setting) {
    // Create filename
    if (angular.isUndefined(vm.displayedFilename)) {
      vm.displayedFilename = '[' + setting.name + ']';
    } else {
      vm.displayedFilename = vm.displayedFilename + ' [' + setting.name + ']';
    }
  };

  /**
   * Send updated filename to the backend
   */
  vm.updateFilenameEmailed = function () {
    AdminUsersClientCompanySettingsService.storeUpdatedFilename(vm.displayedFilename)
    .then(function () {
      // Hide modal
      $scope.$broadcast('hide-modal', 'set-filename-emailed');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'set-filename-emailed-failure', true);
      $scope.scrollTop();
    });
  };

  /**
   * Set fee margin for job types
   */
  vm.setFeeMargin = function () {
    $scope.$broadcast('show-modal', 'set-fee-margin', true);
    $scope.scrollTop();
  };

  /**
   * Keep other options in sync with the backend
   */
  $scope.$watch(function () {
    return AdminUsersClientCompanySettingsService.safeData.other;
  }, function (newVal, oldVal) {
    if (angular.isUndefined(newVal) || !Object.keys(oldVal).length) {
      return;
    }
    if (vm.typing) {
      $timeout.cancel(vm.typing);
    }
    // Wait and then make the request
    vm.typing = $timeout(function () {
      AdminUsersClientCompanySettingsService.updateOtherOptions(newVal)
      .catch(function () {
        $scope.$broadcast('show-modal', 'update-options-failure', true);
        $scope.scrollTop();
      });
      vm.typing = null;
    }, REQUEST_DELAY.ms);
  }, true);

  // Initiate data load
  vm.init();
}]);