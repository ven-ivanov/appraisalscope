'use strict';

var app = angular.module('frontendApp');

/**
 * Set fee margin by state table
 */
app.controller('FeeMarginByStateController', ['$scope', 'AdminUsersClientCompanySettingsService', 'REQUEST_DELAY', '$timeout',
function ($scope, AdminUsersClientCompanySettingsService, REQUEST_DELAY, $timeout) {
  var vm = this;

  vm.heading = [
    {
      label: 'State',
      data: 'state',
      noSort: true
    },
    {
      label: 'Fee margin',
      input: true,
      noSort: true
    }
  ];

  /**
   * Retrieve fee margin by state data
   */
  $scope.$watchCollection(function () {
    return AdminUsersClientCompanySettingsService.feeMarginByState.id;
  }, function (newVal) {
    if (angular.isUndefined(newVal)) {
      return;
    }
    vm.jobTypeId = newVal;
    // Generate fee margin by state data
    AdminUsersClientCompanySettingsService.feeMarginByStateTableData.call(vm, vm.jobTypeId);
  });

  /**
   * Update values on type
   */
  $scope.$watchCollection(function () {
    return vm.tableInputs;
  }, function (newVal, oldVal) {
    if (angular.isUndefined(newVal) || !Object.keys(newVal).length || !Object.keys(oldVal).length) {
      return;
    }
    if (vm.typing) {
      $timeout.cancel(vm.typing);
    }
    // Make request after 500 ms
    vm.typing = $timeout(function () {
      AdminUsersClientCompanySettingsService.updateFeeMarginByState(vm.jobTypeId, newVal)
      .catch(function () {
        $scope.$emit('show-modal', 'fee-margin-by-job-type-failure', true);
        $scope.scrollTop();
      });
      vm.typing = null;
    }, REQUEST_DELAY.ms);
  });
}]);