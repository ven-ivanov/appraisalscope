'use strict';

var app = angular.module('frontendApp');

/**
 * Set fee margin by job type table controller
 */
app.controller('FeeMarginController',
['$scope', 'AdminUsersClientCompanySettingsService', 'REQUEST_DELAY', '$timeout', function ($scope, AdminUsersClientCompanySettingsService, REQUEST_DELAY, $timeout) {
  var vm = this;

  // Table heading data
  vm.heading = [
    {
      label: 'Job Type',
      data: 'name',
      noSort: true
    },
    {
      label: 'State',
      linkLabel: 'Set by state',
      fn: 'setByState',
      noSort: true
    },
    {
      label: 'Fee margin type',
      dropdown: true,
      selectOptions: [{value: 'percentage'}, {value: 'flat'}]
    },
    {
      label: 'Fee margin',
      data: 'feeMargin',
      input: true,
      noSort: true
    }
  ];
  // Inputs and dropdowns
  vm.tableInputs = {};
  vm.multiSelect = {};

  /**
   * Generate table data
   */
  $scope.$watchCollection(function () {
    return AdminUsersClientCompanySettingsService.displayData.feeMargin;
  }, function (newVal) {
    if (!Object.keys(newVal).length) {
      return;
    }
    vm.tableData = newVal;
    vm.rowData = vm.tableData.slice();
    // Generate dropdown and input values
    AdminUsersClientCompanySettingsService.generateFeeMarginTableData.call(vm);
  });

  /**
   * Update fee margin percentage/flat
   */
  $scope.$watchCollection(function () {
    return vm.multiSelect;
  }, function (newVal, oldVal) {
    if (angular.isUndefined(newVal) || !Object.keys(newVal).length || !Object.keys(oldVal).length) {
      return;
    }
    AdminUsersClientCompanySettingsService.updateFeeMarginByJobType('type', newVal)
    .catch(function () {
      $scope.$emit('show-modal', 'fee-margin-by-job-type-failure', true);
      $scope.scrollTop();
    });
  });

  /**
   * Update fee margin value
   */
  $scope.$watchCollection(function () {
    return vm.tableInputs;
  }, function (newVal, oldVal) {
    if (angular.isUndefined(newVal) || !Object.keys(newVal).length || !Object.keys(oldVal).length) {
      return;
    }
    if (vm.typing) {
      $timeout.cancel(vm.typing);
    }
    // Wait and make the request
    vm.typing = $timeout(function () {
      AdminUsersClientCompanySettingsService.updateFeeMarginByJobType('value', newVal)
      .catch(function () {
        $scope.$emit('show-modal', 'fee-margin-by-job-type-failure', true);
        $scope.scrollTop();
      });
      vm.typing = null;
    }, REQUEST_DELAY.ms);
  });

  /**
   * Display individual state functions
   * @param id
   */
  vm.linkFn = function (id) {
    AdminUsersClientCompanySettingsService.feeMarginByState = {id: id};
    $scope.$emit('show-modal', 'fee-margin-by-state', true);
    $scope.scrollTop();
  };
}]);