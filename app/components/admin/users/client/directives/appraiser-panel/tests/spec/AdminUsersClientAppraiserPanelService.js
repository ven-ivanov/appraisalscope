describe('AdminUsersClientAppraiserPanelService', function () {
  var scope, httpBackend, Service, callerObj, data, adminUsersService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });
  beforeEach(inject(function ($rootScope, $httpBackend, AdminUsersClientAppraiserPanelService, AdminUsersService) {
    callerObj = {};
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    Service = AdminUsersClientAppraiserPanelService;
    adminUsersService = AdminUsersService;

    // Hash spy
    spyOn(Service, 'hashData');
    spyOn(Service, 'formatData');

    // Http
    httpBackend.whenGET(/approved/).respond({data: []});
    httpBackend.whenGET(/do-not-use/).respond({data: []});
    httpBackend.whenPUT(/.*/).respond();
    httpBackend.whenDELETE(/.*/).respond();
  }));

  describe('init', function () {
    beforeEach(function () {
      spyOn(Service, 'getApprovedAll').and.callThrough();
      spyOn(Service, 'getDoNotUseAll').and.callThrough();
      spyOn(Service, 'getApprovedAssigned').and.callThrough();
      spyOn(Service, 'getDoNotUseAssigned').and.callThrough();
      spyOn(Service, 'determineIntersection');
      Service.init();
      httpBackend.flush();
      scope.$digest();
    });

    it('should get all approved appraiser lists', function () {
      expect(Service.getApprovedAll).toHaveBeenCalled();
    });
    it('should get all do not use lists', function () {
      expect(Service.getDoNotUseAll).toHaveBeenCalled();
    });
    it('should get assigned approved appraiser lists', function () {
      expect(Service.getApprovedAssigned).toHaveBeenCalled();
    });
    it('should get assigned do not use lists', function () {
      expect(Service.getDoNotUseAssigned).toHaveBeenCalled();
    });
    it('should determine intersection of approved', function () {
      expect(Service.determineIntersection.calls.argsFor(0)).toEqual(['approved']);
    });
    it('should determine intersection of do not use', function () {
      expect(Service.determineIntersection.calls.argsFor(1)).toEqual(['doNotUse']);
    });
  });

  describe('getApprovedAll', function () {
    beforeEach(function () {
      Service.getApprovedAll();
      httpBackend.flush();
      scope.$digest();
    });

    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
  });

  describe('getApprovedAssigned', function () {
    beforeEach(function () {
      Service.getApprovedAssigned();
      httpBackend.flush();
      scope.$digest();
    });

    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
  });

  describe('getDoNotUseAll', function () {
    beforeEach(function () {
      Service.getApprovedAssigned();
      httpBackend.flush();
      scope.$digest();
    });

    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
  });

  describe('getDoNotUseAssigned', function () {
    beforeEach(function () {
      Service.getDoNotUseAssigned();
      httpBackend.flush();
      scope.$digest();
    });

    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
  });

  describe('determineIntersection', function () {
    beforeEach(function () {
      Service.safeData.approvedAll = {
        1: {id: 1},
        2: {id: 2},
        3: {id: 3}
      };
      Service.safeData.approvedAssigned = {
        2: {id: 2},
        3: {id: 3}
      };
      Service.determineIntersection('approved');
    });

    it('should remove intersections from all', function () {
      expect(Service.safeData.approvedAll).toEqual({
        1: {
          id: 1
        }
      });
    });
    it('should format all', function () {
      expect(Service.formatData.calls.argsFor(0)).toEqual([ 'approvedAll' ]);
    });
    it('should format assigned', function () {
      expect(Service.formatData.calls.argsFor(1)).toEqual([ 'approvedAssigned' ]);
    });
  });

  describe('beforeAdd', function () {
    beforeEach(function () {
      Service.safeData.approvedAll = {1: {id: 1}};
      Service.beforeAdd('approved', 1);
    });

    it('should hold reference to the selected list type', function () {
      expect(Service.displayData.selectedListType).toEqual('approved');
    });
    it('should hold reference to the selected list', function () {
      expect(Service.displayData.selectedList).toEqual({id: 1});
    });
  });

  describe('addToList', function () {
    beforeEach(function () {
      spyOn(Service, 'request').and.callThrough();
      spyOn(Service, 'updateValues');
      Service.displayData.selectedListType = 'approved';
      Service.displayData.selectedList = {id: 1};
      Service.addToList();
      httpBackend.flush();
      scope.$digest();
    });

    it('should make the right request', function () {
      expect(Service.request.calls.argsFor(0)).toEqual(['addApproved', {id: 1}, {}]);
    });
    it('should update table and dropdown', function () {
      expect(Service.updateValues).toHaveBeenCalled();
    });
  });

  describe('deleteFromList', function () {
    beforeEach(function () {
      spyOn(Service, 'request').and.callThrough();
      spyOn(Service, 'updateValues');
      Service.displayData.selectedListType = 'approved';
      Service.displayData.selectedList = {id: 1};
      Service.deleteFromList();
      httpBackend.flush();
      scope.$digest();
    });

    it('should make the right request', function () {
      expect(Service.request.calls.argsFor(0)).toEqual([ 'removeApproved', 1 ]);
    });
    it('should update table and dropdown', function () {
      expect(Service.updateValues).toHaveBeenCalled();
    });
  });

  describe('updateValues', function () {
    beforeEach(function () {
      Service.safeData.approvedAll = {1: {id: 1}};
      Service.displayData.selectedList = {id: 1};
      Service.updateValues('approved', 1, true);
    });

    it('should add to assigned safe data', function () {
      expect(Service.safeData.approvedAssigned).toEqual({ 1: { id: 1 } });
    });
    it('should remove from all safe data', function () {
      expect(Service.safeData.approvedAll).toEqual({});
    });
    it('should format assigned', function () {
      expect(Service.formatData.calls.argsFor(0)).toEqual(['approvedAssigned']);
    });
    it('should format all', function () {
      expect(Service.formatData.calls.argsFor(1)).toEqual(['approvedAll']);
    });
  });
});