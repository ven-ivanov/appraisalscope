describe('AdminUsersClientAppraiserPanelCtrl', function () {
  var scope, controller, adminUsersClientAppraiserPanelService, adminUsersClientService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersClientAppraiserPanelService', AdminUsersClientAppraiserPanelServiceMock);
      $provide.factory('AdminUsersClientService', AdminUsersClientServiceMock);
    });
  });

  beforeEach(inject(function ($rootScope, $controller, AdminUsersClientAppraiserPanelService, AdminUsersClientService) {
    scope = $rootScope.$new();
    adminUsersClientAppraiserPanelService = AdminUsersClientAppraiserPanelService;
    adminUsersClientService = AdminUsersClientService;
    spyOn(adminUsersClientAppraiserPanelService, 'getApprovedLists');
    spyOn(adminUsersClientAppraiserPanelService, 'getDoNotUseLists');
    // Spy on broadcasts
    spyOn(scope, '$broadcast');
    controller = $controller('AdminUsersClientAppraiserPanelCtrl', {$scope: scope});
    scope.$digest();
    // Fake scrolltop
    scope.scrollTop = function () {};
  }));

  describe('add to lists', function () {
    it('should add to approved lists', function () {
      spyOn(adminUsersClientAppraiserPanelService, 'beforeAdd');
      controller.displayData.addApprovedList = 1;
      controller.displayData.addDoNotUseList = 1;
      scope.$digest();
      expect(adminUsersClientAppraiserPanelService.beforeAdd).toHaveBeenCalled();
      expect(adminUsersClientAppraiserPanelService.beforeAdd.calls.count()).toEqual(2);
    });
  });

  describe('updateList', function () {
    beforeEach(function () {
      spyOn(adminUsersClientAppraiserPanelService, 'addToList').and.callThrough();
      spyOn(adminUsersClientAppraiserPanelService, 'deleteFromList').and.callThrough();
    });
    describe('addList', function () {
      it('should call updateList', function () {
        controller.addList();
        scope.$digest();
        expect(adminUsersClientAppraiserPanelService.addToList).toHaveBeenCalled();
      });
    });
    describe('deleteListItem', function () {
      it('should call updateList', function () {
        controller.deleteListItem();
        scope.$digest();
        expect(adminUsersClientAppraiserPanelService.deleteFromList).toHaveBeenCalled();
      });
    });
  });
});