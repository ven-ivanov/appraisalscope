/**
 * AdminUsersClientAppraiserPanelService mock
 */
var AdminUsersClientAppraiserPanelServiceMock = function ($q) {
  return {
    safeData: {
      // Approved list
      approved: {},
      // Do not use list
      doNotUse: {}
    },
    displayData: {
      // Approved
      approved: {
        // Table
        tableItems: [],
        // Dropdown
        nonTableItems: []
      },
      // Do not use
      doNotUse: {
        tableItems: [],
        nonTableItems: []
      }
    },
    init: function () {

    },
    beforeAdd: function () {

    },
    getApprovedLists: function () {
      return $q(function (resolve) {
        resolve({
          data: {
            id: 1,
            list: 'Approved1'
          }
        });
      });
    },
    getDoNotUseLists: function () {
      return $q(function (resolve) {
        resolve({
          data: {
            id: 1,
            list: 'DoNotUse1'
          }
        });
      });
    },
    updateList: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    confirmAdd: function () {

    },
    addList: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    removeSelected: function () {

    },
    deleteListItem: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    addToList: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    deleteFromList: function () {
      return $q(function (resolve) {
        resolve();
      });
    }
  };
};