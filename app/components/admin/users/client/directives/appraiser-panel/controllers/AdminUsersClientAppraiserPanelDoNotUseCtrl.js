'use strict';

var app = angular.module('frontendApp');

/**
 * Appraiser panel controller for do not table
 */
app.controller('AdminUsersClientAppraiserPanelDoNotUseCtrl',
['$scope', 'AdminUsersClientAppraiserPanelService', function ($scope, AdminUsersClientAppraiserPanelService) {
  var vm = this;
  var Service = AdminUsersClientAppraiserPanelService;

  /**
   * Table column headers
   */
  vm.heading = [// Company name
    {
      label: 'Title',
      data: 'title',
      search: true
    }, // Function to display users for a company
    {
      label: 'Delete List',
      linkLabel: 'Delete List',
      fn: 'deleteList',
      noSort: true
    }];

  /**
   * Confirm delete
   * @param id
   */
  vm.linkFn = function (id) {
    $scope.$emit('show-modal', 'delete-do-not-use-list', true);
    $scope.scrollTop();
    // Get reference to selected list
    Service.displayData.selectedList = Service.safeData.doNotUseAssigned[id];
    Service.displayData.selectedListType = 'doNotUse';
  };

  /**
   * Regenerate table when records are deleted or added
   */
  $scope.$watchCollection(function () {
    return Service.displayData.doNotUseAssigned;
  }, function (newVal) {
    if (angular.isUndefined(newVal)) {
      return;
    }
    // Update table data
    vm.tableData = newVal;
    vm.rowData = newVal.slice();
  });
}]);