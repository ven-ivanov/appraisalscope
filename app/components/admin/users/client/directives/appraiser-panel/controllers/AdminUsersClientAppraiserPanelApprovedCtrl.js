'use strict';

var app = angular.module('frontendApp');

/**
 * Appraiser panel controller for approved lists
 */
app.controller('AdminUsersClientAppraiserPanelApprovedCtrl',
['$scope', 'AdminUsersClientAppraiserPanelService', function ($scope, AdminUsersClientAppraiserPanelService) {
  var vm = this;
  var Service = AdminUsersClientAppraiserPanelService;
  /**
   * Table column headers
   */
  vm.heading = [// Company name
    {
      label: 'Title',
      data: 'title',
      search: true
    }, // Function to display users for a company
    {
      label: 'Delete List',
      linkLabel: 'Delete List',
      fn: 'deleteList',
      noSort: true
    }];

  /**
   * Confirm delete
   * @param id
   */
  vm.linkFn = function (id) {
    $scope.$emit('show-modal', 'delete-approved-list', true);
    $scope.scrollTop();
    // Save the approved list on the service
    Service.displayData.selectedList = Service.safeData.approvedAssigned[id];
    Service.displayData.selectedListType = 'approved';
  };

  /**
   * Regenerate table when records are deleted or added
   */
  $scope.$watchCollection(function () {
    return Service.displayData.approvedAssigned;
  }, function (newVal) {
    if (angular.isUndefined(newVal)) {
      return;
    }
    // Update table items
    vm.tableData = newVal;
    vm.rowData = newVal.slice();
  });
}]);