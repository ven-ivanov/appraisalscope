'use strict';

var app = angular.module('frontendApp');

/**
 * Appraiser panel controller
 */
app.controller('AdminUsersClientAppraiserPanelCtrl',
['$scope', 'AdminUsersClientAppraiserPanelService',
function ($scope, AdminUsersClientAppraiserPanelService) {
  var vm = this;
  var Service = AdminUsersClientAppraiserPanelService;

  // Display data
  vm.displayData = Service.displayData;

  /**
   * Init data load
   */
  vm.init = function () {
    // Retrieve all lists for dropdown
    Service.init();
  };

  /**
   * Add list from approved lists
   */
  $scope.$watch(function () {
    return vm.displayData.addApprovedList;
  }, function (newVal) {
    if (angular.isUndefined(newVal) || !newVal) {
      return;
    }
    // Keep reference to type and list
    Service.beforeAdd('approved', newVal);
    // Confirm add
    $scope.$broadcast('show-modal', 'add-list', true);
  });

  /**
   * Add list from do not use lists
   */
  $scope.$watch(function () {
    return vm.displayData.addDoNotUseList;
  }, function (newVal) {
    if (angular.isUndefined(newVal) || !newVal) {
      return;
    }
    // Keep reference to type and list
    Service.beforeAdd('doNotUse', newVal);
    // Confirm add do not use list
    $scope.$broadcast('show-modal', 'add-list', true);
    $scope.scrollTop();
  });

  /**
   * Add list
   */
  vm.addList = function () {
    Service.addToList()
    .then(function () {
      $scope.$broadcast('hide-modal', 'add-list');
    });
  };

  /**
   * Delete list item
   */
  vm.deleteListItem = function () {
    Service.deleteFromList()
    .then(function () {
      // Close any open confirm modal
      $scope.$broadcast('hide-modal', 'delete-approved-list');
      $scope.$broadcast('hide-modal', 'delete-do-not-use-list');
    });
  };

  vm.init();
}]);