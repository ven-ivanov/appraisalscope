'use strict';

var app = angular.module('frontendApp');

app.factory('AdminUsersClientAppraiserPanelService',
['AdminUsersService', 'AdminUsersResourceService', 'AsDataService', '$q', function (AdminUsersService, AdminUsersResourceService, AsDataService, $q) {
  var Service = {
    safeData: {
      // Approved lists
      approvedAssigned: {},
      approvedAll: {},
      // Do not use lists
      doNotUseAssigned: {},
      doNotUseAll: {}
    },
    displayData: {
      // Approved lists
      approvedAssigned: [],
      approvedAll: [],
      // Do not use lists
      doNotUseAssigned: [],
      doNotUseAll: []
    },
    /**
     * Request (either company, user, or branch)
     */
    request: function (requestFn, urlParams, bodyParams) {
      // Get $resource configuration
      return AdminUsersResourceService.appraiserPanel[requestFn](AdminUsersService.record._type, urlParams, bodyParams);
    },
    /**
     * Retrieve complete set of appraiser lists
     */
    init: function () {
      $q.all([Service.getApprovedAll(),
              Service.getApprovedAssigned(),
              Service.getDoNotUseAll(),
              Service.getDoNotUseAssigned()])
      .then(function () {
        // Determine intersections between approved all and assigned
        Service.determineIntersection('approved');
        Service.determineIntersection('doNotUse');
      });
    },
    /**
     * Retrieve all approved appraiser lists
     * @returns {Promise}
     */
    getApprovedAll: function () {
      return Service.request('getApprovedAll')
      .then(function (response) {
        Service.hashData(response.data, 'approvedAll');
      });
    },
    /**
     * Retrieve assigned approved appraiser lists
     * @returns {Promise}
     */
    getApprovedAssigned: function () {
      return Service.request('getApproved')
      .then(function (response) {
        // Nothing for mass update users
        if (response.data) {
          Service.hashData(response.data, 'approvedAssigned');
        }
      });
    },
    /**
     * Retrieve all do not use appraiser lists
     * @returns {Promise}
     */
    getDoNotUseAll: function () {
      return Service.request('getDoNotUseAll')
      .then(function (response) {
        Service.hashData(response.data, 'doNotUseAll');
      });
    },
    /**
     * Retrieve assigned do not use appraiser lists
     * @returns {Promise}
     */
    getDoNotUseAssigned: function () {
      return Service.request('getDoNotUse')
      .then(function (response) {
        if (response.data) {
          Service.hashData(response.data, 'doNotUseAssigned');
        }
      });
    },
    /**
     * Determine the intersection between all and assigned lists
     * @param type String Approved or do not use
     */
    determineIntersection: function (type) {
      // Get IDs of all lists
      var all = Object.keys(Service.safeData[type + 'All']);
      // Get IDs of assigned lists
      var assigned = Object.keys(Service.safeData[type + 'Assigned']);
      // Determine intersection of IDs
      var intersections = Lazy(all)
      .intersection(assigned)
      .toArray();
      // Filter on intersection
      Service.safeData[type + 'All'] = Lazy(Service.safeData[type + 'All'])
      .filter(function (item) {
        return intersections.indexOf(String(item.id)) === -1;
      })
      .toObject();
      // Format all and assigned
      Service.formatData(type + 'All');
      Service.formatData(type + 'Assigned');
    },
    /**
     * Keep reference to selected list type and selected list
     * @param type String approved or doNotUse
     * @param id List ID
     */
    beforeAdd: function (type, id) {
      Service.displayData.selectedListType = type;
      Service.displayData.selectedList = angular.copy(Service.safeData[type + 'All'][id]);
    },
    /**
     * Add selected list to table
     */
    addToList: function () {
      // Determine request function
      var selectedListType = Service.displayData.selectedListType;
      var listId = Service.displayData.selectedList.id;
      var requestFn = selectedListType === 'approved' ? 'addApproved' : 'addDoNotUse';
      // Mass update
      if (AdminUsersService.record.massUpdateUsers) {
        requestFn = requestFn + 'Mass';
      }
      // Include employees as header
      var headers = AdminUsersService.record._type === 'user' ? {employees: AdminUsersService.id} : {};
      // Make appropriate request
      return Service.request(requestFn, {id: listId}, headers)
      .then(function () {
        // Update dropdown and table
        Service.updateValues(selectedListType, listId, true);
      });
    },
    /**
     * Delete from one of the lists
     */
    deleteFromList: function () {
      // Determine request function
      var selectedListType = Service.displayData.selectedListType;
      var listId = Service.displayData.selectedList.id;
      var requestFn = selectedListType === 'approved' ? 'removeApproved' : 'removeDoNotUse';
      // Mass update
      if (AdminUsersService.record.massUpdateUsers) {
        requestFn = requestFn + 'Mass';
      }
      return Service.request(requestFn, listId)
      .then(function () {
        // Update dropdown and table
        Service.updateValues(selectedListType, listId, false);
      });
    },
    /**
     * Update values in dropdown and table
     * @param selectedListType Approved or doNotUse
     * @param listId List ID
     * @param addToTable Add or remove from table
     */
    updateValues: function (selectedListType, listId, addToTable) {
      var listType = addToTable ? 'Assigned' : 'All';
      var deleteType = addToTable ? 'All' : 'Assigned';
      // Add to all
      Service.safeData[selectedListType + listType][listId] = angular.copy(Service.displayData.selectedList);
      // Remove from assigned
      delete Service.safeData[selectedListType + deleteType][listId];
      // Format both
      Service.formatData(selectedListType + 'Assigned');
      Service.formatData(selectedListType + 'All');
    }
  };

  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service);
  Service.transformData = AsDataService.transformData.bind(Service);

  return Service;
}]);