'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersClientAppraiserPanel',
['AdminUsersDirectiveInheritanceService', function (AdminUsersDirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/client/directives/appraiser-panel/directives/partials/appraiser-panel.html',
    controller: 'AdminUsersClientAppraiserPanelCtrl',
    controllerAs: 'ctrl',
    link: function (scope) {
      AdminUsersDirectiveInheritanceService.inherit.call(scope);
    }
  };
}]);