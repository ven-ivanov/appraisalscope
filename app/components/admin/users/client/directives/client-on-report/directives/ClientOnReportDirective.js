'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Client on report
 */
app.directive('adminUsersClientOnReport', ['AdminUsersDirectiveInheritanceService', function (AdminUsersDirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/client/directives/client-on-report/directives/partials/client-on-report.html',
    controller: 'AdminUsersClientOnReportCtrl',
    controllerAs: 'ctrl',
    link: function (scope, element, attrs) {
      AdminUsersDirectiveInheritanceService.inherit.call(scope);
      // Whether mass update of employees is taking place
      scope.massUpdate = scope.$eval(attrs.massUpdate);
    }
  };
}]);