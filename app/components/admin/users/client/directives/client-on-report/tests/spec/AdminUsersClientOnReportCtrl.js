'use strict';

describe('AdminUsersClientOnReportCtrl', function () {
  var scope, controller, adminUsersClientOnReportService, adminUsersClientService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersClientOnReportService', AdminUsersClientOnReportServiceMock);
      $provide.factory('AdminUsersClientService', AdminUsersClientServiceMock);
    });
  });
  beforeEach(inject(function ($rootScope, $controller, AdminUsersClientOnReportService, AdminUsersClientService) {
    scope = $rootScope.$new();
    adminUsersClientOnReportService = AdminUsersClientOnReportService;
    adminUsersClientService = AdminUsersClientService;
    // Spy on load functions
    spyOn(adminUsersClientOnReportService, 'getCompanies').and.callThrough();
    spyOn(adminUsersClientOnReportService, 'splitCompanies').and.callThrough();
    controller = $controller('AdminUsersClientOnReportCtrl', {$scope: scope});
    scope.$digest();
  }));

  describe('init', function () {
    it('should load the necessary data on load', function () {
      expect(adminUsersClientOnReportService.getCompanies).toHaveBeenCalled();
      expect(adminUsersClientOnReportService.splitCompanies).toHaveBeenCalled();
    });
  });

  describe('checkAll', function () {
    beforeEach(function () {
      spyOn(adminUsersClientOnReportService, 'updateAll');
      controller.checkAll();
    });
    it('should call updateAll', function () {
      expect(adminUsersClientOnReportService.updateAll).toHaveBeenCalledWith(true);
    });
  });
  describe('uncheckAll', function () {
    beforeEach(function () {
      spyOn(adminUsersClientOnReportService, 'updateAll');
      controller.uncheckAll();
    });
    it('should call updateAll', function () {
      expect(adminUsersClientOnReportService.updateAll).toHaveBeenCalledWith(false);
    });
  });

  describe('updateClientSettings', function () {
    beforeEach(function () {
      spyOn(adminUsersClientOnReportService, 'updateClientSettings').and.callThrough();
      controller.updateClientSettings();
    });

    it('should call updateClientSettings', function () {
      expect(adminUsersClientOnReportService.updateClientSettings).toHaveBeenCalled();
    });
  });
});