'use strict';

describe('AdminUsersClientOnReportService', function () {
  var Service, httpBackend, scope, adminUsersService, asDataService, adminUsersClientService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });
  beforeEach(inject(function (AdminUsersClientOnReportService, $httpBackend, $rootScope, AdminUsersService, AsDataService, AdminUsersClientService) {
    Service = AdminUsersClientOnReportService;
    httpBackend = $httpBackend;
    scope = $rootScope.$new();
    asDataService = AsDataService;
    adminUsersClientService = AdminUsersClientService;

    adminUsersService = AdminUsersService;

    httpBackend.whenGET(/.*/).respond({defaultActive: 1, data: [1,2,3]});
    httpBackend.whenPATCH(/clients-on-report/).respond();
    httpBackend.whenPUT(/.*/).respond();
    httpBackend.whenDELETE(/.*/).respond();

    adminUsersService.record = {
      _type: 'company'
    };

    spyOn(Service, 'formatData');
    spyOn(asDataService, 'formatPlainObjectData');
    spyOn(Service, 'hashData');
  }));

  describe('getCompanies', function () {
    beforeEach(function () {
      spyOn(Service, 'getClientCompanies');
      spyOn(Service, 'mergeClientsOnReportData');
      Service.getCompanies();
      httpBackend.flush();
      scope.$digest();
    });

    it('should get client companies', function () {
      expect(Service.getClientCompanies).toHaveBeenCalled();
    });
    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should merge clients on report with client companies', function () {
      expect(Service.mergeClientsOnReportData).toHaveBeenCalled();
    });
    it('should format default client as plain object', function () {
      expect(asDataService.formatPlainObjectData).toHaveBeenCalled();
    });
    it('should call formatData', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('mergeClientsOnReportData', function () {
    beforeEach(function () {
      Service.safeData.companies = {2: {test: 'test'}, 3: {test: 'test3'}};
      Service.safeData.defaultClient.id = 1;
      Service.mergeClientsOnReportData([{company: {id: 1}}, {company: {id: 2}, blah: 'blah'}, {company: {id: 3}, blah: 'bleh'}]);
    });

    it('should set non-default companies', function () {
      expect(Service.safeData.companies[2]).toEqual({ test: 'test', company: { id: 2 }, blah: 'blah', isActive: true });
      expect(Service.safeData.companies[3]).toEqual({ test: 'test3', company: { id: 3 }, blah: 'bleh', isActive: true });
    });
    it('should set default company', function () {
      expect(Service.safeData.defaultClient).toEqual({ id: 1, company: { id: 1 }, isActive: true });
    });
  });

  describe('extractDefaultClient', function () {
    var response;
    beforeEach(function () {
      adminUsersService.record.id = 1;
    });
    describe('non-default record', function () {
      beforeEach(function () {
        response = Service.extractDefaultClient({id: 2})
      });

      it('should extract default client ID', function () {
        expect(Service.safeData.defaultClient.id).toEqual(1);
      });
      it('should always return false', function () {
        expect(response).toEqual(false);
      });
    });

    describe('default record', function () {
      beforeEach(function () {
        response = Service.extractDefaultClient({id: 1, blah: true})
      });

      it('should set default client', function () {
        expect(Service.safeData.defaultClient).toEqual({id: 1, blah: true});
      });
      it('should return true', function () {
        expect(response).toEqual(true);
      });
      it('should return false after the default client is found', function () {
        expect(Service.extractDefaultClient({id: 1, blah: true})).toEqual(false);
      });
    });
  });

  describe('splitCompanies()', function () {
    beforeEach(function () {
      Service.displayData.companies = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
      Service.splitCompanies();
    });
    it('should split the companies into three columns', function () {
      expect(Service.displayData.splitCompanies).toEqual({
        1: [1, 2, 3, 4],
        2: [5, 6, 7, 8],
        3: [9, 10]
      });
    });
  });

  describe('getSettings', function () {
    beforeEach(function () {
      Service.safeData.companies = {1: {id: 1}, 2: {id: 2}};
      Service.getSettings(1);
    });

    it('should return the selected client', function () {
      expect(Service.safeData.selectedClient).toEqual({id: 1});
    });
  });

  describe('updateAll', function () {
    beforeEach(function () {
      Service.safeData.selectedClient = {inheritance: {test: false, test2: {test3: false}}};
    });

    it('should return an object with all active companies', function () {
      Service.updateAll(1);
      expect(Service.safeData.selectedClient.inheritance).toEqual({ test: 1, test2: { test3: 1 } });
    });
  });

  describe('updateClientOnReport', function () {
    beforeEach(function () {
      spyOn(Service, 'splitCompanies');
      spyOn(Service, 'request').and.callThrough();
      Service.safeData.companies = {2: {id: 2}};
      Service.safeData.defaultClient = {id: 1};
    });

    describe('default', function () {
      describe('non-mass', function () {
        beforeEach(function () {
          Service.updateClientOnReport(1);
          httpBackend.flush();
          scope.$digest();
        });

        it('should add the client when not active', function () {
          expect(Service.request).toHaveBeenCalledWith('addClient', { company: 1 });
        });
        it('should remove the client when active', function () {
          Service.updateClientOnReport(1);
          expect(Service.request.calls.argsFor(1)).toEqual( [ 'removeClient', { company: 1 } ]);
        });
        it('should format data', function () {
          expect(Service.formatData).toHaveBeenCalled();
        });
        it('should format plain object for default', function () {
          expect(asDataService.formatPlainObjectData).toHaveBeenCalled();
        });
        it('should split companies', function () {
          expect(Service.splitCompanies).toHaveBeenCalled();
        });
      });
      describe('mass', function () {
        beforeEach(function () {
          adminUsersService.record.massUpdateUsers = [1,2];
          Service.updateClientOnReport(1);
          httpBackend.flush();
          scope.$digest();
        });

        it('should add the client when not active', function () {
          expect(Service.request).toHaveBeenCalledWith('addClientMass', { company: 1 });
        });
        it('should remove the client when active', function () {
          Service.updateClientOnReport(1);
          expect(Service.request.calls.argsFor(1)).toEqual([ 'removeClientMass', { company: 1 } ]);
        });
      });
    });
  });

  describe('updateClientSettings', function () {
    beforeEach(function () {
      Service.safeData.selectedClient = {id: 1};
      Service.updateClientSettings();
      scope.$digest();
    });
    it('should set the proper checked params', function () {
      expect(Service.safeData.companies).toEqual({ 1: { id: 1 } });
    });
  });
});