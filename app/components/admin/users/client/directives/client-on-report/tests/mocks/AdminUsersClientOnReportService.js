var AdminUsersClientOnReportServiceMock = function ($q) {
  var client1 = {id: 1, name: 'Default Client'};
  var client2 = {id: 2, name: 'Client 2'};
  var client3 = {id: 3, name: 'Client 3'};
  return {
    safeData: {},
    displayData: {
      // Selected companies
      selectedCompanies: []
    },
    getCompanies: function () {
      var that = this;
      that.displayData.companies = [client2, client3];
      return $q(function (resolve) {
        resolve();
      });
    },
    mergeData: function () {

    },
    extractDefaultClient: function () {

    },
    splitCompanies: function () {
      this.displayData.splitCompanies = {1: [client2], 2: [client3]};
    },
    getSelectedCompanies: function () {
      this.displayData.selectedCompanies = [client1, client3];
    },
    updateClientSettings: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    updateClientOnReport: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    updateAll: function () {

    },
    getSettings: function () {

    }
  }
};