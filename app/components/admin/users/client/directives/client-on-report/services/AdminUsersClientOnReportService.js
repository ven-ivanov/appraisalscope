'use strict';

var app = angular.module('frontendApp');

/**
 * @todo I can't proceed on this until we have some semblance of stabilization on the backend
 * Settings needs to be updated, as well as the whole default client bit enabling on the backend
 */

app.factory('AdminUsersClientOnReportService',
['AdminUsersService', 'AdminUsersResourceService', 'traverse', 'AsDataService',
function (AdminUsersService, AdminUsersResourceService, traverse, AsDataService) {
  // If default company has already been found
  var defaultCompanyFound = false;
  var Service = {
    safeData: {
      // Companies hash
      companies: {},
      // Default company
      defaultClient: {},
      // Whether default client on report is active
      defaultActive: '',
      // Selected client when updating settings
      selectedClient: ''
    },
    displayData: {
      // Companies
      companies: [],
      // Default company
      defaultClient: {},
      // Selected companies
      selectedCompanies: [],
      // Number of columns of clients
      columns: 3,
      // Yes no dropdown
      yesNoDropdown: AdminUsersService.yesNoDropdown
    },
    /**
     * Retrieve companies for client on report
     */
    getCompanies: function () {
      var companies;
      defaultCompanyFound = false;
      // Reset when switching views
      Service.safeData.defaultClient = {};
      Service.safeData.companies = {};
      // Get all client companies
      companies = Service.getClientCompanies();
      // Retrieve clients on report
      return Service.request('init')
      .then(function (response) {
        // Hash
        Service.hashData(companies, 'companies');
        // Merge clients and client on report data
        Service.mergeClientsOnReportData(response.data);
        // Format default
        AsDataService.formatPlainObjectData.call(Service, 'defaultClient');
        // Set for display
        Service.formatData();
      });
    },
    /**
     * Merge available companies and companies listed on report
     */
    mergeClientsOnReportData: function (companiesOnReport) {
      angular.forEach(companiesOnReport, function (company) {
        // Merge non-default
        if (company.company.id !== Service.safeData.defaultClient.id) {
          Service.safeData.companies[company.company.id] =
          _.assign(Service.safeData.companies[company.company.id], company);
          // Set active
          Service.safeData.companies[company.company.id].isActive = true;
          // Default
        } else {
          Service.safeData.defaultClient = _.assign(Service.safeData.defaultClient, company);
          // Set active
          Service.safeData.defaultClient.isActive = true;
        }
      });
    },
    /**
    * Extract default client (company that applies to this user, company, or branch)
    */
    extractDefaultClient: function (record) {
      // Don't continue after the default company has been found
      if (defaultCompanyFound) {
        return false;
      }
      // Find default company ID, if we don't already it
      if (angular.isUndefined(Service.safeData.defaultClient.id)) {
        switch (AdminUsersService.record._type) {
          case 'company':
            Service.safeData.defaultClient.id = AdminUsersService.record.id;
            break;
          case 'user':
            Service.safeData.defaultClient.id = AdminUsersService.record.company.id;
            break;
          case 'branch':
            Service.safeData.defaultClient.id = AdminUsersService._record.id;
            break;
        }
      }
      // Default company
      if (parseInt(record.id) === parseInt(Service.safeData.defaultClient.id)) {
        Service.safeData.defaultClient = angular.copy(record);
        defaultCompanyFound = true;
        return true;
      }
      return false;
    },
    /**
     * Split companies in 3 columns
     */
    splitCompanies: function () {
      var splitCompanies = {1: [], 2: [], 3: []}, length = Service.displayData.companies.length;
      var chunkSize = Math.ceil(Service.displayData.companies.length / Service.displayData.columns), i, iterations = 1;
      // Iterate and split into equal chunks
      for (i = 0; i < length; i = i + chunkSize) {
        splitCompanies[iterations] = Service.displayData.companies.slice(i, i + chunkSize);
        iterations = iterations + 1;
      }
      // Set for display
      Service.displayData.splitCompanies = splitCompanies;
    },
    /**
     * Retrieve settings for the selected client
     * @param id
     */
    getSettings: function (id) {
      Service.safeData.selectedClient = angular.copy(Service.safeData.companies[id]);
    },
    /**
     * Check the settings which are currently active
     */
    updateAll: function (activeVal) {
      // Those we retrieved inheritance objects for
      if (Service.safeData.selectedClient.inheritance) {
        // Traverse and set everything to true
        traverse(Service.safeData.selectedClient.inheritance).forEach(function (item) {
          // Split non-leaf nodes
          if (!angular.isObject(item)) {
            this.update(activeVal);
          }
        });
      } else {
        /**
         * @todo This will need to be updated when the backend is finished with this
         */
        Service.safeData.selectedClient.inheritance = {
          'checklist': true,
          'instructions': true,
          'appraisersList': {
            'approved': true,
            'doNotUse': true
          },
          'ucdp': true,
          'jobtypesList': true,
          'settings': {
            'reports': {
              'merging': true,
              'emailBorrowerByDefault': true
            },
            'integrations': {}
          }
        };
      }
    },
    /**
     * Update clients on report
     */
    updateClientOnReport: function (id) {
      // Get company
      var thisCompany = Service.safeData.companies[id] ? Service.safeData.companies[id] : Service.safeData.defaultClient;
      var active = !!thisCompany.isActive, promise;
      // Add client
      if (!active) {
        thisCompany.isActive = true;
        // Mass update
        if (AdminUsersService.record.massUpdateUsers) {
          promise = Service.request('addClientMass', {company: id});
        } else {
          // Single update
          promise = Service.request('addClient', {company: id});
        }
        // Remove client
      } else {
        thisCompany.isActive = false;
        // Mass update
        if (AdminUsersService.record.massUpdateUsers) {
          promise = Service.request('removeClientMass', {company: id});
        } else {
          // Single update
          promise = Service.request('removeClient', {company: id});
        }
      }
      // Format
      return promise.then(function () {
        Service.formatData();
        AsDataService.formatPlainObjectData.call(Service, 'defaultClient');
        // Split 'em on up
        Service.splitCompanies();
      });
    },
    /**
     * Update client settings on submit
     */
    updateClientSettings: function () {
      // Update safe data
      Service.safeData.companies[Service.safeData.selectedClient.id] = Service.safeData.selectedClient;
      // mass update
      if (AdminUsersService.record.massUpdateUsers) {
        return Service.request('updateClientMass', Service.safeData.selectedClient);
      }
      // Request
      return Service.request('updateClient', Service.safeData.selectedClient);
    }
  };

  // General request
  Service.request = AdminUsersService.request.bind(Service, 'clientOnReport');
  // Hash
  Service.hashData = AsDataService.hashData.bind(Service);
  // Format
  Service.formatData = AsDataService.formatData.bind(Service, 'companies');
  // Transform
  Service.transformData = AsDataService.transformData.bind(Service);
  // Get client companies
  Service.getClientCompanies = AdminUsersService.getClientCompanies.bind(Service);

  return Service;
}]);