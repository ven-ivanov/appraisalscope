'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersClientOnReportCtrl',
['$scope', 'AdminUsersClientOnReportService', function ($scope, AdminUsersClientOnReportService) {
  var vm = this;
  // Safe and display data
  vm.displayData = AdminUsersClientOnReportService.displayData;
  vm.safeData = AdminUsersClientOnReportService.safeData;

  /**
   * Companies on load
   */
  vm.init = function () {
    // Get companies on report
    AdminUsersClientOnReportService.getCompanies()
    .then(function () {
      // Split into multiple columns
      AdminUsersClientOnReportService.splitCompanies();
    });
  };

  /**
   * Update which companies will be shown on report
   * @param id
   */
  vm.updateCompanyOnReport = function (id) {
    AdminUsersClientOnReportService.updateClientOnReport(id)
    .catch(function () {
      $scope.$broadcast('show-modal', 'update-client-on-report-failure', true);
      $scope.scrollTop('#user-details');
      $scope.scrollTop();
    });
  };
  /**
   * Individual client settings
   */
  vm.displayClientSettings = function (id) {
    // Hold reference to the client for whom settings is being displayed
    vm.safeData.selectedClient = id;
    // Display modal
    $scope.$broadcast('show-modal', 'client-settings', true);
    $scope.scrollTop('#user-details');
    $scope.scrollTop();
    // Get settings for selected client
    AdminUsersClientOnReportService.getSettings(id);
  };
  /**
   * Set all settings active
   */
  vm.checkAll = function () {
    AdminUsersClientOnReportService.updateAll(true);
  };
  /**
   * Set all settings inactive
   */
  vm.uncheckAll = function () {
    AdminUsersClientOnReportService.updateAll(false);
  };
  /**
   * Update client settings
   */
  vm.updateClientSettings = function () {
    // Make request
    AdminUsersClientOnReportService.updateClientSettings()
    .then(function () {
      // Success
      $scope.$broadcast('hide-modal', 'client-settings');
    })
    .catch(function () {
      $scope.$broadcast('hide-modal', 'client-settings-failure');
    });
  };
  // Load data
  vm.init();
}]);