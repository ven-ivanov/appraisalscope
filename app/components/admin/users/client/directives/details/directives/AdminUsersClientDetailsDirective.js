'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users details pane details tab
 */
app.directive('adminUsersClientDetails', [function () {
  return {
    templateUrl: '/components/admin/users/client/directives/details/directives/partials/details.html'
  };
}]);