'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersDetailsContent', [function () {
  return {
    templateUrl: '/components/admin/users/client/directives/details/directives/partials/details-content.html',
    replace: true,
    scope: {
      ctrl: '='
    }
  };
}]);