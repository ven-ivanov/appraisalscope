'use strict';

var app = angular.module('frontendApp');

/**
 * Hidden details sidebar for smaller screens
 */
app.directive('adminUsersDetailsSidebar', [function () {
  return {
    templateUrl: '/components/admin/users/client/directives/details/directives/partials/details-sidebar.html'
  };
}]);