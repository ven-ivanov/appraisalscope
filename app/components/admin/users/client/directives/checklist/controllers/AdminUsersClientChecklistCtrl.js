'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersClientChecklistCtrl',
['$scope', 'AdminUsersClientService', 'AdminUsersClientChecklistService', '$timeout',
 function ($scope, AdminUsersClientService, AdminUsersClientChecklistService, $timeout) {
   var vm = this;
   var Service = AdminUsersClientChecklistService;
   // Keep reference to display and safe data
   vm.displayData = Service.displayData;
   vm.safeData = Service.safeData;
   // Table heading
   vm.heading = Service.safeData.heading;
   // Initialize empty
   vm.tableInputs = {};
   // Kill input watch
   vm.killInputWatch = angular.noop;
   // Kill name watch
   vm.killNameWatch = angular.noop;
   // Set delay before sending request on updating checklist title
   var typingDelay = 500;
   var typing;

   /**
    * Load data on init
    */
   vm.init = function () {
     // Retrieve checklists on load
     Service.getChecklists()
     .catch(function () {
       // Failed to load data
       $scope.$broadcast('show-modal', 'retrieve-checklist-failure', true);
       $scope.scrollTop();
     });
   };

   /**
    * Build table when data is available
    */
   $scope.$watchCollection(function () {
     return Service.displayData.questions;
   }, function (newVal) {
     if (angular.isUndefined(newVal)) {
       return;
     }
     // Get table data
     vm.tableData = newVal;
     vm.rowData = vm.tableData.slice();
     // Init inputs with db data
     vm.tableInputs = Service.initInputs(vm.tableData);
   });

   /**
    * Watch for checklist being selected
    */
   $scope.$watch(function () {
     return vm.displayData.selectedChecklist;
   }, function (newVal) {
     if (!newVal) {
       return;
     }
     // Disable watching inputs
     vm.killInputWatch();
     // Disable watching name
     vm.killNameWatch();
     // Populate checklist table
     Service.populateChecklistTable()
     .then(function () {
       // Watch for changes to table inputs
       vm.watchInputs();
       // Watch name for change
       vm.watchName();
     });
   });

   /**
    * Watch for changes to table inputs and write to backend
    */
   vm.watchInputs = function () {
     /**
      * Watch table inputs to write when questions are edited
      */
     vm.killInputWatch = $scope.$watchCollection(function () {
       return vm.tableInputs;
     }, function (newVal, oldVal) {
       // Don't run on load
       if (angular.equals(newVal, oldVal) || !Object.keys(oldVal).length) {
         return;
       }
       // Wait to write
       if (typing) {
         $timeout.cancel(typing);
       } else {
         // Store val at beginning
         vm.beginVal = oldVal;
       }
       // Wait for delay, and then update
       typing = $timeout(function () {
         typing = null;
         Service.updateChecklistQuestions(newVal, vm.beginVal)
         .catch(function () {
           // Alert on failure
           $scope.$emit('show-modal', 'question-update-failure', true);
           $scope.scrollTop();
         });
       }, typingDelay);
     });
   };

   /**
    * Update checklist name
    */
   vm.watchName = function () {
     vm.killNameWatch = $scope.$watch(function () {
       return vm.displayData.checklistName;
     }, function (newVal, oldVal) {
       if (angular.isUndefined(oldVal) ||
           angular.equals(newVal, oldVal) ||
           !vm.displayData.selectedChecklist) {
         return;
       }
       // Don't execute the request until after the user has stopped typing
       if (typing) {
         $timeout.cancel(typing);
       }
       // Initiate request after 500 ms
       typing = $timeout(function () {
         Service.updateChecklistTitle(newVal)
         .catch(function () {
           $scope.$broadcast('show-modal', 'update-name-failure', true);
           $scope.scrollTop();
         });
         typing = null;
       }, typingDelay);
     });
   };

   /**
    * Delete question
    */
   vm.linkFn = function (id) {
     $scope.$emit('show-modal', 'delete-question', true);
     $scope.scrollTop();
     Service.displayData.selectedQuestion = id;
   };

   /**
    * Delete currently selected checklist confirmation
    */
   vm.deleteChecklistConfirm = function () {
     $scope.$broadcast('show-modal', 'delete-confirmation', true);
     $scope.scrollTop();
   };
   /**
    * Delete current checklist
    */
   vm.deleteChecklist = function () {
     Service.deleteChecklist()
     // Hide confirmation modal
     .then(function () {
       $scope.$broadcast('hide-modal', 'delete-confirmation');
     })
     .catch(function () {
       $scope.$broadcast('show-modal', 'delete-failure', true);
       $scope.scrollTop();
     });
   };

   /**
    * Assign checklist confirmation
    */
   vm.assignChecklistConfirmation = function () {
     $scope.$broadcast('show-modal', 'assign-checklist', true);
     $scope.scrollTop();
   };
   /**
    * Assign the selected checklist
    */
   vm.assignChecklist = function () {
     Service.assignChecklist()
     .then(function () {
       $scope.$broadcast('hide-modal', 'assign-checklist');
       $scope.$broadcast('show-modal', 'assign-checklist-success', true);
       $scope.scrollTop();
     })
     .catch(function () {
       $scope.$broadcast('show-modal', 'assign-checklist-failure', true);
       $scope.scrollTop();
     });
   };

   /**
    * Duplicate checklist confirmation
    */
   vm.duplicateChecklistConfirmation = function () {
     $scope.$broadcast('show-modal', 'duplicate-checklist', true);
     $scope.scrollTop();
   };
   /**
    * Duplicate checklist
    */
   vm.cloneChecklist = function () {
     Service.cloneChecklist()
     .then(function () {
       // Success
       $scope.$broadcast('hide-modal', 'duplicate-checklist');
       $scope.$broadcast('show-modal', 'duplicate-checklist-success', true);
       $scope.scrollTop();
       // Duplicate checklist name
       vm.duplicateChecklistName = '';
     })
     .catch(function () {
       // Failure
       $scope.$broadcast('show-modal', 'duplicate-checklist-failure', true);
       $scope.scrollTop();
     });
   };
   /**
    * Add question to checklist confirmation
    */
   vm.addQuestionConfirmation = function () {
     $scope.$broadcast('show-modal', 'add-question', true);
     $scope.scrollTop();
   };
   /**
    * Add question to checklist
    */
   vm.addQuestion = function () {
     Service.addQuestion()
     .then(function () {
       $scope.$broadcast('hide-modal', 'add-question');
     })
     .catch(function () {
       $scope.$broadcast('show-modal', 'add-question-failure', true);
       $scope.scrollTop();
     });
   };

   /**
    * Delete question
    */
   vm.deleteQuestion = function () {
     // Stop watching inputs
     vm.killInputWatch();
     Service.deleteQuestion()
     .then(function () {
       // Success
       $scope.$broadcast('hide-modal', 'delete-question');
     }).catch(function () {
       // Failure
       $scope.$broadcast('show-modal', 'delete-question-failure', true);
       $scope.scrollTop();
     })
     .finally(function () {
       vm.watchInputs();
     });
   };

   /**
    * Add new checklist
    */
   vm.addNewChecklistConfirmation = function () {
     $scope.$broadcast('show-modal', 'add-new-checklist', true);
     $scope.scrollTop();
   };
   /**
    * Add new checklist
    */
   vm.addChecklist = function () {
     // Make request
     Service.addNewChecklist()
     .then(function () {
       // Success
       $scope.$broadcast('hide-modal', 'add-new-checklist');
     })
     .catch(function () {
       // Failure
       $scope.$broadcast('show-modal', 'add-new-checklist-failure', true);
       $scope.scrollTop();
     });
   };

   /**
    * Disable assign button when assigned checklist is selected
    */
   vm.assignButtonDisabled = function () {
     return vm.displayData.selectedChecklist === vm.safeData.assignedChecklist;
   };

   // Initialize the view
   vm.init();
}]);