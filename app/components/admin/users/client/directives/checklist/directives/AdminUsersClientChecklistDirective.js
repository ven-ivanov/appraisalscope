'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersClientChecklist', ['AdminUsersDirectiveInheritanceService', function (AdminUsersDirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/client/directives/checklist/directives/partials/checklist.html',
    controller: 'AdminUsersClientChecklistCtrl',
    controllerAs: 'tableCtrl',
    link: function (scope) {
      // Expose scrollTop
      AdminUsersDirectiveInheritanceService.inherit.call(scope);
    }
  };
}]);