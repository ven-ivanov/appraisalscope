'use strict';

var app = angular.module('frontendApp');

app.factory('AdminUsersClientChecklistService',
['AdminUsersService', 'AdminUsersResourceService', '$q', 'AsDataService',
function (AdminUsersService, AdminUsersResourceService, $q, AsDataService) {
  var Service = {
    safeData: {
      // Main checklist data
      checklist: {},
      // Individual checklist for table
      questions: {},
      // Table definition
      heading: [
        // Checklist item title
        {
          label: 'Question Title',
          data: 'title',
          input: true,
          noSort: true
        },
        // Delete question
        {
          label: 'Delete Item',
          linkLabel: 'Delete Item',
          fn: 'deleteItem',
          noSort: true
        }
      ]
    },
    displayData: {
      checklist: [],
      questions: [],
      // Name for duplicating checklist
      duplicateChecklistName: '',
      // New question title
      questionTitle: '',
      // Currently selected question from link functions
      selectedQuestion: 0
    },
    request: function (requestFn, bodyParams) {
      return AdminUsersResourceService.checklist[requestFn](AdminUsersService.record._type, bodyParams);
    },
    /**
     * Retrieve checklists
     */
    getChecklists: function () {
      return Service.request('checklists')
      .then(function (response) {
        // Hash data
        Service.hashData(response.data, 'checklist');
        // Set for display
        Service.formatData('checklist');
        // Retrieve assigned checklist
        return Service.getAssignedChecklist();
      });
    },
    /**
     * Retrieve assigned checklist
     */
    getAssignedChecklist: function () {
      return Service.request('assigned')
      .then(function (response) {
        // Don't proceed with an assigned checklist for mass update users
        if (!angular.isObject(response)) {
          return;
        }
        // Set assigned checklist for display
        Service.displayData.selectedChecklist = response.id;
        // Assigned checklist
        Service.safeData.assignedChecklist = response.id;
      });
    },
    /**
     * Initialize the table inputs based on db data
     */
    initInputs: function (questions) {
      var inputs = {};
      // Get inputs from checklist data
      Lazy(questions)
      .each(function (row) {
        inputs[row.id] = row.text;
      });
      return inputs;
    },
    /**
     * Populate checklist question table
     */
    populateChecklistTable: function () {
      var checkListId = Service.displayData.selectedChecklist;
      // Retrieve questions
      return Service.request('checklistQuestions', checkListId)
      .then(function (response) {
        // Set name
        Service.displayData.checklistName = Service.safeData.checklist[checkListId].title;
        // Hash table data
        Service.hashData(response.data, 'questions');
        // Create display for table
        Service.formatData('questions');
      });
    },
    /**
     * Update current checklist title
     */
    updateChecklistTitle: function (title) {
      return Service.request('updateChecklistTitle', {
        checklistId: Service.displayData.selectedChecklist,
        title: title
      });
    },
    /**
     * Delete the currently selected checklist
     */
    deleteChecklist: function () {
      var checkListId = Service.displayData.selectedChecklist;
      return Service.request('deleteChecklist', {checklistId: checkListId})
      .then(function () {
        // Remove from safeData
        delete Service.safeData.checklist[checkListId];
        // Recreate display data
        Service.formatData('checklist');
        var remainingChecklists = Object.keys(Service.safeData.checklist);
        // Select first remaining checklist
        if (remainingChecklists.length) {
          Service.displayData.selectedChecklist = parseInt(remainingChecklists[0]);
        }
      });
    },
    /**
     * Assign the selected checklist
     */
    assignChecklist: function () {
      var checklistId = Service.displayData.selectedChecklist;
      return Service.request('assignChecklist', {checklistId: checklistId})
      .then(function () {
        // Set assigned checklist to the currently selected one
        Service.safeData.assignedChecklist = checklistId;
      });
    },
    /**
     * Duplicate checklist
     */
    cloneChecklist: function () {
      return Service.request('cloneChecklist', {
        checklistId: Service.displayData.selectedChecklist,
        title: Service.displayData.duplicateChecklistName
      })
      .then(function (response) {
        // Add new checklist onto safe data
        Service.safeData.checklist[response.id] = response;
        // Regenerate dropdown
        Service.formatData('checklist');
        // Clear modal input
        Service.displayData.duplicateChecklistName = '';
      });
    },
    /**
     * Add question to checklist
     */
    addQuestion: function () {
      // Request
      return Service.request('newQuestion', {
        checklistId: Service.displayData.selectedChecklist,
        text: Service.displayData.questionTitle
      })
      .then(function (response) {
        // Add to safe data
        Service.safeData.questions[response.id] = response;
        // Populate table
        Service.formatData('questions');
      });
    },
    /**
     * Update table questions
     */
    updateChecklistQuestions: function (questions, beginQuestion) {
      return $q(function (resolve) {
        var requests, requestLength, i, request;
        // Create array of requests
        requests = Lazy(AsDataService.computeObjectDiff(beginQuestion, questions))
        .map(function (item, key) {
          return {
            id: key,
            text: item
          };
        }).toArray();
        // Number of requests total
        requestLength = requests.length;
        // If no requests, just resolve
        if (!requestLength) {
          return resolve();
        }
        // Iterate queued requests and make each one
        for (i = 0; i < requestLength; i = i + 1) {
          request = requests.shift();
          Service.request('updateChecklistQuestion', {
            checklistId: Service.displayData.selectedChecklist,
            questionId: request.id,
            text: request.text
          }).then(function () {
            // If no more requests, resolve
            if (!requests.length) {
              resolve();
            }
          }); // jshint ignore:line
        }
      });
    },
    /**
     * Delete question
     */
    deleteQuestion: function () {
      var questionId = Service.displayData.selectedQuestion;
      // Request
      return Service.request('deleteQuestion', {
        checklistId: Service.displayData.selectedChecklist,
        questionId: questionId
      }).then(function () {
        // Refresh table
        delete Service.safeData.questions[questionId];
        Service.formatData('questions');
      });
    },
    /**
     * Add new checklist
     */
    addNewChecklist: function () {
      return Service.request('newChecklist', {title: Service.displayData.newChecklistName})
      .then(function (response) {
        // Push onto safe data
        Service.safeData.checklist[response.id] = response;
        // Recreate dropdown
        Service.formatData('checklist');
        // Select the new checklist
        Service.displayData.selectedChecklist = response.id;
        Service.displayData.newChecklistName = '';
        // Populate table
        Service.populateChecklistTable(response.id);
      });
    }
  };

  // Hash
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service);
  Service.transformData = AsDataService.transformData.bind(Service);

  return Service;
}]);