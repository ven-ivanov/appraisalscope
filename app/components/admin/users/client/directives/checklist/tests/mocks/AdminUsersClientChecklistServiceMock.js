var AdminUsersClientChecklistServiceMock = function ($q) {
  return {
    safeData: {
      // Main checklist data
      checklist: {
        1: {
          id: 1,
          name: 'Checklist 1',
          checklist: [{id: 1, question: 'Question 1'}, {id: 2, question: 'Question 2'}]
        },
        2: {
          id: 2,
          name: 'Checklist 2',
          checklist: [{id: 1, question: 'Question 1'}, {id: 2, question: 'Question 2'}]
        }
      },
      // Individual checklist for table
      tableData: []
    },
    displayData: {
      checklist: [],
      tableData: [],
      // Name for duplicating checklist
      duplicateChecklistName: ''
    },
    getChecklists: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    populateChecklistTable: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    notifyObserver: function () {

    },
    updateChecklistTitle: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    deleteChecklist: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    assignChecklist: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    cloneChecklist: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    addQuestion: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    deleteQuestion: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    addNewChecklist: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    registerObserver: function (callback) {
      this.observer = callback;
    }

  }
};