'use strict';

describe('AdminUsersClientChecklistService', function () {
  var Service, scope, httpBackend, rawData, hashData;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });
  beforeEach(inject(function (AdminUsersClientChecklistService, $rootScope, $httpBackend) {
    rawData = [
      {
        id: 1,
        title: 'Checklist 1'
      },
      {
        id: 2,
        title: 'Checklist 2'
      }
    ];
    hashData = {
      1: {
        id: 1,
        title: 'Checklist 1'
      },
      2: {
        id: 2,
        title: 'Checklist 2'
      }
    };

    scope = $rootScope.$new();
    Service = AdminUsersClientChecklistService;
    // Set safe data
    Service.safeData = {
      checklist: {
        1: {
          id: 1,
          title: 'Checklist 1'
        },
        2: {
          id: 2,
          title: 'Checklist 2'
        }
      },
      // Individual checklist for table
      questions: []
    };
    // Display data
    Service.displayData = {
      checklist: [],
      questions: [],
      duplicateChecklistName: ''
    };

    httpBackend = $httpBackend;
    httpBackend.whenGET(/checklists$/).respond({data: rawData});
    httpBackend.whenGET(/\d\/checklist/).respond({id: 1});
    httpBackend.whenGET(/questions/).respond({data: [1,2,3]});
    httpBackend.whenDELETE(/.*/).respond({});
    httpBackend.whenPOST(/clone/).respond({
      id: 1000,
      title: 'Checklist 1000'
    });
    httpBackend.whenPOST(/questions/).respond({id: 1000, name: 'Question 1000'});
    httpBackend.whenPUT(/questions/).respond();
    httpBackend.whenPOST(/checklists/).respond({id: 1000, data: [1,2,3]});
    httpBackend.whenPUT(/checklist/).respond();

    // Spy on format data
    spyOn(Service, 'formatData');
    // and hash data
    spyOn(Service, 'hashData');
  }));

  describe('getChecklists', function () {
    beforeEach(function () {
      spyOn(Service, 'getAssignedChecklist');
      Service.getChecklists();
      httpBackend.flush();
      scope.$digest();
    });
    it('should hash the data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should format the data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
    it('should call getChecklists', function () {
      expect(Service.getAssignedChecklist).toHaveBeenCalled();
    });
  });

  describe('getAssignedChecklist', function () {
    beforeEach(function () {
      Service.getAssignedChecklist();
      httpBackend.flush();
      scope.$digest();
    });

    it('should set assigned checklist to selected', function () {
      expect(Service.displayData.selectedChecklist).toEqual(1);
    });
  });

  describe('initInputs', function () {
    var response;
    beforeEach(function () {
      var questions = [{id: 1, text: 'Question 1'}, {id: 2, text: 'Question 2'}];
      response = Service.initInputs(questions);
    });

    it('should return input init data', function () {
      expect(response).toEqual({ 1: 'Question 1', 2: 'Question 2' });
    });
  });

  describe('populateChecklistTable', function () {
    beforeEach(function () {
      Service.displayData.selectedChecklist = 1;
      Service.populateChecklistTable();
      httpBackend.flush();
      scope.$digest();
    });

    it('should set the checklist name', function () {
      expect(Service.displayData.checklistName).toEqual('Checklist 1');
    });
    it('should hash table data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should format table data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('updateChecklistTitle', function () {
    beforeEach(function () {
      spyOn(Service, 'request').and.callThrough();
      Service.displayData.selectedChecklist = 1;
      Service.updateChecklistTitle('Title');
    });

    it('should make the right request', function () {
      expect(Service.request.calls.argsFor(0)).toEqual(['updateChecklistTitle', {
        checklistId: 1,
        title: 'Title'
      }]);
    });
  });

  describe('deleteChecklist', function () {
    beforeEach(function () {
      Service.displayData.selectedChecklist = 1;
      Service.deleteChecklist();
      httpBackend.flush();
      scope.$digest();
    });

    it('should update safe data', function () {
      expect(Service.safeData.checklist).toEqual({ 2: { id: 2, title: 'Checklist 2' } });
    });
    it('should reset selected checklist', function () {
      expect(Service.displayData.selectedChecklist).toEqual(2);
    });
    it('should reset table data', function () {
      expect(Service.displayData.questions).toEqual([]);
      expect(Service.safeData.questions).toEqual([]);
    });
    it('should call format data', function () {
      expect(Service.formatData).toHaveBeenCalledWith('checklist');
    });
  });

  describe('assignChecklist', function () {
    beforeEach(function () {
      spyOn(Service, 'request').and.callThrough();
      Service.displayData.selectedChecklist = 1;
      Service.assignChecklist();
      httpBackend.flush();
      scope.$digest();
    });

    it('should  make the right request', function () {
      expect(Service.request.calls.argsFor(0)).toEqual([ 'assignChecklist', { checklistId: 1 } ]);
    });
    it('should set the assigned checklist', function () {
      expect(Service.safeData.assignedChecklist).toEqual(1);
    });
  });

  describe('cloneChecklist', function () {
    beforeEach(function () {
      Service.displayData.selectedChecklist = 1;
      Service.displayData.duplicateChecklistName = 'new checklist';
      Service.cloneChecklist();
      httpBackend.flush();
      scope.$digest();
    });

    it('should update safe data', function () {
      expect(Service.safeData.checklist[1000].title).toEqual('Checklist 1000');
    });
    it('should call format data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
    it('should remove the duplicate input value', function () {
      expect(Service.displayData.duplicateChecklistName).toEqual('');
    });
  });

  describe('addQuestion', function () {
    beforeEach(function () {
      Service.displayData.questionTitle = 'new question';
      Service.displayData.selectedChecklist = 1;
      Service.addQuestion();
      httpBackend.flush();
      scope.$digest();
    });

    it('should update safe data', function () {
      expect(Service.safeData.questions[1000].name).toEqual('Question 1000');
    });
    it('should call formatData', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('updateChecklistQuestions', function () {
    var questions, beginQuestions, response;
    beforeEach(function () {
      Service.displayData.selectedChecklist = 1;
      questions = {1: 'a', 2: 'b', 3: 'c'};
      beginQuestions = {1: 'z', 2: 'b', 3: 'c'};
      spyOn(Service, 'request').and.callThrough();
      response = Service.updateChecklistQuestions(questions, beginQuestions);
      httpBackend.flush();
      scope.$digest();
    });

    it('should make a single request for the updated question', function () {
      expect(Service.request.calls.count()).toEqual(1);
    });
    it('should have sent the proper data', function () {
      expect(Service.request).toHaveBeenCalledWith('updateChecklistQuestion', { checklistId: 1, questionId: '1', text: 'a' });
    });
  });

  describe('deleteQuestion', function () {
    beforeEach(function () {
      Service.safeData.questions = {1: {}, 2: {}};
      Service.displayData.selectedChecklist = 1;
      Service.displayData.selectedQuestion = 1;
      spyOn(Service, 'populateChecklistTable');
      Service.deleteQuestion();
      httpBackend.flush();
      scope.$digest();
    });

    it('should update safe data', function () {
      expect(Service.safeData.questions).toEqual({2: {}});
    });
    it('should format data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('addNewChecklist', function () {
    beforeEach(function () {
      spyOn(Service, 'populateChecklistTable');
      Service.displayData.newChecklistName = 'new checklist';
      Service.addNewChecklist();
      httpBackend.flush();
      scope.$digest();
    });

    it('should update safe data', function () {
      expect(Service.safeData.checklist[1000].data).toEqual([1,2,3]);
    });
    it('should call formatData', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
    it('should set selected checklist to new checklist', function () {
      expect(Service.displayData.selectedChecklist).toEqual(1000);
    });
    it('should reset form input', function () {
      expect(Service.displayData.newChecklistName).toEqual('');
    });
    it('should repopulate table', function () {
      expect(Service.populateChecklistTable).toHaveBeenCalled();
    });
  });
});