'use strict';

describe('AdminUsersClientChecklistCtrl', function () {
  var Service, scope, controller, timeout;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersClientChecklistService', AdminUsersClientChecklistServiceMock);
      $provide.factory('AdminUsersClientService', AdminUsersClientServiceMock);
    });
  });

  beforeEach(inject(function (AdminUsersClientChecklistService, $rootScope, $controller, $timeout) {
    Service = AdminUsersClientChecklistService;
    scope = $rootScope.$new();
    timeout = $timeout;

    // Instantiate
    controller = $controller('AdminUsersClientChecklistCtrl', {$scope: scope});
    // Spy on broadcast
    spyOn(scope, '$broadcast');
    // Fake scrolltop
    scope.scrollTop = function () {};
  }));

  describe('init', function () {
    beforeEach(function () {
      spyOn(Service, 'getChecklists').and.callThrough();
      controller.init();
    });
    it('should retrieve checklists on load', function () {
      expect(Service.getChecklists).toHaveBeenCalled();
    });
  });

  describe('selectedChecklist', function () {
    beforeEach(function () {
      spyOn(Service, 'populateChecklistTable').and.callThrough();
      controller.displayData.selectedChecklist = 1;
      scope.$digest();
    });

    it('should call populateChecklistTable', function () {
      expect(Service.populateChecklistTable).toHaveBeenCalled();
    });
  });

  describe('deleteChecklist', function () {
    beforeEach(function () {
      spyOn(Service, 'deleteChecklist').and.callThrough();
      controller.deleteChecklist();
      scope.$digest();
    });
    it('should call deleteChecklist', function () {
      expect(Service.deleteChecklist).toHaveBeenCalled();
    });
    it('should hide the modal', function () {
      expect(scope.$broadcast).toHaveBeenCalledWith('hide-modal', 'delete-confirmation');
    });
  });

  describe('assignChecklist', function () {
    beforeEach(function () {
      spyOn(Service, 'assignChecklist').and.callThrough();
      controller.assignChecklist();
      scope.$digest();
    });
    it('should call assignChecklist', function () {
      expect(Service.assignChecklist).toHaveBeenCalled();
    });
    it('should display the modals', function () {
      expect(scope.$broadcast).toHaveBeenCalledWith('hide-modal', 'assign-checklist');
      expect(scope.$broadcast).toHaveBeenCalledWith('show-modal', 'assign-checklist-success', true);
    });
  });
  //
  describe('cloneChecklist', function () {
    beforeEach(function () {
      spyOn(Service, 'cloneChecklist').and.callThrough();
      controller.cloneChecklist();
      scope.$digest();
    });

    it('should display the modals', function () {
      expect(scope.$broadcast).toHaveBeenCalledWith('show-modal', 'duplicate-checklist-success', true);
      expect(scope.$broadcast).toHaveBeenCalledWith('hide-modal', 'duplicate-checklist');
    });
    it('should call the appropriate services', function () {
      expect(Service.cloneChecklist).toHaveBeenCalled();
    });
  });

  describe('addQuestion', function () {
    beforeEach(function () {
      spyOn(Service, 'addQuestion').and.callThrough();
      controller.addQuestion();
      scope.$digest();
    });

    it('should call the modal', function () {
      expect(scope.$broadcast).toHaveBeenCalledWith('hide-modal', 'add-question');
    });

    it('should call the service', function () {
      expect(Service.addQuestion).toHaveBeenCalled();
    });
  });

  describe('deleteQuestion', function () {
    beforeEach(function () {
      spyOn(Service, 'deleteQuestion').and.callThrough();
      controller.deleteQuestion();
      scope.$digest();
    });

    it('should call the modal', function () {
      expect(scope.$broadcast).toHaveBeenCalledWith('hide-modal', 'delete-question');
    });

    it('should call the service', function () {
      expect(Service.deleteQuestion).toHaveBeenCalled();
    });
  });

  describe('addChecklist', function () {
    beforeEach(function () {
      spyOn(Service, 'addNewChecklist').and.callThrough();
      controller.addChecklist();
      scope.$digest();
    });

    it('should call the modal', function () {
      expect(scope.$broadcast).toHaveBeenCalledWith('hide-modal', 'add-new-checklist');
    });

    it('should call the service', function () {
      expect(Service.addNewChecklist).toHaveBeenCalled();
    });
  });
});