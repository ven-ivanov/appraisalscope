'use strict';

var app = angular.module('frontendApp');

/**
 * Mass update users
 */
app.controller('MassUpdateUsersCtrl',
['$scope', '$timeout', 'REQUEST_DELAY', 'AdminUsersCtrlInheritanceService', 'MassUpdateUsersService',
function ($scope, $timeout, REQUEST_DELAY, AdminUsersCtrlInheritanceService, MassUpdateUsersService) {
  var vm = this;
  // Keep reference
  vm.safeData = MassUpdateUsersService.safeData;
  vm.displayData = MassUpdateUsersService.displayData;
  // Get states dropdown
  AdminUsersCtrlInheritanceService.createCtrl.call(vm, MassUpdateUsersService, $scope, 'mass-user');
  // For changing branch details
  vm.branchDetails = {};
  // Tab config
  vm.detailsTabsConfigMassUsers = {
    details: 'Address & Company',
    feeSchedule: 'Fee Schedule',
    userSettings: 'User Settings',
    userTypeBranch: 'User Type/Branch',
    clientOnReport: 'Client On Report',
    appraiserPanel: 'Appraiser Panel',
    addChecklist: 'Add Checklist',
    instructions: 'Instructions'
  };
  // Tab config (need this to an array since objects don't guarantee order)
  vm.detailsTabsMassUsers =
  ['details', 'feeSchedule', 'userSettings', 'userTypeBranch', 'clientOnReport', 'appraiserPanel', 'addChecklist', 'instructions'];
  // Details
  vm.massDetails = {};

  /**
   * Watch for changes and write to the backend
   */
  $scope.$watch(function () {
    return vm.displayData.details;
  }, function (newVal, oldVal) {
    if (angular.isUndefined(newVal) || !Object.keys(oldVal).length) {
      return;
    }
    if (vm.typing) {
      $timeout.cancel(vm.typing);
    }
    // Wait and then make the request
    vm.typing = $timeout(function () {
      MassUpdateUsersService.updateUserDetails()
      .catch(function () {
        $scope.$broadcast('show-modal', 'branch-settings-update-failed', true);
      });
      vm.typing = null;
    }, REQUEST_DELAY.ms);
  }, true);

  /**
   * Retrieve branches when loading the type/branch view
   */
  $scope.$watchCollection(function () {
    return vm.selectedDetailsTabMassUsers;
  }, function (newVal) {
    if (angular.isUndefined(newVal)) {
      return;
    }
    // Get branches
    if (newVal === 'userTypeBranch') {
      MassUpdateUsersService.getBranches()
      .catch(function () {
        $scope.$broadcast('show-modal', 'retrieve-branch-failure', true);
      });
    }
  });
}]);