'use strict';

var app = angular.module('frontendApp');

/**
 * Mass update users
 */
app.directive('massUpdateUsers', [function () {
  return {
    templateUrl: '/components/admin/users/client/directives/mass-update-users/directives/partials/mass-update.html',
    controller: 'MassUpdateUsersCtrl',
    controllerAs: 'massUpdateCtrl'
  };
}]);