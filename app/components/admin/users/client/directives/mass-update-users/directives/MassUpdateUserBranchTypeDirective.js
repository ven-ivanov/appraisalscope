'use strict';

var app = angular.module('frontendApp');

app.directive('massUpdateUserBranchType', [function () {
  return {
    templateUrl: '/components/admin/users/client/directives/mass-update-users/directives/partials/type-branch.html'
  };
}]);