describe('MassUpdateUsersService', function () {
  var scope, Service, httpBackend, adminUsersService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function ($rootScope, MassUpdateUsersService, $httpBackend, AdminUsersService) {
    scope = $rootScope.$new();
    Service = MassUpdateUsersService;
    httpBackend = $httpBackend;
    adminUsersService = AdminUsersService;
  }));

  describe('prepareData', function () {
    describe('with error', function () {
      it('should throw an error if the user IDs are missing', function () {
        try {
          Service.prepareData();
        } catch (e) {
          expect(e.message).toEqual('User IDs or branch ID not defined');
        }
      });
      it('should throw an error if branch ID is missing', function () {
        try {
          Service.prepareData([1,2]);
        } catch (e) {
          expect(e.message).toEqual('User IDs or branch ID not defined');
        }
      });
    });
    beforeEach(function () {
      spyOn(adminUsersService, 'getStates').and.callThrough();
      Service.prepareData([1,2], 3);
      scope.$digest();
    });

    it('should create the current record', function () {
      expect(Service.record).toEqual();
    });
    it('should leave the AdminUsersService record along', function () {
      expect(adminUsersService.record).toEqual({ id: 1, _type: 'company', massUpdateUsers: [ 1, 2 ], branchId: 3 });
    });
    it('should call getStates', function () {
      expect(adminUsersService.getStates).toHaveBeenCalled();
    });
  });
});