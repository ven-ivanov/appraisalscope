'use strict';

var app = angular.module('frontendApp');

app.factory('MassUpdateUsersService',
['AdminUsersService', 'AdminUsersResourceService', 'AsDataService', function (AdminUsersService, AdminUsersResourceService, AsDataService) {
  var Service = {
    // Safe
    safeData: {
      // States for dropdown
      states: [],
      // Branches for type/branch
      branches: {}
    },
    // Display
    displayData: {
      type: 'mass-user',
      // User details
      details: {},
      // Branches for display
      branches: [],
      // User types
      userTypes: [{id: 'processor', type: 'Processor'}, {id: 'manager', type: 'Manager'}, {id: 'loan-officer', type: 'Loan officer'}]
    },
    /**
     * Request (either company, user, or branch)
     */
    request: function (requestFn, type, params) {
      // Variable argument length
      if (arguments.length === 2) {
        params = type;
        type = 'company';
      }
      // Get $resource configuration
      return AdminUsersResourceService.massUpdateUsers[requestFn](type, params);
    },
    /**
     * Prepare to update multiple users
     */
    massUpdateUsers: function (users, branch) {
      Service.prepareData(users, branch);
    },
    /**
     * Keep old record. Keep user IDs on current record
     * @param users Array User IDs
     * @param branchId int Branch ID
     */
    prepareData: function (users, branchId) {
      if (angular.isUndefined(users) || angular.isUndefined(branchId)) {
        throw Error('User IDs or branch ID not defined');
      }
      // Keep reference to the old branch
      AdminUsersService._record = angular.copy(AdminUsersService.record);
      // Place onto record
      AdminUsersService.record.massUpdateUsers = users;
      AdminUsersService.record.branchId = branchId;
      // Get states
      // @todo Combine all of these getStates calls into one and just reference it on AdminUsersService
      AdminUsersService.getStates().then(function (response) {
        // Set states
        Service.displayData.states = response.data;
      });
    },
    /**
     * Mass update user details
     */
    updateUserDetails: function () {
      return Service.request('updateDetails', {
        body: Service.displayData.details
      });
    },
    /**
     * Retrieve branches
     */
    getBranches: function () {
      return Service.request('getBranches')
      .then(function (response) {
        // Create data
        Service.hashData(response.data, 'branches');
        Service.formatData('branches');
      });
    }
  };
  // Inherit
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service);
  Service.transformData = AsDataService.transformData.bind(Service);

  return Service;
}]);