describe('AdminUsersBranchSettingsService', function () {
  var httpBackend, Service, scope, adminUsersService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function ($rootScope, AdminUsersBranchSettingsService, $httpBackend, AdminUsersService) {
    scope = $rootScope.$new();
    Service = AdminUsersBranchSettingsService;
    httpBackend = $httpBackend;
    adminUsersService = AdminUsersService;

    adminUsersService.record = {id: 1};
    Service.safeData = {branch: 1};

    httpBackend.whenGET(/branches/).respond({data: [1,2,3]});
  }));

  describe('getBranch', function () {
    beforeEach(function () {
      spyOn(Service, 'hashBranch');
      Service.getBranch();
      httpBackend.flush();
      scope.$digest();
    });

    it('should call hashBranch', function () {
      expect(Service.hashBranch).toHaveBeenCalled();
    });
  });

  describe('hashBranch', function () {
    beforeEach(function () {
      spyOn(Service, 'modifyBranchForPersistence');
      spyOn(Service, 'hashData');
      spyOn(adminUsersService, 'getStates').and.callThrough();
      Service.hashBranch({id: 2});
    });

    it('should store a reference to the previous record', function () {
      expect(adminUsersService._record).toEqual({ id: 1 });
    });
    it('should place the new record on admin users service', function () {
      expect(adminUsersService.record).toEqual({ id: 2, _type: 'branch' });
    });
    it('should call modifyBranchForPersistence', function () {
      expect(Service.modifyBranchForPersistence).toHaveBeenCalled();
    });
    it('should call hashData', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should set display data for the branch', function () {
      expect(Service.displayData.branch).toEqual(1);
    });
    it('should call getStates', function () {
      expect(adminUsersService.getStates).toHaveBeenCalled();
    });
  });

  describe('modifyBranchForPersistence', function () {
    var branch;
    beforeEach(function () {
      branch = {id: 1, employees: [1,2], isActive: true, test: 'test', location: {state: {code: 'AK'}}};
      Service.modifyBranchForPersistence(branch);
    });
    it('should remove id', function () {
      expect(branch.id).toBeUndefined();
    });
    it('should remove employees', function () {
      expect(branch.employees).toBeUndefined();
    });
    it('should remove isActive', function () {
      expect(branch.isActive).toBeUndefined();
    });
  });

  xdescribe('getBranchSettings', function () {

  });

  xdescribe('updatePaymentMethod', function () {

  });
});