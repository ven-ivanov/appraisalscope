'use strict';

var app = angular.module('frontendApp');

/**
 * Branch settings controller
 */
app.controller('AdminUsersBranchSettingsCtrl',
['$scope', '$timeout', 'REQUEST_DELAY', 'AdminUsersClientService', 'AdminUsersBranchSettingsService',
function ($scope, $timeout, REQUEST_DELAY, AdminUsersClientService, AdminUsersBranchSettingsService) {
  var vm = this;

  // Keep reference
  vm.displayData = AdminUsersBranchSettingsService.displayData;
  vm.safeData = AdminUsersBranchSettingsService.safeData;

  // For changing branch details
  vm.branchDetails = {};
  // Tab config
  vm.detailsTabsConfigBranch = {
    details: 'Details',
    clientOnReport: 'Client On Report',
    appraiserPanel: 'Appraiser Panel',
    addChecklist: 'Add Checklist',
    branchSettings: 'Branch Settings',
    instructions: 'Instructions',
    feeSchedule: 'Fee Schedule'
  };
  // Start on details
  vm.selectedDetailsTabBranch = 'details';
  // Tab config (need this to an array since objects don't guarantee order)
  vm.detailsTabsBranch =
  ['details', 'clientOnReport', 'appraiserPanel', 'addChecklist', 'branchSettings', 'instructions', 'feeSchedule'];
  // Keep reference to details on service
  vm.branchDetails = AdminUsersClientService.details;

  /**
   * Update branch details
   */
  $scope.$watch(function () {
    return vm.displayData.branch;
  }, function (newVal, oldVal) {
    if (angular.isUndefined(newVal) || !Object.keys(oldVal).length) {
      return;
    }
    if (vm.changingDetails) {
      $timeout.cancel(vm.changingDetails);
    }
    // Wait for the predefined amount, then make request to update details
    vm.changingDetails = $timeout(function () {
      // Make request
      AdminUsersBranchSettingsService.updateBranch(newVal);
      vm.changingDetails = null;
    }, REQUEST_DELAY.ms);
  }, true);

  /**
   * Load branch settings when that view is displayed
   */
  $scope.$watch(function () {
    return vm.selectedDetailsTabBranch;
  }, function (newVal) {
    // Load settings
    if (newVal === 'branchSettings') {
      AdminUsersBranchSettingsService.getBranchSettings()
      .catch(function () {
        $scope.$broadcast('show-modal', 'branch-settings-failed', true);
      });
    }
  });

  /**
   * Enable/disable payment method
   */
  vm.updatePaymentMethod = AdminUsersBranchSettingsService.updatePaymentMethod;

  /**
   * Watch for changes and write to the backend
   */
  $scope.$watch(function () {
    return vm.safeData.branchSettings;
  }, function (newVal, oldVal) {
    if (angular.isUndefined(newVal) || !Object.keys(oldVal).length) {
      return;
    }
    AdminUsersBranchSettingsService.updateBranchSettings()
    .catch(function () {
      $scope.$broadcast('show-modal', 'branch-settings-update-failed', true);
    });
  }, true);
}]);