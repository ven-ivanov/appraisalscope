'use strict';

var app = angular.module('frontendApp');

/**
 * Branch settings
 */
app.directive('adminUsersBranchSettings', [function () {
  return {
    templateUrl: '/components/admin/users/client/directives/branch-settings/directives/partials/branch-settings.html',
    controller: 'AdminUsersBranchSettingsCtrl',
    controllerAs: 'branchSettingsCtrl'
  };
}]);