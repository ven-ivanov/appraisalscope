'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersClientBranchDetails', [function () {
  return {
    templateUrl: '/components/admin/users/client/directives/branch-settings/directives/partials/branch-details.html'
  };
}]);