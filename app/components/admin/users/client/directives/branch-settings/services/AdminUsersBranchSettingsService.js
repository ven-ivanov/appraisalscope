'use strict';

var app = angular.module('frontendApp');

/**
 * Branch settings service
 *
 * @todo Branch settings are still waiting on the backend. 8/9
 */
app.factory('AdminUsersBranchSettingsService',
['$resource', 'API_PREFIX', 'AdminUsersService', 'AdminUsersResourceService', 'AsDataService',
function ($resource, API_PREFIX, AdminUsersService, AdminUsersResourceService, AsDataService) {
  var Service = {
    // ID of the branch we're currently on
    branchId: 0,
    // Safe data
    safeData: {
      branch: {},
      // Branch settings
      branchSettings: {}
    },
    // Display data
    displayData: {
      // Branch data
      branch: {},
      // Type
      type: 'branch',
      // States
      states: [],
      // Branch settings
      branchSettings: {
        paymentForm: [{id: true, name: 'Branch'}, {id: false, name: 'No Payment'}],
        // payment methods
        paymentMethods: [
          {id: 1, name: 'Credit card'},
          {id: 2, name: 'Bank account'},
          {id: 3, name: 'Send payment request to your customer'},
          {id: 4, name: 'Bill me'},
          {id: 5, name: 'Split payment'},
          {id: 6, name: 'Invoice partial payment'}
        ],
        // CC settings
        ccSettings: [{id: 1, name: 'Authorize only'}, {id: 2, name: 'Capture'}, {id: 3, name: 'Save payment to capture later'}]
      },
      // Yes/no
      yesNoDropdown: AdminUsersService.yesNoDropdown
    },
    /**
     * Request (either company, user, or branch)
     */
    request: function (requestFn, type, params) {
      // Variable argument length
      if (arguments.length === 2) {
        params = type;
        type = 'branch';
      }
      // Get $resource configuration
      return AdminUsersResourceService.branchSettings[requestFn](type, params);
    },
    /**
     * Retrieve branch for settings
     */
    getBranch: function (branchId) {
      return Service.request('getBranch', {branchId: branchId})
      .then(function (response) {
        // Hash
        Service.hashBranch(response);
      });
    },
    /**
     * Hash branch data
     */
    hashBranch: function (data) {
      // Keep reference to the old branch
      AdminUsersService._record = angular.copy(AdminUsersService.record);
      // Store branch on admin users service for access to other parts
      AdminUsersService.record = angular.copy(data);
      AdminUsersService.record._type = 'branch';
      // Make into persistable object
      Service.modifyBranchForPersistence(data);
      // Hash
      Service.hashData(data, 'branch');
      // Format
      Service.displayData.branch = angular.copy(Service.safeData.branch);
      // Get states
      AdminUsersService.getStates()
      .then(function (response) {
        // Set states
        Service.displayData.states = response.data;
      });
    },
    /**
     * Modify branch record so it is in accordance with Standard Branch Persistable Object
     * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Branches#standard-branch-persistable-object
     */
    modifyBranchForPersistence: function (branch) {
      delete branch.id;
      delete branch.employees;
      delete branch.isActive;
      branch.location.state = branch.location.state.code;
    },
    /**
     * Update branch data
     * @param data Array
     */
    updateBranch: function (data) {
      return Service.request('updateBranch', data);
    },
    /**
     * Retrieve branch settings
     */
    getBranchSettings: function () {
      return Service.request('branchSettings')
      .then(function (response) {
        Service.hashData(response, 'branchSettings');
      });
    },
    /**
     * Update payment methods
     */
    updatePaymentMethod: function (id) {
      var index = Service.safeData.branchSettings.payment.methods.indexOf(id);
      // Add
      if (index === -1) {
        Service.safeData.branchSettings.payment.methods.push(id);
        // Remove
      } else {
        Service.safeData.branchSettings.payment.methods.splice(index, 1);
      }
    },
    /**
     * Update branch settings
     */
    updateBranchSettings: function () {
      return Service.request('updateBranchSettings', Service.safeData.branchSettings);
    }
  };

  // Hash data
  Service.hashData = AsDataService.hashData.bind(Service);
  // Format
  Service.formatData = AsDataService.formatData.bind(Service);
  // Transform
  Service.transformData = AsDataService.transformData.bind(Service);
  return Service;
}]);