'use strict';

var app = angular.module('frontendApp');

/**
 * User settings directive
 */
app.directive('adminUsersUserSettingsSummary', ['AdminUsersDirectiveInheritanceService', function (AdminUsersDirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/client/directives/user-settings-summary/directives/partials/user-settings.html',
    controller: 'UserSettingsCtrl',
    controllerAs: 'userSettingsCtrl',
    link: function (scope) {
      AdminUsersDirectiveInheritanceService.inherit.call(scope);
    }
  };
}]);