'use strict';

var app = angular.module('frontendApp');

/**
 * Service for user settings
 */
app.factory('AdminUsersUserSettingsSummaryService',
['$q', 'AdminUsersResourceService', 'AdminUsersService', 'AsDataService',
function ($q, AdminUsersResourceService, AdminUsersService, AsDataService) {
  var Service = {
    // Safe
    safeData: {
      // User instruction documents
      documents: {},
      // User instructions
      instructions: {},
      // Approved appraisers
      approvedAppraisers: {},
      // Do not use appraisers
      doNotUseAppraisers: {},
      // Fee schedule (really, jobtypes list)
      jobTypes: {},
      // Checklist
      checklist: {},
      // Clients on report
      clientsOnReport: {},
      // Settings
      settings: {},
      // Payment
      payment: {}
    },
    // Display data
    displayData: {
      documents: [],
      instructions: [],
      approvedAppraisers: [],
      doNotUseAppraisers: [],
      jobTypes: [],
      checklist: [],
      clientsOnReport: [],
      payment: [],
      settings: [],
      // Payment options configuration
      paymentOptionsConfig: {
        bankAccount: 'Bank Account',
        cc: {
          authorizeOnly: 'Authorize Only', capture: 'Capture'
        },
        billMe: 'Bill Me',
        invoicePartial: 'Invoice Partial Payment',
        splitPayment: 'Split Payment',
        showCapture: 'Show Capture on Client',
        sendInvoice: 'Send Invoice to Your Customer'
      }
    },
    /**
     * Retrieve all settings for the current user
     */
    getUserSettingsSummary: function (user) {
      return $q.all([
        // Get instructions
        Service.getInstructions(user),
        // Get appraisers panel
        Service.getAppraisersLists(user),
        // Get fee schedule
        Service.getJobTypes(user),
        // Get checklist
        Service.getChecklist(user),
        // Get clients on report
        Service.getClientsOnReport(user),
        // Get payment options
        Service.getUserSettings(user)
      ]);
    },
    /**
     * Hash instructions
     */
    getInstructions: function (user) {
      return Service.request('userSettingsInstructions', user)
      .then(function (response) {
        // Hash
        Service.hashData(response.data, 'instructions');
        Service.formatData('instructions');
        // Extract documents
        Service.extractDocuments();
      });
    },
    /**
     * Hash appraisers
     */
    getAppraisersLists: function (user) {
      // Approved
      return Service.request('userSettingsApprovedAppraisers', user)
      .then(function (response) {
        // Hash
        Service.hashData(response.data, 'approvedAppraisers');
        // Display
        Service.formatData('approvedAppraisers');
      })
      .then(function () {
        return Service.request('userSettingsDoNotUseAppraisers', user).then(function (response) {
          // Hash
          Service.hashData(response.data, 'doNotUseAppraisers');
          // Display
          Service.formatData('doNotUseAppraisers');
        });
      });
    },
    /**
     * Hash job types
     */
    getJobTypes: function (user) {
      // Hash and format
      return Service.request('userSettingsJobTypes', user).then(function (response) {
        // Hash
        Service.hashData(response.jobTypes, 'jobTypes');
        // Display
        Service.formatData('jobTypes');
      });
    },
    /**
     * Get checklist data for table
     */
    getChecklist: function (user) {
      // Retrieve assigned checklist ID
      return Service.request('userSettingsChecklist', user)
      .then(function (response) {
        // Retrieve checklist questions for assigned checklist
        return Service.request('userSettingsChecklistQuestions', {id: response.id}).then(function (response) {
          // Hash and format questions
          Service.hashData(response.data, 'checklist');
          Service.formatData('checklist');
        });
      });
    },
    /**
     * Get settings to create payment settings display
     *
     * @todo Waiting on the backend to complete the payment section. Still waiting 8/4
     * @link https://github.com/ascope/manuals/issues/57
     * @param user
     */
    getUserSettings: function (user) {
      Service.request('userSettingsSettings', user)
      .then(function (response) {
        Service.hashData(response, 'settings');
        Service.formatPlainObjectData('settings');
        // Payment display info
        Service.createPaymentDisplay();
      });
    },
    /**
     * Create payment display text
     */
    createPaymentDisplay: function () {
      // payment for display
      var payment = Service.displayData.settings.payment;
      payment.sourceText = 'No payment options applied';
      // Payment source
      if (payment.source !== 'no-payment') {
        switch (payment.source) {
          case 'company':
            payment.sourceText = 'Payment options applied from company (' + AdminUsersService.record.company.name + ')';
            break;
          case 'branch':
            payment.sourceText = 'Payment options applied from branch (' + AdminUsersService.record.branch.name + ')';
            break;
          case 'individual':
            payment.sourceText = 'Payment options applied from individual';
            break;
        }
      }
    },
    /**
     * Get client on report table data=
     */
    getClientsOnReport: function (user) {
      return Service.request('userSettingsClientsOnReport', user).then(function (response) {
        // hash initial data
        Service.hashData(response.data, 'clientsOnReport', 'companyId');
        // Get all companies
        var clientCompanies = Service.getClientCompanies();
        // Keep only the companies listed on report
        Lazy(clientCompanies)
        .each(function (company) {
          if (Service.safeData.clientsOnReport[company.id]) {
            Service.safeData.clientsOnReport[company.id] = company;
          }
        });
        // Format for display
        Service.formatData('clientsOnReport');
      });
    }
  };

  // Request
  Service.request = AdminUsersService.request.bind(Service, 'client');
  // Hash
  Service.hashData = AsDataService.hashData.bind(Service);
  // Format for display
  Service.formatData = AsDataService.formatData.bind(Service);
  Service.formatPlainObjectData = AsDataService.formatPlainObjectData.bind(Service);
  Service.transformData = AsDataService.transformData.bind(Service);
  // Extract instruction documents
  Service.extractDocuments = AdminUsersService.extractDocuments.bind(Service);
  // Get client companies
  Service.getClientCompanies = AdminUsersService.getClientCompanies.bind(Service);
  return Service;
}]);