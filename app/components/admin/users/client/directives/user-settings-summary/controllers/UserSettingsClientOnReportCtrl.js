'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - User Settings - Checklist controller
 */
app.controller('UserSettingsClientOnReportCtrl',
['$scope', 'AdminUsersUserSettingsSummaryService',
 function ($scope, AdminUsersUserSettingsSummaryService) {
   var vm = this;
   // Heading
   vm.heading = [
     // title
     {
       label: 'Client',
       data: 'name',
       search: true,
       noSort: true
     }
   ];

   /**
    * Watch user documents and display table when we have the documents
    */
   $scope.$watchCollection(function () {
     return AdminUsersUserSettingsSummaryService.displayData.clientsOnReport;
   }, function (newVal) {
     if (!angular.isArray(newVal)) {
       return;
     }
     // Set table data
     vm.tableData = newVal;
     vm.rowData = newVal.slice();
   });
 }]);