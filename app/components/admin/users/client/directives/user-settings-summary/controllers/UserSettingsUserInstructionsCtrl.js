'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - User Settings - Documents table controller
 */
app.controller('UserSettingsUserInstructionsCtrl',
['$scope', 'AdminUsersUserSettingsSummaryService', function ($scope, AdminUsersUserSettingsSummaryService) {
  var vm = this;
  // Heading
  vm.heading = [
    // Title
    {
      label: 'title',
      data: 'title',
      search: true,
      noSort: true
    }
  ];

  /**
   * Watch user documents and display table when we have the documents
   */
  $scope.$watchCollection(function () {
    return AdminUsersUserSettingsSummaryService.displayData.instructions;
  }, function (newVal) {
    if (!angular.isArray(newVal)) {
      return;
    }
    // Set table data
    vm.tableData = newVal;
    vm.rowData = newVal.slice();
  });
}]);