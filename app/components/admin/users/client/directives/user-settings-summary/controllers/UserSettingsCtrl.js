'use strict';

var app = angular.module('frontendApp');

app.controller('UserSettingsCtrl', ['$scope', 'AdminUsersUserSettingsSummaryService',
function ($scope, AdminUsersUserSettingsSummaryService) {
  var vm = this;
  // Start on instruction tab
  vm.tab = 'instruction';
  // Tab config
  vm.tabs = ['instruction', 'appraiserPanel', 'feeSchedule', 'checklist', 'clientsOnReport', 'paymentOptions'];
  vm.tabConfig = {
    instruction: 'Instructions',
    appraiserPanel: 'Appraisers Panel',
    feeSchedule: 'Fee Schedule',
    checklist: 'Checklist',
    clientsOnReport: 'Clients on Report',
    paymentOptions: 'Payment Options'
  };
  // Change tab in the settings modal
  vm.changeSettingsTab = function (tab) {
    vm.tab = tab;
  };
  // Payment options configuration
  vm.displayData = AdminUsersUserSettingsSummaryService.displayData;
}]);