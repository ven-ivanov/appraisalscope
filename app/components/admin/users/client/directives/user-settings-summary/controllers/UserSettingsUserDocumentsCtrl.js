'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - User Settings - Documents table controller
 */
app.controller('UserSettingsInstructionsDocumentsCtrl',
['$scope', 'AdminUsersUserSettingsSummaryService', function ($scope, AdminUsersUserSettingsSummaryService) {
  var vm = this;
  // Heading
  vm.heading = [
    // Document
    {
      label: 'Title',
      data: 'title',
      search: true,
      noSort: true
    }
  ];

  /**
   * Watch user documents and display table when we have the documents
   */
  $scope.$watchCollection(function () {
    return AdminUsersUserSettingsSummaryService.displayData.documents;
  }, function (newVal) {
    if (!angular.isArray(newVal)) {
      return;
    }
    // Set table data
    vm.tableData = newVal;
    vm.rowData = newVal.slice();
  });

  /**
   * Download document on click
   * @param id
   */
  vm.rowFn = function (id) {
    // Get link
    var downloadLink = AdminUsersUserSettingsSummaryService.safeData.documents[id].url;
    // Trigger download
    if (angular.isDefined(downloadLink)) {
      $scope.triggerDownload(downloadLink);
    } else {
      // Display failure modal, but keep .modal-open class on body
      $scope.$emit('show-modal', 'document-download-failure', true);
      $scope.scrollTop('#user-details');
    }
  };
}]);