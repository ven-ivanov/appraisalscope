'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - User Settings - Fee schedule table ctrl
 */
app.controller('UserSettingsFeeScheduleCtrl',
['$scope', 'AdminUsersUserSettingsSummaryService', 'AsTableService',
 function ($scope, AdminUsersUserSettingsSummaryService, AsTableService) {
   var vm = this;
   // Heading
   vm.heading = [
     // Job Title
     {
       label: 'Label',
       data: 'title',
       search: true,
       noSort: true
     },
     // FHA
     {
       label: 'FHA',
       data: 'isFha',
       isStatic: false,
       search: true,
       filter: 'boolYesNo',
       noSort: true
     }
   ];

   /**
    * Watch user documents and display table when we have the documents
    */
   $scope.$watchCollection(function () {
     return AdminUsersUserSettingsSummaryService.displayData.jobTypes;
   }, function (newVal) {
     if (!angular.isArray(newVal)) {
       return;
     }
     // Apply filters
     AsTableService.applyFilters(newVal, vm.heading)
     .then(function () {
       // Set table data
       vm.tableData = newVal;
       vm.rowData = newVal.slice();
     });
   });
 }]);