var AdminUsersUserSettingsServiceMock = function ($http) {
  return {
    getUserDocuments: function (id) {
      var that = this;
      $http.get('/v2.0/admin/users/client/user-documents/' + id).then(function (data) {
        that.documents = data.data;
      });
    },
    paymentOptionsConfig: {
      bankAccount: 'Bank Account',
      cc: {
        authorizeOnly: 'Authorize Only', capture: 'Capture'
      },
      billMe: 'Bill Me',
      invoicePartial: 'Invoice Partial Payment',
      splitPayment: 'Split Payment',
      showCapture: 'Show Capture on Client',
      sendInvoice: 'Send Invoice to Your Customer'
    },
    /**
     * Exact copy of real method
     * @param newVal
     */
    formatCcDisplay: function (newVal) {
      var vm = this;
      return Object.keys(newVal.cc)
        .join(', ')
        .replace('authorizeOnly', vm.paymentOptions.cc.authorizeOnly ? vm.paymentOptionsConfig.cc.authorizeOnly : '')
        .replace('capture', vm.paymentOptions.cc.capture ? vm.paymentOptionsConfig.cc.capture: '')
        .replace(/,\s+?/, '');
    }
  };
};