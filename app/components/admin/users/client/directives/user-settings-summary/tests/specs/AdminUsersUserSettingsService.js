describe('AdminUsersUserSettingsSummaryService', function () {
  var Service, adminUsersService, scope, httpBackend;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function (AdminUsersService, $rootScope, $httpBackend, AdminUsersUserSettingsSummaryService) {
    scope = $rootScope.$new();
    adminUsersService = AdminUsersService;
    httpBackend = $httpBackend;
    Service = AdminUsersUserSettingsSummaryService;

    spyOn(Service, 'hashData');
    spyOn(Service, 'formatData');
    spyOn(Service, 'formatPlainObjectData');

    httpBackend.whenGET(/instructions/).respond({data: [1,2,3]});
    httpBackend.whenGET(/appraiser-lists/).respond({data: [1,2,3]});
    httpBackend.whenGET(/1\/checklist/).respond({id: 2});
    httpBackend.whenGET(/2\/questions/).respond({data: [1,2,3]});
    httpBackend.whenGET(/settings/).respond({});
  }));

  describe('getUserSettingsSummary', function () {
    beforeEach(function () {
      spyOn(Service, 'getInstructions');
      spyOn(Service, 'getAppraisersLists');
      spyOn(Service, 'getJobTypes');
      spyOn(Service, 'getChecklist');
      spyOn(Service, 'getUserSettings');
      spyOn(Service, 'getClientsOnReport');
      Service.getUserSettingsSummary();
      scope.$digest();
    });

    it('should get instructions', function () {
      expect(Service.getInstructions).toHaveBeenCalled();
    });
    it('should get appraisers lists', function () {
      expect(Service.getAppraisersLists).toHaveBeenCalled();
    });
    it('should get job types', function () {
      expect(Service.getJobTypes).toHaveBeenCalled();
    });
    it('should get checklist', function () {
      expect(Service.getChecklist).toHaveBeenCalled();
    });
    it('should get user settings', function () {
      expect(Service.getUserSettings).toHaveBeenCalled();
    });
    it('should get clients on report', function () {
      expect(Service.getClientsOnReport).toHaveBeenCalled();
    });
  });

  describe('getInstructions', function () {
    beforeEach(function () {
      spyOn(Service, 'extractDocuments');
      Service.getInstructions();
      httpBackend.flush();
      scope.$digest();
    });

    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should format data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
    it('should call extract documents', function () {
      expect(Service.extractDocuments).toHaveBeenCalled();
    });
  });

  describe('getAppraisersLists', function () {
    beforeEach(function () {
      Service.getAppraisersLists();
      httpBackend.flush();
      scope.$digest();
    });

    it('should hash both', function () {
      expect(Service.hashData.calls.count()).toEqual(2);
    });
    it('should format both', function () {
      expect(Service.formatData.calls.count()).toEqual(2);
    });
  });

  describe('getJobTypes', function () {

  });

  describe('getChecklist', function () {
    beforeEach(function () {
      spyOn(Service, 'request').and.callThrough();
      Service.getChecklist();
      httpBackend.flush();
      scope.$digest();
    });

    it('should retrieve the assigned checklist ID', function () {
      expect(Service.request.calls.argsFor(1)).toEqual([ 'userSettingsChecklistQuestions', { id: 2 } ]);
    });
    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should format data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('getUserSettings', function () {
    beforeEach(function () {
      spyOn(Service, 'createPaymentDisplay');
      Service.getUserSettings();
      httpBackend.flush();
      scope.$digest();
    });

    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should format plain object data', function () {
      expect(Service.formatPlainObjectData).toHaveBeenCalled();
    });
    it('should create payment display', function () {
      expect(Service.createPaymentDisplay).toHaveBeenCalled();
    });
  });

  describe('createPaymentDisplay', function () {

  });
});