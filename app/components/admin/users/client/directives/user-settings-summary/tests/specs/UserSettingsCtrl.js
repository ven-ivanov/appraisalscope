'use strict';

describe('UserSettingsCtrl', function () {
  var scope, controller, adminUsersUserSettingsService, httpBackend, adminUsersClientService;

  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('DomLoadService', DomLoadServiceMock);
      $provide.factory('AdminUsersUserSettingsSummaryService', AdminUsersUserSettingsServiceMock);
      $provide.factory('AdminUsersClientService', AdminUsersClientServiceMock);
    });
  });
  beforeEach(inject(function ($rootScope, $controller, AdminUsersUserSettingsSummaryService, $httpBackend, AdminUsersClientService) {
    scope = $rootScope.$new();
    adminUsersUserSettingsService = AdminUsersUserSettingsSummaryService;
    adminUsersClientService = AdminUsersClientService;
    httpBackend = $httpBackend;
    controller = $controller('UserSettingsCtrl', {$scope: scope});
    scope.$digest();
  }));

  // Fake payment options for this user
  var paymentOptions = {
    bankAccount: 1,
    cc: {
      authorizeOnly: 1,
      capture: 0
    },
    billMe: 0,
    invoicePartial: 1,
    splitPayment: 1,
    showCapture: 0,
    sendInvoice: 1
  };
});