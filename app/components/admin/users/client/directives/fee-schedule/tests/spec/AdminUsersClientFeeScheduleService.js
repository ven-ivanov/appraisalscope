'use strict';

describe('AdminUsersClientFeeScheduleService', function () {
  var adminUsersService, Service, scope, httpBackend, feeScheduleData, heading;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function (AdminUsersService, $rootScope, $httpBackend, AdminUsersClientFeeScheduleService) {
    adminUsersService = AdminUsersService;
    Service = AdminUsersClientFeeScheduleService;
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    // Httpbackend
    httpBackend.whenGET(/v2.0\/job-type-lists/).respond({
      data: [{
               id: 1,
               title: 'Job Type 1',
               jobTypes: [1, 2, 3]
             }, {
               id: 2,
               title: 'Job Type 2',
               jobTypes: [4, 5, 6]
             }]
    });
    httpBackend.whenGET(/jobtypes-list/).respond({id: 3, fha: true, jobTypes: [7,8,9]});
    httpBackend.whenPUT(/jobtypes-list/).respond();
    // Spy on hash data
    spyOn(Service, 'hashData').and.callThrough();
    spyOn(Service, 'formatData');
  }));

  describe('getJobList', function () {
    beforeEach(function () {
      Service.getJobList();
      httpBackend.flush();
      scope.$digest();
    });

    it('should call hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should store the data in safe data', function () {
      expect(Service.safeData.jobType[1]).toEqual({ id: 1, title: 'Job Type 1', jobTypes: [ 1, 2, 3 ] });
      expect(Service.safeData.jobType[2]).toEqual({ id: 2, title: 'Job Type 2', jobTypes: [ 4, 5, 6 ] });
    });
    it('should attach the assigned display', function () {
      expect(Service.safeData.jobType[3].jobTypes).toEqual([7,8,9]);
    });
    it('should create data for display', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
    it('should set selectedJobList', function () {
      expect(Service.displayData.selectedJobList).toEqual(3);
    });
  });

  describe('getJobDetails', function () {
    beforeEach(function () {
      Service.safeData.jobType = {1: {jobTypes: 'blah'}, 2: {yup: 'nope'}};
      Service.getJobDetails(1);
    });
    it('should have hashed the data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should have created the table data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('assignJobList', function () {
    describe('mass update', function () {
      beforeEach(function () {
        adminUsersService.record.massUpdateUsers = [1,2];
        Service.displayData.selectedJobList = 1;
        spyOn(Service, 'request').and.callThrough();
        Service.assignJobList();
        httpBackend.flush();
        scope.$digest();
      });

      it('should make a request with the correct body and headers', function () {
        expect(Service.request).toHaveBeenCalledWith('assignMass', { body: { id: 1 } });
      });
    });
    describe('single update', function () {
      beforeEach(function () {
        Service.displayData.selectedJobList = 1;
        spyOn(Service, 'request').and.callThrough();
        Service.assignJobList();
        httpBackend.flush();
        scope.$digest();
      });

      it('should make a request with the correct body and headers', function () {
        expect(Service.request).toHaveBeenCalledWith( 'assign', { body: { id: 1 } });
      });
    });
  });
});