'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Client - Fee Schedule service
 *
 * @todo Waiting on backend
 * @link https://github.com/ascope/manuals/issues/188
 * @link https://github.com/ascope/manuals/issues/203
 * 8/7
 *
 * @todo Change so multiple lists can be assigned
 * @todo This will be broken until the backend is up to date
 */
app.factory('AdminUsersClientFeeScheduleService',
['AdminUsersResourceService', 'AdminUsersService', 'AsDataService', function (AdminUsersResourceService, AdminUsersService, AsDataService) {
  var Service = {
    // Safe data
    safeData: {
      jobType: {},
      jobDetails: {},
      // Assigned list
      assignedList: '',
      // Table heading
      heading: [
        // Job type
        {
          label: 'Job Title',
          data: 'label',
          search: true,
          noSort: true
        },
        // FHA
        {
          label: 'FHA',
          data: 'fha',
          filter: 'boolYesNo',
          search: true,
          noSort: true
        }
      ]
    },
    // Data for display
    displayData: {
      // Dropdown
      jobType: [],
      // Table
      jobDetails: [],
      // Selected job
      selectedJobList: 0
    },
    /**
     * Request types for this view
     * @param requestFn String Function called on AdminUsersResourceService
     * @param params Object Object of params
     */
    request: function (requestFn, params) {
      // Get $resource configuration
      return AdminUsersResourceService.jobTypesList[requestFn](AdminUsersService.record._type, params);
    },
    /**
     * Retrieve job list
     */
    getJobList: function () {
      return Service.request('getJobtypeLists')
      .then(function (response) {
        // Create hash
        Service.hashData(response.data, 'jobType');
        // Get assigned jobtype
        return Service.request('getAssignedJobtypeList');
      })
      .then(function (response) {
        // Don't continue if there's no assigned job list
        if (!Object.keys(response).length) {
          // Set for display
          Service.formatData('jobType');
          return;
        }
        // Add to safe data list
        Service.safeData.jobType[response.id] = response;
        // Set for display
        Service.formatData('jobType');
        // Set as currently selected list
        Service.displayData.selectedJobList = response.id;
        // Store assigned
        Service.safeData.assignedList = response.id;
      });
    },
    /**
     *
     * @param jobId
     */
    getJobDetails: function (jobId) {
      // Hash the data
      Service.hashData(Service.safeData.jobType[jobId].jobTypes, 'jobDetails');
      // Format table data
      Service.formatData('jobDetails');
    },
    /**
     * Assign job list
     */
    assignJobList: function () {
      var request;
      // If doing a mass employee request
      if (AdminUsersService.record.massUpdateUsers) {
        request = Service.request('assignMass', {body: {id: Service.displayData.selectedJobList}});
        // Normal request
      } else {
        request = Service.request('assign', {body: {id: Service.displayData.selectedJobList}});
      }
      return request.then(function () {
        // Set assigned
        Service.safeData.assignedList = Service.displayData.selectedJobList;
      });
    }
  };
  // Inherited from AdminUsersService
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service);
  Service.transformData = AsDataService.transformData.bind(Service);

  return Service;
}]);