'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Client - Company - Fee Schedule
 */
app.directive('adminUsersClientFeeSchedule',
['AdminUsersDirectiveInheritanceService', function (AdminUsersDirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/client/directives/fee-schedule/directives/partials/fee-schedule.html',
    controller: 'AdminUsersClientFeeScheduleCtrl',
    controllerAs: 'feeScheduleCtrl',
    link: function (scope) {
      AdminUsersDirectiveInheritanceService.inherit.call(scope);
    }
  };
}]);