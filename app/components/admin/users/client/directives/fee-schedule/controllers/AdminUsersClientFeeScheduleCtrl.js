'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Client - Fee Schedule controller
 */
app.controller('AdminUsersClientFeeScheduleCtrl',
['$scope', 'AdminUsersClientFeeScheduleService',  function ($scope, AdminUsersClientFeeScheduleService) {
  var vm = this;
  // Keep reference to the display data
  vm.displayData = AdminUsersClientFeeScheduleService.displayData;
  vm.safeData = AdminUsersClientFeeScheduleService.safeData;
  var Service = AdminUsersClientFeeScheduleService;

  /**
   * Gather data on load
   */
  vm.init = function () {
    Service.getJobList();
  };

  /**
   * Watch for selected job type to change
   */
  $scope.$watch(function () {
    return Service.displayData.selectedJobList;
  }, function (newVal) {
    if (angular.isUndefined(newVal) || !newVal) {
      return;
    }
    // Retrieve job details
    Service.getJobDetails(newVal);
  });

  /**
   * Disable assign job list button
   */
  vm.assignJobListDisabled = function () {
    return !vm.displayData.selectedJobList || vm.safeData.assignedList === vm.displayData.selectedJobList;
  };

  /**
   * Confirm assign job list
   */
  vm.assignJobListConfirm = function () {
    $scope.$broadcast('show-modal', 'assign-job-list', true);
    $scope.scrollTop('#user-details');
  };
  /**
   * Assign job list
   */
  vm.assignJobList = function () {
    Service.assignJobList()
    .then(function () {
      $scope.$broadcast('hide-modal', 'assign-job-list');
      $scope.$broadcast('show-modal', 'assign-job-list-success', true);
      $scope.scrollTop('#user-details');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'assign-job-list-failure', true);
      $scope.scrollTop('#user-details');
    });
  };

  vm.init();
}]);