'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersClientFeeScheduleJobTable',
['$scope', 'AdminUsersClientFeeScheduleService', function ($scope, AdminUsersClientFeeScheduleService) {
  var vm = this;

  // Set up references
  vm.safeData = AdminUsersClientFeeScheduleService.safeData;
  vm.displayData = AdminUsersClientFeeScheduleService.displayData;
  // Keep heading
  vm.heading = angular.copy(vm.safeData.heading);

  /**
   * Create table data
   */
  $scope.$watchCollection(function () {
    return AdminUsersClientFeeScheduleService.displayData.jobDetails;
  }, function (newVal) {
    if (!angular.isArray(newVal) || !newVal) {
      return;
    }
    // Create table data
    vm.tableData = newVal;
    vm.rowData = vm.tableData.slice();
  });
}]);