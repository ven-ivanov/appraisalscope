'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersClientInstructions',
['TriggerDownloadService', 'AdminUsersDirectiveInheritanceService',
 function (TriggerDownloadService, AdminUsersDirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/client/directives/instructions/directives/partials/instructions.html',
    controller: 'AdminUsersClientInstructionsCtrl',
    controllerAs: 'instructionsCtrl',
    scope: {
      hide: '@'
    },
    link: function (scope, element, attrs) {
      // Expose trigger download
      TriggerDownloadService.init.call(scope);
      // Expose scrolltop
      AdminUsersDirectiveInheritanceService.inherit.call(scope);
      // Whether mass update of employees is taking place
      scope.massUpdate = scope.$eval(attrs.massUpdate);
      // Table columns to hide
      scope.hide = attrs.hide;
    }
  };
}]);