'use strict';

var app = angular.module('frontendApp');
/*******************************************************
 * Endpoints
 * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client#instructions
 *
 * @todo Many issues
 * @link https://github.com/ascope/manuals/issues/206
 ********************************************************/
app.factory('AdminUsersClientInstructionsService',
['AdminUsersClientService', 'AdminUsersService', 'AsDataService', '$q',
function (AdminUsersClientService, AdminUsersService, AsDataService, $q) {
  var Service = {
    // Safe data
    safeData: {
      // Documents data
      documents: {},
      // Instructions data
      instructions: {},
      // Selected documents
      selectedDocument: '',
      // Selected instruction
      selectedInstruction: '',
      // Instructions tags
      mergeFields: {},
      // Templates
      templates: {},
      // Job types
      jobTypes: {},
      // Hash of states by name for easy retrieval
      states: {},
      // Assignment data
      assign: {
        options: {
          states: [],
          documents: [],
          jobTypes: []
        },
        models: {
          states: [],
          documents: [],
          jobTypes: []
        },
        // Dropdown multiselect values
        settings: {
          states: {displayProp: 'name', idProp: 'code'},
          documents: {displayProp: 'title', idProp: 'id'},
          jobTypes: {displayProp: 'title', idProp: 'id'}
        }
      },
      // Columns for instructions
      columns: [
        // Instruction
        {
          label: 'Instruction',
          data: 'title',
          noSort: true
        },
        // Assign
        {
          id: 'assign',
          label: 'Assign',
          noSort: true,
          linkLabel: 'Assign',
          fn: 'assign'
        },
        // Edit
        {
          id: 'edit',
          label: 'Edit',
          noSort: true,
          linkLabel: 'Edit',
          fn: 'edit'
        },
        // Delete
        {
          id: 'delete',
          label: 'Delete',
          noSort: true,
          linkLabel: 'Delete',
          fn: 'delete'
        },
        {
          id: 'copy',
          label: 'Copy to selected users',
          linkLabel: 'Copy to selected users',
          fn: 'copy'
        }
      ],
      // Table heading
      headings: {
        // Documents
        documents: [
          // Document
          {
            label: 'Document',
            data: 'title',
            input: true,
            noSort: true
          },
          // Download
          {
            label: 'Download',
            noSort: true,
            linkLabel: 'Download',
            fn: 'download'
          },
          // Delete
          {
            label: 'Delete',
            noSort: true,
            linkLabel: 'Delete',
            fn: 'delete'
          }
        ]
      }
    },
    // Data for display
    displayData: {
      documents: [],
      // Instructions
      instructions: [],
      // Acceptance message
      acceptanceMessage: '',
      // New document title
      newDocumentName: '',
      // Merge fields
      mergeFields: [],
      // Templates
      templates: [],
      // Selected merge field
      selectedMergeField: '',
      // Selected template
      selectedTemplate: '',
      // Add/edit instruction modal
      instructionModal: {
        // modal title
        modalTitle: '',
        // Instruction for editing
        instruction: {},
        // Instruction template
        template: ''
      },
      // Job types
      jobTypes: []
    },
    /**
     * Retrieve acceptance message
     */
    getAcceptanceMessage: function () {
      return Service.request('acceptanceMessage')
      .then(function (response) {
        Service.displayData.acceptanceMessage = response.label;
      });
    },
    /**
     * Update acceptance message
     */
    updateAcceptanceMessage: function () {
      return Service.request('updateAcceptanceMessage', {label: Service.displayData.acceptanceMessage});
    },
    /**
     * Retrieve instructions
     */
    getInstructions: function () {
      return Service.request('getInstructions').then(function (response) {
        // hash and format for display
        Service.hashData(response.data, 'instructions');
        Service.formatData('instructions');
        // Extract documents for display in the table
        Service.extractDocuments();
        // Retrieve tags for adding to instructions
        Service.getMergeFields();
        // Retrieve existing instruction templates
        Service.getTemplates();
        // Get states
        Service.getStates();
        // Get jobtype lists
        Service.getJobtypeLists();
      });
    },
    /**
     * Retrieve tags to apply when adding/editing instructions
     */
    getMergeFields: function () {
      Service.request('getMergeFields')
      .then(function (response) {
        Service.hashData(response.data, 'mergeFields', 'placeholder');
        Service.formatData('mergeFields');
      });
    },
    /**
     * Instruction templates
     */
    getTemplates: function () {
      Service.request('templates')
      .then(function (response) {
        Service.hashData(response.data, 'templates');
        Service.formatData('templates');
      });
    },
    /**
     * Get jobtype lists
     * @todo Need some questions answered
     * @link https://github.com/ascope/manuals/issues/203
     */
    getJobtypeLists: function () {
      return Service.request('getJobtypeLists').then(function (response) {
        // Hash and format
        Service.hashData(response.data, 'jobTypes');
        Service.formatData('jobTypes');
        // Copy to multiselect dropdown options
        Service.safeData.assign.options.jobTypes = angular.copy(Service.displayData.jobTypes);
      });
    },
    /**
     * Expose states for assignment
     */
    getStates: function () {
      Service.safeData.assign.options.states = AdminUsersService.states;
      Service.hashData(AdminUsersService.states, 'states', 'code');
    },
    /**
     * Create table input data
     */
    initDocumentTableInputs: function () {
      var inputData = {};
      var data = Service.displayData.documents;
      // Populate object of input data
      Lazy(data)
      .each(function (item) {
        inputData[item.id] = item.title;
      });
      return inputData;
    },
    /**
     * Splice a merge field into the existing content
     */
    spliceInMergeField: function (field) {
      // Current instruction
      var instruction = Service.displayData.instructionModal.instruction;
      // Closing <p> tag
      var closingTag = '</p>';
      // Init content if none exists
      instruction.content = instruction.content || '';
      // Splice position
      var beforeClosing = instruction.content.length - (closingTag.length + 1);
      // Tag to insert
      var itemToInsert = '[' + field + ']';
      // Splice into correct location
      instruction.content =
      [instruction.content.slice(0, beforeClosing), itemToInsert, instruction.content.slice(beforeClosing)].join('');
      // Remove selected splice item
      Service.displayData.selectedMergeField = '';
    },
    /**
     * Insert a template into the instruction content
     */
    insertTemplate: function (id) {
      // Current instruction
      var instruction = Service.displayData.instructionModal.instruction;
      // Insert content
      instruction.content = '<p>' + Service.safeData.templates[id].body + '</p>';
      // Reset selected template
      Service.displayData.selectedTemplate = '';
    },
    /**
     * Update documents
     */
    updateDocuments: function (newVal, oldVal) {
      var diff = AsDataService.computeObjectDiff(oldVal, newVal), requests = [];
      // Push each request into the array
      Lazy(diff)
      .each(function (changedInput, key) {
        requests.push(Service.request('updateDocument', {
          url: {documentId: key},
          body: {title: changedInput}
        }));
      });
      // Resolution of all
      return $q.all(requests);
    },
    /**
     * Delete document
     */
    deleteDocument: function () {
      var id = Service.safeData.selectedDocument;
      return Service.request('deleteDocument', {documentId: id})
      .then(function () {
        // Remove from safe data
        delete Service.safeData.documents[id];
        // Reformat
        Service.formatData('documents');
      });
    },
    /**
     * Get document upload url
     */
    getDocumentUploadUrl: function () {
      var url = Service.request('getUploadDocumentUrl'), urlString;
      urlString = url.url;
      // Replace url with params
      angular.forEach(url.params, function (param, key) {
        urlString = urlString.replace(':' + key, param);
      });
      // Remove version
      urlString = urlString.replace('/v2.0/', '');
      // Return completed url
      return urlString;
    },
    /**
     * Add a document
     * @param document Document response from POST request
     */
    addDocument: function (document) {
      // Add to safe data
      Service.safeData.documents[document.id] = document;
      // Add to table data
      Service.formatData('documents');
    },
    /**
     * Get an instruction for editing
     * @param id Instruction ID
     */
    getInstructionForEditing: function (id) {
      // Set modal title
      Service.displayData.instructionModal.modalTitle = 'Edit instruction';
      // Instruction
      Service.displayData.instructionModal.instruction = angular.copy(Service.safeData.instructions[id]);
    },
    /**
     * Get an instruction for assignment
     * @param id Instruction ID
     */
    getInstructionForAssignment: function (id) {
      var instruction = Service.safeData.instructions[id];
      var assign = Service.safeData.assign;
      // Clear models
      assign.models.documents = [];
      assign.models.jobTypes = [];
      assign.models.states = [];
      var documents = [];
      // Add current documents
      angular.forEach(instruction.documents, function (document) {
        assign.models.documents.push({id: document.id || document});
      });
      // Replace the complete objects with just identifying objects
      instruction.documents = documents;
      // Add states
      angular.forEach(instruction.visibility.states, function (state) {
        assign.models.states.push({id: angular.isObject(state) && angular.isDefined(state.code) ? state.code : state});
      });
      // Add job types
      angular.forEach(instruction.visibility.jobTypes, function (jobType) {
        assign.models.jobTypes.push({id: jobType});
      });
    },
    /**
     * Assign instruction
     */
    assignInstruction: function () {
      // Current instruction
      var instruction = Service.safeData.instructions[Service.safeData.selectedInstruction];
      // Models from assignment modal
      var assignModels = Service.safeData.assign.models;
      angular.forEach(assignModels, function (model, key) {
        // States
        if (key === 'states') {
          Service.assignStates(instruction, model);
          // Documents
        } else if (key === 'documents') {
          Service.assignDocuments(instruction, model);
          // Job types
        } else if (key === 'jobTypes') {
          Service.assignJobTypes(instruction, 'jobTypes', model);
        }
      });
      // Update
      return Service.request('editInstruction', instruction);
    },
    /**
     * Update visibility states during assignment
     * @param instruction
     * @param model
     */
    assignStates: function (instruction, model) {
      // Edit a property within the visibility property
      instruction.visibility.states = [];
      // Add all selected states to visibility
      angular.forEach(model, function (item) {
        instruction.visibility.states.push(item.id);
      });
    },
    /**
     * Update documents during assignment
     * @param instruction
     * @param model
     */
    assignDocuments: function (instruction, model) {
      instruction.documents = [];
      angular.forEach(model, function (item) {
        instruction.documents.push(item.id);
      });
    },
    /**
     * Updates an instruction from assignment data
     */
    assignJobTypes: function (instruction, property, model) {
      // Edit a property within the visibility property
      instruction.visibility[property] = [];
      // Add all selected states to visibility
      angular.forEach(model, function (item) {
        instruction.visibility[property].push(item.id);
      });
    },
    /**
     * Create a new instruction
     */
    submitInstruction: function () {
      var instruction = Service.displayData.instructionModal.instruction;
      // Editing an instruction
      if (Service.safeData.selectedInstruction) {
        return Service.request('editInstruction', instruction)
        .then(function () {
          // Update table
          Service.safeData.instructions[instruction.id] = angular.copy(instruction);
          Service.formatData('instructions');
        });
        // new instruction
      } else {
        return Service.request('newInstruction', instruction)
        .then(function (response) {
          // Update table
          Service.safeData.instructions[response.id] = angular.copy(response);
          Service.formatData('instructions');
        });
      }
    },
    /**
     * Delete instruction
     */
    deleteInstruction: function () {
      var selectedInstruction = Service.safeData.selectedInstruction;
      return Service.request('deleteInstruction', {instructionId: selectedInstruction}).then(function () {
        // Remove from safe data
        delete Service.safeData.instructions[selectedInstruction];
        // Update table
        Service.formatData('instructions');
      });
    },
    /**
     * Copy instruction to selected users for mass update
     *
     * @todo I think this endpoint is incorrect on the backend. Should I be passing the ID as an array, for example?
     */
    copyInstruction: function () {
      return Service.request('copyInstruction', {id: [Service.safeData.selectedInstruction]});
    }
  };

  // Inherit basics
  Service.request = AdminUsersService.request.bind(Service, 'instructions');
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service);
  Service.transformData = AsDataService.transformData.bind(Service);
  // Extract documents
  Service.extractDocuments = AdminUsersService.extractDocuments.bind(Service);
  // Dynamically create table columns
  Service.getDirectiveTableColumns = AdminUsersService.getDirectiveTableColumns.bind(Service);

  return Service;
}]);