'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersClientInstructionsCtrl',
['$scope', 'AdminUsersClientInstructionsService', 'CKEDITOR_CONFIG', '$timeout', 'REQUEST_DELAY',
function ($scope, AdminUsersClientInstructionsService, CKEDITOR_CONFIG, $timeout, REQUEST_DELAY) {
  var vm = this;
  var Service = AdminUsersClientInstructionsService;
  // Safe data
  vm.safeData = Service.safeData;
  // Display data
  vm.displayData = Service.displayData;
  // Ckeditor config
  $scope.editorOptions = CKEDITOR_CONFIG.config;

  /**
   * Retrieve documents on load
   */
  vm.init = function () {
    // Get acceptance message
    Service.getAcceptanceMessage();
  };

  /**
   * Update acceptance message
   */
  vm.killAcceptanceMessageWatch = $scope.$watch(function () {
    return vm.displayData.acceptanceMessage;
  }, function (newVal, oldVal) {
    if (angular.isUndefined(newVal) || angular.equals(newVal, oldVal) || !newVal.length) {
      return;
    }
    if (vm.typing) {
      $timeout.cancel(vm.typing);
    }
    // Wait and then make the request
    vm.typing = $timeout(function () {
      Service.updateAcceptanceMessage()
      .catch(function () {
        $scope.$broadcast('show-modal', 'update-acceptance-message-failure');
        $scope.scrollTop();
      });
      vm.typing = null;
    }, REQUEST_DELAY.ms);
  });

  /**
   * Add selected merge field
   */
  vm.killMergeFieldWatch = $scope.$watchCollection(function () {
    return Service.displayData.selectedMergeField;
  }, function (newVal) {
    if (angular.isUndefined(newVal) || !newVal) {
      return;
    }
    // Splice selected merge field into content
    Service.spliceInMergeField(newVal);
  });

  /**
   * Insert selected templates into instruction content
   */
  vm.killSelectedTemplateWatch = $scope.$watch(function () {
    return Service.displayData.selectedTemplate;
  }, function (newVal) {
    if (angular.isUndefined(newVal) || !newVal) {
      return;
    }
    // Insert a template into the instruction content
    Service.insertTemplate(newVal);
  });

  /**
   * Delete document
   */
  vm.deleteDocument = function () {
    // Stop watching document inputs
    if (angular.isFunction(Service.killInputWatch)) {
      Service.killInputWatch();
    }
    // Make request, remove from safe data
    Service.deleteDocument()
    .then(function () {
      // Success
      $scope.$broadcast('hide-modal', 'delete-document-confirmation');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'delete-document-failure', true);
      $scope.scrollTop();
    })
    .finally(function () {
      // Begin watching document inputs again
      Service.watchDocumentInputs();
    });
  };
  /**
   * Add new document
   *
   * @todo This is not implemented yet, I need to check what's being sent during the upload process. Still waiting as of 8/6
   * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/Instructions/Documents.md#creates-a-document
   */
  vm.addDocument = function () {
    // Retrieve upload URL
    vm.uploadDocumentUrl = Service.getDocumentUploadUrl();
    $scope.$broadcast('show-modal', 'add-document-modal', true);
    $scope.scrollTop();
  };
  /**
   * Upload document success
   */
  vm.addDocumentSuccess = function (response) {
    // Make sure we have a valid id and link
    if (angular.isObject(response) && angular.isDefined(response.id)) {
      Service.addDocument(response);
    }
  };
  /**
   * Disable the upload input until we have a title
   */
  vm.disableUpload = function () {
    return !vm.displayData.newDocumentName.length;
  };
  /**
   * Add new instruction
   */
  vm.displayNewInstruction = function () {
    // Remove selected instruction
    Service.safeData.selectedInstruction = null;
    // Set modal title
    Service.displayData.instructionModal.modalTitle = 'New instruction';
    // Clear any instruction
    Service.displayData.instructionModal.instruction = {};
    $scope.$broadcast('show-modal', 'instruction-modal', true);
    $scope.scrollTop();
  };
  /**
   * Create new instruction
   */
  vm.submitInstruction = function () {
    // Make request
    Service.submitInstruction()
    .then(function () {
      // Hide modals on success
      $scope.$broadcast('hide-modal', 'instruction-modal');
      $scope.$broadcast('show-modal', 'new-instruction-success', true);
      $scope.scrollTop();
    })
    .catch(function () {
      // Show error
      $scope.$broadcast('show-modal', 'new-instruction-failure', true);
      $scope.scrollTop();
    });
  };
  /**
   * Delete instruction
   */
  vm.deleteInstruction = function () {
    // Make request
    Service.deleteInstruction()
    .then(function () {
      // Hide modal
      $scope.$broadcast('hide-modal', 'delete-instruction');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'delete-instruction-failure', true);
      $scope.scrollTop();
    });
  };
  /**
   * Assign instructions
   */
  vm.assignInstruction = function () {
    // Make request
    Service.assignInstruction()
    .then(function () {
      $scope.$broadcast('hide-modal', 'assign-instruction');
      $scope.$broadcast('show-modal', 'assign-instruction-success', true);
      $scope.scrollTop('#user-details');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'assign-instruction-failure', true);
      $scope.scrollTop('#user-details');
    });
  };

  /**
   * Copy instruction to selected users
   */
  vm.copyInstruction = function () {
    Service.copyInstruction()
    .then(function () {
      $scope.$broadcast('hide-modal', 'copy-instruction');
      $scope.$broadcast('show-modal', 'copy-instruction-success', true);
      $scope.scrollTop();
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'copy-instruction-failure', true);
      $scope.scrollTop();
    });
  };

  /**
   * Kill watchers on destroy
   */
  $scope.$on('$destroy', function () {
    vm.killAcceptanceMessageWatch();
    vm.killMergeFieldWatch();
    vm.killSelectedTemplateWatch();
  });

  // Initiate
  vm.init();

  ///**
  // * Get instruction being edited
  // */
  //$scope.$watch(function () {
  //  return Service.editingInstructionId;
  //}, function (newVal) {
  //  // Don't display unless we have one that's being edited
  //  if (angular.isUndefined(newVal) || !newVal) {
  //    return;
  //  }
  //  // Remove the ID of the instruction we're editing after finished
  //  var closed = $scope.$on('modal-closed-instruction-modal', function () {
  //    Service.editingInstructionId = null;
  //    closed();
  //  });
  //  // Get instruction
  //  var instruction = Service.safeData.instructions[newVal];
  //  // Set instruction being edited
  //  vm.instructionTitle = instruction.title;
  //  vm.instructionContent = instruction.content;
  //  vm.instructionTag = instruction.tag;
  //});
  //
  //
  ///**
  // * Only allow assignment modal to be submitted if each item is selected
  // * @returns {boolean}
  // */
  //vm.disableAssignSubmit = function () {
  //  return !vm.assign.models.jobTypes.length || !vm.assign.models.documents.length || !vm.assign.models.states.length;
  //};
  //
  ///**
  // * Watch values and create dropdown multiselect
  // *
  // * Note: These all need to be in watches - just setting a reference to the service does not seem to work -- Logan 5/18/15
  // */
  //// States
  //$scope.$watchCollection(function () {
  //  return vm.instructionsDisplayData.states;
  //}, function (newVal) {
  //  if (!angular.isArray(newVal) || !newVal.length) {
  //    return;
  //  }
  //  // Create state options
  //  vm.assign.options.states = newVal;
  //});
  //// Documents
  //$scope.$watchCollection(function () {
  //  return vm.documentDisplayData.tableData;
  //}, function (newVal) {
  //  if (!angular.isArray(newVal) || !newVal.length) {
  //    return;
  //  }
  //  // Create state options
  //  vm.assign.options.documents = newVal;
  //});
  //// Job types
  //$scope.$watchCollection(function () {
  //  return vm.instructionsDisplayData.jobTypes;
  //}, function (newVal) {
  //  if (!angular.isArray(newVal) || !newVal.length) {
  //    return;
  //  }
  //  // Create state options
  //  vm.assign.options.jobTypes = newVal;
  //});
}]);