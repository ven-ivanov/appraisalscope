'use strict';

var app = angular.module('frontendApp');

/**
 * Instructions table ctrl
 */
app.controller('AdminUsersClientInstructionsInstructionsCtrl',
['$scope', 'AdminUsersClientInstructionsService', function ($scope, AdminUsersClientInstructionsService) {
  var vm = this;
  var Service = AdminUsersClientInstructionsService;
  // Display data
  vm.displayData = Service.displayData;
  // Safe data
  vm.safeData = Service.safeData;
  // Table definition
  //vm.heading = Service.safeData.headings.instructions;
  vm.heading = Service.getDirectiveTableColumns($scope.hide || 'copy');

  /**
   * Retrieve documents on load
   */
  vm.init = function () {
    // Retrieve company documents
    Service.getInstructions();
  };

  /**
   * Recreate table data when display data changes
   */
  vm.killInstructionWatch = $scope.$watch(function () {
    return Service.displayData.instructions;
  }, function (newVal) {
    if (!angular.isArray(newVal) || !newVal.length) {
      return;
    }
    // Create table data from display data
    vm.tableData = newVal;
    vm.safeData = newVal.slice();
  }, true);

  /**
  * Assign, edit, delete records
  */
  vm.linkFn = function (id, fnName) {
    Service.safeData.selectedInstruction = id;
    switch (fnName) {
      case 'assign':
        Service.getInstructionForAssignment(id);
        $scope.$emit('show-modal', 'assign-instruction', true);
        $scope.scrollTop();
        break;
      case 'edit':
        Service.getInstructionForEditing(id);
        // ID of instruction being edited
        $scope.$emit('show-modal', 'instruction-modal', true);
        $scope.scrollTop();
        break;
      case 'delete':
        $scope.$emit('show-modal', 'delete-instruction', true);
        $scope.scrollTop();
        break;
      case 'copy':
        $scope.$emit('show-modal', 'copy-instruction', true);
        $scope.scrollTop();
        break;
      default:
        break;
    }
  };

  /**
   * Kill watch on destroy
   */
  $scope.$on('$destroy', function () {
    vm.killInstructionWatch();
  });

  // Initiate
  vm.init();
}]);