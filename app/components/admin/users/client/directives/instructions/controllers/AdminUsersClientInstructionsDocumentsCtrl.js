'use strict';

var app = angular.module('frontendApp');

/**
 * Documents table ctrl
 */
app.controller('AdminUsersClientInstructionsDocumentsCtrl',
['$scope', 'AdminUsersClientInstructionsService', '$timeout', 'REQUEST_DELAY',
function ($scope, AdminUsersClientInstructionsService, $timeout, REQUEST_DELAY) {
  var vm = this;
  var Service = AdminUsersClientInstructionsService;
  // Table definition
  vm.heading = Service.safeData.headings.documents;
  // Updating document titles
  var oldValForComparison;

  /**
   * Recreate table data when display data changes
   */
  vm.killDocWatch = $scope.$watchCollection(function () {
    return Service.displayData.documents;
  }, function (newVal) {
    if (!angular.isArray(newVal) || !newVal.length) {
      return;
    }
    // Create table data from display data
    vm.tableData = newVal;
    vm.safeData = newVal.slice();
    // Create table input data
    vm.tableInputs = Service.initDocumentTableInputs();
    // Begin watching document inputs
    if (angular.isFunction(Service.killInputWatch)) {
      Service.killInputWatch();
    }
    Service.watchDocumentInputs();
  });

  /**
   * Update document names
   */
  Service.watchDocumentInputs = function () {
    Service.killInputWatch = $scope.$watchCollection(function () {
      return vm.tableInputs;
    }, function (newVal, oldVal) {
      if (angular.isUndefined(oldVal) || angular.equals(newVal, oldVal)) {
        return;
      }
      // Hold reference to the diff since typing began
      oldValForComparison = oldValForComparison || oldVal;
      if (vm.typing) {
        $timeout.cancel(vm.typing);
      }
      // Wait and then make the request
      vm.typing = $timeout(function () {
        // Request
        Service.updateDocuments(newVal, oldValForComparison)
        .catch(function () {
          $scope.$emit('show-modal', 'update-document-failure', true);
          $scope.scrollTop();
        });
        vm.typing = null;
        oldValForComparison = null;
      }, REQUEST_DELAY.ms);
    });
  };

  /**
   * Delete records
   */
  vm.linkFn = function (id, fnName) {
    // Selected document
    Service.safeData.selectedDocument = id;
    // Delete document
    if (fnName === 'delete') {
      $scope.$emit('show-modal', 'delete-document-confirmation', true);
      $scope.scrollTop();
    // Download document
    } else if (fnName === 'download') {
      $scope.triggerDownload(Service.safeData.documents[id].url);
    }
  };

  /**
   * Clean up watchers on $destroy
   */
  $scope.$on('$destroy', function () {
    vm.killDocWatch();
    if (angular.isFunction(Service.killInputWatch)) {
      Service.killInputWatch();
    }
  });
}]);