var AdminUsersClientInstructionsServiceMock = function () {
  return {
    // Safe data
    safeData: {
      // Documents data
      documents: {
        1: {id: 1, document: 'Document 1', link: 'link1.com'},
        2: {id: 2, document: 'Document 2', link: 'link2.com'}
      },
      // Instructions data
      instructions: {
        1: {id: 1, title: 'Instruction 1', content: 'Instruction Content 1', tag: 9},
        2: {id: 2, title: 'Instruction 2', content: 'Instruction Content 2', tag: 7}
      },
      // Instructions tags
      instructionsTags: {1: {id: 1, tag: 'Tag 1'}, 2: {id: 2, tag: 'Tag 2'}},
      // Job types
      jobTypes: {
        1: {id: 1, name: 'Job type 1', percentageFlat: 1, feeMargin: 60.82},
        2: {id: 2, name: 'Job type 2', percentageFlat: 0, feeMargin: 82.48}
      }
    },
    // Data for display
    displayData: {
      documents: {
        // Table data
        tableData: [{"id": 1, "document": "Document 1", "link": "link1.com"},
                    {"id": 2, "document": "Document 2", "link": "link2.com"}],
        // Acceptance message
        acceptanceMessage: 'Acceptance Message'
      },
      instructions: {
        // Instructions table
        tableData: [{
                      "id": 1,
                      "title": "Instruction 1",
                      "content": "Instruction Content 1",
                      "tag": 9
                    }, {
                      "id": 2,
                      "title": "Instruction 2",
                      "content": "Instruction Content 2",
                      "tag": 7
                    }],
        // Instructions tags
        instructionsTags: [{"id":1,"tag":"Tag 1"},{"id":2,"tag":"Tag 2"}],
        // Modal data
        modal: {},
        // States
        states: [{id: 1, code: 'AK', title: 'Arkansas'}],
        // Job types
        jobTypes: [{"id": 1, "name": "Job type 1", "percentageFlat": 1, "feeMargin": 60.82},
                   {"id": 2, "name": "Job type 2", "percentageFlat": 0, "feeMargin": 82.48}]
      }
    },
    // Assignment data
    assignData: {
      options: {
        states: [],
        documents: [],
        jobTypes: []
      },
      models: {
        states: [],
        documents: [],
        jobTypes: []
      },
      settings: {
        states: {displayProp: 'title', idProp: 'id'},
        documents: {displayProp: 'document', idProp: 'id'},
        jobTypes: {displayProp: 'name', idProp: 'id'}
      }
    }
  }
};