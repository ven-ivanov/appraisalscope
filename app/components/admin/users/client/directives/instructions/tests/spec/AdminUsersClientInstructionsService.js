describe('AdminUsersClientInstructionsService', function () {
  var scope, Service, adminUsersService, httpBackend;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function (AdminUsersService, AdminUsersClientInstructionsService, $rootScope, $httpBackend) {
    scope = $rootScope.$new();
    Service = AdminUsersClientInstructionsService;
    adminUsersService = AdminUsersService;
    httpBackend = $httpBackend;

    spyOn(Service, 'request').and.callThrough();
    spyOn(Service, 'hashData');
    spyOn(Service, 'formatData');

    httpBackend.whenGET(/acceptance-message/).respond({label: 'test'});
    httpBackend.whenGET(/instructions$/).respond({data: [1,2,3]});
    httpBackend.whenGET(/job-type-lists/).respond({data: ['yep']});
    httpBackend.whenDELETE(/.*/).respond();
    httpBackend.whenPATCH(/.*/).respond({id: 2});
    httpBackend.whenPOST(/.*/).respond({id: 2});
    httpBackend.whenGET(/merge-fields/).respond({data: []});
    httpBackend.whenGET(/template/).respond({data: []});
  }));

  describe('getAcceptanceMessage', function () {
    beforeEach(function () {
      Service.getAcceptanceMessage();
      httpBackend.flush();
      scope.$digest();
    });

    it('should set the acceptance message', function () {
      expect(Service.displayData.acceptanceMessage).toEqual('test');
    });
  });

  describe('updateAcceptanceMessage', function () {
    beforeEach(function () {
      Service.displayData.acceptanceMessage = 'a new label';
      Service.updateAcceptanceMessage();
    });

    it('should send the request to update the label', function () {
      expect(Service.request).toHaveBeenCalledWith('updateAcceptanceMessage', { label: 'a new label' });
    });
  });

  describe('getInstructions', function () {
    beforeEach(function () {
      spyOn(Service, 'extractDocuments');
      spyOn(Service, 'getMergeFields');
      spyOn(Service, 'getTemplates');
      spyOn(Service, 'getStates');
      spyOn(Service, 'getJobtypeLists');
      Service.getInstructions();
      httpBackend.flush();
      scope.$digest();
    });

    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should format data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
    it('should should extract data', function () {
      expect(Service.extractDocuments).toHaveBeenCalled();
    });
    it('should get instruction tags', function () {
      expect(Service.getMergeFields).toHaveBeenCalled();
    });
    it('should get instruction templates', function () {
      expect(Service.getTemplates).toHaveBeenCalled();
    });
    it('should get states', function () {
      expect(Service.getStates).toHaveBeenCalled();
    });
    it('should get job type lists', function () {
      expect(Service.getJobtypeLists).toHaveBeenCalled();
    });
  });

  describe('getMergeFields', function () {
    beforeEach(function () {
      Service.getMergeFields();
      httpBackend.flush();
      scope.$digest();
    });

    it('should make the right request', function () {
      expect(Service.request.calls.argsFor(0)).toEqual([ 'getMergeFields' ]);
    });
    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalledWith([  ], 'mergeFields', 'placeholder');
    });
    it('should format data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('getTemplates', function () {
    beforeEach(function () {
      Service.getTemplates();
      httpBackend.flush();
      scope.$digest();
    });

    it('should make the right request', function () {
      expect(Service.request.calls.argsFor(0)).toEqual([ 'templates' ]);
    });
    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should format data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('getJobtypeLists', function () {
    beforeEach(function () {
      Service.displayData.jobTypes = [1,2,3];
      Service.getJobtypeLists();
      httpBackend.flush();
      scope.$digest();
    });

    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should format data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
    it('should set job types for display', function () {
      expect(Service.safeData.assign.options.jobTypes).toEqual([1,2,3]);
    });
  });

  describe('getStates', function () {
    beforeEach(function () {
      Service.getStates();
    });

    it('should set the states from adminUsersService', function () {
      expect(Service.safeData.assign.options.states).toEqual({ AK: 'Arkansas' });
    });
  });

  describe('initDocumentTableInputs', function () {
    var response;
    beforeEach(function () {
      Service.displayData.documents = [{id:1, title: 'title'}, {id:2, title: 'title2'}];
      response = Service.initDocumentTableInputs();
    });
    it('should return documents', function () {
      expect(response).toEqual({ 1: 'title', 2: 'title2' });
    });
  });

  describe('updateDocuments', function () {
    var newVal, oldVal;
    beforeEach(function () {
      newVal = {1: 'sup', 2: 'what'};
      oldVal = {1: 'nope', 2: 'where'};
      Service.updateDocuments(newVal, oldVal);
    });
    it('should calculate difference and make a request for each', function () {
      expect(Service.request.calls.argsFor(0)).toEqual([ 'updateDocument', { url: { documentId: '1' }, body: { title: 'sup' } } ]);
    });
    it('should calculate difference and make a request for each', function () {
      expect(Service.request.calls.argsFor(1)).toEqual([ 'updateDocument', { url: { documentId: '2' }, body: { title: 'what' } } ]);
    });
  });

  describe('deleteDocument', function () {
    beforeEach(function () {
      Service.safeData.documents = {1: {test: 'test'}, 2: {test: 'test2'}};
      Service.safeData.selectedDocument = 1;
      Service.deleteDocument();
      httpBackend.flush();
      scope.$digest();
    });

    it('should delete the selected document', function () {
      expect(Service.safeData.documents).toEqual({ 2: { test: 'test2' } });
    });
    it('should reformat data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('getDocumentUploadUrl', function () {
    var response;
    beforeEach(function () {
      response = Service.getDocumentUploadUrl();
    });

    it('should create the url string', function () {
      expect(response).toEqual('client/companies/1/instructions/documents');
    });
  });

  describe('addDocument', function () {
    beforeEach(function () {
      Service.safeData.documents = {1: {id: 1, doc: 'whatever'}};
      Service.addDocument({id: 3, doc: 'test'});
    });

    it('should add document to safe data', function () {
      expect(Service.safeData.documents).toEqual({ 1: { id: 1, doc: 'whatever' }, 3: { id: 3, doc: 'test' } });
    });
    it('should format data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('getInstructionForEditing', function () {
    beforeEach(function () {
      Service.safeData.instructions = {1: {instruction: 'instruction'}};
      Service.getInstructionForEditing(1);
    });

    it('should set the modal title', function () {
      expect(Service.displayData.instructionModal.modalTitle).toEqual('Edit instruction');
    });
    it('should copy over the instruction for editing', function () {
      expect(Service.displayData.instructionModal.instruction).toEqual({instruction: 'instruction'});
    });
  });

  describe('getInstructionForAssignment', function () {
    beforeEach(function () {
      Service.safeData.instructions = {1: {documents: [{id: 1}, {id: 2}, 3], visibility: {states: [{name: 'Ohio', code: 'OH'}, {name: 'Kentucky', code: 'KY'}], jobTypes: [1,2,3]}}};
      Service.getInstructionForAssignment(1);
    });

    it('should set documents to assign data', function () {
      expect(Service.safeData.assign.models.documents).toEqual([{id: 1}, {id: 2}, {id: 3}]);
    });
    it('should set states to assign data', function () {
      expect(Service.safeData.assign.models.states).toEqual([{id: 'OH'}, {id: 'KY'}]);
    });
    it('should set job types to assign data', function () {
      expect(Service.safeData.assign.models.jobTypes).toEqual([{id: 1}, {id: 2}, {id: 3}]);
    });
  });

  describe('submitInstruction', function () {
    describe('editing', function () {
      beforeEach(function () {
        Service.safeData.selectedInstruction = 1;
        Service.displayData.instructionModal.instruction = {id: 1, instruction: 1};
        Service.submitInstruction();
        httpBackend.flush();
        scope.$digest();
      });

      it('should add submitted instruction to safe data', function () {
        expect(Service.safeData.instructions).toEqual({ 1: { id: 1, instruction: 1 } });
      });
      it('should format data', function () {
        expect(Service.formatData).toHaveBeenCalled();
      });
    });

    describe('new', function () {
      beforeEach(function () {
        Service.displayData.instructionModal.instruction = {id: 1, instruction: 1};
        Service.submitInstruction();
        httpBackend.flush();
        scope.$digest();
      });

      it('should add response instruction to safe data', function () {
        expect(Service.safeData.instructions[2].id).toEqual(2);
      });
      it('should format data', function () {
        expect(Service.formatData).toHaveBeenCalled();
      });
    });
  });

  describe('deleteInstruction', function () {
    beforeEach(function () {
      Service.safeData.instructions = {1: {id:1}, 2: {id: 2}};
      Service.safeData.selectedInstruction = 1;
      Service.deleteInstruction();
      httpBackend.flush();
      scope.$digest();
    });

    it('should update safe data', function () {
      expect(Service.safeData.instructions).toEqual({ 2: { id: 2 } });
    });
    it('should format data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  /**
   * @todo need to verify this is correct - 8/6 hrewhere
   */
  describe('assignInstruction', function () {
    beforeEach(function () {
      Service.safeData.selectedInstruction = 1;
      Service.safeData.instructions = {1: {id: 1}};
      Service.safeData.assign.models = {states: ['Ohio', 'Indiana'], documents: [{id:1}, {id: 2}], jobTypes: [1,2]};
      spyOn(Service, 'assignStates');
      spyOn(Service, 'assignDocuments');
      spyOn(Service, 'assignJobTypes');
      Service.assignInstruction();
    });

    it('should update instruction with states', function () {
      expect(Service.assignStates.calls.argsFor(0)).toEqual([ { id: 1 }, [ 'Ohio', 'Indiana' ] ]);
    });
    it('should update instructions with job types', function () {
      expect(Service.assignDocuments.calls.argsFor(0)).toEqual([ { id: 1 }, [ { id: 1 }, { id: 2 } ] ]);
    });
    it('should update the instruction with document ints', function () {
      expect(Service.assignJobTypes.calls.argsFor(0)).toEqual([ { id: 1 }, 'jobTypes', [ 1, 2 ] ]);
    });
  });

  describe('assignStates', function () {

  });

  describe('assignDocuments', function () {

  });

  describe('assignJobTypes', function () {

  });
});