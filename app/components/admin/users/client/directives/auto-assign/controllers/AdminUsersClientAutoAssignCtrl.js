'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Client - Auto assign
 */
app.controller('AdminUsersClientAutoAssignCtrl',
['$scope', 'AdminUsersClientService', 'AdminUsersClientAutoAssignService', '$timeout', 'REQUEST_DELAY',
function ($scope, AdminUsersClientService, AdminUsersClientAutoAssignService, $timeout, REQUEST_DELAY) {
  var vm = this;

  // Safe and display data
  vm.safeData = AdminUsersClientAutoAssignService.safeData;
  vm.displayData = AdminUsersClientAutoAssignService.displayData;
  var Service = AdminUsersClientAutoAssignService;

  /**
   * Get initial data on load
   */
  vm.init = function () {
    // Table data
    Service.getAutoAssignData()
    .then(function () {
      // Init approved list
      Service.initApprovedList();
      // Attach watcher
      vm.attachWatch();
    });
  };
  /**
   * Update top level dropdowns on backend
   */
  vm.attachWatch = function () {
    $scope.$watch(function () {
      return vm.safeData.autoAssign;

    }, function (newVal, oldVal) {
      if (angular.isUndefined(oldVal) || angular.equals(oldVal, newVal)) {
        return;
      }
      if (vm.typing) {
        $timeout.cancel(vm.typing);
      }
      // Wait and then make the request
      vm.typing = $timeout(function () {
        // Initiate request
        Service.writeRequest(newVal)
        .catch(function () {
          // Failure
          $scope.$broadcast('show-modal', 'auto-assign-write-failure', true);
        });
        vm.typing = null;
      }, REQUEST_DELAY.ms);
    }, true);
  };

  /**
   * Manage approved lists
   */
  vm.manageApprovedList = function () {
    $scope.$broadcast('show-modal', 'approved-list-order-modal', true);
  };

  /**
   * Disable job type and approved list buttons
   */
  vm.disableButtons = function (type) {
    return angular.isUndefined(Service.safeData.autoAssign[type]) ||
           !Service.safeData.autoAssign[type].isEnabled;
  };

  /**
   * Approved list order
   */
  $scope.$watchCollection(function () {
    return vm.order;
  }, function (newVal) {
    // If we have an order, then write it
    if (angular.isArray(newVal) && newVal.length) {
      Service.writeRequest(newVal, 'order')
      .then(function () {
        $scope.$broadcast('hide-modal', 'approved-list-order-modal');
      })
      .finally(function () {
        // Reset array
        $scope.order = [];
      })
      .catch(function () {
        // Show failure
        $scope.$broadcast('show-modal', 'auto-assign-write-failure', true);
      });
    }
  });

  // Initiate data
  vm.init();
}]);
