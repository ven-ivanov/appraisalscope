'use strict';

describe('AdminUsersClientAutoAssignCtrl', function () {
  var scope, controller, adminUsersClientAutoAssignService, timeout;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersClientAutoAssignService', AdminUsersClientAutoAssignServiceMock);
      $provide.factory('AdminUsersClientService', AdminUsersClientServiceMock);
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });
  beforeEach(inject(function ($rootScope, $controller, AdminUsersClientAutoAssignService, $timeout) {
    scope = $rootScope.$new();
    adminUsersClientAutoAssignService = AdminUsersClientAutoAssignService;
    timeout = $timeout;
    // SpyOn init items
    spyOn(adminUsersClientAutoAssignService, 'getAutoAssignData').and.callThrough();
    spyOn(adminUsersClientAutoAssignService, 'initApprovedList').and.callThrough();
    spyOn(adminUsersClientAutoAssignService, 'writeRequest').and.callThrough();
    controller = $controller('AdminUsersClientAutoAssignCtrl', {$scope: scope});
    scope.$digest();
  }));

  describe('init()', function () {
    it('should get data on load', function () {
      expect(adminUsersClientAutoAssignService.getAutoAssignData).toHaveBeenCalled();
    });
    it('should initiate approved list on load', function () {
      expect(adminUsersClientAutoAssignService.initApprovedList).toHaveBeenCalled();
    });
  });

  describe('data', function () {
    it('should not run on load', function () {
      expect(adminUsersClientAutoAssignService.writeRequest).not.toHaveBeenCalled();
    });

    it('should run on change', function () {
      controller.safeData.autoAssign = [{id: 1}];
      scope.$digest();
      timeout.flush();
      expect(adminUsersClientAutoAssignService.writeRequest).toHaveBeenCalled();
    });
  });

  describe('order', function () {
    it('should not run on load', function () {
      scope.$digest();
      expect(adminUsersClientAutoAssignService.writeRequest).not.toHaveBeenCalled();
    });

    it('should run on change', function () {
      controller.order = [1,2,3,4];
      scope.$digest();
      expect(adminUsersClientAutoAssignService.writeRequest).toHaveBeenCalled();
    });
  });
});