'use strict';

describe('AdminUsersClientAutoAssignService', function () {
  var adminUsersClientAutoAssignService, scope, httpBackend, data;
  beforeEach(function () {
    module('frontendApp');
  });
  beforeEach(inject(function ($rootScope, AdminUsersClientAutoAssignService, $httpBackend) {
    scope = $rootScope.$new();
    adminUsersClientAutoAssignService = AdminUsersClientAutoAssignService;

    data = [{id: 1, condition: 'Max distance', enable: 1, value: 55},
            {id: 2, condition: 'Minimum rating', enable: 0, value: 1}];

    // Data
    spyOn(adminUsersClientAutoAssignService, 'hashData');

    httpBackend = $httpBackend;
    // Requests
    httpBackend.whenGET(/appraisal-forms/).respond([
      {
        id: 1,
        enabled: false
      },
      {
        id: 2,
        enabled: true
      }
    ]);
    httpBackend.whenGET(/dropdowns/).respond({test: 'test', test2: 'test2'});
    httpBackend.whenGET(/auto-assign/).respond({data: data});

    // Mocking abstract state template
    httpBackend.expectGET(/base/).respond();
  }));

  describe('getAutoAssignData', function () {
    beforeEach(function () {
      adminUsersClientAutoAssignService.getAutoAssignData();
      httpBackend.flush();
      scope.$digest();
    });
    it('should hash data', function () {
      expect(adminUsersClientAutoAssignService.hashData).toHaveBeenCalled();
    });
  });

  xdescribe('sortByOrder', function () {

  });

  xdescribe('initApprovedList', function () {

  });

  xdescribe('writeRequest', function () {

  });
});