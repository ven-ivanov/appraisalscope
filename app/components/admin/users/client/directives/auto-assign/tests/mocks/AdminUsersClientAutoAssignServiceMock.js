var AdminUsersClientAutoAssignServiceMock = function ($q) {
  return {
    safeData: {
      // Table data
      autoAssign: {},
      // Approved list
      approvedList: [],
      // Dropdowns
      selects: {},
      // Table multi-selects
      multiSelect: {},
      // Table heading
      heading: [
        // ID
        {
          label: 'ID',
          data: 'id',
          noSort: true
        },
        // Enable
        {
          label: 'Enable',
          data: 'enable',
          noSort: true,
          checkbox: true
        },
        // Condition
        {
          label: 'Condition',
          data: 'condition',
          noSort: true
        },
        // Function to display users for a company
        {
          label: '',
          special: true,
          noSort: true
        }
      ],
      // Special cells configuration
      specialCellsConfig: [
        // Max distance input
        {
          id: 1,
          type: 'input'
        },
        // Minimum rating dropdown
        {
          id: 2,
          type: 'dropdown',
          options: [
            {
              value: 1
            },
            {
              value: 2
            },
            {
              value: 3
            },
            {
              value: 4
            },
            {
              value: 5
            }
          ]
        },
        // Max pending orders input
        {
          id: 3,
          type: 'input'
        },
        // FHA approved options
        {
          id: 7,
          type: 'dropdown',
          options: [
            {
              id: 0,
              value: 'Search FHA-approved appraisers for all forms'
            },
            {
              id: 1,
              value: 'Search FHA-approved appraisers for FHA forms only'
            }
          ]
        },
        // General appraiser dropdown
        {
          id: 8,
          type: 'dropdown',
          options: [
            {
              value: 1
            },
            {
              value: 2
            },
            {
              value: 3
            }
          ]
        },
        // Certified appraiser dropdown
        {
          id: 9,
          type: 'dropdown',
          options: [
            {
              value: 1
            },
            {
              value: 2
            },
            {
              value: 3
            }
          ]
        },
        // Licensed appraiser dropdown
        {
          id: 10,
          type: 'dropdown',
          options: [
            {
              value: 1
            },
            {
              value: 2
            },
            {
              value: 3
            }
          ]
        },
        // Appraisal forms function
        {
          id: 12,
          type: 'dropdown-multiselect',
          options: [{id: 1, value: 'yes'}],
          settings: {
            scrollableHeight: '150px',
            scrollable: true,
            displayProp: 'value'
          }
        },
        // Prioritize approved lists function
        {
          id: 13,
          type: 'fn',
          label: 'Manage approved list'
        },
        // Profit margin percentage input
        {
          id: 14,
          type: 'input'
        }
      ],
      // Special cells on initialization
      specialCellsInit: {
        // Input
        1: '',
        2: '',
        3: '',
        7: '',
        8: '',
        9: '',
        10: '',
        12: [],
        // 13 defined in controller
        // Input
        14: ''
      }
    },
    displayData: {
      // Table data
      autoAssign: [],
      // Yes/no dropdowns
      yesNoDropdown: [{id: 1, value: 'yes'}, {id: 2, value: 'no'}]
    },
    getAutoAssignData: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    getAppraisalForms: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    checkEnabled: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    initApprovedList: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    getAutoAssignDropdowns: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    runTableValues: function (newVal) {
      // Don't run on load
      var run = false;
      angular.forEach(newVal, function (val) {
        // If we're coming from load, don't run
        if (angular.isArray(val)) {
          if (val.length) {
            run = true;
          }
        } else if (!angular.isFunction(val) && val) {
          run = true;
        }
      });
      return run;
    },
    checkLoad: function (oldVal) {
      var loading = true;
      // Don't run on load
      angular.forEach(oldVal, function (value) {
        if (value) {
          loading = false;
        }
      });
      return loading;
    },
    writeRequest: function () {
      return $q(function (resolve) {
        resolve();
      });
    }
  }
};