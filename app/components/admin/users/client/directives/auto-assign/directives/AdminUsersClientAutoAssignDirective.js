/*global dragula:false */
'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersClientAutoAssign', ['DomLoadService', 'AdminUsersDirectiveInheritanceService',
function (DomLoadService, AdminUsersDirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/client/directives/auto-assign/directives/partials/auto-assign.html',
    controller: 'AdminUsersClientAutoAssignCtrl',
    controllerAs: 'tableCtrl',
    link: function (scope, element, attrs, controller) {
      var order = [];
      var splitId, itemId, thisId;
      // Inherit shared directive functions
      AdminUsersDirectiveInheritanceService.inherit.call(scope);
      /**
       * Reorder approved list submit function
       * @param id
       */
      scope.reorderApprovedList = function (id) {
        angular.element('#' + id).children().each(function (child, elem) {
          // Get ID of current element
          thisId = angular.element(elem).attr('id');
          // Split to find order number
          if (thisId !== id) {
            splitId = thisId.split('-');
            itemId = splitId[splitId.length -1];
            // Push onto scope so the controller can handle it
            order.push(itemId);
          }
        });
        // Set on controller
        controller.order = order;
      };
      // Init dragula
      DomLoadService.load().then(function () {
        dragula([document.querySelector('#approved-list-order')]);
      });
    }
  };
}]);
