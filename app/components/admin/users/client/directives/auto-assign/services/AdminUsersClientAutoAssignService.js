'use strict';

var app = angular.module('frontendApp');

/**
 * @todo Still waiting on the backend for job types and approved lists
 */
app.factory('AdminUsersClientAutoAssignService',
['AdminUsersService', 'AdminUsersResourceService', 'AsDataService', function (AdminUsersService, AdminUsersResourceService, AsDataService) {
  var Service = {
    safeData: {
      // Table data
      autoAssign: {},
      // Approved list
      approvedList: []
    },
    displayData: {
      // Table data
      autoAssign: [],
      // Yes/no dropdowns
      yesNoDropdown: AdminUsersService.yesNoDropdown,
      // Minimum rating dropdown
      minRatingDropdown: [
        {id: 1, val: 1},
        {id: 2, val: 2},
        {id: 3, val: 3},
        {id: 4, val: 4},
        {id: 5, val: 5}
      ],
      // Order for licensed, general, and certified appraiser
      appraiserOrderDropdown: [
        {id: 1, val: 1},
        {id: 2, val: 2},
        {id: 3, val: 3}]
    },
    /**
     * Get auto assignment data
     */
    getAutoAssignData: function () {
      return Service.request('init')
      .then(function (response) {
        // Make a simple hash for manipulation
        Service.hashData(response, 'autoAssign');
      });
    },
    /**
     * Sort the approved forms by order
     * @param a
     * @param b
     * @returns {number}
     */
    sortByOrder: function (a, b) {
      return a.order < b.order ? -1 : 1;
    },
    /**
     * Initialize approved list
     *
     * @todo Waiting on the backend
     * @link https://github.com/ascope/manuals/issues/187
     * 8/3
     */
    initApprovedList: function () {
      angular.forEach(Service.safeData.autoAssign, function (item) {
        // Find the approved list
        if (angular.isDefined(item.approvedList)) {
          angular.forEach(item.approvedList, function (list) {
            Service.safeData.approvedList.push(list);
          });
        }
      });
      // Sort by order
      Service.safeData.approvedList.sort(Service.sortByOrder);
    },
    /**
     * Write updates to backend
     * @param values
     */
    writeRequest: function (values) {
      return Service.request('update', values);
    }
  };

  // Inherit
  Service.request = AdminUsersService.request.bind(Service, 'autoAssign');
  Service.hashData = AsDataService.hashData.bind(Service);

  return Service;
}]);