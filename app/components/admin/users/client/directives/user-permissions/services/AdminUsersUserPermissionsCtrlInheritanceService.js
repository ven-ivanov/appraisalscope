'use strict';

var app = angular.module('frontendApp');

/**
 * Inherit common methods for user permissions tables
 */
app.factory('AdminUsersUserPermissionsCtrlInheritanceService',
['AdminUsersUserPermissionsService', function (AdminUsersUserPermissionsService) {
  return {
    inherit: function ($scope, type, multiSelect) {
      var vm = this;
      /**
       * Watch user documents and display table when we have the documents
       */
      $scope.$watchCollection(function () {
        return AdminUsersUserPermissionsService.displayData[type];
      }, function (newVal) {
        if (!angular.isArray(newVal)) {
          return;
        }
        // Set table data
        vm.tableData = newVal;
        vm.rowData = newVal.slice();
        if (multiSelect) {
          // Check the boxes which are currently active
          vm.multiSelect = AdminUsersUserPermissionsService.checkGroups(newVal);
        }
      });

      // If multiselect is being used in the table, watch those values
      if (multiSelect) {
        /**
         * Watch which records are checked, and update the backend
         */
        $scope.$watchCollection(function () {
          return vm.multiSelect;
        }, function (newVal, oldVal) {
          // Don't write on load
          if (angular.isUndefined(newVal) || angular.isUndefined(oldVal)) {
            return;
          }
          // Write to the backend
          AdminUsersUserPermissionsService.updatePermissions(type, newVal)
          .catch(function () {
            $scope.$emit('show-modal', 'general-update-failure', true);
          });
        });
      }
    }
  };
}]);