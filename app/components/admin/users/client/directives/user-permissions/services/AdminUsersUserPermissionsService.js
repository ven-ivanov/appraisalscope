'use strict';

var app = angular.module('frontendApp');

/**
 * @todo This endpoint doesn't exist yet.
 *
 * @link https://github.com/ascope/manuals/issues/41
 */
app.factory('AdminUsersUserPermissionsService',
['$http', 'API_PREFIX', 'AdminUsersService', '$resource', 'AdminUsersResourceService',
function ($http, API_PREFIX, AdminUsersService, $resource, AdminUsersResourceService) {
  var Service = {
    safeData: {
      userMail: {
        // Mail users
        users: [],
        // Hold mail selections
        selections: [],
        // Selected user
        selectedUser: 0
      },
      brokerMail: {
        // Brokers
        brokers: [],
        // Selected mail items
        selections: [],
        // Selected broker
        selectedBroker: 0
      }
    },
    displayData: {
      mail: {
        /**
         * Type of mail permissions
         */
        types: {
          inspectionScheduled: 'Inspection Scheduled',
          inspectionComplete: 'Inspection Complete',
          onHold: 'On Hold',
          additionalStatus: 'Additional Status',
          completedAppraisal: 'Completed Appraisal',
          inReview: 'In Review',
          accepted: 'Accepted',
          emailOffice: 'Email Office',
          declined: 'Declined',
          assigned: 'Assigned',
          emailedDocuments: 'Emailed Documents',
          acceptedWithConditions: 'Accepted With Conditions',
          newOrderConfirmation: 'New Order Confirmation'
        }
      }
    },
    /**
     * User AdminUserResourceService to general resource requests
     * @param requestType String Function to call in resource service
     * @param params String Argument passed into resource service
     */
    request: function (requestType, params) {
      params = params || null;
      // Get $resource configuration
      return AdminUsersResourceService.client[requestType](params);
    },
    /**
     * Retrieve any of the various types of user permissions
     */
    permissionsRequest: function () {
      return $resource(API_PREFIX() + '/v2.0/admin/users/client/:clientId/permissions/:type/',
      {clientId: AdminUsersService.id, type: '@permissionType'},
      {
        updatePermissions: {method: 'patch'}
      });
    },
    /**
     * Retrieve a permission for the current user
     */
    getUserPermission: function (type, attribute) {
      // Make request
      Service.permissionsRequest()
      .query({type: 'user-permissions-' + type}, function (data) {
        // Set attribute
        if (!attribute) {
          attribute = type;
        }
        Service.displayData[attribute] = data;
      });
    },
    /**
     * Get initial status for auto add new group
     */
    getAutoAddNewGroupStatus: function () {
      return Service.permissionsRequest()
      .get({type: 'auto-add-new-group'}, function (response) {
        Service.displayData.autoAddNewGroupStatus = response.status;
      });
    },
    /**
     * Get initial status for auto add new group
     */
    getAutoAddNewBrokerStatus: function () {
      return Service.permissionsRequest()
      .get({type: 'auto-add-new-broker'}, function (response) {
        Service.displayData.autoAddNewBrokerStatus = response.status;
      });
    },
    /**
    * Get user mail permissions
    */
    getUserMailPermissions: function () {
      return Service.permissionsRequest()
      .query({type: 'user-permissions-mail'}, function (response) {
        // Retrieve users
        Service.safeData.userMail.users = [{name: '',id: 0}].concat(response);
        // Select no user
        Service.safeData.userMail.selectedUser = 0;
      });
    },
    /**
    * Get user mail permissions
    */
    getBrokerMailPermissions: function () {
      return Service.permissionsRequest()
      .query({type: 'broker-permissions-mail'}, function (response) {
        // Set broker selections
        Service.safeData.brokerMail.brokers = [{name: '',id: 0}].concat(response);
        // Select none
        Service.safeData.brokerMail.selectedBroker = 0;
      });
    },
    /**
     * Check the checkboxes for the records which are already active
     */
    checkGroups: function (data) {
      var result = {};
      angular.forEach(data, function (val) {
        if (val.active) {
          result[val.id] = true;
        }
      });
      return result;
    },
    /**
     * Update backend on change
     */
    updatePermissions: function (type, update) {
      // Update permissions
      var permissions = new (Service.permissionsRequest())(update);
      permissions.permissionType = type + '-permissions';
      return permissions.$updatePermissions();
    },
    /**
     * General toggle settings
     */
    toggleSetting: function (type) {
      if (angular.isUndefined(type)) {
        throw Error('Setting is undefined');
      }
      // Update auto add status
      Service.displayData[type] = !Service.displayData[type];
      // Write to backend
      var permissions = new (Service.permissionsRequest())({val: Service.displayData[type]});
      permissions.permissionType = type;
      return permissions.$updatePermissions();
    },
    /**
     * Set mail settings for the selected user
     * @param id Record ID
     * @param type Broker or user
     */
    setMailSettings: function (id, type) {
      var safeData = Service.safeData[type + 'Mail'];
      safeData.selections = [];
      // Reset if no user is selected
      if (id === 0) {
        return;
      }
      // Get the selected user's permissions
      angular.forEach(safeData[type + 's'], function (user) {
        if (user.id === id) {
          angular.forEach(user.data, function (permission, type) {
            if (permission) {
              safeData.selections.push(type);
            }
          });
        }
      });
    },
    /**
     * Apply permissions to all users or brokers, or else set automated permissions for new users or brokers
     */
    massUpdatePermissions: function (userId, automated) {
      var permissions = {};
      automated = automated || false;
      // Get correct permission selections
      var selectedPermissions = Service.displayData.applyAllType === 'user' ? Service.safeData.userMail.selections :
                                Service.safeData.brokerMail.selections;
      // Create an object to pass to backend
      selectedPermissions.forEach(function (permission) {
        permissions[permission] = true;
      });
      return $http.post(API_PREFIX() + '/v2.0/admin/users/client/set-mail-permissions-all',
      {userId: userId, type: Service.displayData.applyAllType, permissions: selectedPermissions, automated: automated});
    },
    /**
     * Set a single permissions on click
     */
    setSingleMailPermission: function (permission, type) {
      type = type === 'user' ? 'userMail' : 'brokerMail';
      var recordId = Service.safeData[type].selectedUser;
      // Add or remove the selected permission item
      if (Service.safeData[type].selections.indexOf(permission) === -1) {
        Service.safeData[type].selections.push(permission);
      } else {
        Service.safeData[type].selections.splice(Service.safeData[type].selections.indexOf(permission), 1);
      }
      // Write to DB
      return Service.updatePermissions(type + '-mail', {selections: Service.safeData[type].selections, recordId: recordId});
    }
  };

  return Service;
}]);
