'use strict';

var app = angular.module('frontendApp');

app.controller('UserPermissionsCtrl', ['$scope', 'AdminUsersUserPermissionsService', 'AdminUsersClientService',
function ($scope, AdminUsersUserPermissionsService, AdminUsersClientService) {
  var vm = this;
  // Shortened name
  var Service = AdminUsersUserPermissionsService;
  // Hold reference to data
  vm.displayData = Service.displayData;
  vm.safeData = Service.safeData;
  // Start on instruction tab
  vm.selectedTab = 'internalUsers';
  // Tab config
  vm.tabs = ['internalUsers', 'brokerUsers', 'mailPermissions'];
  vm.tabConfig = {
    internalUsers: 'Internal Users',
    brokerUsers: 'Broker Users',
    mailPermissions: 'Mail Permissions'
  };

  /**
   * Get status for auto add new group or broker
   */
  $scope.$watchGroup([function () {
    return Service.displayData.autoAddNewGroupStatus;
  }, function () {
    return Service.displayData.autoAddNewBrokerStatus;
  }], function (newVal) {
    vm.displayData.autoAddNewGroupStatus = newVal[0];
    vm.displayData.autoAddNewBrokerStatus = newVal[1];
  });

  /**
   * Change auto add new groups setting
   */
  vm.toggleSetting = function (type) {
    // Change on backend
    Service.toggleSetting(type).catch(function () {
      // Undo on failure
      Service.displayData[type] = !Service.displayData[type];
      // Display failure to user
      $scope.$broadcast('show-modal', 'general-update-failure', true);
    });
  };

  /**
   * Set a single mail permission
   * @param permissions Permission ID
   * @param type User or broker
   */
  vm.updateMailPermissions = function (permissions, type) {
    // Add if it's not currently set
    Service.setSingleMailPermission(permissions, type);
  };

  /**
  * Watch selected user or broker and set selections appropriately
  */
  $scope.$watch(function () {
    return vm.safeData.userMail.selectedUser;
  }, function (newVal) {
    // Don't run on load
    if (angular.isUndefined(newVal)) {
      return;
    }
    // Retrieve mail settings for the selected user
    AdminUsersUserPermissionsService.setMailSettings(newVal, 'user');
  });
  $scope.$watch(function () {
    return vm.safeData.brokerMail.selectedBroker;
  }, function (newVal) {
    // Don't run on load
    if (angular.isUndefined(newVal)) {
      return;
    }
    // Retrieve mail settings for the selected user
    AdminUsersUserPermissionsService.setMailSettings(newVal, 'broker');
  });
  /**
  * Apply selected permissions to all users or brokers
  * @param type
  */
  vm.confirmApplyPermissionsToAll = function (type) {
    vm.displayData.applyAllType = type;
    // Display confirmation modal
    $scope.$broadcast('show-modal', 'confirm-apply-to-all', true);
  };
  /**
  * Apply permissions to all users or brokers under the currently viewed user
  *
  * @param auto Whether updating automatic permissions for new users or brokers, or updating current users or brokers
  */
  vm.massUpdatePermissions = function (auto) {
    var modal;
    // Hide confirmation modal
    modal = auto ? 'confirm-auto-apply-permissions' : 'confirm-apply-to-all';
    // Update mass permissions
    Service.massUpdatePermissions(AdminUsersClientService.displayData.details.id, auto).then(function () {
      // Show success
      $scope.$broadcast('hide-modal', modal);
      $scope.$broadcast('show-modal', 'update-mass-permissions-success', true);
    }, function () {
      $scope.$broadcast('show-modal', 'update-mass-permissions-failure', true);
    });
  };
  /**
  * Apply selected permissions to all users or brokers
  * @param type
  */
  vm.confirmAutoApplyPermissions = function (type) {
    vm.displayData.applyAllType = type;
    // Display confirmation modal
    $scope.$broadcast('show-modal', 'confirm-auto-apply-permissions', true);
  };
}]);
