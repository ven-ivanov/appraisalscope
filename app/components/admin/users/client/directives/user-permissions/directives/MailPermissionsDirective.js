'use strict';

var app = angular.module('frontendApp');

app.directive('mailPermissions', [function () {
  return {
    templateUrl: '/components/admin/users/client/directives/user-permissions/directives/partials/mail-permissions.html',
    scope: {
      type: '@',
      selectModel: '=',
      selectOptions: '=',
      permissionTypes: '=',
      selectionModels: '=',
      updatePermissions: '&',
      applyPermissionsToAll: '&',
      autoApplyPermissions: '&'
    }
  };
}]);