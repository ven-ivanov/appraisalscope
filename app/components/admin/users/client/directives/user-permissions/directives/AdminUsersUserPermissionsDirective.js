'use strict';

var app = angular.module('frontendApp');

/**
 * User settings directive
 */
app.directive('adminUsersUserPermissions', [function () {
  return {
    templateUrl: '/components/admin/users/client/directives/user-permissions/directives/partials/permissions.html',
    controller: 'UserPermissionsCtrl',
    controllerAs: 'permissionsCtrl'
  };
}]);