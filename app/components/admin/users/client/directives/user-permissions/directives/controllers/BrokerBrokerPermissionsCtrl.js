'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - User Permissions - Groups
 */
app.controller('BrokerBrokerPermissionsCtrl',
['$scope', 'AdminUsersUserPermissionsService', 'AdminUsersUserPermissionsCtrlInheritanceService',
function ($scope, AdminUsersUserPermissionsService, AdminUsersUserPermissionsCtrlInheritanceService) {
 var vm = this;
  // Heading
  vm.heading = [// Name
    {
      label: 'Name',
      data: 'name',
      noSort: true
    }, // Whether currently active
    {
      label: 'Active',
      data: 'active',
      noSort: true,
      checkbox: true
    }];

  // Inherit common table functionality
  AdminUsersUserPermissionsCtrlInheritanceService.inherit.call(vm, $scope, 'broker', true);
}]);