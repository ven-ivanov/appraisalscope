'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - User Permissions - Groups
 */
app.controller('BrokerUserPermissionsCtrl',
['$scope', 'AdminUsersUserPermissionsCtrlInheritanceService',
 function ($scope, AdminUsersUserPermissionsCtrlInheritanceService) {
   var vm = this;
   // Heading
   vm.heading = [
     // Name
     {
       label: 'Name',
       data: 'name',
       noSort: true
     }
   ];

   // Inherit common table functionality for this section
   AdminUsersUserPermissionsCtrlInheritanceService.inherit.call(vm, $scope, 'brokerUser');
 }]);