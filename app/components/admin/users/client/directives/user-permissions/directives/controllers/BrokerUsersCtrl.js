'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - User Permissions - Groups
 */
app.controller('BrokerUsersCtrl', ['$scope', 'AdminUsersUserPermissionsCtrlInheritanceService',
function ($scope, AdminUsersUserPermissionsCtrlInheritanceService) {
   var vm = this;
  // Heading
  vm.heading = [// Name
    {
      label: 'Name',
      data: 'name',
      noSort: true
    }, // Whether currently active
    {
      label: 'Active',
      data: 'active',
      noSort: true,
      checkbox: true
    }];

  AdminUsersUserPermissionsCtrlInheritanceService.inherit.call(vm, $scope, 'brokerUser', true);
}]);