'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - User Settings - Checklist controller
 */
app.controller('UserPermissionsPermissionsCtrl',
['$scope', 'AdminUsersUserPermissionsCtrlInheritanceService',
 function ($scope, AdminUsersUserPermissionsCtrlInheritanceService) {
   var vm = this;
   // Heading
   vm.heading = [
     // Name
     {
       label: 'Name',
       data: 'name',
       noSort: true
     }
   ];

   // Inherit table functionality
   AdminUsersUserPermissionsCtrlInheritanceService.inherit.call(vm, $scope, 'permissions', true);
 }]);