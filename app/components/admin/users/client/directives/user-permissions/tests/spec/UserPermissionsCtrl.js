'use strict';

describe('UserPermissionsCtrl', function () {
  var scope, controller, adminUsersUserPermissionsService, httpBackend, adminUsersClientService;

  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersUserPermissionsService', AdminUsersUserPermissionsServiceMock);
      $provide.factory('AdminUsersClientService', AdminUsersClientServiceMock);
    });
  });
  beforeEach(inject(function ($rootScope, $controller, AdminUsersUserPermissionsService, $httpBackend, AdminUsersClientService) {
    scope = $rootScope.$new();
    adminUsersUserPermissionsService = AdminUsersUserPermissionsService;
    adminUsersClientService = AdminUsersClientService;
    httpBackend = $httpBackend;
    controller = $controller('UserPermissionsCtrl', {$scope: scope});
    scope.$digest();
  }));

  describe('toggleSetting', function () {
    beforeEach(function () {
      controller.displayData.toggleTest = true;
      controller.displayData.toggleFail = true;
      spyOn(adminUsersUserPermissionsService, 'toggleSetting').and.callThrough();
    });
    it('should toggle settings', function () {
      controller.toggleSetting('toggleTest');
      scope.$digest();
      expect(adminUsersUserPermissionsService.toggleSetting).toHaveBeenCalled();
      expect(controller.displayData.toggleTest).toEqual(false);
    });

    it('should revert setting on failure', function () {
      controller.toggleSetting('toggleFail');
      scope.$digest();
      expect(controller.displayData.toggleTest).toEqual(true);
    });
  });

  describe('updateMailPermissions()', function () {
    xit('should ', function () {

    });
  });


});