'use strict';

describe('AdminUsersUserPermissionsService', function () {
  var adminUsersUserPermissionsService, adminUsersClientService, httpBackend, scope, resource;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersClientService', AdminUsersClientServiceMock);
    });
  });

  beforeEach(inject(function (AdminUsersUserPermissionsService, AdminUsersClientService, $httpBackend, $rootScope, $resource) {
    adminUsersUserPermissionsService = AdminUsersUserPermissionsService;
    adminUsersClientService = AdminUsersClientService;
    httpBackend = $httpBackend;
    scope = $rootScope.$new();
    resource = $resource;
    
    httpBackend.whenGET(/permissions/).respond([{permissions: true}]);
    httpBackend.whenPATCH(/permissions/).respond();

    // Mocking abstract state template
    httpBackend.expectGET(/base/).respond();
  }));

  describe('checkGroups()', function () {
    it('should return an object of all default checked records', function () {
      var records = [
        {
          active: true,
          id: 1
        },
        {
          active: true,
          id: 2
        },
        {
          active: false,
          id: 3
        }
      ];
      expect(adminUsersUserPermissionsService.checkGroups(records)).toEqual({1: true, 2: true});
      // Update records
      records[0].active = false;
      expect(adminUsersUserPermissionsService.checkGroups(records)).toEqual({2: true});
      records[1].active = false;
      expect(adminUsersUserPermissionsService.checkGroups(records)).toEqual({});
    });
  });
  // Fake mail permissions
  var permissions = [
    {
      id: 1,
      data: {
        accepted: 0,
        additionalStatus: 1,
        blahStatus: 1
      }
    },
    {
      id: 2,
      data: {
        accepted: 1,
        additionalStatus: 0,
        blahStatus: 1
      }
    }
  ];

  describe('setMailSettings()', function () {
    var id;
    beforeEach(function () {
      id = 1;
      adminUsersUserPermissionsService.safeData.userMail.users = [{id: 1, data: {testPermission: true, badPermissions: false}}, {id: 2, data: {otherPermissions: true}}];
      adminUsersUserPermissionsService.safeData.brokerMail.brokers = [{id: 1, data: {testPermission: true}}];
    });
    it('should update model with permissions of the selected user', function () {
      adminUsersUserPermissionsService.setMailSettings(id, 'user');
      expect(adminUsersUserPermissionsService.safeData.userMail.selections).toEqual(['testPermission']);
    });

    it('should update permissions on a broker', function () {
      adminUsersUserPermissionsService.setMailSettings(id, 'broker');
      expect(adminUsersUserPermissionsService.safeData.brokerMail.selections).toEqual(['testPermission']);
    });

    it('should return empty if an invalid user is passed in', function () {
      id = 3;
      var response = adminUsersUserPermissionsService.setMailSettings(id, 'user');
      expect(response).toEqual();
    });

    it('should clear settings and return another users settings', function () {
      // Set for user 1
      adminUsersUserPermissionsService.setMailSettings(id, 'user');
      id = 2;
      adminUsersUserPermissionsService.setMailSettings(id, 'user');
      expect(adminUsersUserPermissionsService.safeData.userMail.selections).toEqual(['otherPermissions']);
    });
  });

  describe('getUserPermission', function () {
    it('should set the proper permissions on display data', function () {
      adminUsersUserPermissionsService.getUserPermission('user', 'userPerm');
      httpBackend.flush();
      scope.$digest();
      expect(adminUsersUserPermissionsService.displayData.userPerm[0].permissions).toEqual(true);
    });
  });

  describe('getUserMailPermissions', function () {
    beforeEach(function () {
      adminUsersUserPermissionsService.getUserMailPermissions();
      httpBackend.flush();
      scope.$digest();
    });
    it('should concatenate the response with the default user', function () {
      expect(adminUsersUserPermissionsService.safeData.userMail.users[0]).toEqual({ name: '', id: 0 });
      expect(adminUsersUserPermissionsService.safeData.userMail.users[1].permissions).toEqual(true);
    });
    it('should set the default selected user', function () {
      expect(adminUsersUserPermissionsService.safeData.userMail.selectedUser).toEqual(0);
    });
  });

  describe('getBrokerMailPermissions', function () {
    beforeEach(function () {
      adminUsersUserPermissionsService.getBrokerMailPermissions();
      httpBackend.flush();
      scope.$digest();
    });
    it('should concatenate the response with the default broker', function () {
      expect(adminUsersUserPermissionsService.safeData.brokerMail.brokers[0]).toEqual({ name: '', id: 0 });
      expect(adminUsersUserPermissionsService.safeData.brokerMail.brokers[1].permissions).toEqual(true);
    });
    it('should set the default selected broker', function () {
      expect(adminUsersUserPermissionsService.safeData.brokerMail.selectedBroker).toEqual(0);
    });
  });

  describe('toggleSetting', function () {
    var response;
    beforeEach(function () {
      adminUsersUserPermissionsService.displayData.test = true;
      adminUsersUserPermissionsService.toggleSetting('test');
      httpBackend.flush();
      scope.$digest();
    });

    it('should update the setting', function () {
      expect(adminUsersUserPermissionsService.displayData.test).toEqual(false);
    });
    it('should throw an error if no setting is passed in', function () {
      try {
        adminUsersUserPermissionsService.toggleSetting();
      } catch (e) {
        expect(e.message).toEqual('Setting is undefined');
      }
    });
  });

  describe('massUpdatePermissions()', function () {
    var http;
    beforeEach(inject(function ($http) {
      http = $http;
      spyOn(http, 'post');
    }));
    var id = 1;
    var automated = false;
    it('should apply the permissions for the current set to all users and send request to backend', function () {
      adminUsersUserPermissionsService.safeData.userMail.selections = ['blahModel', 'fakeModel'];
      adminUsersUserPermissionsService.displayData.applyAllType = 'user';
      adminUsersUserPermissionsService.massUpdatePermissions(id, automated);
      expect(http.post).toHaveBeenCalledWith('//localhost:3000/v2.0/admin/users/client/set-mail-permissions-all', {
        userId: 1,
        type: 'user',
        permissions: ['blahModel', 'fakeModel'],
        automated: false
      });
    });

    it('should also work with brokers', function () {
      adminUsersUserPermissionsService.safeData.userMail.selections = ['something', 'otherModel'];
      adminUsersUserPermissionsService.displayData.applyAllType = 'broker';
      adminUsersUserPermissionsService.massUpdatePermissions(id, automated);
      expect(http.post.calls.mostRecent().args).toEqual( [ '//localhost:3000/v2.0/admin/users/client/set-mail-permissions-all', { userId: 1, type: 'broker', permissions: [  ], automated: false } ]);
    });
  });

  describe('setSingleMailPermission()', function () {
    beforeEach(function () {
      adminUsersUserPermissionsService.safeData.userMail.selectedUser = 1;
      adminUsersUserPermissionsService.safeData.brokerMail.selectedBroker = 1;
    });
    it('should set a permission when it is checked', function () {
      adminUsersUserPermissionsService.setSingleMailPermission('something', 'user');
      expect(adminUsersUserPermissionsService.safeData.userMail.selections).toEqual(['something']);
    });
    it('should work with brokers', function () {
      adminUsersUserPermissionsService.setSingleMailPermission('somethingElse', 'broker');
      expect(adminUsersUserPermissionsService.safeData.brokerMail.selections).toEqual(['somethingElse']);
    });
    it('should be able to add multiple selections', function () {
      adminUsersUserPermissionsService.setSingleMailPermission('something', 'user');
      adminUsersUserPermissionsService.setSingleMailPermission('somethingOther', 'user');
      expect(adminUsersUserPermissionsService.safeData.userMail.selections).toEqual(['something', 'somethingOther']);
    });
    it('should be able to remove permissions', function () {
      adminUsersUserPermissionsService.setSingleMailPermission('something', 'user');
      adminUsersUserPermissionsService.setSingleMailPermission('something', 'user');
      expect(adminUsersUserPermissionsService.safeData.userMail.selections).toEqual([]);
    });
  });
});