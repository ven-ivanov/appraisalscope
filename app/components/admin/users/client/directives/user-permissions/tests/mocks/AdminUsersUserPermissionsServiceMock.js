var AdminUsersUserPermissionsServiceMock = function ($q) {
  return {
    safeData: {
      userMail: {
        // Mail users
        users: [],
        // Hold mail selections
        selections: [],
        // Selected user
        selectedUser: 0
      },
      brokerMail: {
        // Brokers
        brokers: [],
        // Selected mail items
        selections: [],
        // Selected broker
        selectedBroker: 0
      }
    },
    displayData: {},
    mailPermissionTypes: {
      type1: 'Type One',
      type2: 'Type Two'
    },
    toggleSetting: function (type) {
      this.displayData[type] = !this.displayData[type];
      return $q(function (resolve, reject) {
        if (type === 'toggleTest') {
          resolve();
        } else {
          reject();
        }
      });
    },
    setMailSettings: function () {

    }
  };
};