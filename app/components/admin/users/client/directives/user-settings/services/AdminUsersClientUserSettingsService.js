'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Client - User settings service
 *
 * @todo Still in flux
 * @link https://github.com/ascope/manuals/issues/57
 */
app.factory('AdminUsersClientUserSettingsService',
['AdminUsersClientService', '$http', 'API_PREFIX', 'AdminUsersService', 'AdminUsersResourceService', 'AsDataService',
function (AdminUsersClientService, $http, API_PREFIX, AdminUsersService, AdminUsersResourceService, AsDataService) {
  var Service = {
    // Safe data
    safeData: {
      // Setting - new API
      settings: {},
      // Email permissions
      emailPermissions: {},
      // General settings
      general: {}
    },
    // Data for display
    displayData: {
      // Settings - new API
      settings: [],
      // General data
      general: [],
      // Email permissions
      emailPermissions: [
        {
          id: 1,
          name: 'Inspection scheduled'
        },
        {
          id: 2,
          name: 'Inspection complete'
        },
        {
          id: 3,
          name: 'On hold'
        },
        {
          id: 4,
          name: 'Additional status'
        },
        {
          id: 5,
          name: 'Completed appraisal'
        },
        {
          id: 6,
          name: 'In review'
        },
        {
          id: 7,
          name: 'Accepted'
        },
        {
          id: 8,
          name: 'Email office'
        },
        {
          id: 9,
          name: 'Declined'
        },
        {
          id: 10,
          name: 'Assigned'
        },
        {
          id: 11,
          name: 'Emailed documents'
        },
        {
          id: 12,
          name: 'Accepted with conditions'
        },
        {
          id: 13,
          name: 'New order confirmation'
        }
      ],
      // Company, branch, individual dropdown
      companyBranchIndividual: [{id: 1, name: 'Company'}, {id: 2, name: 'Branch'},{id: 3, name: 'Individual'}],
      // Payment form
      paymentForm: [{id: 1, name: 'Company'}, {id: 2, name: 'Branch'},{id: 3, name: 'Individual'}, {id: 4, name: 'No payment'}],
      // Source enum
      // @todo this is still just a guess at how the backend will actually be
      sourceEnum: [{id: 'company', name: 'Company'}, {id: 'branch', name: 'Branch'}, {id: 'individual', name: 'Individual'}]
    },
    /**
     * Retrieve user settings
     */
    getSettings: function () {
      // Mass settings
      if (AdminUsersService.record.massUpdateUsers) {
        return Service.request('getSettingsMass')
        .then(function (response) {
          // Don't need traditional hashing here
          Service.safeData.settings = angular.copy(response);
        });
        // Single user settings
      } else {
        return Service.request('getSettings')
        .then(function (response) {
          // Don't need traditional hashing here
          Service.safeData.settings = angular.copy(response);
        });
      }
    },
    /**
     * Update user settings
     * @param values Object User settings persistable object
     */
    updateSettings: function (values) {
      // Mass update
      if (AdminUsersService.record.massUpdateUsers) {
        return Service.request('updateSettingsMass', {body: values});
      }
      // Single user update
      return Service.request('updateSettings', {body: values});
    },
    /**
     * Toggle lock values of a button
     * @param button String
     */
    toggleLockValue: function (button) {
      try {
        switch (button) {
          case 'emailPermissions':
            Service.safeData.settings.events.internal.canEdit = !Service.safeData.settings.events.internal.canEdit;
            break;
          case 'documents':
            Service.safeData.settings.appraisals.documents.canUpload = !Service.safeData.settings.appraisals.documents.canUpload;
            break;
          case 'appraisals':
            Service.safeData.settings.appraisals.canCreate = !Service.safeData.settings.appraisals.canCreate;
            break;
        }
      } catch (e) {
        return '';
      }
    },
    /**
     * Toggle one of the email permissions
     * @param id int
     */
    toggleEmailPermissions: function (id) {
      // Current index of email permission
      var index = Service.safeData.settings.events.internal.allowable.indexOf(id);
      // If currently exists, splice out
      if (index !== -1) {
        Service.safeData.settings.events.internal.allowable.splice(index, 1);
      } else {
        // Toggle active
        Service.safeData.settings.events.internal.allowable.push(id);
      }
    }

  };

  // Inherit
  Service.request = AdminUsersService.request.bind(Service, 'userSettings');
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service);
  Service.transformData = AsDataService.transformData.bind(Service);

  return Service;
}]);