'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Client - User settings ctrl
 */
app.controller('AdminUsersClientUserSettingsCtrl',
['$scope', 'AdminUsersClientUserSettingsService', function ($scope, AdminUsersClientUserSettingsService) {
  var vm = this;
  var Service = AdminUsersClientUserSettingsService;
  // Keep reference to display data
  vm.displayData = Service.displayData;
  // Keep reference to safe data
  vm.safeData = Service.safeData;

  /**
   * Initiate data load
   */
  vm.init = function () {
    Service.getSettings()
    // Attach watcher
    .then(function () {
      vm.attachUpdateWatcher();
    });
  };

  // Attach watch for settings change
  vm.attachUpdateWatcher = function () {
    /**
     * Update settings data
     */
    $scope.$watch(function () {
      return vm.safeData.settings;
    }, function (newVal, oldVal) {
      if (angular.isUndefined(newVal) || angular.equals(oldVal, newVal)) {
        return;
      }
      // make request
      Service.updateSettings(newVal)
        // Failed to update
      .catch(function () {
        // Show failure
        $scope.$broadcast('show-modal', 'update-setting-failure', true);
        $scope.scrollTop('#user-details');
        // Undo change
        vm.safeData.general = oldVal;
      });
    }, true);
  };

  // Update one of the lock buttons
  vm.toggleLockValue = Service.toggleLockValue;
  // Update email permissions
  vm.toggleEmailPermissions = Service.toggleEmailPermissions;

  /**
   * Display email permissions
   */
  vm.displayEmailPermissions = function () {
    $scope.$broadcast('show-modal', 'email-notification-permissions', true);
    $scope.scrollTop('#user-details');
  };

  /**
   * Display payment form options
   *
   * @todo When I get some clarifications on the backend
   */
  vm.paymentFormOptions = function () {
    $scope.$broadcast('show-modal', 'payment-form-options', true);
  };

  // Initialize the view
  vm.init();
}]);