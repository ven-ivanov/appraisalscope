'use strict';

describe('AdminUsersClientUserSettingsService', function () {
  var adminUsersClientService, Service, scope, httpBackend, adminUsersService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersClientService', AdminUsersClientServiceMock);
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function (AdminUsersClientService, AdminUsersClientUserSettingsService, $rootScope, $httpBackend, AdminUsersService) {
    adminUsersClientService = AdminUsersClientService;
    Service = AdminUsersClientUserSettingsService;
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    adminUsersService = AdminUsersService;
    // Set for user
    adminUsersService.record = {company: {id: 1}, branch: {id: 1}, _type: 'user'};
    // requess
    httpBackend.whenGET(/settings/).respond({setting1: true, setting2: false});

    spyOn(Service, 'request').and.callThrough();
  }));

  describe('getSettings', function () {
    describe('single', function () {
      beforeEach(function () {
        Service.getSettings();
        httpBackend.flush();
        scope.$digest();
      });
      it('should set all settings onto safe data', function () {
        expect(Service.safeData.settings.setting1).toEqual(true);
        expect(Service.safeData.settings.setting2).toEqual(false);
      });
    });

    describe('mass', function () {
      beforeEach(function () {
        adminUsersService.record.massUpdateUsers = [1,2];
        Service.getSettings();
        scope.$digest();
      });

      it('should have made an empty request', function () {
        expect(Service.request.calls.argsFor(0)).toEqual([ 'getSettingsMass' ]);
      });
      it('should set settings to an empty object', function () {
        expect(Service.safeData.settings).toEqual({});
      });
    });
  });

  describe('updateSettings', function () {
    describe('mass update', function () {
      it('should make a request for multiple users', function () {
        adminUsersService.record.massUpdateUsers = [1,2];
        Service.updateSettings({id: 1});
        expect(Service.request).toHaveBeenCalledWith('updateSettingsMass', { body: { id: 1 } });
      });
    });
    describe('single udpate', function () {
      it('should make a request for a single user', function () {
        Service.updateSettings({id: 1});
        expect(Service.request).toHaveBeenCalledWith('updateSettings', { body: { id: 1 } });
      });
    });
  });

  describe('toggleLockValue', function () {
    it('should update documents lock value', function () {
      Service.safeData = {settings: {appraisals: {documents: {canUpload: true}}}};
      Service.toggleLockValue('documents');
      expect(Service.safeData.settings.appraisals.documents.canUpload).toEqual(false);
    });

    it('should update appraisals lock value', function () {
      Service.safeData = {settings: {appraisals: {canCreate: true}}};
      Service.toggleLockValue('appraisals');
      expect(Service.safeData.settings.appraisals.canCreate).toEqual(false);
    });
    it('should update email permissions lock value', function () {
      Service.safeData = {settings: {events: {internal: {canEdit: true}}}};
      Service.toggleLockValue('emailPermissions');
      expect(Service.safeData.settings.events.internal.canEdit).toEqual(false);
    });
  });

  describe('toggleEmailPermissions', function () {
    beforeEach(function () {
      Service.safeData = {settings: {events: {internal: {allowable: [1,2,3]}}}};
    });

    it('should splice out an existing value', function () {
      Service.toggleEmailPermissions(1);
      expect(Service.safeData.settings.events.internal.allowable).toEqual([2,3]);
    });
    it('should add another value in', function () {
      Service.toggleEmailPermissions(4);
      expect(Service.safeData.settings.events.internal.allowable).toEqual([1,2,3,4]);
    });
  });
});