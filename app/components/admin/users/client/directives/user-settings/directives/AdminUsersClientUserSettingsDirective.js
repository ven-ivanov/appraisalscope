'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Client - User settings
 */
app.directive('adminUsersClientUserSettings', ['AdminUsersDirectiveInheritanceService', function (AdminUsersDirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/client/directives/user-settings/directives/partials/user-settings.html',
    controller: 'AdminUsersClientUserSettingsCtrl',
    controllerAs: 'userSettingsCtrl',
    link: function (scope) {
      AdminUsersDirectiveInheritanceService.inherit.call(scope);
    }
  };
}]);