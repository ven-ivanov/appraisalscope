'use strict';

var app = angular.module('frontendApp');

/**
 * Compile the admin users - client - user details display
 */
app.directive('adminUsersClientUserAdditionalDetails', ['DirectiveConditionalLoadService', function (DirectiveConditionalLoadService) {
  return {
    compile: function () {
      return {
        pre: function (scope, elem) {
          // Display content in an info modal
          var template = ['<info-modal ng-if="adminUsersCtrl.displayData.type === \'user\'" modal-id="user-additional-details" modal-title="User Details" button-text="Close" modal-width="100%" modal-height="max" modal-class="notification-window">',
          // Modal tabs
                          '<as-tabs tab-config="adminUsersCtrl.detailsTabsConfigUser" tabs="adminUsersCtrl.detailsTabsUser" tab-width="7ths" selected-tab="adminUsersCtrl.selectedAdditionalDetailsTab" change-tab="adminUsersCtrl.changeTab(tab)"></as-tabs>',
          // Individual tabs
                          '<div ng-switch="adminUsersCtrl.selectedAdditionalDetailsTab">',
                          '<admin-users-notes ng-switch-when="notes" hide="isDisplayed"></admin-users-notes>',
                          '<admin-users-client-on-report ng-switch-when="clientOnReport"></admin-users-client-on-report>',
                          '<admin-users-client-appraiser-panel ng-switch-when="appraiserPanel"></admin-users-client-appraiser-panel>',
                          '<admin-users-client-checklist ng-switch-when="addChecklist"></admin-users-client-checklist>',
                          '<admin-users-client-user-settings ng-switch-when="userSettings"></admin-users-client-user-settings>',
                          '<admin-users-client-instructions ng-switch-when="instructions"></admin-users-client-instructions>',
                          '<admin-users-client-fee-schedule ng-switch-when="feeSchedule"></admin-users-client-fee-schedule>',
                          '</div>', '</info-modal>'];
          // Compile the directive only when necessary and then load it
          DirectiveConditionalLoadService.init.call(scope, template, elem, 'user-additional-details');
        }
      };
    }
  };
}]);