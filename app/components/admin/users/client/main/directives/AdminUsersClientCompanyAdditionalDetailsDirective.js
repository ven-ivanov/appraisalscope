'use strict';

var app = angular.module('frontendApp');

/**
 * Conditionally compile and attach company details in admin users client section
 */
app.directive('adminUsersClientCompanyAdditionalDetails', ['DirectiveConditionalLoadService', function (DirectiveConditionalLoadService) {
  return {
    compile: function () {
      return {
        pre: function (scope, elem) {
            // Display content in an info modal
            var template = ['<info-modal ng-if="adminUsersCtrl.displayData.type === \'company\' || adminUsersCtrl.displayData.type === \'branch\' || adminUsersCtrl.displayData.type === \'mass-user\'" modal-id="company-additional-details" modal-title="Company Details" button-text="Close" modal-width="100%" modal-height="max" modal-class="notification-window">',
                            '<as-tabs tab-config="adminUsersCtrl.detailsTabsConfigCompany" tabs="adminUsersCtrl.detailsTabsCompany" tab-width="9ths" selected-tab="adminUsersCtrl.selectedAdditionalDetailsTab" change-tab="adminUsersCtrl.changeTab(tab)"></as-tabs>',
                            '<div ng-switch="adminUsersCtrl.selectedAdditionalDetailsTab">',
                            '<admin-users-notes ng-switch-when="notes" hide="isDisplayed"></admin-users-notes>',
                            '<admin-users-client-on-report ng-switch-when="clientOnReport"></admin-users-client-on-report>',
                            '<admin-users-client-appraiser-panel ng-switch-when="appraiserPanel"></admin-users-client-appraiser-panel>',
                            '<admin-users-client-auto-assign ng-switch-when="autoAssign"></admin-users-client-auto-assign>',
                            '<admin-users-client-checklist ng-switch-when="addChecklist"></admin-users-client-checklist>',
                            '<admin-users-add-user ng-switch-when="addUsers" hide="appraiserPermissions"></admin-users-add-user>',
                            '<admin-users-client-company-settings ng-switch-when="companySettings"></admin-users-client-company-settings>',
                            '<admin-users-client-instructions ng-switch-when="instructions"></admin-users-client-instructions>',
                            '<admin-users-client-fee-schedule ng-switch-when="feeSchedule"></admin-users-client-fee-schedule>',
                            '</div></info-modal>'
            ];

          // Compile the directive only when necessary and then load it
          DirectiveConditionalLoadService.init.call(scope, template, elem, 'company-additional-details');
        }
      };
    }
  };
}]);