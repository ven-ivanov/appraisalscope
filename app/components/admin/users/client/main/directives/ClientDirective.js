'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users client sub-tab
 */
app.directive('adminUsersClient', ['DirectiveInheritanceService', '$compile', function (DirectiveInheritanceService, $compile) {
  return {
    templateUrl: '/components/admin/users/client/main/directives/partials/client.html',
    controller: 'AdminUsersClientCtrl',
    controllerAs: 'adminUsersCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
      // Compile directives
      scope.compileDirectives = function () {
        // User settings summary
        angular.element('#admin-users-user-settings-summary').replaceWith($compile('<admin-users-user-settings-summary ng-if="adminUsersCtrl.settingsActive"></admin-users-user-settings-summary>')(scope));
        // User permissions
        angular.element('#admin-users-user-permissions').replaceWith($compile('<admin-users-user-permissions ng-if="adminUsersCtrl.permissionsActive"></admin-users-user-permissions>')(scope));
        // Company statistics
        /**
         * @todo Listed as still in progress on the backend -- 8/4
         * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Companies/Scorecard/Index.md#get-clientscompaniescompanyidscorecard-in-progress
         */
        angular.element('#company-scorecard').replaceWith($compile('<info-modal modal-id="company-scorecard" modal-title="Scorecard" button-text="Close" modal-width="100%"><as-table ng-controller="companyScorecardCtrl as tableCtrl" totals="false" hide-page-numbers="true" table-id="company-scorecard-table" bold-columns="1,2,3" infinite-scroll="false"></as-table></info-modal>')(scope));
        // New company
        angular.element('#new-company').replaceWith($compile('<admin-users-new-company ctrl="adminUsersCtrl"></admin-users-new-company>')(scope));
        scope.compileDirectives = angular.noop;
      };

      /**
       * Recompile table to handle conditional link functions
       */
      var template = '<div id="client-table"><as-table ng-controller="AdminUsersClientTableCtrl as tableCtrl" totals="false" bold-columns="2" table-id="search-results-table" infinite-scroll="false" overflow-x="hidden" hide-page-numbers="true"></as-table> </div>';
      scope.compileTable = function () {
        angular.element('#client-table').replaceWith($compile(template)(scope));
      };
    }
  };
}]);
