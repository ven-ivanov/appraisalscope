'use strict';

var app = angular.module('frontendApp');

app.controller('companyScorecardCtrl',
['AdminUsersClientService', 'AsTableService', '$scope', function (AdminUsersClientService, AsTableService, $scope) {
  var vm = this;

  /**
   * Table column headers
   */
  vm.heading = [
    // Date
    {
      label: 'Date',
      data: 'date',
      filter: 'date:MM-yyyy'
    },
    // Total number of completed appraisals
    {
      label: 'Total Completed Appraisals',
      data: 'totalCompletedAppraisals'
    },
    // On time % (completed by due date)
    {
      label: 'Percentage of Appraisals Completed by Due Date',
      data: 'onTime'
    },
    // Acceptance to completion days
    {
      label: 'Average Days from Acceptance to Completion',
      data: 'acceptanceToCompletionDays'
    },
    // Assigned to accepted days
    {
      label: 'Average Days from Assigned to Completed',
      data: 'assignedToCompletedDays'
    },
    // Inspection to completion days
    {
      label: 'Average Days from Inspected to Completed',
      data: 'inspectionToCompletionDays'
    },
    // Total number of reviewer revisions
    {
      label: 'Total Number of Reviewer Revisions',
      data: 'totalReviewerRevisions'
    },
    // Total number of client revisions
    {
      label: 'Total Number of Client Revisions',
      data: 'totalClientRevisions'
    },
    // Total number of reconsideration requests
    {
      label: 'Total Number of Reconsideration Requests',
      data: 'totalReconsiderationRequests'
    },
    // Reviewer revision rate
    {
      label: 'Reviewer Revision Rate',
      data: 'reviewerRevisionRate'
    },
    // Client revision rate
    {
      label: 'Client Revision Rate',
      data: 'clientRevisionRate'
    },
    // Revision TAT
    {
      label: 'Revision TAT',
      data: 'revisionTurnaroundTime'
    }
  ];

  /**
   * Display scorecard data
   */
  $scope.$watchCollection(function () {
    return AdminUsersClientService.displayData.scorecard;
  }, function (newVal) {
    if (!angular.isArray(newVal) || !newVal.length) {
      return;
    }
    vm.rowData = newVal;
    vm.tableData = newVal.slice();
  });
}]);