'use strict';

var app = angular.module('frontendApp');

/**
 * Search table for clients and users
 */
app.controller('AdminUsersClientTableCtrl',
['AdminUsersClientService', 'AsTableService', '$scope', '$q', 'AdminUsersTableCtrlInheritanceService', 'AdminUsersService',
function (AdminUsersClientService, AsTableService, $scope, $q, AdminUsersTableCtrlInheritanceService, AdminUsersService) {
  var vm = this;
  /**
   * @TODO Pagination - should use the pagination directive from as-table
   * @link https://github.com/ascope/front-end/issues/355
   * 8/3
   */
  vm.page = {displayPage: 1};

  // Don't retrieve records on recompile
  $scope.recompile = $scope.$parent.recompile;

  // Filter table by property
  vm.filterByProp = {};

  // Inherit table controller methods
  AdminUsersTableCtrlInheritanceService.inherit.call(vm, $scope, AdminUsersClientService, 'client');

  /**
   * Show users for a specific company
   * @param id
   */
  vm.linkFn = function (id) {
    // Get users
    AdminUsersClientService.getCompanyUsers(id);
    // Display modal with users
    $scope.$parent.$broadcast('show-modal', 'company-users');
  };
  /**
   * Show record details
   */
  vm.rowFn = function (id) {
    // Detach watcher
    if (angular.isFunction(AdminUsersService.detachUpdateWatcher)) {
      AdminUsersService.detachUpdateWatcher();
    }
    // Get details
    AdminUsersClientService.getRecordDetails(id).then(function () {
      // Attach update watcher once the record is retrieved
      AdminUsersService.attachUpdateWatcher.call(vm, $scope, AdminUsersClientService);
    });
  };

  /********************
   * @todo All of this below here is todo when we have a backend
   */
  ///**
  // * Previous page
  // */
  //vm.prevPage = function () {
  //  return $q(function (resolve) {
  //    resolve();
  //  });
  //};
  ///**
  // * Next page
  // */
  //vm.nextPage = function () {
  //  return $q(function (resolve) {
  //    resolve();
  //  });
  //};
  //
  ///**
  // * Search on any property
  // */
  //$scope.$watch(function () {
  //  return AdminUsersClientService.filterAll;
  //}, function (newVal) {
  //  if (angular.isUndefined(newVal)) {
  //    return;
  //  }
  //  AdminUsersClientService.filterByProp.call(vm, newVal, true);
  //});
  ///**
  // * Filter by property
  // */
  //$scope.$watchCollection(function () {
  //  return vm.filterByProp;
  //}, function (newVal) {
  //  AdminUsersClientService.filterByProp.call(vm, newVal);
  //});
}]);