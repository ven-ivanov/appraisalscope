'use strict';

var app = angular.module('frontendApp');

/**
 * @todo Get branches with companies
 * @link https://github.com/ascope/manuals/issues/19
 */

app.controller('AdminUsersClientCtrl',
['$scope', 'AdminUsersCtrlInheritanceService', 'AdminUsersConfigService', 'AdminUsersClientService',
 '$filter', '$stateParams', '$timeout', 'DomLoadService', 'AdminUsersService', function (
$scope, AdminUsersCtrlInheritanceService, AdminUsersConfigService, AdminUsersClientService, $filter,
$stateParams, $timeout, DomLoadService, AdminUsersService) {
  var vm = this;
  // Display data
  vm.displayData = AdminUsersClientService.displayData;
  // Safe data
  vm.safeData = AdminUsersClientService.safeData;
  // Inherit common controller attributes
  AdminUsersCtrlInheritanceService.createCtrl.call(vm, AdminUsersClientService, $scope, 'client');
  vm.selects = {};
  // Reference to details on the currently selected user or company
  vm.details = AdminUsersClientService.details;
  // Tabs for the admin users clients details pane
  vm.detailsPaneTabs = AdminUsersConfigService.getClientDetailsPaneTabs();
  // Set active class on currently active tab
  vm.tabClass = function (tab) {
    return AdminUsersClientService.getDetailsTabClass(tab, vm.displayData.tab);
  };
  vm.activateDataModal = AdminUsersClientService.activateDataModal.bind(vm);
  /**
   * User details modal
   */
  // Tab config
  vm.detailsTabsConfigUser = {
    notes: 'Notes',
    clientOnReport: 'Client On Report',
    appraiserPanel: 'Appraiser Panel',
    addChecklist: 'Add Checklist',
    userSettings: 'User Settings',
    instructions: 'Instructions',
    feeSchedule: 'Fee Schedule'
  };
  // Tab config (need this to an array since objects don't guarantee order)
  vm.detailsTabsUser =
    ['notes', 'clientOnReport', 'appraiserPanel', 'addChecklist', 'userSettings', 'instructions', 'feeSchedule'];
  // Start off on notes tab
  AdminUsersService.currentDetailsTab = vm.currentDetailsTab;

  /**
   * Company details modal
   */
  // Tab config
  vm.detailsTabsConfigCompany = {
    notes: 'Notes',
    clientOnReport: 'Client On Report',
    appraiserPanel: 'Appraiser Panel',
    autoAssign: 'Auto Assign',
    addChecklist: 'Add Checklist',
    addUsers: 'Add Users',
    companySettings: 'Company Settings',
    instructions: 'Instructions',
    feeSchedule: 'Fee Schedule'
  };
  // Tab config
  vm.detailsTabsCompany =
  ['notes', 'clientOnReport', 'appraiserPanel', 'autoAssign', 'addChecklist', 'addUsers', 'companySettings',
   'instructions', 'feeSchedule'];
  AdminUsersService.currentDetailsTab = vm.selectedAdditionalDetailsTab = 'notes';
  vm.changeAdditionalDetailsTab = function (tab) {
    // Keep track of tab
    AdminUsersService.changeUrl({details: tab});
  };
  /**
   * Make sure we're always on a valid tab
   */
  vm.checkDetailsPaneTab = function () {
    // Get type, capitalized
    var type = AdminUsersService.record._type.charAt(0).toUpperCase() + AdminUsersService.record._type.slice(1);
    // Don't display a tab which doesn't exist
    if (angular.isDefined(vm['detailsTabsConfig' + type]) &&
        angular.isUndefined(vm['detailsTabsConfig' + type][$stateParams.details])) {
      AdminUsersService.currentDetailsTab = vm.selectedAdditionalDetailsTab = 'notes';
    }
  };
  /**
   * Load table data on load
   */
  vm.init = function () {
    // Keep reference to which section we're on
    AdminUsersService.section = 'client';
    // Set state on load based on query parameters
    AdminUsersClientService.setStateOnLoad('client', $stateParams, vm)
    .then(function () {
      $scope.compileDirectives();
    });
  };

  /**
   * For showing details on load - The reason for this is that there are two user types being shown here - companies and users
   * Both have their own details pane
   */
  var compile = $scope.$on('compile-directive', function (event, name) {
    // Catch compile and rebroadcast correct compile event
    if (name === 'client-additional-details') {
      var type;
      // Determine if user or company to compile
      if (angular.isObject(AdminUsersClientService.safeData.client[$stateParams.recordId].company)) {
        type = 'user';
      } else {
        type = 'company';
      }
      // Display additional details
      vm.showAdditionalDetails(type);
    }
  });
  /**
   * Remove compilation listener on controller destroy
   */
  $scope.$on('$destroy', function () {
    if (angular.isFunction(compile)) {
      compile();
    }
  });

  /**
   * Submit new company modal
   */
  vm.newCompany = function () {
    // Submit new company
    AdminUsersClientService.newCompany().then(function () {
      // Show success
      $scope.$broadcast('hide-modal', 'new-company');
      $scope.$broadcast('show-modal', 'new-company-successful');
    }, function () {
      // Show failure
      $scope.$broadcast('show-modal', 'new-company-failure');
    });
  };

  /**
   * Show company scorecard modal
   */
  vm.showCompanyScorecard = function () {
    // Instantiate this data modal, destroy the others
    vm.activateDataModal('companyScorecardActive');
    // Retrieve statistics
    AdminUsersClientService.getScorecard()
    .then(function () {
      return DomLoadService.load();
    })
    .then(function () {
      $scope.$broadcast('show-modal', 'company-scorecard');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'company-scorecard-failure');
    });
  };

  /**
   * Switch details pane tab
   * @param tab
   */
  vm.switchDetailsPaneTab = function (tab) {
    // Set active pane tab
    vm.displayData.tab = tab;
    // Change URL to show pane
    AdminUsersService.changeUrl({details: tab});
  };

  /**
   * Confirm deleting a user or company
   */
  vm.confirmDelete = function () {
    $scope.$broadcast('show-modal', 'delete-record');
  };

  /**
   * Delete selected user or company
   */
  vm.deleteRecord = function () {
    AdminUsersClientService.deleteUserOrCompany()
    .then(function () {
      // Hide confirmation modal
      $scope.$broadcast('hide-modal', 'delete-record');
      // Show success modal
      $scope.$broadcast('show-modal', 'delete-success');
    })
    .catch(function () {
      // Show failure modal
      $scope.$broadcast('show-modal', 'delete-failure', true);
    })
    .finally(function () {
      $scope.recompile = true;
      $scope.compileTable();
    });
  };

  /**
   * Confirm send email
   */
  vm.emailSignupLink = function () {
    $scope.$broadcast('show-modal', 'invite-link');
    // Keep reference to the email recipients
    vm.displayData.emailRecipients = {
      1: {recipient: vm.displayData.details.email}
    };
  };

  /**
   * Send invite link
   */
  vm.sendInviteLink = function () {
    // Send invite link
    AdminUsersClientService.sendInviteLink().then(function () {
      // Hide modal
      $scope.$broadcast('hide-modal', 'invite-link');
      // Show success
      $scope.$broadcast('show-modal', 'invite-link-success');
    }, function () {
      // Show failure
      $scope.$broadcast('show-modal', 'invite-link-failure');
    });
  };

  /**
   * Initiate Quickbooks sync
   *
   * @todo Waiting on backend
   * @link https://github.com/ascope/manuals/issues/30
   */
  vm.qbSync = function () {
    // Initiate quickbooks sync
    AdminUsersClientService.qbSync()
    .then(function (response) {
      if (response.syncing) {
        // Show success
        $scope.$broadcast('show-modal', 'qb-sync-success');
      } else {
        // Show nothing to sync
        $scope.$broadcast('show-modal', 'qb-sync-unneeded');
      }
    }, function () {
      // Show failed to sync
      $scope.$broadcast('show-modal', 'qb-sync-failure');
    });
  };

  /**
   * Show send login info confirmation modal
   */
  vm.showLoginCredentialsConfirmation = function () {
    $scope.$broadcast('show-modal', 'login-info-confirmation');
  };
  /**
   * Send a user a link to retrieve login credentials
   */
  vm.sendLoginInfo = function () {
    // Send login credentials
    AdminUsersClientService.sendLoginInfo()
    .then(function () {
      // Hide confirmation modal
      $scope.$broadcast('hide-modal', 'login-info-confirmation');
      // Show success modal
      $scope.$broadcast('show-modal', 'login-info-success');
    }, function () {
      $scope.$broadcast('show-modal', 'login-info-failure');
      // Show failure modal
    });
  };
  /**
   * Display user settings
   */
  vm.displaySettingsSummary = function () {
    // Instantiate this data modal, destroy the others
    vm.activateDataModal('settingsActive');
    // Retrieve user settings
    AdminUsersClientService.getUserSettingsSummary()
    .then(function () {
      $timeout(function () {
        $scope.$broadcast('show-modal', 'user-settings');
      });
    }, function () {
      $scope.$broadcast('show-modal', 'user-settings-failure');
    });
  };
  /**
   * Display user permissions
   */
  vm.displayPermissions = function () {
    // Instantiate this data modal, destroy the others
    vm.activateDataModal('permissionsActive');
    // Retrieve user permissions
    AdminUsersClientService.getUserPermissions()
    .then(function () {
      $timeout(function () {
        $scope.$broadcast('show-modal', 'user-permissions');
        // Destroy on close
        $scope.$broadcast('modal-close', 'user-permissions', function () {
          vm.permissionsActive = false;
        });
      });
    }, function () {
      $scope.$broadcast('show-modal', 'user-permissions-failure');
    });
  };
  /**
   * Set a user active or inactive
   *
   * @todo We seem to have lost this property on the backend
   * @link https://github.com/ascope/manuals/issues/369
   */
  vm.changeActiveStatus = function () {
    var detached = false;
    // Detach watcher so we don't make an extra request
    if (angular.isFunction(AdminUsersService.detachUpdateWatcher)) {
      AdminUsersService.detachUpdateWatcher();
      detached = true;
    }
    // Change active status
    AdminUsersClientService.changeActiveStatus()
    .catch(function () {
      // Reverse on failure
      AdminUsersClientService.displayData.details.isActive = !AdminUsersClientService.displayData.details.isActive;
    })
    .finally(function () {
      // Reattach watcher, if detached
      if (detached) {
        AdminUsersService.attachUpdateWatcher.call(vm, $scope, AdminUsersClientService);
      }
    });
  };

  /**
   * Change tab on details modal
   */
  vm.changeTab = function (tab) {
    // Keep reference to current tab
    AdminUsersService.currentDetailsTab = tab;
    // Change URL
    AdminUsersService.changeUrl({details: tab});
  };

  /**
   * Display user from company users modal
   */
  vm.displayUser = function (id) {
    // Retrieve details
    AdminUsersClientService.getRecordDetails(id);
    // Hide modal
    $scope.$broadcast('hide-modal', 'company-users');
  };

  /*************************
   * @todo When there is a backend
   * 8/3
   */
  ///**
  // * Keep filterAll in sync with the table
  // */
  //$scope.$watch(function () {
  //  return vm.filterAll;
  //}, function (newVal) {
  //  if (angular.isUndefined(newVal)) {
  //    return;
  //  }
  //  AdminUsersClientService.filterAll = newVal;
  //});

  // Initiate controller
  vm.init();
}]);


