'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users client data service
 */
app.factory('AdminUsersClientService',
['$q', 'AdminUsersUserSettingsSummaryService', 'AdminUsersUserPermissionsService', 'AdminUsersService',
'AdminUsersResourceService', 'AsDataService',
function ($q, AdminUsersUserSettingsSummaryService, AdminUsersUserPermissionsService, AdminUsersService, AdminUsersResourceService, AsDataService) {
  var Service = {
    // Data for display on search table
    tableData: [],
    // Complete data set
    safeData: {
      // Complete data set
      client: {},
      // Only users
      users: {},
      // Currently selected user
      user: {},
      // Branches
      branches: {},
      // Only company
      companies: {},
      // Company users
      companyUsers: {},
      // Company scorecard
      scorecard: {},
      // Table definition
      heading: [
        // Company name
        {
          label: 'Company Name',
          data: 'companyName',
          search: true
        },
        {
          label: 'Account Type',
          data: 'companyOrEmployee',
          search: true
        },
        // User type
        {
          label: 'User Type',
          data: 'displayType',
          search: true
        },
        {
          label: 'First Name',
          data: 'firstName',
          search: true
        },
        {
          label: 'Last Name',
          data: 'lastName',
          search: true
        },
        {
          label: 'Email Address',
          data: 'email',
          search: true
        },
        {
          label: 'State',
          data: 'state',
          search: true
        },
        {
          label: 'Branch',
          data: 'branchName',
          search: true
        },
        // Function to display users for a company
        {
          label: '',
          linkLabel: 'Show users',
          fn: 'displayUsers',
          noSort: true,
          optionalFn: true,
          data: 'data.companyOrEmployee === "Employee"'
        }
      ],
      // For switching transformers when switching record types
      transformers: {
        // User transformers
        user: {
          companyName: {
            source: 'company.name'
          },
          companyOrEmployee: {
            staticVal: 'Employee'
          },
          state: {
            source: 'location.state.name'
          },
          branchName: {
            source: 'branch.name'
          },
          phone: {
            source: 'phones.primary.number'
          },
          fax: {
            source: 'phones.fax'
          },
          joinedAt: {
            filter: 'date'
          }
        },
        // Company transformers
        company: {
          companyName: {
            source: 'name'
          },
          companyOrEmployee: {
            staticVal: 'Company'
          },
          email: {
            source: 'contact.email'
          },
          type: {
            staticVal: ''
          },
          firstName: {
            source: 'contact.firstName'
          },
          lastName: {
            source: 'contact.lastName'
          },
          phone: {
            source: 'contact.phones.primary.number'
          },
          fax: {
            source: 'contact.phones.fax'
          },
          location: {
            source: 'contact.location'
          },
          state: {
            source: 'contact.location.state.name'
          },
          branchName: {
            staticVal: ''
          },
          joinedAt: {
            filter: 'date'
          }
        },
        // Company scorecard
        scorecard: {
          date: {
            filter: 'date:MMM yyyy'
          },
          totalCompletedAppraisals: {
            source: 'totals.completedAppraisals'
          },
          onTime: {
            filter: 'number:2'
          },
          acceptanceToCompletionDays: {
            source: 'periods.acceptanceToCompletion',
            filter: 'number:2'
          },
          assignedToCompletedDays: {
            source: 'periods.inspectionToCompletion',
            filter: 'number:2'
          },
          inspectionToCompletionDays: {
            source: 'periods.assignedToAccepted',
            filter: 'number:2'
          },
          totalReviewerRevisions: {
            source: 'totals.reviewerRevisions'
          },
          totalClientRevisions: {
            source: 'totals.clientRevisions'
          },
          totalReconsiderationRequests: {
            source: 'totals.reconsiderationRequests'
          },
          reviewerRevisionRate: {
            source: 'rates.reviewerRevision',
            filter: 'number:2'
          },
          clientRevisionRate: {
            source: 'rates.clientRevision',
            filter: 'number:2'
          }
        }
      },
      // Keep reference to the current transformer to avoid unnecessary switching
      currentTransformer: ''
    },
    // Display data
    displayData: {
      // Table
      client: [],
      // Create new company
      newCompany: {},
      // For displaying details of a user from within company view
      user: {},
      // Users in a company
      companyUsers: [],
      // Company scorecard
      scorecard: [],
      // Account type options for dropdown
      accountTypeOptions: {
        user: [
          {
          id: 'appraiser',
          value: 'Appraiser'
          },
          {
          id: 'client',
          value: 'Client'
          },
          {
          id: 'admin',
          value: 'Admin'
          },
          {
          id: 'staff',
          value: 'Staff'
          },
          {
          id: 'amc',
          value: 'AMC'
          }
        ],
        company: [
          {
            id: 'attorney',
            value: 'Attorney'
          },
          {
            id: 'bank',
            value: 'Bank'
          },
          {
            id: 'correspondent-lender',
            value: 'Correspondent lender'
          },
          {
            id: 'credit-union',
            value: 'Credit union'
          },
          {
            id: 'home-owner',
            value: 'Home owner'
          },
          {
            id: 'mortgage-broker',
            value: 'Mortgage broker'
          },
          {
            id: 'lender',
            value: 'Lender'
          },
          {
            id: 'real-estate-agent',
            value: 'Real estate agent'
          },
          {
            id: 'wholesale-lender',
            value: 'Wholesale lender'
          },
          {
            id: 'other',
            value: 'Other'
          }
        ]
      },
      //'processor', 'manager', 'loan-officer'
      userTypeOptions: [
        {
          id: 'processor',
          value: 'Processor'
        },
        {
          id: 'manager',
          value: 'Manager'
        },
        {
          id: 'loan-officer',
          value: 'Loan officer'
        }
      ]
    },
    // Transformers (dynamically generated on run)
    transformers: {},
    // Safe data of selected branch
    selectedBranch: {},
    // Details of the currently selected user or company
    details: {
      tab: {}
    },
    /**
     * General client view requests
     * @param requestType Request wrapper method to call
     * @param params Any additional params that may need to be passed in to the resource
     */
    request: function (requestType, params) {
      // Get $resource configuration
      return AdminUsersResourceService.client[requestType](params);
    },
    /**
     * Switch transformers to the correct type before loading record details, if necessary
     */
    beforeFormatEach: function (record) {
      var type = Service.determineCompanyOrUser(record) ? 'company' : 'user';
      // Switch to the needed transformer
      if (Service.safeData.currentTransformer !== type) {
        Service.transformers = angular.copy(Service.safeData.transformers[type]);
        // Store so we don't switch unnecessarily
        Service.safeData.currentTransformer = type;
      }
    },
    /**
     * Determine if the current record is a user or company
     * @param record Object
     * @returns {boolean}
     */
    determineCompanyOrUser: function (record) {
      // Determine if company or user
      return !angular.isObject(record.company);
    },
    /**
     * Retrieve users for a specific company
     */
    getCompanyUsers: function (id) {
      Service.displayData.companyUsers.length = 0;
      // Set company users here to share with clientCtrl
      Lazy(Service.safeData.client)
      .each(function (user) {
        // Find users and push them into the array
        if (angular.isDefined(user.company) && user.company.id === id) {
          Service.safeData.companyUsers[user.id] = user;
        }
      });
      // Format data
      AsDataService.formatData.call(Service, 'companyUsers');
    },
    /**
     * Retrieve record content callback
     */
    getClientRecord: function () {
      // Copy record
      var record = angular.copy(Service.safeData.client[AdminUsersService.id]);
      // Any any general transformations
      Service.transformData(record);
      // Determine if user or company
      Service.displayData.details = record;
      // @todo I need to keep reference to the record at the top level service, but this means that keeping it on
      // other services is likely redundant. Refactor.
      AdminUsersService.record = record;
      // Company
      if (!Service.safeData.client[AdminUsersService.id].company) {
        Service.displayData.type = 'company';
        // See above note
        AdminUsersService.record._type = 'company';
        // User
      } else {
        Service.displayData.type = 'user';
        // See above note
        AdminUsersService.record._type = 'user';
        // Get following
        // @todo Awaiting Igor's response
        //Service.getFollowing();
      }
    },
    /**
     * Get following information for the current user
     */
    getFollowing: function () {
      Service.request('following').then(function (response) {
        console.log(response);
      });
    },
    /**
     * Revert to details after branch settings is closed
     */
    revertDetails: function () {
      // Restore record from before another was being viewed
      AdminUsersService.record = angular.copy(AdminUsersService._record);
      // Remove old record and branch settings ID
      AdminUsersService._record = null;
      AdminUsersService.branchSettingsId = null;
    },
    /**
     * Get tab class for each of the details pane tabs
     */
    getDetailsTabClass: function (tab, activeTab) {
      if (tab.tab === activeTab) {
        return 'col-xs-6 active';
      }
      return 'col-xs-6';
    },
    /**
     * Get statistics for a company
     */
    getScorecard: function () {
      return Service.request('companyScorecard').then(function (response) {
        response.data = Lazy(response.data)
        .sort(function (recordOne, recordTwo) {
          return recordOne.date > recordTwo.date;
        })
        .toArray();
        // Set specific transformers for company scorecard
        Service._transformers = angular.copy(Service.transformers);
        Service.transformers = angular.copy(Service.safeData.transformers.scorecard);
        // Hash and format
        AsDataService.hashData.call(Service, response.data, 'scorecard', 'date');
        AsDataService.formatData.call(Service, 'scorecard');
        // Restore transformers
        Service.transformers = angular.copy(Service._transformers);
        Service._transformers = null;
      });
    },
    /**
     * Delete selected user or company
     */
    deleteUserOrCompany: function () {
      // Determine method name based on whether we're examining a user or company
      var fnName = AdminUsersService.record._type === 'user' ? 'deleteUser' : 'deleteCompany';
      // Make request
      return Service.request(fnName)
      .then(function () {
        // Remove the deleted company or user
        delete Service.safeData.client[AdminUsersService.id];
        // Recreate display data
        Service.formatData();
        // Reset URL
        AdminUsersService.changeUrl({client: ''});
        // Hold reference to deleted type
        Service.displayData.deletedType = Service.displayData.type;
        // Clear details
        Service.displayData.details = null;
        // Clear record
        AdminUsersService.record = null;
      });
    },
    /**
     * Send invite link to selected recipients
     */
    sendInviteLink: function () {
      var recipients = [];
      // Create array of recipients
      angular.forEach(Service.displayData.emailRecipients, function (recipient) {
        recipients.push(recipient.recipient);
      });
      // Send email
      return Service.request('emailSignUp', recipients);
    },
    /**
     * Sync Quickbooks
     *
     * @todo I've requested this API endpoint
     */
    qbSync: function () {
      return Service.request('quickbooksSync');
    },
    /**
     * Send login information
     */
    sendLoginInfo: function () {
      return Service.request('sendLoginDetails', {employees: [AdminUsersService.id]});
    },
    /**
     * Change user active status
     */
    changeActiveStatus: function () {
      // Update display
      Service.displayData.details.isActive = !Service.displayData.details.isActive;
      return Service.request('changeActiveStatus', {isActive: Service.displayData.details.isActive});
    },
    /**
     * Retrieve data for user settings
     *
     * @param user Object User record, when not using default
     */
    getUserSettingsSummary: AdminUsersUserSettingsSummaryService.getUserSettingsSummary,
    /**
     * Retrieve user permissions
     */
    getUserPermissions: function () {
      return $q.all([
        // Get permissions
        AdminUsersUserPermissionsService.getUserPermission('permissions'),
        // Get groups
        AdminUsersUserPermissionsService.getUserPermission('groups'),
        // Get users
        AdminUsersUserPermissionsService.getUserPermission('users'),
        // Get broker user permissions
        AdminUsersUserPermissionsService.getUserPermission('broker-user', 'brokerUser'),
        // Get broker broker permissions
        AdminUsersUserPermissionsService.getUserPermission('broker', 'broker'),
        // Get auto add new group status
        AdminUsersUserPermissionsService.getAutoAddNewGroupStatus(),
        // Get auto add new group status
        AdminUsersUserPermissionsService.getAutoAddNewBrokerStatus(),
        // Get internal users and permissions
        AdminUsersUserPermissionsService.getUserMailPermissions(),
        // Get broker users and permissions
        AdminUsersUserPermissionsService.getBrokerMailPermissions()
      ]);
    },
    /**
     * When a data-heavy modal is shown, destroy the others
     */
    activateDataModal: function (active) {
      var dataModals = ['companyScorecardActive', 'settingsActive', 'permissionsActive'];
      var that = this;
      dataModals.forEach(function (modal) {
        that[modal] = modal === active;
      });
    },
    /**
     * Get details for a specific user
     */
    getUserDetails: function (userId) {
      // Expose on display data
      Service.displayData.user = angular.copy(Service.safeData.users[userId]);
    }
  };

  // Load and process records on load
  Service.loadRecords = AdminUsersService.loadRecords.bind(Service, 'client');
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'client');
  Service.transformData = AsDataService.transformData.bind(Service);
  // Retrieve single record
  Service.getRecordDetails = AdminUsersService.getRecordDetails.bind(Service, 'client', Service.getClientRecord);
  // Update record
  Service.updateRecord = AdminUsersService.updateRecord.bind(Service, 'client');
  // Set state on load
  Service.setStateOnLoad = AdminUsersService.setStateOnLoad.bind(Service);
  // Create new company
  Service.newCompany = AdminUsersService.newCompany.bind(Service, 'client');

  return Service;
}]);
