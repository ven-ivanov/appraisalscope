'use strict';

describe('AdminUsersClientTableCtrl', function () {
  var scope, controller, adminUsersClientService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersClientService', AdminUsersClientServiceMock);
    });
  });
  beforeEach(inject(function ($rootScope, $controller, AdminUsersClientService) {
    scope = $rootScope.$new();
    adminUsersClientService = AdminUsersClientService;
    spyOn(adminUsersClientService, 'filterByProp');
    controller = $controller('AdminUsersClientTableCtrl', {$scope: scope});
  }));

  describe('filterAll', function () {
    xit('should call filterByProp()', function () {
      //expect(controller.filterAll).toBeUndefined();
      //adminUsersClientService.filterAll = 'test';
      //scope.$digest();
      //expect(adminUsersClientService.filterByProp).toHaveBeenCalled();
    });
  });

  describe('filterByProp', function () {
    xit('should call filterByProp on service', function () {
      //controller.filterByProp = {email: 'email'};
      //scope.$digest();
      //expect(adminUsersClientService.filterByProp).toHaveBeenCalled();
    });
  });
});