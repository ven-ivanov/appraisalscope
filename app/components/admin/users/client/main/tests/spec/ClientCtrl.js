'use strict';

describe('ClientCtrl', function () {
  var controller, scope, httpBackend, adminUsersConfigService, adminUsersCtrlInheritanceService,
  adminUsersClientService, timeout, adminUsersDataService;

  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersClientService', AdminUsersClientServiceMock);
      $provide.factory('DomLoadService', DomLoadServiceMock);
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function (
  $rootScope, $controller, $httpBackend, AdminUsersCtrlInheritanceService, AdminUsersConfigService,
  AdminUsersClientService, $timeout, AdminUsersService) {
    scope = $rootScope.$new();
    // Dependencies
    adminUsersCtrlInheritanceService = AdminUsersCtrlInheritanceService;
    adminUsersConfigService = AdminUsersConfigService;
    adminUsersClientService = AdminUsersClientService;
    timeout = $timeout;
    adminUsersDataService = AdminUsersService;
    // Spy on inheritance
    spyOn(adminUsersCtrlInheritanceService, 'createCtrl').and.callThrough();
    spyOn(AdminUsersConfigService, 'getClientDetailsPaneTabs').and.callThrough();
    // Spy on broadcast
    spyOn(scope, '$broadcast');
    // Instantiate controller
    controller = $controller('AdminUsersClientCtrl', {$scope: scope});
    httpBackend = $httpBackend;
    // Mock states
    httpBackend.whenGET(/handbook\/states/).respond({});
    httpBackend.whenGET(/users\/client\/test/).respond({});
    httpBackend.whenGET(/users\/client/).respond({});
    // Set safe data
    adminUsersClientService.safeData = {
      users: {
        1: {
          isActive: true
        }
      },
      companies: {
        1: {

        }
      }
    };
    // Set details
    controller.displayData = {
      type: 'user',
      details: {
        email: 'me@me.com'
      }
    };
    // Mock compile directives
    scope.compileDirectives = angular.noop;
  }));

  describe('properties', function () {
    it('should immediately retrieve the necessary properties', function () {
      expect(adminUsersConfigService.getClientDetailsPaneTabs).toHaveBeenCalled();
    });
  });

  describe('tabClass()', function () {
    it('should set the active tab class', function () {

    });
  });

  describe('newCompanyModal()', function () {
    it('should broadcast to show the modal when the function is called', function () {
      controller.newCompanyModal();
      scope.$digest();
      expect(scope.$broadcast).toHaveBeenCalled();
    });
  });

  describe('newCompany()', function () {
    it('should display success modal and hide company modal', function () {
      controller.newCompany();
      scope.$digest();
      expect(scope.$broadcast.calls.count()).toBe(2);
      expect(scope.$broadcast).toHaveBeenCalledWith('hide-modal', 'new-company');
      expect(scope.$broadcast).toHaveBeenCalledWith('show-modal', 'new-company-successful');
    });
  });

  describe('showCompanyScorecard()', function () {
    it('should display the company statistics modal', function () {
      spyOn(controller, 'activateDataModal').and.callThrough();
      controller.showCompanyScorecard();
      timeout.flush();
      scope.$digest();
      expect(scope.$broadcast).toHaveBeenCalledWith('show-modal', 'company-scorecard');
    });
  });

  describe('switchDetailsPaneTab()', function () {
    it('should switch the details pane', function () {
      spyOn(adminUsersDataService, 'changeUrl');
      controller.switchDetailsPaneTab('test');
      expect(controller.displayData.tab).toEqual('test');
      expect(adminUsersDataService.changeUrl).toHaveBeenCalledWith({details: 'test'});
    });
  });

  describe('confirmDelete()', function () {
    it('should display the confirm delete modal', function () {
      controller.confirmDelete();
      expect(scope.$broadcast).toHaveBeenCalledWith('show-modal', 'delete-record');
    });
  });

  describe('deleteRecord()', function () {
    beforeEach(function () {
      spyOn(adminUsersClientService, 'deleteUserOrCompany').and.callThrough();
      spyOn(adminUsersClientService, 'resetSearchTableData');
      controller.deleteRecord();
      scope.$digest();
    });
    it('should call the service to delete the record', function () {
      expect(adminUsersClientService.deleteUserOrCompany).toHaveBeenCalled();
    });

    it('should broadcast to show the correct modals', function () {
      expect(scope.$broadcast.calls.count()).toBe(2);
      expect(scope.$broadcast).toHaveBeenCalledWith('hide-modal', 'delete-record');
      expect(scope.$broadcast).toHaveBeenCalledWith('show-modal', 'delete-success');
    });
  });

  describe('emailSignupLink()', function () {
    it('should display the invite link modal with the right information', function () {
      controller.emailSignupLink();
      scope.$digest();
      expect(scope.$broadcast).toHaveBeenCalled();
      expect(controller.displayData.emailRecipients).toEqual({ 1: { recipient: 'me@me.com' } });
    });
  });

  describe('sendInviteLink()', function () {
    beforeEach(function () {
      spyOn(adminUsersClientService, 'sendInviteLink').and.callThrough();
      controller.emailSignupLink();
      controller.sendInviteLink();
      scope.$digest();
    });
    it('should call sendInviteLink on the service', function () {
      expect(adminUsersClientService.sendInviteLink).toHaveBeenCalledWith();
    });
    it('should display the right modals', function () {
      // One for emailSignupLink, two for sendInviteLink
      expect(scope.$broadcast.calls.count()).toBe(3);
      expect(scope.$broadcast).toHaveBeenCalledWith('show-modal', 'invite-link-success');
      expect(scope.$broadcast).toHaveBeenCalledWith('hide-modal', 'invite-link');
    });
  });

  describe('qbSync()', function () {
    beforeEach(function () {
      controller.qbSync();
      scope.$digest();
    });
    it('should call qbSync and display the right modals', function () {
      expect(scope.$broadcast).toHaveBeenCalledWith('show-modal', 'qb-sync-success');
    });
  });

  describe('showLoginCredentialsConfirmation()', function () {
    it('should display the confirmation modal', function () {
      controller.showLoginCredentialsConfirmation();
      expect(scope.$broadcast).toHaveBeenCalledWith('show-modal', 'login-info-confirmation');
    });
  });

  describe('sendLoginInfo()', function () {
    beforeEach(function () {
      controller.sendLoginInfo();
      scope.$digest();
    });

    it('should display the right modals', function () {
      expect(scope.$broadcast).toHaveBeenCalledWith('hide-modal', 'login-info-confirmation');
      expect(scope.$broadcast).toHaveBeenCalledWith('show-modal', 'login-info-success');
    });
  });

  describe('displaySettings()', function () {
    it('should display the settings modal', function () {
      //controller.details = {
      //  data: {
      //    id: 2,
      //    company: 1
      //  }
      //};
      controller.displayData.details = {
        id: 2,
        company: 1
      };
      spyOn(adminUsersClientService, 'getUserSettingsSummary').and.callThrough();
      controller.displaySettingsSummary();
      timeout.flush();
      scope.$digest();
      expect(scope.$broadcast).toHaveBeenCalledWith('show-modal', 'user-settings');
      expect(adminUsersClientService.getUserSettingsSummary).toHaveBeenCalled();
    });
  });

  describe('displayPermissions()', function () {
    it('should display the modal', function () {
      spyOn(adminUsersClientService, 'getUserPermissions').and.callThrough();
      controller.displayPermissions();
      timeout.flush();
      scope.$digest();
      expect(scope.$broadcast).toHaveBeenCalledWith('show-modal', 'user-permissions');
      expect(adminUsersClientService.getUserPermissions).toHaveBeenCalled();
    });
  });

  xdescribe('filterAll', function () {
    it('should keep filterAll accessible via the service', function () {
      controller.filterAll = 'test';
      scope.$digest();
      expect(adminUsersClientService.filterAll).toEqual(controller.filterAll);
    });
  });
});