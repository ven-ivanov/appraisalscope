'use strict';

/**
 * Test the data service for the client view
 */
describe('ClientDataService', function () {
  var scope, httpBackend, adminUsersClientService, searchData, callerObj, adminUsersService;

  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('DomLoadService', DomLoadServiceMock);
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function ($rootScope, $httpBackend, AdminUsersClientService, AdminUsersService) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    adminUsersClientService = AdminUsersClientService;
    adminUsersService = AdminUsersService;
    // Search results for companies and users
    searchData = [{
                       id: 1,
                       companyName: "Walker Bank",
                       companyType: "attorney",
                       company: 0,
                       firstName: "Jessica",
                       lastName: "Johnson",
                       levelId: 0,
                       accountType: "Mortgage Broker",
                       branches: [{value: "branch1"}, {"value": "branch2"}],
                       created: '2014-02-18T08:40:26.241Z'
                     }, {
                       id: 2,
                       companyName: "Beach Co",
                       companyType: "attorney",
                       company: 0,
                       firstName: "Charlie",
                       lastName: "Beach",
                       levelId: 0,
                       accountType: "Mortgage Broker"
                     }, {
                       id: 20,
                       companyName: "Harris LLC",
                       companyType: "home owner",
                       company: 1,
                       created: '2014-02-18T08:40:26.241Z'
                     }, {
                       id: 21,
                       companyName: "Walker Bank",
                       companyType: "attorney",
                       company: 2
                     }, {
                       id: 22,
                       company: 1
                     }];
    // Hash
    adminUsersClientService.hashData(searchData, 'client');
    adminUsersClientService.safeData.client = {1: {id: 1, sup: 'sup', company: {id: 1}}};
    // Set display data
    adminUsersClientService.displayData.client = JSON.parse(JSON.stringify(searchData));
    adminUsersClientService.rowData = adminUsersClientService.displayData.client.slice();
    // For requesting user that is not already retrieved
    httpBackend.whenGET(/client\/user\/23/).respond([{id: 23}]);
    // For requesting a company that is not retrieved already
    httpBackend.whenGET(/\/client\/company\/3/).respond([{id: 3}]);
    // For retrieving a list of clients
    httpBackend.whenGET(/users\/client/).respond([{id: 1, company: 0}, {id: 2, company: 1}]);
    // Object for setting context
    callerObj = {
      selects: {},
      details: {},
      displayData: {}
    };
    callerObj.showAdditionalDetails = jasmine.createSpy('showAdditionalDetails');
    spyOn(adminUsersService, 'changeUrl');
  }));

  describe('getCompanyUsers()', function () {
    beforeEach(function () {
      spyOn(adminUsersClientService, 'formatData');
    });
    it('should retrieve only the users for the requested company', function () {
      // Get the users for this company
      adminUsersClientService.getCompanyUsers(1);
      // Make sure they're stored on the service
      expect(adminUsersClientService.safeData.companyUsers).toEqual({ 1: { id: 1, sup: 'sup', company: { id: 1 } } });
    });
  });

  describe('getUserPermissions()', function () {
    var adminUsersUserPermissionsService;
    beforeEach(inject(function (AdminUsersUserPermissionsService) {
      adminUsersUserPermissionsService = AdminUsersUserPermissionsService;
    }));
    it('should call all promises necessary for user settings before completing', function () {
      spyOn(adminUsersUserPermissionsService, 'getUserPermission');
      spyOn(adminUsersUserPermissionsService, 'getAutoAddNewGroupStatus');
      spyOn(adminUsersUserPermissionsService, 'getAutoAddNewBrokerStatus');
      spyOn(adminUsersUserPermissionsService, 'getUserMailPermissions');
      spyOn(adminUsersUserPermissionsService, 'getBrokerMailPermissions');
      adminUsersClientService.getUserPermissions();
      // Make sure all methods were called
      expect(adminUsersUserPermissionsService.getUserPermission).toHaveBeenCalled();
      expect(adminUsersUserPermissionsService.getAutoAddNewGroupStatus).toHaveBeenCalled();
      expect(adminUsersUserPermissionsService.getAutoAddNewBrokerStatus).toHaveBeenCalled();
      expect(adminUsersUserPermissionsService.getUserMailPermissions).toHaveBeenCalled();
      expect(adminUsersUserPermissionsService.getBrokerMailPermissions).toHaveBeenCalled();
      // It should have called getUserPermission 5 times
      expect(adminUsersUserPermissionsService.getUserPermission.calls.count()).toEqual(5);
    });
  });

  describe('activateDataModal()', function () {
    it('should set only the requested modal to active, all other inactive', function () {
      var callerObj = {};
      adminUsersClientService.activateDataModal.call(callerObj, 'settingsActive');
      expect(callerObj).toEqual({ companyScorecardActive: false, settingsActive: true, permissionsActive: false });
      adminUsersClientService.activateDataModal.call(callerObj, 'permissionsActive');
      expect(callerObj).toEqual({ companyScorecardActive: false, settingsActive: false, permissionsActive: true });
    });
  });

  xdescribe('filterByProp()', function () {
    it('should not run on load', function () {
      callerObj = {};
      expect(adminUsersClientService.filterByProp.call(callerObj)).toEqual();
    });

    it('should only run if there is some valid property to search on', function () {
      expect(adminUsersClientService.filterByProp.call(callerObj, {prop: ''})).toEqual();
      expect(callerObj).toEqual({ selects: {  }, details: {  }, filterAll: '' });
    });
  });

  describe('getClientRecord', function () {
    beforeEach(function () {
      spyOn(adminUsersClientService, 'determineCompanyOrUser');
      adminUsersClientService.getClientRecord();
    });

    it('should set details to the current record', function () {
      expect(adminUsersClientService.displayData.details).toEqual({ id: 1, sup: 'sup', company: { id: 1 }, companyName: '', companyOrEmployee: 'Employee', state: '', branchName: '', phone: '', fax: '', _type: 'user' });
    });
    it('should store the record on AdminUsersService', function () {
      expect(adminUsersService.record).toEqual({ id: 1, sup: 'sup', company: { id: 1 }, companyName: '', companyOrEmployee: 'Employee', state: '', branchName: '', phone: '', fax: '', _type: 'user' });
    });
    it('should set to user if a company is listed in record', function () {
      expect(adminUsersClientService.displayData.type).toEqual('user');
    });
    it('should store type on admin user service', function () {
      expect(adminUsersService.record._type).toEqual('user');
    });
  });
});