'use strict';

var AdminUsersClientServiceMock = function ($q) {
  return {
    displayData: {
      type: 'user',
      details: {
        id: 1
      },
      deletedType: '',
      companyUsers: []
    },
    safeData: {},
    companyUsers: [],
    // For setting state when loading the application with query parameters
    setStateOnLoad: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    resetSearchTableData: function () {

    },
    getClients: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    setSearchTableByCompany: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    selectCompany: function () {

    },
    loadTableData: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    newCompany: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    displayUser: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    changeUrl: function () {

    },
    deleteUserOrCompany: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    sendInviteLink: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    qbSync: function () {
      return $q(function (resolve) {
        resolve({
          syncing: true
        });
      });
    },
    sendLoginInfo: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    getBranch: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    revertDetails: function () {

    },
    massUpdateUsers: function () {

    },
    changeActiveStatus: function () {
      return $q(function (resolve) {
        resolve({
          data: {
            id: 1,
            isActive: false
          }
        });
      });
    },
    getUserSettingsSummary: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    getUserPermissions: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    getScorecard: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    activateDataModal: function () {

    },
    filterByProp: function () {

    },
    loadRecords: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    getUserDetails: function () {

    }
  };
};