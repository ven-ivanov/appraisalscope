'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Inspector view controller
 */
app.controller('AdminUsersInspectorCtrl',
['$scope', 'AdminUsersInspectorService', '$stateParams', 'AdminUsersService', 'AdminUsersCtrlInheritanceService',
function (
$scope, AdminUsersInspectorService, $stateParams, AdminUsersService, AdminUsersCtrlInheritanceService) {
  var vm = this;
  // Inherit common functions
  AdminUsersCtrlInheritanceService.createCtrl.call(vm, AdminUsersInspectorService, $scope, 'inspector');
  // Inspector details
  vm.displayData = AdminUsersInspectorService.displayData;
  // Safe data
  vm.safeData = AdminUsersInspectorService.safeData;
  /**
  * Additional details tabs
  */
  // Tab config
  vm.additionalDetailsTabsConfig = {
    notes: 'Notes',
    coverage: 'Coverage',
    appraisalForms: 'Appraisal forms',
    feeSchedule: 'Fee schedule',
    pendingOrders: 'Pending orders',
    history: 'History',
    additionalDocs: 'Additional docs'
  };
  // Tab config array (for ordering)
  vm.additionalDetailsTabs =
  ['notes', 'coverage', 'appraisalForms', 'feeSchedule', 'pendingOrders', 'history', 'additionalDocs'];
  // Selected tab
  AdminUsersService.currentDetailsTab = vm.selectedAdditionalDetailsTab = 'notes';
  vm.changeAdditionalDetailsTab = function (tab) {
    // Keep track of tab
    AdminUsersService.changeUrl({details: tab});
  };

  /**
   * Initiate data and state
   */
  vm.init = function () {
    // Set state on load
    AdminUsersInspectorService.setStateOnLoad($stateParams, vm);
    // Keep reference to which section we're on
    AdminUsersService.section = 'inspector';
  };

  // Init controller load
  vm.init();
}]);