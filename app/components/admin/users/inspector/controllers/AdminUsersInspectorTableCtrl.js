'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersInspectorTableCtrl', ['$scope', 'AdminUsersInspectorService', 'AdminUsersTableCtrlInheritanceService', function ($scope, AdminUsersInspectorService, AdminUsersTableCtrlInheritanceService) {

  // Inherit table controller methods
  AdminUsersTableCtrlInheritanceService.inherit.call(this, $scope, AdminUsersInspectorService, 'inspector');
}]);