'use strict';

var app = angular.module('frontendApp');

/**
 * Admin User - Inspector service
 */
app.factory('AdminUsersInspectorService',
['$http', 'API_PREFIX', 'AdminUsersService', 'AsDataService', function ($http, API_PREFIX, AdminUsersService, AsDataService) {
  var AdminUsersInspectorService = {
    // Inspector ID
    inspectorId: 0,
    // Safe data
    safeData: {
      // Inspectors table
      inspector: {},
      // Table heading
      heading: [
        // Name
        {
          label: 'Name',
          data: 'name',
          search: true
        },
        // Certification
        {
          label: 'Certification',
          data: 'certification',
          search: true
        },
        // FHA
        {
          label: 'FHA',
          data: 'fha',
          filter: 'boolYesNo',
          isStatic: true,
          search: true
        }
      ],
      // Inspector ratings
      ratings: {},
      // Certification details
      certificationDetails: {},
      // Reports
      inspectorReports: {}
    },
    // Display data
    displayData: {
      // Table data
      inspector: [],
      // Details
      details: {},
      // States dropdown
      states: [],
      // inspector quality data
      ratings: {},
      // inspector quality tab values
      ratingTabs: {},
      // Selected tab
      inspectorRatingsSelectedTab: '',
      // Certification details
      certificationDetails: [],
      // Inspector reports
      inspectorReports: [],
      // Yes/no dropdown
      yesNoDropdown: AdminUsersService.yesNoDropdown
    },
    // Data transformations
    transformers: {
      // Certification display
      certification: AdminUsersService.getCertificationTransformer(),
      // Registered data
      registered: {
        filter: 'date'
      }
    },
    /**
     * Callback for formatting data
     */
    formatCallback: function () {
      angular.forEach(AdminUsersInspectorService.displayData.inspector, function (inspector) {
        inspector.name = inspector.firstName + ' ' + inspector.lastName;
      });
    },
    /**
     * Retrieve inspector reports
     */
    getInspectorReports: function () {
      return $http.get(API_PREFIX() + '/v2.0/users/inspector/' + AdminUsersInspectorService.inspectorId + '/reports')
      .then(function (response) {
        // Hash
        AdminUsersInspectorService.hashData(response.data, 'inspectorReports');
        // Set for display
        AdminUsersInspectorService.displayData.inspectorReports = response.data;
      });
    }
  };

  /**
   * Inherit from AdminUsersService
   */
  AdminUsersInspectorService.loadRecords = AdminUsersService.loadRecords.bind(AdminUsersInspectorService, 'inspector');
  AdminUsersInspectorService.hashData = AsDataService.hashData.bind(AdminUsersInspectorService);
  AdminUsersInspectorService.formatData = AsDataService.formatData.bind(AdminUsersInspectorService, 'inspector');
  AdminUsersInspectorService.transformData = AsDataService.transformData.bind(AdminUsersInspectorService);
  // Get record details
  AdminUsersInspectorService.getRecordDetails =
  AdminUsersService.getRecordDetails.bind(AdminUsersInspectorService, 'inspector',
  AdminUsersInspectorService.getInspectorReports);
  // Set state
  AdminUsersInspectorService.setStateOnLoad = AdminUsersService.setStateOnLoad.bind(AdminUsersInspectorService, 'inspector');
  // Toggle settings
  AdminUsersInspectorService.toggleSetting = AdminUsersService.toggleSetting.bind(AdminUsersInspectorService, 'inspector');
  // Send login credentials
  AdminUsersInspectorService.sendLogin = AdminUsersService.sendLogin.bind(AdminUsersInspectorService, 'inspector');
  // Ratings
  AdminUsersInspectorService.getRatings =
  AdminUsersService.getRatings.bind(AdminUsersInspectorService, 'inspector', null);
  // Update record
  AdminUsersInspectorService.updateRecord = AdminUsersService.updateRecord.bind(AdminUsersInspectorService, 'inspector');


  return AdminUsersInspectorService;
}]);