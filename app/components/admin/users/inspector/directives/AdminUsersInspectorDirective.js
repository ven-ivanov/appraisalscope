'use strict';

var app = angular.module('frontendApp');

/**
 * Admin User - Inspector view
 */
app.directive('adminUsersInspector', ['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/inspector/directives/partials/inspector.html',
    controller: 'AdminUsersInspectorCtrl',
    controllerAs: 'inspectorCtrl',
    compile: function () {
      return {
        pre: function (scope) {
          // Expose trigger download
          DirectiveInheritanceService.inheritDirective.call(scope);
        }
      };
    }
  };
}]);