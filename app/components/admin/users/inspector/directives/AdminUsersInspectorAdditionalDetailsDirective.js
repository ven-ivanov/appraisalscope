'use strict';

var app = angular.module('frontendApp');

/**
 * Compile additional details only when it's needed
 */
app.directive('adminUsersInspectorAdditionalDetails', ['DirectiveConditionalLoadService', function (DirectiveConditionalLoadService) {
  return function (scope, elem) {
    var template;
    // Template
    template =
    ['<info-modal modal-id="inspector-additional-details" modal-title="Additional details" button-text="OK" modal-class="notification-window" modal-width="100%" modal-height="max">',
     '<as-tabs tab-config="inspectorCtrl.additionalDetailsTabsConfig" tabs="inspectorCtrl.additionalDetailsTabs" tab-width="7ths" selected-tab="inspectorCtrl.selectedAdditionalDetailsTab" change-tab="inspectorCtrl.changeAdditionalDetailsTab(tab)">',
     '</as-tabs>',
      // Switch show directive
     '<div ng-switch="inspectorCtrl.selectedAdditionalDetailsTab">',
     // Notes
     '<admin-users-notes ng-switch-when="notes"></admin-users-notes>',
     // Coverage
     '<admin-users-coverage ng-switch-when="coverage"></admin-users-coverage>',
     // Appraisal forms
     '<admin-users-jobtypes ng-switch-when="appraisalForms"></admin-users-jobtypes>',
     // Fee schedule
     '<admin-users-fee-schedule ng-switch-when="feeSchedule"></admin-users-fee-schedule>',
     // Pending order
     '<admin-users-pending-orders ng-switch-when="pendingOrders"></admin-users-pending-orders>',
     // History
     '<admin-users-history ng-switch-when="history"></admin-users-history>',
     // Additional docs
     '<admin-users-appraiser-docs ng-switch-when="additionalDocs"></admin-users-appraiser-docs>',
     '</div>',
     '</info-modal>'];

    // Compile the directive only when necessary and then load it
    DirectiveConditionalLoadService.init.call(scope, template, elem, 'inspector-additional-details');
  };
}]);