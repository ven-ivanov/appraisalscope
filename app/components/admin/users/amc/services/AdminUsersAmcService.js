'use strict';

var app = angular.module('frontendApp');

/**
 * Admin User - Amc service
 */
app.factory('AdminUsersAmcService', ['AdminUsersService', 'AsDataService', function (AdminUsersService, AsDataService) {
  var Service = {
    // Safe data
    safeData: {
      // Amcs table
      amc: {},
      // Realtor ratings
      ratings: {},
      // Certification details
      certificationDetails: {},
      // Reports
      amcReports: {},
      // Settings
      settings: {},
      heading: [
        // Name
        {
          label: 'Company Name',
          data: 'name',
          search: true
        },
        // Rating
        {
          label: 'Rating',
          data: 'ratingTotal',
          search: true
        }
      ]
    },
    // Display data
    displayData: {
      // Table data
      amc: [],
      // Details
      details: {},
      // States dropdown
      states: [],
      // amc quality data
      ratings: {},
      // amc quality tab values
      ratingTabs: {},
      // Selected tab
      //amcRatingsSelectedTab: '',
      // Certification details
      certificationDetails: [],
      // Submit payment dropdown
      yesNoDropdown: AdminUsersService.yesNoDropdown
    },
    // Data transformations
    transformers: {
      // Registered data
      registered: {
        source: 'registeredAt',
        filter: 'date'
      },
      companyName: {
        source: 'name'
      },
      phone: {
        source: 'contact.phones.primary.number'
      },
      fax: {
        source: 'contact.phones.fax'
      },
      address1: {
        source: 'contact.location.address1'
      },
      address2: {
        source: 'contact.location.address2'
      },
      city: {
        source: 'contact.location.city'
      },
      state: {
        source: 'contact.location.state.code'
      },
      zip: {
        source: 'contact.location.zip'
      }
    },
    /**
     * Calculate ratings before format
     */
    beforeInitFormat: function (records) {
      var ratingTotal, ratingVal;
      // Create ratings for table
      angular.forEach(records, function (record) {
        ratingTotal = 0;
        if (angular.isDefined(record.ratingSummary) && Object.getPrototypeOf(record.ratingSummary) === Object.prototype) {
          angular.forEach(record.ratingSummary, function (rating) {
            // Get each integer rating value
            ratingVal = parseInt(rating);
            // Create total
            if (!isNaN(ratingVal)) {
              ratingTotal = ratingTotal + rating;
            }
          });
          // Attach to record
          record.ratingTotal = Math.round(ratingTotal / Object.keys(record.ratingSummary).length);
        }
      });
    },
    /**
     * Retrieve settings
     */
    getSettings: function () {
      return Service.request('settings')
      .then(function (response) {
        // Request type
        response._type = 'amc';
        Service.hashData(response, 'settings');
      });
    },
    /**
     * Update AMC settings
     */
    updateSettings: function (newVal) {
      return Service.request('updateSettings', newVal);
    }
  };

  /**
   * Inherit from AdminUsersService
   */
  Service.loadRecords = AdminUsersService.loadRecords.bind(Service, 'amc');
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'amc');
  Service.transformData = AsDataService.transformData.bind(Service);
  // Requests
  Service.request = AdminUsersService.request.bind(Service, 'amc');
  // Get record details
  Service.getRecordDetails = AdminUsersService.getRecordDetails.bind(Service, 'amc');
  // Set state
  Service.setStateOnLoad = AdminUsersService.setStateOnLoad.bind(Service, 'amc');
  // Toggle settings
  Service.toggleSetting = AdminUsersService.toggleSetting.bind(Service, 'amc');
  // Send login credentials
  Service.sendLogin = AdminUsersService.sendLogin.bind(Service, 'amc');
  // Ratings
  Service.getRatings = AdminUsersService.getRatings.bind(Service, 'amc', null);
  // Update record
  Service.updateRecord = AdminUsersService.updateRecord.bind(Service, 'amc');


  return Service;
}]);