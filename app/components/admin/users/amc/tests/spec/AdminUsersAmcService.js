describe('AdminUsersAmcService', function () {
  var Service, adminUsersService, scope, httpBackend;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function (AdminUsersService, $rootScope, $httpBackend, AdminUsersAmcService) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    Service = AdminUsersAmcService;
    adminUsersService = AdminUsersService;

    httpBackend.whenGET().respond({data: [1,2,3]});
    spyOn(Service, 'hashData');
  }));

  describe('beforeInitFormat', function () {
    var records;
    beforeEach(function () {
      records = [{id:1, ratingSummary: {a: 1, b: 3, c: 5}}, {id:2}];
      Service.beforeInitFormat(records);
    });

    it('should set a summary on the first record', function () {
      expect(records[0]).toEqual({ id: 1, ratingSummary: { a: 1, b: 3, c: 5 }, ratingTotal: 3 });
    });
    it('should not alter the second record', function () {
      expect(records[1]).toEqual({ id: 2 });
    });
  });

  describe('getSettings', function () {
    beforeEach(function () {
      Service.getSettings();
      httpBackend.flush();
      scope.$digest();
    });

    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should set the type as amc', function () {
      expect(Service.hashData.calls.argsFor(0)[0]._type).toEqual('amc');
    });
  });
});