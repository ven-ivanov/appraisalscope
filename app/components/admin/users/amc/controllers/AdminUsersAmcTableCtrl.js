'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersAmcTableCtrl',
['$scope', 'AdminUsersAmcService', 'AdminUsersTableCtrlInheritanceService', function ($scope, AdminUsersAmcService, AdminUsersTableCtrlInheritanceService) {

  // Inherit table controller methods
  AdminUsersTableCtrlInheritanceService.inherit.call(this, $scope, AdminUsersAmcService, 'amc');
}]);