'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Amc view controller
 */
app.controller('AdminUsersAmcCtrl', ['$scope', 'AdminUsersAmcService', '$stateParams', 'AdminUsersService', 'AdminUsersCtrlInheritanceService', function (
$scope, AdminUsersAmcService, $stateParams, AdminUsersService, AdminUsersCtrlInheritanceService) {
  var vm = this;
  // Inherit common functions
  AdminUsersCtrlInheritanceService.createCtrl.call(vm, AdminUsersAmcService, $scope, 'amc', {disableUpdates: true});
  // Amc details
  vm.displayData = AdminUsersAmcService.displayData;
  // Safe data
  vm.safeData = AdminUsersAmcService.safeData;
  /**
   * Additional details tabs
   */
    // Tab config
  vm.additionalDetailsTabsConfig = {
    notes: 'Notes',
    coverage: 'Coverage',
    appraisalForms: 'Appraisal forms',
    history: 'History',
    additionalDocs: 'Additional docs',
    settings: 'Settings'
  };
  // Tab config array (for ordering)
  vm.additionalDetailsTabs = [
    'notes',
    'coverage',
    'appraisalForms',
    'history',
    'additionalDocs',
    'settings'
  ];
  // Selected tab
  AdminUsersService.currentDetailsTab = vm.selectedAdditionalDetailsTab = 'notes';
  vm.changeAdditionalDetailsTab = function (tab) {
    // Keep track of tab
    AdminUsersService.changeUrl({details: tab});
    // Write to service
    AdminUsersService.currentDetailsTab = tab;
  };

  /**
   * Initiate data and state
   */
  vm.init = function () {
    // Set state on load
    AdminUsersAmcService.setStateOnLoad($stateParams, vm)
    .then(function () {
      // Compile additional directives
      $scope.compileDirectives();
    });
    // Keep reference to which section we're on
    AdminUsersService.section = 'amc';
  };

  // Init controller load
  vm.init();
}]);