'use strict';

var app = angular.module('frontendApp');

/**
 * AMC settings
 */
app.controller('AdminUsersAmcSettingsCtrl',
['$scope', 'AdminUsersService', 'AdminUsersAmcService', 'DomLoadService', function (
$scope, AdminUsersService, AdminUsersAmcService, DomLoadService) {
  var vm = this;
  vm.safeData = AdminUsersAmcService.safeData;

  /**
   * Data load
   */
  vm.init = function () {
    // Retrieve settings
    AdminUsersAmcService.getSettings()
    .then(function () {
      // Update on change
      vm.attachWatcher();
    })
    .catch(function () {
      // Display settings failure
      $scope.$emit('show-modal', 'amc-settings-failure', true);
    });
  };

  /**
   * Watch for settings and write to backend
   */
  vm.attachWatcher = function () {
    DomLoadService.load()
    .then(function () {
      $scope.$watchCollection(function () {
        return AdminUsersAmcService.safeData.settings;
      }, function (newVal, oldVal) {
        if (angular.isUndefined(newVal) || angular.equals(newVal, oldVal) ||
            Object.getPrototypeOf(newVal) !== Object.prototype || !Object.keys(newVal).length) {
          return;
        }
        // Make request
        AdminUsersAmcService.updateSettings(newVal)
        .catch(function () {
          $scope.$broadcast('show-modal', 'update-amc-failure', true);
        });
      });
    });
  };

  vm.init();
}]);