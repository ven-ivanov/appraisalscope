'use strict';

var app = angular.module('frontendApp');

/**
 * AMC settings
 */
app.directive('adminUsersAmcSettings', [function () {
  return {
    templateUrl: '/components/admin/users/amc/directives/AdminUsersAmcSettings/partials/settings.html',
    controller: 'AdminUsersAmcSettingsCtrl',
    controllerAs: 'settingsCtrl'
  };
}]);