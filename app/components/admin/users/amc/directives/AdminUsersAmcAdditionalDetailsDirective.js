'use strict';

var app = angular.module('frontendApp');

/**
 * Compile additional details only when it's needed
 */
app.directive('adminUsersAmcAdditionalDetails', ['DirectiveConditionalLoadService', function (DirectiveConditionalLoadService) {
  return function (scope, elem) {
    var template;
    // Template
    template =
    ['<info-modal modal-id="amc-additional-details" modal-title="Additional details" button-text="OK" modal-class="notification-window" modal-width="100%" modal-height="max">',
     '<as-tabs tab-config="amcCtrl.additionalDetailsTabsConfig" tabs="amcCtrl.additionalDetailsTabs" tab-width="2" selected-tab="amcCtrl.selectedAdditionalDetailsTab" change-tab="amcCtrl.changeAdditionalDetailsTab(tab)">',
     '</as-tabs>',
      // Switch show directive
     '<div ng-switch="amcCtrl.selectedAdditionalDetailsTab">',
     // Notes
     '<admin-users-notes ng-switch-when="notes"></admin-users-notes>',
     // Coverage
     '<admin-users-coverage ng-switch-when="coverage" hide="fha,commercial,edit,delete" amc="true"></admin-users-coverage>',
     // Appraisal forms
     '<admin-users-jobtypes ng-switch-when="appraisalForms" hide="appraiser-requested-fee,set-fee,selected" show="selected-disabled,fee"></admin-users-jobtypes>',
     // History
     '<admin-users-history ng-switch-when="history" hide="jobType,appFee"></admin-users-history>',
     // Additional docs
     '<admin-users-appraiser-docs ng-switch-when="additionalDocs"></admin-users-appraiser-docs>',
     // Settings
     '<admin-users-amc-settings ng-switch-when="settings"></admin-users-amc-settings>',
     '</div>',
     '</info-modal>'];

    // Compile the directive only when necessary and then load it
    DirectiveConditionalLoadService.init.call(scope, template, elem, 'amc-additional-details');
  };
}]);