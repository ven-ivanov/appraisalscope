'use strict';

var app = angular.module('frontendApp');

/**
 * Admin users - Amc view
 */
app.directive('adminUsersAmc',
['DirectiveInheritanceService', '$compile', function (DirectiveInheritanceService, $compile) {
  return {
    templateUrl: '/components/admin/users/amc/directives/partials/amc.html',
    controller: 'AdminUsersAmcCtrl',
    controllerAs: 'amcCtrl',
    compile: function () {
      return {
        pre: function (scope) {
          // Expose trigger download
          DirectiveInheritanceService.inheritDirective.call(scope);
        },
        post: function (scope) {
          /**
           * Compile directives after state set
           */
          scope.compileDirectives = function () {
            // Ratings
            angular.element('#admin-users-ratings').replaceWith($compile('<admin-users-rating type="amc" ctrl="amcCtrl" hide="customerRating,cuScore"></admin-users-rating>')(scope));
            // Certificates
            angular.element('#admin-users-certificates').replaceWith($compile('<admin-users-certification ctrl="amcCtrl" type="amc" hide-primary="true" hide="primary,fha,commercial"></admin-users-certification>')(scope));
          };
        }
      };
    }
  };
}]);