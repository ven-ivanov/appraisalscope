'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersJobtypes', [function () {
  return {
    templateUrl: '/components/admin/users/directives/appraisal-forms/directives/partials/appraisal-forms.html',
    controller: 'AdminUsersJobtypesCtrl',
    controllerAs: 'tableCtrl',
    scope: {
      hide: '@',
      show: '@'
    }
  };
}]);