'use strict';

var app = angular.module('frontendApp');

/**
 * @todo Awaiting replies from backend
 * @link https://github.com/ascope/manuals/issues/188
 */
app.factory('AdminUsersJobtypesService',
['AdminUsersService', 'AsDataService', '$q', function (AdminUsersService, AsDataService, $q) {
  var Service = {
    // Safe data of job types
    safeData: {
      // Table data
      jobTypes: {},
      // Heading
      heading: [],
      // Possible table columns
      columns: [
        {
          // Id is intended to identify columns to be hidden
          id: 'title',
          label: 'Job title',
          data: 'label',
          isStatic: false
        },
        {
          id: 'selected',
          label: 'Selected',
          checkbox: true,
          isStatic: false
        },
        {
          id: 'set-fee',
          label: 'Set fee',
          input: true,
          isStatic: false,
          placeholder: 'Input a fee to select this appraisal form'
        },
        {
          id: 'appraiser-requested-fee',
          label: 'Appraiser requested fee',
          data: 'requestedFee',
          filter: 'currency',
          isStatic: false
        }
      ],
      // Conditionally shown columns
      additionalColumns: [
        {
          id: 'fee',
          label: 'Fee',
          data: 'amount',
          isStatic: false
        },
        {
          id: 'selected-disabled',
          label: 'Selected',
          checkbox: true,
          isStatic: false,
          disabled: true
        }
      ]
    },
    // display data
    displayData: {
      // Job types collection
      jobTypes: [],
      // Inputs on table
      inputs: {},
      // Checks on table
      multiSelect: {}
    },
    transformers: {
      // Apply label
      label: {
        source: function (data) {
          if (data.addons) {
            data.label = data.title;
          } else {
            data.label = data.jobtype.title;
          }
        }
      },
      //amount: {
      //  filter: 'currency'
      //}
    },
    /**
     * Retrieve job types on load
     */
    getJobTypes: function () {
      return Service.request('getAllJobTypes')
      .then(function (response) {
        // Hash all job types
        Service.hashData(response.data, 'jobTypes');
        // Get appraiser selected job types
        return Service.request('jobSelectedJobTypes');
      })
      .then(function (response) {
        // Merge the two objects
        Service.mergeJobTypes(response.data);
      });
    },
    /**
     * Merge all job type objects and appraiser selected job type objects
     */
    mergeJobTypes: function (appraiserJobTypes) {
      var jobTypes = Service.safeData.jobTypes;
      // Merge all and selected job types
      Lazy(appraiserJobTypes)
      .each(function (jobType) {
        jobTypes[jobType.id] = jobType;
        // Set appraiser requested fee to fee
        jobTypes[jobType.id].requestedFee = jobTypes[jobType.id].amount;
      });
      // Format
      Service.formatData(function () {
        // Init table
        Service.initTable();
      });
    },
    /**
     * Set table inputs to initial value
     */
    initTable: function () {
      var displayData = Service.displayData;
      // Fill in inputs and check checkboxes
      Lazy(displayData.jobTypes)
      .each(function (jobType) {
        if (angular.isObject(jobType.jobtype)) {
          displayData.inputs[jobType.id] = jobType.amount;
          displayData.multiSelect[jobType.id] = true;
        }
      });
    },
    /**
     * Check multiselect boxes which have input values
     */
    updateMultiSelect: function (newVal) {
      angular.forEach(newVal, function (row, key) {
        Service.displayData.multiSelect[key] = !!row;
        // Remove empty values
        if (!row) {
          delete newVal[key];
        }
      });
    },
    /**
     * Update jobType values
     */
    updateForms: function (newVal, oldVal) {
      // Get all updated values
      var updated = {};
      // Collect all requests
      var requests = [];
      angular.forEach(newVal, function (row, key) {
        // if we have an update, make the request
        if (row !== oldVal[key]) {
          // Keep reference to updated value
          updated[key] = row;
          // Add or make updates to a job type
          if (newVal[key]) {
            // Update
            if (oldVal[key]) {
              requests.push(Service.request('updateJobType', {url: {jobTypeId: key}, body: {amount: parseInt(newVal[key])}}));
              // Add
            } else {
              requests.push(Service.request('addJobType', {jobtype: key, amount: parseInt(newVal[key])}));
            }
            // Delete job type
          } else {
            requests.push(Service.request('deleteJobType', {jobTypeId: key}));
          }
        }
      });
      // Update requested fee
      Service.updateRequestedFee(updated);
      return $q.all(requests);
    },
    /**
     * Update requested fee from input value
     */
    updateRequestedFee: function (updated) {
      // Iterate requested fee and update according to input values
      Lazy(Service.displayData.jobTypes)
      .each(function (jobType) {
        if (angular.isDefined(updated[jobType.id])) {
          jobType.requestedFee = updated[jobType.id] === '' ? null : updated[jobType.id];
        }
        return jobType;
      });
    },
    /**
     * Check that we have a valid number
     */
    checkValid: AdminUsersService.checkValidNumber
  };

  /**
   * Inherit from AdminUsersService
   */
  Service.request = AdminUsersService.request.bind(Service, 'jobTypes');
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'jobTypes');
  Service.transformData = AsDataService.transformData.bind(Service);
  // Create the table with the necessary columns
  Service.getDirectiveTableColumns = AdminUsersService.getDirectiveTableColumns.bind(Service);
  return Service;
}]);