describe('AdminUsersJobtypesService', function () {
  var Service, httpBackend, scope, adminUsersService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function (AdminUsersService, $rootScope, $httpBackend, AdminUsersJobtypesService) {
    scope = $rootScope.$new();
    adminUsersService = AdminUsersService;
    httpBackend = $httpBackend;
    Service = AdminUsersJobtypesService;

    spyOn(Service, 'hashData');
    spyOn(Service, 'formatData');
    spyOn(Service, 'request').and.callThrough();

    httpBackend.whenGET(/v2.0\/job-types/).respond({data: [1,2]});
    httpBackend.whenGET(/job-types/).respond({data: [1]});
    httpBackend.whenPOST(/.*/).respond();
    httpBackend.whenPUT(/.*/).respond();
    httpBackend.whenDELETE(/.*/).respond();
  }));

  describe('getJobTypes', function () {
    beforeEach(function () {
      spyOn(Service, 'mergeJobTypes');
      Service.getJobTypes();
      httpBackend.flush();
      scope.$digest();
    });

    it('should request all job types', function () {
      expect(Service.request.calls.argsFor(0)).toEqual(['getAllJobTypes']);
    });
    it('should hash job types', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should request assigned job types', function () {
      expect(Service.request.calls.argsFor(1)).toEqual(['jobSelectedJobTypes']);
    });
    it('should merge assigned job types with all', function () {
      expect(Service.mergeJobTypes).toHaveBeenCalled();
    });
  });

  describe('mergeJobTypes', function () {
    beforeEach(function () {
      Service.safeData.jobTypes = {1: {id: 1}, 2: {id: 2}};
      Service.mergeJobTypes([{id: 1, test: 'test', amount: 100}]);
    });
    it('should merge job types', function () {
      expect(Service.safeData.jobTypes).toEqual({
        1: {
          id: 1,
          test: 'test',
          amount: 100,
          requestedFee: 100
        },
        2: {id: 2}
      });
    });
    it('should format with callback', function () {
      expect(Service.formatData.calls.argsFor(0)[0].toString()).toMatch(/initTable/);
    });
  });

  describe('initTable', function () {
    beforeEach(function () {
      Service.displayData.jobTypes = [{id:1, amount: 900}, {id:2, jobtype: {}, amount: 700}];
      Service.initTable();
    });

    it('should set inputs', function () {
      expect(Service.displayData.inputs).toEqual({ 2: 700 });
    });
    it('should set multiselect', function () {
      expect(Service.displayData.multiSelect).toEqual({ 2: true });
    });
  });

  describe('updateMultiSelect', function () {
    var inputs;
    beforeEach(function () {
      inputs = {1: '', 2: 'hi there'};
      Service.displayData.multiSelect = {1: true, 2: false, 3: true};
      Service.updateMultiSelect(inputs);
    });

    it('should update multiselect', function () {
      expect(Service.displayData.multiSelect).toEqual({ 1: false, 2: true, 3: true });
    });
    it('should update table inputs', function () {
      expect(inputs).toEqual({2: 'hi there'});
    });
  });

  describe('updateForms', function () {
    var oldVal, newVal;
    beforeEach(function () {
      oldVal = {1: 5, 2: 10};
      newVal = {1: 6, 2: '', 3: 100};
      Service.updateForms(newVal, oldVal);
      httpBackend.flush();
      scope.$digest();
    });

    it('should make an update request', function () {
      expect(Service.request.calls.argsFor(0)).toEqual([ 'updateJobType', { url: { jobTypeId: '1' }, body: { amount: 6 } } ]);
    });
    it('should make a delete request', function () {
      expect(Service.request.calls.argsFor(1)).toEqual([ 'deleteJobType', { jobTypeId: '2' } ]);
    });
    it('should make a post request', function () {
      expect(Service.request.calls.argsFor(2)).toEqual([ 'addJobType', { jobtype: '3', amount: 100 } ]);
    });
  });
});