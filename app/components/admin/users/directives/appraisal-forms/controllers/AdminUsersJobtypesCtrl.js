'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersJobtypesCtrl',
['$scope', 'AdminUsersJobtypesService', '$timeout', 'REQUEST_DELAY',
function ($scope, AdminUsersJobtypesService, $timeout, REQUEST_DELAY) {
  var vm = this;
  var Service = AdminUsersJobtypesService;
  // Hold reference to display data
  vm.displayData = Service.displayData;
  // Safe data
  vm.safeData = Service.safeData;
  // Table inputs
  vm.tableInputs = Service.displayData.inputs;
  // Checkboxes
  vm.multiSelect = Service.displayData.multiSelect;
  // Construct the heading based on the non-hidden items
  vm.heading = Service.getDirectiveTableColumns($scope.hide, $scope.show);
  // Table definition
  vm.safeData.heading = vm.heading;

  /**
   * Retrieve data
   */
  vm.init = function () {
    Service.getJobTypes()
    .then(function () {
      vm.attachMultiSelectWatch();
      vm.attachTableInputWatch();
    });
  };

  /**
   * Create table data
   */
  $scope.$watchCollection(function () {
    return Service.displayData.jobTypes;
  }, function (newVal) {
    if (!angular.isArray(newVal) || !newVal.length) {
      return;
    }
    vm.tableData = newVal;
    vm.rowData = vm.tableData.slice();
  });

  /**
   * Watch multi-select
   */
  vm.attachMultiSelectWatch = function () {
    vm.killMultiselectWatch = $scope.$watchCollection(function () {
      return vm.multiSelect;
    }, function (newVal, oldVal) {
      if (!angular.isDefined(newVal) || !Object.keys(oldVal).length) {
        return;
      }
      // Clear rows which are unchecked, and prevent checking empty rows
      angular.forEach(newVal, function (row, key) {
        if (!row) {
          vm.tableInputs[key] = '';
        } else if (!vm.tableInputs[key]) {
          vm.multiSelect[key] = false;
        }
      });
    });
  };

  /**
   * Watch for table inputs being modified
   */
  vm.attachTableInputWatch = function () {
    vm.killTableInputWatch = $scope.$watchCollection(function () {
      return vm.tableInputs;
    }, function (newVal, oldVal) {
      if (!angular.isDefined(newVal) || !Object.keys(oldVal).length) {
        return;
      }
      // Check all checkboxes that have input values
      Service.updateMultiSelect(newVal);
      // Don't run if only empty values have been added
      if (angular.equals(oldVal, newVal)) {
        return;
      }
      // Make sure we have a valid input here
      if (Service.checkValid(newVal)) {
        if (vm.typing) {
          $timeout.cancel(vm.typing);
        } else {
          // Keep reference to the original value when typing began
          if (!vm.oldVal) {
            vm.oldVal = oldVal;
          }
        }
        // Wait and then make the request
        vm.typing = $timeout(function () {
          // Update only those items which are actually being updated
          Service.updateForms(newVal, vm.oldVal)
          .catch(function () {
            $scope.$broadcast('show-modal', 'update-form-failure', true);
          });
          vm.oldVal = null;
          vm.typing = null;
        }, REQUEST_DELAY.ms);
      } else {
        if (vm.typing) {
          $timeout.cancel(vm.typing);
        }
      }
    });
  };

  /**
   * Clean up the directive for switching between views
   */
  $scope.$on('$destroy', function () {
    vm.killMultiselectWatch();
    vm.killTableInputWatch();
    Service.displayData.inputs = {};
    vm.multiSelect = null;
  });

  // Init controller
  vm.init();
}]);