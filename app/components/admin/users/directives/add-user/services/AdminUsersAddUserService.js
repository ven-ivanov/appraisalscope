'use strict';

var app = angular.module('frontendApp');

app.factory('AdminUsersAddUserService',
['AdminUsersService', 'AdminUsersResourceService', '$q', 'AsDataService', '$stateParams',
function (AdminUsersService, AdminUsersResourceService, $q, AsDataService, $stateParams) {
  var Service = {
    // Company ID
    companyId: '',
    // Selected branch
    branchId: '',
    // Hashed raw data
    safeData: {
      // Users
      users: {},
      // Branches
      branches: {},
      // Table data
      tableData: {},
      // Multiselect
      multiSelect: {},
      // Possible table columns
      columns: [
        {
          id: 'user-select',
          label: '',
          data: '',
          checkbox: true,
          noSort: true
        },
        // Name
        {
          label: 'Name',
          data: 'fullName',
          noSort: true
        },
        // Username
        {
          label: 'Username',
          data: 'username',
          noSort: true
        },
        // Phone
        {
          label: 'Phone',
          data: 'phone',
          noSort: true
        },
        // User Type
        {
          label: 'User Type',
          data: 'type',
          noSort: true
        },
        // Permissions
        {
          id: 'clientPermissions',
          label: 'Permission',
          linkLabel: 'Set permissions',
          fn: 'setPermissions',
          noSort: true
        },
        // Appraiser manager permissions
        {
          id: 'appraiserPermissions',
          label: 'Permissions',
          linkLabel: {false: '', true: 'Set permissions'},
          fn: 'setPermissionsAppraiser',
          noSort: true,
          optionalFn: true,
          //data: 'data.type === "Appraiser"'
          data: 'isManager'
        },
        // Summary
        {
          id: 'settings',
          label: 'Summary',
          linkLabel: 'Settings Summary',
          fn: 'settingsSummary',
          noSort: true
        },
        // Delete
        {
          label: 'Delete User',
          linkLabel: 'Delete',
          fn: 'deleteUser',
          noSort: true
        }
      ]
    },
    // Display data
    displayData: {
      // Users
      users: {},
      // Branches
      branches: [],
      // User types
      userTypes: [
        {
          id: 'processor',
          type: 'Processor'
        },
        {
          id: 'manager',
          type: 'Manager'
        },
        {
          id: 'loan-officer',
          type: 'Loan Officer'
        }
      ],
      // Table data
      tableData: [],
      // When creating a new user for a branch
      newUserData: {},
      // Creating a new branch
      newBranch: {},
      // Yes no dropdown
      yesNoDropdown: AdminUsersService.yesNoDropdown
    },
    // Branches (for dropdown)
    branches: [],
    // Multiselect
    multiSelect: {},
    /**
     * Request (either company, user, or branch)
     */
    request: function (requestFn, type, params) {
      // Variable argument length
      if (arguments.length === 2) {
        params = type;
        type = null;
      }
      // Default to record type
      if (!type) {
        // Force appraiser company settings
        if ($stateParams.activeTab === 'appraiser') {
          AdminUsersService.record._type = 'appraiser-company';
        }
        type = AdminUsersService.record._type;
      }
      // Get $resource configuration
      return AdminUsersResourceService.addUsers[requestFn](type, params);
    },
    /**
     * Retrieve current users
     */
    getCurrentUsers: function () {
      // Keep reference to states
      Service.displayData.states = AdminUsersService.states;
      // Get branches
      return Service.request('getBranches')
      .then(function (response) {
        // Appraiser company
        if (AdminUsersService.record._type === 'appraiser-company') {
          // Prepare for appraiser company view
          return Service.getAppraisementMembers(response.data);
        } else {
          // Prepare for client company view
          return Service.prepClientCompanyDisplayData(response.data);
        }
      });
    },
    /**
     * Prepare data for client company view
     * @param data DB data
     */
    prepClientCompanyDisplayData: function (data) {
      var employees = [];
      // Extract employees, assign branch
      Service.extractEmployees(employees, data);
      // Has employees and branches separately
      Service.hashData(employees, 'users');
      Service.hashData(data, 'branches');
      // Create table data
      Service.createTableData(null);
      // Create branches dropdown
      Service.createBranchesDropdown();
    },
    /**
     * Retrieve members for appraiser companies
     */
    getAppraisementMembers: function (data) {
      // Also hash, just for a test
      Service.hashData(data, 'branches');
      // Get appraisement members
      return Service.request('getAppraisementMember')
      .then(function (response) {
        // Add branch and type to each appraisement member
        Service.modifyAppraisementMembers(response);
        // Hash after setting branch
        Service.hashData(response.data, 'users');
        // Create table data
        Service.createTableData(null);
        // Create branches dropdown
        Service.createBranchesDropdown();
      });
    },
    /**
     * Add branch and type to each appraisement member
     */
    modifyAppraisementMembers: function (response) {
      Lazy(response.data)
      .each(function (member) {
        // Get branch ID
        member.branch = member.branch.id;
        // Member type
        member.type = _.capitalize(member.memberType);
      });
    },
    /**
     * Extract employees and place them assign them a branch identifier
     * @param employees Array
     * @param response Array DB data
     */
    extractEmployees: function (employees, response) {
      // Iterate branches, add branch ID to users objects
      Lazy(response)
      .each(function (branch) {
        // Add branch ID to each employee
        Lazy(branch.employees)
        .each(function (employee) {
          employee.branch = branch.id;
          // Add to array for hashing, so we have access to each user
          employees.push(employee);
        });
      });
    },
    /**
     * Create table data from safe data
     */
    createTableData: function (branchId) {
      // Break references
      var users = angular.copy(Service.safeData.users),
        branches = angular.copy(Service.safeData.branches);
      // Default to no selected branch
      branchId = branchId || null;
      Service.displayData.tableData = [];
      // If generating the table for a single branch, remove all except for the one we want
      Service.removeNonSelectedBranches(branches, branchId);
      // Init sorted identifier for branches
      Service.setBranchSortedIdentifier(branches);
      // Add all users to the appropriate branch for display
      Service.addUsersToBranch(branches, users);
      // Hash table data
      Service.hashData(branches, 'tableData');
      // Create it
      Service.formatData('tableData', null);
      // Uncheck users
      Service.uncheckMultiSelect();
    },
    /**
     * Uncheck all multiselect elements
     */
    uncheckMultiSelect: function () {
      angular.forEach(Service.safeData.multiSelect, function (select, key) {
        delete Service.safeData.multiSelect[key];
      });
    },
    /**
     * Remove non-selected branches, when only viewing a single branch in the table
     * @param branches Array Branches
     * @param branchId Int Branch ID
     */
    removeNonSelectedBranches: function (branches, branchId) {
      // If we're looking for a specific branch, remove unused branches
      if (branchId && angular.isDefined(branches[branchId])) {
        Lazy(branches)
        .each(function (branch) {
          if (branch.id !== branchId) {
            delete branches[branch.id];
          }
        });
      }
    },
    /**
     * Set sorted identifier for branches
     * @param branches Array
     */
    setBranchSortedIdentifier: function (branches) {
      // Fill in branch data
      Lazy(branches)
      .each(function (branch) {
        branch.sortedIdentifier = branch.id;
        branch.data = [];
      });
    },
    /**
     * Add all users to the appropriate branch for table data
     * @param branches Array
     * @param users Array
     */
    addUsersToBranch: function (branches, users) {
      var safeBranches = Service.safeData.branches;
      Lazy(users)
      .each(function (user) {
        // If the branch exists, add to it
        if (angular.isDefined(branches[user.branch])) {
          // Keep sorted identifier
          user.sortedIdentifier = user.branch;
          user.branch = safeBranches[user.branch].name;
          user.phone = user.phones.primary.number;
          // Push the whole user object in to create the table rows
          branches[user.sortedIdentifier].data.push(user);
        }
      });
    },
    /**
     * Generate branches dropdown from safedata
     */
    createBranchesDropdown: function () {
      // Create from safe data
      Service.formatData('branches', function () {
        // Add default value
        Service.displayData.branches.unshift({id: 0, name: 'All'});
        // Set to all branches
        Service.displayData.selectedBranch = 0;
      });
    },
    /**
     * Delete the selected user
     */
    deleteUser: function () {
      return Service.request('deleteUser', 'user', Service.safeData.users[Service.selectedUser])
      .then(function () {
        // Delete selected user
        delete Service.safeData.users[Service.selectedUser];
        Service.selectedUser = null;
        // Recreate table
        Service.createTableData();
      });
    },
    /**
     * Create a new branch
     */
    newBranch: function () {
      return Service.request('newBranch', Service.displayData.newBranch)
      .then(function (response) {
        // Add to safe data
        Service.safeData.branches[response.id] = response;
        // Recreate table with new branch
        Service.createTableData();
        // Recreate dropdown
        Service.createBranchesDropdown();
      });
    },
    /**
     * Create new user
     *
     * @todo Need notify property for creating an appraiser
     * @link https://github.com/ascope/manuals/issues/454
     */
    newUser: function () {
      return Service.request('newUser', {
        branchId: Service.displayData.selectedBranch,
        user: Service.displayData.newUserData
      })
      .then(function (response) {
        // Add branch to user
        response.branch = Service.displayData.selectedBranch;
        // Add user to safe data
        Service.safeData.users[response.id] = response;
        // Recreate table data
        Service.createTableData(Service.displayData.selectedBranch);
        // Reset user data
        Service.displayData.newUserData = {};
      });
    },
    /**
     * Delete selected branch
     */
    deleteBranch: function () {
      return Service.request('deleteBranch', {branchId: Service.displayData.selectedBranch})
      .then(function () {
        // Remove from safe data
        delete Service.safeData.branches[Service.displayData.selectedBranch];
        // Deselect this branch
        Service.displayData.selectedBranch = null;
        // Recreate table
        Service.createTableData();
        // Recreate dropdown
        Service.createBranchesDropdown();
      });
    },
    /**
     * Disable branch
     */
    disableBranch: function () {
      // Enable/disable
      var enabled = !Service.safeData.branches[Service.displayData.selectedBranch].isActive;
      return Service.request('disableBranch', {isActive: enabled, branchId: Service.displayData.selectedBranch})
      .then(function () {
        // Save on safe data
        Service.safeData.branches[Service.displayData.selectedBranch].isActive =
        !Service.safeData.branches[Service.displayData.selectedBranch].isActive;
      });
    },
    /**
     * Send login information to selected user
     */
    sendLogin: function () {
      // Get selected users as array
      var userIds = Object.keys(Lazy(Service.safeData.multiSelect)
      .filter(function (selected) {
        return selected;
      })
      .toObject());
      // Make the request
      return Service.request('sendLogin', {
        url: {
          // Branch
          branch: Service.safeData.users[userIds[0]].branch
        },
        employeeIds: userIds
      });
    },
    /**
     * Return whether a branch is enabled
     * @returns {string}
     */
    branchEnabled: function () {
      if (angular.isDefined(Service.safeData.branches[Service.displayData.selectedBranch])) {
        return Service.safeData.branches[Service.displayData.selectedBranch].isActive ?
               'Disable' : 'Enable';
      }
      return '';
    },
    /**
     * Whether user buttons are disabled - Only allow users on the same branch to proceed
     * @returns {boolean}
     */
    singleBranchButtonDisabled: function () {
      var i;
      var branch = false;
      // If branch and user are selected, enable button
      for (i in Service.safeData.multiSelect) {
        if (!Service.safeData.multiSelect.hasOwnProperty(i)) {
          continue;
        }
        if (Service.safeData.multiSelect[i]) {
          // Only allow if on the same branch
          if (branch && branch !== Service.safeData.users[i].branch) {
            return true;
          }
          branch = Service.safeData.users[i].branch;
        }
      }
      return !branch;
    }
  };
  // Hash data
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service);
  Service.transformData = AsDataService.transformData.bind(Service);
  // Create the table with the necessary columns
  Service.getDirectiveTableColumns = AdminUsersService.getDirectiveTableColumns.bind(Service);

  return Service;
}]);