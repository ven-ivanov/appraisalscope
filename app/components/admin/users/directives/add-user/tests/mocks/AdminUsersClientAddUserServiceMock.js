/**
 * Mock for AdminUsersAddUserService
 * @constructor
 */
var AdminUsersClientAddUserServiceMock = function ($q) {
  return {
    companyId: 1,
    // Selected branch
    branchId: 1,
    // Hashed raw data
    safeData: {
      // Users
      users: {},
      // Branches
      branches: {},
      // Table data
      tableData: {},
      // Multiselect
      multiSelect: {},
      // User types
      userTypes: {
        1: {
          id: 1,
          type: 'Processor'
        },
        2: {
          id: 2,
          type: 'Manager'
        },
        3: {
          id: 3,
          type: 'Loan Officer'
        }
      }
    },
    // Display data
    displayData: {
      // Users
      users: {},
      // Branches
      branches: {},
      // User types
      userTypes: [],
      // Table data
      tableData: [],
      // When creating a new user for a branch
      newUserData: {}
    },
    // Branches (for dropdown)
    branches: [],
    // Multiselect
    multiSelect: {},
    // Displaying permissions
    displayPermissionsCallback: '',
    getCurrentUsers: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    createTableData: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    registerDisplaySettings: function (callback) {
      this.displaySettingsCallback = callback;
    },
    callDisplaySettings: function (userId) {
      this.displaySettingsCallback(userId);
    },
    registerDisplayPermissions: function (callback) {
      this.displayPermissionsCallback = callback;
    },
    callDisplayPermissions: function (userId) {
      this.displayPermissionsCallback(userId);
    },
    getUserPermissions: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    getUserSettingsSummary: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    deleteUser: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    newBranch: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    newUser: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    deleteBranch: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    sendLogin: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    getDirectiveTableColumns: function () {

    }
  };
};