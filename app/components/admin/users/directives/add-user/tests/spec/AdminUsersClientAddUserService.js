'use strict';

describe('AdminUsersAddUserService', function () {
  var Service, scope, httpBackend, users, branches, userTypes, adminUsersService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });
  beforeEach(inject(function (AdminUsersAddUserService, $rootScope, $httpBackend, AdminUsersService) {
    Service = AdminUsersAddUserService;
    adminUsersService = AdminUsersService;
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    users = {1: {id: 1, branch: 1, userType: 1, firstName: 'Jeff', lastName: 'Perkins', type: 1, phones: {primary: {number: 1}}, company: {id: 1}}, 2:{id: 2, branch: 2, userType: 2, firstName: "Mallory", lastName: 'Aisha', type: 2, phones: {primary: {number: 2}}, company: {id: 2}}};
    //branches = [{id: 3, branch: 'Branch 3'}, {id: 4, branch: 'Branch 4'}];
    branches = {1: {id: 1, test: 1, name: 'Branch 1', data: [], isActive: true}, 2: {id: 2, test: 2, name: 'Branch 2', data: [], isActive: false}};
    userTypes = [{id: 1, type: 'Saleclerk'}, {id: 2, type: 'Whatever'}];
    // Backend
    httpBackend.whenGET(/employees/).respond(users);
    httpBackend.whenGET(/branches/).respond();
    httpBackend.whenGET(/members/).respond({data: []});
    httpBackend.whenGET(/user-type/).respond(userTypes);
    httpBackend.whenPOST(/branch/).respond({id: 100});
    httpBackend.whenPUT(/new-user/).respond({id: 100});
    httpBackend.whenDELETE(/branch/).respond();
    httpBackend.whenPATCH(/branch/).respond();
    // Set some data
    Service.safeData.users = users;
    Service.safeData.branches = branches;
    Service.displayData.branches = [];
    // Spies
    spyOn(Service, 'hashData');
    spyOn(Service, 'formatData').and.callThrough();
    // Set states on AdminUsersService
    adminUsersService.states = [{AK: 'Arkansas'}];
  }));

  describe('getCurrentUsers', function () {
    describe('client', function () {
      beforeEach(function () {
        spyOn(Service, 'prepClientCompanyDisplayData');
        Service.getCurrentUsers();
        httpBackend.flush();
        scope.$digest();
      });

      it('should set states', function () {
        expect(Service.displayData.states).toEqual([{AK: 'Arkansas'}]);
      });
      it('should prepare display data', function () {
        expect(Service.prepClientCompanyDisplayData).toHaveBeenCalled();
      });
    });

    describe('appraiser', function () {
      beforeEach(function () {
        adminUsersService.record._type = 'appraiser-company';
        spyOn(Service, 'getAppraisementMembers');
        Service.getCurrentUsers();
        httpBackend.flush();
        scope.$digest();
      });

      it('should prep appraiser data', function () {
        expect(Service.getAppraisementMembers).toHaveBeenCalled();
      });
    });
  });

  describe('prepClientCompanyDisplayData', function () {
    beforeEach(function () {
      spyOn(Service, 'extractEmployees');
      spyOn(Service, 'createTableData');
      spyOn(Service, 'createBranchesDropdown');
      Service.prepClientCompanyDisplayData([]);
    });

    it('should extract employees', function () {
      expect(Service.extractEmployees).toHaveBeenCalled();
    });
    it('should hash employees', function () {
      expect(Service.hashData.calls.argsFor(0)).toEqual([ [  ], 'users' ]);
    });
    it('should hash branches', function () {
      expect(Service.hashData.calls.argsFor(1)).toEqual([ [  ], 'branches' ]);
    });
    it('should create table data', function () {
      expect(Service.createTableData).toHaveBeenCalled();
    });
    it('should create branches dropdown', function () {
      expect(Service.createBranchesDropdown).toHaveBeenCalled();
    });
  });

  describe('getAppraisementMembers', function () {
    var data;
    beforeEach(function () {
      Service.managersTemp = [{id: 1}, {id: 2}];
      spyOn(Service, 'modifyAppraisementMembers');
      spyOn(Service, 'createTableData');
      spyOn(Service, 'createBranchesDropdown');
      data = {};
      Service.getAppraisementMembers(data);
      httpBackend.flush();
      scope.$digest();
    });

    it('should hash data for branches', function () {
      expect(Service.hashData.calls.argsFor(0)).toEqual([{}, 'branches' ]);
    });
    it('should modify appraisement members in preparation for display', function () {
      expect(Service.modifyAppraisementMembers).toHaveBeenCalled();
    });
    it('should hash for users', function () {
      expect(Service.hashData.calls.argsFor(1)).toEqual([ [], 'users' ]);
    });
    it('should create table data', function () {
      expect(Service.createTableData).toHaveBeenCalled();
    });
    it('should create branches dropdown', function () {
      expect(Service.createBranchesDropdown).toHaveBeenCalled();
    });
  });

  describe('modifyAppraisementMembers', function () {
    var data;
    beforeEach(function () {
      data = {data: [{branch: {id: 1}, memberType: 'appraiser'}, {branch: {id: 2}, memberType: 'manager'}]};
      Service.modifyAppraisementMembers(data);
    });

    it('should set the branch ID', function () {
      expect(data.data[0].branch).toEqual(1);
    });
    it('should set the record type', function () {
      expect(data.data[0].type).toEqual('Appraiser');
    });
  });

  describe('extractEmployees', function () {
    var employees = [];
    beforeEach(function () {
      Service.extractEmployees(employees, [{id: 1, employees: [{id:1}, {id: 2}]}, {id: 2, employees: [{id: 3},{id:4}]}])
    });

    it('should separate employees and note their branch', function () {
      expect(employees).toEqual([ { id: 1, branch: 1 }, { id: 2, branch: 1 }, { id: 3, branch: 2 }, { id: 4, branch: 2 } ]);
    });
  });

  describe('createTableData', function () {
    beforeEach(function () {
      spyOn(Service, 'removeNonSelectedBranches');
      spyOn(Service, 'setBranchSortedIdentifier');
      spyOn(Service, 'addUsersToBranch');
      spyOn(Service, 'uncheckMultiSelect');
      Service.createTableData();
    });

    it('should call removeNonSelectedBranches', function () {
      expect(Service.removeNonSelectedBranches).toHaveBeenCalled();
    });
    it('should call setBranchSortedIdentifier', function () {
      expect(Service.setBranchSortedIdentifier).toHaveBeenCalled();
    });
    it('should call addUsersToBranch', function () {
      expect(Service.addUsersToBranch).toHaveBeenCalled();
    });
    it('should call hashData', function () {
      var args = Service.hashData.calls.argsFor(0);
      expect(args[args.length - 1]).toEqual('tableData');
    });
    it('should call formatData', function () {
      var args = Service.formatData.calls.argsFor(0);
      expect(args[0]).toEqual('tableData');
    });
    it('should set multiselect', function () {
      expect(Service.uncheckMultiSelect).toHaveBeenCalled();
    });
  });

  describe('uncheckMultiSelect', function () {
    var multiSelect;
    beforeEach(function () {
      Service.safeData.multiSelect = {1: true, 2: false};
      multiSelect = Service.safeData.multiSelect;
      Service.uncheckMultiSelect();
    });

    it('should delete all multiselect properties', function () {
      expect(Service.safeData.multiSelect).toEqual({});
    });
    it('should not break the reference between multiselect and safe data', function () {
      Service.safeData.multiSelect[1] = true;
      expect(Service.safeData.multiSelect).toEqual(multiSelect);
      expect(multiSelect).toEqual({1: true});
    });
  });

  describe('removeNonSelectedBranches', function () {
    var branchId = 1;
    it('should leave all branches intact if no branch ID is specified', function () {
      Service.removeNonSelectedBranches(branches, null);
      expect(branches).toEqual({
        1: {
          id: 1,
          test: 1,
          name: 'Branch 1',
          data: [],
          isActive: true
        },
        2: {
          id: 2,
          test: 2,
          name: 'Branch 2',
          data: [],
          isActive: false
        }
      });
    });
    it('should remove all branches other than the selected branch', function () {
      Service.removeNonSelectedBranches(branches, branchId);
      expect(branches).toEqual({
        1: {
          id: 1,
          test: 1,
          name: 'Branch 1',
          data: [],
          isActive: true
        }
      });
    });
  });

  describe('setBranchSortedIdentifier', function () {
    beforeEach(function () {
      Service.setBranchSortedIdentifier(branches);
    });
    it('should set sorted identifier on all branches', function () {
      expect(branches).toEqual({
        1: {
          id: 1,
          test: 1,
          name: 'Branch 1',
          data: [],
          isActive: true,
          sortedIdentifier: 1
        },
        2: {
          id: 2,
          test: 2,
          name: 'Branch 2',
          data: [],
          isActive: false,
          sortedIdentifier: 2
        }
      });
    });
  });

  describe('addUsersToBranch', function () {
    beforeEach(function () {
      Service.addUsersToBranch(branches, users);
    });

    it('should correctly set user information', function () {
      expect(users).toEqual({
        1: {
          id: 1,
          branch: 'Branch 1',
          userType: 1,
          firstName: 'Jeff',
          lastName: 'Perkins',
          type: 1,
          phones: {primary: {number: 1}},
          company: {id: 1},
          sortedIdentifier: 1,
          phone: 1
        },
        2: {
          id: 2,
          branch: 'Branch 2',
          userType: 2,
          firstName: 'Mallory',
          lastName: 'Aisha',
          type: 2,
          phones: {primary: {number: 2}},
          company: {id: 2},
          sortedIdentifier: 2,
          phone: 2
        }
      });
    });
  });

  describe('createBranchesDropdown', function () {
    beforeEach(function () {
      Service.createBranchesDropdown();
      scope.$digest();
    });
    it('should call the callback and init data', function () {
      expect(Service.displayData.branches).toEqual([{
        id: 0,
        name: 'All'
      }, {
        id: 1,
        test: 1,
        name: 'Branch 1',
        data: [],
        isActive: true
      }, {
        id: 2,
        test: 2,
        name: 'Branch 2',
        data: [],
        isActive: false
      }]);
    });
    it('should set selected branch to 0', function () {
      expect(Service.displayData.selectedBranch).toEqual(0);
    });
  });

  describe('deleteUser', function () {
    beforeEach(function () {
      spyOn(Service, 'createTableData');
      Service.selectedUser = 1;
      Service.deleteUser();
      httpBackend.flush();
      scope.$digest();
    });

    it('should remove selected user', function () {
      expect(Service.safeData.users).toEqual( { 2: { id: 2, branch: 2, userType: 2, firstName: 'Mallory', lastName: 'Aisha', type: 2, phones: { primary: { number: 2 } }, company: { id: 2 } } } );
    });
    it('should reset selected user', function () {
      expect(Service.selectedUser).toEqual(null);
    });
    it('should call createTableData', function () {
      expect(Service.createTableData).toHaveBeenCalled();
    });
  });

  describe('newBranch', function () {
    beforeEach(function () {
      spyOn(Service, 'createTableData');
      spyOn(Service, 'createBranchesDropdown');
      Service.displayData.newBranchName = 'new branch';
      Service.newBranch();
      httpBackend.flush();
      scope.$digest();
    });

    it('should add to branches in safe data', function () {
      expect(Service.safeData.branches[100].id).toEqual(100);
    });
    it('should call createTableData', function () {
      expect(Service.createTableData).toHaveBeenCalled();
    });
    it('should call createBranchesDropdown', function () {
      expect(Service.createBranchesDropdown).toHaveBeenCalled();
    });
  });

  describe('newUser', function () {
    beforeEach(function () {
      Service.displayData.selectedBranch = 1;
      Service.displayData.newUserData = {};
      spyOn(Service, 'createTableData');
      Service.newUser();
      httpBackend.flush();
      scope.$digest();
    });
    it('should add the new user to safe data', function () {
      expect(Service.safeData.users[100].id).toEqual(100);
    });
    it('should create table data', function () {
      expect(Service.createTableData).toHaveBeenCalledWith(1);
    });
    it('should clear new user data', function () {
      expect(Service.displayData.newUserData).toEqual({});
    });
  });

  describe('deleteBranch', function () {
    beforeEach(function () {
      spyOn(Service, 'createTableData');
      spyOn(Service, 'createBranchesDropdown');
      Service.displayData.selectedBranch = 1;
      Service.deleteBranch();
      httpBackend.flush();
      scope.$digest();
    });

    it('should update safe data', function () {
      expect(Service.safeData.branches).toEqual({
        2: {
          id: 2,
          test: 2,
          name: 'Branch 2',
          data: [],
          isActive: false
        }
      });
    });
    it('should clear selected branch', function () {
      expect(Service.displayData.selectedBranch).toEqual(null);
    });
    it('should call create table data', function () {
      expect(Service.createTableData).toHaveBeenCalled();
    });
    it('should call createBranchesDropdown', function () {
      expect(Service.createBranchesDropdown).toHaveBeenCalled();
    });
  });

  describe('disableBranch', function () {
    beforeEach(function () {
      Service.safeData.branches[1].isActive = true;
      Service.displayData.selectedBranch = 1;
      Service.disableBranch();
      httpBackend.flush();
      scope.$digest();
    });

    it('should update safe data', function () {
      expect(Service.safeData.branches).toEqual({
        1: {
          id: 1,
          test: 1,
          name: 'Branch 1',
          data: [],
          isActive: false
        },
        2: {
          id: 2,
          test: 2,
          name: 'Branch 2',
          data: [],
          isActive: false
        }
      });
    });
  });

  describe('sendLogin', function () {
    beforeEach(function () {
      spyOn(Service, 'request');
      Service.safeData.multiSelect = {1: true, 2: false};
      Service.sendLogin();
    });

    it('should select the correct user IDs', function () {
      expect(Service.request.calls.argsFor(0)).toEqual(['sendLogin', {
        url: {branch: 1},
        employeeIds: ['1']
      }]);
    });
  });

  describe('branchEnabled', function () {
    it('should return enabled for enabled branches', function () {
      Service.displayData.selectedBranch = 1;
      expect(Service.branchEnabled()).toEqual('Disable');
    });

    it('should return disabled for disabled branches', function () {
      Service.displayData.selectedBranch = 2;
      expect(Service.branchEnabled()).toEqual('Enable');
    });
  });

  describe('singleBranchButtonDisabled', function () {
    var response;
    var check = function (multiSelect) {
      Service.safeData.multiSelect = multiSelect;
      response = Service.singleBranchButtonDisabled();
    };
    it('should return true if no user is checked', function () {
      check({});
      expect(response).toEqual(true);
    });
    it('should return false if only users from a single branch are checked', function () {
      check({1: true, 2: false});
      expect(response).toEqual(false);
    });
    it('should return true if users from multiple branches are checked', function () {
      check({1: true, 2: true});
      expect(response).toEqual(true);
    });
  });
});