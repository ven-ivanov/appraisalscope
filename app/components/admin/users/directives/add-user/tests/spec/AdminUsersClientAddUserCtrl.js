'use strict';

describe('AdminUsersClientAddUserCtrl', function () {
  var scope, controller, Service, adminUsersClientService, $timeout, userId, domLoadService, adminUsersBranchSettingsService, massUpdateUsersService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersAddUserService', AdminUsersClientAddUserServiceMock);
      $provide.factory('AdminUsersClientService', AdminUsersClientServiceMock);
      $provide.factory('DomLoadService', DomLoadServiceMock);
      $provide.factory('AdminUsersBranchSettingsService', AdminUsersBranchSettingsServiceMock);
      $provide.factory('MassUpdateUsersService', MassUpdateUsersServiceMock);
    });
  });
  beforeEach(inject(function ($rootScope, $controller, AdminUsersAddUserService, AdminUsersClientService, _$timeout_, AdminUsersBranchSettingsService, MassUpdateUsersService) {
    domLoadService = DomLoadServiceMock;
    scope = $rootScope.$new();
    $timeout = _$timeout_;
    Service = AdminUsersAddUserService;
    adminUsersClientService = AdminUsersClientService;
    massUpdateUsersService = MassUpdateUsersService;
    Service.safeData.multiSelect = [{1: true, 2: true, 3: false}];
    spyOn(scope, '$broadcast').and.callThrough();
    spyOn(adminUsersClientService, 'revertDetails');
    userId = 1;
    controller = $controller('AdminUsersClientAddUserCtrl', {$scope: scope});
    // Fake scrolltop
    scope.scrollTop = function () {};
    // Compile table
    scope.compileTable = function(){};
    adminUsersBranchSettingsService = AdminUsersBranchSettingsService;
  }));

  describe('init', function () {
    beforeEach(function () {
      spyOn(Service, 'getCurrentUsers').and.callThrough();
      spyOn(scope, 'compileTable');
      controller.init();
      scope.$digest();
    });
    it('should retrieve current users from service', function () {
      expect(Service.getCurrentUsers).toHaveBeenCalled();
    });
    it('should compile the table', function () {
      expect(scope.compileTable).toHaveBeenCalled();
    });
  });

  describe('selectedBranch', function () {
    beforeEach(function () {
      spyOn(Service, 'createTableData');
      controller.displayData.selectedBranch = 1;
      scope.$digest();
    });

    it('should call createTableData', function () {
      expect(Service.createTableData).toHaveBeenCalled();
    });
  });

  describe('deleteUser', function () {
    beforeEach(function () {
      spyOn(Service, 'deleteUser').and.callThrough();
      controller.deleteUser();
      spyOn(scope, 'compileTable');
      scope.$digest();
    });
    it('should call deleteUser', function () {
      expect(Service.deleteUser).toHaveBeenCalled();
    });
    it('should recompile table rows', function () {
      expect(scope.compileTable).toHaveBeenCalled();
    });
  });

  describe('newBranch', function () {
    it('should call newBranch', function () {
      spyOn(Service, 'newBranch').and.callThrough();
      controller.newBranchName = 'New branch';
      controller.newBranch();
      scope.$digest();
      expect(Service.newBranch).toHaveBeenCalled();
    });
  });

  describe('newUser', function () {
    beforeEach(function () {
      controller.displayData.newUserData = {name: "New User"};
      spyOn(Service, 'newUser').and.callThrough();
      controller.newUser();
      scope.$digest();
    });
    it('should call newUser', function () {
      expect(Service.newUser).toHaveBeenCalled();
    });
  });

  describe('deleteBranch', function () {
    beforeEach(function () {
      spyOn(Service, 'deleteBranch').and.callThrough();
      controller.deleteBranch();
      scope.$digest();
    });
    it('should call deleteBranch', function () {
      expect(Service.deleteBranch).toHaveBeenCalled();
    });
  });

  describe('sendLogin', function () {
    beforeEach(function () {
      spyOn(Service, 'sendLogin').and.callThrough();
      controller.sendLogin();
      scope.$digest();
    });
    it('should have called sendLogin', function () {
      expect(Service.sendLogin).toHaveBeenCalled();
    });
  });

  describe('branchSettings', function () {
    beforeEach(function () {
      spyOn(adminUsersBranchSettingsService, 'getBranch').and.callThrough();
      controller.branchSettings();
      scope.$digest();
    });
    it('should call getBranch', function () {
      expect(adminUsersBranchSettingsService.getBranch).toHaveBeenCalled();
    });
    it('should revert details when the modal closes', function () {
      scope.$broadcast('modal-closed-branch-settings');
      scope.$digest();
      expect(adminUsersClientService.revertDetails).toHaveBeenCalled();
    });
  });

  describe('massUpdateUsers', function () {
    beforeEach(function () {
      Service.safeData.multiSelect = {1: true, 2: false};
      Service.safeData.users = {1: {branch: 1}};
      spyOn(massUpdateUsersService, 'massUpdateUsers').and.callThrough();
      controller.massUpdateUsers();
    });
    it('should call the function with the correct parameters', function () {
      expect(massUpdateUsersService.massUpdateUsers).toHaveBeenCalledWith([ '1', '2' ], 1 );
    });
    it('should render the directive', function () {
      expect(controller.displayUserMassUpdate).toEqual(true);
    });
    it('should kill the render on close and revert details', function () {
      scope.$digest();
      scope.$broadcast('modal-closed-user-mass-update');
      scope.$digest();
      expect(controller.displayUserMassUpdate).toEqual(false);
      expect(adminUsersClientService.revertDetails).toHaveBeenCalled();
    });
  });
});