'use strict';

describe('AdminUsersClientAddUserTableCtrl', function () {
  var scope, adminUsersAddUserService, adminUsersClientService, controller, timeout, $q, adminUsersAppraiserService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersClientService', AdminUsersClientServiceMock);
      $provide.factory('AdminUsersAddUserService', AdminUsersClientAddUserServiceMock);
      $provide.factory('AdminUsersAppraiserService', AdminUsersAppraiserServiceMock);
    });
  });
  beforeEach(inject(function ($rootScope, AdminUsersAddUserService, AdminUsersClientService, $controller, $timeout, _$q_, AdminUsersAppraiserService) {
    scope = $rootScope.$new();
    timeout = $timeout;
    $q = _$q_;
    adminUsersAppraiserService = AdminUsersAppraiserService;
    adminUsersAddUserService = AdminUsersAddUserService;
    adminUsersClientService = AdminUsersClientService;
    scope.scrollTop = function () {};
    controller = $controller('AdminUsersClientAddUserTableCtrl', {$scope: scope});
  }));

  describe('linkFn', function () {
    beforeEach(function () {
      spyOn(controller, 'displayPermissions');
      spyOn(controller, 'displaySettingsSummary');
      spyOn(adminUsersClientService, 'getUserDetails');
    });
    it('should always set the user ID', function () {
      controller.linkFn(1, 'whatever');
      expect(adminUsersAddUserService.selectedUser).toEqual(1);
    });
    it('should always retrieve user settings from client service', function () {
      controller.linkFn(1, 'whatever');
      expect(adminUsersClientService.getUserDetails).toHaveBeenCalled();
    });
    it('should display permissions', function () {
      controller.linkFn(1, 'setPermissions');
      expect(controller.displayPermissions).toHaveBeenCalled();
    });
    it('should display permissions for an appraiser', function () {
      controller.linkFn(1, 'setPermissionsAppraiser');
      expect(controller.displayPermissions).toHaveBeenCalledWith(1, true);
    });
    it('should display settings', function () {
      controller.linkFn(1, 'settingsSummary');
      expect(controller.displaySettingsSummary).toHaveBeenCalled();
    });
    it('should display the modal for deleting users', function () {
      spyOn(scope, '$emit');
      controller.linkFn(1, 'deleteUser');
      expect(scope.$emit).toHaveBeenCalled();
    });
  });

  describe('displaySettingsSummary', function () {
    beforeEach(function () {
      adminUsersClientService.getUserSettingsSummary = function () {
        return $q(function (resolve) {
          resolve();
        });
      };
      spyOn(adminUsersClientService, 'getUserSettingsSummary').and.callThrough();
      controller.displaySettingsSummary();
      scope.$digest();
    });

    it('should call getUserSettingsSummary()', function () {
      expect(adminUsersClientService.getUserSettingsSummary).toHaveBeenCalled();
    });
  });

  describe('displayPermissions', function () {
    describe('client', function () {
      beforeEach(function () {
        spyOn(adminUsersClientService, 'getUserPermissions').and.callThrough();
        controller.displayPermissions(1);
        scope.$digest();
      });

      it('should call getUserPermissions', function () {
        expect(adminUsersClientService.getUserPermissions ).toHaveBeenCalled();
      });
    });

    describe('appraiser', function () {
      beforeEach(function () {
        spyOn(adminUsersAppraiserService, 'getAppraiserManagerPermissions').and.callThrough();
        controller.displayPermissions(1, true);
        scope.$digest();
      });

      it('should call getUserPermissions', function () {
        expect(adminUsersAppraiserService.getAppraiserManagerPermissions ).toHaveBeenCalled();
      });
    });
  });
});