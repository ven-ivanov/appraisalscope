'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersAddUser', ['AdminUsersDirectiveInheritanceService', '$compile', function (AdminUsersDirectiveInheritanceService, $compile) {
  return {
    templateUrl: '/components/admin/users/directives/add-user/directives/partials/add-user.html',
    controller: 'AdminUsersClientAddUserCtrl',
    controllerAs: 'addUserCtrl',
    scope: {
      appraiser: '@',
      // Conditionally hide table columns
      hide: '@'
    },
    link: function (scope) {
      // Inherit scroll to top
      AdminUsersDirectiveInheritanceService.inherit.call(scope);
      scope.appraiser = !!scope.$eval(scope.appraiser);

      var template = '<div class="clearfix col-md-12" id="add-users-table"><as-table totals="false" hide-page-numbers="true" table-id="current-users-table" infinite-scroll="false" bold-columns="2,4" overflow-x="hidden" ng-controller="AdminUsersClientAddUserTableCtrl as tableCtrl"></as-table></div>';

      // Compile table
      scope.compileTable = function () {
        angular.element('#add-users-table').replaceWith($compile(template)(scope));
      };
      // Deconstruct table on destroy
      scope.deconstructTable = function () {
        angular.element('#add-users-table').replaceWith('<div class="clearfix col-md-12" id="add-users-table"></div>');
      };
    }
  };
}]);