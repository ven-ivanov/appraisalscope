'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersClientAddUserTableCtrl',
['$scope', 'AdminUsersAddUserService', 'AdminUsersClientService', '$timeout', 'AdminUsersAppraiserService',
function ($scope, AdminUsersAddUserService, AdminUsersClientService, $timeout, AdminUsersAppraiserService) {
  var vm = this;
  // Keep reference to safe data
  vm.safeData = AdminUsersAddUserService.safeData;
  // Table heading (built in main ctrl)
  vm.heading = vm.safeData.heading;
  // How the rows should be grouped (by branch, name as identifier)
  vm.tableGrouping = ['name'];
  // Initialize empty
  vm.tableData = [];
  vm.tableInputs = {};
  vm.multiSelect = {};

  /**
   * Keep reference to multiselect on service
   */
  $scope.$watchCollection(function () {
    return vm.multiSelect;
  }, function (newVal) {
    if (angular.isUndefined(newVal) || !Object.keys(newVal).length) {
      return;
    }
    AdminUsersAddUserService.safeData.multiSelect = newVal;
  });

  /**
   * Table data
   */
  $scope.$watchCollection(function () {
    return AdminUsersAddUserService.displayData.tableData;
  }, function (newVal) {
    if (!angular.isArray(newVal) || !newVal.length) {
      return;
    }
    vm.tableData = newVal;
    vm.rowData = newVal.slice();
  });

  /**
   * Row link functions
   */
  vm.linkFn = function (userId, fnName) {
    // Get user
    AdminUsersAddUserService.selectedUser = userId;
    // Expose user details
    AdminUsersClientService.getUserDetails(userId);
    // Display permissions
    // @todo This whole permissions bit is waiting on the backend
    if (fnName === 'setPermissions') {
      //AdminUsersAddUserService.callDisplayPermissions(userId);
      vm.displayPermissions(userId);
      // Display permissions for appraiser
    } else if (fnName === 'setPermissionsAppraiser') {
      vm.displayPermissions(userId, true);
      // Display settings
    } else if (fnName === 'settingsSummary') {
      //AdminUsersAddUserService.callDisplaySettings(userId);
      vm.displaySettingsSummary(userId);
      // Delete user
    } else if (fnName === 'deleteUser') {
      // Display confirmation modal
      $scope.$emit('show-modal', 'delete-user-confirm', true);
      $scope.scrollTop('#user-details');
      $scope.scrollTop();
    }
  };

  /**
   * Display user settings
   */
  vm.displaySettingsSummary = function (userId) {
    // Retrieve user settings
    AdminUsersClientService.getUserSettingsSummary(AdminUsersAddUserService.safeData.users[userId])
    .then(function () {
      $timeout(function () {
        $scope.$emit('show-modal', 'user-settings', true);
        $scope.scrollTop('#user-details');
        $scope.scrollTop();
      });
    }, function () {
      $scope.$broadcast('show-modal', 'user-settings-failure');
      $scope.scrollTop('#user-details');
      $scope.scrollTop();
    });
  };

  /**
   * Display user permissions
   *
   * @todo When backend is complete
   */
  vm.displayPermissions = function (userId, appraiser) {
    appraiser = appraiser || false;
    var retrievalFunction;
    // Get appraiser manager
    if (appraiser) {
      /**
       * @todo Waiting on permissions on the backend
       * @link https://github.com/ascope/manuals/issues/453
       */
      retrievalFunction = AdminUsersAppraiserService.getAppraiserManagerPermissions(userId);
    } else {
      // Retrieve user settings
      retrievalFunction = AdminUsersClientService.getUserPermissions(userId);
    }
    retrievalFunction
    .then(function () {
      $timeout(function () {
        $scope.$emit('show-modal', 'user-permissions', true);
        $scope.scrollTop('#user-details');
        $scope.scrollTop();
      });
    }, function () {
      $scope.$broadcast('show-modal', 'user-permissions-failure');
      $scope.scrollTop('#user-details');
      $scope.scrollTop();
    });
  };
}]);