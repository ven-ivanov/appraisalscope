'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersClientAddUserCtrl',
['$scope', 'AdminUsersAddUserService', 'AdminUsersClientService', '$timeout', 'DomLoadService',
 'AdminUsersBranchSettingsService', 'MassUpdateUsersService', function (
$scope, AdminUsersAddUserService, AdminUsersClientService, $timeout, DomLoadService,
AdminUsersBranchSettingsService, MassUpdateUsersService) {
  var vm = this;
  var Service = AdminUsersAddUserService;
  // Safe and display data
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;
  // Construct the heading based on the non-hidden items
  vm.safeData.heading = Service.getDirectiveTableColumns($scope.hide, $scope.show);

  /**
   * Initialize view
   */
  vm.init = function () {
    Service.getCurrentUsers()
    .then(function () {
      Service.watchBranch();
    })
    .then(function () {
      $scope.compileTable();
    });
  };

  /**
   * Branch change
   */
  Service.watchBranch = function () {
    Service.killBranchWatch = $scope.$watch(function () {
      return vm.displayData.selectedBranch;
    }, function (newVal, oldVal) {
      if (angular.isUndefined(newVal) || angular.isUndefined(oldVal)) {
        return;
      }
      // Regenerate the table for only this branch
      Service.createTableData(newVal);
    });
  };

  /**
   * Delete selected user
   */
  vm.deleteUser = function () {
    // Remove from table
    Service.deleteUser()
    .then(function () {
      // Hide modal
      $scope.$broadcast('hide-modal', 'delete-user-confirm');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'delete-user-failure', true);
      $scope.scrollTop('#user-details');
      $scope.scrollTop();
    })
    .finally(function () {
      $scope.compileTable();
    });
  };

  /**
   * Add a new branch modal
   */
  vm.newBranchConfirmation = function () {
    $scope.$broadcast('show-modal', 'new-branch-confirm', true);
    $scope.scrollTop('#user-details');
    $scope.scrollTop();
  };
  /**
   * Add branch
   */
  vm.newBranch = function () {
    // Send request, recreate table
    Service.newBranch()
    .then(function () {
      // Hide create branch modal
      $scope.$broadcast('hide-modal', 'new-branch-confirm');
    })
    .catch(function () {
      // Failure modal
      $scope.$broadcast('show-modal', 'new-branch-failure', true);
      $scope.scrollTop('#user-details');
      $scope.scrollTop();
    });
  };

  /**
   * New user create modal
   */
  vm.newUserConfirmation = function (manager) {
    // Creating an appraiser manager
    $scope.manager = manager;
    if ($scope.appraiser) {
      vm.addUserText = manager === true ? 'manager' : 'appraiser';
    } else {
      vm.addUserText = 'user';
    }
    $scope.$broadcast('show-modal', 'new-user-confirmation', true);
    $scope.scrollTop('#user-details');
    $scope.scrollTop();
  };
  /**
   * Create new user
   */
  vm.newUser = function () {
    Service.newUser()
    .then(function () {
      // Close modal
      $scope.$broadcast('hide-modal', 'new-user-confirmation');
    })
    .catch(function () {
      // Display error modal
      $scope.$broadcast('show-modal', 'new-user-failure');
      $scope.scrollTop('#user-details');
      $scope.scrollTop();
    });
  };
  /**
   * Delete branch confirmation
   */
  vm.deleteBranchConfirmation = function () {
    $scope.$broadcast('show-modal', 'delete-branch-confirmation', true);
    $scope.scrollTop('#user-details');
    $scope.scrollTop();
  };
  /**
   * Delete branch
   */
  vm.deleteBranch = function () {
    Service.deleteBranch()
    .then(function () {
      $scope.$broadcast('hide-modal', 'delete-branch-confirmation');
    })
    .catch(function () {
      // Error
      $scope.$broadcast('show-modal', 'delete-branch-failure', true);
      $scope.scrollTop('#user-details');
      $scope.scrollTop();
    });
  };

  /**
   * Disable/enable branch
   */
  vm.disableBranch = function () {
    Service.disableBranch()
    .catch(function () {
      $scope.$broadcast('show-modal', 'disable-branch-failure', true);
      $scope.scrollTop('#user-details');
      $scope.scrollTop();
    });
  };

  /**
   * Branch enable/disable button text
   */
  vm.branchEnabled = function () {
    return Service.branchEnabled();
  };

  /**
   * Only enable the send login info or mass update users when a user is selected
   */
  vm.singleBranchButtonDisabled = function () {
    return Service.singleBranchButtonDisabled();
  };

  /**
   * Send login information to selected users confirmation
   */
  vm.sendLoginConfirmation = function () {
    $scope.$broadcast('show-modal', 'send-login-confirmation', true);
    $scope.scrollTop('#user-details');
    $scope.scrollTop();
  };
  /**
   * Send login to selected users
   */
  vm.sendLogin = function () {
    Service.sendLogin()
    .then(function () {
      $scope.$broadcast('hide-modal', 'send-login-confirmation');
      $scope.$broadcast('show-modal', 'send-login-success', true);
      $scope.scrollTop('#user-details');
      $scope.scrollTop();
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'send-login-failure', true);
      $scope.scrollTop('#user-details');
      $scope.scrollTop();
    });
  };

  /**
   * Display branch settings
   */
  vm.branchSettings = function () {
    vm.displayBranchSettings = true;
    // Get branch details
    AdminUsersBranchSettingsService.getBranch(Service.displayData.selectedBranch)
    .then(function () {
      $scope.$broadcast('show-modal', 'branch-settings', true);
      $scope.scrollTop('#user-details');
      $scope.scrollTop();
      // Restore details on close
      var closed = $scope.$on('modal-closed-branch-settings', function () {
        vm.displayBranchSettings = false;
        // Go back to the original details
        AdminUsersClientService.revertDetails();
        closed();
      });
    }).catch(function () {
      // Unable to display settings
      $scope.$broadcast('show-modal', 'branch-settings-failure', true);
      $scope.scrollTop('#user-details');
      $scope.scrollTop();
      vm.displayBranchSettings = false;
    });
  };

  /**
   * Mass update selected users
   */
  vm.massUpdateUsers = function () {
    vm.displayUserMassUpdate = true;
    var users = Object.keys(Service.safeData.multiSelect);
    // Prepare for mass update
    MassUpdateUsersService.massUpdateUsers(users, Service.safeData.users[Number(users[0])].branch);
    // Allow directive to load, then display it
    DomLoadService.load().then(function () {
      //Mass update users
      $scope.$broadcast('show-modal', 'user-mass-update', true);
      $scope.scrollTop('#user-details');
      $scope.scrollTop();
      var closed = $scope.$on('modal-closed-user-mass-update', function () {
        vm.displayUserMassUpdate = false;
        // Revert back to the original details
        AdminUsersClientService.revertDetails();
        closed();
      });
    });
  };

  /**
   * Form submit disabled
   * @param formName String
   */
  vm.formSubmitDisabled = function (formName) {
    switch (formName) {
      case 'newUser':
        return !vm.newUserForm.$valid;
      case 'newBranch':
        return !vm.newBranchForm.$valid;
    }
  };

  /**
   * Button is disabled
   * @param buttonName String
   */
  vm.buttonDisabled = function (buttonName) {
    switch (buttonName) {
      case 'branchSelected':
        return !vm.displayData.selectedBranch;
    }
  };

  /**
   * Remove watchers
   */
  $scope.$on('$destroy', function () {
    $scope.deconstructTable();
    Service.killBranchWatch();
  });

  // Initiate load
  vm.init();
}]);