'use strict';

var app = angular.module('frontendApp');

/**
 * Quality modal
 */
app.directive('adminUsersRating', [function () {
  return {
    templateUrl: '/components/admin/users/directives/ratings/directives/partials/rating.html',
    scope: {
      type: '@',
      ctrl: '=',
      hide: '@'
    },
    controller: 'AdminUsersRatingsCtrl',
    controllerAs: 'tableCtrl'
  };
}]);