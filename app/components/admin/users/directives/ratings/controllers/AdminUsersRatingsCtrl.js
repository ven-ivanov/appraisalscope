'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersRatingsCtrl',
['$scope', 'AdminUsersService', function ($scope, AdminUsersService) {
  var vm = this;

  /**
   * Determine the service to use on this instance
   */
  vm.service = AdminUsersService.getComponentService();
  /**
   * Table header when displaying different data sets
   */
  vm.qualityColumnConfig = {
    quality: 'Quality',
    service: 'Service',
    turnAroundTime: 'Turn around time'
  };

  // Set tab width depending on the number of tabs
  vm.tabWidth = '3rds';

  /**
   * Table config
   */
  vm.heading = [
    // Borrower name
    {
      label: 'Borrower name',
      data: 'borrowerName'
    },
    // Street address
    {
      label: 'Street address',
      data: 'streetAddress'
    },
    // Date ordered
    {
      label: 'Date ordered',
      data: 'dateOrdered',
      filter: 'date',
      isStatic: false
    },
    // Date completed
    {
      label: 'Date completed',
      data: 'dateCompleted',
      filter: 'date',
      isStatic: false
    },
    // Date ordered
    {
      label: 'Quality',
      data: 'quality'
    }
  ];
  // Make this heading non-static
  vm.headingStatic = false;

  /**
   * Init data load
   */
  vm.init = function () {
    // Ratings summary
    vm.service.getRatings(vm.heading);
  };

  /**
   * Create table
   */
  $scope.$watchCollection(function () {
    return vm.service.displayData.ratings;
  }, function (newVal) {
    if (angular.isUndefined(newVal) || !Object.keys(newVal).length) {
      return;
    }
    vm.tableData = newVal;
    vm.rowData = vm.tableData.slice();
  });

  /**
   * Display the correct data depending on the quality type being shown
   */
  $scope.$watch(function () {
    return vm.service.displayData.selectedRatingTab;
  }, function (newVal) {
    if (angular.isUndefined(newVal)) {
      return;
    }
    // Replace data
    vm.heading[vm.heading.length - 1].data = newVal;
    vm.heading[vm.heading.length - 1].label = vm.qualityColumnConfig[newVal];
  });

  vm.init();
}]);