'use strict';

var app = angular.module('frontendApp');

/**
 * Appraiser statistics
 */
app.directive('adminUsersAppraiserStatistics', [function () {
  return {
    templateUrl: '/components/admin/users/directives/appraiser-statistics/directives/partials/statistics.html',
    controller: 'AdminUsersAppraiserStatisticsCtrl',
    controllerAs: 'tableCtrl',
    scope: {}
  };
}]);