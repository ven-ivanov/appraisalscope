'use strict';

var app = angular.module('frontendApp');

/**
 * @todo Not available in backend yet
 * @link https://github.com/ascope/manuals/issues/77
 */

app.controller('AdminUsersAppraiserScorecardCtrl', [function () {
  var vm = this;
  /**
   * Table column headers
   */
  vm.heading = [
    // Date
    {
      label: 'Date',
      data: 'date',
      filter: 'date:MM-yyyy'
    },
    // Total number of completed appraisals
    {
      label: 'Total Completed Appraisals',
      data: 'totalCompletedAppraisals'
    },
    // On time % (completed by due date)
    {
      label: 'Percentage of Appraisals Completed by Due Date',
      data: 'onTimePercent',
      filter: 'number:2'
    },
    // Acceptance to completion days
    {
      label: 'Average Days from Acceptance to Completion',
      data: 'acceptanceToCompletionDays',
      filter: 'number:2'
    },
    // Assigned to accepted days
    {
      label: 'Average Days from Assigned to Completed',
      data: 'assignedToCompletedDays',
      filter: 'number:2'
    },
    // Inspection to completion days
    {
      label: 'Average Days from Inspected to Completed',
      data: 'inspectionToCompletionDays',
      filter: 'number:2'
    },
    // Acceptance to upload days
    {
      label: 'Average Days from Acceptance to Upload',
      data: 'acceptanceToUploadDays',
      filter: 'number:2'
    },
    // Inspection to upload days
    {
      label: 'Average Days from inspection to Upload',
      data: 'inspectionToUploadDays',
      filter: 'number:2'
    },
    // Total number of reviewer revisions
    {
      label: 'Total Number of Reviewer Revisions',
      data: 'totalReviewerRevisions'
    },
    // Total number of client revisions
    {
      label: 'Total Number of Client Revisions',
      data: 'totalClientRevisions'
    },
    // Total number of reconsideration requests
    {
      label: 'Total Number of Reconsideration Requests',
      data: 'totalReconsiderationRequests'
    },
    // Reviewer revision rate
    {
      label: 'Reviewer Revision Rate',
      data: 'reviewerRevisionRate',
      filter: 'number:2'
    },
    // Client revision rate
    {
      label: 'Client Revision Rate',
      data: 'clientRevisionRate',
      filter: 'number:2'
    },
    // Revision TAT
    {
      label: 'Revision TAT',
      data: 'revisionTat'
    }
  ];
}]);