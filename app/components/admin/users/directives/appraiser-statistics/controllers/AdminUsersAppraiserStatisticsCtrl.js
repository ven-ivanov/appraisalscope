'use strict';

var app = angular.module('frontendApp');

/**
 * @todo missing the total property
 * @link https://github.com/ascope/manuals/issues/421
 */
app.controller('AdminUsersAppraiserStatisticsCtrl',
['$scope', 'AdminUsersAppraiserService', function ($scope, AdminUsersAppraiserService) {
  var vm = this;

  /**
   * Stats tabs
   */
  vm.statisticsTabConfig = {
    scorecard: 'Scorecard',
    statistics: 'Appraiser Statistics'
  };
  // Tab config (need this to an array since objects don't guarantee order)
  vm.statisticsTabs =
  ['scorecard', 'statistics'];
  // Selected stats tab
  vm.selectedStatsTab = 'scorecard';

  /**
  * Table column headers
  */
  vm.heading = [
    {
      label: '',
      data: 'label'
    },
    {
      label: 'Accepted orders',
      data: 'accepted'
    },
    {
      label: 'Declined orders',
      data: 'declined'
    },
    {
      label: 'Late orders',
      data: 'late'
    },
    {
      label: 'On time orders',
      data: 'onTime'
    },
    {
      label: 'Open orders',
      data: 'open'
    },
    {
      label: 'Unaccepted orders',
      data: 'unaccepted'
    },
    {
      label: 'Due date requests',
      data: 'dueDate'
    },
    {
      label: 'Fee increase requests',
      data: 'feeIncrease'
    }
  ];

  /**
   * Retrieve data on load
   */
  vm.init = function () {
    AdminUsersAppraiserService.getStatistics(vm.heading);
  };

  /**
  * Get statistics, filter, display
  */
  $scope.$watchCollection(function () {
    return AdminUsersAppraiserService.displayData.appraiserStats;
  }, function (newVal) {
    if (!angular.isArray(newVal)) {
      return;
    }
    // Create table data
    vm.rowData = newVal;
    vm.tableData = newVal.slice();
  });

  vm.init();
}]);