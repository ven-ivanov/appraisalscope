describe('AdminUsersAppraiserDocsService', function () {
  var scope, adminUsersService, Service, httpBackend;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function ($rootScope, AdminUsersService, AdminUsersAppraiserDocsService, $httpBackend) {
    scope = $rootScope.$new();
    adminUsersService = AdminUsersService;
    Service = AdminUsersAppraiserDocsService;
    httpBackend = $httpBackend;

    spyOn(Service, 'request').and.callThrough();
    spyOn(Service, 'hashData');
    spyOn(Service, 'formatData');

    httpBackend.whenGET(/documents/).respond({data: [1,2,3]});
    httpBackend.whenDELETE(/.*/).respond();
  }));

  describe('getAdditionalDocs', function () {
    beforeEach(function () {
      Service.getAdditionalDocs();
      httpBackend.flush();
      scope.$digest();
    });

    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should format data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('deleteDocument', function () {
    beforeEach(function () {
      Service.selectedDoc = 1;
      Service.safeData.additionalDocs = {1: {test: 'test'}, 2: {test: 'test2'}};
      Service.deleteDocument();
      httpBackend.flush();
      scope.$digest();
    });

    it('should remove document from safe data', function () {
      expect(Service.safeData.additionalDocs).toEqual({ 2: { test: 'test2' } });
    });
    it('should reformat data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('addDocument', function () {
    beforeEach(function () {
      Service.addDocument({id: 1000, test: 'test1000'});
    });

    it('should add the new document to safe data', function () {
      expect(Service.safeData.additionalDocs).toEqual({ 1000: { id: 1000, test: 'test1000' } });
    });
    it('should call formatdata', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });
});