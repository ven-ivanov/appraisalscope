'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersAppraiserDocs', ['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/directives/appraiser-docs/directives/partials/appraiser-docs.html',
    controller: 'AdminUsersAppraiserDocsCtrl',
    controllerAs: 'tableCtrl',
    scope: {
      hide: '@'
    },
    link: function (scope) {
      // Expose trigger download
      DirectiveInheritanceService.inheritDirective(scope);
    }
  };
}]);