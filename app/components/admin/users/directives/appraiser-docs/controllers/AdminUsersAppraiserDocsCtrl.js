'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersAppraiserDocsCtrl',
['$scope', 'AdminUsersAppraiserDocsService', function ($scope, AdminUsersAppraiserDocsService) {
  var vm = this;

  // Service data
  vm.displayData = AdminUsersAppraiserDocsService.displayData;
  vm.safeData = AdminUsersAppraiserDocsService.safeData;

  /**
   * Table config
   */
  vm.heading = vm.safeData.heading;

  /**
   * Data load
   */
  vm.init = function () {
    AdminUsersAppraiserDocsService.getAdditionalDocs();
  };

  /**
   * Link functions
   */
  vm.linkFn = function (id, fnName) {
    // Hold reference
    AdminUsersAppraiserDocsService.selectedDoc = id;
    // Delete
    if (fnName === 'delete') {
      $scope.$broadcast('show-modal', 'delete-doc-confirm', true);
    }
    // Download
    if (fnName === 'downloadFile') {
      $scope.triggerDownload(AdminUsersAppraiserDocsService.safeData.additionalDocs[id].url);
    }
  };
  /**
   * Delete document
   */
  vm.deleteDocument = function () {
    AdminUsersAppraiserDocsService.deleteDocument()
    .then(function () {
      $scope.$broadcast('hide-modal', 'delete-doc-confirm');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'delete-doc-failure', true);
    });
  };

  /**
   * Create table data
   */
  $scope.$watchCollection(function () {
    return vm.displayData.additionalDocs;
  }, function (newVal) {
    if (angular.isUndefined(newVal)) {
      return;
    }
    vm.tableData = newVal;
    vm.rowData = vm.tableData.slice();
  });

  /**
   * Show upload appraiser document modal
   */
  vm.showUploadModal = function () {
    $scope.$broadcast('show-modal', 'upload-appraiser-doc', true);
  };
  /**
   * Success callback for uploading a document
   * @param response
   */
  vm.uploadFileSuccess = function (response) {
    // Add to data
    AdminUsersAppraiserDocsService.addDocument(response);
  };

  vm.init();
}]);