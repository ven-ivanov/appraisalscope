'use strict';

var app = angular.module('frontendApp');

/**
 * @todo Need to change how documents are uploaded once the backend is done
 * @link https://github.com/ascope/manuals/issues/294
 */
app.factory('AdminUsersAppraiserDocsService',
['$http', 'API_PREFIX', 'AdminUsersAppraiserService', 'AdminUsersService', 'AdminUsersResourceService', 'AsDataService',
function ($http, API_PREFIX, AdminUsersAppraiserService, AdminUsersService, AdminUsersResourceService, AsDataService) {
  var Service = {
    // Safe data
    safeData: {
      additionalDocs: {},
      // Table heading
      heading: [
        {
          label: 'Date',
          data: 'uploadedAt'
        },
        {
          label: 'Doc type',
          data: 'type'
        },
        {
          label: 'file',
          fn: 'downloadFile',
          linkLabel: {data: 'file'},
          variableLabel: true
        },
        {
          label: 'Delete',
          fn: 'delete',
          linkLabel: 'Delete'
        }
      ]
    },
    // Data for display
    displayData: {
      // Additional docs table
      additionalDocs: [],
      // Appraiser ID for upload
      appraiserId: AdminUsersService.id
    },
    transformers: {
      uploadedAt: {
        filter: 'date'
      }
    },
    /**
     * Retrieve additional docs
     */
    getAdditionalDocs: function () {
      return Service.request('getDocuments')
      .then(function (response) {
        // Hash
        Service.hashData(response.data, 'additionalDocs');
        // Format for display
        Service.formatData();
      });
    },
    /**
     * Delete document
     */
    deleteDocument: function () {
      return Service.request('deleteDocument', {id: Service.selectedDoc})
      .then(function () {
        delete Service.safeData.additionalDocs[Service.selectedDoc];
        // Recreate display
        Service.formatData();
      });
    },
    /**
     * Add document
     * @param document
     */
    addDocument: function (document) {
      // Add to safe data and display
      Service.safeData.additionalDocs[document.id] = document;
      Service.formatData();
    }
  };

  /**
   * Inherit from AdminUsersService
   */
  Service.request = AdminUsersService.request.bind(Service, 'appraiserDocs');
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'additionalDocs');
  Service.transformData = AsDataService.transformData.bind(Service);
  return Service;
}]);