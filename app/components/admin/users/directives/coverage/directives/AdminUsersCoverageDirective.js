'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersCoverage', ['TriggerDownloadService', function (TriggerDownloadService) {
  return {
    templateUrl: '/components/admin/users/directives/coverage/directives/partials/coverage.html',
    controller: 'AdminUsersCoverageCtrl',
    controllerAs: 'coverageCtrl',
    scope: {
      hide: '@',
      show: '@',
      state: '@',
      amc: '@'
    },
    link: function (scope) {
      // Expose trigger download
      TriggerDownloadService.init.call(scope);
    }
  };
}]);