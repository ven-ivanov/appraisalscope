'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersCoverageCtrl',
['$scope', 'AdminUsersCoverageService',  function ($scope, AdminUsersCoverageService) {
  var vm = this;
  // Keep reference to display data
  vm.displayData = AdminUsersCoverageService.displayData;
  // Safe data
  vm.safeData = AdminUsersCoverageService.safeData;
  // Show add state by default
  vm.addStateButton = angular.isDefined($scope.state) && $scope.state === 'false' ? false : true;
  // Construct the heading based on the non-hidden items
  vm.heading = AdminUsersCoverageService.getDirectiveTableColumns($scope.hide, $scope.show);

  // Keep reference on service
  AdminUsersCoverageService.safeData.heading = vm.heading;

  /**
   * Get data on load
   */
  vm.init = function () {
    AdminUsersCoverageService.getCoverage();
    // Attach state watcher
    AdminUsersCoverageService.attachStateWatcher();
  };

  /**
   * Get counties when a state is selected
   *
   * @todo Waiting on backend for counties to stabilize
   * @link https://github.com/ascope/manuals/issues/111
   */
  AdminUsersCoverageService.stateWatcher = function () {
    AdminUsersCoverageService.killStateWatch = $scope.$watch(function () {
      return vm.displayData.selectedCoverage.coverage.state;
    }, function (newVal, oldVal) {
      if (angular.isUndefined(newVal) || angular.equals(newVal, oldVal)) {
        return;
      }
      AdminUsersCoverageService.getSelectedCounties(newVal);
    });
  };

  /**
   * Delete coverage
   */
  vm.deleteCoverage = function () {
    // Make request and remove from safe data
    AdminUsersCoverageService.deleteCoverage()
    .then(function () {
      $scope.$broadcast('hide-modal', 'delete-coverage-confirm');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'delete-coverage-failure', true);
    });
  };

  /**
   * Add coverage
   */
  vm.addCoverage = function () {
    // Remove reference to any previously selected coverage
    AdminUsersCoverageService.selectedCoverage = null;
    vm.displayData.coverageTitle = 'Add coverage';
    // Remove selected record
    vm.displayData.selectedCoverage = {
      coverage: {},
      counties: []
    };
    $scope.$broadcast('show-modal', 'edit-coverage', true);
  };

  /**
   * Update license doc
   */
  vm.updateDateLicense = function () {
    $scope.$broadcast('show-modal', 'upload-license-doc', true);
  };

  /**
   * Upload license doc success
   *
   * @todo Waiting on the backend for this
   * @link https://github.com/ascope/manuals/issues/440
   */
  vm.uploadLicenseDocSuccess = function (response) {
    AdminUsersCoverageService.updateLicenseDoc(response);
    // Hide modal
    $scope.$broadcast('hide-modal', 'upload-license-doc');
  };

  /**
   * Edit coverage/add new coverage
   */
  vm.editCoverage = function () {
    AdminUsersCoverageService.editCoverage()
    .then(function () {
      $scope.$broadcast('hide-modal', 'edit-coverage');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'edit-coverage-failure', true);
    });
  };

  /**
   * Get zips for selected counties
   */
  vm.getZips = function () {
    AdminUsersCoverageService.getZips();
    $scope.$broadcast('show-modal', 'county-zips', true);
  };
  /**
   * Update selected zips
   * @todo Waiting on backend
   */
  vm.updateSelectedZips = function (id) {
    AdminUsersCoverageService.updateSelectedZips(id);
  };

  /**
   * Disable form if incomplete
   * @returns {boolean}
   */
  vm.disableSubmit = function () {
    return !vm.displayData.selectedCoverage.coverage.state || !vm.coverageForm.$valid;
  };

  /**
   * Stop watching states on controller destroy
   */
  $scope.$on('$destroy', function () {
    AdminUsersCoverageService.killStateWatch();
  });

  // Init data
  vm.init();
}]);