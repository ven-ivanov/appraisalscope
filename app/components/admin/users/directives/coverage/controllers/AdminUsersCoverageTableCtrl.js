'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersCoverageTableCtrl',
['$scope', 'AdminUsersCoverageService', function ($scope, AdminUsersCoverageService) {
  var vm = this;

  // Get table definition from service
  vm.heading = AdminUsersCoverageService.safeData.heading;

  /**
   * Create table when data is available
   */
  vm.killTableWatch = $scope.$watchCollection(function () {
    return AdminUsersCoverageService.displayData.coverage;
  }, function (newVal) {
    if (!angular.isArray(newVal) || !newVal.length) {
      return;
    }
    vm.tableData = newVal;
    vm.rowData = vm.tableData.slice();
  });

  /**
   * Edit a coverage record
   */
  vm.edit = function () {
    // Stop watching state while record loads
    AdminUsersCoverageService.killStateWatch();
    AdminUsersCoverageService.displayData.coverageTitle = 'Edit coverage';
    // Get record
    AdminUsersCoverageService.getCoverageRecord()
    .then(function () {
      $scope.$emit('show-modal', 'edit-coverage', true);
      // Begin watching state again
      AdminUsersCoverageService.attachStateWatcher();
    });
  };
  /**
   * Trigger edit as row function
   */
  vm.rowFn = function (id) {
    // Keep reference to selected coverage
    AdminUsersCoverageService.selectedCoverage = id;
    vm.edit();
  };

  /**
   * Link functions
   */
  vm.linkFn = function (id, fnName) {
    // Keep reference to selected coverage
    AdminUsersCoverageService.selectedCoverage = id;
    // Download license doc
    if (fnName === 'downloadLicenseDoc') {
      $scope.triggerDownload(AdminUsersCoverageService.safeData.coverage[id].document.url);
      // Edit
    } else if (fnName === 'edit') {
      vm.edit();
      // Delete
    } else if (fnName === 'delete') {
      $scope.$emit('show-modal', 'delete-coverage-confirm', true);
      // Show counties
    } else if (fnName === 'counties') {
      AdminUsersCoverageService.getCoverageRecord();
      $scope.$emit('show-modal', 'show-coverage-counties', true);
    }
  };

  /**
   * Stop watching table when controller is destroyed
   */
  $scope.$on('$destroy', function () {
    vm.killTableWatch();
  });
}]);