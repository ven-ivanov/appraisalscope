describe('AdminUsersCoverageService', function () {
  var scope, Service, httpBackend, adminUsersService;

  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function ($rootScope, AdminUsersCoverageService, $httpBackend, AdminUsersService) {
    scope = $rootScope.$new();
    Service = AdminUsersCoverageService;
    httpBackend = $httpBackend;
    adminUsersService = AdminUsersService;

    // Http
    httpBackend.whenGET(/certificates\/state/).respond({data: [{id: 1, name: 'blah'}, {id: 2, name: 'other'}]});
    httpBackend.whenGET(/certificates/).respond({data: [{id: 1, state: 'OH', primaryState: 'OH', license: 'Test License'}, {id: 2, state: 'AK', primaryState: 'OH', license: 'Test License 2'}]});
    httpBackend.whenGET(/counties/).respond({data: ['harris']});
    httpBackend.whenPUT(/.*/).respond({id: 1000, license: 'New license!'});
    httpBackend.whenPOST(/.*/).respond({id: 1, license: 'New license!'});
    httpBackend.whenDELETE(/.*/).respond();
    httpBackend.whenGET(/zips/).respond({data: [{id: 1, zip: 10000}, {id: 2, zip: 10001}, {id: 3, zip: 10002}, {id: 4, zip: 10003}]});
    httpBackend.whenPATCH(/zips/).respond();

    // Spies
    spyOn(Service, 'transformData');
    spyOn(Service, 'formatData');
    spyOn(Service, 'hashData');
    spyOn(Service, 'request').and.callThrough();

    // Data
    Service.safeData.coverage = {
      1: {
        blah: 'bleh',
        coverage: {counties: [{name: 'County1'}, {name: 'County2'}]}
      },
      2: {
        blah: 'bloop',
        coverage: {counties: [{name: 'County3'}, {name: 'County4'}]}
      }
    };
    Service.selectedCoverage = 1;
  }));

  describe('getCoverage', function () {
    describe('no certificate available', function () {
      beforeEach(function () {
        Service.getCoverage();
        httpBackend.flush();
        scope.$digest();
      });

      it('should hash states', function () {
        expect(Service.hashData.calls.argsFor(0)).toEqual([{
          AK: 'Arkansas'
        }, 'states', 'code']);
      });
      it('should hash coverage', function () {
        expect(Service.hashData.calls.argsFor(1)).toEqual([[{
          id: 1,
          state: 'OH',
          primaryState: 'OH',
          license: 'Test License'
        }, {
          id: 2,
          state: 'AK',
          primaryState: 'OH',
          license: 'Test License 2'
        }], 'coverage']);
      });
      it('should set states', function () {
        expect(Service.displayData.states).toEqual({ AK: 'Arkansas' });
      });
      it('should call formatData', function () {
        expect(Service.formatData).toHaveBeenCalled();
      });
    });

    describe('certificates already available', function () {
      beforeEach(function () {
        adminUsersService.record = {certificates: {1: [], 2: []}};
        Service.getCoverage();
        scope.$digest();
      });

      // Note: This will only pass if no request was made, since no request flush is called
      it('should not make a request', function () {
        expect(true).toEqual(true);
      });
    });
  });

  describe('deleteCoverage', function () {
    describe('error', function () {
      beforeEach(function () {
        Service.selectedCoverage = null;
      });

      it('should throw an error if no coverage is selected', function () {
        try {
          Service.deleteCoverage();
        } catch (e) {
          expect(e.message).toEqual('Selected coverage is not defined');
        }
      });
    });

    describe('no error', function () {
      beforeEach(function () {
        Service.deleteCoverage();
        httpBackend.flush();
        scope.$digest();
      });

      it('should update safe data', function () {
        expect(Service.safeData.coverage).toEqual({
          2: {
            blah: 'bloop',
            coverage: {counties: [{name: 'County3'}, {name: 'County4'}]}
          }
        });
      });
      it('should call formatData', function () {
        expect(Service.formatData).toHaveBeenCalled();
      });
      it('should set selectedCoverage to null', function () {
        expect(Service.selectedCoverage).toEqual(null);
      });
    });
  });

  describe('getCoverageRecord', function () {
    beforeEach(function () {
      spyOn(Service, 'getSelectedCounties');
      Service.getCoverageRecord();
    });

    it('should transform data', function () {
      expect(Service.transformData.calls.argsFor(0)).toEqual([{
        blah: 'bleh',
        coverage: {counties: [{name: 'County1'}, {name: 'County2'}]},
        isFhaApproved: true,
        isCommercial: true,
        counties: [{id: 'County1'}, {id: 'County2'}]
      }]);
    });
    it('should set selected coverage', function () {
      expect(Service.displayData.selectedCoverage).toEqual({
        blah: 'bleh',
        coverage: {counties: [{name: 'County1'}, {name: 'County2'}]},
        isFhaApproved: true,
        isCommercial: true,
        counties: [{id: 'County1'}, {id: 'County2'}]
      });
    });
    it('should get selected counties', function () {
      expect(Service.getSelectedCounties).toHaveBeenCalled();
    });
  });

  describe('getSelectedCounties', function () {
    describe('state selected', function () {
      beforeEach(function () {
        Service.displayData.selectedCoverage.state = 'Texas';
        Service.getSelectedCounties();
        httpBackend.flush();
        scope.$digest();
      });

      it('should make the right request', function () {
        expect(Service.request.calls.argsFor(0)).toEqual(['getCounties', {state: 'Texas'}]);
      });
      it('should set counties from response', function () {
        expect(Service.displayData.counties).toEqual([ 'harris' ]);
      });
    });

    describe('state passed in', function () {
      beforeEach(function () {
        Service.getSelectedCounties('Texas');
        httpBackend.flush();
        scope.$digest();
      });

      it('should make the right request', function () {
        expect(Service.request.calls.argsFor(0)).toEqual(['getCounties', {state: 'Texas'}]);
      });
    });
  });

  describe('updateLicenseDoc', function () {
    beforeEach(function () {
      Service.updateLicenseDoc({licenseDoc: 'license'});
    });

    it('should update safe data', function () {
      expect(Service.safeData.coverage).toEqual({
        1: {
          blah: 'bleh',
          coverage: {counties: [{name: 'County1'}, {name: 'County2'}]},
          licenseDoc: 'license'
        },
        2: {
          blah: 'bloop',
          coverage: {counties: [{name: 'County3'}, {name: 'County4'}]}
        }
      });
    });
    it('should call formatData', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('editCoverage', function () {
    beforeEach(function () {
      Service.displayData.selectedCoverage = {coverage: {}};
      Service.displayData.counties = [{name: 'harris'}, {name: 'montgomery'}];
    });

    describe('edit', function () {
      beforeEach(function () {
        Service.safeData.selectedZips = [1,2];
        Service.editCoverage();
        httpBackend.flush();
        scope.$digest();
      });

      it('should call transformData', function () {
        expect(Service.transformData).toHaveBeenCalled();
      });
      it('should make the right request', function () {
        expect(Service.request.calls.argsFor(0)).toEqual(['updateCertificate', {
          url: {certificateId: 1},
          body: {
            coverage: {
              counties: ['harris', 'montgomery'],
              zips: [1, 2]
            }
          }
        }]);
      });
      it('should update safe data', function () {
        expect(Service.safeData.coverage[1000].id).toEqual(1000);
      });
      it('should format data', function () {
        expect(Service.formatData).toHaveBeenCalled();
      });
    });

    describe('new', function () {
      beforeEach(function () {
        Service.selectedCoverage = null;
        Service.editCoverage();
        httpBackend.flush();
        scope.$digest();
      });

      it('should make the request with the right parameters', function () {
        expect(Service.request).toHaveBeenCalledWith('newCertificate', {
          url: {certificateId: null},
          body: {
            coverage: {
              counties: ['harris', 'montgomery'],
              zips: []
            }
          }
        });
      });
    });
  });

  describe('getZips', function () {
    beforeEach(function () {
      Service.displayData.selectedCoverage = {counties: [{id: 'harris'}, {id: 'hamilton'}]};
      Service.getZips();
      httpBackend.flush();
      scope.$digest();
    });

    it('should make the right request', function () {
      expect(Service.request.calls.argsFor(0)).toEqual([ 'getZips', [ 'harris', 'hamilton' ] ]);
    });
    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should set zips', function () {
      expect(Service.displayData.zips).toEqual({ 1: [ { id: 1, zip: 10000 } ], 2: [ { id: 2, zip: 10001 } ], 3: [ { id: 3, zip: 10002 } ], 4: [ { id: 4, zip: 10003 } ] });
    });
  });

  describe('updateSelectedZips', function () {
    describe('individual', function () {
      beforeEach(function () {
        spyOn(Service, 'addRemoveIndividualZip');
        Service.updateSelectedZips(1);
      });

      it('should call addRemoveIndividualZip', function () {
        expect(Service.addRemoveIndividualZip).toHaveBeenCalled();
      });
    });

    describe('all', function () {
      describe('remove', function () {
        beforeEach(function () {
          Service.safeData.selectedZips = [1,2];
          Service.updateSelectedZips('remove');
        });

        it('should remove all zips', function () {
          expect(Service.safeData.selectedZips).toEqual([]);
        });
      });

      describe('add', function () {
        beforeEach(function () {
          Service.safeData.zips = [{id: 3}, {id: 4}];
          Service.safeData.selectedZips = [1,2];
          Service.updateSelectedZips('add');
        });

        it('should set all available zips', function () {
          expect(Service.safeData.selectedZips).toEqual([3,4]);
        });
      });
    });
  });

  describe('addRemoveIndividualZip', function () {
    describe('add', function () {
      beforeEach(function () {
        Service.safeData.selectedZips = [1,2];
        Service.addRemoveIndividualZip(3);
      });

      it('should add the zip', function () {
        expect(Service.safeData.selectedZips).toEqual([1,2,3]);
      });
    });

    describe('remove', function () {
      beforeEach(function () {
        Service.safeData.selectedZips = [1,2];
        Service.addRemoveIndividualZip(1);
      });

      it('should remove the selected zip', function () {
        expect(Service.safeData.selectedZips).toEqual([2]);
      });
    });
  });
});