'use strict';

var app = angular.module('frontendApp');

app.factory('AdminUsersCoverageService',
['$http', 'API_PREFIX', 'AdminUsersService', 'AdminUsersResourceService', 'AsDataService',
function ($http, API_PREFIX, AdminUsersService, AdminUsersResourceService, AsDataService) {
  var Service = {
    // Unmodified data
    safeData: {
      // Coverage
      coverage: {},
      // States
      states: {},
      // table header
      heading: [],
      // Zips
      zips: {},
      // Selected zips
      selectedZips: [],
      // Table columns
      columns: [
        {
          label: 'State',
          data: 'state'
        },
        {
          id: 'counties',
          label: 'Show selected counties',
          fn: 'counties',
          linkLabel: 'Show counties'
        },
        {
          label: 'License Number',
          data: 'number'
        },
        {
          label: 'License Type',
          data: 'type'
        },
        {
          id: 'fha',
          label: 'FHA',
          data: 'isFhaApproved'
        },
        {
          id: 'commercial',
          label: 'Commercial',
          data: 'isCommercial'
        },
        {
          label: 'Expiration Date',
          data: 'expireAt',
          filter: 'date'
        },
        {
          label: 'License Doc',
          fn: 'downloadLicenseDoc',
          data: 'licenseDoc',
          // Allow the link label to be shown is one is uploaded
          isStatic: false,
          linkLabel: {false: '', true: 'License doc'}
        },
        {
          id: 'edit',
          label: 'Edit',
          fn: 'edit',
          linkLabel: 'Edit'
        },
        {
          id: 'delete',
          label: 'Delete',
          fn: 'delete',
          data: 'primary',
          isStatic: false,
          linkLabel: {false: 'Delete', true: ''}
        }
      ]
    },
    // Data for display
    displayData: {
      // User ID
      userId: AdminUsersService.id,
      // Current section
      section: AdminUsersService.section,
      // Coverage
      coverage: [],
      // Selected coverage
      selectedCoverage: {
        coverage: {},
        // Counties model
        counties: []
      },
      // States
      states: [],
      // Counties
      counties: [],
      // License types
      licenseType: [
        {
          id: 'licensed-appraiser',
          value: 'Licensed Appraiser'
        },
        {
          id: 'general-appraiser',
          value: 'General Appraiser'
        },
        {
          id: 'certified-appraiser',
          value: 'Certified Appraiser'
        },
        {
          id: 'trainee-license',
          value: 'Trainee License'
        },
        {
          id: 'amc',
          value: 'AMC'
        }
      ],
      // FHA
      isFhaApproved: AdminUsersService.yesNoDropdown,
      // Commercial
      isCommercial: AdminUsersService.yesNoDropdown,
      // Dropdown multiselect settings
      settings: {
        counties: {displayProp: 'displayName', idProp: 'name'}
      },
      // Zips
      zips: {},
      // Number of columns of zips to create
      zipColumns: 4
    },
    // Data transformation config
    transformers: {
      expireAt: {
        filter: 'date:MM/dd/yyyy'
      },
      // Set state to top level property
      state: {
        source: 'coverage.state.name'
      },
      // License doc download URL
      licenseDoc: {
        source: 'document.url'
      },
      isCommercial: {
        filter: 'boolYesNo'
      },
      isFhaApproved: {
        filter: 'boolYesNo'
      }
    },
    // Watcher for retrieving counties on state change
    stateWatcher: angular.noop,
    killStateWatch: angular.noop,
    attachStateWatcher: function () {
      Service.stateWatcher();
    },
    /**
     * Request (either company, user, or branch)
     */
    request: function (requestFn, type, params) {
      if (arguments.length === 2 && angular.isObject(type)) {
        params = type;
        type = null;
      }
      type = type || AdminUsersService.record._type;
      // Get $resource configuration
      return AdminUsersResourceService.coverage[requestFn](type, params);
    },
    /**
     * Get coverage on load
     */
    getCoverage: function () {
      return Service.request('getCertifications')
      .then(function (response) {
        // Hash states
        Service.hashData(AdminUsersService.states, 'states', 'code');
        // Create display data for states
        Service.displayData.states = AdminUsersService.states;
        // Hash data
        Service.hashData(response.data, 'coverage');
        // Format for display
        Service.formatData();
      });
    },
    /**
     * Delete coverage
     */
    deleteCoverage: function () {
      if (angular.isUndefined(Service.selectedCoverage)) {
        throw Error('Selected coverage is not defined');
      }
      return Service.request('deleteCoverage', {certificateId: Service.selectedCoverage})
      .then(function () {
        // Remove from safe data
        delete Service.safeData.coverage[Service.selectedCoverage];
        // Recreate display
        Service.formatData();
        // Remove selected coverage
        Service.selectedCoverage = null;
      });
    },
    /**
     * Get individual coverage record
     */
    getCoverageRecord: function () {
      // Get coverage record
      var coverageRecord = angular.copy(Service.safeData.coverage[Service.selectedCoverage]);
      // Transform data
      Service.transformData(coverageRecord);
      // Convert to boolean
      coverageRecord.isFhaApproved = coverageRecord.isFhaApproved !== 'No';
      coverageRecord.isCommercial = coverageRecord.isCommercial !== 'No';
      // Get selected counties into dropdown multiselect format
      coverageRecord.counties = Lazy(coverageRecord.coverage.counties)
      .map(function (county) {
        return {id: county.name};
      })
      .toArray();
      // Apply it for display
      Service.displayData.selectedCoverage = coverageRecord;
      // Get selected counties for this record
      return Service.getSelectedCounties();
    },
    /**
     * Get selected counties for a coverage record
     *
     * @todo This doesn't exist yet, I can't really finalize until it's done
     * @link https://github.com/ascope/manuals/issues/97
     */
    getSelectedCounties: function (state) {
      state = state || Service.displayData.selectedCoverage.state;
      return Service.request('getCounties', {state: state})
      .then(function (response) {
        // Set counties for dropdown multi-select
        Service.displayData.counties = response.data;
      });
    },
    /**
     * Update license doc
     */
    updateLicenseDoc: function (data) {
      // Update safe data
      Service.safeData.coverage[Service.selectedCoverage].licenseDoc = data.licenseDoc;
      // Update display
      Service.formatData();
    },
    /**
     * Edit coverage
     *
     * @todo Backend does not currently include zip codes
     * @link https://github.com/ascope/manuals/issues/441
     */
    editCoverage: function () {
      var data = Service.displayData.selectedCoverage;
      // New or update
      var requestFn = Service.selectedCoverage ? 'updateCertificate' : 'newCertificate';
      // Get counties
      data.coverage.counties = Lazy(Service.displayData.counties)
      .pluck('name')
      .toArray();
      // Get zips
      data.coverage.zips = Service.safeData.selectedZips;
      // Undo transformation
      Service.transformData(data, true);
      // Make request
      return Service.request(requestFn, {url: {certificateId: Service.selectedCoverage}, body: data})
      .then(function (response) {
        // Add to safe data
        Service.safeData.coverage[response.id] = response;
        // Add to display
        Service.formatData();
      });
    },
    /**
     * Retrieve zips for all selected counties
     *
     * @todo This doesn't exist yet, I can't really finalize until it's done
     * @link https://github.com/ascope/manuals/issues/97
     */
    getZips: function () {
      // Get directly selected counties from multiselect
      var counties = Service.displayData.selectedCoverage.counties, chunkedCounties, i;
      Service.displayData.zips = {};
      // Get county unchangeable names
      counties = Lazy(counties)
      .pluck('id')
      .toArray();
      // Retrieve zips for the selected counties
      Service.request('getZips', counties)
      .then(function (response) {
        Service.hashData(response.data, 'zips');
        // Chunk zips into as many columns as necessary
        chunkedCounties = _.chunk(response.data, Math.ceil(response.data.length / Service.displayData.zipColumns));
        // Create display data from chunks zips
        for (i = 0; i < Service.displayData.zipColumns; i = i + 1) {
          Service.displayData.zips[i + 1] = chunkedCounties[i];
        }
      });
    },
    /**
     * Update selected zips
     * @param id Zip ID
     *
     * @todo This doesn't exist yet, I can't really finalize until it's done
     * @link https://github.com/ascope/manuals/issues/97
     */
    updateSelectedZips: function (id) {
      // Individual zip
      if (angular.isNumber(id)) {
        Service.addRemoveIndividualZip(id);
        // Add or remove all zips
      } else if (angular.isString(id)) {
        // Clear selected zips
        Service.safeData.selectedZips = [];
        if (id === 'add') {
          Service.safeData.selectedZips = Lazy(Service.safeData.zips)
          .map(function (zip) {
            return zip.id;
          })
          .toArray();
        }
      }
    },
    /**
     * Add or remove an individual zip
     * @param id
     */
    addRemoveIndividualZip: function (id) {
      // Remove
      if (Service.safeData.selectedZips.indexOf(id) !== -1) {
        Service.safeData.selectedZips.splice(Service.safeData.selectedZips.indexOf(id), 1);
      } else {
        // Add
        Service.safeData.selectedZips.push(id);
      }
    }
  };
  /**
   * Inherit from AdminUsersService
   */
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'coverage');
  Service.transformData = AsDataService.transformData.bind(Service);
  // Create table columns
  Service.getDirectiveTableColumns = AdminUsersService.getDirectiveTableColumns.bind(Service);
  return Service;
}]);