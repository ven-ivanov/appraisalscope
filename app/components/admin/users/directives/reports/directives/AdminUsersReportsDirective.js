'use strict';

var app = angular.module('frontendApp');

/**
 * Reports for users
 */
app.directive('adminUsersReports',
['AdminUsersDirectiveInheritanceService', function (AdminUsersDirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/directives/reports/directives/partials/reports.html',
    scope: {
      ctrl: '=',
      type: '@'
    },
    link: function (scope) {
      AdminUsersDirectiveInheritanceService.inherit.call(scope);
    }
  };
}]);