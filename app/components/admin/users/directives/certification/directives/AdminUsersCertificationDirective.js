'use strict';

var app = angular.module('frontendApp');

/**
 * Certification details
 */
app.directive('adminUsersCertification', ['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/directives/certification/directives/partials/certification.html',
    controller: 'AdminUsersCertificationCtrl',
    controllerAs: 'tableCtrl',
    bindToController: {
      hide: '@',
      type: '@',
      hidePrimary: '@'
    },
    scope: {},
    link: function (scope) {
      // Expose trigger download
      DirectiveInheritanceService.inheritDirective(scope);
    }
  };
}]);