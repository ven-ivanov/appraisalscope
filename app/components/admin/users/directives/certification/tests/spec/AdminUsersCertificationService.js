describe('AdminUsersCertificationService', function () {
  var scope, httpBackend, Service, adminUsersService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function ($rootScope, $httpBackend, AdminUsersCertificationService, AdminUsersService) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    Service = AdminUsersCertificationService;
    adminUsersService = AdminUsersService;

    httpBackend.whenGET(/.*/).respond({data: [1,2,3]});
    httpBackend.whenPOST(/.*/).respond();

    adminUsersService.record._type = 'appraiser-directory';

    spyOn(Service, 'getRequestObject').and.callThrough();
    spyOn(Service, 'hashData');
    spyOn(Service, 'formatData');
  }));

  describe('getRequestObject', function () {
    var response;
    it('should return the normal record type', function () {
      adminUsersService.record._type = 'company';
      response = Service.getRequestObject();
      expect(response).toEqual('company');
    });
    it('should return appraiser for appraiser-directory', function () {
      response = Service.getRequestObject();
      expect(response).toEqual('appraiser');
    });
  });

  describe('getCertificationDetails', function () {
    beforeEach(function () {
      spyOn(Service, 'getPrimaryLicenseInfo');
      Service.getCertificationDetails();
      httpBackend.flush();
      scope.$digest();
    });
    it('should determine the request object', function () {
      expect(Service.getRequestObject).toHaveBeenCalled();
    });
    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should format data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
    it('should retrieve primary license info', function () {
      expect(Service.getPrimaryLicenseInfo).toHaveBeenCalled();
    });
  });

  describe('getPrimaryLicenseInfo', function () {
    beforeEach(function () {
      adminUsersService.record.certification = {licensedAt: '06/01/2010'};
      Service.getPrimaryLicenseInfo();
    });

    it('should set the primary certification', function () {
      expect(Service.displayData.primaryCert).toEqual({ licensedAt: '06/01/2010' });
    });
  });

  describe('emailLicense', function () {
    beforeEach(function () {
      Service.safeData.selectedDoc = 1;
      Service.displayData.emailLicenseEmail = 'test@test.com';
      spyOn(Service, 'request').and.callThrough();
      Service.emailLicense();
      httpBackend.flush();
      scope.$digest();
    });

    it('should retrieve request object', function () {
      expect(Service.getRequestObject).toHaveBeenCalled();
    });
    it('should make the request with the right params', function () {
      expect(Service.request).toHaveBeenCalledWith('appraiser', 'emailLicenseDoc', { params: { certificateId: 1 }, body: { email: 'test@test.com' } });
    });
  });

  describe('includesLicenseDoc', function () {
    var records;
    beforeEach(function () {
      records = [{id: 1, document: {url: 'www.com'}}, {id: 2, document: {url: ''}}, {id: 3}];
      Service.includesLicenseDoc(records);
    });

    it('should set the first record as including a license doc', function () {
      expect(records[0]).toEqual({id: 1, document: {url: 'www.com'}, includesDoc: true});
    });
    it('should set the second to not include license doc', function () {
      expect(records[1]).toEqual({id: 2, document: {url: ''}, includesDoc: false});
    });
    it('should set the third to not include the license doc', function () {
      expect(records[2]).toEqual({id:3, includesDoc: false});
    });
  });
});