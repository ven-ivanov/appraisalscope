'use strict';

var app = angular.module('frontendApp');

/**
 * Appraiser certifications
 */
app.factory('AdminUsersCertificationService',
['AdminUsersService', '$filter', 'AsDataService', function (AdminUsersService, $filter, AsDataService) {
  var Service = {
    safeData: {
      // Certification data
      certificates: {},
      // Columns for building the table
      columns: [
        {
          label: 'State',
          data: 'state'
        },
        {
          label: 'License #',
          data: 'number'
        },
        {
          label: 'License type',
          data: 'type'
        },
        {
          id: 'primary',
          label: 'Primary license',
          data: 'primary'
        },
        {
          id: 'fha',
          label: 'FHA',
          data: 'isFhaApproved'
        },
        {
          id: 'commercial',
          label: 'isCommercial',
          data: 'isCommercial'
        },
        {
          label: 'Expiration date',
          data: 'expireAt'
        },
        {
          id: 'licenseDoc',
          label: 'License doc',
          linkLabel: 'License doc',
          fn: 'licenseDoc',
          data: 'includesDoc',
          optionalFn: true
        },
        {
          id: 'email',
          label: 'Email',
          linkLabel: 'Email',
          fn: 'email',
          optionalFn: true,
          data: 'includesDoc'
        }
      ],
      // primary certification
      primaryCert: {}
    },
    displayData: {
      // Certification
      certificates: [],
      // Primary certification
      primaryCert: {}
    },
    // Data transformation
    transformers: {
      state: {
        source: 'coverage.state.name',
        dest: 'state'
      },
      type: {
        filter: 'replaceChar:-: '
      },
      isFhaApproved: {
        filter: 'boolYesNo'
      },
      isCommercial: {
        filter: 'boolYesNo'
      },
      expireAt: {
        filter: 'date'
      },
      primary: {
        filter: 'boolYesNo'
      }
    },
    /**
     * Determine the request object in AdminUsersResourceService to use
     */
    getRequestObject: function () {
      switch (AdminUsersService.record._type) {
        case 'appraiser-directory':
          return 'appraiser';
      }
      return AdminUsersService.record._type;
    },
    /**
     * Get appraiser certification details
     */
    getCertificationDetails: function () {
      try {
        return Service.request(Service.getRequestObject(), 'certificates')
        .then(function (response) {
          // Note whether each record includes a license doc
          Service.includesLicenseDoc(response.data);
          // Hash and set for display
          Service.hashData(response.data, 'certificates');
          Service.formatData('certificates');
          Service.getPrimaryLicenseInfo();
        });
        // For those views which do not have certifications for some, but do have certifications for others
        // such as appraisers and company managers
      } catch (e) {

      }
    },
    /**
     * Whether the certificate includes a license document
     */
    includesLicenseDoc: function (records) {
      angular.forEach(records, function (record) {
        record.includesDoc = angular.isObject(record.document) && !!record.document.url;
      });
    },
    /**
     * Format primary license info for display
     */
    getPrimaryLicenseInfo: function () {
      // Primary license
      Service.displayData.primaryCert = AdminUsersService.record.certification;
      if (AdminUsersService.record && AdminUsersService.record.certification &&
          typeof AdminUsersService.record.certification === 'object' &&
          Object.getPrototypeOf(AdminUsersService.record.certification) === Object.prototype &&
          AdminUsersService.record.certification.licensedAt) {
        // Filter licensed at
        Service.displayData.primaryCert.licensedAt = $filter('date')(Service.displayData.primaryCert.licensedAt);
      }
    },
    /**
     * Email license
     * @todo When backend ready
     */
    emailLicense: function () {
      return Service.request(Service.getRequestObject(), 'emailLicenseDoc', {
        params: {certificateId: Service.safeData.selectedDoc},
        body: {email: Service.displayData.emailLicenseEmail}
      });
    }
  };
  // Create table columns
  Service.getDirectiveTableColumns = AdminUsersService.getDirectiveTableColumns.bind(Service);
  // Requests
  Service.request = AdminUsersService.request.bind(Service);
  // Hash and display
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'certificates');
  Service.transformData = AsDataService.transformData.bind(Service);
  return Service;
}]);