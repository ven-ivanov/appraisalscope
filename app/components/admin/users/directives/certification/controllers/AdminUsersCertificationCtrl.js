'use strict';

var app = angular.module('frontendApp');

/**
 * Certification details table
 */
app.controller('AdminUsersCertificationCtrl',
['$scope', 'AdminUsersService', 'AdminUsersCertificationService',
function ($scope, AdminUsersService, AdminUsersCertificationService) {
  var vm = this;
  var Service = AdminUsersCertificationService;
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;
  // Construct the heading based on the non-hidden items
  vm.heading = Service.getDirectiveTableColumns(vm.hide);
  // Table definition
  vm.safeData.heading = vm.heading;
  // Whether to hide primary license info
  vm.hidePrimary = $scope.$eval(vm.hidePrimary);

  /**
   * Data load
   */
  vm.init = function () {
    // Check that a record is selected
    if (AdminUsersService.record) {
      Service.getCertificationDetails(vm.heading);
    }
  };

  /**
   * Email and license doc functions
   */
  vm.linkFn = function (id, fnName) {
    // Store the selected realtor document
    Service.safeData.selectedDoc = id;
    // Trigger download of license doc
    if (fnName === 'licenseDoc') {
      $scope.triggerDownload(Service.safeData.certificates[id].document.url);
      // Email license doc
    } else if (fnName === 'email') {
      $scope.$broadcast('show-modal', 'email-license-doc', true);
    }
  };

  /**
   * Email an appraiser's license document
   */
  vm.emailLicenseDoc = function () {
    AdminUsersCertificationService.emailLicense()
    .then(function () {
      // Success
      $scope.$broadcast('hide-modal', 'email-license-doc');
      $scope.$broadcast('show-modal', 'email-license-doc-success', true);
    })
    .catch(function () {
      $scope.$broadcast('hide-modal', 'email-license-doc-failure');
    });
  };

  /**
   * Watch table data
   */
  $scope.$watchCollection(function () {
    return Service.displayData.certificates;
  }, function (newVal) {
    if (!angular.isArray(newVal) || !newVal.length) {
      return;
    }
    vm.tableData = newVal;
    vm.rowData = vm.tableData.slice();
  });

  vm.init();
}]);