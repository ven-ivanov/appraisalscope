'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersHistoryCtrl',
['$scope', 'AdminUsersHistoryService', function ($scope, AdminUsersHistoryService) {
  var vm = this;

  // Display data
  vm.displayData = AdminUsersHistoryService.displayData;
  // Safe data
  vm.safeData = AdminUsersHistoryService.safeData;
  // Construct the heading based on the non-hidden items
  vm.heading = vm.safeData.heading = AdminUsersHistoryService.getDirectiveTableColumns(vm.hide);

  /**
   * Init data load
   */
  vm.init = function () {
    AdminUsersHistoryService.getHistory()
    .catch(function () {
      $scope.$broadcast('show-modal', 'load-history-fail', true);
    });
  };

  /**
   * Watch history data and create table
   */
  $scope.$watchCollection(function () {
    return vm.displayData.history;
  }, function (newVal) {
    if (angular.isUndefined(newVal)) {
      return;
    }
    vm.tableData = newVal;
    vm.rowData = vm.tableData.slice();
  });

  /**
   * @todo Link to the proper records
   */
  vm.rowFn = function (id, fnName) { // jshint ignore:line

  };

  vm.init();
}]);