'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersHistory', [function () {
  return {
    templateUrl: '/components/admin/users/directives/history/directives/partials/history.html',
    controller: 'AdminUsersHistoryCtrl',
    controllerAs: 'tableCtrl',
    scope: {},
    bindToController: {
      hide: '@'
    }
  };
}]);