describe('AdminUsersHistoryService', function () {
  var scope, httpBackend, Service, adminUsersService, adminUsersAppraiserService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
      $provide.factory('AdminUsersAppraiserService', AdminUsersAppraiserServiceMock);
    });
  });

  beforeEach(inject(function (
  $rootScope, $httpBackend, AdminUsersHistoryService, AdminUsersService, AdminUsersAppraiserService) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    Service = AdminUsersHistoryService;
    adminUsersService = AdminUsersService;
    adminUsersAppraiserService = AdminUsersAppraiserService;

    spyOn(Service, 'hashData');
    spyOn(Service, 'formatData');

    httpBackend.whenGET(/.*/).respond({data: [1,2,3]});
  }));

  describe('getHistory', function () {
    beforeEach(function () {
      spyOn(Service, 'amcHistory');
      spyOn(Service, 'appraiserHistory');
    });

    describe('appraiser', function () {
      beforeEach(function () {
        adminUsersService.record._type = 'appraiser';
        Service.getHistory();
      });

      it('should retrieve appraiser history', function () {
        expect(Service.appraiserHistory).toHaveBeenCalled();
      });
    });

    describe('amc', function () {
      beforeEach(function () {
        adminUsersService.record._type = 'amc';
        Service.getHistory();
      });

      it('should get AMC history', function () {
        expect(Service.amcHistory).toHaveBeenCalled();
      });
    });
  });

  describe('amcHistory', function () {
    beforeEach(function () {
      spyOn(Service, 'request').and.callThrough();
      Service.amcHistory();
      httpBackend.flush();
      scope.$digest();
    });

    it('should make the proper request', function () {
      expect(Service.request).toHaveBeenCalledWith('amc', 'history');
    });
    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should format data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('appraiserHistory', function () {
    beforeEach(function () {
      spyOn(adminUsersAppraiserService, 'getAppraisals').and.callThrough();
    });

    describe('error on no appraisals', function () {
      beforeEach(function () {
        delete adminUsersAppraiserService.safeData.appraiser[1]._appraisals.completed;
      });
      it('should throw an error on no appraisals', function () {
        try {
          Service.appraiserHistory();
          scope.$digest();
          expect(true).toEqual(false);
        } catch (e) {
          expect(e.message).toEqual('Unable to load history data');
        }
      });
    });

    describe('no error', function () {
      beforeEach(function () {
        Service.appraiserHistory();
        scope.$digest();
      });
      it('should get appraisers', function () {
        expect(adminUsersAppraiserService.getAppraisals).toHaveBeenCalled();
      });
      it('should hash Data', function () {
        expect(Service.hashData).toHaveBeenCalled();
      });
      it('should format data', function () {
        expect(Service.formatData).toHaveBeenCalled();
      });
    });
  });
});