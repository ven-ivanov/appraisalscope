'use strict';

var app = angular.module('frontendApp');

/**
 * @todo Missing the ability to filter by appraiser ID on the backend
 * @link https://github.com/ascope/manuals/issues/447
 */
app.factory('AdminUsersHistoryService',
['$http', 'API_PREFIX', 'AsTableService', 'AdminUsersService', 'AdminUsersAppraiserService', 'AsDataService',
function ($http, API_PREFIX, AsTableService, AdminUsersService, AdminUsersAppraiserService, AsDataService) {
  var Service = {
    // Safe data
    safeData: {
      // History data
      history: {},
      // Table heading
      heading: [],
      // Table columns
      columns: [
        {
          label: 'File #',
          fn: 'displayApplication',
          linkLabel: {data: 'file'},
          variableLabel: true,
          search: true
        },
        {
          label: 'Borrower name',
          fn: 'displayApplication',
          linkLabel: {data: 'fullName'},
          variableLabel: true,
          search: true
        },
        {
          label: 'Address',
          data: 'address1',
          search: true
        },
        {
          label: 'City',
          data: 'city',
          search: true
        },
        {
          label: 'State',
          data: 'state',
          search: true
        },
        {
          id: 'jobType',
          label: 'Job Type',
          data: 'jobTypeLabel',
          search: true
        },
        {
          id: 'appFee',
          label: 'App fee',
          data: 'appFee',
          search: true
        },
        {
          label: 'Client',
          fn: 'displayClient',
          linkLabel: {data: 'clientName'},
          variableLabel: true,
          search: true
        },
        {
          label: 'Date completed',
          data: 'completedAt',
          filter: 'date',
          search: true
        }
      ]
    },
    // Data for display
    displayData: {
      // History
      history: []
    },
    // Data transformation
    transformers: {
      fullName: {
        source: 'property.contacts.borrower.fullName',
        dest: 'fullName'
      },
      address1: {
        source: 'property.location.address1',
        dest: 'address1'
      },
      city: {
        source: 'property.location.city',
        dest: 'city'
      },
      state: {
        source: 'property.location.state.name',
        dest: 'state'
      },
      clientName: {
        source: 'client.name',
        dest: 'clientName'
      },
      completedAt: {
        filter: 'date'
      },
      jobTypeLabel: {
        source: 'jobType.label',
        dest: 'jobTypeLabel'
      },
      appFee: {
        source: 'payment.payable.fee',
        dest: 'appFee',
        filter: 'currency'
      }
    },
    /**
     * Get history data
     */
    getHistory: function () {
      try {
        // AMC
        if (AdminUsersService.record._type === 'amc') {
          return Service.amcHistory();
          // Appraiser
        } else if (AdminUsersService.record._type === 'appraiser') {
          return Service.appraiserHistory();
        }
      } catch (e) {
        console.log(e.message);
      }
    },
    /**
     * Get history for AMC view
     */
    amcHistory: function () {
      return Service.request('amc', 'history')
      .then(function (response) {
        // Hash and format for table
        Service.hashData(response.data, 'history');
        Service.formatData();
      });
    },
    /**
     * Get history for appraiser view
     */
    appraiserHistory: function () {
      var appraiser = AdminUsersAppraiserService.safeData.appraiser[AdminUsersService.id];
      // In case we don't have appraisals yet
      return AdminUsersAppraiserService.getAppraisals()
      .then(function () {
        if (angular.isDefined(appraiser._appraisals.completed)) {
          // Hash
          Service.hashData(appraiser._appraisals.completed, 'history');
          // Format for display
          Service.formatData();
        } else {
          // Very basic error handling
          throw Error('Unable to load history data');
        }
      });
    }
  };
  /**
   * Inherit from AdminUsersService
   */
  Service.request = AdminUsersService.request.bind(Service);
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'history');
  Service.transformData = AsDataService.transformData.bind(Service);
  Service.getDirectiveTableColumns = AdminUsersService.getDirectiveTableColumns.bind(Service);

  return Service;
}]);