'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersPendingOrdersCtrl',
['$scope', 'AdminUsersPendingOrdersService', '$state', '$timeout',
function ($scope, AdminUsersPendingOrdersService, $state, $timeout) {
  var vm = this;
  // @todo There's a better way to do this. Just need to find it
  var modalCloseDelay = 1000;
  // Hold reference to display data
  vm.displayData = AdminUsersPendingOrdersService.displayData;
  // Safe data
  vm.safeData = AdminUsersPendingOrdersService.safeData;
  /**
   * Table config
   */
  vm.safeData.heading = vm.heading = [
    {
      label: 'File #',
      fn: 'displayApplication',
      linkLabel: {data: 'file'},
      variableLabel: true,
      search: true
    },
    {
      label: 'Borrower name',
      fn: 'displayApplication',
      linkLabel: {data: 'fullName'},
      variableLabel: true,
      search: true
    },
    {
      label: 'Address',
      data: 'address1',
      search: true
    },
    {
      label: 'City',
      data: 'city',
      search: true
    },
    {
      label: 'State',
      data: 'state',
      search: true
    },
    {
      label: 'Client',
      fn: 'displayClient',
      linkLabel: {data: 'clientName'},
      variableLabel: true,
      search: true
    },
    {
      label: 'Status',
      data: 'assignedStatus',
      search: true
    }
  ];

  /**
   * Load data
   */
  vm.init = function () {
    AdminUsersPendingOrdersService.getPendingOrders();
  };

  /**
   * Watch table data and display it
   */
  $scope.$watchCollection(function () {
    return vm.displayData.pending;
  }, function (newVal) {
    if (angular.isUndefined(newVal)) {
      return;
    }
    vm.tableData = newVal;
    vm.rowData = vm.tableData.slice();
  });

  /**
   * Link to the proper location from each record link
   */
  vm.linkFn = function (id, name) {
    switch (name) {
      case 'displayApplication':
        break;
      case 'displayClient':
        /**
         * @todo I have a modal defined here for client not found. If the state transitions and client isn't
         * found, transition back here and display modal
         */
        $timeout(function () {
          // Transition to show client
          $scope.$emit('hide-modal', 'appraiser-user-additional-details');
          $state.go('main.users', {activeTab: 'client', client: id, details: 'details', appraiser: ''});
        }, modalCloseDelay);
        break;
    }
  };

  // Init controller
  vm.init();
}]);
