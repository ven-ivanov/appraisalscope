describe('AdminUsersPendingOrdersService', function () {
  var scope, Service, adminUsersService, httpBackend, adminUsersAppraiserService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
      $provide.factory('AdminUsersAppraiserService', AdminUsersAppraiserServiceMock);
    });
  });

  beforeEach(inject(function (AdminUsersPendingOrdersService, AdminUsersService, $rootScope, $httpBackend, AdminUsersAppraiserService) {
    scope = $rootScope.$new();
    Service = AdminUsersPendingOrdersService;
    adminUsersService = AdminUsersService;
    httpBackend = $httpBackend;
    adminUsersAppraiserService = AdminUsersAppraiserService;

    spyOn(Service, 'hashData');
    spyOn(Service, 'formatData');
  }));

  describe('getPendingOrders', function () {
    beforeEach(function () {
      spyOn(adminUsersAppraiserService, 'queryAppraisalQueue').and.callThrough();
      spyOn(Service, 'createPendingOrdersView');
    });

    it('should call createPendingOrdersView when _appraisals is defined', function () {
      Service.getPendingOrders();
      expect(Service.createPendingOrdersView).toHaveBeenCalled();
    });
    it('should call queryAppraisalQueue when _appraisals are not defined', function () {
      adminUsersService.id = 2;
      Service.getPendingOrders();
      expect(adminUsersAppraiserService.queryAppraisalQueue).toHaveBeenCalled();
    });
    it('should call create pending orders after getting appraisals', function () {
      adminUsersService.id = 2;
      Service.getPendingOrders();
      scope.$digest();
      expect(Service.createPendingOrdersView).toHaveBeenCalled();
    });
  });

  describe('createPendingOrdersView', function () {
    beforeEach(function () {
      Service.createPendingOrdersView();
    });

    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should format data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });
});