'use strict';

var app = angular.module('frontendApp');

/**
 * @todo I need to link file and borrower name to the appraisal in question, but need to wait for orders to be completed
 * @todo Waiting on pending queue
 * @link https://github.com/ascope/manuals/issues/125
 */
app.factory('AdminUsersPendingOrdersService',
['$http', 'API_PREFIX', 'AdminUsersService', 'AdminUsersAppraiserService', 'AsDataService',
function ($http, API_PREFIX, AdminUsersService, AdminUsersAppraiserService, AsDataService) {
  var Service = {
    // Safe data
    safeData: {
      // Table data
      pending: {},
      // Table heading
      heading: []
    },
    // Data for display
    displayData: {
      // Table data
      pending: []
    },
    // Transform data for display
    transformers: {
      fullName: {
        source: 'property.contacts.borrower.fullName',
        dest: 'fullName'
      },
      address1: {
        source: 'property.location.address1',
        dest: 'address1'
      },
      city: {
        source: 'property.location.city',
        dest: 'city'
      },
      state: {
        source: 'property.location.state.name',
        dest: 'state'
      },
      clientName: {
        source: 'client.name',
        dest: 'clientName'
      }
    },
    /**
     * Retrieve pending orders
     */
    getPendingOrders: function () {
      // if appraisals aren't loaded for any reason, load them
      if (angular.isUndefined(AdminUsersAppraiserService.safeData.appraiser[AdminUsersService.id]._appraisals)) {
        AdminUsersAppraiserService.queryAppraisalQueue('pending')
        .then(function () {
          // Calculate data
          return Service.createPendingOrdersView();
        });
      } else {
        // Calculate data
        return Service.createPendingOrdersView();
      }
    },
    /**
     * Determine which appraisals actually are pending
     */
    createPendingOrdersView: function () {
      // Hash
      Service.hashData(AdminUsersAppraiserService.safeData.appraiser[AdminUsersService.id]._appraisals.pending,
      'pending');
      // Set for display
      Service.formatData();
    }
  };
  /**
   * Inherit from AdminUsersService
   */
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'pending');
  Service.transformData = AsDataService.transformData.bind(Service);

  return Service;
}]);