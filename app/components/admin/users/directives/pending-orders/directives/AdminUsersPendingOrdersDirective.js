'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersPendingOrders', ['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/directives/pending-orders/directives/partials/pending-orders.html',
    controller: 'AdminUsersPendingOrdersCtrl',
    controllerAs: 'tableCtrl',
    scope: {},
    link: function (scope) {
      DirectiveInheritanceService.inheritDirective.call(scope);
    }
  };
}]);