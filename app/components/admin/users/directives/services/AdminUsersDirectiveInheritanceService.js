'use strict';

var app = angular.module('frontendApp');

app.factory('AdminUsersDirectiveInheritanceService', ['$stateParams', function ($stateParams) {
  var defaultScrollTop = 30;
  return {
    /**
     * Shared directive functions
     */
    inherit: function () {
      var scope = this;

      /**
       * Scroll to top
       *
       * This is really not a good solution. I need to have modals appear in center screen always
       * @link https://github.com/ascope/front-end/issues/239
       */
      scope.scrollTop = function (distance, element, ignoreStateParams) {
        // Handle single argument
        if (arguments.length === 1 && isNaN(parseInt(distance))) {
          element = distance;
          distance = defaultScrollTop;
        }
        if (!ignoreStateParams) {
          // Element for scrolling
          if ($stateParams.activeTab === 'client') {
            scope.scrollTop(defaultScrollTop, '#company-additional-details', true);
            scope.scrollTop(defaultScrollTop, '#user-additional-details', true);
            return;
          }
        }
        element = element || '#company-details';
        // Distance
        distance = distance || null;
        if (distance) {
          angular.element(element).animate({ scrollTop: distance + 'px' });
        } else {
          angular.element(element).animate({ scrollTop: defaultScrollTop + 'px' });
        }
      };
    }
  };
}]);