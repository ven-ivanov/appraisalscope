'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersNewCompany', [function () {
  return {
    templateUrl: '/components/admin/users/directives/new-company/directives/partials/new-company.html',
    scope: {
      ctrl: '=',
      appraiser: '@'
    },
    link: function (scope) {
      // Whether this is in the appraiser view
      scope.appraiser = scope.$eval(scope.appraiser);
    }
  };
}]);