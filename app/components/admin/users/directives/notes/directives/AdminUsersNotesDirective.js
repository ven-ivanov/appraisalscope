'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Notes
 */
app.directive('adminUsersNotes', [function () {
  return {
    templateUrl: '/components/admin/users/directives/notes/directives/partials/notes.html',
    controller: 'AdminUsersNotesCtrl',
    controllerAs: 'notesCtrl',
    scope: {
      hide: '@'
    }
  };
}]);