describe('AdminUsersNotesService', function () {
  var adminUsersNotesService, scope, httpBackend, notes, adminUsersService;

  notes = [{id: 1, type: 'Admin', sender: 'Sender 1', text: 'Note 1', isDisplayed: false},
           {id: 2, type: 'Admin', sender: 'Sender 2', text: 'Note 2', isDisplayed: true}];
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersAppraiserService', AdminUsersAppraiserServiceMock);
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function (AdminUsersNotesService, $rootScope, $httpBackend, AdminUsersService) {
    adminUsersNotesService = AdminUsersNotesService;
    scope = $rootScope.$new();
    // Record
    adminUsersService = AdminUsersService;
    adminUsersService.record = {
      _type: 'company'
    };
    httpBackend = $httpBackend;
    // Requests
    httpBackend.whenGET(/notes/).respond({data: notes});
    httpBackend.whenPATCH(/note/).respond({id: 1, text: 'updated note'});
    httpBackend.whenDELETE(/note/).respond();
    httpBackend.whenPOST(/note/).respond({id: 1000, text: 'new note'});
    httpBackend.whenPUT(/note/).respond();
  }));

  describe('getNotes', function () {
    beforeEach(function () {
      spyOn(adminUsersNotesService, 'hashData');
      spyOn(adminUsersNotesService, 'formatData');
      adminUsersNotesService.getNotes();
      httpBackend.flush();
      scope.$digest();
    });

    it('should hash data', function () {
      expect(adminUsersNotesService.hashData).toHaveBeenCalled();
    });
    it('should format data', function () {
      expect(adminUsersNotesService.formatData).toHaveBeenCalled();
    });
  });

  describe('with data', function () {
    beforeEach(function () {
      adminUsersNotesService.hashData(notes, 'notes');
    });

    describe('formatDataCallback', function () {

    });

    describe('updateDisplay', function () {
      var response;
      beforeEach(function () {
        spyOn(adminUsersNotesService, 'formatData');
        response = adminUsersNotesService.updateDisplay(1);
        httpBackend.flush();
        scope.$digest();
      });
      it('should update the safe data', function () {
        expect(adminUsersNotesService.safeData.notes).toEqual({ 1: { id: 1, type: 'Admin', sender: 'Sender 1', text: 'Note 1', isDisplayed: true }, 2: { id: 2, type: 'Admin', sender: 'Sender 2', text: 'Note 2', isDisplayed: true } });
      });
      it('should update the display data', function () {
        expect(adminUsersNotesService.formatData).toHaveBeenCalled();
      });
    });

    describe('deleteNote', function () {
      beforeEach(function () {
        adminUsersNotesService.selectedNote = 1;
        spyOn(adminUsersNotesService, 'formatData');
        adminUsersNotesService.deleteNote();
        httpBackend.flush();
        scope.$digest();
      });

      it('should delete the note', function () {
        expect(adminUsersNotesService.safeData.notes).toEqual({
          2: {
            id: 2,
            type: 'Admin',
            sender: 'Sender 2',
            text: 'Note 2',
            isDisplayed: true
          }
        });
      });
      it('should call format data', function () {
        expect(adminUsersNotesService.formatData).toHaveBeenCalled();
      });
    });

    describe('submitNote', function () {
      beforeEach(function () {
        spyOn(adminUsersNotesService, 'formatData');
      });
      describe('update', function () {
        beforeEach(function () {
          adminUsersNotesService.displayData = {text: 'Text test'};
          adminUsersNotesService.selectedNote = 1;
          adminUsersNotesService.submitNote();
          httpBackend.flush();
          scope.$digest();
        });

        it('should update the safe data', function () {
          expect(adminUsersNotesService.safeData.notes).toEqual({ 1: { id: 1, type: 'Admin', sender: 'Sender 1', text: 'Text test', isDisplayed: false }, 2: { id: 2, type: 'Admin', sender: 'Sender 2', text: 'Note 2', isDisplayed: true } });
        });
      });

      describe('new', function () {
        beforeEach(function () {
          adminUsersNotesService.submitNote();
          httpBackend.flush();
          scope.$digest();
        });

        it('should update the safe data', function () {
          expect(adminUsersNotesService.safeData.notes[1000].text).toEqual('new note');
        });
      });
    });
  });
});