'use strict';

var app = angular.module('frontendApp');

/**
 * Notes table
 */
app.controller('AdminUsersNotesTableCtrl',
['$scope', 'AdminUsersNotesService', function ($scope, AdminUsersNotesService) {
  var vm = this;

  // Safe data and table heading
  vm.safeData = AdminUsersNotesService.safeData;
  vm.heading = AdminUsersNotesService.safeData.heading;

  /**
   * Create table from display data
   */
  $scope.$watchCollection(function () {
    return AdminUsersNotesService.displayData.notes;
  }, function (newVal) {
    if (angular.isUndefined(newVal)) {
      return;
    }
    vm.tableData = newVal;
    vm.rowData = vm.tableData.slice();
  });

  /**
   * Edit an individual note
   * @param id
   */
  vm.editNote = function (id) {
    AdminUsersNotesService.displayData.noteContentTitle = 'Edit note';
    // Get note content
    AdminUsersNotesService.displayData.text = AdminUsersNotesService.safeData.notes[id].text;
    $scope.$emit('show-modal', 'edit-note', true);
  };

  /**
   * Edit note when it is clicked
   * @param id
   */
  vm.rowFn = function (id) {
    // Keep reference to selected note
    AdminUsersNotesService.selectedNote = id;
    // Edit
    vm.editNote(id);
  };

  /**
   * Link functions
   */
  vm.linkFn = function (id, fnName) {
    // Keep reference to selected note
    AdminUsersNotesService.selectedNote = id;
    // Display/do not display
    if (fnName === 'updateDisplay') {
      AdminUsersNotesService.updateDisplay(id)
      .catch(function () {
        $scope.$emit('show-modal', 'update-data-failed', true);
        // Update safe data
        AdminUsersNotesService.safeData.notes[id].isDisplayed = AdminUsersNotesService.safeData.notes[id].isDisplayed ? false : true;
        // Update display data
        AdminUsersNotesService.formatData();
      });
      // Delete note
    } else if (fnName === 'delete') {
      $scope.$emit('show-modal', 'delete-note-confirm', true);
      // Edit note
    } else if (fnName === 'edit') {
      vm.editNote(id);
    }
  };
}]);
