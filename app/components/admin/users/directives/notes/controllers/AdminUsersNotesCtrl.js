'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Notes
 */
app.controller('AdminUsersNotesCtrl',
['$scope', 'AdminUsersNotesService', 'CKEDITOR_CONFIG', function ($scope, AdminUsersNotesService, CKEDITOR_CONFIG) {
  var vm = this;
  // Ckeditor config
  $scope.editorOptions = CKEDITOR_CONFIG.config;
  // Display data
  vm.displayData = AdminUsersNotesService.displayData;
  // Safe data
  vm.safeData = AdminUsersNotesService.safeData;
  // Construct the heading based on the non-hidden items
  vm.safeData.heading = AdminUsersNotesService.getDirectiveTableColumns($scope.hide);

  /**
   * Data load
   */
  vm.init = function () {
    AdminUsersNotesService.getNotes();
  };

  /**
   * Display new note modal
   */
  vm.showNewNote = function () {
    vm.displayData.text = '';
    // Remove any previously selected note ID
    AdminUsersNotesService.selectedNote = null;
    // Set up modal
    $scope.$broadcast('show-modal', 'edit-note', true);
    vm.displayData.noteContentTitle = 'Create new note';
  };
  /**
   * Submit new note
   */
  vm.submitNote = function () {
    AdminUsersNotesService.submitNote()
    .then(function (type) {
      // Set content to blank
      vm.displayData.text = '';
      // Hide modal
      $scope.$broadcast('hide-modal', 'edit-note');
      // Success
      if (type === 'new') {
        $scope.$broadcast('show-modal', 'new-note-success', true);
      }
    })
    // Show appropriate failure modal
    .catch(function (headers) {
      try {
        if (headers.config.method === 'PATCH') {
          $scope.$broadcast('show-modal', 'edit-note-failure', true);
        } else {
          $scope.$broadcast('show-modal', 'new-note-failure', true);
        }
      } catch (e) {
        // Display general failure modal
      }
    })
    .finally(function () {

    });
  };
  /**
   * Delete note
   */
  vm.deleteNote = function () {
    AdminUsersNotesService.deleteNote()
    .then(function () {
      $scope.$broadcast('hide-modal', 'delete-note-confirm');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'delete-note-failure', true);
    });
  };

  // Init view
  vm.init();
}]);