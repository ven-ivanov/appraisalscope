'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Notes
 *
 * Looks good -- 8/7
 */
app.factory('AdminUsersNotesService',
['$http', 'API_PREFIX', '$filter', 'AdminUsersService', '$resource', 'AdminUsersResourceService', 'AsDataService',
function ($http, API_PREFIX, $filter, AdminUsersService, $resource, AdminUsersResourceService, AsDataService) {
  var Service = {
    // Unmodified data from DB
    safeData: {
      notes: {},
      // Table columns
      columns: [
        // Sender info
        {
          label: 'Sender',
          data: 'senderInfo'
        },
        // Note content
        {
          label: 'Note',
          data: 'text',
          filter: 'stripTags'
        },
        // Is display/is not displayed
        {
          id: 'isDisplayed',
          label: 'Is displayed',
          fn: 'updateDisplay',
          linkLabel: {true: 'Display', false: 'Do not display'},
          data: 'isDisplayed'
        },
        {
          label: 'Edit',
          fn: 'edit',
          linkLabel: 'Edit'
        },
        {
          label: 'Delete',
          fn: 'delete',
          linkLabel: 'Delete'
        }
      ],
      // Type under which to make requests
      requestType: ''
    },
    // Modified data for display
    displayData: {
      // Yes/no for isDisplayed
      yesNoDropdown: AdminUsersService.yesNoDropdown,
      notes: []
    },
    // Transform data for display
    transformers: {
      // Filter date
      createdAt: {
        filter: 'date'
      },
      text: {
        filter: 'stripTags'
      }
    },
    /**
     * Request (either company, user, or branch)
     */
    request: function (requestFn, urlParams, bodyParams) {
      Service.getRequestType();
      // Get $resource configuration
      return AdminUsersResourceService.notes[requestFn](Service.safeData.requestType, urlParams, bodyParams);
    },
    /**
     * Determine request type
     */
    getRequestType: function () {
      switch (AdminUsersService.record._type) {
        case 'asc':
          Service.safeData.requestType = 'appraiser';
          break;
        case 'inspector':
        case 'realtor':
          Service.safeData.requestType = 'user';
          break;
        default:
          Service.safeData.requestType = AdminUsersService.record._type;
      }
    },
    /**
     * Retrieve notes on load
     */
    getNotes: function () {
      return Service.request('init')
      .then(function (response) {
        // Create hash
        Service.hashData(response.data, 'notes');
        // Create display data
        Service.formatData();
      });
    },
    /**
     * Create sender info, remove tags from notes content when updating display
     */
    formatDataCallback: function () {
      // Create sender info
      angular.forEach(Service.displayData.notes, function (data) {
        data.senderInfo = data.author.fullName;
        // Special case for display name
        if (data.author.type) {
          data.senderInfo = data.senderInfo + ' (' + data.author.type + ')';
        }
        // Attach date
        if (data.createdAt) {
          data.senderInfo = data.senderInfo + ' -- ' + data.createdAt;
        }
      });
    },
    /**
     * Update display/do not display value
     * @param id Number
     */
    updateDisplay: function (id) {
      // Update safe data
      Service.safeData.notes[id].isDisplayed = Service.safeData.notes[id].isDisplayed ? false : true;
      return Service.updateNote({isDisplayed: Service.safeData.notes[id].isDisplayed, method: 'patch'})
      .then(function () {
        // Update display data
        Service.formatData();
      });
    },
    /**
     * Delete note
     */
    deleteNote: function () {
      return Service.request('deleteNote', {noteId: Service.selectedNote}).then(function () {
        // Delete from safe data
        delete Service.safeData.notes[Service.selectedNote];
        // Update display
        Service.formatData();
      });
    },
    /**
     * Update or create note
     */
    submitNote: function () {
      // Update note
      if (Service.selectedNote) {
        return Service.updateNote({text: Service.displayData.text, method: 'put'}).then(function () {
          // Add to safe data
          Service.safeData.notes[Service.selectedNote].text = angular.copy(Service.displayData.text);
          // Update display
          Service.formatData();
          return 'update';
        });
      } else {
        // new note
        return Service.newNote().then(function (response) {
          // Add to safe data
          Service.safeData.notes[response.id] = response;
          // Update display
          Service.formatData();
          return 'new';
        });
      }
    },
    /**
     * Create a new note
     */
    newNote: function () {
      return Service.request('newNote', {}, {text: Service.displayData.text});
    },
    /**
     * Update an existing note
     */
    updateNote: function (params) {
      return Service.request('updateNote', {noteId: Service.selectedNote}, params);
    }
  };
  /**
   * Inherit from AdminUsersService
   */
  Service.hashData = AsDataService.hashData.bind(Service);
  // Format data, then call callback
  Service.formatData = AsDataService.formatData.bind(Service, 'notes', Service.formatDataCallback);
  Service.transformData = AsDataService.transformData.bind(Service);
  // Create the table with the necessary columns
  Service.getDirectiveTableColumns = AdminUsersService.getDirectiveTableColumns.bind(Service);
  return Service;
}]);
