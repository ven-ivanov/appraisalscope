'use strict';

var app = angular.module('frontendApp');

/**
 * Tabs for Admin Users section
 */
app.controller('AdminUsersTabsCtrl',
['$stateParams', '$scope', 'AdminUsersService', function ($stateParams, $scope, AdminUsersService) {
  var vm = this;
  var Service = AdminUsersService;

  // Keep any query parameters previously set by loading a record
  //AdminUsersService.setTabs(vm.tabs);

  vm.displayData = Service.displayData;

  /**
   * Get tab values
   */
  Service.getTabValues($stateParams);
  /**
   * Set active class on currently active tab
   * @param tab
   * @returns {string}
   */
  vm.tabClass = function (tab) {
    return Service.getTabClass(tab, $scope.activeTab);
  };

  /**
   * Remove record details when switching tab
   */
  vm.switchTab = function () {
    // Don't watch for update when removing record
    if (angular.isFunction(AdminUsersService.detachUpdateWatcher)) {
      AdminUsersService.detachUpdateWatcher();
    }
    delete Service.id;
    delete Service.record;
  };
}]);