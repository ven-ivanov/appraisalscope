'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users tabs directive
 */
app.directive('adminUsersTabs', [function () {
  return {
    restrict: 'E',
    templateUrl: '/components/admin/users/directives/tabs/directives/partials/tabs.html',
    controller: 'AdminUsersTabsCtrl',
    controllerAs: 'adminUsersTabsCtrl'
  };
}]);