'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersFeeScheduleCtrl',
['$scope', 'AdminUsersFeeScheduleService', '$timeout', 'REQUEST_DELAY',
function ($scope, AdminUsersFeeScheduleService, $timeout, REQUEST_DELAY) {
  var vm = this;
  // keep reference
  vm.displayData = AdminUsersFeeScheduleService.displayData;

  /**
   * Init controller on load
   */
  vm.init = function () {
    AdminUsersFeeScheduleService.getSettings()
    .then(function () {
      // Hide tech fee if necessary
      AdminUsersFeeScheduleService.displayData.hideTechService = !!$scope.hideTechService;
    });
  };

  /**
   * Watch and update
   */
  $scope.$watchCollection(function () {
    return vm.displayData.feeSchedule;
  }, function (newVal, oldVal) {
    if (angular.isUndefined(newVal) ||
        !Object.keys(newVal).length ||
        !Object.keys(oldVal).length ||
        angular.equals(oldVal, newVal)) {
      return;
    }
    if (vm.typing) {
      $timeout.cancel(vm.typing);
    }
    // Make sure we have a valid number
    if (AdminUsersFeeScheduleService.checkValid(newVal)) {
      // Wait and then make the request
      vm.typing = $timeout(function () {
        AdminUsersFeeScheduleService.updateData(newVal)
        .catch(function () {
          $scope.$broadcast('show-modal', 'update-value-failure', true);
        });
        vm.typing = null;
      }, REQUEST_DELAY.ms);
    }
  });
  // Init controller
  vm.init();
}]);