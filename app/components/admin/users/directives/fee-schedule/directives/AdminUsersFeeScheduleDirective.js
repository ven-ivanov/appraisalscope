'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersFeeSchedule', [function () {
  return {
    templateUrl: '/components/admin/users/directives/fee-schedule/directives/partials/fee-schedule.html',
    controller: 'AdminUsersFeeScheduleCtrl',
    controllerAs: 'feeScheduleCtrl',
    scope: {
      hideTechService: '@'
    }
  };
}]);