describe('AdminUsersFeeScheduleService', function () {
  var scope, Service, adminUsersService, httpBackend;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function ($rootScope, AdminUsersFeeScheduleService, AdminUsersService, $httpBackend) {
    scope = $rootScope.$new();
    Service = AdminUsersFeeScheduleService;
    adminUsersService = AdminUsersFeeScheduleService;
    httpBackend = $httpBackend;

    httpBackend.whenGET(/.*/).respond({appraiserFeePercentage: true, techFeeEnabled: true, percentageFromClientFees: 52.43});
    httpBackend.whenPATCH(/.*/).respond();

    spyOn(Service, 'hashData');
    spyOn(Service, 'formatPlainObjectData');
    spyOn(Service, 'request').and.callThrough();
  }));

  describe('getSettings', function () {
    beforeEach(function () {
      Service.getSettings();
      httpBackend.flush();
      scope.$digest();
    });

    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should format as plain object', function () {
      expect(Service.formatPlainObjectData).toHaveBeenCalled();
    });
  });

  describe('updateData', function () {
    var params;
    describe('valid number', function () {
      beforeEach(function () {
        params = {appraiserFeePercentage: true, techFeeEnabled: true, percentageFromClientFees: 99.99};
        Service.updateData(params);
        httpBackend.flush();
        scope.$digest();
      });

      it('should call the request with the right parameters', function () {
        expect(Service.request).toHaveBeenCalledWith('updateSettings', { appraiserFeePercentage: true, techFeeEnabled: true, percentageFromClientFees: 99.99 });
      });
      it('should hash data', function () {
        expect(Service.hashData).toHaveBeenCalled();
      });
    });

    describe('zero value', function () {
      beforeEach(function () {
        params = {appraiserFeePercentage: true, techFeeEnabled: true, percentageFromClientFees: 0};
        Service.updateData(params);
      });

      it('should make the report fee percentage as on, but fee as 0', function () {
        expect(Service.request.calls.argsFor(0)[1].percentageFromClientFees).toEqual(0);
      });
    });

    describe('null value', function () {
      beforeEach(function () {
        params = {appraiserFeePercentage: true, techFeeEnabled: true, percentageFromClientFees: null};
        Service.updateData(params);
      });

      it('should make the report fee percentage as on, but fee as 0', function () {
        expect(Service.request.calls.argsFor(0)[1].percentageFromClientFees).toEqual(null);
      });
    });

    describe('empty string', function () {
      beforeEach(function () {
        params = {appraiserFeePercentage: true, techFeeEnabled: true, percentageFromClientFees: ''};
        Service.updateData(params);
      });

      it('should make the report fee percentage as on, but fee as 0', function () {
        expect(Service.request.calls.argsFor(0)[1].percentageFromClientFees).toEqual(null);
      });
    });
  });
});