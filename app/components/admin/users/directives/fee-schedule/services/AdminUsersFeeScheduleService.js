'use strict';

var app = angular.module('frontendApp');

app.factory('AdminUsersFeeScheduleService',
['$http', 'API_PREFIX', 'AdminUsersService', 'AsDataService', function ($http, API_PREFIX, AdminUsersService, AsDataService) {
  var Service = {
    // Safe data
    safeData: {
      feeSchedule: {}
    },
    // Data for display
    displayData: {
      feeSchedule: {},
      // Yes no dropdown
      yesNoDropdown: AdminUsersService.yesNoDropdown,
      // Section
      section: AdminUsersService.section
    },
    transformers: {
      // Display 2 decimals for percentage from client fees
      percentageFromClientFees: {
        filter: 'number:2'
      }
    },
    /**
     * Retrieve job types on load
     */
    getSettings: function () {
      return Service.request('getSettings')
      .then(function (response) {
        // Appraiser fee percentage (based on whether percentage from client fees is null)
        response.appraiserFeePercentage = response.percentageFromClientFees !== null;
        // Create hash
        Service.hashData(response, 'feeSchedule');
        // Create display
        Service.formatPlainObjectData();
      });
    },
    /**
     * Update data
     * @param params Object
     */
    updateData: function (params) {
      // Don't update view during this process
      params = angular.copy(params);
      // Return null if empty or null, but not if zero
      if (params.percentageFromClientFees === '' || params.percentageFromClientFees === null || !params.appraiserFeePercentage) {
        params.percentageFromClientFees = null;
      } else {
        params.percentageFromClientFees = parseFloat(params.percentageFromClientFees);
      }
      return Service.request('updateSettings', params)
      .then(function () {
        // Update safe data
        Service.hashData(params, 'feeSchedule');
      });
    },
    /**
     * Check that we have a valid number
     */
    checkValid: AdminUsersService.checkValidNumber
  };

  /**
   * Inherit from AdminUsersService
   */
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatPlainObjectData = AsDataService.formatPlainObjectData.bind(Service, 'feeSchedule');
  Service.transformData = AsDataService.transformData.bind(Service);
  // Requests
  Service.request = AdminUsersService.request.bind(Service, 'appraisementSettings');

  return Service;
}]);