'use strict';

var app = angular.module('frontendApp');

/**
 * Admin User - Realtor service
 */
app.factory('AdminUsersRealtorService',
['$http', 'API_PREFIX', 'AdminUsersService', 'AsDataService', function ($http, API_PREFIX, AdminUsersService, AsDataService) {
  var AdminUsersRealtorService = {
    // Realtor ID
    realtorId: 0,
    // Safe data
    safeData: {
      // Realtors table
      realtor: {},
      // Table heading
      heading: [
        // Name
        {
          label: 'Name',
          data: 'name',
          search: true
        },
        // Certification
        {
          label: 'Certification',
          data: 'certification',
          search: true
        },
        // FHA
        {
          label: 'FHA',
          data: 'fha',
          filter: 'boolYesNo',
          isStatic: true,
          search: true
        }
      ],
      // Realtor ratings
      ratings: {},
      // Certification details
      certificationDetails: {},
      // Reports
      realtorReports: {}
    },
    // Display data
    displayData: {
      // Table data
      realtor: [],
      // Details
      details: {},
      // States dropdown
      states: [],
      // realtor quality data
      ratings: {},
      // realtor quality tab values
      ratingTabs: {},
      // Selected tab
      //realtorRatingsSelectedTab: '',
      // Certification details
      certificationDetails: [],
      // Realtor reports
      realtorReports: [],
      // Yes/no dropdown
      yesNoDropdown: AdminUsersService.yesNoDropdown
    },
    // Data transformations
    transformers: {
      // Certification display
      certification: AdminUsersService.getCertificationTransformer(),
      // Registered data
      registered: {
        filter: 'date'
      }
    },
    /**
     * Callback for formatting data
     */
    formatCallback: function () {
      angular.forEach(AdminUsersRealtorService.displayData.realtor, function (realtor) {
        realtor.name = realtor.firstName + ' ' + realtor.lastName;
      });
    },
    /**
     * Retrieve realtor reports
     */
    getRealtorReports: function () {
      return $http.get(API_PREFIX() + '/v2.0/users/realtor/' + AdminUsersRealtorService.realtorId + '/reports')
      .then(function (response) {
        // Hash
        AdminUsersRealtorService.hashData(response.data, 'realtorReports');
        // Set for display
        AdminUsersRealtorService.displayData.realtorReports = response.data;
      });
    }
  };

  /**
   * Inherit from AdminUsersService
   */
  AdminUsersRealtorService.loadRecords = AdminUsersService.loadRecords.bind(AdminUsersRealtorService, 'realtor');
  AdminUsersRealtorService.hashData = AsDataService.hashData.bind(AdminUsersRealtorService);
  AdminUsersRealtorService.formatData = AsDataService.formatData.bind(AdminUsersRealtorService, 'realtor');
  AdminUsersRealtorService.transformData = AsDataService.transformData.bind(AdminUsersRealtorService);
  // Get record details
  AdminUsersRealtorService.getRecordDetails =
  AdminUsersService.getRecordDetails.bind(AdminUsersRealtorService, 'realtor',
  AdminUsersRealtorService.getRealtorReports);
  // Set state
  AdminUsersRealtorService.setStateOnLoad = AdminUsersService.setStateOnLoad.bind(AdminUsersRealtorService, 'realtor');
  // Toggle settings
  AdminUsersRealtorService.toggleSetting = AdminUsersService.toggleSetting.bind(AdminUsersRealtorService, 'realtor');
  // Send login credentials
  AdminUsersRealtorService.sendLogin = AdminUsersService.sendLogin.bind(AdminUsersRealtorService, 'realtor');
  // Ratings
  AdminUsersRealtorService.getRatings =
  AdminUsersService.getRatings.bind(AdminUsersRealtorService, 'realtor', null);
  // Update record
  AdminUsersRealtorService.updateRecord = AdminUsersService.updateRecord.bind(AdminUsersRealtorService, 'realtor');


  return AdminUsersRealtorService;
}]);