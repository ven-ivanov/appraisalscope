'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersRealtorTableCtrl', ['$scope', 'AdminUsersRealtorService', 'AdminUsersTableCtrlInheritanceService', function ($scope, AdminUsersRealtorService, AdminUsersTableCtrlInheritanceService) {

  // Inherit table controller methods
  AdminUsersTableCtrlInheritanceService.inherit.call(this, $scope, AdminUsersRealtorService, 'realtor');
}]);