'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Realtor view controller
 */
app.controller('AdminUsersRealtorCtrl',
['$scope', 'AdminUsersRealtorService', '$stateParams', 'AdminUsersService', 'AdminUsersCtrlInheritanceService',
 function (
 $scope, AdminUsersRealtorService, $stateParams, AdminUsersService, AdminUsersCtrlInheritanceService) {
   var vm = this;
   // Inherit common functions
   AdminUsersCtrlInheritanceService.createCtrl.call(vm, AdminUsersRealtorService, $scope, 'realtor');
   // Realtor details
   vm.displayData = AdminUsersRealtorService.displayData;
   // Safe data
   vm.safeData = AdminUsersRealtorService.safeData;
   /**
    * Additional details tabs
    */
     // Tab config
   vm.additionalDetailsTabsConfig = {
     notes: 'Notes',
     coverage: 'Coverage',
     appraisalForms: 'Appraisal forms',
     feeSchedule: 'Fee schedule',
     pendingOrders: 'Pending orders',
     history: 'History',
     additionalDocs: 'Additional docs'
   };
   // Tab config array (for ordering)
   vm.additionalDetailsTabs =
   ['notes', 'coverage', 'appraisalForms', 'feeSchedule', 'pendingOrders', 'history', 'additionalDocs'];
   // Selected tab
   AdminUsersService.currentDetailsTab = vm.selectedAdditionalDetailsTab = 'notes';
   vm.changeAdditionalDetailsTab = function (tab) {
     // Keep track of tab
     AdminUsersService.changeUrl({details: tab});
   };

   /**
    * Initiate data and state
    */
   vm.init = function () {
     // Set state on load
     AdminUsersRealtorService.setStateOnLoad($stateParams, vm);
     // Keep reference to which section we're on
     AdminUsersService.section = 'realtor';
   };

   // Init controller load
   vm.init();
 }]);