'use strict';

var app = angular.module('frontendApp');

/**
 * Compile additional details only when it's needed
 */
app.directive('adminUsersRealtorAdditionalDetails', ['DirectiveConditionalLoadService', function (DirectiveConditionalLoadService) {
  return function (scope, elem) {
    var template;
    // Template
    template =
    ['<info-modal modal-id="realtor-additional-details" modal-title="Additional details" button-text="OK" modal-class="notification-window" modal-width="100%" modal-height="max">',
     '<as-tabs tab-config="realtorCtrl.additionalDetailsTabsConfig" tabs="realtorCtrl.additionalDetailsTabs" tab-width="7ths" selected-tab="realtorCtrl.selectedAdditionalDetailsTab" change-tab="realtorCtrl.changeAdditionalDetailsTab(tab)">',
     '</as-tabs>',
      // Switch show directive
     '<div ng-switch="realtorCtrl.selectedAdditionalDetailsTab">',
     // Notes
     '<admin-users-notes ng-switch-when="notes"></admin-users-notes>',
     // Coverage
     '<admin-users-coverage ng-switch-when="coverage"></admin-users-coverage>',
     // Appraisal forms
     '<admin-users-jobtypes ng-switch-when="appraisalForms" hide="appraiser-requested-fee"></admin-users-jobtypes>',
     // Fee schedule
     '<admin-users-fee-schedule ng-switch-when="feeSchedule" hide-tech-service="true"></admin-users-fee-schedule>',
     // Pending order
     '<admin-users-pending-orders ng-switch-when="pendingOrders"></admin-users-pending-orders>',
     // History
     '<admin-users-history ng-switch-when="history"></admin-users-history>',
     // Additional docs
     '<admin-users-appraiser-docs ng-switch-when="additionalDocs"></admin-users-appraiser-docs>',
     '</div>',
     '</info-modal>'];

    // Compile the directive only when necessary and then load it
    DirectiveConditionalLoadService.init.call(scope, template, elem, 'realtor-additional-details');
  };
}]);