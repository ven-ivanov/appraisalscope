'use strict';

var app = angular.module('frontendApp');

/**
 * Admin users - Realtor view
 */
app.directive('adminUsersRealtor', ['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/realtor/directives/partials/realtor.html',
    controller: 'AdminUsersRealtorCtrl',
    controllerAs: 'realtorCtrl',
    compile: function () {
      return {
        pre: function (scope) {
          // Expose trigger download
          DirectiveInheritanceService.inheritDirective.call(scope);
        }
      };
    }
  };
}]);