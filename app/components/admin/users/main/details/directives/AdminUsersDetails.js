'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Details
 */
app.directive('adminUsersDetails', ['DirectiveInheritanceService', '$compile', 'DomLoadService',
function (DirectiveInheritanceService, $compile, DomLoadService) {
  return {
    scope: {
      type: '@',
      ctrl: '=',
      sidebar: '=',
      disableEdit: '@'
    },
    link: function (scope, element) {
      var template = '';
      // Expose the trigger download method on scope
      DirectiveInheritanceService.inheritDirective.call(scope);

      /**
       * Watch for when sidebar should be attached
       */
      scope.$watch('sidebar', function (newVal) {
        if (angular.isUndefined(newVal)) {
          return;
        }
        // @todo - This is a complete repaint, need to refactor
        angular.element(element).html('');
        // Sidebar
        if (newVal) {
          template = ['<div class="hidden-xs hidden-ml hidden-el">',
                      '<button class="btn btn-info sidebar-btn" type="button" ng-click="toggleSidebar()">Open Sidebar</button>',
                      '<div class="sidebar col-lg-6 pad-null hidden-el" ng-show="sidebarOpen">',
                      '<button class="btn btn-info sidebarbtn-close" type="button" ng-click="toggleSidebar()">Close Sidebar</button>',
                      '<div class="dashboard-sidebar" >', '<div class="page-wrapper col-md-12 col-sm-10  col-xs-12">',
                      '<ng-include src="\'/components/admin/users/main/details/directives/partials/details.html\'" include-replace></ng-include>', '</div>', '</div>', '</div>',
                      '</div>'];
          template = $compile(template.join(''))(scope);
          angular.element(element).append(template);
          // No sidebar
        } else {
          template = ['<div class="col-el-4 col-ml-5 pad-null visible-el  visible-ml visible-xs" >',
                      '<div class="dashboard-sidebar" >', '<div class="page-wrapper col-md-12 col-sm-10  col-xs-12">',
                      '<ng-include src="\'/components/admin/users/main/details/directives/partials/details.html\'" include-replace></ng-include>',
                      '</div>', '</div>', '</div>'];
          template = $compile(template.join(''))(scope);
          angular.element(element).append(template);
        }
        // Editing disabled
        DomLoadService.load().then(function () {
          angular.element('.admin-user-details input:visible, .admin-user-details select:visible').attr('disabled',
          scope.$eval(scope.disableEdit) || false);
        });
      });
    }
  };
}]);