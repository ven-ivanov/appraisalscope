'use strict';

/**
 * Base controller of the Admin Users section
 */
var app = angular.module('frontendApp');

app.controller('AdminUsersCtrl',
['$scope', 'AdminUsersService', '$stateParams', '$state', function ($scope, AdminUsersService, $stateParams, $state) {
  var vm = this;

  /**
   * Redirect to client by default
   */
  if ($stateParams.activeTab === '') {
    $state.go('main.users', {activeTab: 'client'});
    return;
  }

  /**
   * Directives for each individual section
   */
  var directives = {
    client: 'admin-users-client',
    appraiser: 'admin-users-appraiser',
    amc: 'admin-users-amc',
    'appraiser-directory': 'admin-users-appraiser-directory',
    'amc-directory': 'admin-users-amc-directory',
    asc: 'admin-users-asc',
    disabled: 'admin-users-appraiser disabled="true"',
    'appraiser-panel': 'admin-users-appraiser-panel',
    'pending-users': 'admin-users-pending-users',
    'email-users': 'admin-users-email-users'
  };
  // Set active tab
  AdminUsersService.tab = vm.activeTab = $scope.activeTab = $stateParams.activeTab;
  // Set directive to compile
  $scope.directive = directives[$scope.activeTab];
  // Set page
  AdminUsersService.page = vm.page = $scope.page = $stateParams.page ? parseInt($stateParams.page) : null;
  // Get states and make available for entire section
  AdminUsersService.getStates().then(function (response) {
    // Hold reference to states
    AdminUsersService.states = response.data;
  });
}]);
