'use strict';

var app = angular.module('frontendApp');

/**
 * Service for constructing $resource URLs, parameters, methods, etc
 */
app.factory('AdminUsersResourceService', ['$resource', 'API_PREFIX', 'AdminUsersService', 'SessionService', '$q', function ($resource, API_PREFIX, AdminUsersService, SessionService, $q) {

  /**
   * Get request, non-array expected
   */
  var getRequest = function (url, params) {
    return $resource(API_PREFIX() + url).get(params).$promise;
  };

  /**
   * General post request
   */
  var post = function (url, params, body, headers) {
    if (!headers) {
      return new ($resource(API_PREFIX() + url, params))(body).$save();
    }
    // Specify put request with potential custom headers
    return new ($resource(API_PREFIX() + url, params, {
      postWithHeaders: {method: 'POST', headers: headers}
    }))(body).$postWithHeaders();
  };

  /**
   * General PUT request
   */
  var put = function (url, params, body, headers) {
    // Specify put request with potential custom headers
    return new ($resource(API_PREFIX() + url, params, {
      update: {method: 'PUT', headers: headers}
    }))(body).$update();
  };

  /**
   * Update request via POST
   */
  var patch = function (url, params, body, headers) {
    return new ($resource(API_PREFIX() + url, params, {
      update: {method: 'PATCH', headers: headers}
    }))(body).$update();
  };

  /**
   * Post with array body
   */
  var postArray = function (url, params, body) {
    return new ($resource(API_PREFIX() + url, params, {
      postArray: {
        method: 'POST',
        transformRequest: function (data) {
          var thisData = angular.copy(data), response = [];
          // Iterate and send as array
          angular.forEach(thisData, function (item) {
            response.push(item);
          });
          return JSON.stringify(response);
        }
      }
    }))(body).$postArray();
  };

  /**
   * Initiate a delete request
   */
  var deleteRequest = function (url, params, headers) {
    return new ($resource(API_PREFIX() + url, params, {
      doDelete: {method: 'DELETE', headers: headers}
    }))().$doDelete();
  };

  /**
   * AdminUsersResourceService
   */
  var Service = {
    // Standard request URLs
    requests: {
      branch: '/v2.0/client/companies/:companyId/branches/:branchId/',
      company: '/v2.0/client/companies/:companyId/',
      appraiser: '/v2.0/appraisement/appraisers/:appraiserId/',
      user: '/v2.0/client/companies/:companyId/branches/:branchId/employees/:employeeId/',
      // @todo This isn't the backend yet
      amc: '/v2.0/appraisement/amcs/:amcId/',
      appraiserManager: '/v2.0/appraisement/companies/:companyId/branches/:branchId/managers/:managerId',
      appraiserCompany: '/v2.0/appraisement/companies/:companyId/',
      appraiserCompanyBranch: '/v2.0/appraisement/companies/:companyId/branches/:branchId/',
      // Appraisers list main view
      appraisersList: '/v2.0/appraiser-lists/',
      email: '/v2.0/emails/',
      // Platform
      platform: '/v2.0/platform/',
      // Job type lists
      jobTypeLists: '/v2.0/job-type-lists/',
      // Location
      // @todo This is still undetermined
      // @link https://github.com/ascope/manuals/issues/111
      location: '/v2.0/location/',
      // Appraisers
      appraisals: '/v2.0/appraisals'
    },
    // Standard params
    params: {
      // Branch params
      branch: function () {
        return {
          companyId: AdminUsersService.id,
          branchId: AdminUsersService.record.id
        };
      },
      // Company params
      company: function () {
        return {
          companyId: AdminUsersService.id
        };
      },
      // User params
      user: function () {
        return {
          companyId: AdminUsersService.record.company.id,
          branchId: AdminUsersService.record.branch.id,
          employeeId: AdminUsersService.id
        };
      },
      // Appraiser params
      appraiser: function () {
        return {
          appraiserId: AdminUsersService.id
        };
      },
      // AMC
      amc: function () {
        return {
          amcId: AdminUsersService.id
        };
      },
      // GET request for multiple users at once
      massUserGet: function () {
        return {
          companyId: AdminUsersService.id,
          branchId: AdminUsersService.record.branchId,
          employeeId: AdminUsersService.record.massUpdateUsers.join(',')
        };
      },
      // update (POST, PATCH, PUT) requests for multiple users
      massUserUpdate: function () {
        return {
          companyId: AdminUsersService.id,
          branchId: AdminUsersService.record.branchId
        };
      },
      /**
       * Set employees as headers for mass update
       */
      massUpdateHeaders: function () {
        return {employees: AdminUsersService.record.massUpdateUsers};
      },
      /**
       * Appraiser manager
       */
      appraiserManager: function () {
        return {
          companyId: AdminUsersService.record.company.id,
          branchId: AdminUsersService.record.branch.id,
          managerId: AdminUsersService.id
        };
      },
      /**
       * Appraiser company
       */
      appraiserCompany: function () {
        return {companyId: AdminUsersService.id};
      }
    },
    /**
     * Client sub-view
     */
    client: {
      params: {
        // Company params
        company: function () {
          return {
            companyId: AdminUsersService.id
          };
        },
        // User params
        user: function () {
          return {
            companyId: AdminUsersService.record.company.id,
            branchId: AdminUsersService.record.branch.id,
            employeeId: AdminUsersService.id
          };
        },
        // When overriding the default record
        userOverride: function (user) {
          return {
            companyId: user.company.id,
            branchId: user.branch,
            employeeId: user.id
          };
        }
      },
      // Standard request URLs
      requests: {
        user: '/v2.0/client/companies/:companyId/branches/:branchId/employees/:employeeId/',
        company: '/v2.0/client/companies/:companyId/'
      },
      /**
       * Initial data load
       */
      init: function (params) {
        return getRequest('/v2.0/client/members', params);
      },
      /**
       * Delete a company
       */
      deleteCompany: function () {
        return deleteRequest(Service.client.requests.company, Service.client.params.company());
      },
      /**
       * Delete a user
       */
      deleteUser: function () {
        return deleteRequest(Service.client.requests.user, Service.client.params.user());
      },
      /**
       * Email sign up link
       */
      emailSignUp: function (body) {
        return postArray(Service.client.requests.company + 'email-signup-links', Service.client.params.company(), body);
      },
      /**
       * Get following for user
       */
      following: function () {
        return getRequest(Service.client.requests.user + 'followings/internal', Service.client.params.user());
      },
      /**
       * Retrieve company statistics
       */
      companyScorecard: function () {
        return getRequest(Service.requests.company + 'scorecard', Service.client.params.company());
      },
      /**
       * Quickbooks sync
       */
      quickbooksSync: function () {
        return post(Service.client.requests.company + 'qb-sync', Service.client.params.company());
      },
      /**
       * Send login details to employee
       */
      sendLoginDetails: function (params) {
        return post(Service.client.requests.user + 'send-login-details', Service.client.params.user(), {}, params);
      },
      /**
       * Change active status for user
       */
      changeActiveStatus: function (body) {
        return patch(Service.client.requests.user, Service.client.params.user(), body);
      },
      /**
       * Retrieve client user instructions
       */
      userSettingsInstructions: function (type, user) {
        return getRequest(Service.requests[type] + 'instructions',
        angular.isDefined(user) ? Service.params.userOverride(user) : Service.params[type]());
      },
      /**
       * Retrieve approved appraisers list
       */
      userSettingsApprovedAppraisers: function (type, user) {
        return getRequest(Service.requests[type] + 'appraiser-lists/approved',
          angular.isDefined(user) ? Service.params.userOverride(user) : Service.params[type]());
      },
      /**
       * Retrieve do not use appraisers list
       */
      userSettingsDoNotUseAppraisers: function (type, user) {
        return getRequest(Service.requests[type] + 'appraiser-lists/do-not-use',
          angular.isDefined(user) ? Service.params.userOverride(user) : Service.params[type]());
      },
      /**
       * Fee schedule
       */
      userSettingsJobTypes: function (type, user) {
        return getRequest(Service.client.requests[type] + 'jobtypes-list',
          angular.isDefined(user) ? Service.client.params.userOverride(user) : Service.client.params[type]());
      },
      /**
       * User settings checklist
       */
      userSettingsChecklist: function (type, user) {
        return getRequest(Service.client.requests[type] + 'checklist',
          angular.isDefined(user) ? Service.client.params.userOverride(user) : Service.client.params[type]());
      },
      /**
       * Checklist questions
       */
      userSettingsChecklistQuestions: function (type, params) {
        return getRequest(Service.checklist.requests.general + params.id + '/questions');
      },
      /**
       * Client employee client on report
       */
      userSettingsClientsOnReport: function (type, user) {
        return getRequest(Service.client.requests[type] + 'clients-on-report',
          angular.isDefined(user) ? Service.client.params.userOverride(user) : Service.client.params[type]());
      },
      /**
       * Retrieve settings object
       */
      userSettingsSettings: function (type, user) {
        return getRequest(Service.client.requests[type] + 'settings',
          angular.isDefined(user) ? Service.client.params.userOverride(user) : Service.client.params[type]());
      },
      /**
       * Get permissions
       *
       * @todo Not defined yet
       */
      permissions: function (type) {}, // jshint ignore:line
      /**
       * Create a new company
       * @param params Object
       * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Companies#standard-client-company-persistable-object
       */
      newCompany: function (params) {
        return post(Service.client.requests.company, {}, params);
      },
      /**
       * Write company details
       */
      companyUpdateDetails: function (params) {
        return put(Service.requests.company, Service.params.company(), params);
      },
      /**
       * Write user details
       */
      userUpdateDetails: function (params) {
        return put(Service.requests.user, Service.params.user(), params);
      },
      /**
       * Retrieve user settings summary
       */
      getUserSettingsSummary: function (type, user) {
        return getRequest(Service.client.requests[type] + 'settings',
        angular.isDefined(user) ? Service.client.params.userOverride(user) : Service.client.params[type]());
      }
    },
    /**
     * Notes reusable component
     */
    notes: {
      params: {
        // Branch params
        branch: function () {
          return {
            companyId: AdminUsersService.id,
            branchId: AdminUsersService.record.id
          };
        },
        // Company params
        company: function () {
          return {
            companyId: AdminUsersService.id
          };
        },
        // User params
        user: function () {
          return {
            companyId: AdminUsersService.record.company.id,
            branchId: AdminUsersService.record.branch.id,
            employeeId: AdminUsersService.id
          };
        },
        // Appraiser params
        appraiser: function () {
          return {
            appraiserId: AdminUsersService.id
          };
        },
        // AMC
        amc: function () {
          return {
            amcId: AdminUsersService.id
          };
        }
      },
      // Standard request URLs
      requests: {
        branch: '/v2.0/client/companies/:companyId/branches/:branchId/notes/:noteId/',
        company: '/v2.0/client/companies/:companyId/notes/:noteId/',
        appraiser: '/v2.0/appraisement/appraisers/:appraiserId/notes/:noteId/',
        user: '/v2.0/client/companies/:companyId/branches/:branchId/employees/:employeeId/notes/:noteId/',
        amc: '/v2.0/appraisement/amcs/:amcId/notes/:noteId/'
      },
      /**
       * Load notes
       */
      init: function (type) {
        return getRequest(Service.notes.requests[type], Service.notes.params[type]());
      },
      /**
       * Delete a note
       * @param type Request type
       * @param urlParams Note ID
       */
      deleteNote: function (type, urlParams) {
        return deleteRequest(Service.notes.requests[type], _.assign(Service.notes.params[type](), urlParams));
      },
      /**
       * Update note (patch for isDisplayed, put for content)
       * @param type Request type
       * @param urlParams Note ID
       * @param bodyParams Note text
       */
      updateNote: function (type, urlParams, bodyParams) {
        return put(Service.notes.requests[type], _.assign(Service.notes.params[type](), urlParams),
        _.assign({editorId: SessionService.id}, bodyParams));
      },
      /**
       * Create a new note
       * @param type
       * @param urlParams
       * @param bodyParams
       */
      newNote: function (type, urlParams, bodyParams) {
        return post(Service.notes.requests[type], Service.notes.params[type](),
        _.assign(bodyParams, {authorId: SessionService.id}));
      }
    },
    /**
     * Client on report reusable component
     */
    clientOnReport: {
      params: {
        // Company params
        company: function () {
          return {
            companyId: AdminUsersService.id
          };
        },
        // User params
        user: function () {
          return {
            companyId: AdminUsersService.record.company.id,
            branchId: AdminUsersService.record.branch.id,
            employeeId: AdminUsersService.id
          };
        },
        // Branch params
        branch: function () {
          return {
            companyId: AdminUsersService.id,
            branchId: AdminUsersService.record.id
          };
        }
      },
      // Standard request URLs
      requests: {
        company: '/v2.0/client/companies/:companyId/clients-on-report/:reportId/',
        user: '/v2.0/client/companies/:companyId/branches/:branchId/employees/:employeeId/clients-on-report/:reportId/',
        branch: '/v2.0/client/companies/:companyId/branches/:branchId/clients-on-report/:reportId/'
      },
      /**
       * Load notes
       */
      init: function (type) {
        // Don't get assigned values on mass update
        if (AdminUsersService.record.massUpdateUsers) {
          return $q(function (resolve) {
            resolve({});
          });
        }
        return getRequest(Service.clientOnReport.requests[type], Service.clientOnReport.params[type]());
      },
      /**
       * Adds a client on report
       */
      addClient: function (type, params) {
        return put(Service.clientOnReport.requests[type], Service.clientOnReport.params[type](), params);
      },
      /**
       * Adds a client on report for multiple users
       */
      addClientMass: function (type, params) {
        return put(Service.requests.user + 'clients-on-report', Service.params.massUserUpdate(), params,
        Service.params.massUpdateHeaders());
      },
      /**
       * Delete a client on report
       */
      removeClient: function (type, params) {
        return deleteRequest(Service.requests[type] + 'clients-on-report/' + params.company, Service.clientOnReport.params[type]());
      },
      /**
       * Delete a client on report for multiple users
       *
       * @todo Not defined in API manual
       */
      removeClientMass: function (type, params) {
        return deleteRequest(Service.requests.user + 'clients-on-report/' + params.company, Service.params.massUserUpdate(),
        Service.params.massUpdateHeaders());
      },
      /**
       * Update client on report
       */
      updateClient: function (type, client) {
        return put(Service.clientOnReport.requests[type], Service.clientOnReport.params[type](), client);
      },
      /**
       * Update client on report for multiple users
       */
      updateClientMass: function (type, client) {
        return put(Service.requests.user + 'clients-on-report', Service.params.massUserUpdate(), client,
        Service.params.massUpdateHeaders());
      }
    },
    /**
     * Appraiser panel
     */
    appraiserPanel: {
      params: {
        // Company params
        company: function () {
          return {
            companyId: AdminUsersService.id
          };
        },
        // User params
        user: function () {
          return {
            companyId: AdminUsersService.record.company.id,
            branchId: AdminUsersService.record.branch.id,
            employeeId: AdminUsersService.id
          };
        },
        // Branch params
        branch: function () {
          return {
            companyId: AdminUsersService.id,
            branchId: AdminUsersService.record.id
          };
        }
      },
      // Standard request URLs
      requests: {
        company: '/v2.0/client/companies/:companyId/appraiser-lists/',
        user: '/v2.0/client/companies/:companyId/branches/:branchId/employees/:employeeId/appraiser-lists/',
        branch: '/v2.0/client/companies/:companyId/branches/:branchId/appraiser-lists/'
      },
      /**
       * Appraiser panel view
       */
      /**
       * Data load for appraiser list view
       */
      init: function () {
        return getRequest('/v2.0/appraiser-lists/appraisers');
      },
      /**
       * All approved lists
       */
      getApprovedAll: function () {
        return getRequest(Service.requests.appraisersList + 'approved');
      },
      /**
       * Get all do not use lists
       */
      getDoNotUseAll: function () {
        return getRequest(Service.requests.appraisersList + 'do-not-use');
      },
      /**
       * Retrieve appraisers in a list
       */
      getAppraisersInList: function (type, params) {
        return getRequest(Service.requests.appraisersList + params.type + '/' + params.id + '/appraisers');
      },
      /**
       * Add selected appraisers to list
       * @param type
       * @param params
       */
      addToList: function (type, params) {
        return put(Service.requests.appraisersList + params.url.listType + '/' + params.url.listId, null, params.body.ids ? params.body : null);
      },
      /**
       * Add new list
       */
      addNewList: function (type, params) {
        return post(Service.requests.appraisersList + params.type, null, {title: params.title});
      },
      /**
       * Delete list
       */
      deleteList: function (type, params) {
        return deleteRequest(Service.requests.appraisersList + params.type + '/' + params.id);
      },
      /**
       * Delete selected appraisers from list
       */
      deleteAppraisersFromList: function (type, params) {
        var idString = '';
        // If specifying IDs
        if (params.appraiserIds.length) {
          idString = '/ids/' + params.appraiserIds.join(',');
        }
        return deleteRequest(Service.requests.appraisersList + params.listType + '/' + params.listId +
                             '/appraisers' + idString);
      },
      /**
       * Appraiser panel directive
       */
      /**
       * Get approved appraisers
       */
      getApproved: function (type) {
        // Mass update
        if (AdminUsersService.record.massUpdateUsers) {
          // Don't make request for mass update users
          return $q(function (resolve) {
            resolve({});
          });
        }
        // Singular update
        return getRequest(Service.requests[type] + 'appraiser-lists/approved', Service.params[type]());
      },
      /**
       * Get do not use appraisers
       */
      getDoNotUse: function (type) {
        // Mass update
        if (AdminUsersService.record.massUpdateUsers) {
          // Don't make request for mass update users
          return $q(function (resolve) {
            resolve({});
          });
        }
        // Singular update
        return getRequest(Service.requests[type] + 'appraiser-lists/do-not-use', Service.params[type]());
      },
      /**
       * Add a list to approved
       */
      addApproved: function (type, body, headers) {
        return put(Service.appraiserPanel.requests[type] + 'approved', Service.appraiserPanel.params[type](), body, headers);
      },
      /**
       * Add do not use list
       */
      addDoNotUse: function (type, body, headers) {
        return put(Service.appraiserPanel.requests[type] + 'do-not-use', Service.appraiserPanel.params[type](), body, headers);
      },
      /**
       * Remove a list from approved
       */
      removeApproved: function (type, id) {
        return deleteRequest(Service.appraiserPanel.requests[type] + 'approved/' + id, Service.appraiserPanel.params[type]());
      },
      /**
       * Remove do not use list
       */
      removeDoNotUse: function (type, id) {
        return deleteRequest(Service.appraiserPanel.requests[type] + 'do-not-use/' + id, Service.appraiserPanel.params[type]());
      },
      /**
       * Add a list to approved for multiple users
       */
      addApprovedMass: function (type, body) {
        return put(Service.requests.user + 'appraiser-lists/approved', Service.params.massUserUpdate(), body,
        Service.params.massUpdateHeaders());
      },
      /**
       * Add do not use list for multiple users
       */
      addDoNotUseMass: function (type, body) {
        return put(Service.requests.user + 'appraiser-lists/do-not-use', Service.params.massUserUpdate(), body,
        Service.params.massUpdateHeaders());
      },
      /**
       * Remove a list from approved for multiple users
       */
      removeApprovedMass: function (type, id) {
        return deleteRequest(Service.requests.user + 'appraiser-lists/approved/' + id, Service.params.massUserUpdate(),
        Service.params.massUpdateHeaders());
      },
      /**
       * Remove do not use list for multiple users
       */
      removeDoNotUseMass: function (type, id) {
        return deleteRequest(Service.requests.user + 'appraiser-lists/do-not-use/' + id,
        Service.params.massUserUpdate(), Service.params.massUpdateHeaders());
      }
    },
    /**
     * Auto assign
     */
    autoAssign: {
      params: {
        // Company params
        company: function () {
          return {
            companyId: AdminUsersService.id
          };
        }
      },
      // Standard request URLs
      requests: {
        company: '/v2.0/client/companies/:companyId/auto-assign/criteria/'
      },
      /**
       * Get approved appraisers
       */
      init: function () {
        return getRequest(Service.autoAssign.requests.company, Service.autoAssign.params.company());
      },
      /**
       * Update table on change
       */
      update: function (body) {
        return put(Service.autoAssign.requests.company, Service.autoAssign.params.company(), body);
      }
    },
    /**
     * Checklist
     */
    checklist: {
      params: {
        // Company params
        company: function () {
          return {
            companyId: AdminUsersService.id
          };
        },
        // User params
        user: function () {
          return {
            companyId: AdminUsersService.record.company.id,
            branchId: AdminUsersService.record.branch.id,
            employeeId: AdminUsersService.id
          };
        },
        // Branch params
        branch: function () {
          return {
            companyId: AdminUsersService.id,
            branchId: AdminUsersService.record.id
          };
        }
      },
      // Standard request URLs
      requests: {
        general: '/v2.0/checklists/',
        company: '/v2.0/client/companies/:companyId/checklist/',
        user: '/v2.0/client/companies/:companyId/branches/:branchId/employees/:employeeId/checklist/',
        branch: '/v2.0/client/companies/:companyId/branches/:branchId/checklist/'
      },
      /**
       * Retrieve checklists
       */
      checklists: function () {
        return getRequest(Service.checklist.requests.general);
      },
      /**
       * Retrieve assigned checklist
       */
      assigned: function (type) {
        // Multiple users
        if (AdminUsersService.record.massUpdateUsers) {
          return $q(function (resolve) {
            resolve({});
          });
        }
        // Single user
        return getRequest(Service.checklist.requests[type], Service.checklist.params[type]());
      },
      /**
       * Retrieve questions for a specific checklist
       * @param type string Type
       * @param id int Checklist ID
       */
      checklistQuestions: function (type, id) {
        return getRequest(Service.checklist.requests.general + id + '/questions');
      },
      /**
       * Delete a checklist question
       * @param type
       * @param params Object Checklist and question ID
       */
      deleteQuestion: function (type, params) {
        return deleteRequest(Service.checklist.requests.general + params.checklistId + '/questions/' + params.questionId);
      },
      /**
       * Delete a checklist
       * @param type
       * @param params Object Checklist ID
       */
      deleteChecklist: function (type, params) {
        return deleteRequest(Service.checklist.requests.general + params.checklistId);
      },
      /**
       * Update a checklist name
       */
      updateChecklistTitle: function (type, params) {
        return put(Service.checklist.requests.general + params.checklistId, {}, {title: params.title});
      },
      /**
       * Update checklist question
       */
      updateChecklistQuestion: function (type, params) {
        return put(Service.checklist.requests.general + params.checklistId + '/questions/' + params.questionId, {},
        {text: params.text});
      },
      /**
       * Assign a checklist
       */
      assignChecklist: function (type, params) {
        // Mass assign
        if (AdminUsersService.record.massUpdateUsers) {
          return put(Service.requests.user, Service.params.massUserUpdate(), {id: params.checklistId},
          Service.params.massUpdateHeaders());
        }
        // Single assign
        return put(Service.checklist.requests[type], Service.checklist.params[type](), {id: params.checklistId});
      },
      /**
       * Clone a checklist
       */
      cloneChecklist: function (type, params) {
        return post(Service.checklist.requests.general + params.checklistId + '/clone', {}, {title: params.title});
      },
      /**
       * New checklist question
       */
      newQuestion: function (type, params) {
        return post(Service.checklist.requests.general + params.checklistId + '/questions', {}, {text: params.text});
      },
      /**
       * New checklist
       */
      newChecklist: function (type, params) {
        return post(Service.checklist.requests.general, {}, {title: params.title});
      }
    },
    /**
     * Company settings
     */
    companySettings: {
      params: {
        // Company params
        company: function () {
          return {
            companyId: AdminUsersService.id
          };
        }
      },
      // Standard request URLs
      requests: {
        company: '/v2.0/client/companies/:companyId/settings/'
      },
      /**
       * Retrieve settings
       */
      getSettings: function () {
        return getRequest(Service.companySettings.requests.company, Service.companySettings.params.company());
      }
    },
    /**
     * Instructions
     */
    instructions: {
      /**
       * Get instructions
       * @param type String
       */
      getInstructions: function (type) {
        return getRequest(Service.requests[type] + 'instructions', Service.params[type]());
      },
      /**
       * Get merge fields
       */
      getMergeFields: function () {
        return getRequest(Service.requests.platform + 'merge-fields');
      },
      /**
       * Get instruction templates
       */
      templates: function () {
        return getRequest(Service.requests.platform + 'instruction-templates');
      },
      /**
       * Retrieve acceptance message
       * @param type
       * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/Instructions/AcceptanceMessage.md#gets-an-acceptance-message
       */
      acceptanceMessage: function (type) {
        return getRequest(Service.requests[type] + 'instructions/acceptance-message', Service.params[type]());
      },
      /**
       * Update acceptance message
       * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/Instructions/AcceptanceMessage.md#updates-an-acceptance-message
       */
      updateAcceptanceMessage: function (type, params) {
        return put(Service.requests[type] + 'instructions/acceptance-message', Service.params[type](), {label: params.label});
      },
      /**
       * Update document title
       * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Client/Employees/Instructions/Documents.md#updates-a-document
       */
      updateDocument: function (type, params) {
        return patch(Service.requests[type] + 'instructions/documents/' + params.url.documentId, Service.params[type](),
        {title: params.body.title});
      },
      /**
       * Delete document
       */
      deleteDocument: function (type, params) {
        return deleteRequest(Service.requests[type] + 'instructions/documents/' + params.documentId, Service.params[type]());
      },
      /**
       * Retrieve upload URL for documents
       * @param type
       */
      getUploadDocumentUrl: function (type) {
        return {url: Service.requests[type] + 'instructions/documents', params: Service.params[type]()};
      },
      /**
       * Edit an existing instruction
       * @param type
       * @param params Instruction
       */
      editInstruction: function (type, params) {
        return patch(Service.requests[type] + 'instructions/' + params.id, Service.params[type](), params);
      },
      /**
       * New instruction
       * @param type
       * @param params Instruction
       */
      newInstruction: function (type, params) {
        return post(Service.requests[type] + 'instructions', Service.params[type](), params);
      },
      /**
       * Delete instruction
       * @param type
       * @param params Instruction Id
       */
      deleteInstruction: function (type, params) {
        return deleteRequest(Service.requests[type] + 'instructions/' + params.instructionId, Service.params[type]());
      },
      /**
       * Get jobtype list
       */
      getJobtypeLists: function () {
        return getRequest('/v2.0/job-type-lists');
      },
      /**
       * Copy instruction to selected users (mass update)
       */
      copyInstruction: function (type, params) {
        return post(Service.requests.branch + 'employees/instructions', Service.params.massUserUpdate(), params,
        Service.params.massUpdateHeaders());
      }
    },
    /**
     * Jobtypes list
     */
    jobTypesList: {
      /**
       * Retrieve all jobtype lists
       */
      getJobtypeLists: function () {
        return getRequest(Service.requests.jobTypeLists);
      },
      /**
       * Retrieve assigned jobtype list
       */
      getAssignedJobtypeList: function (type) {
        // Don't retrieve a default for mass update
        if (AdminUsersService.record.massUpdateUsers) {
          return $q(function (resolve) {
            resolve({});
          });
        }
        return getRequest(Service.requests[type] + 'jobtypes-list', Service.params[type]());
      },
      /**
       * Assign jobs list
       * @param type String
       * @param params Object
       */
      assign: function (type, params) {
        return put(Service.requests[type] + 'jobtypes-list', Service.params[type](), params.body);
      },
      /**
       * Mass update jobs list
       * @param type
       * @param params Body
       *
       * @todo I opened an issue about this endpoint. It might not be correct.
       * @link https://github.com/ascope/manuals/issues/65
       */
      assignMass: function (type, params) {
        return put(Service.requests.user + 'jobtypes-list', Service.params[type](), params.body, Service.params.massUpdateHeaders());
      }
    },
    /**
     * User settings
     */
    userSettings: {
      params: {
        // User params
        user: function () {
          return {
            companyId: AdminUsersService.record.company.id,
            branchId: AdminUsersService.record.branch.id,
            employeeId: AdminUsersService.id
          };
        }
      },
      // Standard request URLs
      requests: {
        user: '/v2.0/client/companies/:companyId/branches/:branchId/employees/:employeeId/settings/'
      },
      /**
       * Retrieve user settings
       */
      getSettings: function () {
        return getRequest(Service.requests.user + 'settings', Service.params.user());
      },
      /**
       * Retrieve user settings for multiple users
       */
      getSettingsMass: function () {
        return $q(function (resolve) {
          resolve({});
        });
      },
      /**
       * Update user settings
       */
      updateSettings: function (type, params) {
        return patch(Service.requests.user + 'settings', Service.params.user(), params.body);
      },
      /**
       * Update user settings for multiple users
       */
      updateSettingsMass: function (type, params) {
        return patch(Service.requests.user + 'settings', Service.params.massUserUpdate(), params.body,
        Service.params.massUpdateHeaders());
      }
    },
    /**
     * Add users
     */
    addUsers: {
      /**
       * Retrieve branches
       */
      getBranches: function (type) {
        // Appraiser company branch
        if (type === 'appraiser-company') {
          return getRequest(Service.requests.appraiserCompanyBranch, Service.params.appraiserCompany());
        }
        // Client company branch
        return getRequest(Service.requests.branch, Service.params.company());
      },
      /**
       * Get branch managers
       */
      getAppraisementMember: function (type, params) {
        return getRequest(Service.requests.appraiserCompany + 'members', _.assign(Service.params.appraiserCompany(), params));
      },
      /**
       * Create a new branch
       */
      newBranch: function (type, body) {
        // New appraiser company branch
        if (type === 'appraiser-company') {
          return post(Service.requests.appraiserCompanyBranch, Service.params.appraiserCompany(), body);
        }
        // New client company branch
        return post(Service.requests.branch, Service.params.company(), body);
      },
      /**
       * Delete user
       *
       * @param type String Request type
       * @param user Object Selected user
       */
      deleteUser: function (type, user) {
        // Appraiser company
        if (AdminUsersService.record._type === 'appraiser-company') {
          // Manager
          if (user.memberType === 'manager') {
            return deleteRequest(Service.requests.appraiserManager, {
              companyId: AdminUsersService.id,
              branchId: user.branch,
              managerId: user.id
            });
            // Appraiser
          } else {
            return deleteRequest(Service.requests.appraiser, {
              appraiserId: user.id
            });
          }
        }
        // Client
        return deleteRequest(Service.requests.user, Service.client.params.userOverride(user));
      },
      /**
       * Add user to branch
       */
      newUser: function (type, params) {
        // Appraiser company
        if (AdminUsersService.record._type === 'appraiser-company') {
          return post(Service.requests.appraiserCompanyBranch + 'managers', {
            companyId: AdminUsersService.id,
            branchId: params.branchId
          }, params.user,
          {notifyManager: params.user.notifyEmployee});
        }
        // Client company
        return post(Service.requests.user, {
          companyId: AdminUsersService.id,
          branchId: params.branchId
        }, params.user,
        {notifyEmployee: params.user.notifyEmployee});
      },
      /**
       * Delete branch
       */
      deleteBranch: function (type, params) {
        // Appraiser company
        if (type === 'appraiser-company') {
          return deleteRequest(Service.requests.appraiserCompanyBranch, {
            companyId: AdminUsersService.id,
            branchId: params.branchId
          });
        }
        // Client company
        return deleteRequest(Service.requests.branch, {companyId: AdminUsersService.id, branchId: params.branchId});
      },
      /**
       * Disable or enable branch
       */
      disableBranch: function (type, params) {
        // Appraiser company
        if (type === 'appraiser-company') {
          return patch(Service.requests.appraiserCompanyBranch, {
            companyId: AdminUsersService.id,
            branchId: params.branchId
          }, {isActive: params.isActive});
        }
        // Client company
        return patch(Service.requests.branch, {
          companyId: AdminUsersService.id,
          branchId: params.branchId
        }, {isActive: params.isActive});
      },
      /**
       * Send login details to selected users
       * @param type
       * @param params Selected users
       */
      sendLogin: function (type, params) {
        return post(Service.requests.user + 'send-login-details', {
          companyId: AdminUsersService.id,
          branchId: params.url.branch
        }, {}, {employeeIds: params.employeeIds});
      }
    },
    /**
     * Add users
     */
    branchSettings: {
      /**
       * Retrieve branch details
       */
      getBranch: function (type, params) {
        return getRequest(Service.requests.branch, {companyId: AdminUsersService.id, branchId: params.branchId});
      },
      /**
       * Update branch
       */
      updateBranch: function (type, params) {
        return put(Service.requests.branch, Service.params.branch(), params);
      },
      /**
       * Retrieve branch settings
       */
      branchSettings: function () {
        return getRequest(Service.requests.branch + 'settings', Service.params.branch());
      },
      /**
       * Update branch settings
       */
      updateBranchSettings: function (type, params) {
        return patch(Service.requests.branch + 'settings', Service.params.branch(), params);
      }
    },
    /**
     * Mass update users
     */
    massUpdateUsers: {
      /**
       * User details (address and company)
       *
       * @todo The endpoint was not available when I made this. It's just a best guess -- Logan 6/19/15
       */
      updateDetails: function (type, params) {
        return put(Service.requests.user, Service.params.massUserUpdate(), params.body, Service.params.massUpdateHeaders());
      },
      /**
       * Retrieve branches
       */
      getBranches: function () {
        return getRequest(Service.requests.branch, {companyId: AdminUsersService.id});
      }
    },
    /**
     * Appraiser section
     */
    appraiser: {
      /**
       * Retrieve appraisement members on load (or just appraisers for appraiser directory)
       */
      init: function (params, type) {
        // Filtering for disabled view
        if (angular.isString(params) && params.indexOf('filter') !== -1) {
          params = '?' + params;
        } else {
          params = null;
        }
        // Appraiser directory
        if (type === 'appraiser-directory' || params) {
          return getRequest('/v2.0/appraisement/appraisers' + params);
        }
        // Appraiser view
        return getRequest('/v2.0/appraisement/members');
      },
      /**
       * Update appraiser details
       */
      appraiserUpdateDetails: function (params) {
        return put(Service.requests.appraiser, Service.params.appraiser(), params);
      },
      /**
       * Update manager details
       */
      managerUpdateDetails: function (params) {
        return put(Service.requests.appraiserManager, Service.params.appraiserManager(), params);
      },
      /**
       * Update company details
       */
      appraiserCompanyUpdateDetails: function (params) {
        return put(Service.requests.appraiserCompany, Service.params.appraiserCompany(), params);
      },
      /**
       * Appraiser approved lists
       */
      approvedList: function () {
        return getRequest(Service.requests.appraiser + 'appraiser-lists/approved', Service.params.appraiser());
      },
      /**
       * Appraiser do not use lists
       */
      doNotUseList: function () {
        return getRequest(Service.requests.appraiser + 'appraiser-lists/do-not-use', Service.params.appraiser());
      },
      /**
       * Send login details to selected user
       *
       * @todo I need to check on the progress of this. -- 8/10
       * @link https://github.com/ascope/manuals/issues/81
       */
      sendLoginDetails: function () {
        return post(Service.requests.appraiser + 'send-login-details', Service.params.appraiser());
      },
      /**
       * General patch request
       * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/Index.md#patch-appraisementappraisersappraiserid
       */
      update: function (params) {
        return patch(Service.requests.appraiser, Service.params.appraiser(), params);
      },
      /**
       * Create new appraiser company
       *
       * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Companies/Index.md#post-appraisementcompanies
       */
      newCompany: function (params) {
        return post(Service.requests.appraiserCompany, {}, params);
      },
      /**
      * Get certificates
      * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/Certificates/Inedx.md#gets-all-certificates
      */
      certificates: function () {
        return getRequest(Service.requests.appraiser + 'certificates', Service.params.appraiser());
      },
      /**
       * Appraiser ratings
       *
       */
      ratings: function () {
        return getRequest(Service.requests.appraiser + 'ratings', Service.params.appraiser());
      },
      /**
       * Email license doc
       * @param type
       * @param params Certificate ID and email recipient
       */
      emailLicenseDoc: function (type, params) {
        return post(Service.requests.appraiser + 'certificates/:certificateId/email',
        _.assign(Service.params.appraiser(), params.params), params.body);
      },
      /**
       * Appraiser statistics
       */
      statistics: function () {
        return getRequest(Service.requests.appraiser + 'statistics', Service.params.appraiser());
      },
      /**
       * Get appraisals from one of the queues
       */
      queryAppraisalQueue: function (queueType) {
        return getRequest('/v2.0/appraisals/queues/' + queueType + '?filter[assignment][assignee][id]=' + AdminUsersService.id);
      },
      /**
       * Retrieve appraisals from standard /appraisals endpoint
       * @param filter Properly formatted filter
       * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Use%20Cases/Appraisals/Appraisers.md#appraisers
       * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisals/Index.md
       */
      queryAppraisals: function (filter) {
        return getRequest('/v2.0/appraisals?filter' + filter.filter + '=' + filter.filterArg +
                          '&filter[assignment][assignee][id]=' + AdminUsersService.id);
      },
      /**
       * Submit w9
       */
      updateW9: function (params) {
        return post(Service.requests.appraiser + 'w9', Service.params.appraiser(), params);
      }
    },
    /**
     * Coverage
     * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/Certificates/Inedx.md
     */
    coverage: {
      /**
       * Get certifications
       */
      getCertifications: function (type) {
        // Get certificates if they're currently available
        var certificates = AdminUsersService.record.certificates;
        if (angular.isObject(certificates) && Object.getPrototypeOf(certificates) === Object.prototype &&
            Object.keys(certificates).length) {
          return $q(function (resolve) {
            resolve({data: AdminUsersService.record.certificates});
          });
        } else {
          return getRequest(Service.requests[type] + 'certificates', Service.params[type]());
        }
      },
      /**
       * Delete certificates
       * @param type
       * @param params Certificate ID
       */
      deleteCoverage: function (type, params) {
        return deleteRequest(Service.requests[type] + 'certificates/' + params.certificateId, Service.params[type]());
      },
      /**
       * Get counties for a state
       * @param type
       * @param params State
       *
       * @todo This is not the correct endpoint
       * @link https://github.com/ascope/manuals/issues/97
       */
      getCounties: function (type, params) {
        return getRequest(Service.requests.location + 'states/' + params.state + '/counties');
      },
      /**
       * Retrieve zips for selected counties
       */
      getZips: function (type, counties) {
        var filter = '', countyLength = counties.length, i;
        // Create filter parameters
        for (i = 0; i < countyLength; i = i + 1) {
          filter = filter + (i === 0 ? '?filter[county]=' + counties[i] : '&filter[county]=' + counties[i]);
        }
        return getRequest(Service.requests.location + 'zips' + filter);
      },
      /**
       * Update certificate
       * @param type
       * @param params Certificate ID, certificate info
       */
      updateCertificate: function (type, params) {
        return put(Service.requests[type] + 'certificates/' + params.url.certificateId, Service.params[type](), params.body);
      },
      /**
       * New certificate
       * @param type
       * @param params Certificate ID, certificate info
       */
      newCertificate: function (type, params) {
        return post(Service.requests[type] + 'certificates', Service.params[type](), params.body);
      }
    },
    /**
     * Appraiser/appraiser company settings
     */
    appraisementSettings: {
      /**
       * Get settings
       */
      getSettings: function (type) {
        return getRequest(Service.requests[type] + 'settings', Service.params[type]());
      },
      /**
       * Update settings
       */
      updateSettings: function (type, params) {
        return patch(Service.requests[type] + 'settings', Service.params[type](), params);
      }
    },
    /**
     * Appraiser additional docs
     * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Appraisers/Documents/Index.md
     */
    appraiserDocs: {
      /**
       * Get documents on load
       */
      getDocuments: function (type) {
        return getRequest(Service.requests[type] + 'documents', Service.params[type]());
      },
      /**
       * Delete document
       */
      deleteDocument: function (type, params) {
        return deleteRequest(Service.requests[type] + 'documents/' + params.id, Service.params[type]());
      }
    },
    /**
     * ACH Information
     * @link https://github.com/ascope/manuals/blob/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Companies/Ach/Index.md
     */
    ach: {
      /**
       * Get ACH Information
       */
      getAchInformation: function () {
        return getRequest(Service.requests.appraiserCompany + 'ach', Service.params.appraiserCompany());
      },
      /**
       * Update ACH information
       * @param type
       * @param params
       */
      updateAchInformation: function (type, params) {
        return put(Service.requests.appraiserCompany + 'ach', Service.params.appraiserCompany(), params);
      }
    },
    /**
     * AMC view requests
     */
    amc: {
      // Load AMC records
      init: function () {
        return getRequest(Service.requests.amc, Service.params.amc());
      },
      // Ratings
      ratings: function () {
        return getRequest(Service.requests.amc + 'ratings', Service.params.amc());
      },
      /**
       * Retrieve certificates for this AMC
       */
      certificates: function () {
        return getRequest(Service.requests.amc + 'certificates', Service.params.amc());
      },
      /**
       * Email license doc
       */
      emailLicenseDoc: function (type, params) {
        return post(Service.requests.amc + 'certificates/' + params.params.certificateId + '/email',
        Service.params.amc(), params.body);
      },
      /**
       * Get AMC history
       * @todo The query parameters for this are unclear
       * @link https://github.com/ascope/manuals/issues/465
       */
      history: function () {
        return getRequest(Service.requests.appraisals + '?filter[completedAtRange][to]=' + new Date().toISOString() +
                          '&filter[client][id]=' + AdminUsersService.record.id);
      },
      /**
       * Get AMC settings
       */
      settings: function () {
        return getRequest(Service.requests.amc + 'settings', Service.params.amc());
      },
      /**
       * Update AMC settings
       */
      updateSettings: function (type, params) {
        return patch(Service.requests.amc + 'settings', Service.params.amc(), params);
      }
    },
    /**
     * Pending users view
     */
    pendingUsers: {
      /**
       * Load pending users
       *
       * @todo These may be incorrect
       * @link https://github.com/ascope/manuals/issues/171
       */
      init: function (type, params) {
        return getRequest(Service.requests[params] + '?filter[isPending]=true');
      },
      /**
       * Approve an appraiser or company
       */
      approve: function (type, params) {
        // Approve appraiser
        if (params.type === 'appraiser') {
          return patch(Service.requests.appraiser, {appraiserId: params.id}, {isActive: true, isPending: false});
          // Approve company
        } else if (params.type === 'company') {
          return patch(Service.requests.company, {companyId: params.id}, {isPending: false});
        }
      },
      /**
       * Disable appraiser
       */
      disable: function (type, params) {
        return patch(Service.requests.appraiser, {appraiserId: params.id}, {isActive: false, isPending: false});
      },
      /**
       * Delete company
       */
      deleteCompany: function (type, params) {
        return deleteRequest(Service.requests.company, {companyId: params.id});
      }
    },
    /**
     * Email users view
     */
    emails: {
      /**
       * Retrieve emails on load
       */
      init: function () {
        return getRequest(Service.requests.email);
      },
      /**
       * Retrieve recipient list
       */
      getRecipientList: function () {
        var recipients = {client: [], appraiser: []};
        // Get client members
        return getRequest('/v2.0/client/members').then(function (response) {
          recipients.client = response.data;
          // Get appraiser members
          return getRequest('/v2.0/appraisement/members').then(function (response) {
            recipients.appraiser = response.data;
            // Return complete list
            return recipients;
          });
        });
      },
      /**
       * Send an email
       */
      sendEmail: function (type, params) {
        return post(Service.requests.email, null, params);
      }
    },
    /**
     * This is what is listed in v1 as "Appraisal forms"
     */
    jobTypes: {
      /**
       * Retrieve all job types, both selected and unselected
       * @todo This is not implemented yet, so the filter may be incorrect
       * @link https://github.com/ascope/manuals/issues/188
       */
      getAllJobTypes: function () {
        return getRequest('/v2.0/job-types?filter[appraiser][id]=' + AdminUsersService.id);
      },
      /**
       * Retrieve job types selected by the current appraiser
       * @param type
       */
      jobSelectedJobTypes: function (type) {
        // Appraiser view
        return getRequest(Service.requests[type] + 'job-types', Service.params[type]());
      },
      /**
       * Update job type items
       */
      updateJobType: function (type, params) {
        return put(Service.requests[type] + 'job-type/' + params.url.jobTypeId, Service.params[type](), params.body);
      },
      /**
       * Add new job type
       */
      addJobType: function (type, params) {
        return post(Service.requests[type] + 'job-types', Service.params[type](), params);
      },
      /**
       * Delete job type
       */
      deleteJobType: function (type, params) {
        return deleteRequest(Service.requests[type] + 'job-type/' + params.jobTypeId, Service.params[type]());
      }
    }
  };
  return Service;
}]);
