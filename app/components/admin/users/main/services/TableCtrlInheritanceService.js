'use strict';

var app = angular.module('frontendApp');

/**
 * Inherit common table controller functions for each sub-view main search table
 */
app.factory('AdminUsersTableCtrlInheritanceService', ['AdminUsersService', function (AdminUsersService) {
  return {
    // Inherit
    inherit: function ($scope, Service, heading, type, loadParams) {
      var vm = this;
      // Safe data
      vm.safeData = Service.safeData;
      /**
       * @todo I need to remove this ASAP
       */
      if (!angular.isObject(heading)) {
        loadParams = type;
        type = heading;
        vm.heading = vm.safeData.heading;
      } else {
        // Set table heading
        vm.safeData.heading = vm.heading = heading;
      }

      /**
       * Init data load
       */
      vm.init = function () {
        if ($scope.recompile) {
          return;
        }
        Service.loadRecords(loadParams);
      };

      /**
       * Watch table data
       */
      $scope.$watchCollection(function () {
        return Service.displayData[type];
      }, function (newVal) {
        if (!angular.isArray(newVal)) {
          return;
        }
        // Table data
        vm.tableData = newVal;
        vm.rowData = vm.tableData.slice();
      });

      /**
       * Show record details
       */
      vm.rowFn = function (id) {
        // Detach watcher
        if (angular.isFunction(AdminUsersService.detachUpdateWatcher)) {
          AdminUsersService.detachUpdateWatcher();
        }
        // Get details
        Service.getRecordDetails(id).then(function () {
          // Attach update watcher once the record is retrieved
          AdminUsersService.attachUpdateWatcher.call(vm, $scope, Service);
          // Compile any directives
          if (angular.isFunction($scope.compileDirectives)) {
            $scope.compileDirectives();
          }
        });
      };

      // Init data loading
      vm.init();
    }
  };
}]);
