'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users config service
 */
app.factory('AdminUsersConfigService', [function () {
  var AdminUsersConfigService = {
    /**
     * Tabs for the details pane of the client view
     * @returns {{name: string, tab: string}[]}
     */
    getClientDetailsPaneTabs: function () {
      return [
        {name: 'details', tab: 'details'},
        {name: 'notes', tab: 'notes'},
        {name: 'client on report', tab: 'clientOnReport'},
        {name: 'appraiser panel', tab: 'appraiserPanel'},
        {name: 'auto assign', tab: 'autoAssign'},
        {name: 'add checklist', tab: 'addChecklist'},
        {name: 'add users', tab: 'addUsers'},
        {name: 'company settings', tab: 'companySettings'},
        {name: 'instructions', tab: 'instructions'},
        {name: 'fee schedule', tab: 'feeSchedule'}
      ];
    }
  };

  return AdminUsersConfigService;
}]);