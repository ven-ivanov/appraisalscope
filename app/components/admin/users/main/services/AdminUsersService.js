'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users base data service
 */
app.factory('AdminUsersService',
['$http', 'API_PREFIX', 'HandbookService', 'HANDBOOK_CATEGORIES',
 'DomLoadService', '$injector', '$q', '$resource', '$timeout', 'REQUEST_DELAY', 'AsDataService',
 'AsUrlService', function (
$http, API_PREFIX, HandbookService, HANDBOOK_CATEGORIES, DomLoadService, $injector, $q,
$resource, $timeout, REQUEST_DELAY, AsDataService, AsUrlService) {
  var Service = {
    // Branch currently being worked with
    branchId: 0,
    // For holding safe data when mixing sections (such as viewing user details from within company view)
    safeData: {
      user: {},
      // Cache tabs once they're retrieved
      tabs: [
        {name: 'client', tab: 'client', page: 1, recordId: 1},
        {name: 'appraiser', tab: 'appraiser', page: 1, recordId: 1},
        {name: 'amc', tab: 'amc', page: 1, recordId: 1},
        {name: 'appraiser directory', tab: 'appraiser-directory', page: 1, recordId: 1},
        {name: 'amc directory', tab: 'amc-directory', page: 1, recordId: 1},
        {name: 'asc', tab: 'asc', page: 1, recordId: 1},
        {name: 'disabled', tab: 'disabled', page: 1, recordId: 1},
        {name: 'appraiser panel', tab: 'appraiser-panel', page: 1, recordId: 1},
        {name: 'pending users', tab: 'pending-users', page: 1, recordId: 1},
        {name: 'email users', tab: 'email-users', page: 1, recordId: 1}
      ]
    },
    displayData: {
      tabs: []
    },
    /**
     * Watch for changes to the current record
     */
    detachUpdateWatcher: '',
    attachUpdateWatcher: function ($scope, service) {
      var vm = this;
      // Update appraiser info when typing
      Service.detachUpdateWatcher = $scope.$watch(function () {
        return service.displayData.details;
      }, function (newVal, oldVal) {
        if (angular.isUndefined(newVal) || angular.isUndefined(oldVal) || angular.equals(newVal, oldVal) ||
            !Object.keys(oldVal).length || !Service.record) {
          return;
        }
        if (vm.typing) {
          $timeout.cancel(vm.typing);
        }
        // Wait and then make the request
        vm.typing = $timeout(function () {
          try {
            var displayRecord = angular.copy(newVal);
            // Undo transformations
            service.transformData(displayRecord, true);
            // Write to backend
            service.updateRecord(displayRecord);
            vm.typing = null;
          } catch (e) {
            console.log('Could not undo transformations for writing to the backend');
            console.log(e.stack);
          }
        }, REQUEST_DELAY.ms);
      }, true);
    },
    /**
     * Current tab for additional details pane
     */
    currentDetailsTab: '',
    /**
     * Yes no dropdown values
     */
    yesNoDropdown: [{id: false, value: 'No'}, {id: true, value: 'Yes'}],
    /**
     * Retrieve count for each tab
     * @param stateParams $stateParams
     *
     * @todo
     */
    getTabValues: function (stateParams) {
      // Use cached value
      if (Object.keys(Service.displayData.tabs).length) {
        return Service.displayData.tabs;
      }
      // Make request
      $http.get(API_PREFIX() + '/v2.0/admin/users/tab-values').then(function (response) {
        Lazy(Service.safeData.tabs)
        .each(function (tab) {
          // Get tab values
          tab.value = response.data[tab.tab];
          // Keep any params passed in via $stateParams
          if (tab.tab === stateParams.activeTab) {
            tab.recordId = stateParams.recordId;
            tab.page = stateParams.page;
          }
        });
        // Set tabs display data
        Service.displayData.tabs = angular.copy(Service.safeData.tabs);
      });
    },
    /**
     * Keep reference to which record is selected when switching tabs.
     *
     * If client 1 is being examined, and then switch to appraiser tab, when switching back to client, load 1
     */
    persistUrlsOnRecordChange: function (recordId, type) {
      Lazy(Service.displayData.tabs)
      .each(function (tab) {
        if (tab.tab === type) {
          tab.recordId = recordId;
        }
      });
    },
    /**
     * Set active class on active tab
     * @returns {string}
     */
    getTabClass: function (tab, activeTab) {
      if (tab.tab === activeTab) {
        return 'col-md-1 active';
      }
      return 'col-md-1';
    },
    /**
     * Retrieve states from handbook
     */
    getStates: function () {
      return HandbookService.query({category: HANDBOOK_CATEGORIES.states}).$promise;
    },
    /**
     * Change URL to maintain state
     */
    changeUrl: AsUrlService.changeUrl,
    /**
     * Create a transformer based on states, with state code as key, state name as value
     */
    getStateTransformer: function () {
      var transformer = {};
      Lazy(Service.states)
      .each(function (state) {
        transformer[state.code] = state.name;
      });
      return transformer;
    },
    /**
     * Get record details
     */
    getRecordDetails: function (type, callback, recordId) {
      var service = this;
      // Variable argument length
      if (angular.isUndefined(recordId) && (angular.isNumber(callback) || !isNaN(parseInt(callback)))) {
        recordId = callback;
        callback = null;
      }
      // Ensure that transform data is defined
      if (!angular.isFunction(service.transformData)) {
        throw Error('getRecordDetails requires transformData to be defined');
      }
      return $q(function (resolve) {
        var state = {};
        // Set state based on type
        state.recordId = recordId;
        // Keep reference to inspector ID
        service[type + 'Id'] = recordId;
        // Keep ID on Service
        Service.id = recordId;
        // Store records -- This is likely redundant and will be refactored out
        Service.record = angular.copy(service.safeData[type][recordId]);
        // If the selected record doesn't exist, revert to record 1
        if (angular.isUndefined(Service.record)) {
          state.recordId = 1;
          var keys = Object.keys(service.safeData[type]);
          // if we have no records, return
          if (!keys.length) {
            return resolve();
          }
          Service.record = angular.copy(service.safeData[type][keys[0]]);
        }
        // Keep type, if it's already been defined
        Service.record._type =
        angular.isDefined(Service.record._type) ? Service.record._type : type;
        // Change URL
        Service.changeUrl(state);
        // Keep reference to record on tab change
        Service.persistUrlsOnRecordChange(recordId, type);
        // Don't continue if the record doesn't exist
        if (angular.isUndefined(service.safeData[type]) || angular.isUndefined(service.safeData[type][recordId])) {
          state[type] = '';
          // Reset URL
          return Service.changeUrl(state);
        }
        // Retrieve appraiser
        var record = JSON.parse(JSON.stringify(service.safeData[type][recordId]));
        // Transform as necessary
        service.transformData(record);
        // Set to details
        service.displayData.details = record;
        // Callback
        if (callback) {
          var response = callback();
          // Async callback
          if (angular.isDefined(response) && angular.isDefined(response.then)) {
            return response.then(function () {
              resolve();
            });
          }
        }
        resolve();
      });
    },
    /**
     * Set state on load
     */
    setStateOnLoad: function (type, stateParams, ctrl) {
      var service = this;
      // Get states
      Service.getStates().then(function (response) {
        service.displayData.states = response.data;
      });
      // Make sure we have actual params to parse
      if (!angular.isObject(stateParams)) {
        return;
      }
      return DomLoadService.load().then(function () {
        // Load appraiser
        if ('recordId' in stateParams && stateParams.recordId) {
          return service.getRecordDetails(stateParams.recordId)
          .then(function () {
            // Display details pane
            if ('details' in stateParams && stateParams.details !== 'details') {
              // Details pane
              Service.currentDetailsTab = stateParams.details;
              // Show details
              ctrl.showAdditionalDetails(type);
              // Select correct tab
              ctrl.selectedAdditionalDetailsTab = stateParams.details;
            }
          });
        }
      });
    },
    /**
     * Toggle appraiser active or vacation
     */
    toggleSetting: function (type, property) {
      var service = this, param = {}, record;
      // In case I overlooked any usages when modularizing this
      property = property || 'isActive';
      // Get record
      record = service.safeData[type][Service.id];
      // Toggle property
      record[property] = !record[property];
      // Update display data
      service.displayData.details[property] = record[property];
      // Create request param
      param[property] = record[property];
      return service.request('update', param);
    },
    /**
     * Send login credentials
     */
    sendLogin: function () {
      var service = this;
      return service.request('sendLoginDetails');
    },
    /**
     * Retrieve ratings summary for the selected record
     */
    getRatings: function (type, createTabsFn, heading) {
      var service = this, record = service.safeData[type][Service.id];
      if (angular.isUndefined(service.displayData.ratingTabs) || angular.isUndefined(record)) {
        return;
      }
      // Create ratings tabs
      Service.createRatingsTabs(service, record);
      // Get ratings
      return service.request('ratings')
      .then(function (response) {
        // Format table data
        Service.formatRatings(response);
        // Hash and display
        service.hashData(response.data, 'ratings');
        AsDataService.formatData.call(service, 'ratings', heading);
      });
    },
    /**
     * Format ratings tabs
     * @param service
     * @param record
     */
    createRatingsTabs: function (service, record) {
      // Apply rating values
      Lazy(record.ratingSummary)
      .each(function (rating, key) {
        if (angular.isDefined(service.displayData.ratingTabs[key])) {
          service.displayData.ratingTabs[key] =
          service.displayData.ratingTabs[key].replace('N/A', rating);
        }
      });
    },
    /**
     * Format ratings for display in table
     * @param response
     */
    formatRatings: function (response) {
      var quality = 0, service = 0, turnAroundTime = 0;
      // Create properties for display
      Lazy(response.data)
      .each(function (rating) {
        rating.borrowerName =
        rating.appraisal.property.contacts.borrower.firstName + ' ' + rating.appraisal.property.contacts.borrower.lastName;
        rating.streetAddress = rating.appraisal.property.location.address1;
        rating.dateOrdered = rating.appraisal.createdAt;
        rating.dateCompleted = rating.appraisal.completedAt;
        quality = quality + rating.quality;
        service = service + rating.service;
        turnAroundTime = turnAroundTime + rating.turnAroundTime;
      });
      // Get averages for each type of rating
      quality = Math.round(quality / response.data.length);
      service = Math.round(service / response.data.length);
      turnAroundTime = Math.round(turnAroundTime / response.data.length);
      // Add in totals
      response.data.push({borrowerName: 'AVERAGE', quality: quality, service: service, turnAroundTime: turnAroundTime});
    },
    /**
     * Update record
     */
    updateRecord: function (type, details) {
      var ResourceService = $injector.get('AdminUsersResourceService'),
      fn = details._type ? details._type : Service._type;
      // Convert dash to uppercase letter
      if (fn.indexOf('-') !== -1) {
        fn = fn.split('-').map(function (word, key) {
          // Don't capitalize first word
          if (!key) {
            return word;
          }
          // Capitalize all others
          return word.charAt(0).toUpperCase() + word.slice(1);
        }).join('');
      }
      fn = fn + 'UpdateDetails';
      // Write to backend
      return ResourceService[type][fn](details);
    },
    /**
     * Check that we have a valid number being passed in
     */
    checkValidNumber: function (data) {
      var valid = true;
      Lazy(data)
      .each(function (input, key) {
        // Only run on primitive values which are not number
        if (isNaN(Number(input)) && angular.isString(input)) {
          valid = false;
          // Replace with regex
          data[key] = input.replace(/[^\d.]*/g, '');
        }
      });
      return valid;
    },
    /**
     * Certification transformer values
     */
    getCertificationTransformer: function () {
      return {
        0: '',
        1: 'Licensed',
        2: 'Certified',
        3: 'General'
      };
    },
    /**
     * Determine service for initializing a component
     */
    getComponentService: function () {
      switch (Service.section) {
        case 'realtor':
          return $injector.get('AdminUsersRealtorService');
        case 'inspector':
          return $injector.get('AdminUsersInspectorService');
        case 'appraiser':
          return $injector.get('AdminUsersAppraiserService');
        case 'amc':
          return $injector.get('AdminUsersAmcService');
        case 'appraiser-directory':
          return $injector.get('AdminUsersAppraiserDirectoryService');
        case 'amc-directory':
          return $injector.get('AdminUsersAmcDirectoryService');
        case 'asc':
          return $injector.get('AdminUsersAscService');
        default:
          throw Error('Section must be specified for certification');
      }
    },
    /**
     * Construct the table with the necessary columns hidden and shown
     * @param hide Comma separated list of columns to hide
     * @param show Comma separated list of columns to show
     * @returns {Array}
     */
    getDirectiveTableColumns: function (hide, show) {
      var service = this, hideItems, showItems, heading = [];
      // If items are hidden, only include those that are not
      if (hide) {
        hideItems = hide.split(',');
        Lazy(service.safeData.columns)
        .each(function (item) {
          if (hideItems.indexOf(item.id) === -1) {
            heading.push(item);
          }
        });
        // Otherwise include all items
      } else {
        heading = service.safeData.columns;
      }
      // If additional columns are to be conditionally shown, find them here
      if (show) {
        showItems = show.split(',');
        Lazy(service.safeData.additionalColumns)
        .each(function (item) {
          if (showItems.indexOf(item.id) !== -1) {
            heading.push(item);
          }
        });
      }
      return heading;
    },
    /**
     * Register record (appraiser or AMC from directory/ASC)
     */
    registerRecord: function (type, safeData) {
      var service = this;
      // Default to type
      safeData = safeData || type;
      // Make request
      return $resource(API_PREFIX() + '/v2.0/users/:type/:id/register',
      {id: Service.id, type: type})
      .save().$promise
      .then(function () {
        // Set to registered
        service.safeData[safeData][Service.id].registeredStatus = Service.registeredStatusValues.registered;
      });
    },
    /**
     * Retrieve data on load
     */
    loadRecords: function (type, hashValue, params, callback, paramFn) {
      var service = this, ResourceService, formatCallback = service.formatCallback || angular.noop;
      // Account for only two parameters
      if (angular.isObject(hashValue)) {
        params = hashValue;
        hashValue = null;
      }
      // Function as second param
      if (angular.isFunction(hashValue)) {
        callback = hashValue;
        hashValue = null;
      }
      // Callback with no params
      if (angular.isFunction(params)) {
        callback = params;
        params = null;
      }
      // Determine params at runtime via function
      if (angular.isFunction(paramFn)) {
        params = paramFn();
      }
      // Default to null
      params = angular.isDefined(params) ? params : null;
      // Default to type
      hashValue = angular.isDefined(hashValue) && hashValue !== null ? hashValue :  type;
      // Get resource service
      ResourceService = $injector.get('AdminUsersResourceService');
      // Load records
      ResourceService[type].init(params, hashValue)
      .then(function (response) {
        // Real API requests will contain a data parameter, if I need the rest of the object for any reason this can be changed later
        if (angular.isArray(response.data)) {
          response = response.data;
        }
        // Any operations that need to be performed before hashing/displaying
        if (angular.isFunction(service.beforeInitFormat)) {
          service.beforeInitFormat(response);
        }
        // Hash
        service.hashData(response, hashValue);
        // Format
        service.formatData(formatCallback);
      }).then(function () {
        // If callback
        if (angular.isDefined(callback)) {
          callback();
        }
      });
    },
    /**
     * Registered status values
     */
    registeredStatusValues: {
      // Registered statuses
      uninvited: 0,
      invited: 1,
      registered: 2
    },
    /**
     * Retrieve values for registered status for a user/company
     */
    getRegisteredStatus: function () {
      var statusValues = {};
      statusValues[Service.registeredStatusValues.uninvited] = 0;
      statusValues[Service.registeredStatusValues.invited] = 1;
      statusValues[Service.registeredStatusValues.registered] = 2;
      return statusValues;
    },
    /**
     * Create new company
     * @link https://github.com/ascope/manuals/tree/master/Developer's%20Guide/API%20Endpoints/v2.0/Appraisement/Companies#standard-appraiser-company-persistable-object
     */
    newCompany: function (type) {
      var service = this, newRecord = service.displayData.newCompany;
      return service.request('newCompany', newRecord)
        // Add company to table on success
      .then(function (response) {
        // Format raw record
        if (angular.isFunction(service.formatRawRecords)) {
          service.formatRawRecords(response);
        }
        // Add to table
        service.safeData[type][response.id] = response;
        // Recreate table
        service.formatData();
      });
    },
    /**
     * Standard requests
     */
    request: function (requestObject, requestFn, type, params) {
      var ResourceService = $injector.get('AdminUsersResourceService');
      // Partial args
      if (arguments.length === 3 && angular.isObject(type)) {
        params = type;
        type = null;
      }
      // Get request type
      type = Service.getRequestType(type);
      // Get $resource configuration
      return ResourceService[requestObject][requestFn](type, params);
    },
    /**
     * Request modification
     */
    getRequestType: function (type) {
      // No type specified
      if (typeof Service.record === 'undefined') {
        return '';
      }
      // Use type of record if one isn't passed in
      type = type || Service.record._type;
      // Type special case modifications
      switch (true) {
        case type === 'appraiser-directory':
          type = 'appraiser';
          break;
      }
      // Replace - with uppercase letter (appraiser-company to appraiserCompany)
      if (type.indexOf('-') !== -1) {
        type = type.replace(/-(.)/, function(match){
          return match.toUpperCase().slice(1);
        });
      }
      return type;
    },
    /**
     * Extract documents from instructions
     */
    extractDocuments: function () {
      var service = this;
      // Iterate instructions
      Lazy(service.safeData.instructions)
      // Pluck documents
      .each(function (instruction) {
        // Extract documents
        Lazy(instruction.documents)
        .each(function (document) {
          service.safeData.documents[document.id] = angular.copy(document);
        });
        // Turn documents property of instruction into an array of IDs
        instruction.documents = Lazy(instruction.documents)
        .map(function (document) {
          return document.id;
        }).toArray();
      });
      // Format
      service.formatData('documents');
    },
    /**
     * Extract default companies from client service
     * @returns {Array}
     */
    getClientCompanies: function () {
      var companies = [], service = this, AdminUsersClientService = $injector.get('AdminUsersClientService');
      angular.forEach(AdminUsersClientService.safeData.client, function (client) {
        if (!angular.isObject(client.company)) {
          // Default client
          if (!angular.isFunction(service.extractDefaultClient) || !service.extractDefaultClient(client)) {
            companies.push(client);
          }
        }
      });
      return companies;
    }
  };

  return Service;
}]);
