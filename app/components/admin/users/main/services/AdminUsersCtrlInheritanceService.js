'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users controller inheritance service
 */
app.factory('AdminUsersCtrlInheritanceService',
['HandbookService', 'HANDBOOK_CATEGORIES', '$timeout', 'REQUEST_DELAY', 'DomLoadService',
 'AdminUsersService', function (
HandbookService, HANDBOOK_CATEGORIES, $timeout, REQUEST_DELAY, DomLoadService, AdminUsersService) {
  return {
    createCtrl: function (service, $scope, type, options) {
      var vm = this;
      // Select inputs
      vm.selects = {};
      /**
       * Retrieve states for dropdown
       */
      HandbookService.query({category: HANDBOOK_CATEGORIES.states}).$promise
      .then(function (data) {
        vm.selects.state = data.data;
        vm.displayData.states = data.data;
      });
      // Country selects
      vm.selects.country = [
        {
          title: 'United States',
          code: 'US'
        },
        {
          title: 'Canada',
          code: 'CA'
        }
      ];

      /**
       * Rating tabs
       */
      // Quality modal tab config
      vm.ratingTabConfig = {
        quality: 'Quality: N/A',
        service: 'Service: N/A',
        turnAroundTime: 'Turn around time: N/A'
      };
      // Hold reference on service
      service.displayData.ratingTabs = vm.ratingTabConfig;
      // Tab config (need this to an array since objects don't guarantee order)
      vm.ratingTabs = ['quality', 'service', 'turnAroundTime'];
      // Selected stats tab
      vm.selectedRatingTab = 'quality';
      // Change quality tab
      vm.changeRatingTab = function (tab) {
        service.displayData.selectedRatingTab = tab;
      };

      /**
       * Active button text
       */
      vm.activeButtonText = function () {
        return vm.displayData.details.isActive ? 'Active' : 'Inactive';
      };

      /**
       * Toggle active/inactive
       */
      vm.toggleActive = function () {
        // Detach watcher
        AdminUsersService.detachUpdateWatcher();
        // Update record
        vm.displayData.details.active = !vm.displayData.details.active;
        // Make request
        service.toggleSetting('isActive')
        .catch(function () {
          // Reset safe data
          service.safeData[type][AdminUsersService.id].active = !service.safeData[type][AdminUsersService.id].active;
          // Reset display data
          service.displayData.details.active = !service.displayData.details.active;
          // Show failure modal
          $scope.$broadcast('show-modal', 'update-' + type + '-failure');
        })
        .finally(function () {
          // Reattach watcher
          AdminUsersService.attachUpdateWatcher.call(vm, $scope, service);
        });
      };
      /**
       * Send login credentials confirmation
       */
      vm.sendLoginConfirm = function () {
        $scope.$broadcast('show-modal', 'send-login-confirm');
      };
      /**
       * Send login credentials
       */
      vm.sendLogin = function () {
        // Send login credentials
        service.sendLogin()
        .then(function () {
          $scope.$broadcast('hide-modal', 'send-login-confirm');
          $scope.$broadcast('show-modal', 'send-login-success');
        })
        .catch(function () {
          $scope.$broadcast('show-modal', 'send-login-failure');
        });
      };
      /**
       * Download a document
       */
      vm.downloadDocument = function (type) {
        type = type || 'resume';
        // If the user has a resume, trigger download (or display if PDF)
        if (angular.isObject(vm.displayData.details[type]) && angular.isDefined(vm.displayData.details[type].url) &&
            vm.displayData.details[type].url) {
          $scope.triggerDownload(vm.displayData.details[type].url);
        } else {
          // Show no resume available
          $scope.$broadcast('show-modal', 'no-document');
        }
      };
      /**
       * Show rating info
       */
      vm.showRatings = function () {
        $scope.$broadcast('show-modal', type + '-quality');
      };
      /**
       * Show certification details
       */
      vm.showCertificationDetails = function () {
        $scope.$broadcast('show-modal', 'certification-info');
      };
      /**
      * Update details when typing
      */
      if (angular.isUndefined(options) || !options.disableUpdates) {
        AdminUsersService.attachUpdateWatcher.call(vm, $scope, service);
      }
      /**
       * Get appraisal reports
       */
      vm.showReports = function () {
        $scope.$broadcast('show-modal', type + '-reports');
      };
      /**
       * Upload new appraisal report
       */
      vm.displayUploadReport = function () {
        $scope.$broadcast('show-modal', 'upload-' + type + '-report', true);
      };
      /**
       * Display appraiser details
       * @param type View type
       */
      vm.showAdditionalDetails = function (type) {
        // Compile additional details
        $scope.$broadcast('compile-directive', type + '-additional-details');
        // Display modal
        DomLoadService.load()
        .then(function () {
          //// Ensure that a valid tab is displayed
          if (typeof vm.checkDetailsPaneTab === 'function') {
            vm.checkDetailsPaneTab();
          }
          // Change to notes so it can be displayed on load
          AdminUsersService.changeUrl({details: AdminUsersService.currentDetailsTab || 'notes'});
          // Display modal
          $scope.$broadcast('show-modal', type + '-additional-details');
          // Change URL on close
          vm.onAdditionalDetailsClose(type);
        });
      };
      /**
       * Callback for when additional details is closed
       */
      vm.onAdditionalDetailsClose = function (type) {
        var closed = $scope.$on('modal-closed-' + type + '-additional-details', function () {
          AdminUsersService.changeUrl({details: 'details'});
          closed();
        });
      };

      /**
       * Invite an appraiser
       */
      vm.inviteAppraiserConfirm = function () {
        $scope.$broadcast('show-modal', 'invite-appraiser');
      };
      /**
       * Send invitation
       */
      vm.inviteAppraiser = function () {
        service.inviteAppraiser(vm.displayData.details.email)
        .then(function () {
          $scope.$broadcast('hide-modal', 'invite-appraiser');
          $scope.$broadcast('show-modal', 'invite-appraiser-success');
        })
        .catch(function () {
          $scope.$broadcast('show-modal', 'invite-appraiser-failure');
        });
      };
      /**
       * Whether the appraiser is registered
       *
       * @todo
       */
      vm.recordRegistered = function () {
        var view;
        // Only run on selected sub-views
        angular.forEach(['appraiser-directory', 'amc-directory', 'asc'], function (subView) {
          if (angular.isDefined(service.safeData[subView])) {
            view = subView;
          }
        });
        if (angular.isUndefined(view)) {
          return;
        }
        // Return appraiser registration status
        if (angular.isDefined(service.safeData[view][AdminUsersService.id])) {
          return service.safeData[view][AdminUsersService.id].registeredStatus;
        }
      };
      /**
       * Register appraiser
       */
      vm.registerConfirm = function () {
        $scope.$broadcast('show-modal', 'register-record');
      };
      /**
       * Register appraiser
       */
      vm.registerRecord = function () {
        service.registerRecord()
        .then(function () {
          $scope.$broadcast('hide-modal', 'register-record');
          $scope.$broadcast('show-modal', 'register-record-success');
        })
        .catch(function () {
          $scope.$broadcast('show-modal', 'register-record-failure');
        });
      };
      /**
       * Display new company modal
       */
      vm.newCompanyModal = function () {
        vm.showNewCompany = true;
        // Wait for modal, then display it
        DomLoadService.load().then(function () {
          $scope.$broadcast('show-modal', 'new-company');
        });
        // Kill modal on close
        var closed = $scope.$on('modal-closed-new-company', function () {
          vm.showNewCompany = false;
          closed();
        });
      };
      /**
       * Submit new company modal
       */
      vm.newCompany = function () {
        // Submit new company
        service.newCompany()
        .then(function () {
          // Show success
          $scope.$broadcast('hide-modal', 'new-company');
          $scope.$broadcast('show-modal', 'new-company-successful');
          // reset new company modal
          service.displayData.newCompany = {};
        })
        .catch(function () {
          // Show failure
          $scope.$broadcast('show-modal', 'new-company-failure');
        });
      };

      /**
       * Trigger download of E&O Insurance
       */
      vm.eoInsurance = function () {
        try {
          $scope.triggerDownload(service.safeData[type][AdminUsersService.id].eo.insurance.url);
        } catch (e) {
          $scope.$broadcast('show-modal', 'eo-insurance-fail');
        }
      };

      /**
       * Disable submit of the new company modal
       */
      vm.newCompanyDisabled = function () {
        return vm.newCompanyForm.$invalid;
      };
    }
  };
}]);