describe('CtrlInheritanceService', function () {
  var controller, scope, ctrlInheritanceService, adminUsersAppraiserService;

  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersAppraiserService', AdminUsersAppraiserServiceMock);
    });
  });

  /**
   * I need to test this service as a controller, since it's explicitly meant to provide controller methods
   */
  beforeEach(inject(function ($controller, $rootScope, CtrlInheritanceService, AdminUsersAppraiserService) {
    scope = $rootScope.$new();
    controller = $controller('AdminUsersAppraiserCtrl', {$scope: scope});
    ctrlInheritanceService = CtrlInheritanceService;
    adminUsersAppraiserService = AdminUsersAppraiserService;

    // Inherit
    ctrlInheritanceService.createCtrl.call(controller, adminUsersAppraiserService, scope, 'appraiser');
  }));

  describe('states', function () {
    xit('should retrieve states on load');
  });

  describe('country', function () {
    xit('should retrieve counties on load');
  });

  describe('activeButtonText', function () {
    xit('should determine button text');
  });

  describe('rating tabs', function () {
    xit('should define rating tabs');
  });

  describe('toggleActive', function () {
    xit('should toggle active status');
  });

  describe('sendLogin', function () {
    xit('should send login credentials');
  });

  describe('downloadDocument', function () {
    xit('should trigger download or display of PDF');
  });

  describe('showAdditionalDetails', function () {
    xit('should show additional details');
    xit('should handle additional details modal close');
  });

  describe('inviteAppraiser', function () {
    xit('should invite appraiser');
  });

  describe('recordRegistered', function () {
    xit('should return whether the appraiser is registered');
  });

  describe('registerRecord', function () {
    xit('should handle registration event');
  });
});
