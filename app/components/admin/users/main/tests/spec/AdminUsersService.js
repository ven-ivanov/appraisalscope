describe('AdminUsersService', function () {
  var scope, Service, httpBackend, that, data, asTableService, domLoadService, $q, asDataService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AsTableService', AsTableServiceMock);
      $provide.factory('DomLoadService', DomLoadServiceMock);
    });
  });

  beforeEach(inject(function ($rootScope, AdminUsersService, $httpBackend, AsTableService, DomLoadService, _$q_, AsDataService) {
    scope = $rootScope.$new();
    Service = AdminUsersService;
    httpBackend = $httpBackend;
    asTableService = AsTableService;
    domLoadService = DomLoadService;
    $q = _$q_;
    asDataService = AsDataService;

    that = this;
    // Tabs
    that.tabs = [{tab: 'fakeTab'}, {tab: 'otherFakeTab'}];

    // Caller service
    this.safeData = {
      test: {},
      heading: []
    };
    this.displayData = {
      test: []
    };
    this.transformers = {
      test: {
        'Test 1': 'blah'
      },
      id: {
        filter: 'currency'
      }
    };

    // Data to be hashed
    data = [{id: 1, test: 'Test 1'}, {id: 2, test: 'Test 2'}];
    
    // Spies
    spyOn(AsTableService, 'applyFilters').and.callThrough();
    
    // http
    httpBackend.whenGET(/tab-values/).respond({client: 1000});
    httpBackend.whenGET(/states/).respond({code: 'AK', title: 'Arkansas'});

    // Mocking abstract state template
    httpBackend.expectGET(/base/).respond();
  }));

  describe('getTabValues', function () {
    beforeEach(function () {
      Service.getTabValues({recordId: 1000, page: 1000, activeTab: 'client'});
      httpBackend.flush();
      scope.$digest();
    });

    it('should set tab values', function () {
      expect(Service.displayData.tabs[0]).toEqual({
        name: 'client',
        tab: 'client',
        page: 1000,
        recordId: 1000,
        value: 1000
      });
    });
  });

  describe('getTabClass', function () {
    var response;
    it('should set the active tab to active', function () {
      response = Service.getTabClass({tab: 'fakeTab'}, 'fakeTab');
      expect(response).toEqual('col-md-1 active');
    });
    it('should set non-active tabs to normal class', function () {
      response = Service.getTabClass({tab: 'fakeTab'}, 'NOT THIS ONE');
      expect(response).toEqual('col-md-1');
    });
  });

  describe('with data', function () {
    beforeEach(function () {
      asDataService.hashData.call(this, data, 'test');
    });

    /**
     * @todo
     */
    xdescribe('updateRecord', function () {

    });

    // @todo Update
    describe('getRecordDetails', function () {
      beforeEach(function () {
        this.getRecordDetails = Service.getRecordDetails.bind(this, 'test');
        this.transformData = asDataService.transformData.bind(this);
        spyOn(Service, 'changeUrl');
        spyOn(this, 'transformData');
      });

      describe('no callback', function () {
        beforeEach(function () {
          this.getRecordDetails(null, 1);
        });
        it('should change URL to the appropriate state', function () {
          expect(Service.changeUrl).toHaveBeenCalledWith({recordId: 1});
        });
        it('should set the ID of the parameter', function () {
          expect(this.testId).toEqual(1);
        });
        it('should call transformData', function () {
          expect(this.transformData).toHaveBeenCalled();
        });
        it('should set displayData', function () {
          expect(this.displayData).toEqual({ test: [  ], details: { id: 1, test: 'Test 1' } });
        });
      });
    });

    describe('setStateOnLoad', function () {
      var ctrl = {};
      beforeEach(function () {
        ctrl.showAdditionalDetails = function(){};
        this.setStateOnLoad = Service.setStateOnLoad.bind(this, 'user');
        this.getRecordDetails = Service.getRecordDetails.bind(this, 'test', null);
        this.transformData = asDataService.transformData.bind(this);
        spyOn(this, 'getRecordDetails').and.callThrough();
        spyOn(Service, 'getStates').and.callThrough();
        spyOn(ctrl, 'showAdditionalDetails');
      });
      describe('functional', function () {
        beforeEach(function () {
          Service.changeUrl = function(){};
          this.setStateOnLoad({recordId: 1, details: 'notes'}, ctrl);
          httpBackend.flush();
          scope.$digest();
        });

        it('should call getStates()', function () {
          expect(Service.getStates).toHaveBeenCalled();
        });
        it('should retrieve record details if the requested type is in stateParams', function () {
          expect(this.getRecordDetails).toHaveBeenCalled();
        });
        it('should call showAdditionalDetails on ctrl', function () {
          expect(ctrl.showAdditionalDetails).toHaveBeenCalled();
        });
        it('should set selectedAdditionalDetailsTab on ctrl', function () {
          expect(ctrl.selectedAdditionalDetailsTab).toEqual('notes');
        });
      });

      describe('no params', function () {
        beforeEach(function () {
          spyOn(domLoadService, 'load').and.callThrough();
          this.setStateOnLoad();
          httpBackend.flush();
          scope.$digest();
        });
        it('should not continue checking state', function () {
          expect(domLoadService.load).not.toHaveBeenCalled();
        });
      });

      describe('no desired params', function () {
        beforeEach(function () {
          this.setStateOnLoad({test: 'test'});
          httpBackend.flush();
          scope.$digest();
        });
        it('should not call getRecordDetails', function () {
          expect(this.getRecordDetails).not.toHaveBeenCalled();
        });
        it('should not call showAdditionalDetails', function () {
          expect(ctrl.showAdditionalDetails).not.toHaveBeenCalled();
        });
      });
    });

    describe('toggleSetting', function () {
      beforeEach(function () {
        Service.id = 1;
        this.displayData = {details: {test: true}};
        this.request = jasmine.createSpy(this, 'request');
        this.safeData.testy = {1: {test: true}};
        this.toggleSetting = Service.toggleSetting.bind(this, 'testy', 'test');
        this.toggleSetting();
      });
      it('should send the request with the right parameters', function () {
        expect(this.request).toHaveBeenCalledWith('update', { test: false });
      });
    });

    describe('ratings', function () {
      beforeEach(function () {
        Service.id = 1;
        this.displayData = {
          ratingTabs: []
        };
        this.safeData = {
          test: {
            1: {}
          }
        };
        this.request = function () {
          return $q(function (resolve) {
            resolve({data: []});
          });
        };
        spyOn(Service, 'createRatingsTabs');
        spyOn(Service, 'formatRatings');
        this.hashData = jasmine.createSpy('hashData');
        spyOn(asDataService, 'formatData');
      });

      describe('getRatings', function () {
        beforeEach(function () {
          Service.getRatings.call(this, 'test', null, [1]);
          httpBackend.flush();
          scope.$digest();
        });
        it('should create ratings tabs', function () {
          expect(Service.createRatingsTabs).toHaveBeenCalled();
        });
        it('should format ratings', function () {
          expect(Service.formatRatings).toHaveBeenCalled();
        });
        it('should hash data properly', function () {
          expect(this.hashData).toHaveBeenCalledWith([], 'ratings');
        });
        it('should format data properly', function () {
          expect(asDataService.formatData).toHaveBeenCalledWith('ratings', [1]);
        });
      });

      xdescribe('createRatingsTabs', function () {

      });

      xdescribe('formatRatings', function () {

      });
    });

    describe('checkValidNumber', function () {
      var response, data;
      it('should remove non-digit characters', function () {
        data = {test: 'test', test2: '___ffwq__'};
        response = Service.checkValidNumber(data);
        expect(response).toEqual(false);
        expect(data).toEqual({
          test: '',
          test2: ''
        });
      });
      it('should allow digits to go through', function () {
        data = {test: 'test532', test2: '___868'};
        response = Service.checkValidNumber(data);
        expect(response).toEqual(false);
        expect(data).toEqual({
          test: '532',
          test2: '868'
        });
      });
      it('should return true is only digits are passed in', function () {
        data = {test: '532', test2: '868'};
        response = Service.checkValidNumber(data);
        expect(response).toEqual(true);
        expect(data).toEqual({
          test: '532',
          test2: '868'
        });
      });
    });

    describe('getDirectiveTableColumns', function () {
      xit('should be defined');
    });

    describe('setTabs', function () {
      xit('should be defined');
    });

    describe('registerRecord', function () {
      xit('should be defined');
    });

    describe('attachUpdateWatcher', function () {
      xit('should attach and detach watchers');
    });

    describe('loadRecords', function () {
      xit('should load records on view load');
    });

    xdescribe('newCompany', function () {

    });

    xdescribe('request', function () {

    });

    xdescribe('modifyRequestType', function () {

    });

    describe('getClientCompanies', function () {
      var response, service, adminUsersClientService;
      beforeEach(inject(function (AdminUsersClientService) {
        adminUsersClientService = AdminUsersClientService;
      }));
      beforeEach(function () {
        service = this;
        service.extractDefaultClient = jasmine.createSpy('extractDefaultClient');
        adminUsersClientService.safeData.client = [{id: 1}, {id: 2}, {company: {}, id: 3}];
        response = Service.getClientCompanies.call(this);
      });

      it('should check for default client on each client company', function () {
        expect(service.extractDefaultClient.calls.count()).toEqual(2);
      });
      it('should return client companies (ignoring users)', function () {
        expect(response).toEqual([{id:1}, {id:2}]);
      });
    });

    describe('extractDocuments', function () {
      var service;
      beforeEach(function () {
        service = this;
        service.formatData = jasmine.createSpy('formatData');
        service.safeData = {instructions: {1: {id: 1, documents: [{id:1, doc: 1}, {id:2, doc: 2}]}, 2: {id: 2, documents: [{id:3, doc: 3}, {id:2, doc: 2}]}}, documents: {}};
        Service.extractDocuments.call(this);
      });

      it('should set safe data', function () {
        expect(service.safeData.documents).toEqual({ 1: { id: 1, doc: 1 }, 2: { id: 2, doc: 2 }, 3: { id: 3, doc: 3 } });
      });
      it('should format data', function () {
        expect(service.formatData).toHaveBeenCalled();
      });
    });
  });
});