// Mock AdminUsersService
var AdminUsersServiceMock = function ($q, $injector) {
  var Mock = {
    id: 1,
    section: 'appraiser',
    // Just set a default to prevent errors
    record: {id: 1, _type: 'company'},
    getStates: function () {
      return $q(function (resolve) {
        resolve({
          data: []
        });
      });
    },
    // States
    states: {
      AK: 'Arkansas'
    },
    changeUrl: function () {

    },
    /**
     * ID val for hash data
     * @param idVal
     * @returns {*}
     */
    setIdVal: function (idVal) {
      // If idVal isn't set, or if a callback is passed as the third argument, return 'id'
      if (angular.isUndefined(idVal) || angular.isFunction(idVal)) {
        return 'id';
      }
      // Return value for idVal
      return idVal;
    },
    /**
     * Data handling
     *
     * This needs to work pretty much like how it does in the real service, or else it's going to make testing a pain
     */
    hashData: function (data, type, idVal, callback) {
      var that = this;
      // Default to id
      idVal = Mock.setIdVal(idVal);
      // Break references
      data = JSON.parse(JSON.stringify(data));
      // Store safe data
      angular.forEach(data, function (record) {
        that.safeData[type][record[idVal]] = record;
      });
      // Partial application
      if (arguments.length !== 4 && angular.isFunction(arguments[arguments.length - 1])) {
        callback = arguments[arguments.length - 1];
      }
      // Callback
      if (angular.isDefined(callback)) {
        callback();
      }
    },
    formatData: function (type, callback) {
      if (callback && angular.isFunction(callback)) {
        callback();
      }
    },
    transformData: function () {

    },
    getStateTransformer: function () {
      return {'AK': 'Arkansas'}
    },
    sendLogin: function () {
      
    },
    getRecordDetails: function () {

    },
    setStateOnLoad: function () {

    },
    toggleSetting: function () {

    },
    getRatings: function () {

    },
    getCertificationDetails: function () {

    },
    updateRecord: function () {

    },
    getCertificationTransformer: function () {
      
    },
    loadRecords: function () {

    },
    getDirectiveTableColumns: function () {

    },
    attachUpdateWatcher: function () {

    },
    newCompany: function () {

    },
    formatPlainObjectData: function () {

    },
    /**
     * This is a direct copy of the actual function. Need to keep it here to keep requests working
     */
    request: function (requestObject, requestFn, type, params) {
      var ResourceService = $injector.get('AdminUsersResourceService');
      // Partial args
      if (arguments.length === 3 && angular.isObject(type)) {
        params = type;
        type = null;
      }
      // Use type of record if one isn't passed in
      type = type || Mock.record._type;
      // Replace - with uppercase letter (appraiser-company to appraiserCompany)
      if (type.indexOf('-') !== -1) {
        type = type.replace(/-(.)/, function(match){
          return match.toUpperCase().slice(1);
        });
      }
      // Get $resource configuration
      return ResourceService[requestObject][requestFn](type, params);
    },
    extractDocuments: function () {

    },
    getClientCompanies: function () {
      
    }
  };

  return Mock;
};