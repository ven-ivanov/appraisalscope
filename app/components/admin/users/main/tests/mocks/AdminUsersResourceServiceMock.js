/**
 * Resource service mock
 */
var AdminUsersResourceServiceMock = function ($q) {
  return {
    client: {
      clientUserDocuments: function () {
        return $q(function (resolve) {
          resolve({data: [{id: 1}, {id: 2}]});
        });
      },
      clientUserInstructions: function () {
        return $q(function (resolve) {
          resolve({data: [{id: 1}, {id: 2}]});
        });
      },
      approvedAppraisers: function () {
        return $q(function (resolve) {
          resolve({data: [{id: 1}, {id: 2}]});
        });
      },
      disallowedAppraisers: function () {
        return $q(function (resolve) {
          resolve({data: [{id: 1}, {id: 2}]});
        });
      },
      feeSchedule: function () {
        return $q(function (resolve) {
          resolve({data: [{id: 1}, {id: 2}]});
        });
      },
      checklist: function () {
        return $q(function (resolve) {
          resolve({data: [{id: 1}, {id: 2}]});
        });
      },
      clientsOnReport: function () {
        return $q(function (resolve) {
          resolve({data: [{id: 1}, {id: 2}]});
        });
      }
    },
    notes: function () {
      //return function(){
        return {
          $update: function () {
            return $q(function (resolve) {
              resolve();
            });
          },
          query: function () {
            return $q(function (resolve) {
              resolve();
            });
          }
        };
      //};
    },
    clientOnReport: {
      init: function () {
        return $q(function (resolve) {
          resolve();
        });
      }
    },
    // Appraiser panel
    appraiserPanel: {
      addApproved: function () {
        return $q(function (resolve) {
          resolve();
        });
      },
      addDoNotUse: function () {
        return $q(function (resolve) {
          resolve();
        });
      },
      removeApproved: function () {
        return $q(function (resolve) {
          resolve();
        });
      },
      removeDoNotUse: function () {
        return $q(function (resolve) {
          resolve();
        });
      }
    }
  }
};
