'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - ASC view controller
 */
app.controller('AdminUsersAscCtrl',
['$scope', 'AdminUsersAscService', '$stateParams', 'AdminUsersService',
 'AdminUsersCtrlInheritanceService', function (
$scope, AdminUsersAscService, $stateParams, AdminUsersService, AdminUsersCtrlInheritanceService) {
  var vm = this, // This service name is just getting out of hand with the line breaks
  Service = AdminUsersAscService;
  // Inherit common functions
  AdminUsersCtrlInheritanceService.createCtrl.call(vm, Service, $scope, 'asc');
  // Directory details
  vm.displayData = Service.displayData;
  // Safe data
  vm.safeData = Service.safeData;
  /**
   * Additional details tabs
   */
    // Tab config
  vm.additionalDetailsTabsConfig = {
    notes: 'Notes',
    coverage: 'Coverage',
    appraisalForms: 'Appraisal forms'
  };
  // Tab config array (for ordering)
  vm.additionalDetailsTabs = ['notes', 'coverage', 'appraisalForms'];
  // Selected tab
  AdminUsersService.currentDetailsTab = vm.selectedAdditionalDetailsTab = 'notes';
  vm.changeAdditionalDetailsTab = function (tab) {
    // Keep track of tab
    AdminUsersService.changeUrl({details: tab});
  };

  /**
   * Initiate data and state
   */
  vm.init = function () {
    // Set state on load
    Service.setStateOnLoad($stateParams, vm);
    // Keep reference to which section we're on
    AdminUsersService.section = 'asc';
  };

  // Init controller load
  vm.init();
}]);