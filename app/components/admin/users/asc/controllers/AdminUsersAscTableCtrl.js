'use strict';

var app = angular.module('frontendApp');

/**
 * ASC results table
 */
app.controller('AdminUsersAscTableCtrl',
['$scope', 'AdminUsersAscService', 'AdminUsersTableCtrlInheritanceService',
 function ($scope, AdminUsersAscService, AdminUsersTableCtrlInheritanceService) {

   // Inherit table controller methods
   AdminUsersTableCtrlInheritanceService.inherit.call(this, $scope, AdminUsersAscService, 'asc');
 }]);