'use strict';

var app = angular.module('frontendApp');

/**
 * Admin User - ASC service
 */
app.factory('AdminUsersAscService',
['$http', 'API_PREFIX', 'AdminUsersService', '$resource', 'AsDataService', function ($http, API_PREFIX, AdminUsersService, $resource, AsDataService) {
  var Service = {
    // Directory ID
    directoryId: 0,
    // Safe data
    safeData: {
      // Directorys table
      asc: {},
      // Table heading
      heading: [
        // Name
        {
          label: 'Name',
          data: 'name',
          search: true
        },
        // Company name
        {
          label: 'Company',
          data: 'companyName',
          search: true
        },
        // Address
        {
          label: 'Address',
          data: 'address',
          search: true
        }
      ],
      // Certification details
      certificationDetails: {}
    },
    // Display data
    displayData: {
      // Table data
      asc: [],
      // Details
      details: {},
      // States dropdown
      states: [],
      // Certification details
      certificationDetails: [],
      // Yes/no dropdown
      yesNoDropdown: AdminUsersService.yesNoDropdown,
      // Register status
      registerStatus: {
        unregistered: 0,
        inviteSent: 1,
        registered: 2
      },
      // Text for the current section
      sectionText: 'appraiser'
    },
    // Data transformations
    transformers: {
      // Certification display
      certification: AdminUsersService.getCertificationTransformer(),
      // Registered date
      registered: {
        filter: 'date'
      },
      // E&O expiration date
      eoExpirationDate: {
        filter: 'date'
      },
      // Registration status
      registeredStatus: AdminUsersService.getRegisteredStatus()
    },
    /**
     * Callback for formatting data
     */
    formatCallback: function () {
      angular.forEach(Service.displayData.asc, function (directory) {
        // Create name
        directory.name = directory.firstName + ' ' + directory.lastName;
        // Create address
        directory.address = directory.officeAddress1 + ' ' + directory.officeAddress2;
      });
    },
    /**
     * Send invitation to an appraiser
     */
    inviteAppraiser: function (email) {
      return $resource(API_PREFIX() + '/v2.0/users/appraiser/:appraiserId/invite',
      {appraiserId: Service.ascId}, {
        email: {method: 'POST'}
      })
      .email({email: email}).$promise
      .then(function () {
        // Set to invite sent
        Service.safeData.asc[AdminUsersService.id].registeredStatus = AdminUsersService.registeredStatusValues.invited;
        // Update button text
        Service.displayData.details.registeredStatus = 'Invite sent';
      });
    }
  };

  /**
   * Inherit from AdminUsersService
   */
  Service.loadRecords = AdminUsersService.loadRecords.bind(Service, 'asc');
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'asc');
  Service.transformData = AsDataService.transformData.bind(Service);
  // Get record details
  Service.getRecordDetails =
  AdminUsersService.getRecordDetails.bind(Service, 'asc', null);
  // Set state
  Service.setStateOnLoad = AdminUsersService.setStateOnLoad.bind(Service, 'asc');
  // Toggle settings
  Service.toggleSetting = AdminUsersService.toggleSetting.bind(Service, 'asc');
  // Update record
  Service.updateRecord = AdminUsersService.updateRecord.bind(Service, 'asc');
  // Register record
  Service.registerRecord = AdminUsersService.registerRecord.bind(Service, 'appraiser', 'asc');

  return Service;
}]);