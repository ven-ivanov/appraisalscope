'use strict';

var app = angular.module('frontendApp');

/**
 * Compile additional details only when it's needed
 */
app.directive('adminUsersAscAdditionalDetails', ['DirectiveConditionalLoadService', function (DirectiveConditionalLoadService) {
  return function (scope, elem) {
    var template;
    // Template
    template =
    ['<info-modal modal-id="asc-additional-details" modal-title="Additional details" button-text="OK" modal-class="notification-window" modal-width="100%" modal-height="max">',
     '<as-tabs tab-config="ascCtrl.additionalDetailsTabsConfig" tabs="ascCtrl.additionalDetailsTabs" tab-width="3rds" selected-tab="ascCtrl.selectedAdditionalDetailsTab" change-tab="ascCtrl.changeAdditionalDetailsTab(tab)">',
     '</as-tabs>',
      // Switch show directive
     '<div ng-switch="ascCtrl.selectedAdditionalDetailsTab">',
      // Notes
     '<admin-users-notes ng-switch-when="notes"></admin-users-notes>',
     // Coverage
     '<admin-users-coverage ng-switch-when="coverage" hide="fha,commercial,edit,delete" state="false"></admin-users-coverage>',
     // Appraisal forms
     '<admin-users-jobtypes ng-switch-when="appraisalForms" hide="appraiser-requested-fee,set-fee,selected" show="selected-disabled,fee"></admin-users-jobtypes>',
     '</div>',
     '</info-modal>'];

    // Compile the directive only when necessary and then load it
    DirectiveConditionalLoadService.init.call(scope, template, elem, 'asc-additional-details');
  };
}]);