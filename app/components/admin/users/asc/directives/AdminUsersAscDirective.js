'use strict';

var app = angular.module('frontendApp');

/**
 * Admin User - ASC
 */
app.directive('adminUsersAsc', ['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/asc/directives/partials/asc.html',
    controller: 'AdminUsersAscCtrl',
    controllerAs: 'ascCtrl',
    compile: function () {
      return {
        pre: function (scope) {
          // Expose trigger download
          DirectiveInheritanceService.inheritDirective.call(scope);
        }
      };
    }
  };
}]);