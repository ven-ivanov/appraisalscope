'use strict';

var app = angular.module('frontendApp');

/**
 * Appraiser panel service
 */
app.factory('AdminUsersAppraiserPanelService',
['AdminUsersService', 'AsDataService', 'DomLoadService', 'AdminUsersAppraiserPanelTableService',
function (AdminUsersService, AsDataService, DomLoadService, AdminUsersAppraiserPanelTableService) {
  var Service = {
    safeData: {
      // Main records
      appraiser: {},
      // Approved lists
      approved: {},
      // Do not use lists
      'do-not-use': {},
      // Table heading (begin on all)
      heading: '',
      // Various table configurations
      tableDefinitions: {
        // Table displaying all records
        all: [
          // Name
          {
            label: 'Name',
            data: 'appraiserName',
            search: true
          },
          //  Address
          {
            label: 'Address',
            data: 'address',
            search: true
          },
          //  List (approved/dnu)
          {
            label: 'List type',
            data: 'listType',
            search: true
          },
          //  List name
          {
            label: 'List name',
            data: 'listTitle',
            search: true
          }
        ],
        // Approved/do not use
        specific: [
          // Multiselect
          {
            label: '',
            noSort: true,
            checkbox: true
          },
          // Name
          {
            label: 'Name',
            data: 'appraiserName',
            search: true
          },
          // Company
          {
            label: 'Company',
            data: 'companyName',
            search: true
          },
          //  Address
          {
            label: 'Address',
            data: 'address',
            search: true
          },
          // State
          {
            label: 'State',
            data: 'state',
            search: true
          }
        ]
      },
      // Cache for switching between view types
      cache: {
        all: {},
        approved: {},
        'do-not-use': {}
      },
      // Store the selected list when changing types
      selectedLists: {
        approved: 0,
        'do-not-use': 0
      },
      // Multiselect for each list type
      multiSelectCache: {
        approved: {},
        'do-not-use': {}
      },
      // Selected appraisers in a specific list
      selectedAppraisersInList: [],
      // Delete multiple button disabled status
      deleteMultipleFromListDisabled: true
    },
    displayData: {
      appraiser: [],
      // Approved list
      approved: [],
      // Do not use lists
      'do-not-use': [],
      // List types
      listTypes: [
        {id: 'all', name: 'All'},
        {id: 'approved', name: 'Approved'},
        {id: 'do-not-use', name: 'Do not use'}
      ],
      // Currently selected list type
      listType: 'all',
      // Selected list
      selectedList: 0
    },
    // Transformations for display in table
    transformers: {
      appraiserName: {
        source: 'appraiser.name',
        dest: 'appraiserName'
      },
      address: {
        source: 'appraiser.locations.office.address1',
        dest: 'address'
      },
      listType: {
        source: 'list.type',
        dest: 'listType',
        filter: 'replaceChar:-: '
      },
      listTitle: {
        source: 'list.title',
        dest: 'listTitle'
      },
      companyName: {
        source: 'appraiser.company.name',
        dest: 'companyName'
      },
      state: {
        source: 'appraiser.locations.office.state.name',
        dest: 'state'
      }
    },
    /**
     * Create a sensible hash value for each appraiser/list combination
     */
    beforeInitFormat: function (response) {
      angular.forEach(response, function (record) {
        // ID val
        record.id = record.appraiser.id + '-' + record.list.id;
      });
    },
    /**
     * Alter the table definition so that we can have new columns
     */
    alterTableDefinition: function (displayAll) {
      var heading = displayAll === 'all' ? 'all' : 'specific';
      Service.safeData.heading = Service.safeData.tableDefinitions[heading];
      return Service.safeData.heading;
    },
    /**
     * When switching table type, load the proper records
     */
    loadDataType: function (newView, oldView) {
      // Cache record set if not already done
      if (!Object.keys(Service.safeData.cache[oldView]).length) {
        Service.safeData.cache[oldView] = angular.copy(Service.safeData.appraiser);
      }
      // Retrieve cache value if available
      if (Object.keys(Service.safeData.cache[newView]).length) {
        Service.safeData.appraiser = angular.copy(Service.safeData.cache[newView]);
      } else {
        // Create data organization
        Service.resortData(newView);
      }
      // Format for display
      Service.formatData();
    },
    /**
     * Create data organization
     */
    resortData: function (type) {
      var data = angular.copy(Service.safeData.cache.all), sortedData = {};
      // Extract only the necessary records
      angular.forEach(data, function (record) {
        if (record.list.type === type) {
          sortedData[record.id] = record;
        }
      });
      // Set to safe data
      Service.safeData.appraiser = sortedData;
    },
    /**
     * Retrieve approved and do not use lists
     */
    getLists: function () {
      // Retrieve lists after the rest of the view has finished loading
      DomLoadService.load().then(function () {
        // Get approved lists
        Service.request('getApprovedAll').then(function (response) {
          // Add default
          response.data.unshift({id: 0, title: 'No list selected'});
          // Hash and format
          Service.hashData(response.data, 'approved');
          AsDataService.formatData.call(Service, 'approved');
          // Get do not use lists
          return Service.request('getDoNotUseAll');
        })
        .then(function (response) {
          // Add default
          response.data.unshift({id: 0, title: 'No list selected'});
          // Hash and format
          Service.hashData(response.data, 'do-not-use');
          AsDataService.formatData.call(Service, 'do-not-use');
        });
      });
    },
    /**
     * Reset multiselect based on list selected
     */
    resetMultiSelect: function (newListType, oldListType, multiSelect) {
      // Cache selections from previous list type
      Service.safeData.multiSelectCache[oldListType] = angular.copy(multiSelect);
      // Return previous multiselect sections
      return Service.safeData.multiSelectCache[newListType];
    },
    /**
     * Check if the button for adding appraisers should be disabled
     */
    addToListDisabled: function (multiSelect) {
      var disabled = true, prop;
      // No multiselect
      if (!angular.isObject(multiSelect) || !Object.keys(multiSelect).length) {
        return true;
      }
      // See if at least one multi select is checked
      for (prop in multiSelect) {
        if (!multiSelect.hasOwnProperty(prop)) {
          continue;
        }
        if (multiSelect[prop]) {
          disabled = false;
          break;
        }
      }
      // Make sure a list is also selected for this type
      if (!disabled) {
        return !Service.safeData.selectedLists[Service.displayData.listType];
      }
    },
    /**
     * Add appraisers to selected list
     */
    addToList: function (multiSelect) {
      // Get appraiser IDs
      try {
        var ids = Service.extractSelectedAppraisers(multiSelect);
        var listType = Service.displayData.listType;
        var listId = Service.safeData.selectedLists[listType];
        // Make request
        return Service.request('addToList', {
          url: {
            listType: listType,
            listId: listId
          },
          body: {ids: ids}
        })
          // Get table details again
        .then(function () {
          AdminUsersAppraiserPanelTableService.getListDetails(listType, listId);
        });
      } catch (e) {
        // @todo Log error
        console.log('Could not extract selected appraisers');
      }
    },
    /**
     * Extract selected appraisers from multiselect
     */
    extractSelectedAppraisers: function (multiSelect) {
      var ids = [];
      // Get the selected items from multiselect
      if (multiSelect) {
        angular.forEach(multiSelect, function (selection, key) {
          if (selection) {
            ids.push(key);
          }
        });
        // Slice out the appraier IDs
        angular.forEach(ids, function (responseItem, key) {
          ids[key] = parseInt(responseItem.slice(0, responseItem.indexOf('-')));
        });
        // Get all
      } else {
        return null;
      }
      return ids;
    },
    /**
     * Add new list
     */
    addNewList: function () {
      // List type
      return Service.request('addNewList', {type: Service.displayData.listType, title: Service.displayData.newListTitle})
      .then(function (response) {
        // Add to safe data
        Service.safeData[response.type][response.id] = response;
        // Format for display
        AsDataService.formatData.call(Service, response.type);
      });
    },
    /**
     * Delete selected appraiser list
     */
    deleteList: function () {
      return Service.request('deleteList', {type: Service.displayData.listType, id: Service.safeData.selectedLists[Service.displayData.listType]})
      .then(function () {
        // Delete list
        delete Service.safeData[Service.displayData.listType][Service.safeData.selectedLists[Service.displayData.listType]];
        // Format for display
        AsDataService.formatData.call(Service, Service.displayData.listType);
        // Set selected list to 0
        Service.safeData.selectedLists[Service.displayData.listType] = 0;
      });
    },
    /**
     * Deletes a specific appraiser from selected list
     */
    deleteAppraisersFromList: function () {
      var listType = Service.displayData.listType;
      var listId = Service.safeData.selectedLists[listType];
      var appraiserIds = Service.safeData.selectedAppraisersInList;
      // Request
      return Service.request('deleteAppraisersFromList', {
        listType: listType,
        listId: listId,
        appraiserIds: appraiserIds
      })
      .then(function () {
        AdminUsersAppraiserPanelTableService.removeAppraisersFromTable(appraiserIds);
      });
    },
    /**
     * Get the appraisers currently selected
     */
    getSelectedAppraisersFromMultiSelect: function (multiselect) {
      var selectedAppraisers = Service.safeData.selectedAppraisersInList;
      selectedAppraisers.length = 0;
      // Find the selected appraisers
      angular.forEach(multiselect, function (appraiser, key) {
        if (appraiser) {
          selectedAppraisers.push(parseInt(key));
        }
      });
    }
  };

  // Begin on showing all records results
  Service.safeData.heading = Service.safeData.tableDefinitions.all;

  /**
   * Inherit from AdminUsersService and AsDataService
   */
  Service.request = AdminUsersService.request.bind(Service, 'appraiserPanel');
  Service.loadRecords = AdminUsersService.loadRecords.bind(Service, 'appraiserPanel', 'appraiser', Service.getLists);
  // Hash
  Service.hashData = AsDataService.hashData.bind(Service);
  // Format table data
  Service.formatData = AsDataService.formatData.bind(Service, 'appraiser');
  // Transform
  Service.transformData = AsDataService.transformData.bind(Service);
  // Get record details
  Service.getRecordDetails = AdminUsersService.getRecordDetails.bind(Service, 'appraiser');
  // Set state on load
  Service.setStateOnLoad = AdminUsersService.setStateOnLoad.bind(Service, 'appraiser');
  return Service;
}]);