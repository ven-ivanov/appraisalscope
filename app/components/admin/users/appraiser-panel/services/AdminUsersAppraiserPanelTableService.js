'use strict';

var app = angular.module('frontendApp');

app.factory('AdminUsersAppraiserPanelTableService',
['AdminUsersService', 'AsDataService', function (AdminUsersService, AsDataService) {
  var Service = {
    // Callback for clearing the multiselect
    observer: angular.noop,
    safeData: {
      // Details view table for an individual list
      appraiserList: {},
      // Cache the results of the details pane to avoid extraneous requests
      appraiserListCache: {
        approved: {},
        'do-not-use': {}
      }
    },
    displayData: {},
    transformers: {
      address: {
        source: 'locations.office.address1',
        dest: 'address'
      },
      city: {
        source: 'locations.office.city',
        dest: 'city'
      },
      state: {
        source: 'locations.office.state.name',
        dest: 'state'
      },
      zip: {
        source: 'locations.office.zip',
        dest: 'zip'
      },
      licenseNumber: {
        source: 'certification.certificate.number',
        dest: 'licenseNumber'
      },
      licenseType: {
        source: 'certification.certificate.type',
        dest: 'licenseType',
        filter: 'replaceChar:-: '
      }
    },
    registerObserver: function (callback) {
      Service.observer = callback;
    },
    /**
     * Begin watching when a checklist is selected
     */
    notifyObserver: function () {
      Service.observer();
    },
    /**
     * Retrieve list details
     */
    getListDetails: function (type, id) {
      // Clear multiselect
      Service.notifyObserver();
      // Use cached values if we have them
      if (angular.isDefined(Service.safeData.appraiserListCache[type][id])) {
        // Set to safe data and format
        Service.safeData.appraiserList = angular.copy(Service.safeData.appraiserListCache[type][id]);
        Service.formatData();
        return;
      }
      // Otherwise, make request
      return Service.request('getAppraisersInList', {type: type, id: id})
      .then(function (response) {
        // hash and format
        Service.hashData(response.data, 'appraiserList');
        Service.formatData();
        // Move to cache
        Service.safeData.appraiserListCache[type][id] = angular.copy(Service.safeData.appraiserList);
      });
    },
    /**
     * Remove appraisers from current table
     * @param ids
     */
    removeAppraisersFromTable: function (ids) {
      var appraisersList = Service.safeData.appraiserList;
      // Remove each deleted appraiser
      angular.forEach(appraisersList, function (listItem, key) {
        // Delete all, or delete selected
        if (!ids.length || ids.indexOf(listItem.id) !== -1) {
          delete appraisersList[key];
        }
      });
      // Reformat table
      Service.formatData();
    },
    /**
     * Determine if the delete multiple button should be disabled
     * @param multiSelect
     */
    deleteMultipleFromListDisabled: function (multiSelect) {
      var disabled = true;
      angular.forEach(multiSelect, function (item) {
        if (item) {
          disabled = false;
        }
      });
      return disabled;
    }
  };

  /**
   * Inherit from AdminUsersService and AsDataService
   */
  Service.request = AdminUsersService.request.bind(Service, 'appraiserPanel');
  // Hash
  Service.hashData = AsDataService.hashData.bind(Service);
  // Format table data
  Service.formatData = AsDataService.formatData.bind(Service, 'appraiserList');
  // Transform
  Service.transformData = AsDataService.transformData.bind(Service);
  return Service;
}]);