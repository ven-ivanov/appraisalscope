describe('AdminUsersAppraiserPanelService', function () {
  var scope, httpBackend, Service, adminUsersService, domLoadService, adminUsersAppraiserPanelTableService, asDataService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
      $provide.factory('DomLoadService', DomLoadServiceMock);
      $provide.factory('AdminUsersAppraiserPanelTableService', function () {
        return {
          getListDetails: function () {},
          removeAppraisersFromTable: function () {}
        };
      });
    });
  });

  beforeEach(inject(function (
  $httpBackend, $rootScope, AdminUsersAppraiserPanelService, AdminUsersService, DomLoadService,
  AdminUsersAppraiserPanelTableService, AsDataService) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    Service = AdminUsersAppraiserPanelService;
    adminUsersService = AdminUsersService;
    domLoadService = DomLoadService;
    adminUsersAppraiserPanelTableService = AdminUsersAppraiserPanelTableService;
    asDataService = AsDataService;

    // Spy on format and hash
    spyOn(Service, 'formatData');
    spyOn(Service, 'hashData');
    spyOn(asDataService, 'formatData');
    spyOn(Service, 'request').and.callThrough();

    httpBackend.whenGET(/appraiser-lists\/approved$/).respond({data: [{id:1, title: 'Approved title'}]});
    httpBackend.whenGET(/appraiser-lists\/do-not-use$/).respond({data: [{id:1, title: 'Do not use title'}]});
    httpBackend.whenPUT(/appraiser-lists\/approved/).respond();
    httpBackend.whenPOST(/appraiser-lists\/approved/).respond({type: 'approved', id: 1000});
    httpBackend.whenDELETE(/appraiser-lists\/approved/).respond();
  }));

  describe('beforeInitFormat', function () {
    var data = [{appraiser: {id: 1}, list: {id: 2}}];
    beforeEach(function () {
      Service.beforeInitFormat(data);
    });
    it('should have created a unique ID of list and appraiser', function () {
      expect(data).toEqual([ { appraiser: { id: 1 }, list: { id: 2 }, id: '1-2' } ]);
    });
  });

  describe('alterTableDefinition', function () {
    it('should set the table definition to specific by default', function () {
      expect(Service.alterTableDefinition()).toEqual([ { label: '', noSort: true, checkbox: true }, { label: 'Name', data: 'appraiserName', search: true }, { label: 'Company', data: 'companyName', search: true }, { label: 'Address', data: 'address', search: true }, { label: 'State', data: 'state', search: true } ]);
    });

    it('should set the table definition to all when requested', function () {
      expect(Service.alterTableDefinition('all')).toEqual([ { label: 'Name', data: 'appraiserName', search: true }, { label: 'Address', data: 'address', search: true }, { label: 'List type', data: 'listType', search: true }, { label: 'List name', data: 'listTitle', search: true } ]);
    });
  });

  describe('loadDataType', function () {
    beforeEach(function () {
      spyOn(Service, 'resortData');
      Service.safeData.appraiser = {appraiser: 1};
      Service.safeData.cache.old = {};
      Service.safeData.cache.new = {new: 3};
    });

    describe('cached', function () {
      beforeEach(function () {
        Service.loadDataType('new', 'old');
      });

      it('should cache the incoming data', function () {
        expect(Service.safeData.cache.old).toEqual({ appraiser: 1 });
      });
      it('should retrieve the new view data from cache', function () {
        expect(Service.safeData.appraiser).toEqual({new: 3});
      });
      it('should call format data', function () {
        expect(Service.formatData).toHaveBeenCalled();
      });
    });

    describe('not cached', function () {
      it('should resort data if data is not cached', function () {
        Service.safeData.cache.new = {};
        Service.loadDataType('new', 'old');
        expect(Service.resortData).toHaveBeenCalled();
      });
    });
  });

  describe('resortData', function () {
    beforeEach(function () {
      Service.safeData.cache.all = [{list: {type: 'test'}, id: 1}];
      Service.resortData('test');
    });

    it('should set sorted data to the appraiser', function () {
      expect(Service.safeData.appraiser).toEqual({ 1: { list: { type: 'test' }, id: 1 } });
    });
  });

  describe('getLists', function () {
    beforeEach(function () {
      Service.getLists();
      httpBackend.flush();
      scope.$digest();
    });

    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should add no list selected to safe data', function () {
      expect(Service.hashData.calls.argsFor(0)).toEqual([ [ { id: 0, title: 'No list selected' }, { id: 1, title: 'Approved title' } ], 'approved' ]);
      expect(Service.hashData.calls.argsFor(1)).toEqual([ [ { id: 0, title: 'No list selected' }, { id: 1, title: 'Do not use title' } ], 'do-not-use' ]);
    });
    it('should format data', function () {
      expect(asDataService.formatData).toHaveBeenCalled();
      expect(asDataService.formatData.calls.count()).toEqual(2);
    });
  });

  describe('resetMultiSelect', function () {
    var response;
    beforeEach(function () {
      Service.safeData.multiSelectCache.approved = {1000: true};
      response = Service.resetMultiSelect('approved', 'do-not-use', {1: true, 2: false});
    });

    it('should cache the multiselect value', function () {
      expect(Service.safeData.multiSelectCache['do-not-use']).toEqual({1: true, 2: false});
    });
    it('should return the cache value for the new list type', function () {
      expect(response).toEqual({1000: true});
    });
  });

  describe('addToListDisabled', function () {
    var response;
    describe('nothing selected in multiselect', function () {
      beforeEach(function () {
        response = Service.addToListDisabled();
      });

      it('should return true', function () {
        expect(response).toEqual(true);
      });
    });

    describe('multiselect selected, but no list', function () {
      beforeEach(function () {
        Service.displayData.listType = 'approved';
        response = Service.addToListDisabled({1: true});
      });

      it('should return true', function () {
        expect(response).toEqual(true);
      });
    });

    describe('multiselect selected, list selected', function () {
      beforeEach(function () {
        Service.safeData.selectedLists.approved = 1;
        Service.displayData.listType = 'approved';
        response = Service.addToListDisabled({1: true});
      });

      it('should return true', function () {
        expect(response).toEqual(false);
      });
    });
  });

  describe('addToList', function () {
    beforeEach(function () {
      spyOn(Service, 'extractSelectedAppraisers');
      spyOn(adminUsersAppraiserPanelTableService, 'getListDetails');
      Service.displayData.listType = 'approved';
      Service.safeData.selectedLists.approved = 1;
      Service.addToList();
      httpBackend.flush();
      scope.$digest();
    });

    it('should call getListDetails', function () {
      expect(adminUsersAppraiserPanelTableService.getListDetails).toHaveBeenCalled();
    });
    it('should make the request with the right parameters', function () {
      expect(Service.request).toHaveBeenCalledWith('addToList', { url: { listType: 'approved', listId: 1 }, body: { ids: undefined } });
    });
  });

  describe('extractSelectedAppraisers', function () {
    var response;
    describe('appraisers selected', function () {
      beforeEach(function () {
        response = Service.extractSelectedAppraisers({'1-52': true, '5-99': false});
      });
      it('should return the selected appraisers', function () {
        expect(response).toEqual([1]);
      });
    });

    describe('no appraisers selected', function () {
      beforeEach(function () {
        response = Service.extractSelectedAppraisers();
      });
      it('should return the selected appraisers', function () {
        expect(response).toEqual(null);
      });
    });
  });

  describe('addNewList', function () {
    beforeEach(function () {
      Service.displayData.listType = 'approved';
      Service.displayData.newListTitle = 'New list';
      Service.addNewList();
      httpBackend.flush();
      scope.$digest();
    });

    it('should make the request with the right parameters', function () {
      expect(Service.request).toHaveBeenCalledWith('addNewList', { type: 'approved', title: 'New list' });
    });
    it('should format data', function () {
      expect(asDataService.formatData).toHaveBeenCalled();
    });
  });

  describe('deleteList', function () {
    beforeEach(function () {
      Service.displayData.listType = 'approved';
      Service.safeData.selectedLists.approved = 1;
      Service.safeData.approved = {1: {test: 'test'}, 2: {test: 'test2'}};
      Service.safeData.selectedLists.approved = 1;
      Service.deleteList();
      httpBackend.flush();
      scope.$digest();
    });

    it('should make the request with the right parameters', function () {
      expect(Service.request).toHaveBeenCalledWith('deleteList', { type: 'approved', id: 1 });
    });
    it('should delete the relevant list item', function () {
      expect(Service.safeData.approved).toEqual({ 2: { test: 'test2' } });
    });
    it('should reset to no list selected', function () {
      expect(Service.safeData.selectedLists.approved).toEqual(0);
    });
  });

  describe('deleteAppraisersFromList', function () {
    beforeEach(function () {
      spyOn(adminUsersAppraiserPanelTableService, 'removeAppraisersFromTable');
      Service.displayData.listType = 'approved';
      Service.safeData.selectedLists.approved = 1;
      Service.safeData.selectedAppraisersInList = [1,2];
      Service.deleteAppraisersFromList();
      httpBackend.flush();
      scope.$digest();
    });

    it('should call removeAppraisersFromTable', function () {
      expect(adminUsersAppraiserPanelTableService.removeAppraisersFromTable).toHaveBeenCalled();
    });
    it('should make the proper request', function () {
      expect(Service.request).toHaveBeenCalledWith('deleteAppraisersFromList', { listType: 'approved', listId: 1, appraiserIds: [ 1, 2 ] });
    });
  });

  describe('getSelectedAppraisersFromMultiSelect', function () {
    beforeEach(function () {
      Service.safeData.selectedAppraisersInList = [1,2];
      Service.getSelectedAppraisersFromMultiSelect({3: true, 4: false});
    });

    it('should set selected appraisers', function () {
      expect(Service.safeData.selectedAppraisersInList).toEqual([3]);
    });
  });
});