'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersAppraiserPanelCtrl',
['$scope', 'AdminUsersAppraiserPanelService', 'AdminUsersCtrlInheritanceService', '$stateParams', 'AdminUsersService',
 'AdminUsersTableCtrlInheritanceService', 'AdminUsersAppraiserPanelTableService', function (
$scope, AdminUsersAppraiserPanelService, AdminUsersCtrlInheritanceService, $stateParams, AdminUsersService,
AdminUsersTableCtrlInheritanceService, AdminUsersAppraiserPanelTableService) {
  var vm = this;
  var Service = AdminUsersAppraiserPanelService;
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;
  // Inherit common functions
  AdminUsersCtrlInheritanceService.createCtrl.call(vm, Service, $scope, 'appraiser');

  /**
   * Watch list type and update table accordingly
   */
  $scope.$watch(function () {
    return vm.displayData.listType;
  }, function (newVal, oldVal) {
    if (angular.isUndefined(newVal) || angular.equals(newVal, oldVal)) {
      return;
    }
    // Keep original table definition for reference
    var oldHeading = angular.copy(vm.heading);
    // Get table heading
    vm.heading = Service.alterTableDefinition(newVal);
    // Recompile table if changing structure
    if (!angular.equals(vm.heading, oldHeading)) {
      $scope.recompileTable();
    }
    // Load records for the new view
    Service.loadDataType(newVal, oldVal);
    // Reset multiselect
    vm.multiSelect = Service.resetMultiSelect(newVal, oldVal, vm.multiSelect);
  });

  /**
   * Watch group is not reliable for this. Watch both list types separately
   */
  $scope.$watch(function () {
    return vm.safeData.selectedLists.approved;
  }, function (newVal) {
    if (angular.isUndefined(newVal) || !newVal) {
      return;
    }
    // Retrieve details for this list
    vm.getListDetails('approved', newVal);
  });
  $scope.$watch(function () {
    return vm.safeData.selectedLists['do-not-use'];
  }, function (newVal) {
    if (angular.isUndefined(newVal) || !newVal) {
      return;
    }
    // Retrieve details for this list
    vm.getListDetails('do-not-use', newVal);
  });

  /**
   * Retrieve a list's details
   */
  vm.getListDetails = function (type, id) {
    AdminUsersAppraiserPanelTableService.getListDetails(type, id);
  };

  /**
   * Button disabled for adding appraisers to list
   */
  vm.addToListDisabled = function () {
    return Service.addToListDisabled(vm.multiSelect);
  };
  /**
   * Add appraisers to list
   */
  vm.addToList = function (all) {
    var response;
    if (all) {
      response = Service.addToList();
    } else {
      response = Service.addToList(vm.multiSelect);
    }
    // Display success
    response.then(function () {
      $scope.$broadcast('hide-modal', 'add-all-to-list-confirm');
      $scope.$broadcast('show-modal', 'add-appraiser-success');
      // Failure
    }).catch(function () {
      $scope.$broadcast('show-modal', 'add-appraiser-failure');
    });
  };
  /**
   * Add all appraisers to list confirm
   */
  vm.addAllToListConfirm = function () {
    $scope.$broadcast('show-modal', 'add-all-to-list-confirm');
  };
  /**
   * Delete all appraisers from list confirm
   */
  vm.deleteAllFromListConfirm = function () {
    // Make sure none are selected
    vm.safeData.selectedAppraisersInList.length = 0;
    $scope.$broadcast('show-modal', 'delete-appraisers-confirm');
  };
  /**
   * Disable list details when no list selected
   */
  vm.listFunctionButtonDisabled = function () {
    return !vm.safeData.selectedLists[vm.displayData.listType];
  };
  /**
   * Display list details
   */
  vm.showListDetails = function () {
    $scope.$broadcast('show-modal', 'appraiser-list-details');
  };
  /**
   * Add a new list confirmation
   */
  vm.newListConfirm = function () {
    $scope.$broadcast('show-modal', 'add-new-list');
  };
  /**
   * Add new list
   */
  vm.addNewList = function () {
    // Request
    Service.addNewList()
    .then(function () {
      $scope.$broadcast('hide-modal', 'add-new-list');
      $scope.$broadcast('show-modal', 'add-new-list-success');
      // Clear input
      vm.displayData.newListTitle = '';
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'add-new-list-failure');
    });
  };
  /**
   * Submit new list disabled
   */
  vm.newListSubmitDisabled = function () {
    return !vm.displayData.newListTitle;
  };
  /**
   * Delete list confirm
   */
  vm.deleteListConfirm = function () {
    $scope.$broadcast('show-modal', 'delete-list');
  };
  /**
   * Delete list
   */
  vm.deleteList = function () {
    // Request
    Service.deleteList()
    // Handle response
    .then(function () {
      $scope.$broadcast('hide-modal', 'delete-list');
      $scope.$broadcast('show-modal', 'delete-list-success');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'delete-list-failure');
    });
  };
  /**
   * Delete appraisers from list
   */
  vm.deleteAppraisersFromList = function (confirm) {
    // Delete multiple confirm
    if (confirm) {
      $scope.$broadcast('show-modal', 'delete-appraisers-confirm', true);
      return;
    }
    // request
    Service.deleteAppraisersFromList()
    .then(function () {
      $scope.$broadcast('hide-modal', 'delete-appraisers-confirm');
      $scope.$broadcast('show-modal', 'delete-appraiser-success', true);
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'delete-appraiser-failure', true);
    });
  };
  /**
   * Delete multiple button disabled
   */
  vm.deleteMultipleFromListDisabled = function () {
    //console.log(Service.safeData.deleteMultipleFromListDisabled);
    return Service.safeData.deleteMultipleFromListDisabled;
  };

  /**
   * Import appraisers by document
   * @todo I'm unsure of the response to expect from the backend on failure
   * @link https://github.com/ascope/manuals/issues/170
   */
  vm.importAppraisers = function () {
    $scope.$broadcast('show-modal', 'add-appraisers-by-upload');
  };
  /**
   * Import appraisers document response
   */
  vm.importResponse = function (response) {
    console.log(response);
  };

  /**
   * Inherit table functions
   */
  AdminUsersTableCtrlInheritanceService.inherit.call(vm, $scope, Service, 'appraiser');
}]);