'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersAppraiserListTableCtrl',
['$scope', 'AdminUsersAppraiserPanelTableService', 'AdminUsersAppraiserPanelService',
function ($scope, AdminUsersAppraiserPanelTableService, AdminUsersAppraiserPanelService) {
  var vm = this;
  var Service = AdminUsersAppraiserPanelTableService;

  vm.heading = [
    // Multiselect
    {
      label: '',
      noSort: true,
      checkbox: true
    },
    // Name
    {
      label: 'Name',
      data: 'name'
    },
    // Address
    {
      label: 'Address',
      data: 'address'
    },
    // City
    {
      label: 'City',
      data: 'city'
    },
    // State
    {
      label: 'State',
      data: 'state'
    },
    // Zip
    {
      label: 'Zip',
      data: 'zip'
    },
    // License
    {
      label: 'License Number',
      data: 'licenseNumber'
    },
    // License type
    {
      label: 'License type',
      data: 'licenseType'
    },
    // Delete
    {
      label: 'Delete',
      linkLabel: 'Delete',
      fn: 'delete'
    }
  ];

  /**
   * Create table as we get new records
   */
  $scope.$watchCollection(function () {
    return Service.displayData.appraiserList;
  }, function (newVal) {
    if (angular.isUndefined(newVal)) {
      return;
    }
    // Table data
    vm.tableData = newVal;
    vm.rowData = vm.tableData.slice();
  });

  /**
   * Watch multiselect and see if delete multiple button should be disabled
   */
  $scope.$watchCollection(function () {
    return vm.multiSelect;
  }, function (newVal) {
    if (angular.isUndefined(newVal)) {
      return;
    }
    // Determine if delete multiple button should be disabled
    AdminUsersAppraiserPanelService.safeData.deleteMultipleFromListDisabled =
    AdminUsersAppraiserPanelTableService.deleteMultipleFromListDisabled(vm.multiSelect);
    // Keep reference to selected appraisers
    AdminUsersAppraiserPanelService.getSelectedAppraisersFromMultiSelect(vm.multiSelect);
  });

  /**
   * Clear multiselect on list change
   */
  AdminUsersAppraiserPanelTableService.registerObserver(function () {
    vm.multiSelect = {};
  });

  /**
   * Delete appraiser from list
   */
  vm.linkFn = function (id) {
    // This appraiser only selected
    var selectedAppraisersInList = AdminUsersAppraiserPanelService.safeData.selectedAppraisersInList;
    selectedAppraisersInList.length = 0;
    selectedAppraisersInList.push(id);
    // Show confirmation modal
    $scope.$emit('show-modal', 'delete-appraisers-confirm', true);
  };
}]);