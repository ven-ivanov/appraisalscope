'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersAppraiserPanel', ['$compile', function ($compile) {
  return {
    templateUrl: '/components/admin/users/appraiser-panel/directives/partials/appraiser-panel.html',
    controller: 'AdminUsersAppraiserPanelCtrl',
    controllerAs: 'tableCtrl',
    link: function (scope) {
      /**
       * Recompile table after switching result list types
       */
      scope.recompileTable = function () {
        angular.element('#appraiser-panel-table-wrapper').replaceWith($compile('<div class="col-md-12" id="appraiser-panel-table-wrapper"><as-table totals="false" hide-page-numbers="true" table-id="admin-users-appraiser-panel-table" bold-columns="1,4" infinite-scroll="false" overflow-x="hidden"></as-table></div>')(scope));
      };
    }
  };
}]);