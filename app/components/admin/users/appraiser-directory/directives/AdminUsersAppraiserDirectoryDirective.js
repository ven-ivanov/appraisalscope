'use strict';

var app = angular.module('frontendApp');

/**
 * Admin User - Directory view (appraiser)
 */
app.directive('adminUsersAppraiserDirectory',
['DirectiveInheritanceService', '$compile', function (DirectiveInheritanceService, $compile) {
  return {
    templateUrl: '/components/admin/users/appraiser-directory/directives/partials/directory.html',
    controller: 'AdminUsersAppraiserDirectoryCtrl',
    controllerAs: 'directoryCtrl',
    compile: function () {
      return {
        pre: function (scope) {
          // Expose trigger download
          DirectiveInheritanceService.inheritDirective.call(scope);
        },
        post: function (scope) {
          scope.compileDirectives = function () {
            // Certificates
            angular.element('#admin-users-certification').replaceWith($compile('<admin-users-certification ctrl="directoryCtrl" type="directory" hide="primary,licenseDoc,email"></admin-users-certification>')(scope));
          };
        }
      };
    }
  };
}]);