'use strict';

var app = angular.module('frontendApp');

/**
 * Admin User - Directory service
 */
app.factory('AdminUsersAppraiserDirectoryService',
['AdminUsersService', 'AsDataService', function (AdminUsersService, AsDataService) {
  var Service = {
    // Directory ID
    directoryId: 0,
    // Safe data
    safeData: {
      // Directorys table
      'appraiser-directory': {},
      // Table heading
      heading: [
        // Name
        {
          label: 'Name',
          data: 'name',
          search: true
        },
        // Company name
        {
          label: 'Company',
          data: 'companyName',
          search: true
        },
        // Address
        {
          label: 'Address',
          data: 'address',
          search: true
        }
      ],
      // Directory ratings
      ratings: {},
      // Certification details
      certificationDetails: {},
      // Reports
      directoryReports: {}
    },
    // Display data
    displayData: {
      // Table data
      'appraiser-directory': [],
      // Details
      details: {},
      // States dropdown
      states: [],
      // directory quality data
      ratings: {},
      // directory quality tab values
      ratingTabs: {},
      // Selected tab
      directoryRatingsSelectedTab: '',
      // Certification details
      certificationDetails: [],
      // Directory reports
      directoryReports: [],
      // Yes/no dropdown
      yesNoDropdown: AdminUsersService.yesNoDropdown,
      // Register status
      registerStatus: {
        unregistered: 0,
        inviteSent: 1,
        registered: 2
      },
      // Text for the current section
      sectionText: 'appraiser'
    },
    // Data transformations
    transformers: {
      address: {
        source: 'locations.office.address1',
        dest: 'address'
      },
      companyName: {
        source: 'company.name',
        dest: 'companyName'
      },
      joinedAt: {
        filter: 'date'
      },
      eoExpireAt: {
        source: 'eo.expireAt',
        dest: 'eoExpireAt',
        filter: 'date'
      }
      /**
       * @todo Waiting on the backend for this one
       */
      //// Registration status
      //registeredStatus: AdminUsersService.getRegisteredStatus()
    },
    /**
     * Add type before formatting and hashing
     */
    beforeInitFormat: function (data) {
      angular.forEach(data, function (record) {
        record._type = 'appraiser';
      });
    },
    /**
     * Send invitation to an appraiser
     * @todo Waiting on the backend
     * @link https://github.com/ascope/manuals/issues/153
     */
    inviteAppraiser: function (email) {
      return Service.request('inviteAppraiser', {appraiserId: Service['appraiser-directoryId'], email: email})
      .then(function () {
        // Set to invite sent
        Service.safeData['appraiser-directory'][AdminUsersService.id].registeredStatus = AdminUsersService.registeredStatusValues.invited;
        // Update button text
        Service.displayData.details.registeredStatus = 'Invite sent';
      });
    }
  };

  /**
   * Inherit from AdminUsersService
   */
  Service.loadRecords = AdminUsersService.loadRecords.bind(Service, 'appraiser', 'appraiser-directory');
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'appraiser-directory');
  Service.transformData = AsDataService.transformData.bind(Service);
  // Requests
  Service.request = AdminUsersService.request.bind(Service, 'appraiser');
  // Get record details
  Service.getRecordDetails = AdminUsersService.getRecordDetails.bind(Service, 'appraiser-directory', null);
  // Set state
  Service.setStateOnLoad = AdminUsersService.setStateOnLoad.bind(Service, 'appraiser-directory');
  // Toggle settings
  Service.toggleSetting = AdminUsersService.toggleSetting.bind(Service, 'appraiser-directory');
  // Update record
  Service.updateRecord = AdminUsersService.updateRecord.bind(Service, 'appraiser');
  // Register record
  Service.registerRecord = AdminUsersService.registerRecord.bind(Service, 'appraiser', 'appraiser-directory');

  return Service;
}]);