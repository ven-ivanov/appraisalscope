'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersAppraiserDirectoryTableCtrl',
['$scope', 'AdminUsersAppraiserDirectoryService', 'AdminUsersTableCtrlInheritanceService',
function ($scope, AdminUsersAppraiserDirectoryService, AdminUsersTableCtrlInheritanceService) {

  // Inherit table controller methods
  AdminUsersTableCtrlInheritanceService.inherit.call(this, $scope, AdminUsersAppraiserDirectoryService, 'appraiser-directory');
}]);