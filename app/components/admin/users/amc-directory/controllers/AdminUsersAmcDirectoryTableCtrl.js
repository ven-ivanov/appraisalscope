'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersAmcDirectoryTableCtrl',
['$scope', 'AdminUsersAmcDirectoryService', 'AdminUsersTableCtrlInheritanceService',
 function ($scope, AdminUsersAmcDirectoryService, AdminUsersTableCtrlInheritanceService) {

   // Inherit table controller methods
   AdminUsersTableCtrlInheritanceService.inherit.call(this, $scope, AdminUsersAmcDirectoryService, 'amc-directory');
 }]);