'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Amc directory view controller
 */
app.controller('AdminUsersAmcDirectoryCtrl',
['$scope', 'AdminUsersAmcDirectoryService', '$stateParams', 'AdminUsersService',
 'AdminUsersCtrlInheritanceService', function (
$scope, AdminUsersAmcDirectoryService, $stateParams, AdminUsersService, AdminUsersCtrlInheritanceService) {
  var vm = this,
  // This service name is just getting out of hand with the line breaks
  Service = AdminUsersAmcDirectoryService;
  // Inherit common functions
  AdminUsersCtrlInheritanceService.createCtrl.call(vm, Service, $scope, 'directory', {disableUpdates: true});
  // Directory details
  vm.displayData = Service.displayData;
  // Safe data
  vm.safeData = Service.safeData;
  /**
   * Additional details tabs
   */
    // Tab config
  vm.additionalDetailsTabsConfig = {
    coverage: 'Coverage',
    appraisalForms: 'Appraisal forms'
  };
  // Tab config array (for ordering)
  vm.additionalDetailsTabs =
  ['coverage', 'appraisalForms'];
  // Selected tab
  AdminUsersService.currentDetailsTab = vm.selectedAdditionalDetailsTab = 'coverage';
  vm.changeAdditionalDetailsTab = function (tab) {
    // Keep track of tab
    AdminUsersService.changeUrl({details: tab});
  };

  /**
   * Initiate data and state
   */
  vm.init = function () {
    // Set state on load
    Service.setStateOnLoad($stateParams, vm);
    // Keep reference to which section we're on
    AdminUsersService.section = 'amc-directory';
  };

  // Init controller load
  vm.init();
}]);