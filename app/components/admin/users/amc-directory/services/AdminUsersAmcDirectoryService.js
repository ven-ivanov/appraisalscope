'use strict';

var app = angular.module('frontendApp');

/**
 * Admin User - AMC Directory service
 */
app.factory('AdminUsersAmcDirectoryService',
['$http', 'API_PREFIX', 'AdminUsersService', 'AsDataService', function ($http, API_PREFIX, AdminUsersService, AsDataService) {
  var Service = {
    // Directory ID
    directoryId: 0,
    // Safe data
    safeData: {
      // Directorys table
      'amc-directory': {},
      // Table heading
      heading: [
        // Company name
        {
          label: 'Company',
          data: 'companyName',
          search: true
        }
      ],
      // Directory ratings
      ratings: {},
      // Certification details
      certificationDetails: {},
      // Reports
      directoryReports: {}
    },
    // Display data
    displayData: {
      // Table data
      'amc-directory': [],
      // Details
      details: {},
      // States dropdown
      states: [],
      // directory quality data
      ratings: {},
      // directory quality tab values
      ratingTabs: {},
      // Selected tab
      directoryRatingsSelectedTab: '',
      // Certification details
      certificationDetails: [],
      // Directory reports
      directoryReports: [],
      // Yes/no dropdown
      yesNoDropdown: AdminUsersService.yesNoDropdown,
      // Register status
      registerStatus: {
        unregistered: 0,
        inviteSent: 1,
        registered: 2
      },
      // Text for the current section
      sectionText: 'AMC'
    },
    // Data transformations
    transformers: {
      // Certification display
      certification: AdminUsersService.getCertificationTransformer(),
      // Registered date
      registered: {
        filter: 'date'
      },
      // E&O expiration date
      eoExpirationDate: {
        filter: 'date'
      },
      // Registration status
      registeredStatus: AdminUsersService.getRegisteredStatus()
    }
  };

  /**
   * Inherit from AdminUsersService
   */
  Service.loadRecords = AdminUsersService.loadRecords.bind(Service, 'directory', 'amc-directory');
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'amc-directory');
  Service.transformData = AsDataService.transformData.bind(Service);
  // Get record details
  Service.getRecordDetails = AdminUsersService.getRecordDetails.bind(Service, 'amc-directory', null);
  // Set state
  Service.setStateOnLoad = AdminUsersService.setStateOnLoad.bind(Service, 'amc-directory');
  // Toggle settings
  Service.toggleSetting = AdminUsersService.toggleSetting.bind(Service, 'amc-directory');
  // Update record
  Service.updateRecord = AdminUsersService.updateRecord.bind(Service, 'amc-directory');
  // Register record
  Service.registerRecord = AdminUsersService.registerRecord.bind(Service, 'amc', 'amc-directory');

  return Service;
}]);