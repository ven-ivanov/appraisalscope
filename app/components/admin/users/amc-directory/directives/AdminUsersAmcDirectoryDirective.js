'use strict';

var app = angular.module('frontendApp');

/**
 * Admin User - Directory view (amc)
 */
app.directive('adminUsersAmcDirectory', ['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/users/amc-directory/directives/partials/directory.html',
    controller: 'AdminUsersAmcDirectoryCtrl',
    controllerAs: 'directoryCtrl',
    compile: function () {
      return {
        pre: function (scope) {
          // Expose trigger download
          DirectiveInheritanceService.inheritDirective.call(scope);
        }
      };
    }
  };
}]);