describe('AdminUsersEmailUsersService', function () {
  var adminUsersService, Service, httpBackend, scope, asDataService;

  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function (AdminUsersService, $rootScope, $httpBackend, AdminUsersEmailUsersService, AsDataService) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    adminUsersService = AdminUsersService;
    Service = AdminUsersEmailUsersService;
    asDataService = AsDataService;

    httpBackend.whenGET(/client\/members/).respond({data: [{id:1, name: 'me', email: 'me@me.com'}]});
    httpBackend.whenGET().respond(/appraisement\/members/).respond({data: [{id: 2, name: 'someone', email: 'else@else.com'}, {id: 3, name: 'WHO KNOWS', contact: {email: 'you@do.com'}}]});
  }));

  describe('searchRecipients', function () {
    beforeEach(function () {
      Service.displayData.emailRecipientsList = [1];
      spyOn(Service, 'searchEmails');
      Service.searchRecipients('query');
    });

    it('should call searchEmails', function () {
      expect(Service.searchEmails).toHaveBeenCalledWith('query', [ 1 ]);
    });
  });

  describe('searchSenders', function () {
    beforeEach(function () {
      Service.displayData.emailSendersList = [2];
      spyOn(Service, 'searchEmails');
      Service.searchSenders('query2');
    });

    it('should call searchEmails', function () {
      expect(Service.searchEmails).toHaveBeenCalledWith('query2', [ 2 ]);
    });
  });

  describe('searchEmails', function () {
    var result;
    beforeEach(function () {
      result = Service.searchEmails('a', [{id:1, name: 'a@a.com'}, {id:2, name: 'b@a.com'}, {id:3, name: 'c@c.com'}]);
    });

    it('should find emails by simple string comparison', function () {
      expect(result).toEqual([ { id: 1, name: 'a@a.com' }, { id: 2, name: 'b@a.com' } ]);
    });
  });

  describe('generateRecipientList', function () {
    beforeEach(function () {
      spyOn(Service, 'generateSenderList');
      Service.generateRecipientList();
      httpBackend.flush();
      scope.$digest();
    });

    it('should generate a list of email recipients', function () {
      expect(Service.displayData.emailRecipientsList).toEqual([ { id: 1, name: 'me <me@me.com>' }, { id: 2, name: 'someone <else@else.com>' }, { id: 3, name: 'WHO KNOWS <you@do.com>' } ] );
    });
    it('should generate sender list', function () {
      expect(Service.generateSenderList).toHaveBeenCalled();
    });
  });

  // @todo When the backend is ready
  xdescribe('generateSenderList', function () {

  });

  describe('sendEmail', function () {
    beforeEach(function () {
      spyOn(Service, 'request');
      Service.displayData.email.sender = [{id: 18, name: 'sender <superman@crystalpalace.com>'}];
      Service.displayData.email.receiver = [{id:1, name: 'blah <blah@blah.com>'}, {id:4, name: 'yup <yippers@skippers.com>'}];
      Service.displayData.email.subject = 'A good subject';
      Service.displayData.email.content = 'Content';
      Service.sendEmail();
    });
    it('should make the request with the right parameters', function () {
      expect(Service.request).toHaveBeenCalledWith('sendEmail', { sender: 18, receiver: [ 1, 4 ], subject: 'A good subject', message: 'Content' });
    });
  });

  describe('getEmailDetails', function () {
    beforeEach(function () {
      spyOn(Service, 'hashData');
      spyOn(asDataService, 'formatPlainObjectData');
      Service.safeData.emails = {1: {hi: 'hi'}, 2: {bye: 'bye'}};
      Service.getEmailDetails(1);
    });

    it('should hash the selected email', function () {
      expect(Service.hashData).toHaveBeenCalledWith({ hi: 'hi' }, 'emailDetails');
    });
    it('should format the data', function () {
      expect(asDataService.formatPlainObjectData).toHaveBeenCalled();
    });
  });
});