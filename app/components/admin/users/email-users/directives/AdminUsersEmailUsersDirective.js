'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersEmailUsers', [function () {
  return {
    templateUrl: '/components/admin/users/email-users/directives/partials/email-users.html',
    controller: 'AdminUsersEmailUsersCtrl',
    controllerAs: 'emailUsersCtrl'
  };
}]);