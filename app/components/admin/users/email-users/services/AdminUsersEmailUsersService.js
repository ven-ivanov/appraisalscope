'use strict';

var app = angular.module('frontendApp');

/**
 * Email users service
 */
app.factory('AdminUsersEmailUsersService',
['AdminUsersService', 'AsDataService', function (AdminUsersService, AsDataService) {
  var Service = {
    safeData: {
      // Emails
      emails: {},
      // Main
      heading: [
        {
          label: 'Sent to',
          data: 'sentTo',
          search: true
        },
        {
          label: 'Subject',
          data: 'subject',
          search: true
        },
        {
          label: 'Sent by',
          data: 'sentBy',
          search: true
        },
        {
          label: 'Sent date',
          data: 'sentAt',
          search: true
        },
        {
          label: 'Status',
          data: 'status',
          search: true
        }
      ]
    },
    displayData: {
      emails: [],
      // New email
      email: {
        receiver: [],
        sender: [],
        content: '',
        subject: ''
      },
      // Potential recipients
      emailRecipientsList: [],
      // Potential senders
      emailSendersList: [],
      // Email details
      emailDetails: {}
    },
    transformers: {
      sentTo: {
        source: 'receiver.name',
        dest: 'sentTo'
      },
      sentBy: {
        source: 'sender.name',
        dest: 'sentBy'
      },
      sentAt: {
        filter: 'date'
      },
      status: {
        filter: 'capitalizeFirst'
      }
    },
    /**
     * Perform a basic search for recipients
     * @param query
     */
    searchRecipients: function (query) {
      return Service.searchEmails(query, Service.displayData.emailRecipientsList);
    },
    /**
     * Search the potential list of senders and find those that match
     */
    searchSenders: function (query) {
      return Service.searchEmails(query, Service.displayData.emailSendersList);
    },
    /**
     * Basic email string comparison
     */
    searchEmails: function (query, emails) {
      var emailLength = emails.length;
      var foundEmails = [];
      for (var i = 0; i < emailLength; i = i + 1) {
        // Only return 10 results max
        if (foundEmails.length >= 10) {
          break;
        }
        // Add found emails to return array
        if (emails[i].name.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
          foundEmails.push(emails[i]);
        }
      }
      return foundEmails;
    },
    /**
     * Generate recipient list for searching available email addresses. Generated from all clients and appraiser members
     */
    generateRecipientList: function () {
      // Retrieve client and appraiser members
      var emails = Service.displayData.emailRecipientsList;
      Service.request('getRecipientList').then(function (response) {
        // Add client members
        angular.forEach(response.client, function (record) {
          if (angular.isDefined(record.email)) {
            emails.push({id: record.id, name: record.name + ' <' + record.email + '>'});
          }
        });
        // Add appraiser members
        angular.forEach(response.appraiser, function (record) {
          if (angular.isDefined(record.email)) {
            emails.push({id: record.id, name: record.name + ' <' + record.email + '>'});
          } else {
            // Get from contact for appraiser companies
            if (angular.isDefined(record.contact) && angular.isString(record.contact.email)) {
              emails.push({id: record.id, name: record.name + ' <' + record.contact.email + '>'});
            }
          }
        });
      });
      // Get senders
      Service.generateSenderList();
    },
    /**
     * Generate possible sender options
     *
     * @todo Need to get what was previously in scope_companyinfo in v1
     * @link https://github.com/ascope/manuals/issues/179
     * @link https://github.com/ascope/manuals/issues/180
     */
    generateSenderList: function () {
      // This is fake until the endpoints are in place
      Service.displayData.emailSendersList = [{id:1, name: 'Fakey Faker <fake@fake.com>'}, {id:2, name: 'Testy Tester <test@test.com>'}];
    },
    /**
     * Send email
     */
    sendEmail: function () {
      // Sender
      var sender = Service.displayData.email.sender[0].id;
      // Recipients
      var to = Service.displayData.email.receiver;
      var receiver = [];
      // Generate simple array of emails
      to.forEach(function (recipient) {
        receiver.push(recipient.id);
      });
      // Send email
      return Service.request('sendEmail', {sender: sender, receiver: receiver, subject: Service.displayData.email.subject, message: Service.displayData.email.content});
    },
    /**
     * Retrieve email details
     * @param id Email id
     */
    getEmailDetails: function (id) {
      // Hash and set for display
      Service.hashData(Service.safeData.emails[id], 'emailDetails');
      AsDataService.formatPlainObjectData.call(Service, 'emailDetails');
    }
  };

  /**
   * Inherit from AdminUsersService and AsDataService
   */
  Service.request = AdminUsersService.request.bind(Service, 'emails');
  Service.loadRecords = AdminUsersService.loadRecords.bind(Service, 'emails', Service.generateRecipientList);
  // Hash
  Service.hashData = AsDataService.hashData.bind(Service);
  // Format table data
  Service.formatData = AsDataService.formatData.bind(Service, 'emails');
  // Transform
  Service.transformData = AsDataService.transformData.bind(Service);
  return Service;
}]);