'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersEmailUsersCtrl',
['$scope', 'AdminUsersEmailUsersService', 'AdminUsersCtrlInheritanceService', 'CKEDITOR_CONFIG', function (
$scope, AdminUsersEmailUsersService, AdminUsersCtrlInheritanceService, CKEDITOR_CONFIG) {
  var vm = this;
  var Service = AdminUsersEmailUsersService;
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;
  // Inherit common functions
  AdminUsersCtrlInheritanceService.createCtrl.call(vm, Service, $scope, 'appraiser');
  // Ckeditor config
  $scope.editorOptions = CKEDITOR_CONFIG.config;

  /**
   * Compose a new email
   */
  vm.composeEmail = function () {
    $scope.$broadcast('show-modal', 'compose-email');
  };

  /**
   * Basic search to find recipients
   * @param query
   */
  vm.searchRecipients = function (query) {
    return Service.searchRecipients(query);
  };
  /**
   * Search from the available list of senders
   * @param query
   */
  vm.searchSenders = function (query) {
    return Service.searchSenders(query);
  };

  /**
   * Make sure we have at least one good recipient
   */
  vm.disableSendEmail = function () {
    return !Service.displayData.email.receiver.length || vm.sendEmailForm.$invalid;
  };

  /**
   * Send email
   */
  vm.sendEmail = function () {
    Service.sendEmail()
    .then(function () {
      $scope.$broadcast('hide-modal', 'compose-email');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'send-email-failure', true);
    });
  };
}]);