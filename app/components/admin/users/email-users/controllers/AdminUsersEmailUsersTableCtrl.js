'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersEmailUsersTableCtrl', ['$scope', 'AdminUsersEmailUsersService', 'AdminUsersTableCtrlInheritanceService',
function ($scope, AdminUsersEmailUsersService, AdminUsersTableCtrlInheritanceService) {
  var vm = this;
  var Service = AdminUsersEmailUsersService;

  // Inherit table controller methods
  AdminUsersTableCtrlInheritanceService.inherit.call(vm, $scope, Service, 'emails');

  /**
   * Display emails on row click
   */
  vm.rowFn = function (id) {
    // Retrieve details
    Service.getEmailDetails(id);
    // Show modal
    $scope.$parent.$broadcast('show-modal', 'email-details');
  };
}]);