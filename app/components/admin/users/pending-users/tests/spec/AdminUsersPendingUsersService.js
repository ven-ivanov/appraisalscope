describe('AdminUsersPendingUsersService', function () {
  var scope, httpBackend, Service, asDataService, adminUsersService;

  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function ($rootScope, $httpBackend, AdminUsersPendingUsersService, AsDataService, AdminUsersService) {
    httpBackend = $httpBackend;
    scope = $rootScope.$new();
    Service = AdminUsersPendingUsersService;
    asDataService = AsDataService;
    adminUsersService = AdminUsersService;
    
    spyOn(Service, 'formatData');
    spyOn(asDataService, 'formatData');

    httpBackend.whenPATCH(/.*/).respond();
  }));

  describe('beforeInitFormat', function () {
    var records;
    describe('company', function () {
      it('should gracefully ignore non-appraiser records', function () {
        records = [{id: 1}, {id: 2}];
        Service.beforeInitFormat(records);
        expect(records).toEqual([{id: 1}, {id: 2}]);
      });
    });

    describe('appraiser', function () {
      beforeEach(function () {
        records = [{locations: {office: {city: 'Test city', state: {name: 'Test state'}, zip: 10055}}}];
        Service.beforeInitFormat(records);
      });

      it('should alter the records to include city, state, zip string', function () {
        expect(records).toEqual([{
                                   locations: {
                                     office: {
                                       city: 'Test city',
                                       state: {name: 'Test state'},
                                       zip: 10055
                                     }
                                   },
                                   appraiserCityStateZip: 'Test city, Test state, 10055'
                                 }]);
      });
    });
  });

  describe('update', function () {
    beforeEach(function () {
      Service.safeData.selectedRecord = 1;
      Service.displayData.fnType = 'approve';
    });

    describe('appraiser', function () {
      beforeEach(function () {
        Service.safeData.appraiser = {1: {id: 1, test: 'test1'}, 2: {id: 2, test: 'test2'}};
        Service.displayData.recordType = 'appraiser';
        Service.update();
        httpBackend.flush();
        scope.$digest();
      });

      it('should delete the selected record', function () {
        expect(Service.safeData.appraiser).toEqual({ 2: { id: 2, test: 'test2' } });
      });
      it('should format the table', function () {
        expect(Service.formatData).toHaveBeenCalled();
      });
    });

    describe('company', function () {
      beforeEach(function () {
        Service.safeData.company = {1: {id: 1, test: 'test1'}, 2: {id: 2, test: 'test2'}};
        Service.displayData.recordType = 'company';
        Service.update();
        httpBackend.flush();
        scope.$digest();
      });

      it('should delete the selected record', function () {
        expect(Service.safeData.company).toEqual({ 2: { id: 2, test: 'test2' } });
      });
      it('should format the table', function () {
        expect(asDataService.formatData).toHaveBeenCalled();
      });
    });
  });
});