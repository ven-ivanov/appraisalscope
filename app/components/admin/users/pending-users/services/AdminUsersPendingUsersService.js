'use strict';

var app = angular.module('frontendApp');

/**
 * Pending users service
 */
app.factory('AdminUsersPendingUsersService',
['AdminUsersService', 'AsDataService', function (AdminUsersService, AsDataService) {
  var Service = {
    safeData: {
      // Appraisers
      appraiser: {},
      // Companies
      company: {},
      // Selected appraiser or company
      selectedRecord: 0,
      // Both table configurations
      tableDefinitions: {
        // Pending appraisers table
        appraiser: [
          // First name
          {
            label: 'First name',
            data: 'firstName',
            search: true
          },
          // Last name
          {
            label: 'Last name',
            data: 'lastName',
            search: true
          },
          // Company name
          {
            label: 'Company name',
            data: 'appraiserCompanyName',
            search: true
          },
          // Registered date
          {
            label: 'Registered date',
            data: 'joinedAt',
            search: true
          },
          // City, state, zip
          {
            label: 'City, state, zip',
            data: 'appraiserCityStateZip',
            search: true
          },
          // Certification
          {
            label: 'Certification',
            data: 'appraiserCertification',
            search: true
          },
          // Office phone
          {
            label: 'Office phone',
            data: 'appraiserOfficePhone',
            search: true
          },
          {
            label: 'Approve',
            linkLabel: 'approve',
            fn: 'approve'
          },
          {
            label: 'Disable',
            linkLabel: 'disable',
            fn: 'disable'
          }
        ],
        // Pending companies table
        company: [
          // Company name
          {
            label: 'Company name',
            data: 'name',
            search: true
          },
          // Address
          {
            label: 'Address',
            data: 'companyAddress',
            search: true
          },
          // City
          {
            label: 'City',
            data: 'companyCity',
            search: true
          },
          // State
          {
            label: 'State',
            data: 'companyState',
            search: true
          },
          // Zip
          {
            label: 'Zip',
            data: 'companyZip',
            search: true
          },
          // Phone
          {
            label: 'Phone',
            data: 'companyPhone',
            search: true
          },
          // Account type
          {
            label: 'Account type',
            data: 'accountType',
            search: true
          },
          {
            label: 'Approve',
            linkLabel: 'approve',
            fn: 'approve'
          },
          {
            label: 'Delete',
            linkLabel: 'delete',
            fn: 'delete'
          }
        ]
      }
    },
    displayData: {
      appraiser: [],
      company: [],
      // Function being performed on selected record
      fnType: ''
    },
    // Transformations for display in table
    transformers: {
      /**
       * Appraiser transformers
       */
      appraiserCompanyName: {
        source: 'company.name',
        dest: 'appraiserCompanyName'
      },
      joinedAt: {
        filter: 'date'
      },
      appraiserCertification: {
        source: 'certification.certificate.type',
        dest: 'appraiserCertification',
        filter: 'replaceChar:-: '
      },
      appraiserOfficePhone: {
        source: 'phones.primary.number',
        dest: 'appraiserOfficePhone'
      },
      companyAddress: {
        source: 'contact.location.address1',
        dest: 'companyAddress'
      },
      companyCity: {
        source: 'contact.location.city',
        dest: 'companyCity'
      },
      companyState: {
        source: 'contact.location.state.name',
        dest: 'companyState'
      },
      companyZip: {
        source: 'contact.location.zip',
        dest: 'companyZip'
      },
      companyPhone: {
        source: 'contact.phones.number',
        dest: 'companyPhone'
      }
    },
    /**
     * Create appraiser city, state, zip before hashing
     */
    beforeInitFormat: function (records) {
      var office;
      // Get office city, state, zip for appraisers
      try {
        angular.forEach(records, function (appraiser) {
          office = appraiser.locations.office;
          appraiser.appraiserCityStateZip = office.city + ', ' + office.state.name + ', ' + office.zip;
        });
        // Ignore for companies
      } catch (e) {}
    },
    /**
     * Approve a company or appraiser
     */
    update: function () {
      // Record ID and type (appraiser or company)
      var id = Service.safeData.selectedRecord;
      var type = Service.displayData.recordType;
      // make request and update table
      return Service.request(Service.displayData.fnType, {type: type, id: id})
      .then(function () {
        // Remove from safe data
        delete Service.safeData[type][id];
        // Reformat table
        if (type === 'appraiser') {
          Service.formatData();
        } else if (type === 'company') {
          AsDataService.formatData.call(Service, 'company');
        }
      });
    }
  };

  // Begin on showing all records results
  Service.safeData.heading = Service.safeData.tableDefinitions.all;

  /**
   * Inherit from AdminUsersService and AsDataService
   */
  Service.request = AdminUsersService.request.bind(Service, 'pendingUsers');
  Service.loadRecords = AdminUsersService.loadRecords.bind(Service, 'pendingUsers', 'appraiser');
  // Hash
  Service.hashData = AsDataService.hashData.bind(Service);
  // Format table data
  Service.formatData = AsDataService.formatData.bind(Service, 'appraiser');
  // Transform
  Service.transformData = AsDataService.transformData.bind(Service);
  return Service;
}]);