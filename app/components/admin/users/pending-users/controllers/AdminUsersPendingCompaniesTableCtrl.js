'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersPendingCompaniesTableCtrl', ['$scope', 'AdminUsersPendingUsersService', 'AdminUsersService', 'AsDataService',
function ($scope, AdminUsersPendingUsersService, AdminUsersService, AsDataService) {
  var vm = this;
  var Service = AdminUsersPendingUsersService;
  // Safe data
  vm.safeData = Service.safeData;
  // Heading
  vm.heading = Service.safeData.tableDefinitions.company;

  /**
   * Init data load. Custom load records and format data
   */
  vm.init = function () {
    AdminUsersService.loadRecords.call(Service, 'pendingUsers', 'company', function () {
      AsDataService.formatData.call(Service, 'company');
    });
  };

  /**
   * Watch table data
   */
  $scope.$watchCollection(function () {
    return Service.displayData.company;
  }, function (newVal) {
    if (!angular.isArray(newVal)) {
      return;
    }
    // Table data
    vm.tableData = newVal;
    vm.rowData = vm.tableData.slice();
  });

  /**
   * Approve or disable
   * @param id Record ID
   * @param fn Function name
   */
  vm.linkFn = function (id, fn) {
    // Store reference to selected appraiser and function type
    Service.safeData.selectedRecord = id;
    Service.displayData.fnType = fn === 'delete' ? 'deleteCompany' : fn;
    Service.displayData.recordType = 'company';
    // Display confirmation
    $scope.$parent.$broadcast('show-modal', 'update-function');
  };

  // Load records
  vm.init();
}]);