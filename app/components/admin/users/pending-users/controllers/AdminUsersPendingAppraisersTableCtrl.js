'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersPendingAppraisersTableCtrl', ['$scope', 'AdminUsersPendingUsersService', 'AdminUsersTableCtrlInheritanceService',
function ($scope, AdminUsersPendingUsersService, AdminUsersTableCtrlInheritanceService) {
  var vm = this;
  var Service = AdminUsersPendingUsersService;
  // Set heading
  var heading = AdminUsersPendingUsersService.safeData.tableDefinitions.appraiser;

  // Inherit table controller methods
  AdminUsersTableCtrlInheritanceService.inherit.call(this, $scope, AdminUsersPendingUsersService, heading, 'appraiser');

  /**
   * Approve or disable
   * @param id Record ID
   * @param fn Function name
   */
  vm.linkFn = function (id, fn) {
    // Store reference to selected appraiser and function type
    Service.safeData.selectedRecord = id;
    Service.displayData.fnType = fn;
    Service.displayData.recordType = 'appraiser';
    // Display confirmation
    $scope.$parent.$broadcast('show-modal', 'update-function');
  };

  vm.rowFn = angular.noop;
}]);