'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersPendingUsersCtrl',
['$scope', 'AdminUsersPendingUsersService', 'AdminUsersCtrlInheritanceService', function (
$scope, AdminUsersPendingUsersService, AdminUsersCtrlInheritanceService) {
  var vm = this;
  var Service = AdminUsersPendingUsersService;
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;
  // Inherit common functions
  AdminUsersCtrlInheritanceService.createCtrl.call(vm, Service, $scope, 'appraiser');

  /**
   * Update any given record
   */
  vm.updateRecord = function () {
    // Make request
    Service.update().then(function () {
      $scope.$broadcast('hide-modal', 'update-function');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'update-failure');
    });
  };
}]);