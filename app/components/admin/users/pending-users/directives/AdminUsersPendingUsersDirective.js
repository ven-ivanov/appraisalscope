'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersPendingUsers', [function () {
  return {
    templateUrl: '/components/admin/users/pending-users/directives/partials/pending-users.html',
    controller: 'AdminUsersPendingUsersCtrl',
    controllerAs: 'pendingUsersCtrl'
  };
}]);