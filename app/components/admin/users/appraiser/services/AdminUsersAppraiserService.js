'use strict';

var app = angular.module('frontendApp');

/**
 * Admin User - Appraiser service
 */
app.factory('AdminUsersAppraiserService',
['AdminUsersService', 'AdminUsersResourceService', '$q', 'AsDataService', '$filter', function (AdminUsersService, AdminUsersResourceService, $q, AsDataService, $filter) {
  var Service = {
    // Appraiser ID
    appraiserId: 0,
    // Display disabled
    disabled: false,
    // Safe data
    safeData: {
      // Appraiser table
      appraiser: {},
      // Approved table
      approvedList: {},
      // DNU table
      dnuList: {},
      // Appraiser statistics
      appraiserStats: {},
      // Appraiser quality
      ratings: {},
      // Certification details
      certificationDetails: {},
      // Appraisal reports
      appraiserReports: {},
      // Selected document
      selectedDoc: {},
      // W9 form on IRS site
      w9FormUrl: 'http://www.irs.gov/pub/irs-pdf/fw9.pdf',
      // Table heading
      heading: [
        // Name
        {
          label: 'Name',
          data: 'fullName',
          search: true
        },
        // Record type
        {
          label: 'Type',
          data: 'recordType',
          search: true
        },
        // Certification
        {
          label: 'Certification',
          data: 'certificationType',
          search: true
        },
        // FHA
        {
          label: 'FHA',
          data: 'fha',
          search: true
        },
        // Rating
        {
          label: 'Rating',
          data: 'rating',
          search: true
        }
      ]
    },
    // Display data
    displayData: {
      // Table data
      appraiser: [],
      // Appraiser details
      details: {},
      // Appraiser ID
      appraiserId: 0,
      // Appraiser stats
      appraiserStats: [],
      // Appraiser quality data
      ratings: {},
      // Appraiser quality tab values
      ratingTabs: {},
      // Selected tab
      appraiserRatingsSelectedTab: '',
      // Certification details
      certificationDetails: [],
      // Appraisal reports
      appraiserReports: [],
      // w9 form
      w9: {},
      // w9 options
      w9Options: {
        // Business type
        businessType: [
          {
            id: 'individual-sole-proprietor',
            value: 'Individual/sole proprietor'
          },
          {
            id: 'c-corporation',
            value: 'C corporation'
          },
          {
            id: 's-corporation',
            value: 'S corporation'
          },
          {
            id: 'partnership',
            value: 'Partnership'
          },
          {
            id: 'limited-liability-company',
            value: 'Limited liability corporation'
          },
          {
            id: 'trust-estate',
            value: 'Trust/estate'
          },
          {
            id: 'other',
            value: 'other'
          }
        ],
        // Tax classification
        taxClassification: [
          {
            id: 'd',
            value: 'D'
          },
          {
            id: 'c',
            value: 'C'
          },
          {
            id: 'p',
            value: 'P'
          }
        ]
      },
      // Yes/no dropdown
      yesNoDropdown: AdminUsersService.yesNoDropdown
    },
    // Table heading
    tableHeading: [],
    transformers: {
      // Custom transformations
      custom: {
        // Apply custom transformations
        source: function (record) {
          switch (record.memberType) {
            case 'appraiser':
              // Type
              record._type = 'appraiser';
              // Date formatting
              record.joinedAtFormatted = record.joinedAt;
              // Determine if also manager
              record.recordType = record.isManager ? 'Appraiser/Manager' : 'Appraiser';
              // FHA
              record.fha = record.certification.certificate.fhaApproved ? 'Yes' : 'No';
              // Rating
              record.rating = record.ratingSummary.quality;
              break;
            case 'manager':
              // Type
              record._type = 'manager';
              // Join date
              record.joinedAtFormatted = record.joinedAt;
              // Add record type
              record.recordType = 'Company Manager';
              break;
            // Companies (both new and from /members)
            default:
              // Contact name
              if (angular.isObject(record.contact)) {
                record.firstName = record.contact.firstName;
                record.lastName = record.contact.lastName;
              }
              record.fullName = record.name;
              // Type
              record._type = 'appraiser-company';
              // Record type
              record.recordType = 'Company';
              break;
          }
        },
        // Undo custom transformations
        revert: function (record) {
          if (record._type === 'appraiser-company') {
            record.fullName = Service.displayData.details.name;
          }
        }
      },
      // Registered date (just for the placeholder)
      joinedAt: {
        filter: 'date'
      },
      // E&O expiration date
      eoExpireAt: {
        source: 'eo.expireAt',
        dest: 'eoExpireAt',
        filter: 'date'
      },
      // E&O per claim amount
      eoClaimAmount: {
        filter: 'currency',
        source: 'eo.claimAmount',
        dest: 'eoClaimAmount'
      },
      // E&O aggregate amount
      eoAggregateAmount: {
        filter: 'currency',
        source: 'eo.aggregateAmount',
        dest: 'eoAggregateAmount'
      },
      address1: {
        source: 'locations.office.address1'
      },
      address2: {
        source: 'locations.office.address2'
      },
      city: {
        source: 'locations.office.city'
      },
      state: {
        source: 'locations.office.state.code'
      },
      zip: {
        source: 'locations.office.zip'
      },
      vendorStatus: {
        filter: 'capitalizeFirst'
      },
      certificationType: {
        source: 'certification.certificate.type',
        filter: 'replaceChar:-: |capitalizeFirst'
      }
    },
    /**
     * Request wrapper
     */
    request: function (requestFn, params) {
      // Get $resource configuration
      return AdminUsersResourceService.appraiser[requestFn](params);
    },
    /**
     * Get approved lists for this appraiser
     */
    getApprovedLists: function () {
      return Service.request('approvedList')
      .then(function (response) {
        // Hash and return data
        Service.hashData(response.data, 'approvedList');
        return response.data;
      });
    },
    /**
     * Get DNU lists
     */
    getDnuLists: function () {
      return Service.request('doNotUseList')
      .then(function (response) {
        // Hash and return
        Service.hashData(response.data, 'dnuList');
        return response.data;
      });
    },
    /**
     * Get statistics for the currently selected appraiser
     */
    getStatistics: function (heading) {
      var displayData;
      if (!heading) {
        throw Error('Header must be passed in to getStatistics');
      }
      return Service.request('statistics')
      .then(function (response) {
        // Hash and format for display
        Service.hashData(response, 'appraiserStats');
        displayData = [{}, {}];
        // Label
        displayData[0].label = 'Percentage';
        displayData[1].label = 'Total';
        // Accepted orders
        Service.createStatisticsColumn(displayData, response, 'accepted');
        // Declined orders
        Service.createStatisticsColumn(displayData, response, 'declined');
        // Late orders
        Service.createStatisticsColumn(displayData, response, 'late');
        // On time orders
        Service.createStatisticsColumn(displayData, response, 'onTime');
        // Open orders
        Service.createStatisticsColumn(displayData, response, 'open');
        // Unaccepted orders
        Service.createStatisticsColumn(displayData, response, 'unaccepted');
        // Due date requests
        Service.createStatisticsColumn(displayData, response, 'dueDate', 'requests');
        // Fee increase requests
        Service.createStatisticsColumn(displayData, response, 'feeIncrease', 'requests');
        // Set for display
        Service.displayData.appraiserStats = displayData;
      });
    },
    /**
     * Generates a statistics table column
     *
     * Note: This is probably a temporary function once the table is removed and visualization is added
     * See task in PT 92738230
     */
    createStatisticsColumn: function (displayData, response, type, orders) {
      orders = orders || 'orders';
      displayData[0][type] = response[orders][type].percentage.toFixed(2) + '%';
      // @todo Waiting on the backend for this
      //displayData[1][type] = response[orders][type].number + '/' + response[orders][type].total;
    },
    /**
     * Add a new appraisal after upload
     */
    addNewAppraisal: function (data) {
      // Add to safe data
      Service.safeData.appraiserReports[data.id] = data;
      // Add to display data
      Service.displayData.appraiserReports.push(data);
    },
    /**
     * If an appraiser is selected, get appraisals and hash on appraiser record
     */
    getAppraisals: function () {
      if (AdminUsersService.id && Service.safeData.appraiser[AdminUsersService.id] &&
          Service.safeData.appraiser[AdminUsersService.id].memberType === 'appraiser') {
        return $q.all([
          Service.queryAppraisalQueue('pending'),
          // Get all since the beginning
          Service.queryAppraisals('[completedAtSince][year]', '1970', 'completed')
        ]);
      } else {
        return $q(function (resolve) {
          resolve();
        });
      }
    },
    /**
     * Callback on record load to retrieve assigned appraisals
     */
    queryAppraisalQueue: function (queueType) {
      // Don't query more than once
      if (Service.checkAppraisalQueried(queueType)) {
        // Don't query again
        return $q(function (resolve) {
          resolve();
        });
      }
      // Get assigned appraisals
      return Service.request('queryAppraisalQueue', queueType)
      .then(function (response) {
        // Add to the selected appraiser record
        Service.addAppraisalsToAppraiser(response.data, queueType);
      });
    },
    /**
     * Query appraisals not in a queue
     * @param filter Properly formatted filter (as in: [completedAtSince][year])
     * @param filterArg Argument for filter (as in: 1970)
     * @param appraisalType Property under which to save the appraisal ('completed' would save under
     * Service.safeData.appraiser[AdminUsersService.id]._appraisals.completed)
     */
    queryAppraisals: function (filter, filterArg, appraisalType) {
      // Don't query more than once
      if (Service.checkAppraisalQueried(filter)) {
        // Don't query again
        return $q(function (resolve) {
          resolve();
        });
      }
      // Make request
      return Service.request('queryAppraisals', {filter: filter, filterArg: filterArg})
      .then(function (response) {
        // Add to the selected appraiser record
        Service.addAppraisalsToAppraiser(response.data, appraisalType);
      });
    },
    /**
     * Check if an appraisal has already been queried before querying again
     * @returns {*}
     */
    checkAppraisalQueried: function (type) {
      // Add property for storing appraisals
      if (angular.isUndefined(Service.safeData.appraiser[AdminUsersService.id]._appraisals)) {
        Service.safeData.appraiser[AdminUsersService.id]._appraisals = {};
      }
      // Check if it's actually an object, and if it's populated
      return (angular.isDefined(Service.safeData.appraiser[AdminUsersService.id]._appraisals[type]) &&
              Object.getPrototypeOf(Service.safeData.appraiser[AdminUsersService.id]._appraisals[type]) ===
              Object.prototype &&
              Object.keys(Service.safeData.appraiser[AdminUsersService.id]._appraisals[type]).length);
    },
    /**
     * Save appraisals on a selected appraiser record
     */
    addAppraisalsToAppraiser: function (data, queueType) {
      // Hash and add to appraiser record
      Service.hashData(data, '_' + queueType);
      Service.safeData.appraiser[AdminUsersService.id]._appraisals[queueType] = angular.copy(Service.safeData['_' + queueType]);
      // Delete temporary property
      delete Service.safeData['_' + queueType];
    },
    /**
     * Determine filter params at runtime (for disabled view)
     */
    filterParams: function () {
      if (Service.disabled) {
        return 'filter[isActive]=false';
      }
      return '';
    },
    /**
     * Retrieve appraiser w9 info for modal
     */
    getW9Info: function () {
      Service.displayData.w9 = angular.copy(Service.displayData.details.w9);
      Service.displayData.w9.signedAt = $filter('date')(Service.displayData.w9.signedAt, 'MM/dd/yyyy');
    },
    /**
     * Submit w9
     */
    submitW9: function () {
      var w9 = Service.displayData.w9;
      // Convert back to ISO string
      w9.signedAt = new Date(w9.signedAt).toISOString();
      // Convert 'other' to null
      w9.business.type = w9.business.type === 'other' ? null : w9.business.type;
      return Service.request('updateW9', w9);
    },
    /**
     * Get appraiser manager permissions
     * @todo Waiting on permissions on the backend
     * @link https://github.com/ascope/manuals/issues/453
     */
    getAppraiserManagerPermissions: function (id) { // jshint ignore:line
      return $q(function (resolve) {
        resolve();
      });
    }
  };
  Service.loadRecords =
  AdminUsersService.loadRecords.bind(Service, 'appraiser', null, null, Service.formatRawRecords, Service.filterParams);
  // Hash
  Service.hashData = AsDataService.hashData.bind(Service);
  // Format table data
  Service.formatData = AsDataService.formatData.bind(Service, 'appraiser', Service.formatCallback);
  // Transform
  Service.transformData = AsDataService.transformData.bind(Service);
  // Get record details
  Service.getRecordDetails = AdminUsersService.getRecordDetails.bind(Service, 'appraiser');
  // Set state on load
  Service.setStateOnLoad = AdminUsersService.setStateOnLoad.bind(Service, 'appraiser');
  // Toggle setting
  Service.toggleSetting = AdminUsersService.toggleSetting.bind(Service, 'appraiser');
  // Send login credentials
  Service.sendLogin = AdminUsersService.sendLogin.bind(Service, 'appraiser');
  // Ratings
  Service.getRatings = AdminUsersService.getRatings.bind(Service, 'appraiser', null);
  // Update record
  Service.updateRecord = AdminUsersService.updateRecord.bind(Service, 'appraiser');
  // Create new company
  Service.newCompany = AdminUsersService.newCompany.bind(Service, 'appraiser');
  return Service;
}]);