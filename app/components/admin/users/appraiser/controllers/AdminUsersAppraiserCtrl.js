'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users - Appraiser view controller
 */
app.controller('AdminUsersAppraiserCtrl',
['$scope', 'AdminUsersAppraiserService', '$stateParams', '$timeout', 'REQUEST_DELAY', 'DomLoadService',
 'AdminUsersService', 'AdminUsersCtrlInheritanceService', function (
$scope, AdminUsersAppraiserService, $stateParams, $timeout, REQUEST_DELAY, DomLoadService,
AdminUsersService, AdminUsersCtrlInheritanceService) {
  var vm = this;
  var Service = AdminUsersAppraiserService;
  // Whether displaying disabled view
  Service.disabled = $scope.$eval($scope.disabled);
  // Inherit common functions
  AdminUsersCtrlInheritanceService.createCtrl.call(vm, Service, $scope, 'appraiser');
  // Appraiser details
  vm.displayData = Service.displayData;
  // Safe data
  vm.safeData = Service.safeData;
  // Change details
  vm.changeAdditionalDetailsTab = function (tab) {
    // Keep track of tab
    AdminUsersService.changeUrl({details: tab});
  };
  /**
   * Appraiser
   */
    // Tab config
  vm.additionalDetailsTabsConfig = {
    notes: 'Notes',
    coverage: 'Coverage',
    appraisalForms: 'Appraisal forms',
    feeSchedule: 'Fee schedule',
    pendingOrders: 'Pending orders',
    history: 'History',
    additionalDocs: 'Additional docs'
  };
  // Tab config array (for ordering)
  vm.additionalDetailsTabs =
  ['notes', 'coverage', 'appraisalForms', 'feeSchedule', 'pendingOrders', 'history', 'additionalDocs'];
  /**
   * Appraiser company
   */
    // Tab config
  vm.companyAdditionalDetailsTabsConfig = {
    addUsers: 'Add Users',
    companySettings: 'Company Settings',
    achInformation: 'ACH Information'
  };
  // Tab config array (for ordering)
  vm.companyAdditionalDetailsTabs =
  ['addUsers', 'companySettings', 'achInformation'];

  /**
   * Initiate data and state
   */
  vm.init = function () {
    // Set state on load
    Service.setStateOnLoad($stateParams, vm)
    .then(function () {
      if (AdminUsersService.id) {
        $scope.compileDirectives();
      }
    });
    // Keep reference to which section we're on
    AdminUsersService.section = 'appraiser';
  };
  /**
   * On vacation / not on vacation button text
   */
  vm.vacationButtonText = function () {
    return vm.displayData.details.isOnVacation ? 'On vacation' : 'Not on vacation';
  };
  /**
   * Toggle on vacation / not on vacation
   */
  vm.toggleVacation = function () {
    // Detach watcher
    AdminUsersService.detachUpdateWatcher();
    // Update
    vm.displayData.details.isOnVacation = !vm.displayData.details.isOnVacation;
    // Make request
    Service.toggleSetting('isOnVacation')
    .catch(function () {
      // Reset safe data
      Service.safeData[Service.appraiserId].isOnVacation =
      !Service.safeData[Service.appraiserId].isOnVacation;
      // Reset display data
      Service.displayData.details.isOnVacation = !Service.displayData.details.isOnVacation;
      // Show failure modal
      $scope.$broadcast('show-modal', 'update-appraiser-failure');
    })
    .finally(function () {
      // Reattach watcher
      AdminUsersService.attachUpdateWatcher.call(vm, $scope, Service);
    });
  };
  /**
   * Show approved/DNU lists
   */
  vm.approvedDnu = function () {
    $scope.$broadcast('show-modal', 'approved-dnu-lists');
  };
  /**
   * Show appraiser statistics
   */
  vm.displayStatistics = function () {
    $scope.$broadcast('show-modal', 'appraiser-statistics');
  };
  /**
   * Toggle weekends for statistics
   */
  vm.toggleWeekends = function () {
    // @todo
  };
  /**
   * On successful upload
   */
  vm.uploadReportSuccess = function (response) {
    // Add appraisal report to list
    if (angular.isDefined(response.link)) {
      Service.addNewAppraisal(response);
    }
  };

  /**
   * Catch details display for companies and display correct item
   */
  $scope.$on('compile-directive', function (event, name) {
    // Appraiser company
    if (name === 'company-additional-details') {
      vm.showAdditionalDetails('appraiser-company');
      // Appraiser user - can also run on load, so check which user type
    } else if (name === 'appraiser-additional-details') {
      // Appraiser
      if (angular.isObject(Service.safeData.appraiser[$stateParams.recordId].company)) {
        vm.showAdditionalDetails('appraiser-user');
      } else {
        vm.showAdditionalDetails('appraiser-company');
      }
    }
  });

  /**
   * Ensure that a valid tab is shown
   */
  vm.checkDetailsPaneTab = function () {
    // Check tab on company
    if (typeof Service.safeData.appraiser[AdminUsersService.id].company === 'undefined') {
      if (typeof AdminUsersService.currentDetailsTab === 'undefined' ||
          typeof vm.companyAdditionalDetailsTabsConfig[AdminUsersService.currentDetailsTab] === 'undefined') {
        vm.companySelectedAdditionalDetailsTab = AdminUsersService.currentDetailsTab = 'addUsers';
      } else {
        vm.companySelectedAdditionalDetailsTab = AdminUsersService.currentDetailsTab;
      }
      // Appraiser
    } else {
      if (typeof AdminUsersService.currentDetailsTab === 'undefined' ||
          typeof vm.additionalDetailsTabsConfig[AdminUsersService.currentDetailsTab] === 'undefined') {
        vm.selectedAdditionalDetailsTab = AdminUsersService.currentDetailsTab = 'notes';
      } else {
        vm.selectedAdditionalDetailsTab = AdminUsersService.currentDetailsTab;
      }
    }
  };

  /**
   * Display Appraiser w9
   */
  vm.displayW9 = function () {
    Service.getW9Info();
    $scope.$broadcast('show-modal', 'appraiser-w9');
  };

  /**
   * Submit w9 form disabled
   */
  vm.w9SubmitDisabled = function () {
    return vm.w9Form.$invalid;
  };

  /**
   * Submit updated w9
   */
  vm.submitW9 = function () {
    Service.submitW9()
    .then(function () {
      $scope.$broadcast('hide-modal', 'appraiser-w9');
      $scope.$broadcast('show-modal', 'appraiser-w9-success');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'appraiser-w9-failure');
    });
  };

  /**
   * Download existing W9 as PDF
   * @todo When backend complete
   * @link https://github.com/ascope/manuals/issues/422
   */
  vm.downloadW9 = function () {
    $scope.triggerDownload();
  };

  /**
   * Display W9 form on IRS site
   */
  vm.displayW9Form = function () {
    $scope.triggerDownload(Service.safeData.w9FormUrl);
  };

  /**
   * Download a report
   */
  vm.downloadReport = function (url) {
    $scope.triggerDownload(url);
  };

  // Init controller load
  vm.init();
}]);
