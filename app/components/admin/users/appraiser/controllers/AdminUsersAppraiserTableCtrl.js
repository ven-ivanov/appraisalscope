'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersAppraiserTableCtrl', ['$scope', 'AdminUsersAppraiserService', 'AdminUsersTableCtrlInheritanceService',
function ($scope, AdminUsersAppraiserService, AdminUsersTableCtrlInheritanceService) {

  // Inherit table controller methods
  AdminUsersTableCtrlInheritanceService.inherit.call(this, $scope, AdminUsersAppraiserService, 'appraiser',
  {disabled: AdminUsersAppraiserService.disabled});
}]);