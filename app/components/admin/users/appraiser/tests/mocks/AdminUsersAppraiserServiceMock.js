var AdminUsersAppraiserServiceMock = function ($q) {
  return {
    appraiserId: 1,
    safeData: {
      appraiser: {
        1: {test: 'test', _appraisals: {
          scheduled: {
            1: ['blah']
          },
          assigned: {
            1: ['blah'],
            2: ['bloop']
          },
          completed: {
            1: ['yeah'],
            2: ['its done']
          },
          pending: {
            1: ['pending1'],
            2: ['pending2']
          }
        }},
        2: {test: 'test2'}
      }
    },
    queryAppraisalQueue: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    getAppraisals: function () {
      return $q(function (resolve) {
        resolve();
      });
    },
    getAppraiserManagerPermissions: function () {
      return $q(function (resolve) {
        resolve();
      });
    }
  };
};