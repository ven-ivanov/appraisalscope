'use strict';
describe('AdminUsersAppraiserService', function () {
  var scope, Service, httpBackend, appraisers, asTableService, adminUsersService;

  appraisers = [
    {id: 1, firstName: 'Logan', lastName: 'Etherton', certification: 1, active: true},
    {id: 2, firstName: 'Logans', lastName: 'EvilTwin', certification: 2, active: false}
  ];
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('DomLoadService', DomLoadServiceMock);
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
      $provide.factory('AsTableService', AsTableServiceMock);
    });
  });

  beforeEach(inject(function (AdminUsersAppraiserService, $rootScope, $httpBackend, AsTableService, AdminUsersService) {
    Service = AdminUsersAppraiserService;
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    asTableService = AsTableService;
    adminUsersService = AdminUsersService;

    // Set table config
    Service.tableHeading = [{data: 'firstName'}, {data: 'lastName'}];
    // Spy on change URL
    spyOn(adminUsersService, 'changeUrl');
    spyOn(Service, 'hashData');
    spyOn(Service, 'formatData');
    spyOn(asTableService, 'applyFilters').and.callThrough();

    // Requests
    httpBackend.whenGET(/users\/appraiser\/$/).respond(appraisers);
    httpBackend.whenPATCH(/appraiser/).respond({success: true});
    httpBackend.whenGET(/quality/).respond({id: 1});
    httpBackend.whenGET(/certification-details/).respond({id:1, test: 'test'});
    httpBackend.whenGET(/reports/).respond({id:1, test: 'appraisalReports'});
    // Queue
    httpBackend.whenGET(/queues/).respond({data: [1,2,3]});
    httpBackend.whenGET(/appraisals/).respond({data: [4,5,6]});
  }));

  xdescribe('formatCallback', function () {
    beforeEach(function () {
      Service.displayData = {appraiser: [{
        firstName: 'Logan',
        lastName: 'Etherton'
      }]};
      Service.formatCallback();
    });

    it('should create the full name of the appraiser', function () {
      expect(Service.displayData.appraiser).toEqual([ { firstName: 'Logan', lastName: 'Etherton', name: 'Logan Etherton' } ]);
    });
  });

  xdescribe('formatRawRecords', function () {

  });

  describe('getStatistics', function () {
    describe('no header', function () {
      it('should throw an error if header is not passed in', function () {
        try {
          Service.getStatistics();
        } catch (e) {
          expect(e.message).toEqual('Header must be passed in to getStatistics');
        }
      });
    });

    describe('with header', function () {
      beforeEach(function () {
        spyOn(adminUsersService, 'formatData');
        spyOn(Service, 'createStatisticsColumn');
        httpBackend.whenGET(/statistics/).respond({data: [{test: 'test'}]});
        Service.getStatistics([]);
        httpBackend.flush();
        scope.$digest();
      });

      it('should hash data', function () {
        expect(Service.hashData).toHaveBeenCalled();
      });
      it('should call createStatisticsColumn for each column', function () {
        expect(Service.createStatisticsColumn).toHaveBeenCalled();
      });
      it('should call createStatisticsColumn 8 times', function () {
        expect(Service.createStatisticsColumn.calls.count()).toEqual(8);
      });
    });
  });

  describe('createStatisticsColumn', function () {
    var displayData = [{},{}];
    var response = {orders: {test: {percentage: 50, number: 1, total: 2}}};
    beforeEach(function () {
      Service.createStatisticsColumn(displayData, response, 'test');
    });

    it('should update display data with percentage', function () {
      expect(displayData[0]).toEqual({ test: '50.00%' });
    });
    it('should update display data with total', function () {
      expect(displayData[1]).toEqual({});
    });
  });

  describe('addNewAppraisal', function () {
    beforeEach(function () {
      Service.addNewAppraisal({id: 1, appraisal: 'appraisal'});
    });

    it('should set safe data', function () {
        expect(Service.safeData.appraiserReports).toEqual({1: {id: 1, appraisal: 'appraisal'}});
    });

    it('should set display data', function () {
        expect(Service.displayData.appraiserReports).toEqual([{id: 1, appraisal: 'appraisal'}]);
    });
  });

  describe('getApprovedLists', function () {
    xit('to be defined');
  });

  describe('getDnuLists', function () {
    xit('to be defined');
  });

  describe('load appraisals on load', function () {
    beforeEach(function () {
      Service.safeData.appraiser = {
        1: {
          _type: 'appraiser',
          memberType: 'appraiser'
        }
      };
    });

    /**
     * @todo Update me
     */
    describe('getAppraisals', function () {
      beforeEach(function () {
        spyOn(Service, 'queryAppraisalQueue');
        spyOn(Service, 'queryAppraisals');
      });
      it('should call nothing if no record is set', function () {
        adminUsersService.id = null;
        Service.getAppraisals();
        scope.$digest();
        expect(Service.queryAppraisalQueue).not.toHaveBeenCalled();
      });
      describe('appraiser selected', function () {
        beforeEach(function () {

          Service.getAppraisals();
          scope.$digest();
        });

        it('should retrieve appraisals from queue', function () {
          expect(Service.queryAppraisalQueue).toHaveBeenCalled();
        });
        it('should have queried appraisals not in queue', function () {
          expect(Service.queryAppraisals).toHaveBeenCalled();
        });
      });
    });

    xdescribe('queryAppraisals', function () {

    });

    describe('queryAppraisalQueue', function () {
      beforeEach(function () {
        spyOn(Service, 'checkAppraisalQueried');
        spyOn(Service, 'addAppraisalsToAppraiser');
      });

      it('should add appraisals to appraiser record', function () {
        Service.queryAppraisalQueue('test');
        httpBackend.flush();
        scope.$digest();
        expect(Service.addAppraisalsToAppraiser).toHaveBeenCalled();
      });
      it('should not query again if appraisals are not set', function () {
        Service.safeData.appraiser[1]._appraisals = {test: {1: 'blah', 2: 'bleh'}};
        Service.queryAppraisalQueue('test');
        scope.$digest();
        expect(Service.addAppraisalsToAppraiser).not.toHaveBeenCalled();
      });
    });

    describe('addAppraisalsToAppraiser', function () {
      beforeEach(function () {
        Service.safeData._test = [3,4];
        Service.safeData.appraiser[1]._appraisals = {};
        Service.addAppraisalsToAppraiser([1,2], 'test');
      });

      it('should hash appraisals', function () {
        expect(Service.hashData).toHaveBeenCalled();
      });
      it('should copy data to appraiser', function () {
        expect(Service.safeData.appraiser[1]).toEqual({
          _type: 'appraiser',
          memberType: 'appraiser',
          _appraisals: Object({test: [3, 4]})
        });
      });
      it('should delete temporary property', function () {
        expect(Service.safeData._test).toBeUndefined();
      });
    });
  });
});