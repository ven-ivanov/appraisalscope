'use strict';

var app = angular.module('frontendApp');

/**
 * Compile additional details only when it's needed
 */
app.directive('adminUsersAppraiserCompanyAdditionalDetails', ['DirectiveConditionalLoadService', 'AdminUsersAppraiserService', function (DirectiveConditionalLoadService, AdminUsersAppraiserService) {
  return function (scope, elem) {
    var template;
    // For disable view, don't compile company
    if (AdminUsersAppraiserService.disabled) {
      return;
    }
    // Template
    template =
    ['<info-modal modal-id="appraiser-company-additional-details" modal-title="Additional details" button-text="OK" modal-class="notification-window" modal-width="100%" modal-height="max">',
     '<as-tabs tab-config="appraiserCtrl.companyAdditionalDetailsTabsConfig" tabs="appraiserCtrl.companyAdditionalDetailsTabs" tab-width="3rds" selected-tab="appraiserCtrl.companySelectedAdditionalDetailsTab" change-tab="appraiserCtrl.changeAdditionalDetailsTab(tab)">',
     '</as-tabs>',
      // Switch show directive
     '<div ng-switch="appraiserCtrl.companySelectedAdditionalDetailsTab">',
     // Add user
     '<admin-users-add-user ng-switch-when="addUsers" appraiser="true" hide="settings,clientPermissions"></admin-users-add-user>',
     // Company settings
     '<admin-users-fee-schedule ng-switch-when="companySettings"></admin-users-fee-schedule>',
     // ACH information
     '<admin-users-ach-information ng-switch-when="achInformation"></admin-users-ach-information>',
     '</div>',
     '</info-modal>'];

    // Compile the directive only when necessary and then load it
    DirectiveConditionalLoadService.init.call(scope, template, elem, 'appraiser-company-additional-details');
  };
}]);