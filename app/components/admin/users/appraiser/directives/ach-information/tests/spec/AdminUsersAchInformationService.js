describe('AdminUsersAchInformationService', function () {
  var scope, httpBackend, adminUsersService, Service;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminUsersService', AdminUsersServiceMock);
    });
  });

  beforeEach(inject(function ($rootScope, AdminUsersAchInformationService, AdminUsersService, $httpBackend) {
    scope = $rootScope.$new();
    Service = AdminUsersAchInformationService;
    adminUsersService = AdminUsersService;
    httpBackend = $httpBackend;

    httpBackend.whenGET(/.*/).respond({
      name: 'Bank',
      accountNumber: '5256256567',
      routing: '52523562'
    });
    httpBackend.whenPUT(/.*/).respond();

    spyOn(Service, 'hashData');
    spyOn(Service, 'formatData');
    spyOn(Service, 'request').and.callThrough();
  }));

  describe('getAchInformation', function () {
    beforeEach(function () {
      Service.getAchInformation();
      httpBackend.flush();
      scope.$digest();
    });

    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should format for display', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('updateAchInformation', function () {
    beforeEach(function () {
      Service.updateAchInformation({test: 'test'});
    });

    it('should send update requests', function () {
      expect(Service.request).toHaveBeenCalledWith('updateAchInformation', { test: 'test' });
    });
  });
});