'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersAchInformation', [function () {
  return {
    templateUrl: '/components/admin/users/appraiser/directives/ach-information/partials/ach-information.html',
    controller: 'AdminUsersAchInformationCtl',
    controllerAs: 'achCtrl'
  };
}]);