'use strict';

var app = angular.module('frontendApp');

app.factory('AdminUsersAchInformationService', ['AdminUsersService', 'AsDataService', function (AdminUsersService, AsDataService) {
  var Service = {
    safeData: {
      ach: {}
    },
    displayData: {
      ach: {}
    },
    /**
     * Retrieve ACH information on load
     */
    getAchInformation: function () {
      return Service.request('getAchInformation')
      .then(function (response) {
        // Hash and display
        Service.hashData(response, 'ach');
        Service.formatData();
      });
    },
    /**
     * Update information on change
     */
    updateAchInformation: function (body) {
      return Service.request('updateAchInformation', body);
    }
  };
  // Inherit
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatPlainObjectData.bind(Service, 'ach');
  Service.transformData = AsDataService.transformData.bind(Service);
  Service.request = AdminUsersService.request.bind(Service, 'ach');
  return Service;
}]);