'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersAchInformationCtl',
['$scope', 'AdminUsersAchInformationService', '$timeout', 'REQUEST_DELAY', function ($scope, AdminUsersAchInformationService, $timeout, REQUEST_DELAY) {
  var vm = this;
  var Service = AdminUsersAchInformationService;
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;

  /**
   * Get data on load
   */
  vm.init = function () {
    Service.getAchInformation()
    .then(function () {
      // Watch and update
      vm.attachUpdateWatcher();
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'ach-load-failed', true);
    });
  };

  /**
   * Watch and write changes
   */
  vm.attachUpdateWatcher = function () {
    $scope.$watchCollection(function () {
      return vm.displayData.ach;
    }, function (newVal, oldVal) {
      if (angular.isUndefined(newVal) || angular.equals(newVal, oldVal)) {
        return;
      }
      if (vm.typing) {
        $timeout.cancel(vm.typing);
      }
      // Wait and then make the request
      vm.typing = $timeout(function () {
        Service.updateAchInformation(newVal)
        .catch(function () {
          $scope.$broadcast('show-modal', 'ach-update-failed', true);
        });
        vm.typing = null;
      }, REQUEST_DELAY.ms);
    });
  };

  vm.init();
}]);