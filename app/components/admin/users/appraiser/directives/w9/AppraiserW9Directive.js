'use strict';

var app = angular.module('frontendApp');

app.directive('appraiserW9', [function () {
  return {
    templateUrl: '/components/admin/users/appraiser/directives/w9/partials/w9.html'
  };
}]);