'use strict';

var app = angular.module('frontendApp');

/**
 * Admin User - Appraiser view
 */
app.directive('adminUsersAppraiser',
['DirectiveInheritanceService', '$compile', function (DirectiveInheritanceService, $compile) {
  return {
    templateUrl: '/components/admin/users/appraiser/directives/partials/appraiser.html',
    controller: 'AdminUsersAppraiserCtrl',
    controllerAs: 'appraiserCtrl',
    scope: {
      disabled: '@'
    },
    compile: function () {
      return {
        pre: function (scope) {
          // Expose trigger download
          DirectiveInheritanceService.inheritDirective.call(scope);
        },
        post: function (scope) {
          /**
           * Compile directives after state set
           */
          scope.compileDirectives = function () {
            // Ratings directive
            angular.element('#admin-users-rating').replaceWith($compile('<admin-users-rating type="appraiser" ctrl="appraiserCtrl"></admin-users-rating>')(scope));
            // Certificates directive
            angular.element('#admin-users-certifications').replaceWith($compile('<admin-users-certification ctrl="appraiserCtrl" type="appraiser" hide="primary"></admin-users-certification>')(scope));
            // Reports
            angular.element('#admin-users-reports').replaceWith($compile('<admin-users-reports ctrl="appraiserCtrl" type="appraiser"></admin-users-reports>')(scope));
            // Statistics
            angular.element('#appraiser-statistics').replaceWith($compile('<admin-users-appraiser-statistics></admin-users-appraiser-statistics>')(scope));
            // Approved/DNU lists
            angular.element('#appraiser-approved-dnu').replaceWith($compile('<admin-users-appraiser-approved-dnu></admin-users-appraiser-approved-dnu>')(scope));
            // New company
            angular.element('#appraiser-new-company').replaceWith($compile('<admin-users-new-company ctrl="appraiserCtrl" appraiser="true"></admin-users-new-company>')(scope));
            // Appraiser w9
            angular.element('#appraiser-w9').replaceWith($compile('<appraiser-w9></appraiser-w9>')(scope));
            // Don't run again
            scope.compileDirectives = angular.noop;
          };
        }
      };
    }
  };
}]);