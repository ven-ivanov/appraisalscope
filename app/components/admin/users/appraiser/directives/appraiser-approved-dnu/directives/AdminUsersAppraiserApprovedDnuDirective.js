'use strict';

var app = angular.module('frontendApp');

app.directive('adminUsersAppraiserApprovedDnu', [function () {
  return {
    templateUrl: '/components/admin/users/appraiser/directives/appraiser-approved-dnu/directives/partials/approved-dnu.html'
  };
}]);