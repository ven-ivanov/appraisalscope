'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersAppraiserApprovedListsCtrl',
['$scope', 'AdminUsersAppraiserService', function ($scope, AdminUsersAppraiserService) {
  var vm = this;

  /**
   * Table heading
   */
  vm.heading = [
    {
      label: 'Name',
      data: 'title'
    }
  ];
  /**
   * Initiate data load
   */
  vm.init = function () {
    AdminUsersAppraiserService.getApprovedLists()
    .then(function (tableData) {
      // Set table data
      vm.tableData = tableData;
      vm.rowData = tableData.slice();
    })
    .catch(function () {
      // Failure
      $scope.$emit('show-modal', 'approved-list-failure');
    });
  };

  // Load data
  vm.init();
}]);