'use strict';

var app = angular.module('frontendApp');

app.controller('AdminUsersAppraiserDnuListsCtrl', ['$scope', 'AdminUsersAppraiserService', function ($scope, AdminUsersAppraiserService) {
  var vm = this;

  /**
   * Table heading
   */
  vm.heading = [
    {
      label: 'Name',
      data: 'title'
    }
  ];
  /**
   * Initiate data load
   */
  vm.init = function () {
    AdminUsersAppraiserService.getDnuLists()
    .then(function (tableData) {
      // Set table data
      vm.tableData = tableData;
      vm.rowData = tableData.slice();
    })
    .catch(function () {
      // Failure
      $scope.$emit('show-modal', 'approved-list-failure');
    });
  };

  // Load data
  vm.init();
}]);