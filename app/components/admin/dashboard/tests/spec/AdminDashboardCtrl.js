'use strict';

describe('Controller: AdminDashboardCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var AdminDashboardCtrl,
  scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminDashboardCtrl = $controller('AdminDashboardCtrl', {
      $scope: scope
    });
  }));
});
