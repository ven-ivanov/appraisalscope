'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminDashboardCtrl
 * @description
 * # AdminDashboardCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AdminDashboardActivitesCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
