'use strict';

/**
 * @ngdoc function
 * @name frontendApp.factory:AdminReportsFiltersService
 * @description
 * # AdminReportsFiltersService
 * factory of the AdminReportsFiltersService
 */
(function (ng, undefined){
  ng.module('frontendApp')
    .factory('AdminReportsDynamicFiltersService', [
      'Utils',

      function (Utils) {

        var operations = [
          {'id' : '=', 'name' : '=', 'dataTypes' : ['varchar', 'int', 'date', 'float', 'char', 'text', 'tinyint'] },
          {'id' : '>', 'name' : '>', 'dataTypes' : ['int', 'date', 'float', 'char', 'text', 'tinyint'] },
          {'id' : '>=', 'name' : '>=', 'dataTypes' : ['int', 'date', 'float', 'char', 'text', 'tinyint'] },
          {'id' : '<', 'name' : '<', 'dataTypes' : ['int', 'date', 'float', 'char', 'text', 'tinyint'] },
          {'id' : '<=', 'name' : '<=', 'dataTypes' : ['int', 'date', 'float', 'char', 'text', 'tinyint'] },
          {'id' : '<>', 'name' : '<>', 'dataTypes' : ['varchar', 'int', 'float', 'char', 'text', 'tinyint'] },
          {'id' : 'IN', 'name' : 'IN (Separated by ;)', 'dataTypes' : ['varchar', 'int', 'date', 'float', 'char', 'text', 'tinyint'] },
          {'id' : 'NOTIN', 'name' : 'NOTIN (Separated by ;)', 'dataTypes' : ['varchar', 'int', 'date', 'char', 'text', 'tinyint'] },
          {'id' : 'DOESNOTCONTAIN', 'name' : 'DOESN\'T CONTAIN', 'dataTypes' : ['varchar', 'date'] },
          {'id' : 'CONTAINS', 'name' : 'CONTAINS', 'dataTypes' : ['varchar', 'date'] },
          {'id' : 'BEGINSWITH', 'name' : 'BEGINS WITH', 'dataTypes' : ['varchar', 'date'] },
          {'id' : 'ENDSWITH', 'name' : 'ENDS WITH', 'dataTypes' : ['varchar', 'date'] },

          {'id' : 'LAST COMPLETED DAY', 'name' : 'LAST COMPLETED DAY', 'dataTypes' : ['date'], 'conflictsWith' : ['LAST COMPLETED DAY', 'LAST COMPLETED WEEK', 'LAST COMPLETED MONTH'] },
          {'id' : 'LAST COMPLETED WEEK', 'name' : 'LAST COMPLETED WEEK', 'dataTypes' : ['date'], 'conflictsWith' : ['LAST COMPLETED DAY', 'LAST COMPLETED WEEK', 'LAST COMPLETED MONTH'] },
          {'id' : 'LAST COMPLETED MONTH', 'name' : 'LAST COMPLETED MONTH', 'dataTypes' : ['date'], 'conflictsWith' : ['LAST COMPLETED DAY', 'LAST COMPLETED WEEK', 'LAST COMPLETED MONTH'] }
        ];

        var dynamicConditions = [
          {'id' :'@@DYNAMIC_DATA@@', 'name' :'Dynamic Data', 'dataTypes' : ['all']},
          {'id' :'@@CURRENT_DATE@@', 'name' :'Current Date', 'dataTypes' : ['date']}
        ];

        return {

          getOperation : function(id){
            var index;
            index = Utils.indexOf(operations, id, 'id');
            if(index <= -1) {
              return false;
            }
            return operations[index];
          },

          getOperationByType : function(dataType){
            return operations.filter(function(operation){
              return operation.dataTypes.indexOf(dataType) > -1;
            });
          },



          getDynamicCondition : function(dataType){
            return dynamicConditions.filter(function(condition){
              return condition.dataTypes.indexOf(dataType) > -1 || condition.dataTypes.indexOf('all') > -1;
            });
          },

          checkForConflict : function(currentFilters, newFilter){
            var operation, index, newOperation, newIndex, conflict = false, conflictIndex, i, l, filter;

            newIndex = Utils.indexOf(operations, newFilter.operation, 'id');
            if(newIndex <= -1) {
              return false;
            }
            newOperation = operations[newIndex];

            i = 0;
            l = currentFilters.length;
            for(i;i<l;i++){
              filter = currentFilters[i];
              index = Utils.indexOf(operations, filter.operation, 'id');
              if(index <= -1) {
                continue;
              }
              operation = operations[index];
              if(!operation.conflictsWith || !operation.conflictsWith.length) {
                continue;
              }
              conflictIndex = operation.conflictsWith.indexOf(newOperation.id);
              if(conflictIndex > -1) {
                conflict = filter;
                break;
              }
            }

            return conflict;
          },

          createDynamicFilter : function(field, filter){
            return {
              'id' : field.id,
              'name' : field.name,
              'table' : field.table,
              'tabledisplaylabel' : field.tabledisplaylabel,
              'fieldlabel' : field.fieldlabel,
              'fieldType' : field.datatype,
              'operation' : filter.operation,
              'condition' :filter.condition
            };
          }
        };
      }
    ]);
})(angular);