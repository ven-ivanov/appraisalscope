'use strict';

/**
 * @ngdoc function
 * @name frontendApp.factory:AdminReportsService
 * @description
 * # AdminReportsService
 * Admin Reports base data service.
 */
angular.module('frontendApp').factory('AdminReportsService', [
  'DomLoadService',
  function (DomLoadService) {

    /**
     * This service only holds the data to pass around the Admin Reports module.
     * Eg. selectedReportId, gridState
     */
    var AdminReportsService = {

      setStateOnLoad: function () {
        return DomLoadService.load()
        .then();
      },

      /**
       * Set active class on active tab
       * @returns {string}
       */
      getTabClass: function (tab, activeTab) {
        if (tab.tab === activeTab) {
          return 'col-md-1 active';
        }
        return 'col-md-1';
      },

      /**
       * Keep any query parameters previously set
       */
      setTabs: function (tabs) {
        // Keep any query params
        angular.forEach(AdminReportsService.tabs, function (tab, key) {
          if (tab.query) {
            tabs[key].query = tab.query;
          }
        });

        AdminReportsService.tabs = tabs;
      },

      /**
       * Get grid-state object for a specific component to make a request parameter.
       * @param {String} componentName Eg. list-reports, client-permission, staff-permission, etc...
       */
      getGridState: function (componentName) {
        switch (componentName) {
          case 'list-reports':
            return {
              autoIncreaseFirstColumn: true,
              columns: [
                {
                  filter: true,
                  name: 'data1',
                  nameAs: 'Report group'
                },
                {
                  filter: true,
                  name: 'data2',
                  nameAs: 'Report Name'
                },
                {
                  filter: true,
                  name: 'data3',
                  nameAs: 'Description'
                },
                {
                  filter: true,
                  name: 'data4',
                  nameAs: 'Created By'
                },
                {
                  filter: true,
                  name: 'data5',
                  nameAs: 'Created On'
                },
                {
                  filter: true,
                  name: 'data6',
                  nameAs: 'Last Generated On'
                }
              ],
              filters: {},
              groupers: [
                {
                  name: 'data1',
                  nameAs: 'Report group',
                  reverse: false
                }
              ],
              limitRows: false,
              //      pagination: {},
              sorters: [
                {
                  name: 'data2',
                  nameAs: 'Report Name',
                  predicate: 'data2',
                  reverse: false
                }
              ]
            };
          case 'client-permission':
            return {
              columns: [
                {
                  label: '',
                  data: 'active',
                  noSort: true,
                  checkbox: true
                },
                {
                  label: 'Name',
                  data: 'name',
                  noSort: false
                },
                {
                  label: 'User Levels',
                  data: 'user_levels',
                  noSort: false
                }
              ],
              sorters: [
                {
                  name: 'name',
                  predicate: 'name',
                  reverse: false
                }
              ],
              pagination: {
                start: 1,
                perPage: 15
              },
              limitRows: false,
              autoIncreaseFirstColumn: true
            };
          case 'staff-permission':
            return {
              columns: [
                {
                  label: '',
                  data: 'active',
                  noSort: true,
                  checkbox: true
                },
                {
                  label: 'Sr #',
                  data: 'number' // @TODO: enable auto increasement number for this column
                },
                {
                  label: 'Name',
                  data: 'name',
                  noSort: false
                }
              ],
              sorters: [
                {
                  name: 'name',
                  predicate: 'name',
                  reverse: false
                }
              ],
              limitRows: false,
              autoIncreaseFirstColumn: true
            };
          default:
            return {};
        }
      }
    };

    return AdminReportsService;
  }
]);
