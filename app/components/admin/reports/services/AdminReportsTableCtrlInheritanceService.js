/**
 * Created by Simon on June 24th, 2015.
 */
'use strict';

/**
 * Represent a common service for partial views.
 * The table controller should inject this service.
 */
angular.module('frontendApp').factory('AdminReportsTableCtrlInheritanceService', [
  function () {
    var Service = {
      /**
       * All the table controllers can call this service to handle the table work.
       *
       * @params $scope: The scope that the controller was bound to.
       * @params Service: The service where the controller accesses data from.
       * @params heading: The table heading object which the controller holds to.
       * @params type: The controller unique-name, it will be used to parse the displayData.
       * @params loadParams: All the tables in Admin Reports module use
       * gridState as a parameter to make a API request.
       */
      inherit: function ($scope, Service, heading, type, loadParams) {
        var vm = this;

        vm.rowSelectionCount = 0;
        vm.canDeleteReports = false;
        vm.canEditReport = false;

        vm.safeData = Service.safeData;

        // Set table heading.
        vm.safeData.heading = vm.heading = heading;

        // Table group-by default is Report Group
        //        vm.groupBy = 'Report Group';
        // Stores all checked/selected row(s)
        vm.selectedRecords = [];

        /**
         * Populate Admin Reports data
         * @params loadParams: the gridState object.
         */
        vm.init = function () {
          if (type === 'generate-report') {
            Service.generateReports(loadParams);
          } else {
            Service.getAdminReportList(loadParams);
          }
        };

        /**
         * Handle row selection event
         * @params id: The ID of the curent data-row
         */
        vm.rowFn = function () {
          // @TODO: Detach the update watcher

          // Retrieve details then attach the update watcher once the record is retrieved
        };

        /**
         * Bind the link-label clicked event on as-table;
         * also bind the current report-ID.
         * Call the directive controller to show the Client/Ctaff Perrmission modal.
         * Look at the AdminReportsListReportsTableCtrl to see the heading configuration.
         *
         * @params id: the report-ID of the current row.
         * @params modalName: '{'client-permission'|'staff-permission'} the type of modal that we want to show.
         */
        vm.linkFn = function (id, modalName) {
          // Store the current report-ID so other controller can use it.
          // Eg. Staff/Client Permission needs the report-ID to make a request.
          Service.selectedReportId = id;

          // We must call the $parent to broadcast,
          // because that's where the directives were bound to the view.
          $scope.$parent.$broadcast('show-modal', modalName);
        };

        /**
         * Update selected records on multi-select click
         */
        $scope.$watchCollection(function () {
          return vm.multiSelect;
        }, function (newVal) {
          vm.rowSelectionCount = 0;

          angular.forEach(newVal, function (select, key) {
            if (select) {
              vm.rowSelectionCount++;
              vm.selectedRecords.push(parseInt(key));
            }
          });
        });

        /**
         * Watch table data
         */
        $scope.$watchCollection(function () {
          return Service.displayData[type].data;
        }, function (newVal) {
          if (!angular.isArray(newVal)) {
            return;
          }

          vm.tableData = newVal;
          vm.rowData = vm.tableData.slice();
        });

        $scope.$watchCollection(function () {
          return Service.displayData[type].selection;
        }, function (assignedIds) {
          angular.forEach(assignedIds, function (assignedId) {
            angular.forEach(Service.displayData[type].data, function (item) {
              if (item.id === assignedId) {
                vm.multiSelect[assignedId] = true;
              }
            });
          });
        });

        // Init data loading
        vm.init();
      }
    };

    return Service;
  }
]);