'use strict';

angular.module('frontendApp').factory('AdminReportsTabsService', [
  function () {
    var Service = {
      currentTab: '',
      tabs: [
        {
          name: 'list reports',
          tab: 'list-reports',
          closeable: false,
          active: true
        },
        {
          name: 'statistics',
          tab: 'statistics',
          closeable: false,
          active: false,
        }
      ],
      createReportTab: {
        name: 'add new report',
        tab: 'create-report',
        closeable: true,
        active: false
      },
      editReportTab: {
        name: 'edit report',
        tab: 'edit-report',
        closeable: true,
        active: false
      },
      generateReportTab: {
        name: 'generated report',
        tab: 'generate-view',
        closeable: true,
        active: false
      },

      /**
       * Return the Admin Report tab array.
       * @returns {Array} Admin Report tab array.
       */
      getTabs: function () {
        return Service.tabs;
      },

      /**
       * Return the current active tab-ID.
       * @returns {String} The current active tab-ID.
       */
      getCurrentTab: function () {
        return Service.currentTab;
      },

      /**
       * Activate the current tab by its ID.
       * @param {String} id The tab-ID.
       */
      setCurrentTab: function (id) {
        Service.currentTab = id;
      },

      /**
       * Close a closeable tab.
       * @param {Object} tab The tab object that needs to be closed.
       */
      closeTab: function (tab) {
        var removedIndex = Service.tabs.indexOf(tab);
        if (removedIndex > -1) {
          Service.tabs.splice(removedIndex, 1);

          if (Service.tabs.length === 2) {
            // When there are only static tabs left,
            // Go to the list-report tab
            Service.setCurrentTab('list-reports');
          }
        }
      },

      /**
       * Open the Report tab for a new one or editing one.
       * This tab is closeable.
       * @param {Boolean} isEditMode True, the tab is in edit mode, otherwise, creating new.
       */
      openReportTab: function (isEditMode) {
        var createReportTabIndex = Service.tabs.indexOf(Service.createReportTab);
        var editReportTabIndex = Service.tabs.indexOf(Service.editReportTab);

        // add tab to the bottom of the tab-list
        if (isEditMode) {
          Service.setCurrentTab(Service.editReportTab.tab);

          if (editReportTabIndex < 0) {
            // edit-report tab doesn't exist
            if (createReportTabIndex > -1) {
              // remove the create-new-report tab first.
              Service.tabs.splice(createReportTabIndex, 1);
            }

            // add edit-report tab
            Service.tabs.push(Service.editReportTab);
          }
        } else {
          Service.setCurrentTab(Service.createReportTab.tab);

          if (createReportTabIndex < 0) {
            // create-new-report tab doesn't exist
            if (editReportTabIndex > -1) {
              // remove the edit-report tab first
              Service.tabs.splice(editReportTabIndex, 1);
            }
            // add edit-report tab
            Service.tabs.push(Service.createReportTab);
          }
        }
      },

      /**
       * Open the Generate Report tab.
       * This tab is closeable.
       */
      openGenerateReportTab: function () {
        var generateReportTabIndex = Service.tabs.indexOf(Service.generateReportTab);

        Service.setCurrentTab(Service.generateReportTab.tab);

        if (generateReportTabIndex < 0) {
          // generate-report tab doesn't exist
          // add generate-report tab
          Service.tabs.push(Service.generateReportTab);
        }
      }
    };

    return Service;
  }
]);
