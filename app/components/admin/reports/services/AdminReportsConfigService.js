'use strict';

var app = angular.module('frontendApp');

app.factory('AdminReportsConfigService', [function () {

  // Operations for create-report filters
  var filterOperations = [
    {
      'id': '=',
      'name': '=',
      'dataTypes': ['varchar', 'int', 'date', 'float', 'char', 'text', 'tinyint']
    },
    {
      'id': '>',
      'name': '>',
      'dataTypes': ['int', 'date', 'float', 'char', 'text', 'tinyint']
    },
    {
      'id': '>=',
      'name': '>=',
      'dataTypes': ['int', 'date', 'float', 'char', 'text', 'tinyint']
    },
    {
      'id': '<',
      'name': '<',
      'dataTypes': ['int', 'date', 'float', 'char', 'text', 'tinyint']
    },
    {
      'id': '<=',
      'name': '<=',
      'dataTypes': ['int', 'date', 'float', 'char', 'text', 'tinyint']
    },
    {
      'id': '<>',
      'name': '<>',
      'dataTypes': ['varchar', 'int', 'float', 'char', 'text', 'tinyint']
    },
    {
      'id': 'IN',
      'name': 'IN (Separated by ;)',
      'dataTypes': ['varchar', 'int', 'date', 'float', 'char', 'text', 'tinyint']
    },
    {
      'id': 'NOTIN',
      'name': 'NOTIN (Separated by ;)',
      'dataTypes': ['varchar', 'int', 'date', 'char', 'text', 'tinyint']
    },
    {
      'id': 'DOESNOTCONTAIN',
      'name': 'DOESN\'T CONTAIN',
      'dataTypes': ['varchar', 'date']
    },
    {
      'id': 'CONTAINS',
      'name': 'CONTAINS',
      'dataTypes': ['varchar', 'date']
    },
    {
      'id': 'BEGINSWITH',
      'name': 'BEGINS WITH',
      'dataTypes': ['varchar', 'date']
    },
    {
      'id': 'ENDSWITH',
      'name': 'ENDS WITH',
      'dataTypes': ['varchar', 'date']
    },
    {
      'id': 'LAST COMPLETED DAY',
      'name': 'LAST COMPLETED DAY',
      'dataTypes': ['date'],
      'conflictsWith': ['LAST COMPLETED DAY', 'LAST COMPLETED WEEK', 'LAST COMPLETED MONTH']
    },
    {
      'id': 'LAST COMPLETED WEEK',
      'name': 'LAST COMPLETED WEEK',
      'dataTypes': ['date'],
      'conflictsWith': ['LAST COMPLETED DAY', 'LAST COMPLETED WEEK', 'LAST COMPLETED MONTH']
    },
    {
      'id': 'LAST COMPLETED MONTH',
      'name': 'LAST COMPLETED MONTH',
      'dataTypes': ['date'],
      'conflictsWith': ['LAST COMPLETED DAY', 'LAST COMPLETED WEEK', 'LAST COMPLETED MONTH']
    }
  ];

  var Service = {

    // ***************************************************************************************
    //                                   Create report
    // ***************************************************************************************

    /**
     * Get operations by datatype
     * @param dataType: datatype to get operations for.
     * @return operations list
     */
    getOperationsByType: function (dataType) {
      return filterOperations.filter(function (operation) {
        return operation.dataTypes.indexOf(dataType) !== -1;
      });
    }

  };

  return Service;

}]);