/**
 * Created by Simon on June 24th, 2015.
 */
'use strict';

var app = angular.module('frontendApp');

app.factory('AdminReportsResourceService', ['$resource', 'API_PREFIX', 'RequestMethodsService', 'AdminReportsService',
  function ($resource, API_PREFIX, RequestMethodsService, AdminReportsService) {
    /**
     * AdminReportsResourceService
     */
    var Service = {
      // Standard request URLs
      requests: {
        base: '/v2.0/admin/reports',
        single: '/v2.0/admin/reports/:id',
        reportsList: '/v2.0/admin/reports/listreports/:gridState', // data for reports table

        // *** Permissions ***

        // Get all staffs that were granted permission on the selected report
        staffPermission: '/v2.0/admin/reports/:id/staffpermission/:gridState',
        // Get all staffs that are available to granted permission on the selected report
        availableStaffPermission: '/v2.0/admin/reports/:id/availablestaffpermission/:gridState',
        // Get all clients that were granted permission on the selected report
        clientPermission: '/v2.0/admin/reports/:id/clientpermission/:gridState',
        // Get all clients that are available to granted permission on the selected report
        availableClientPermission: '/v2.0/admin/reports/:id/availableclientpermission/:gridState',
        // Get client permission user levels
        clientPermissionUserLevels: '/v2.0/admin/reports/:id/clientpermissionuserlevels',

        // *** Create report ***
        reportColumns: '/v2.0/admin/reports/columns/:type', // for tree and filter
        //reportTreeColumns: '/v2.0/admin/reports/treecolumns/:type', // for tree
        reportsTypes: '/v2.0/admin/reports/reporttypes/:empty', // for reports types select
        reportSampleData: '/v2.0/admin/reports/constructor/data', // sample data generator

        generateReport: '/v2.0/admin/reports/generatereport/:gridState',
        generateView: '/v2.0/admin/reports/generateview'
      },
      /**
       * TODO Move to appropriate views
       */
      params: {
        /**
         * Build the request paramater using the selected report-ID.
         */
        id: function () {
          return {
            id: AdminReportsService.selectedReportId
          };
        },

        /**
         * Build the request paramater for grid-state.
         *
         * @params gridStateParams: Object that's created/bound from the
         * AdminReportsListReportsTableCtrl
         *
         * @returns Object with a property named gridState.
         *
         * @TODO: Find a way to create/bind the gridState from the as-table
         */
        gridState: function (gridStateParams) {
          return {
            gridState: JSON.stringify(gridStateParams)
          };
        },

        clientPermission: function () {
          return {
            id: AdminReportsService.record.id,
            gridState: {
              columns: [
                {
                  name: 'name',
                  nameAs: 'Name',
                  filter: false
                },
                {
                  name: 'user_levels',
                  nameAs: 'User Levels',
                  filter: false,
                  cellTemplate: ''
                }
              ],
              sorters: [{
                name: 'name',
                predicate: 'name',
                reverse: false
              }],
              groupers: [],
              filters: {},
              menuOptions: [
                {
                  name: 'sortAsc',
                  nameAs: 'Sort Ascending'
                },
                {
                  name: 'sortDesc',
                  nameAs: 'Sort Descending'
                }
              ],
              autoIncreaseFirstColumn: true,
              globalSearch: false,
              perPage: 100
            }
          };
        },

        staffPermission: function () {
          return {
            id: AdminReportsService.record.id,
            gridState: {
              columns: [
                {
                  name: 'name',
                  nameAs: 'Name',
                  filter: false
                }
              ],
              sorters: [{
                name: 'name',
                predicate: 'name',
                reverse: false
              }],
              groupers: [],
              filters: {},
              menuOptions: [
                {
                  name: 'sortAsc',
                  nameAs: 'Sort Ascending'
                },
                {
                  name: 'sortDesc',
                  nameAs: 'Sort Descending'
                }
              ],
              autoIncreaseFirstColumn: true,
              globalSearch: false,
              perPage: 10
            }
          };
        }
      },
      /**
       * TODO Separate by views
       */
      listReports: {
        /**
         * Get all admin reports
         * @param params The gridState that should be transform to a raw JSON string
         * before making the request.
         * @returns {$promise}
         */
        getAdminReportList: function (params) {
          return RequestMethodsService.getRequest(Service.requests.reportsList, Service.params.gridState(params));
        },

        /**
         * TODO
         */
        generateReport: function (params) {
          return RequestMethodsService.getRequest(Service.requests.generateReport, params);
        },

        /**
         * Get all client permissions that were assigned for the current report.
         */
        getClientPermission: function (params) {
          return RequestMethodsService.getRequest(Service.requests.clientPermission, params);
        },

        /**
         * Get all client permissions that are available for the current report.
         * @param {Object} params Object {id:selectedReportId, gridState: JSON.stringify(...)}
         */
        getAvailableClientPermission: function (params) {
          return RequestMethodsService.getRequest(Service.requests.availableClientPermission, params);
        },

        /**
         * Get all client permissions user levels that are available for the current report.
         * @param   {Object} params The selected report ID.
         * @returns {Object} User-Levels promise for a specific Report.
         */
        getClientPermissionUserLevels: function (params) {
          return RequestMethodsService.getRequest(Service.requests.clientPermissionUserLevels, params);
        },

        /**
         * Get all staff permissions that were assigned for the current report.
         */
        getStaffPermission: function (params) {
          return RequestMethodsService.getRequest(Service.requests.staffPermission, params);
        },

        /**
         * Get all staff permissions that are available for the current report.
         */
        getAvailableStaffPermission: function (params) {
          return RequestMethodsService.getRequest(Service.requests.availableStaffPermission, params);
        },

        /**
         * Update the current report client-permissions.
         * @param   {Object} params           Contains the current report-ID.
         * @param   {Array}  clientPermissions The array of user-levels that were assigned to the client.
         * @returns {Object} Response promise.
         */
        updateReportClientPermissions: function (params, clientPermissions) {
          return RequestMethodsService.post(Service.requests.clientPermission, params, clientPermissions);
        },

        /**
         * Update the current report staff-permissions.
         * @param   {Object} params           Contains the current report-ID
         * @param   {Array}  staffPermissions The array of staff-IDs that were assigned to the report.
         * @returns {Object} Response promise
         */
        updateReportStaffPermissions: function (params, staffPermissions) {
          return RequestMethodsService.post(Service.requests.staffPermission, params, staffPermissions);
        },

        /**
         * Get all alerts by id
         * id: id
         * if alertId: null, it return report all alert
         * Opposite, it return report this alert
         */
        getReportAlerts: function (params) {
          return RequestMethodsService.getRequest('/v2.0/admin/reports/:id/reportAlerts/:alertId?', params);
        }
      },

      // ***************************************************************************************
      //                                   Create report
      // ***************************************************************************************

      createReport: {
        /**
         * Get all report-types for new report.
         * @param params a string with value 'empty'
         * @returns {$promise}
         */
        getReportTypes: function () {
          return RequestMethodsService.getRequest(Service.requests.reportsTypes, {
            empty: 'any' // or fail
          });
        },

        /**
         * Get all columns for report filtering.
         * @param params a string with value 'empty'
         * @returns {$promise}
         */
        loadReportColumns: function (type) {
          return RequestMethodsService.getRequest(Service.requests.reportColumns, {
            type: type
          });
        },

        /**
         * Generate sample data to see in constructor
         */
        sampleData: function (report) {
          return RequestMethodsService.post(Service.requests.reportSampleData, {}, report);
        }

      },
      /**
       * Statistics view
       */
      statistic: {
        getStatistics: function () {
          return RequestMethodsService.getRequest();
        }
      },

      /**
       * Create a request to get a Report record by ID.
       * @param   {Object} params The parameter object that consumes the report ID.
       * @returns {Object} $promise.
       */
      getReport: function (params) {
        return RequestMethodsService.getRequest('/v2.0/admin/reports/:id', params);
      },

      /**
       * Copy report view
       */
      copyReport: function (params) {
        return RequestMethodsService.post('/v2.0/admin/reports/:id/copy', params);
      },

      /**
       * Delete report view
       */
      deleteReport: function (params) {
        return RequestMethodsService.deleteRequest('/v2.0/admin/reports/delete/', params);
      }
    };

    return Service;
  }
]);
