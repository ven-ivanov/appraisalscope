'use strict';

/**
 * @ngdoc function
 * @name frontendApp.directive:asReportDnd
 * @description
 * # asReportDnd
 * directive of the asReportDnd
 */
/* global dragula:false */
(function (ng, undefined){
  ng.module('frontendApp')
    .directive('asReportDnd', ['Utils', 'OrderAbleColl', function (Utils, OrderAbleColl) {
      return {
        restrict: 'A',
        scope : {
          asDndList : '=?',
          asDndListProp : '=?',
          asReportDnd : '=?',
          asDndHandle : '=?'
        },
        link : function(scope, element, attrs){
          if(!scope.asReportDnd) {
            return false;
          }

          var containers, options , nodeList, drak, listIdProp, drag, dragend,
            drop, cancel, remove, shadow, cloned, changeList, disabledClassName;

          disabledClassName = 'dnd-disabled';

          listIdProp = scope.asDndListProp;

          options = {
            moves: function (el, container, handle) {
              return !ng.element(handle).hasClass(disabledClassName) && !ng.element(el).hasClass(disabledClassName) ;
            },
            accepts: function (el, target, source, sibling) {
              return !ng.element(sibling).hasClass(disabledClassName);
            },
            direction: 'horizontal',
            copy: false,
            revertOnSpill: false,
            removeOnSpill: false
          };

          containers = attrs.containers;
          if( containers ) {
            nodeList = document.querySelectorAll(containers);
            if( nodeList.length ) {
              containers = Array.prototype.slice.apply(nodeList);
            }
          }else{
            containers = element[0];
          }

          drak = dragula(containers, options || {});

          drag = function(){};

          drop = function(el, container){
            if(!ng.isArray(scope.asDndList) || !scope.asDndList.length) {
              return false;
            }
            var children, newOrder, contr;

            contr = ng.element(container);
            children = contr.children(':not(.' + disabledClassName + ')');
            newOrder = [];
            children.each(function(i, item){
              newOrder.push(ng.element(item).data('original-index'));
              ng.element(item).data('original-index',i);
            });

            scope.$apply(changeList({method: 'setOrder', prop : 'columns', colIdProp: listIdProp, args: [newOrder]}));
          };

          changeList = function(msg){
            var defArrConfig, stateArrHelper, colIdProp;
            colIdProp = msg.colIdProp || 'name';
            defArrConfig = { unique : true, collection : scope.asDndList };
            if(colIdProp){ defArrConfig.prop = colIdProp; }
            stateArrHelper = new OrderAbleColl(defArrConfig);
            if(!ng.isFunction(stateArrHelper[msg.method])) {
              return false;
            }
            stateArrHelper[msg.method].apply(stateArrHelper, msg.args);
            scope.asDndList = stateArrHelper.get();
            return scope.asDndList;
          };

          dragend = angular.noop;
          cancel = angular.noop;
          remove = angular.noop;
          shadow = angular.noop;
          cloned = angular.noop;

          drak.on('drag', drag);
          drak.on('dragend', dragend);
          drak.on('drop', drop);
          drak.on('cancel', cancel);
          drak.on('remove', remove);
          drak.on('shadow', shadow);
          drak.on('cloned', cloned);

          scope.$on('$destroy', function() {
            drak.destroy();
          });
        }
      };
    }]);
})(angular);