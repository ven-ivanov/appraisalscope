'use strict';

/**
 * @ngdoc function
 * @name frontendApp.directive:asReportTab
 * @description
 * # asReportTab
 * directive of the asReportTab
 */
(function (ng, undefined){
  ng.module('frontendApp')
    .directive('asReportTab',['$templateRequest', '$compile', '$location','ObjectDecorator', 'Utils',
      function ($templateRequest, $compile, $location,ObjectDecorator, Utils) {

        return {
          restrict: 'AE',
          scope: {
            tab: '=asAttrModel'
          },
          link : function(scope, element, attrs) {

            // scope is reset.
            scope.inited = false;

            /**
             * Generating decorator name.
             *
             * @returns {string} decorator name
             */
            function generateDecoratorName(){
              return 'AdminReports' + Utils.capitalize(scope.tab.type) + 'ScopeDecorator';
            }

            // decorating scope with the required decorator.
            ObjectDecorator.decorate(scope, generateDecoratorName());


            /**
             * initiate tab by checking the tab id/location.hash, and if tab's scope got initiated already.
             */
            function initTab(){
              // checking if location.has equals tab.id and tab scope was initiated.
              if($location.hash() === scope.tab.id && !scope.inited) {

                //trying to invoking scope.init(); will fail in-case init is not a function or there's an error in init.
                try{
                  scope.init();
                }catch(e){
                  console.log(e);
                }
                // marking scope as being inititated.
                scope.inited = true;
              }
            }

            /**
             * Build tab
             */
            function buildTab(){

              // binding locationChangeSuccess to initTab - whenever you add a new tab or switch tab your #location changes
              scope.$on('$locationChangeSuccess',initTab);

              // on first call location doesnt change so we invoke init tab "manually"
              initTab();

              // requesting the desired tab template(view)
              $templateRequest('/components/admin/reports/views/partials/' + attrs.asReportTab + '.html')
              .then(function(tpl){

                // compiling the template with the scope after we decorated it with the required decorator.
                element.html($compile(tpl)(scope));
              });
            }


            // checking if scope got decorated with a resolve function.
            // resolve function happened after scope got decorated but before tab view got compiled.
            // resolve function used to load things before we actually got into the tab.
            if(ng.isFunction(scope.resolve)) {

              // resolving and then building tab.
              scope.resolve()
              .then(buildTab);
            } else {
              // did not found resolve or resolve isnt a function then just building tab.
              buildTab();
            }
          }
        };
      }
    ]);
})(angular);
