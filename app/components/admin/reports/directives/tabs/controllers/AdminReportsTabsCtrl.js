/**
 * Created by Simon on June 24th, 2015.
 */
'use strict';

/**
 * Represent a controller for Admin Reports tab directive.
 */
angular.module('frontendApp').controller('AdminReportsTabsCtrl', [
  '$scope', '$state', 'AdminReportsService', 'AdminReportsTabsService',
  function ($scope, $state, AdminReportsService, AdminReportsTabsService) {
    var vm = this;

    // Get the default Admin Report tabs.
    vm.tabs = AdminReportsTabsService.getTabs();

    AdminReportsService.setTabs(vm.tabs);

    /**
     * Set active class for a specified tab if it is the selected one.
     * @param tab A tab from the tab-array.
     * @returns {String} 'active' or empty (used with ngClass).
     */
    vm.tabClass = function (tab) {
      return AdminReportsService.getTabClass(tab, $scope.activeTab);
    };

    /**
     * Close the closable tab.
     * @param {Object} tab The current tab that fired the closed event.
     */
    vm.closeTab = function (tab) {
      // TODO: Show the confirmation modal if the tab is
      // create-new-report or edit-report.

      AdminReportsTabsService.closeTab(tab);
    };

    /** $SCOPE WATCH **/

    /**
     * Watch for the length of the tabs.
     * We want to go back to the list-reports tab if there is any
     * closeable tab that's been closed recently.
     */
    $scope.$watch(function () {
      return AdminReportsTabsService.tabs.length;
    }, function (newLength, oldLength) {
      if (oldLength !== 2 && newLength === 2) {
        // Go back to list-reports tab.
        $state.go('main.reports', {
          activeTab: AdminReportsTabsService.currentTab,
        });
      }
    });

    /**
     * Watch for the AdminReportsTabsService.currentTab,
     * especially the create-report or edit-report when switching between them
     */
    $scope.$watch(function () {
      return AdminReportsTabsService.currentTab;
    }, function (newTabId, oldTabId) {
      if (angular.isDefined(newTabId) && angular.isDefined(oldTabId)) {
        // @TODO: show a confirmation modal to ask if the user wants to save the changes first.
      }
    });
  }
]);
