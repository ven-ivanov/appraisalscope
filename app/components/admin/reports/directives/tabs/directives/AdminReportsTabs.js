/**
 * Created by Simon on June 24th, 2015.
 */
'use strict';

/**
 * Admin Reports tabs directive.
 * Represents an HTML element that can be used by defined <admin-reports-tab>
 */
angular.module('frontendApp').directive('adminReportsTabs', [
  function () {
    return {
      restrict: 'E',
      templateUrl: '/components/admin/reports/directives/tabs/directives/partials/admin-reports-tabs.html',
      controller: 'AdminReportsTabsCtrl',
      controllerAs: 'adminReportsTabsCtrl'
    };
  }
]);