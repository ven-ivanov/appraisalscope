'use strict';

/**
 * @ngdoc function
 * @name frontendApp.directive:asReportHeight
 * @description
 * # asReportHeight
 * directive of the asReportHeight
 */
(function (ng, undefined){
  ng.module('frontendApp')
    .directive('asReportHeight',[function () {

      return {
        restrict: 'AE',
        scope : {
          asReportHeight : '='
        },
        link : function(scope, element) {
          scope.$watch(function(){
            return element[0].getBoundingClientRect().height;
          }, function(newVal){
            // Holds the search bar height in pixes.
            var searchBarHeight = 80;
            scope.asReportHeight = parseInt(newVal) - searchBarHeight;
          });
        }
      };
    }
    ]);
})(angular);