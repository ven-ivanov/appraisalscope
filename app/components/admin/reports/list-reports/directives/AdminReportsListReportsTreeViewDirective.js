/*
 @license Angular Treeview version 0.1.6
 ⓒ 2013 AHN JAE-HA http://github.com/eu81273/angular.treeview
 License: MIT
 */

'use strict';

var app = angular.module('frontendApp');

app.directive('angularTreeview', ['$compile', function ($compile) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      //tree id
      var treeId = attrs.treeId;

      //tree model
      var treeModel = attrs.treeModel;

      //node id
      var nodeId = attrs.nodeId || 'id';

      //node label
      var nodeLabel = attrs.nodeLabel || 'label';

      //children
      var nodeChildren = attrs.nodeChildren || 'children';

      //tree template
      // TODO: Update to ng-class
      var template = '<ul ng-sortable="{group: {name: \'constructor\', pull: \'clone\', put: false}, handle: \'.tree-label\'}">' +
                     // for each node in tree model
                     '<li data-ng-repeat="node in ' + treeModel + ' | childrenSearch:treeCtrl.columnsSearch">' +
                     // if node.children and collapsed show closed folder icon
                     '<i class="collapsed fa fa-folder-o" data-ng-show="node.' + nodeChildren +
                     // on click fire node head select
                     '.length && !node.expanded" data-ng-click="' + treeId + '.selectNodeHead(node, $event)"></i>' +
                     // if node.children and expanded show opened folder icon
                     '<i class="expanded fa fa-folder-open-o" data-ng-show="node.' + nodeChildren +
                     // on click fire node head select
                     '.length && node.expanded" data-ng-click="' + treeId + '.selectNodeHead(node)"></i>' +
                     // if not node.children - it's element - show icon
                     '<i class="normal" data-ng-hide="node.' + nodeChildren + '.length"></i> ' +
                     // on click fire node label select
                     '<span class="tree-label" data-ng-class="node.selected" data-ng-click="' + treeId +
                     // show node label text
                     '.selectNodeLabel(node)">{{node.' + nodeLabel + '}}</span>';

      //check tree id, tree model
      if (treeId && treeModel) {

        //root node
        if (attrs.angularTreeview) {

          //create tree object if not exists
          scope[treeId] = scope[treeId] || {};

          //if node head clicks,
          scope[treeId].selectNodeHead = scope[treeId].selectNodeHead || function (selectedNode, evt) {

            if (!selectedNode.loaded) {
              selectedNode.loaded = true;
              var target = angular.element(evt.currentTarget.parentNode);

              var children = '<div data-angular-treeview="true" data-ng-hide="!node.expanded" data-tree-id="' + treeId +
                             '" data-tree-model="node.' + nodeChildren + '" data-node-id=' + nodeId +
                             ' data-node-label=' + nodeLabel + ' data-node-children=' + nodeChildren + '></div>' +
                             '</li>' + '</ul>';

              target.append($compile(children)(target.scope()));

              //$compile(target.contents())(target.scope());
            }
            //Collapse or Expand
            selectedNode.expanded = !selectedNode.expanded;
          };

          //if node label clicks,
          scope[treeId].selectNodeLabel = scope[treeId].selectNodeLabel || function (selectedNode) {

            //remove highlight from previous node
            if (scope[treeId].currentNode && scope[treeId].currentNode.selected) {
              scope[treeId].currentNode.selected = undefined;
            }

            //set highlight to selected node
            selectedNode.selected = 'selected';

            //set currentNode
            scope[treeId].currentNode = selectedNode;
          };
        }

        //Rendering template.
        element.html('').append($compile(template)(scope));
      }
    }
  };
}]);