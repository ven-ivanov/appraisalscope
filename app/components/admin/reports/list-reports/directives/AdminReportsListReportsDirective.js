/**
 * Created by Simon on June 27th, 2015.
 */
'use strict';
angular.module('frontendApp').directive('adminReportsListReports', [
  '$compile', 'DirectiveInheritanceService',
  function ($compile, DirectiveInheritanceService) {
    return {
      templateUrl: '/components/admin/reports/list-reports/directives/partials/list-reports.html',
      controller: 'AdminReportsListReportsCtrl',
      controllerAs: 'listReportsCtrl',
      scope: {
        disabled: '@'
      },
      compile: function () {
        return {
          pre: function (scope) {
            // Expose trigger download
            DirectiveInheritanceService.inheritDirective.call(scope);
            // Whether displaying disabled appraisers
            scope.disabled = scope.$eval(scope.disabled);
          },
          post: function (scope) {
            /**
             * Compile directives after state set
             */
            scope.compileDirectives = function () {
              // Client Permission directive
              angular.element('#list-reports-client-permission').replaceWith($compile('<admin-reports-list-reports-client-permission></admin-reports-list-reports-client-permission>')(scope));
              // Staff Permission directive
              angular.element('#list-reports-staff-permission').replaceWith($compile('<admin-reports-list-reports-staff-permission></admin-reports-list-reports-staff-permission>')(scope));
              // Create new report directive
              angular.element('#list-reports-create-report').replaceWith($compile('<admin-reports-list-reports-create-report ctrl="listReportsCtrl"></admin-reports-list-reports-create-report>')(scope));
              // Generate View directive
              angular.element('#list-reports-generate-view').replaceWith($compile('<admin-reports-list-reports-generate-view ctrl="listReportsCtrl"></admin-reports-list-reports-generate-view>')(scope));
              // Show alert reprt
              angular.element('#list-reports-alerts').replaceWith($compile('<admin-reports-list-reports-alerts ctrl="listReportsCtrl"></admin-reports-list-reports-alerts>')(scope));
              // Don't run again
              scope.compileDirectives = angular.noop;
            };
          }
        };
      }
    };
  }
]);
