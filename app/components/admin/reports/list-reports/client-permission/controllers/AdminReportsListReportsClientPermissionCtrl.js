'use strict';

/**
 * Represents an controller for the Admin Reports > List Reports - Client Permission modal
 **/
angular.module('frontendApp').controller('AdminReportsListReportsClientPermissionCtrl', [
  '$scope',
  'AdminReportsResourceService', 'AdminReportsListReportsService',
  'AdminReportsListReportsClientPermissionService',
  function ($scope, AdminReportsResourceService, AdminReportsListReportsService, AdminReportsListReportsClientPermissionService) {
    var Service = AdminReportsListReportsClientPermissionService;
    var vm = this;
    vm.safeData = Service.safeData;
    vm.userLevels = [];
    vm.clientPermissionTableData = [];
    vm.clientPermissionTablePagination = {};

    /**
     * Set the current page button active.
     * @param   {Number} pageNo The current page number.
     * @returns {String} Bootstrap active class for the button.
     */
    vm.pageButtonClass = function (pageNo) {
      return Service.getPaginationButtonClass(pageNo, vm.currentPage);
    };

    /**
     * Get data for page.
     * @param {Number} pageNo The page number.
     */
    vm.getPage = function (pageNo) {
      if (pageNo > 0 && pageNo <= vm.totalPages.length) {
        var id = AdminReportsListReportsService.selectedReportId;

        // Populate the table data when the page changed.
        Service.getReportClientPermissionData(id, pageNo);
      }
    };

    /**
     * Handle the event when a permission check-box has changed its value.
     * @param {Integer} clientId the current client ID.
     */
    vm.updateClientPermissionState = function (clientId) {
      Service.updateClientPermissionState(clientId);
    };

    /**
     * Save the report client-permissions changes to backend.
     */
    vm.submitClientPermissions = function () {
      // 1. Start progress bar
      // 2. Send request
      var selectedReportId = AdminReportsListReportsService.selectedReportId;
      Service.updateReportClientPermissions(selectedReportId)
      .then(function (response) {
        var updatedClientPermissionData = response.data;
        if (angular.isObject(updatedClientPermissionData)) {
          // 3. Succeeded 
          $scope.$broadcast('hide-modal', 'client-permission');
          //$scope.$broadcast('show-modal', 'client-permission-success');
        }
      });

      // 4. Failed
      // $scope.$broadcast('hide-modal', 'client-permission');
      // $scope.$broadcast('show-modal', 'client-permission-failure');

      // 5. Stop progress bar
    };

    /** $SCOPE WATCHING **/

    /**
     * Watch for the AdminReportsListReportsService.selectedReportId changed
     * then make a request to get all available client-permissions,
     * and populate the data onto the table.
     */
    $scope.$watch(function () {
      return AdminReportsListReportsService.selectedReportId;
    }, function (id) {
      if (angular.isString(id)) {

        // Get the User-Levels for the current Report once.
        Service.getReportClientPermissionUserLevels(id)
        .then(function (response) {
          var data = response.data;
          if (angular.isArray(data)) {
            vm.userLevels = data;
          }
        });
        
        // Populate the table's first page data.
        Service.getReportClientPermissionData(id);
      }
    });

    /** SCOPE WATCHINGS **/
    
    /**
     * Watch the table pagination.
     */
    $scope.$watch(function () {
      return Service.displayData.tablePagination;
    }, function (newPagination) {
      vm.currentPage = newPagination.currentPage;
      vm.totalPages = newPagination.totalPages;
    });
  }
]);