'use strict';

angular.module('frontendApp').factory('AdminReportsListReportsClientPermissionService', [
  'AsDataService',
  'AdminReportsService', 'AdminReportsResourceService',
  function (AsDataService, AdminReportsService, AdminReportsResourceService) {
    var Service = {
      safeData: {
        // Hold the Client-Permission hash-table data.
        clientPermissionTableData: {},
        // Hold the assigned Client-Permission data {id: "124235", userLevels: {...}}
        clientPermissions: [],
      },
      displayData: {
        currentReport: {
          id: 0
        },
        tablePagination: {}
      },
      clientPermissionTablePagination: {},

      /**
       * Check if a client exists in the collection by its ID.
       * @param   {String}  clientId The client ID string.
       * @returns {Boolean} True if exists; otherwise False.
       */
      containsClientPermission: function (clientId) {
        for (var i = 0, len = Service.safeData.clientPermissions.length; i < len; i++) {
          if (Service.safeData.clientPermissions[i].id === clientId) {
            return true;
          }
        }
        
        return false;
      },

      /**
       * Get a client-permission element from the Service.clientPermissions by ID.
       * @param   {String} clientId The client ID.
       * @returns {Object} The element which has the same specific ID; otherwise undefined.
       */
      getClientPermissionById: function (clientId) {
        var foundIndex = Service.safeData.clientPermissions.map(function (element) {
          return element.id;
        }).indexOf(clientId);

        return Service.safeData.clientPermissions[foundIndex];
      },

      /**
       * Hash the main client-permission table data to get the client-permission selection data
       * for binding and update
       * @TODO: Limit the Service.clientPermissions before update (only contains the dirty client-permission).
       */
      hashClientPermissionsFromTableData: function () {
        var clientPermission = {};
        angular.forEach(Service.safeData.clientPermissionTableData, function (rowData) {
          if (!Service.containsClientPermission(rowData.id)) {
            clientPermission = {
              id: rowData.id,
              dirty: false,
              userLevels: rowData.userLevels
            };
            Service.safeData.clientPermissions.push(clientPermission);
          }
        });
      },

      /**
       * Create an array with length equal to page total.
       * @param {Array} pageNumber  The total-page number of the table.
       * @returns {Array}  The array that the ngRepeat uses to create the pagination-buttons.
       */
      getPagesRange: function (pageNumber) {
        return new Array(pageNumber);
      },

      /**
       * Set CSS class for the pagination buttons.
       * @param   {Number} pageNo The current page number.
       * @returns {String} Bootstrap active class for the button.
       */
      getPaginationButtonClass: function (page, currentPage) {
        if (page === currentPage) {
          return 'active';
        }
        return '';
      },

      /**
       * Create requests to get data for the Report User Levels.
       * @param   {Number} id The current Report ID.
       * @returns {Object} User-Levels $promise.
       */
      getReportClientPermissionUserLevels: function (id) {
        // Get the User-Levels for the current Report.
        var userLevelsParamObj = {
          id: id
        };

        // Get all client permissions user levels that are available for the current report.
        return AdminReportsResourceService.listReports.getClientPermissionUserLevels(userLevelsParamObj);
      },

      /**
       * Create requests to get data for the Report Client Permission.
       * Also apply the pagination for the table.
       * @param {Number} id The current Report ID.
       * @param {Number} page     The current page of the table.
       */
      getReportClientPermissionData: function (id, page) {
        // Reset the table and selection.
        Service.safeData.clientPermissionTableData = {};

        // Reset the client-permission selection array if current report changed.
        if (Service.displayData.currentReport.id !== id) {
          Service.safeData.clientPermissions.splice(0, Service.safeData.clientPermissions.length);
        }
        
        // Update the current report ID.
        Service.displayData.currentReport.id = id;

        // Get the grid-state
        var gridStateObj = AdminReportsService.getGridState('client-permission');

        // Get the first page by default if page is undefined.
        if (angular.isDefined(page) && angular.isNumber(page)) {
          var itemsPerPage = 15;

          // Eg. Page 1 -> start = 1; Page 2 -> start = 16; page 3 -> start = 31
          gridStateObj.pagination.start = itemsPerPage * (page - 1) + 1;
        }

        // Prepare the request parameter
        var clientPermissionsParamObj = {
          id: id,
          gridState: JSON.stringify(gridStateObj)
        };

        // Get all client permissions that are available for the current report.
        AdminReportsResourceService.listReports.getAvailableClientPermission(clientPermissionsParamObj)
        .then(function (response) {
          var data = response.data;
          if (angular.isArray(data)) {
            // Store to Service.safeData as a hash-table data.
            Service.hashData(data, 'clientPermissionTableData');
            // Hash the table selection data into Service.safeData.clientPermissions
            Service.hashClientPermissionsFromTableData(data);
          }

          // Update the table pagination
          var pagination = response.meta.pagination;
          if (angular.isObject(pagination)) {
            var paginationData = {
              currentPage: pagination.page,
              totalPages: Service.getPagesRange(pagination.totalPages)
            };
            Service.displayData.tablePagination = paginationData;
          }
        });
      },

      /**
       * Update a client-permission state after checking/un-checking.
       * @param {String} clientId the current client ID.
       */
      updateClientPermissionState: function (clientId) {
        var clientPermission = Service.getClientPermissionById(clientId);
        if (angular.isDefined(clientPermission)) {
          clientPermission.dirty = true;
        }
      },

      /**
       * Prepare a request to update the report client-permission.
       * @param   {Number} id The report ID.
       * @returns {Object} Report client-permission update result $promise.
       */
      updateReportClientPermissions: function (id) {
        var paramObj = {
          id: id
        };

        var dirtyArr = [];

        // filter the Service.clientPermissions for the dirty one(s).
        angular.forEach(Service.safeData.clientPermissions, function (clientPermission) {
          if (clientPermission.dirty) {
            dirtyArr.push(clientPermission);
          }
        });

        return AdminReportsResourceService.listReports.updateReportClientPermissions(paramObj, dirtyArr);
      }
    };

    Service.hashData = AsDataService.hashData.bind(Service);
    Service.formatData = AsDataService.formatData.bind(Service, 'clientPermissionTableData');
    Service.transformData = AsDataService.transformData.bind(Service);

    return Service;
  }
]);