/**
 * Created by Peter on June 26th, 2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Client permission directive will be compiled to admin-reports-list-reports-client-permission
 * in the adminReportsListReports directive
 */
app.directive('adminReportsListReportsClientPermission', [function () {
  return {
    templateUrl: '/components/admin/reports/list-reports/client-permission/directives/partials/client-permission.html',
    controller: 'AdminReportsListReportsClientPermissionCtrl',
    controllerAs: 'clientPermissionCtrl'
  };
}]);
