/**
 * Created by Simon on June 24th, 2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * The controller for the Admin Reports > List Reports table.
 */
app.controller('AdminReportsListReportsTableCtrl', [
  '$scope',
  'AdminReportsListReportsService',
  function ($scope, AdminReportsListReportsService) {
    var vm = this;
    vm.reportTableData = [];
    vm.selectAllReports = false;
    vm.selectedReports = [];

    /**
     * Call the directive controller to show the Staff Perrmission modal.
     * @params {Number} id The report-ID of the current row.
     */
    vm.openStaffPermissionModal = function (id) {
      // Store the current report-ID so other controller can use it.
      AdminReportsListReportsService.selectedReportId = id;

      // We must call the $parent to broadcast,
      // because that's where the directives were bound to the view.
      $scope.$parent.$broadcast('show-modal', 'staff-permission');
    };

    /**
     * Call the directive controller to show the Client Perrmission modal.
     * @params {Number} id The report-ID of the current row.
     */
    vm.openClientPermissionModal = function (id) {
      // Store the current report-ID so other controller can use it.
      AdminReportsListReportsService.selectedReportId = id;

      // We must call the $parent to broadcast,
      // because that's where the directives were bound to the view.
      $scope.$parent.$broadcast('show-modal', 'client-permission');
    };

    /**
     * Handle the checked event for the row-checkbox.
     * @param   {Number}  id The report-id that the row-checkbox was bound to.
     * @returns {Boolean} True if the checkbox is checked, otherwise False.
     */
    vm.reportCheckboxCheckedChanged = function (id) {
      return vm.selectedReports.indexOf(id) > -1;
    };

    /**
     * Toggle checked state of a report row.
     * @param {Number} id The current report-row ID.
     */
    vm.toggleSelection = function (id) {
      var idx = vm.selectedReports.indexOf(id);
      if (idx > -1) {
        // is currently selected
        vm.selectedReports.splice(idx, 1);
      } else {
        // is newly selected
        vm.selectedReports.push(id);
      }
      // Update the selected/checked reports Array to the service.
      AdminReportsListReportsService.selectedReports = vm.selectedReports;
    };

    /**
     * Toggle all report-row selection if the vm.selectAllReports changed.
     */
    vm.toggleAllReports = function () {
      if (vm.selectAllReports) {
        angular.forEach(vm.reportTableData, function (report) {
          var idx = vm.selectedReports.indexOf(report.id);
          if (idx < 0) {
            // push if the ID doesn't exist.
            vm.selectedReports.push(report.id);
          }
        });
      } else {
        // Empty the selection array, this has no effect on performance whatsoever.
        vm.selectedReports.splice(0, vm.selectedReports.length);
      }

      // Update the selected/checked reports Array to the service.
      AdminReportsListReportsService.selectedReports = vm.selectedReports;
    };

    /**
     * Populate Admin Reports data
     * @params loadParams: the gridState object.
     */
    vm.init = function () {
      AdminReportsListReportsService.getAdminReportList();
    };
    vm.init();

    /**
     * Watch table data
     */
    $scope.$watchCollection(function () {
      return AdminReportsListReportsService.displayData['list-reports'].data;
    }, function (newVal) {
      if (!angular.isArray(newVal)) {
        return;
      }

      /**
       * Use Lodash to:
       * use groupBy to group data follow _groupBy.
       * use pairs to creates a two dimensional array of the key-value pairs for object
       * use map and zip to convert array data to
       * [
       *  {"groupBy": name group, "items":[{item}, ...]}, ...
       * ]
       *
       */
      var groupedListReports = _.chain(newVal)
        .groupBy('data1')
        // pairs(): Creates a two dimensional array of the key-value pairs for object
        .pairs()
        .map(function (currentItem) {
          /**
           *  zip: Creates an array of grouped elements, the first of which
           *  contains the first elements of the given arrays,
           *  the second of which contains the second elements of the given arrays, and so on.
           */
          return _.object(_.zip(['groupBy', 'items'], currentItem));
        })
        .value();
      vm.reportTableData = groupedListReports;
    });
  }
]);