/**
 * Created by Simon on June 24th, 2015.
 */
'use strict';

/**
 * The main controller for the Admin Reports > List Reports view.
 */
angular.module('frontendApp').controller('AdminReportsListReportsCtrl', [
  '$scope', '$state', '$stateParams',
  'AdminReportsService', 'AdminReportsListReportsService',
  'AdminReportsTabsService',
  function ($scope, $state, $stateParams, AdminReportsService, AdminReportsListReportsService, AdminReportsTabsService) {

    var vm = this;
    var listReportsSection = 'list-reports';
    var Service = AdminReportsListReportsService;
    // Whether displaying disabled view
    Service.disabled = $scope.disabled;
    // Stores all checked/selected row(s)
    vm.selectedRecords = [];

    vm.canEditReport = false;
    vm.canDeleteReports = false;

    /**
     * Add new tab (named Add new Report)
     * and compile the create-report directive
     */
    vm.createReport = function () {
      AdminReportsTabsService.openReportTab(false);
      $state.go('main.reports', {
        activeTab: AdminReportsTabsService.currentTab
      });
    };

    /**
     * Add new tab (named Edit Report)
     * Compile and switch to the create-report directive in edit mode.
     */
    vm.editReport = function () {
      AdminReportsTabsService.openReportTab(true);
      $state.go('main.reports', {
        activeTab: AdminReportsTabsService.currentTab
      });
    };

    /**
     * Delete the current selected Report.
     */
    vm.deleteReport = function () {
      // @TODO: Show confirmation modal.
      // If user clicks Yes, send a request to delete the selected report.
      // Update the Reports table.
      Service.deleteRowReport();
    };

    /**
     * Copy the current selected Report to a new one with named "Copy - [Original-Name]".
     */
    vm.copyReport = function () {
      Service.copyRowReport();
    };

    /**
     * Add new tab (named Generated Report)
     * and compile the generated-report directive
     */
    vm.generateReport = function () {

      AdminReportsTabsService.openGenerateReportTab();
      $state.go('main.reports', {
        activeTab: AdminReportsTabsService.currentTab
      });
    };

    /**
     * Show the Alerts modal.
     */
    vm.showReportAlerts = function () {
      $scope.$broadcast('show-modal', 'alerts');
    };

    /**
     * Initiate data and state
     */
    vm.init = function () {
      // set ui-state on load from Service then compile the Directive for it
      AdminReportsListReportsService.setStateOnLoad(listReportsSection, $stateParams, vm)
      .then(function () {
        // Compile the directive from AdminReportsListReportsDirective
        $scope.compileDirectives();
      });

      // Keep reference to which section we're on
      AdminReportsService.section = listReportsSection;
    };

    // Init controller load
    vm.init();

    /** $SCOPE WATCH **/

    /**
     * Watch for the length of the selected/checked report-rows.
     */
    $scope.$watch(function () {
      return AdminReportsListReportsService.selectedReports.length;
    }, function (newVal) {
      // Enable/Disable the command buttons for the selected/checked report-rows.

      /**
       * Enable Edit, Copy, Generate View, Alerts buttons
       * when there is only one selected/checked report.
       */
      vm.canEditReport = newVal === 1;

      /**
       * Enable Delete button when there is more than one row was selected.
       */
      vm.canDeleteReports = newVal >= 1;
    });
  }
]);
