'use strict';

angular.module('frontendApp').factory('AdminReportsListReportsStaffPermissionService', [
  'AdminReportsService', 'AdminReportsResourceService',
  function (AdminReportsService, AdminReportsResourceService) {
    var Service = {
      // Hold all the staff's IDs selection.
      selectedStaffIds: [],
      // Hold the table data.
      staffPermissionTableData: [],

      /**
       * Handle the checked event for the row-checkbox.
       * @param   {Number}  staffId The staff-ID that the row-checkbox was bound to.
       * @returns {Boolean} True if the checkbox is checked, otherwise False.
       */
      staffCheckboxCheckedChanged: function (staffId) {
        return Service.selectedStaffIds.indexOf(staffId) > -1;
      },

      /**
       * Toggle the checked state of a report-row's checkbox.
       * @param {Number} staffId The current staff-row ID.
       */
      toggleStaffSelection: function (staffId) {
        var idx = Service.selectedStaffIds.indexOf(staffId);
        if (idx > -1) {
          // is currently selected
          Service.selectedStaffIds.splice(idx, 1);
        } else {
          // is newly selected
          Service.selectedStaffIds.push(staffId);
        }
      },

      /**
       * Add all staffs' ID to the selection array (if not exists).
       */
      selectAllStaffs: function () {
        angular.forEach(Service.staffPermissionTableData, function (row) {
          var idx = Service.selectedStaffIds.indexOf(row.id);
          if (idx < 0) {
            // push if the ID doesn't exist.
            Service.selectedStaffIds.push(row.id);
          }
        });
      },

      /**
       * Empty the staff selection array.
       */
      deselectAllStaffs: function () {
        // this has no effect on performance whatsoever.
        Service.selectedStaffIds.splice(0, Service.selectedStaffIds.length);
      },

      /**
       * Get and populate the report's staff-permission data and selection.
       * @param {Number} id The current selected report ID.
       */
      getReportStaffPermissionData: function (id) {

        // Reset the table and selection.
        Service.staffPermissionTableData.splice(0, Service.staffPermissionTableData.length);
        Service.selectedStaffIds.splice(0, Service.selectedStaffIds.length);

        // Get the grid-state
        var gridStateObj = AdminReportsService.getGridState('staff-permission');

        // Prepare the request parameter
        var staffPermissionParamObj = {
          id: id,
          gridState: JSON.stringify(gridStateObj)
        };

        // Get all available staff for the report.
        AdminReportsResourceService.listReports.getAvailableStaffPermission(staffPermissionParamObj)
        .then(function (response) {
          var tableData = response.data;

          if (angular.isArray(tableData)) {
            // Populate the data onto the table.
            Service.staffPermissionTableData = tableData;
          }

          // Get the assigned staff for the report.
          AdminReportsResourceService.listReports.getStaffPermission(staffPermissionParamObj)
          .then(function (response) {
            var assignedStaffIds = response.data;
            if (angular.isArray(assignedStaffIds)) {
              Service.selectedStaffIds = assignedStaffIds;
            }
          });
        });
      },
      
      /**
       * Update the assigned staff-permission for the current report.
       * @param   {Number} id         The rurrent report ID.
       * @param   {Array}  assignedStaffIds The assigned staff IDs.
       * @returns {Object} Update result $promise.
       */
      updateReportStaffPermissions: function (id) {
        var paramObj = {
          id: id
        };

        return AdminReportsResourceService.listReports.updateReportStaffPermissions(paramObj, Service.selectedStaffIds);
      }
    };

    return Service;
  }
]);