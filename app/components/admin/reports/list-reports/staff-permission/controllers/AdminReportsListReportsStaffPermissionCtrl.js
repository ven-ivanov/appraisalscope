'use strict';

/**
 * Represents an controller for the Admin Reports > List Reports - Staff Permission modal
 */
angular.module('frontendApp').controller('AdminReportsListReportsStaffPermissionCtrl', [
  '$scope',
  'AdminReportsListReportsStaffPermissionService', 'AdminReportsListReportsService',
  function ($scope, AdminReportsListReportsStaffPermissionService, AdminReportsListReportsService) {

    var Service = AdminReportsListReportsStaffPermissionService;
    var vm = this;

    vm.staffPermissionTableData = [];
    vm.selectAllStaffs = false;
//    vm.selectedStaffIds = [];

    /**
     * Handle the checked event for the row-checkbox.
     * @param   {Number}  staffId The staff-ID that the row-checkbox was bound to.
     * @returns {Boolean} True if the checkbox is checked, otherwise False.
     */
    vm.staffCheckboxCheckedChanged = function (staffId) {
      return Service.staffCheckboxCheckedChanged(staffId);
    };

    /**
     * Toggle checked state of a report row.
     * @param {Number} staffId The current staff-row ID.
     */
    vm.toggleSelection = function (staffId) {
      Service.toggleStaffSelection(staffId);
    };

    /**
     * Toggle all staff-row selection if the vm.selectedAllStaffs changed.
     */
    vm.toggleAllStaffs = function () {
      if (vm.selectAllStaffs) {
        Service.selectAllStaffs();
      } else {
        Service.deselectAllStaffs();
      }
    };

    /**
     * Save the report client-permissions changes to backend.
     * @TODO: move this function to the AdminReportsListReportsStaffPermissionService.
     */
    vm.submitStaffPermissions = function () {
      // 1. Start progress-bar
      // 2. Send request
      var selectedReportId = AdminReportsListReportsService.selectedReportId;
      Service.updateReportStaffPermissions(selectedReportId)
      .then(function(response){
        var updatedStaffPermissionData = response.data;
          if (angular.isObject(updatedStaffPermissionData)) {
            // 3. Succeeded
            $scope.$broadcast('hide-modal', 'staff-permission');
            //$scope.$broadcast('show-modal', 'staff-permission-success');
          }
      });
      
      // 4. Failed
      // $scope.$broadcast('hide-modal', 'staff-permission');
      // $scope.$broadcast('show-modal', 'staff-permission-failure');

      // 5. Stop progress bar
    };

    /** $SCOPE WATCHING **/

    /**
     * Watch for the AdminReportsListReportsService.selectedReportId value changed.
     * Then make a request to get all available client-permissions,
     * and populate the data onto the table.
     */
    $scope.$watch(function () {
      return AdminReportsListReportsService.selectedReportId;
    }, function (newid, oldid) {
      if (newid !== oldid) {
        // Populate the staff-permission data for the current report.
        Service.getReportStaffPermissionData(newid);
      }
    });

    /**
     * Watch for the AdminReportsListReportsStaffPermissionService.staffPermissionTableData collection changed.
     * Then update the table data and selection.
     */
    $scope.$watchCollection(function () {
      return Service.staffPermissionTableData;
    }, function (updatedTableData) {
      vm.staffPermissionTableData = updatedTableData;
    });
    
//    $scope.$watchCollection(function () {
//      return Service.selectedStaffIds;
//    }, function (updatedAssignedStaffIds) {
//      vm.selectedStaffIds = updatedAssignedStaffIds;
//    });
  }
]);