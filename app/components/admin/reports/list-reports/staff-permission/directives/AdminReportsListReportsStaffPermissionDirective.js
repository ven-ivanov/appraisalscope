/**
 * Created by Peter on June 26th, 2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Staff permission directive will be compiled to admin-reports-list-reports-staff-permission
 * in the adminReportsListReports directive
 */
app.directive('adminReportsListReportsStaffPermission', [function () {
  return {
    templateUrl: '/components/admin/reports/list-reports/staff-permission/directives/partials/staff-permission.html',
    controller: 'AdminReportsListReportsStaffPermissionCtrl',
    controllerAs: 'staffPermissionCtrl'
  };
}]);
