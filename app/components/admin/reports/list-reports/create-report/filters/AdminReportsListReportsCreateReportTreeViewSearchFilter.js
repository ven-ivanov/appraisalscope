'use strict';

// Used as columns tree search filter in angularTreeview directive
angular.module('frontendApp').filter('childrenSearch', function () {
  return function childrenSearch(nodes, search) {
    // if search field is empty - return all nodes
    if (!search) {
      return nodes;
    }
    // return matching results for passed nodes collection
    return nodes.filter(function(node) {
      if (node.children) { // if child node is a group, not element...
        return childrenSearch(node.children, search).length; // ...check if there are matching items inside
      } else { // otherwise it's element...
        return node.label.match(new RegExp(search, 'i')); // ...check for match in label text
      }
    });
  };
});