'use strict';

angular.module('frontendApp').factory('AdminReportsCreateReportService', [
  'AsDataService', 'AdminReportsConfigService', function (AsDataService, AdminReportsConfigService) {

    var fixedSelects = {
      headerAlignments: [{
        value: 0,
        label: 'Left'
      }, {
        value: 1,
        label: 'Center'
      }, {
        value: 2,
        label: 'Right'
      }],
      pageBreaks: [{
        value: 0,
        label: 'No Page Break'
      }, {
        value: 1,
        label: 'After group'
      }],
      detailTypes: [{
        value: 0,
        label: 'Detail'
      }, {
        value: 1,
        label: 'Summary'
      }]
    };

    var Service = {
      safeData: {
        // Report Object layout settings
        id: 0,
        typeId: 0,
        name: '',
        headerAlignment: '',
        pageBreak: '',
        description: '',
        header: '',
        detailType: 0,

        // Report Object data settings
        filters: [],
        groupers: [],
        columns: []
      },

      displayData: {

        // lists
        reportTypes: [], // Report types list
        reportColumns: [], // Columns for selected report type

        // Report Object layout settings
        reportType: {}, // Report type item
        name: 'New report', // Selected report type
        pageBreak: fixedSelects.pageBreaks[0],
        description: 'New report description',
        header: 'New report header',
        detailType: fixedSelects.detailTypes[0],
        headerAlignment: fixedSelects.headerAlignments[0],

        // Report Object data settings
        filters: [],
        groupers: [],
        columns: [{
          id: 0,
          text: 'Drop columns here',
          fake: true
        }],

        // selected filter items in FILTERS section
        filter: {
          column: {}, // selected column
          operation: {}, // selected operation (<, >, contains...)
          constraint: '' // string value to check
        },
        filterField: {}, // Flat list of columns, same as used in tree
        filterConstraint: '@@DYNAMIC_DATA@@' // Macro for dynamic filter
      },

      selects: fixedSelects,

      /**
       * Temporary transformers
       */
      revert: function () {
        return {
          // Report Object layout settings
          id: Service.safeData.id || null,
          typeId: Service.displayData.reportType.id,
          name: Service.displayData.name,
          headerAlignment: Service.displayData.headerAlignment.value,
          pageBreak: Service.displayData.pageBreak.value,
          description: Service.displayData.description,
          header: Service.displayData.header.value,
          detailType: 0,

          // Report Object data settings
          filters: Service.displayData.filters.map(function(item) {
            return {
              column: item.column.id,
              operation: item.operation.id,
              constraint: item.constraint
            };
          }),
          groupers: Service.displayData.groupers.map(function(item) {
            return item.id;
          }),
          columns: Service.displayData.columns.filter(function(item) {
            return !item.fake;
          }).map(function(item) {
            return {
              id: item.id,
              sort: item.sort
            };
          })
        };
      },

      /**
       * Sorters, groupers and columns options
       * for Sortable drag and drop
       */

      sortableOptions: function () {
        return {
          group: {
            name: 'constructor',
            pull: 'clone'
          },
          // Drag ended Sortable event
          onAdd: function (evt) {
            var items = evt.models; // Drop container model
            var dropIndex = evt.newIndex; // To put group elements from group drop position in list
            var newItems; // Will be single element or children if group dropped, see 'if'
            var groupDropped = evt.model.children;

            // Fill newItems array to iterate
            if (groupDropped) { // If group dropped
              items.splice(evt.newIndex, 1); // Kill it...
              // TODO Columns list can be multi-level?
              newItems = evt.model.children; // ...and iterate nested
            } else { // Single element dropped
              newItems = [evt.model]; // Single item to check
            }

            // Iterate items to add
            newItems.forEach(function (dropped) {
              // Check for duplicates
              var occurrences = items.filter(function (item) {
                return item.id === dropped.id;
              }).length;
              if (occurrences > 1) { // Element was dropped and list already contains it
                items.splice(dropped.id, 1); // Kill it
              } else if (!occurrences && groupDropped) { // We iterate elements of dropped group
                items.splice(dropIndex, 0, dropped); // Add item of group next to group dropped spot
                dropIndex++; // For next item
              }
            });
          }
        };
      },

      /**
       * Load report types
       * fires on create report controller init
       */
      initBaseState: function () {
        return Service.request('getReportTypes')
        .then(function (response) {

          Service.hashData(response.data, 'reportTypes');
          Service.formatData('reportTypes');
          Service.displayData.reportType = Service.displayData.reportTypes[0]; // Pick 1st to prevent empty state
        });
      },

      /**
       * Load columns for constructor tree and filter columns
       * fires on report type change
       */
      onReportTypeChange: function () {
        return Service.request('loadReportColumns', Service.displayData.reportType.id)
        .then(function (response) {
          // For tree
          Service.displayData.reportColumns = response.data;

          // Flat list for filter columns list
          Service.displayData.flatColumns = [];
          Service.displayData.reportColumns.forEach(function (node) {
            Service.displayData.flatColumns = Service.displayData.flatColumns.concat(node.children.map(function (item) {
              item.group = node.label;
              return item;
            }));
          });
          Service.displayData.filter.column = Service.displayData.flatColumns[0];
          Service.updateOperations();
        });
      },

      /**
       * Add new filter into the filters table.
       * Fires on 'ADD FILTER' click
       */
      updateOperations: function () {
        Service.displayData.filterOperations =
        AdminReportsConfigService.getOperationsByType(Service.displayData.filter.column.datatype);
        Service.displayData.filter.operation = Service.displayData.filterOperations[0];
      },

      /**
       * Add new filter into the filters table.
       * Fires on 'ADD FILTER' click
       */
      addFilter: function () {
        Service.displayData.filters.push({
          column: Service.displayData.filter.column,
          operation: Service.displayData.filter.operation,
          constraint: Service.displayData.filter.constraint
        });
      },

      /**
       * Load data for sample table
       * Fires on 'REFRESH DATA' click
       */
      loadSampleData: function () {
        return Service.request('sampleData', {
          report: Service.revert()
        })
        .then(function (response) {
          Service.transformers = {};
          Service.displayData.columns.forEach(function(item) {
            if (item.datatype === 'date') {
              Service.transformers[item.id] = {
                filter: 'date'
              };
            }
          });
          Service.hashData(response.rows, 'sampleData');
          Service.formatData('sampleData');
        });
      },

      /**
       * Remove a filter by its index
       * @param {Number} index - Filter row index.
       * Fires on 'Delete' button in filters table
       */
      deleteFilter: function (index) {
        Service.displayData.filters.splice(index, 1);
      },

      /**
       * For sample data columns sorting
       * Fires on sort header icons of sample data table
       */
      sortColumn: function (node) {
        if (!node.sort) {
          node.sort = 1; // asc
        } else {
          node.sort = node.sort === 1 ? -1 : 0;
        }
      },

      /**
       * Fires on deleting draggable node
       * @param {String} type - node type (column, group, etc)
       * @param {index} type - node $index
       */
      deleteDroppedNode: function (type, index) {
        Service.displayData[type].splice(index, 1);
      },

      /**
       * Get the report data by ID.
       * @param {Number} id The report ID.
       * @returns {Object} $promise.
       */
      getReport: function (id) {
        // Make request
        return Service.request('getReport', {
          id: id
        })
        .then(function () {
          // TODO
        });
      }
    };

    Service.request = AsDataService.request.bind(Service, 'AdminReportsResourceService', 'createReport');

    Service.hashData = AsDataService.hashData.bind(Service);
    Service.formatData = AsDataService.formatData.bind(Service);
    Service.transformData = AsDataService.transformData.bind(Service);

    return Service;
  }
]);
