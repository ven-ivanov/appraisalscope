'use strict';

angular.module('frontendApp').directive('asReportFilters', [
  function () {
    return {
      templateUrl: '/components/admin/reports/list-reports/create-report/directives/partials/report-filters.html',
      controller: 'ReportFiltersCtrl',
      controllerAs: 'filtersCtrl'
    };
  }
]);
