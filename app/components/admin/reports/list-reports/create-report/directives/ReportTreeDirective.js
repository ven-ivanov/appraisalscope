'use strict';

angular.module('frontendApp').directive('asReportTree', [
  function () {
    return {
      templateUrl: '/components/admin/reports/list-reports/create-report/directives/partials/report-tree.html',
      controller: 'ReportTreeCtrl',
      controllerAs: 'treeCtrl'
    };
  }
]);
