'use strict';

/**
 * Directive for Create New Report.
 * <admin-reports-list-reports-create-new-report>
 */
angular.module('frontendApp').directive('adminReportsListReportsCreateReport', [function () {
  return {
    templateUrl: '/components/admin/reports/list-reports/create-report/directives/partials/create-report.html',
    controller: 'AdminReportsListReportsCreateReportCtrl',
    controllerAs: 'createReportCtrl'
  };
}]);
