'use strict';

angular.module('frontendApp').directive('asReportDataTable', [
  function () {
    return {
      templateUrl: '/components/admin/reports/list-reports/create-report/directives/partials/report-table.html',
      controller: 'ReportDataTableCtrl',
      controllerAs: 'reportTableCtrl'
    };
  }
]);
