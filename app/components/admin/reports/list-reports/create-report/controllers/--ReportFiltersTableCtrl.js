'use strict';

angular.module('frontendApp').controller('ReportFiltersTableCtrl', [
  '$scope',
  'AdminReportsCreateReportService',
  function ($scope, AdminReportsCreateReportService) {
    var vm = this;
    vm.filtersTableData = [];

    /**
     * Handle the click event on the 'Delete Filter' link label.
     * @param id: The ID of filter row.
     */
    vm.removeFilter = function (rowIndex) {
      // Only accept valid index
      if (rowIndex >= 0) {
        AdminReportsCreateReportService.removeFilterAt(rowIndex);
      }
    };

    /** $SCOPE WATCHING **/

    /**
     * Watch the AdminReportsCreateReportService.filtersTableData,
     * then update the filters table.
     */
    $scope.$watchCollection(function () {
      return AdminReportsCreateReportService.filtersTableData;
    }, function (newVal) {
      vm.filtersTableData = newVal;
    });
  }
]);
