'use strict';

angular.module('frontendApp').controller('ReportFiltersCtrl',
['AdminReportsCreateReportService', function (AdminReportsCreateReportService) {
  var vm = this;

  var Service = AdminReportsCreateReportService;

  // AsDataService defaults
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;

  // ng-click for Add filter button
  vm.addFilter = Service.addFilter;
  // ng-click for remove filter button
  vm.deleteFilter = Service.deleteFilter;
  // on filter column change
  vm.updateOperations = Service.updateOperations;

  /**
   * Add dynamic value for the condition input.
   * @param date {boolean} If true - add dynamic date, otherwise - add data quote.
   */
  vm.addDynamic = function (date) {
    Service.displayData.filter.constraint = date ? '@@DYNAMIC_DATE@@' : '@@CURRENT_DATA@@';
  };

}]);
