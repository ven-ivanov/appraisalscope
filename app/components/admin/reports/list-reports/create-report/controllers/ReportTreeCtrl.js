'use strict';

angular.module('frontendApp').controller('ReportTreeCtrl', [
  '$scope', 'AdminReportsCreateReportService',

  /**
   * Represents the controller for the Report Columns Tree
   */
  function ($scope, AdminReportsCreateReportService) {
    var vm = this;

    var Service = AdminReportsCreateReportService;

    // AsDataService defaults
    vm.safeData = Service.safeData;
    vm.displayData = Service.displayData;

    // For tree columns search
    vm.columnsSearch = '';

    /**
     * Watch for the selected report type to load the columns.
     */
    $scope.$watch(function () {
      return Service.displayData.reportType;
    }, function (newVal, oldVal) {
      if (angular.equals(oldVal, newVal)) {
        return;
      }
      Service.onReportTypeChange()
      .then();
    });

  }
]);
