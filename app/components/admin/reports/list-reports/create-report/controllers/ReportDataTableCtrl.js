'use strict';

angular.module('frontendApp').controller('ReportDataTableCtrl',
['$scope', 'AdminReportsCreateReportService', function ($scope, AdminReportsCreateReportService) {
  var vm = this;

  // TODO
  $scope.$broadcast('show-modal', 'duplicate-constructor-field');

  var Service = AdminReportsCreateReportService;

  // AsDataService defaults
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;

  // For Sortable attribute
  vm.sortableOptions = Service.sortableOptions();
  vm.sortColumn = Service.sortColumn;

  // Fires on X button click at filter drag and drop columns
  vm.deleteDroppedNode = Service.deleteDroppedNode;

}]);
