'use strict';

angular.module('frontendApp').controller('AdminReportsListReportsCreateReportCtrl', [
  'AdminReportsCreateReportService',
  function (AdminReportsCreateReportService) {
    var vm = this;

    var Service = AdminReportsCreateReportService;

    // AsDataService defaults
    vm.safeData = Service.safeData;
    vm.displayData = Service.displayData;

    vm.selects = Service.selects; // fixed lists

    // TODO temporary checks
    vm.revert = function() {
      console.log('/////////////'); // jshint ignore:line
      console.log(JSON.stringify(Service.revert(), null, 4)); // jshint ignore:line
    };
    vm.sampleData = function() {
      if (Service.displayData.columns.length === 1) {
        alert('Pick some columns 1st!'); // jshint ignore:line
      }
      Service.loadSampleData();
    };

    /**
     * Load report types on init
     */
    vm.init = function () {
      Service.initBaseState()
      .then();
    };
    vm.init();

  }
]);
