/**
 * Created by Peter on July 8th, 2015.
 */
'use strict';

/**
 * The main controller for the Admin Reports > Generate Report.
 */
angular.module('frontendApp').controller('AdminReportsGenerateReportCtrl', [
  '$scope', '$stateParams', 'AdminReportsService', 'AdminReportsGenerateReportService',
  function ($scope, $stateParams, AdminReportsService, AdminReportsGenerateReportService) {
    var vm = this;
    var Service = AdminReportsGenerateReportService;
    var generateReportSection = 'generate-report';
    //Display data
    vm.displayData = Service.displayData;
    // Safe data
    vm.safeData = Service.safeData;
    // Set heading for the admin-reports-generate-report-table
    vm.heading = [
      {
        label: 'Col1',
        data: 'col_2437'
      },
      {
        label: 'Col1',
        data: 'col_2430'
      },
      {
        label: 'Col1',
        data: 'col_2442'
      },
      {
        label: 'Col1',
        data: 'col_2444'
      },
      {
        label: 'Col1',
        data: 'col_2432'
      },
      {
        label: 'Col1',
        data: 'col_2443'
      },
      {
        label: 'Col1',
        data: 'col_2445'
      },
      {
        label: 'Col1',
        data: 'col_2446'
      },
      {
        label: 'Col1',
        data: 'col_2447'
      },
      {
        label: 'Col1',
        data: 'col_2451'
      },
      {
        label: 'Col1',
        data: 'col_2450'
      },
      {
        label: 'Col1',
        data: 'col_2961'
      },
      {
        label: 'Col1',
        data: 'col_2449'
      }
    ];

    /**
     * The old code (decorator) uses as-grid for displaying data
     * and we can bind the grid.options using the sg-state directive.
     * The gridState is needed as a parameter for making requests
     */
    vm.gridState = {
      columns: [
        {
          label: 'Col1',
          data: 'col_2437'
        },
        {
          label: 'Col1',
          data: 'col_2430'
        },
        {
          label: 'Col1',
          data: 'col_2442'
        },
        {
          label: 'Col1',
          data: 'col_2444'
        },
        {
          label: 'Col1',
          data: 'col_2432'
        },
        {
          label: 'Col1',
          data: 'col_2443'
        },
        {
          label: 'Col1',
          data: 'col_2445'
        },
        {
          label: 'Col1',
          data: 'col_2446'
        },
        {
          label: 'Col1',
          data: 'col_2447'
        },
        {
          label: 'Col1',
          data: 'col_2451'
        },
        {
          label: 'Col1',
          data: 'col_2450'
        },
        {
          label: 'Col1',
          data: 'col_2961'
        },
        {
          label: 'Col1',
          data: 'col_2449'
        }
      ],
      sorters: [
        {
          name: 'name',
          predicate: 'name',
          reverse: false
        }
      ],
      groupers: [],
      filters: {},
      limitRows: false,
      autoIncreaseFirstColumn: true
    };

    /**
     * Initiate data and state
     */
    vm.init = function () {
      Service.tableHeading = vm.heading;
      // set ui-state on load from Service then compile the Directive for it
      Service.setStateOnLoad(generateReportSection, $stateParams, vm)
      .then(function () {
        Service.generateReports(vm.gridState);
        // Keep reference to which section we're on
        AdminReportsService.section = generateReportSection;
      });
    };

    // Init controller load
    vm.init();
  }
]);
