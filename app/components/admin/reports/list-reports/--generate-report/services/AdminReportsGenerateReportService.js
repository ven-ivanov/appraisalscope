﻿/**
 * Created by Peter on July 3rd, 2015.
 */
'use strict';

angular.module('frontendApp').factory('AdminReportsGenerateReportsService',
['AdminReportsService', 'AdminReportsResourceService', function (AdminReportsService, AdminReportsResourceService) {
  var Service = {
    safeData: {
      'generate-report': {
        data: {}
      }
    }, // Report data that was display on the table
    displayData: {
      'generate-report': {
        data: {}
      }
    },

    // Data that'll be displayed on the admin-reports-generate-report-table
    tableData: [],

    // admin-reports-generate-report-table heading
    tableHeading: [],

    /**
     * Request wrapper
     */
    request: function (requestFn, params) {
      /** @TODO: create another service AdminReportsResourceService
       * that bind the $resource to make request to the API.
       */

      // Get $resource configuration
      return AdminReportsResourceService.listReports[requestFn](params);
    },

    generateReports: function (gridState) {
      var self = this;

      if (!gridState) {
        throw Error('Grid-State must be passed into generateReports!');
      }

      return Service.request('generateReport', gridState)
      .then(function (response) {
        var data = response.data;

        if (data && angular.isArray(data)) {
          // @TODO: bind all reports data to the table
          AdminReportsService.safeData['generate-report'].data = data;

          self.displayData['generate-report'].data = data;
        }
      });
    }
  };

  /**
   * Set state onload to get the $stateParams to build the directive for the state
   */
  Service.setStateOnLoad = AdminReportsService.setStateOnLoad.bind(Service, 'generate-report');

  return Service;

}]);
