﻿/**
 * Created by Peter on July 8th, 2015.
 */
'use strict';
angular.module('frontendApp').directive('adminReportsGenerateReport', [
  '$compile', 'DirectiveInheritanceService',
  function ($compile, DirectiveInheritanceService) {
    return {
      templateUrl: '/components/admin/reports/generate-report/directives/partials/generate-report.html',
      controller: 'AdminReportsGenerateReportCtrl',
      controllerAs: 'generateReportsCtrl',
      scope: {
        disabled: '@'
      },
      compile: function () {
        return {
          pre: function (scope) {
            // Expose trigger download
            DirectiveInheritanceService.inheritDirective.call(scope);
            // Whether displaying disabled appraisers
            scope.disabled = scope.$eval(scope.disabled);
          }
        };
      }
    };
  }
]);