/**
 * Created by Peter on July 06th, 2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Generate View directive will be compiled to admin-reports-list-reports-generate-view
 * in the adminReportsListReports directive
 */
app.directive('adminReportsListReportsGenerateView', [function () {
  return {
    templateUrl: '/components/admin/reports/list-reports/generate-view/directives/partials/generate-view.html',
    controller: 'AdminReportsListReportsGenerateViewCtrl',
    controllerAs: 'tableCtrl'
  };
}]);
