'use strict';

/**
 * Represents a controller for the Admin Reports > List Reports - Generate View modal
 **/

angular.module('frontendApp').controller('AdminReportsListReportsGenerateViewCtrl', [
  '$scope', 'AdminReportsListReportsService', 'AdminReportsResourceService',
  function ($scope, AdminReportsListReportsService, AdminReportsResourceService) {
    var vm = this;
    var reportExcelName = 'Report'; //set default name excel report is Report
    /**
     * Endpoint mocks required gridState
     * @type {{limitRows: number}}: number row which generate view table show
     */
    var gridState = {
      limitRows: 10
    };

    /**
     * Table column headers and data column
     */
    vm.tableColumnHeaders = null;
    vm.tableGenerateViewData = null;

    /**
     * Watch for the AdminReportsListReportsService.selectedReports changed
     * then make a request to get all data generate view,
     * and populate the data onto the table.
     */
    $scope.$watch(function () {
      return AdminReportsListReportsService.selectedReports[0];
    }, function (newVal) {
      if (angular.isDefined(newVal) && newVal > 0) {
        var paramObj = {
          id: newVal, //newVal is selectedReports get from AdminReportsListReportsService
          gridState: JSON.stringify(gridState)
        };
        /**
         * Get data from getReport function in AdminReportsResourceService service
         * Set data into tableColumnHeaders
         */
        AdminReportsResourceService.getReport(paramObj)
        .then(function (response) {
          var data = response.data;
          if (angular.isArray(data.columns)) {
            reportExcelName = data.name; //set name excel report
            vm.tableColumnHeaders = data.columns; // data.columns is name header column
            return data.columns;
          }
        })
        /**
         * After finished set data into tableColumnHeaders,
         * get data from generateReport function in AdminReportsResourceService service
         */
          .then(function (results) {
            if (angular.isArray(results)) {
              AdminReportsResourceService.listReports.generateReport(paramObj)
              .then(function (response) {
                var data = response.data;
                if (angular.isArray(data)) {
                  vm.tableGenerateViewData = data;
                }
              });
            }
          });
      }
    });

    /**
     * Load data row of tableGenerateViewData follow name get from tableColumnHeaders
     * @param item: data row of tableGenerateViewData
     * @param name: name column of tableColumnHeaders
     * @returns {*}
     */
    vm.getValueByName = function (item, name) {
      /**
       * If item contains name, get value item by name.
       * Otherwise, set value item is null
       */
      return item[name] ? item[name] : '';
    };

    /**
     * Export data to excel.
     * Using FileSaver.js library export data table to excel file.
     */
    vm.exportExcel = function () {
      // exportable is a id table of generate-view.html
      var blob = new Blob([document.getElementById('exportable').innerHTML], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8'
      });
      // saveAs is a function of FileSaver.js
      saveAs(blob, reportExcelName + '.xls');// reportExcelName is name of excel file
    };
  }]);
