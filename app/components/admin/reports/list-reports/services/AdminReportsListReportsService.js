/**
 * Created by Simon on June 24th, 2015.
 */
'use strict';

angular.module('frontendApp').factory('AdminReportsListReportsService',
['API_PREFIX', 'AdminReportsService', 'AdminReportsResourceService',
 function (API_PREFIX, AdminReportsService, AdminReportsResourceService) {
   var Service = {
     // Hold all selected report-rows' ID.
     selectedReports: [], // The selected report-row's ID.
     selectedReportId: 0,
     editMode: false, // Display disabled
     isDisabled: false,
     safeData: {}, // Report data that was display on the table
     displayData: {
       'list-reports': {
         data: {},
         pagination: {}
       },
       'generate-view': {
         heading: [],
         data: {}
       }
     },

     /**
      * Request wrapper
      */
     request: function (requestFn, params) {
       /** @TODO: create another service AdminReportsResourceService
        * that bind the $resource to make request to the API.
        */

       // Get $resource configuration
       return AdminReportsResourceService.listReports[requestFn](params);
     },

     /**
      * Send a request for Admin Reports data.
      * @param   {Object}   gridState The configuration object for pagination, sort, group that the old code has used.
      * @returns {Object} Admin Reports promise.
      */
     getAdminReportList: function () {
       var gridStateObj = AdminReportsService.getGridState('list-reports');

       return Service.request('getAdminReportList', gridStateObj).then(function (response) {
         var data = response.data, pagination = response.meta.pagination;

         if (data && angular.isArray(data)) {

           Service.displayData['list-reports'].data = data;
           Service.displayData['list-reports'].pagination = pagination;
         }
       });
     },

     /**
      * Update backend on change
      */
     updatePermissions: function (type, update) {
       // Update permissions
       var permissions = new (Service.permissionsRequest())(update);
       permissions.permissionType = type + '-permissions';
       return permissions.$updatePermissions();
     },

     /**
      * Create a request to copy the selected report.
      * Replace this name object report and add it into data table report.
      */
     copyRowReport: function () {
       var params = {
         id: Service.selectedReports[0]
       };
       AdminReportsResourceService.copyReport(params).then(function (response) {
         var objReport = response.data;
         if (angular.isDefined(objReport)) {
           // Replace name report as format "Copy - Name report"
           objReport.data2 = 'Copy - ' + objReport.data2;
           // Add report copied into list data report
           Service.displayData['list-reports'].data.push(objReport);
           // After copy, set selectedReports is null
           Service.selectedReports.splice(0, Service.selectedReports.length);
         }
       });
     },

     /**
      * Create a request to delete the selected report(s).
      * Mock-up return list ids
      * Remove all row report by list mock-up ids
      */
     deleteRowReport: function () {
       var params = {
         id: Service.selectedReports
       };
       AdminReportsResourceService.deleteReport(params).then(function (response) {
         var arrayIDs = response.data;
         if (angular.isArray(arrayIDs)) {
           var countTemp = 0;
           angular.forEach(arrayIDs, function (id) {
             countTemp++;
             var itemRemoved = _.findWhere(Service.displayData['list-reports'].data, {id: id});
             Service.displayData['list-reports'].data =
             _.without(Service.displayData['list-reports'].data, itemRemoved);
           });
           /**
            *  After remove all row report by list mock-up ids,
            *  set selectedReports is null
            */
           if (countTemp === arrayIDs.length) {
             Service.selectedReports.splice(0, Service.selectedReports.length);
           }
         }
       });
     },

     /**
      * Generate report view
      */
     generateView: function (id) {

       var requestParams = {
         id: id
       };

       return AdminReportsResourceService.getReport(requestParams).then(function (response) {
         var data = response.data;

         if (data && angular.isArray(data)) {
           // @TODO: bind all reports data to the table
           AdminReportsService.safeData['generate-view'].data = data;

           Service.displayData['generate-view'].data = data;
         }

         return data;
       });
     }
   };

   // @TODO: bind the service with the parent service Service.doXXX =
   // AdminReportsService.doYYY.bind(this, params, module);

   /**
    * Set state onload to get the $stateParams to build the directive for the state
    */
   Service.setStateOnLoad = AdminReportsService.setStateOnLoad.bind(Service, 'list-reports');
   return Service;
 }]);
