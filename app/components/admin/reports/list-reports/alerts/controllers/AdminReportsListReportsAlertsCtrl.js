'use strict';

angular.module('frontendApp').controller('AdminReportsListReportsAlertsCtrl', [
  '$scope', '$stateParams', 'AdminReportsAlertsService', 'AdminReportsService', 'AdminReportsResourceService',
  function ($scope, $stateParams, AdminReportsAlertsService, AdminReportsService, AdminReportsResourceService) {
    var vm = this;
    var limitHour = 12;
    var limitDay = 31;
    var limitNumber = 10;

    /**
     *  @HACK: load the report alert data using 328 as the report id
     *  @TODO: get the report id from the service
     */
    var id = 328;

    vm.chosenAlert = 0;
    vm.alertName = null;
    vm.alertDate = null;
    vm.monthDate = null;
    vm.cmbHour = null;
    vm.cmbMeridian = 'AM';
    vm.emailType = null;
    var Service = AdminReportsResourceService;

    vm.selectedAlert = {
      id: null,
      name: '',
      generatedOn: null,
      generatedOnDayOfMonth: [], // 1~31
      // In America, Sunday is sometimes considered to be the first day of the week
      generatedOnDayOfWeek: [
        {name: 'Monday', value: false},
        {name: 'Tuesday', value: false},
        {name: 'Wednesday', value: false},
        {name: 'Thursday', value: false},
        {name: 'Friday', value: false},
        {name: 'Saturday', value: false},
        {name: 'Sunday', value: false}
      ],
      generatedOnTime: [], // 1-12 AM|PM
      typeOfTime: ['AM', 'PM'] //AM|PM
    };

    //loop for to have from 01 to 31 and set into generatedOnDayOfMonth
    for (var i = 1; i <= limitDay; i++) {
      vm.selectedAlert.generatedOnDayOfMonth.push(i < limitNumber ? '0' + i : i.toString());
    }

    //loop for to have from 01 to 12 and set into generatedOnTime
    for (var j = 1; j <= limitHour; j++) {
      vm.selectedAlert.generatedOnTime.push(j < limitNumber ? '0' + j : j.toString());
    }

    /**
     * Load report alert data after alert selected
     * Binding all of them into select box and check box
     */
    vm.changeAlert = function () {
      var requestParams = {
        id: id,
        alertId: vm.chosenAlert.id
      };
      if (!id) {
        throw Error('A report must be selected/checked first!');
      }
      Service.listReports.getReportAlerts(requestParams)
        .then(function (response) {
          var objAlert = response.data;
          vm.alertDate = objAlert.alertDate;
          vm.monthDate = objAlert.monthDate;
          /**
           * Loop objAlert.weekdays to get true or false value, set day selection.
           * @param entry: true or false value.
           * @param index: position in array objAlert.weekdays.
           */
          objAlert.weekdays.forEach(function (entry, index) {
            vm.selectedAlert.generatedOnDayOfWeek[index].value = entry;
          });
          vm.cmbHour = objAlert.cmbHour;
          vm.cmbMeridian = objAlert.cmbMeridian;
          vm.emailType = objAlert.emailType;
        });
    };

    /**
     * If there is no select alert from the Select Existing Alert ComboBox/Dropdown
     * The Delete Selected Alert button should be disabled.
     */
    vm.canDeleteSelectedAlert = false;
    vm.sendReportEmails = [];

    /**
     * The alert that was selected from the id report
     * Select Existing Alert ComboBox/Dropdown
     * @param: id : the selected report-id
     * @TODO: get the selected id from the service
     */
    function loadAlert(id) {
      var requestParams = {
        id: id
      };
      if (!id) {
        throw Error('A report must be selected/checked first!');
      }
      Service.listReports.getReportAlerts(requestParams)
        .then(function (response) {
          vm.selectAlert = response.data;
        });
    }

    /**
     *  @HACK: load the report alert data using 328 as the report id
     *  Load all alert item of report id
     */
    loadAlert(id);

    /**
     * The Alert Timings table's heading.
     */
    vm.heading = [
      {
        label: 'Sr#',
        data: 'id'
      },
      {
        label: 'Alert At',
        data: 'alertAt'
      },
      {
        label: ' ',
        noSort: true,
        linkLabel: 'Delete',
        fn: '$index'
      }
    ];
    /**
     * @TODO: Add alert timings for the current selected Alert.
     * Binding data into table alert timings after selected from selectbox
     */
    vm.addAlertTimings = function () {
      //get data from selectbox and checkbox
      var generateOn = vm.alertDate;
      var generateMonth = vm.monthDate;
      var hour = vm.cmbHour;
      var typeHour = vm.cmbMeridian;
      var generateEvery = vm.selectedAlert.generatedOnDayOfWeek;

      // Show data as format version one styles
      var rowReportOn = 'Generate Report on ' + generateOn + ', ' + hour + ':00 ' + typeHour;
      var rowEachMonth = generateMonth + ' of Each Month, ' + hour + ':00 ' + typeHour;
      var rowDayOfWeek = [];

      // Loop to get checkbox is checked
      generateEvery.forEach(function (entry) {
        if (entry.value) {
          rowDayOfWeek.push({alertAt: entry.name + ' of a Week, ' + hour + ':00 ' + typeHour});
        }
      });

      // Don't add into alertItems if this field equal null
      var alertItems = [];
      if (generateOn) {
        alertItems.push({alertAt: rowReportOn});
      }
      if (generateMonth) {
        alertItems.push({alertAt: rowEachMonth});
      }
      // Using lodash to merge two array
      var mergeAlertItems = _.union(alertItems, rowDayOfWeek);
      AdminReportsAlertsService.addAlertTimings(mergeAlertItems);
    };

    /**
     * Handle the click event on the 'Delete' link label.
     * @param id: The ID of alert row.
     */
    vm.linkFn = function (id) {
      if (id) {
        //get item need to remove
        var itemRemoved = _.findWhere(AdminReportsAlertsService.alertTableData, {id: id});
        AdminReportsAlertsService.alertTableData = _.without(AdminReportsAlertsService.alertTableData, itemRemoved);
      }
    };

    /**
     * Watch the AdminReportsAlertsService.alertTableData,
     * then update the alert table.
     */
    $scope.$watchCollection(function () {
      return AdminReportsAlertsService.alertTableData;
    }, function (newVal) {
      vm.tableData = newVal;
      vm.rowData = vm.tableData.slice();
    });

    /**
     * @TODO: save the vm.sendReportEmails to DB
     */
    vm.saveReportEmails = function () {
      if (vm.sendReportEmails.length < 1) {
        return;
      }

      // @TODO create an API request with vm.sendReportEmails as parameter
    };

    /**
     * @TODO: Delete the alert that was selected from the
     * Select Existing Alert ComboBox/Dropdown.
     */
    vm.deleteSelectedAlert = function () {
      if (!vm.selectedAlert) {
        return;
      }

      // @TODO: create an API request with vm.seletedAlert.id as parameter
    };

    /**
     * Add new alert and binding into alert selectbox
     */
    vm.addNewAlert = function () {
      // @TODO: create API request to add new alert after binding it into alert selectbox
    };
  }
]);