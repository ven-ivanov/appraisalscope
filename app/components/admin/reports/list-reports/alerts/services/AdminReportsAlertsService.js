/**
 * Created by cssdna on 07/16/15.
 */
'use strict';

angular.module('frontendApp').factory('AdminReportsAlertsService', [
  function () {
    var Service = {
      alertTableData: [],
      /**
       * Add data into alertTableData
       * Before add data into table alert timings, check is alertTableData existed
       * If alertTableData exists, set it to an empty array. Then loop through the data and push it.
       * @param data: array values which selected from alert.html
       */
      addAlertTimings: function (data) {
        var self = this;
        if(self.alertTableData){
          self.alertTableData = [];
        }
        var alertData = [];
        // Loop data to add Id equal i, using this Id to remove row of alert table timings
        data.forEach(function (entry, i) {
          alertData.push({id:i + 1, alertAt:entry.alertAt});
        });
        self.alertTableData = alertData;
      }
    };
    return Service;
  }
]);
