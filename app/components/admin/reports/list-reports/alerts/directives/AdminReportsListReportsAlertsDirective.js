'use strict';

angular.module('frontendApp').directive('adminReportsListReportsAlerts', [
  function () {
    return {
      templateUrl: '/components/admin/reports/list-reports/alerts/directives/partials/alerts.html',
      controller: 'AdminReportsListReportsAlertsCtrl',
      controllerAs: 'alertsCtrl'
    };
   // console.log(alertsCtrl)
  }
]);
