'use strict';

/**
 * Represents the controller of the Admin Reports section
 */
angular.module('frontendApp').controller('AdminReportsCtrl', [
  '$scope', '$stateParams', '$state',
  'AdminReportsService',
  function ($scope, $stateParams, $state, AdminReportsService) {
    var vm = this;

    if ($stateParams.activeTab === '') {
      $state.go('main.reports', {
        activeTab: 'list-reports'
      });

      return;
    }

    var directives = {
      'list-reports': 'admin-reports-list-reports',
      statistics: 'admin-reports-statistics',
      'create-report': 'admin-reports-list-reports-create-report',
      'edit-report': 'admin-reports-list-reports-create-report',
      'generate-report': 'admin-reports-list-reports-generate-report',
      'generate-view': 'admin-reports-list-reports-generate-view',
      alerts: 'admin-reports-list-reports-alerts',
      'client-permission': 'admin-reports-list-reports-client-permission',
      'staff-permission': 'admin-reports-list-reports-staff-permission'
    };

    /**
     * Set active tab from the $stateParams
     * /reports/:activeTab?list-reports&statistics&create-new&generate-view
     */
    AdminReportsService.activeTab = vm.activeTab = $scope.activeTab = $stateParams.activeTab;

    // Set page
    AdminReportsService.page = vm.page = $scope.page = $stateParams.page ? parseInt($stateParams.page) : null;

    // Set directive to compile from the active-tab
    $scope.directive = directives[$scope.activeTab];
  }
]);
