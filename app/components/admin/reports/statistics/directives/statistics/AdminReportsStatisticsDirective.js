﻿/**
 * Created by Peter on July 3rd, 2015.
 */
'use strict';
angular.module('frontendApp').directive('adminReportsStatistics', [
  '$compile', 'DirectiveInheritanceService',
  function ($compile, DirectiveInheritanceService) {
    return {
      templateUrl: '/components/admin/reports/statistics/directives/partials/statistics.html',
      controller: 'AdminReportsStatisticsCtrl',
      controllerAs: 'statisticsCtrl',
      scope: {
        disabled: '@'
      },
      compile: function () {
        return {
          pre: function (scope) {
            // Expose trigger download
            DirectiveInheritanceService.inheritDirective.call(scope);
            // Whether displaying disabled appraisers
            scope.disabled = scope.$eval(scope.disabled);
          }
        };
      }
    };
  }
]);