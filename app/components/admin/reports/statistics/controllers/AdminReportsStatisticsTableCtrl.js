/**
 * Created by Peter on July 3rd, 2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * The controller for the Admin Reports > Statistics table.
 */
app.controller('AdminReportsStatisticsTableCtrl', [
  '$scope',
  'AdminReportsStatisticsService', 'AdminReportsTableCtrlInheritanceService',
  function ($scope, AdminReportsStatisticsService, AdminReportsTableCtrlInheritanceService) {
    var vm = this;
    // Set heading for the admin-reports-statistics-table
    var heading = AdminReportsStatisticsService.tableHeading = [
      {
        label: 'Content',
        data: 'data1',
      },
      {
        label: 'Value',
        data: 'data2',
      }
    ];

    var gridState = {
      autoIncreaseFirstColumn: true,
      columns: [
        {
          filter: false,
          name: 'data1',
          nameAs: 'Content'
        },
        {
          filter: false,
          name: 'data2',
          nameAs: 'Value'
        }
      ],
      filters: {},
      groupers: [
        {
          name: 'data1',
          nameAs: 'Content',
          reverse: false
        }
      ],
      limitRows: false,
      //      pagination: {},
      sorters: [
        {
          name: 'data1',
          nameAs: 'Content',
          predicate: 'data1',
          reverse: false
        }
      ]
    };

    // Inherit table controller methods
    AdminReportsTableCtrlInheritanceService.inherit.call(vm,
      $scope,
      AdminReportsStatisticsService,
      heading,
      'statistics',
      gridState);
  }
]);