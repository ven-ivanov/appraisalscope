﻿/**
 * Created by Peter on July 3rd, 2015.
 */
'use strict';

/**
 * The main controller for the Admin Reports > Statistics view.
 */
angular.module('frontendApp').controller('AdminReportsStatisticsCtrl', [
  '$scope', '$stateParams', 'AdminReportsService', 'AdminReportsStatisticsService',
  function ($scope, $stateParams, AdminReportsService, AdminReportsStatisticsService) {
    var vm = this;
    var Service = AdminReportsStatisticsService;
    //Display data
    vm.displayData = Service.displayData;
    // Safe data
    vm.safeData = Service.safeData;

    /**
     * Initiate data and state
     */
    vm.init = function () {
      // set ui-state on load from Service then compile the Directive for it
      //AdminReportsStatisticsService.setStateOnLoad('statistics', $stateParams, vm).then(function () {
      //  // Compile the directive from AdminReportsListReportsDirective
      //  $scope.compileDirectives();
      //});
      console.log('statisitics');
      $scope.statistics = [
      {
        content: 'Average Turn Time - (hour)',
        value: '48'
      },
      {
        content: 'Average Acceptance Time - (hour)',
        value: '419.5'
      },
      {
        content: 'Average days in QC',
        value: '7'
      },
      {
        content: 'Average Margin of Completed Orders - (%)',
        value: '80'
      },
      ];
    };

    // Keep reference to which section we're on
    AdminReportsService.section = 'statistics';

    // Init controller load
    vm.init();
  }
]);