﻿/**
 * Created by Peter on July 3rd, 2015.
 */
'use strict';

angular.module('frontendApp').factory('AdminReportsStatisticsService',
  ['AdminReportsService', 'AdminReportsResourceService',
  function (AdminReportsService, AdminReportsResourceService) {
    var Service = {
      safeData: {},
      // Report data that was display on the table
      displayData: {
        'statistics': {
          data: {},
        }
      },

      // Data that'll be displayed on the admin-reports-statistics-table
      tableData: [],

      // admin-reports-statistics-table heading
      tableHeading: [],

      /**
       * Request wrapper
       */
      request: function (requestFn, params) {
        /** @TODO: create another service AdminReportsResourceService
         * that bind the $resource to make request to the API.
         */

        // Get $resource configuration
        return AdminReportsResourceService.listReports[requestFn](params);
      },
    };

    // @TODO: bind the service with the parent service Service.doXXX =
    // AdminReportsService.doYYY.bind(this, params, module);

    /**
     * Set state onload to get the $stateParams to build the directive for the state
     */
    Service.setStateOnLoad = AdminReportsService.setStateOnLoad.bind(Service, 'statistics');

    return Service;
  }
  ]);