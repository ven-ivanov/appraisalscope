'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminSettingsFileCtrl
 * @description
 * @extends RecordNumberPrefixService
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AdminSettingsFileCtrl', [
    '$scope', 'AdminSettingsService', 'RecordNumberPrefixService', 'Utils',

    function ($scope, AdminSettingsService, RecordNumberPrefixService, Utils) {

      // Creating a new recordNumberPrefix using the file endpoint
      var file = new RecordNumberPrefixService({'apiResource' : AdminSettingsService.file});

      // Decorating $scope with file properties and methods
      Utils.decorate($scope, file);
    }
  ]);
