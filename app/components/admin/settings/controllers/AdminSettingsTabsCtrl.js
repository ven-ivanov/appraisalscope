'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminSettingsTabsCtrl
 * @description
 * # AdminSettingsTabsCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AdminSettingsTabsCtrl', ['$scope', '$location', function ($scope, $location) {

    $scope.tabs = [
      {link: 'main.settings.companyInformation', label: 'Company Information'},
      {link: 'main.settings.notification', label: 'Notification'},
      {link: 'main.settings.file', label: 'File'},
      {link: 'main.settings.invoice', label: 'Invoice'},
      {link: 'main.settings.reminders', label: 'Reminders'},
      {link: 'main.settings.options', label: 'Options'},
      {link: 'main.settings.instructions', label: 'Instructions'},
      {link: 'main.settings.email', label: 'Emails'},
      {link: 'main.settings.amcLicense', label: 'AMC License`s'},
      {link: 'main.settings.ucdp', label: 'UCDP'},
      {link: 'main.settings.predefinedMessages', label: 'Predefined Messages'},
      {link: 'main.settings.documentUpload', label: 'Document Upload'},
      {link: 'main.settings.importUser', label: 'Import User'},
      {link: 'main.settings.importClient', label: 'Import Client'},
      {link: 'main.settings.qcReview', label: 'QC Review'},
    ];

    // Set selected tab on initial load
    var label = $location.url().split('/')[4];

    $scope.tabs.forEach(function (element, index) {
      if (label === element.label.toLowerCase().replace(' ', '-')) {
        $scope.selectedTab = $scope.tabs[index];
      }
    });

    $scope.setSelectedTab = function (tab) {
      $scope.selectedTab = tab;
    };

    $scope.tabClass = function (tab) {
      if ($scope.selectedTab === tab) {
        return 'active';
      } else {
        return '';
      }
    };

    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  }]);

