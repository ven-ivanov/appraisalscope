'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminSettingsNotificationTableCtrl
 * @description
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AdminSettingsNotificationTableCtrl', [
  	'AdminSettingsNotificationService',
    '$scope',

    function (AdminSettingsNotificationService, $scope) { 
     
      var vm = this;
      var mainCtrl = $scope.notificationCtrl;
      
      //as-table data
      AdminSettingsNotificationService.getAll().then(function (res) {
        vm.tableData = res;
      });

      //as-table headings
      vm.heading = [
        {
          'label': 'Notification Title',
          'data' : 'label'
        },
        {
          'label': 'Notification',
          'data' : 'message'
        },
        {
        	'label': 'Set for appraiser',
        	'custom': true,
        	'template': '<input type="checkbox" class="form-control" ng-model="data.isSetForAppraiser" ng-change="linkFn(update)" checkable/>'
        },
        {
          'label': 'Edit',
          'linkLabel': 'Edit',
          'fn'   : 'edit'
        },
        {
          'label': 'Delete',
          'linkLabel': 'Delete',
          'fn' : 'delete'
        }
      ];

      /* function call for as-table */
      vm.linkFn = function (id, fnName) {
      	switch(fnName){
          case 'delete':
            vm.deleteNotification(id);
            break;

          case 'edit':
            vm.editNotification(id);
            break;

          case 'update':
          	vm.updateNotification(id);
          	break;
        }
      };

      /** 
       * Pass specified notification to main controller for editing
       * @param {Int} id
       */
      vm.editNotification = function (id) {
        var notification = AdminSettingsNotificationService.findById(id);

        /** loop is for avoiding angular links issue */
        angular.forEach(notification, function (val, key) {

          mainCtrl.notification[key] = val;
        });
      };

      /**
       * Delete specified notification
       * @param {Int} id
       */
      vm.deleteNotification = function(id) {       
        AdminSettingsNotificationService.destroy(id);
      };

      /**
       * Update specified notification
       * @param {Int} id
       */
      vm.updateNotification = function (id) {
        var notification = AdminSettingsNotificationService.findById(id);

        AdminSettingsNotificationService.update(notification);
      };
    }
  ]);