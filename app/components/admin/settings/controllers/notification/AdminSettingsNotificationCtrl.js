'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminSettingsNotificationCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AdminSettingsNotificationCtrl', [
    'AdminSettingsNotificationService',

    function (AdminSettingsNotificationService) {

      var vm = this, save;
      vm.notification = {};

      /** Save notification */
      vm.saveNotification = function () {

        /** If there is  an id then notification exists, so update it */
        if (vm.notification.id) {
          save = AdminSettingsNotificationService.update(vm.notification);
        } else {
          save = AdminSettingsNotificationService.store(vm.notification);
        }

        return save.then(function () {
          vm.resetForm();
        });
      };

      /** Reset editing form */
      vm.resetForm = function () {
        vm.notification = {};
      };
    }
  ]);