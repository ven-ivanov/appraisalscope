'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminSettingsInsctructionsCtrl
 * @description
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AdminSettingsInstructionsCtrl', [
  	'AdminSettingsInstructionsService',
    '$q',

    function (AdminSettingsInstructionsService, $q) {
    	
    	var vm = this;
    	var documents = AdminSettingsInstructionsService.adminInstructionsDocuments.getAll();      
    	var instructionsForClietns = AdminSettingsInstructionsService.adminInstructionsForClient.getAll();      

      $q.all([
        documents.$promise,
        instructionsForClietns.$promise       
      ]).then(function () {        	
        vm.Documents = documents;
        vm.instructionsForClietns = instructionsForClietns;             
      });

      /**
       * Open modal for add document
       */
      vm.openAddDocumentModal = function() {
      	vm.newDocument = {};
      	vm.setAddDocumentModal = true;
      };

      /**
       * Open modal for assign
       */
      vm.openAssignModal = function() { 
      	vm.states = AdminSettingsInstructionsService.adminStates.getAll(); 
      	vm.jobTypes = AdminSettingsInstructionsService.adminJobTypes.getAll(); 
      	vm.clientCompanies = AdminSettingsInstructionsService.adminClientCompanies.getAll(); 
    		
    		vm.checkedStates = [];
    		vm.checkedjobTypes = [];
    		vm.checkedclientCompanies = [];
    		vm.checkedDocuments = [];

      	vm.setAssignModal = true;
      };

      /**
       * Open modal for add instruction
       */
      vm.openInstructionsModal = function(row) {
      	vm.instructionsMode = 'Add';
      	vm.newInstruction = {}; 

      	if(row){
      		vm.newInstruction.id = row.id;
      		vm.newInstruction.Name = row.Name;
      		vm.newInstruction.template = row.template;
      		vm.instructionsMode = 'Edit';
      	}  

      	vm.setAddInstructionsModal = true;
      };

      /**
       * Add an instruction     
       * @param {Object} document
       * @return {Resource} response
       */
      vm.addInstruction = function () {	      	
        var success, fail;
        // query succeeded.
        success = function(){        	
	        vm.instructionsForClietns.push(vm.newInstruction);	      
	        vm.setAddInstructionsModal = false;
        };
        // query failed.
        fail = function(){ };

        // sending query.
        return AdminSettingsInstructionsService.adminInstructionsForClient.store(vm.newInstruction, success, fail).$promise;     
      };

      /**
       * Update an instruction     
       * @param {Object} document
       * @return {Resource} response
       */
      vm.updateInstruction = function () { 
        var success, fail;
        // query succeeded.
        success = function(){
	        for (var i = vm.instructionsForClietns.length - 1; i >= 0; i--) {
	        	if(vm.instructionsForClietns[i].id === vm.newInstruction.id) {
	        		vm.instructionsForClietns[i] = vm.newInstruction;
	        	}
	        }

	        vm.setAddInstructionsModal = false;
        };
        // query failed.
        fail = function(){ }; 

        return AdminSettingsInstructionsService.adminInstructionsForClient.update(vm.newInstruction, success, fail).$promise;  
      };

      /**
       * Delete an instruction
       * @param {Int} key
       * @param {Int} instructionID
       * @return {Resource} response
       */
      vm.deleteInstruction =  function (key, instructionID) {      	
      	for (var i = vm.instructionsForClietns.length - 1; i >= 0; i--) {
      		if(key === i){
      			vm.instructionsForClietns.splice(key,1);
      		}
      	}      
        return AdminSettingsInstructionsService.adminInstructionsForClient.destroy(instructionID);
      };

      /**
       * Add a document       
       * @param {Object} document
       * @return {Resource} response
       */
      vm.addDocument = function (newDocument) {
      	var success, fail;
        // query succeeded.
        success = function(response){
	        vm.Documents.push(response);
	        vm.setAddDocumentModal = false;
        };
        // query failed.
        fail = function(){ };

        // sending query.
        return AdminSettingsInstructionsService.adminInstructionsDocuments.store(newDocument, success, fail).$promise;            
      };

      /**
       * Update a document
       * @param {Int} key
       * @param {Object} document
       * @return {Resource} response
       */
      vm.updateDocument = function (key, documentData) {
        vm.editDocument[key] = false;
        return AdminSettingsInstructionsService.adminInstructionsDocuments.update(documentData);
      };

       /**
       * Delete a document
       * @param {Int} key
       * @param {Int} documentID
       * @return {Resource} response
       */
      vm.deleteDocument =  function (key, documentID) {      	
      	for (var i = vm.Documents.length - 1; i >= 0; i--) {
      		if(key === i){
      			vm.Documents.splice(key,1);
      		}
      	}     
        return AdminSettingsInstructionsService.adminInstructionsDocuments.destroy(documentID);
      };

       /**
       * Assign
       * @return {Resource} response
       */
      vm.assign =  function () {  
      	vm.setAssignModal = false;    	
      	return AdminSettingsInstructionsService.adminAssign.update().$promise;
      };      
     
    	return this;     
    }
  ]);
