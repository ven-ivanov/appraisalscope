'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminSettingsImportClientCtrl
 * @description
 * # AdminSettingsImportClientCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AdminSettingsImportClientCtrl', [
  	'AdminSettingsUploadService', '$scope',

    function (AdminSettingsUploadService, $scope) {

      var vm = this;

      vm.rules = AdminSettingsUploadService.clientImportRules();

      vm.uploadCSV = function (file) {
        // This is triggered on change, so ensure that a file is being input
        if (!file) {
          return;
        }

        // Import csv via upload service
        AdminSettingsUploadService.importClient(file).promise.then(function () {
          // Eventual response
          $scope.$broadcast('show-modal', 'import-csv-successful');
        }, function (err) { // jshint ignore:line
          vm.uploadError = err.data;
          $scope.$broadcast('show-modal', 'import-csv-failure');
        });
      };
    }
  ]);
