'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminSettingsInsctructionsCtrl
 * @description
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AdminSettingsAmcLicenseCtrl', [
  	'AdminSettingsAmcLicenseService',
    '$q',

    function (AdminSettingsAmcLicenseService, $q) { 
     
      var vm = this;
      var adminAmcLicense = AdminSettingsAmcLicenseService.adminAmcLicense.getAll();
      vm.DisplayLicense  = '0';   
      vm.DisplayExpirationDate  = '0';   
      vm.DisplayClientFeeToAppraiser  = '0';   
      vm.DisplayAppraiserFeeToClient  = '0';   
     
      $q.all([       
        adminAmcLicense.$promise
      ]).then(function () {         
        vm.adminAmcLicense = adminAmcLicense;       
      });

       /**
       * Update a row
       * @param {Int} key
       * @param {Object} document
       * @return {Resource} response
       */
      vm.updateRow = function (key, row) {
        if(typeof vm.editRow[key] !== 'undefined'){
          vm.editRow[key] = false;
        }      
       
        return AdminSettingsAmcLicenseService.adminAmcLicense.update(row);
      };



      vm.changeModel = function (row, checkBox){
        row[checkBox] = (row[checkBox] === '1') ? '0' : '1';
       
        return AdminSettingsAmcLicenseService.adminAmcLicense.update(row);
      };

      vm.changeAll = function (checkBox) {
        vm.checkBox  = (vm.checkBox === '1') ? '0' : '1';
       
        angular.forEach(vm.adminAmcLicense, function(value) {
          value[checkBox] = vm.checkBox;
        });
        var row = {
            type : checkBox,
            active: vm.checkBox
        };
        
        return AdminSettingsAmcLicenseService.adminAmcLicense.update(row);
      };

      return this;     
    }
  ]);
