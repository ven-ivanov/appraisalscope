'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminSettingsInsctructionsCtrl
 * @description
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AdminSettingsPredefinedMessagesCtrl', [
  	'AdminSettingsPredefinedMessagesService',
    '$q',

    function (AdminSettingsPredefinedMessagesService, $q) { 
     
      var vm = this;
      vm.currentMessage = {};
      
      var Messages = AdminSettingsPredefinedMessagesService.PredefinedMessages.getAll();
      var MessagesType = AdminSettingsPredefinedMessagesService.PredefinedMessagesTypes.getAll();

      $q.all([       
        Messages.$promise,
        MessagesType.$promise
      ]).then(function () { 
        vm.PredefinedMessages = Messages.predefinedMessage;            
        vm.PredefinedMessagesForRevision = Messages.PredefinedMessagesForRevision; 
        vm.MessageTypes = MessagesType;          
      });

      vm.editMessage = function (message) {
        vm.currentMessage.id = message.id;
        vm.currentMessage.Title = message.Title;
        vm.currentMessage.Message = message.Message;
 				vm.currentMessage.type = message.type;        
      };

      /**
       * Update a message
       * @return {Resource} response
       */
      vm.saveMessage = function() {
        if(vm.currentMessage.id){
          return AdminSettingsPredefinedMessagesService.PredefinedMessages.update(vm.currentMessage).$promise ;
        }
        var arr = (vm.currentMessage.type === 1) ? vm.PredefinedMessages : vm.PredefinedMessagesForRevision;
        arr.push(vm.currentMessage);
        return AdminSettingsPredefinedMessagesService.PredefinedMessages.store(vm.currentMessage).$promise ;
      };

       /**
       * Delete a message
       * @param {Int} key
       * @param {Int} message
       * @return {Resource} response
       */
      vm.deleteMessage = function(key, row) {       
        var arr = (row.type === 1) ? vm.PredefinedMessages : vm.PredefinedMessagesForRevision;
        for (var i = arr.length - 1; i >= 0; i--) {
          if(key === i){
            arr.splice(key,1);
          }
        }  
       
        return AdminSettingsPredefinedMessagesService.PredefinedMessages.destroy(row.id).$promise ;
      };

      vm.resetForm = function () {
        vm.currentMessage = {};
      };

      return this;     
    }
  ]);
