'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminSettingsRemindersCtrl
 * @description
 * # AdminSettingsRemindersCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AdminSettingsRemindersCtrl', [
    'AdminSettingsRemindersService', '$q',

    function (AdminSettingsRemindersService, $q) {

      var vm = this;

      var availableOptions = AdminSettingsRemindersService.availableOptions.getAll();
      var reminders = AdminSettingsRemindersService.reminders.getAll();

      $q.all([
        availableOptions.$promise,
        reminders.$promise
      ]).then(function () {
        vm.availableOptions = availableOptions;
        vm.reminders = reminders;
      });

		  /**
		   * Update reminder
		   * @param {Int} Id
		   * @return {Resource} response
		   */
      vm.updateReminder = function (id) {
      	return AdminSettingsRemindersService.reminders.update(vm.reminders[id]);
      };

		  /**
		   * Change reminder activation status
		   * @param {Int} Id
		   * @param {Boolean} status
		   * @return {Resource} response
		   */
      vm.toogleRemider = function (id, status) {
      	vm.reminders[id].active = status;
      	return AdminSettingsRemindersService.reminders.update(vm.reminders[id]);
      };

	    /**
	     * Open modal window for appraisal forms
	     * @param {Int} reminderId
	     */
	    vm.openAppraisalFormsModal = function (reminderId) {
	      var appraisalForms = AdminSettingsRemindersService.appraisalForms.getAll({id: reminderId});

	      $q.all([
	        appraisalForms.$promise
	      ]).then(function () {
	        vm.appraisalForms = {};

	        angular.forEach(appraisalForms, function (form) {
	          vm.appraisalForms[form.id] = form;
	        });

	        vm.selectedReminder = reminderId;
	        vm.appraisalFormsModal = true;
	      });
	    };

      /**
       * Select a jobType in appraisal forms window
       * @param {Int} reminderId
       * @return {Resource} response
       */
      vm.selectJobType = function (reminderId) {
        return AdminSettingsRemindersService.appraisalForms.update({id: reminderId}, vm.appraisalForms);
      };

      /**
       * Select all jobTypes in appraisal forms window
       * @param {Int} reminderId
       * @param {Boolean} checked
       * @return {Resource} response
       */
      vm.selectAllJobTypes = function (reminderId, checked) {
        angular.forEach(vm.appraisalForms, function (form) {
          form.val = checked;
        });
        return AdminSettingsRemindersService.appraisalForms.update({id: reminderId}, vm.appraisalForms);
      };

	    /**
	     * Open modal window to set schedule for reminders
	     */
	    vm.openReminderScheduleModal = function () {
	    	var scheduler = AdminSettingsRemindersService.scheduler.getAll();

	      $q.all([
	        scheduler.$promise
	      ]).then(function () {
	        vm.schedulerHours = scheduler;
	        vm.reminderScheduleModal = true;
	      });
	    };

      /**
       * Select an hour in scheduler modal
       * @return {Resource} response
       */
      vm.selectSchedulerHour = function () {
        return AdminSettingsRemindersService.scheduler.update(vm.schedulerHours);
      };

      /**
       * Select all hours in scheduler modal
       * @param {Boolean} checked
       */
      vm.selectAllSchedulerHours = function (checked) {
        angular.forEach(vm.schedulerHours, function (hour) {
          hour.val = checked;
        });
        return AdminSettingsRemindersService.scheduler.update(vm.schedulerHours);
      };

      return this;
    }
  ]);
