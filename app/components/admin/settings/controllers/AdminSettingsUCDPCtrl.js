'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminSettingsInsctructionsCtrl
 * @description
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AdminSettingsUCDPCtrl', [
  	'AdminSettingsUCDPService',
    '$q',

    function (AdminSettingsUCDPService, $q) { 
     
      var vm = this;
      var userData = AdminSettingsUCDPService.adminUCDP.getAll();

      $q.all([       
        userData.$promise
      ]).then(function () {       	       
        vm.userData = userData;            
      });

      vm.updateUserData = function (){
      	console.log(1);
 				return AdminSettingsUCDPService.adminUCDP.update(vm.userData ).$promise;
      };

      return this;     
    }
  ]);
