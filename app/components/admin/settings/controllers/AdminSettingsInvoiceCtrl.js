'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminSettingsInvoiceCtrl
 * @description
 * # AdminSettingsInvoiceCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AdminSettingsInvoiceCtrl', [
    '$scope', 'AdminSettingsService', 'RecordNumberPrefixService', 'Utils',

    function ($scope, AdminSettingsService, RecordNumberPrefixService, Utils) {

      // Creating a new recordNumberPrefix using the file endpoint
      var invoice = new RecordNumberPrefixService({'apiResource' : AdminSettingsService.invoice});

      // Decorating $scope with file properties and methods
      Utils.decorate($scope, invoice);
    }
  ]);