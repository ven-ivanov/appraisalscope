'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminSettingsDocumentUploadCtrl
 * @description
 * # AdminSettingsDocumentUploadCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AdminSettingsDocumentUploadCtrl', [
    '$scope', 'AdminSettingsUploadService', '$q',

    function ($scope, AdminSettingsUploadService, $q) {

      var vm = this;

      var addDocType = AdminSettingsUploadService.additionalDocType.getAll();
      var systemDocs = AdminSettingsUploadService.systemDocs.getAll();
      var templates = AdminSettingsUploadService.templates.getAll();
      var sysTemplates = AdminSettingsUploadService.sysTemplates.getAll();
      var templateTags = AdminSettingsUploadService.templateTags.getAll();

      $q.all([
        addDocType.$promise,
        systemDocs.$promise,
        templates.$promise,
        sysTemplates.$promise,
        templateTags.$promise
      ]).then(function () {

        vm.addDocType = addDocType.data;
        vm.systemDocs = systemDocs.data;
        vm.templates = templates.data;
        vm.sysTemplates = sysTemplates.data;
        vm.templateTags = templateTags.data;
      });

      vm.uploadDoc = function (file) {
        // This is triggered on change, so ensure that a file is being input
        if (!file) {
          return;
        }

        // Import csv via upload service
        AdminSettingsUploadService.uploadDoc(file).promise.then(function (response) { // jshint ignore:line
          // Eventual response
          $scope.$broadcast('show-modal', 'upload-doc-successful');
        }, function (err) { // jshint ignore:line
          vm.uploadError = err.data;
          $scope.$broadcast('show-modal', 'upload-doc-failure');
        });
      };

      vm.updateAddDocType = function (doc) {
        return AdminSettingsUploadService.additionalDocType.update(doc);
      };

      /**
       * Delete an additional document type
       * @param {Int} docId
       * @param {Int} key
       * @return {Resource} response
       */
      vm.deleteAddDocType = function (docId, key) {
        vm.addDocType.splice(key, 1);
        return AdminSettingsUploadService.additionalDocType.destroy(docId);
      };

      /**
       * Delete a system document
       * @param {Int} docId
       * @param {Int} key
       * @return {Resource} response
       */
      vm.deleteSystemDoc = function (docId, key) {
        vm.systemDocs.splice(key, 1);
        return AdminSettingsUploadService.systemDocs.destroy(docId);
      };

      /**
       * Delete a template
       * @param {Int} docId
       * @param {Int} key
       * @return {Resource} response
       */
      vm.deleteTemplate = function (templateId, key) {
        vm.templates.splice(key, 1);
        return AdminSettingsUploadService.templates.destroy(templateId);
      };

      /**
       * Open modal window to edit template
       * @param {Object} template
       */
      vm.editTemplate = function (template) {
        vm.currentTemplate = template;
        vm.editTemplateModal = true;
      };

      /**
       * Add tag to editing template body
       * @param {String} tag
       */
      vm.addTagtoEditField = function (tag) {
        if (tag) {
          vm.currentTemplate.body += '[' + tag + ']';
        }
      };

      /**
       * Update specified template
       * @param {Object} template
       * @return {Resource} response
       */
      vm.updateTemplate = function (template) {
        return AdminSettingsUploadService.templates.update(template);
      };

      /**
       * Open modal window to create new template
       */
      vm.openNewTemplateModal = function () {
        vm.newTemplate = {};
        vm.createTemaplteModal = true;
      };

      /**
       * Add tag to new template body
       * @param {String} tag
       */
      vm.addTagToNewField = function (tag) {
        if (tag) {
          vm.newTemplate.body += '[' + tag + ']';
        }
      };

      /**
       * Store new template
       * @param {Object} template
       * @return {Object} newTemplate
       */
      vm.storeTemplate = function (template) {
        var newTemplate = AdminSettingsUploadService.templates.store(template);

        $q.all([
          newTemplate.$promise
        ]).then(function () {
          vm.templates.push(newTemplate);
        });

        return newTemplate;
      };

      /**
       * Open modal window to create new document type
       */
      vm.openNewDocTypeModal = function () {
        vm.newDocType = {};
        vm.newDocTypeModal = true;
      };

      /**
       * Store new document type
       * @param {Object} docType
       * @return {Object} newDocType
       */
      vm.addDocType = function (docType) {
        var newDocType = AdminSettingsUploadService.additionalDocType.store(docType);

        $q.all([
          newDocType.$promise
        ]).then(function () {
          vm.addDocType.push(newDocType);
        });

        return newDocType;
      };

    }
  ]);
