'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminSettingsOptionsCtrl
 * @description
 * # AdminSettingsOptionsCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AdminSettingsOptionsCtrl', [
    'AdminSettingsOptionsService', '$q',

    function (AdminSettingsOptionsService, $q) {

      var vm = this;

      vm.turnTimeRatings = {
        star5: 0,
        star4: 0,
        star3: 0,
        star2: 0
      };
      var defaultSliderOptions = function () {
        return {
          from: 1,
          to: 60,
          step: 1,
          smooth: true,
          css: {
            background: {'background-color': 'silver'},
            before: {'background-color': 'silver'},
            default: {'background-color': 'white'},
            after: {'background-color': 'silver'},
            pointer: {'background-color': 'green'},
            range: {'background-color': 'silver'},
          }
        };
      };
      vm.sliderOptions = {
        star5: defaultSliderOptions(),
        star4: defaultSliderOptions(),
        star3: defaultSliderOptions(),
        star2: defaultSliderOptions()
      };

      var availableOption = AdminSettingsOptionsService.availableOption.getAll();
      var adminOptions = AdminSettingsOptionsService.adminOptions.getAll();

      $q.all([
        availableOption.$promise,
        adminOptions.$promise
      ]).then(function () {
        vm.unpaidOrdersOptions = availableOption.data.unpaidOrders;
        vm.appraisalDocumentsOptions = availableOption.data.appraisalDocuments;
        vm.additionalDocumentsOptions = availableOption.data.additionalDocuments;
        vm.adminOptions = adminOptions.adminOptions;

        vm.dueDateOptions = availableOption.data.dueDate;

        vm.currentOptions = {};
      });

      /**
       * Open modal for due dates
       */
      vm.openDueDateModal = function () {
        var jobTypesForDueDates = AdminSettingsOptionsService.dueDateJobType.getAll();

        $q.all([
          jobTypesForDueDates.$promise
        ]).then(function () {
          vm.jobTypesForDueDates = jobTypesForDueDates;
          vm.setDueDateModal = true;
        });
      };

      /**
       * Update due date JobTypes
       * @param {Object} jobTypes
       * @return {Object} newJobType
       */
      vm.updateDueDateJobTypes = function (jobTypes) {
        return AdminSettingsOptionsService.dueDateJobType.update(jobTypes);
      };

      /**
       * Open modal window for appraisal forms
       */
      vm.openAppraisalFormsModal = function () {
        var appraisalForms = AdminSettingsOptionsService.appraisalForm.getAll();

        $q.all([
          appraisalForms.$promise
        ]).then(function () {
          vm.appraisalForms = {};

          angular.forEach(appraisalForms, function (form) {
            vm.appraisalForms[form.id] = form;
          });

          vm.appraisalFormsModal = true;
        });
      };

      /**
       * Select a jobType in appraisal forms window
       */
      vm.selectJobType = function () {
        return AdminSettingsOptionsService.appraisalForm.update(vm.appraisalForms);
      };

      /**
       * Select all jobTypes in appraisal forms window
       * @param {Boolean} checked
       */
      vm.selectAllJobTypes = function (checked) {
        angular.forEach(vm.appraisalForms, function (form) {
          form.val = checked;
        });
        return AdminSettingsOptionsService.appraisalForm.update(vm.appraisalForms);
      };

      /**
       * Open modal window for revision types
       */
      vm.openRevisionTypeModal = function () {
        var revisionTypes = AdminSettingsOptionsService.revisionType.getAll();

        $q.all([
          revisionTypes.$promise
        ]).then(function () {
          vm.revisionTypes = revisionTypes;
          vm.newRevisionType = {};
          vm.revisionTypeModal = true;
        });
      };

      /**
       * Crete a new revision type
       * @param {Object} newRevisionType
       */
      vm.addRevisionType = function (newRevisionType) {

        newRevisionType.asClientRevision = newRevisionType.type === '1' ? true : false;
        newRevisionType.asReviewerRevision = !newRevisionType.asClientRevision;

        var stored = AdminSettingsOptionsService.revisionType.store(newRevisionType);

        $q.all([
          stored.$promise
        ]).then(function () {
          if (stored) {
            vm.revisionTypes.push(stored);
          }
        });
      };

      /**
       * Update a revision type
       * @param {Int} key
       * @param {Object} revisionType
       * @return {Resource} response
       */
      vm.updateRevisionType =  function (key, revisionType) {
        vm.editRevisionType[key] = false;
        return AdminSettingsOptionsService.revisionType.update(revisionType);
      };

      /**
       * Delete a revision type
       * @param {Int} key
       * @param {Int} revisionTypeId
       * @return {Resource} response
       */
      vm.deleteVendor =  function (key, revisionTypeId) {
        vm.revisionTypes.remove(key);
        return AdminSettingsOptionsService.revisionType.destroy(revisionTypeId);
      };

      /**
       * Open modal window for sorting order on manual appraisal assignment
       */
      vm.openManualSortOrderModal = function () {
        var manualSortOrder = AdminSettingsOptionsService.manualSortOrder.getAll();

        $q.all([
          manualSortOrder.$promise
        ]).then(function () {
          vm.manualSortOrder = manualSortOrder;
          vm.manualSortOrderModal = true;
        });
      };

      /**
       * Update specified sorting order on manual appraisal assignment
       * @param {Object} order
       * @return {Resource} response
       */
      vm.updateManualSortOrder = function (order) {
        return AdminSettingsOptionsService.manualSortOrder.update(order);
      };

      /**
       * Open modal window for sorting order on auto appraisal assignment
       */
      vm.openAutoSortOrderModal = function () {
        var autoSortOrder = AdminSettingsOptionsService.autoSortOrder.getAll();

        $q.all([
          autoSortOrder.$promise
        ]).then(function () {
          vm.autoSortOrder = autoSortOrder;
          vm.autoSortOrderModal = true;
        });
      };

      /**
       * Update specified sorting order on auto appraisal assignment
       * @param {Object} order
       * @return {Resource} response
       */
      vm.updateAutoSortOrder = function (order) {
        return AdminSettingsOptionsService.autoSortOrder.update(order);
      };

      /**
       * Open modal window for Mercury jobType mapping
       */
      vm.openMapJobTypesModal = function () {
        var localJobTypes = AdminSettingsOptionsService.localJobTypes.getAll();
        var mercuryJobTypes = AdminSettingsOptionsService.mercuryJobTypes.getAll();

        $q.all([
          localJobTypes.$promise,
          mercuryJobTypes.$promise
        ]).then(function () {
          vm.localJobTypes = localJobTypes;
          vm.mercuryJobTypes = mercuryJobTypes;
          vm.mapJobTypesModal = true;
        });
      };

      /**
       * Update jobType mapping to mercury
       * @param {Object} jobType
       * @return {Resource} response
       */
      vm.mapMercuryJobType = function (jobType) {
        return AdminSettingsOptionsService.localJobTypes.update(jobType);
      };

      /**
       * Open modal window for turn time ratings and set sliders options
       */
      vm.openTurnTimeRatingModal = function () {
        var ratings = AdminSettingsOptionsService.turnTimeRatings.getAll();

        $q.all([
          ratings.$promise
        ]).then(function () {
          vm.turnTimeRatings = ratings;

          vm.sliderOptions.star5.from = 1;
          vm.sliderOptions.star4.from = ratings.star5 + 1;
          vm.sliderOptions.star3.from = ratings.star4 + 1;
          vm.sliderOptions.star2.from = ratings.star3 + 1;

          vm.turnTimeRatingModal = true;
        });
      };

      /**
       * Update turn time ratings
       * @return {Resource} response
       */
      vm.updateTurnTimeRatings = function () {
        return AdminSettingsOptionsService.turnTimeRatings.update(vm.turnTimeRatings);
      };

      /**
       * Asjust turn time ratings sliders options when value changed
       * @param {Object} jobType
       * @return {Resource} response
       */
      vm.adjustTurnTimeRatings = function () {

        if(vm.turnTimeRatings.star5 >= vm.turnTimeRatings.star4) {
          vm.turnTimeRatings.star4 = parseInt(vm.turnTimeRatings.star5) + 1;
        }

        if(vm.turnTimeRatings.star4 >= vm.turnTimeRatings.star3) {
          vm.turnTimeRatings.star3 = parseInt(vm.turnTimeRatings.star4) + 1;
        }

        if(vm.turnTimeRatings.star3 >= vm.turnTimeRatings.star2) {
          vm.turnTimeRatings.star2 = parseInt(vm.turnTimeRatings.star3) + 1;
        }

        if(vm.turnTimeRatings.star3 >= vm.turnTimeRatings.star1) {
          vm.turnTimeRatings.star1 = parseInt(vm.turnTimeRatings.star2) + 1;
        }

        vm.sliderOptions.star4.from = parseInt(vm.turnTimeRatings.star5) + 1;
        vm.sliderOptions.star3.from = parseInt(vm.turnTimeRatings.star4) + 1;
        vm.sliderOptions.star2.from = parseInt(vm.turnTimeRatings.star3) + 1;

        vm.updateTurnTimeRatings();
      };

      /**
       * Open modal window for deduction fields
       */
      vm.openDeductionFieldsModal = function () {
        var deductionFields = AdminSettingsOptionsService.deductionField.getAll();

        $q.all([
          deductionFields.$promise
        ]).then(function () {
          vm.deductionFields = deductionFields;
          vm.deductionFieldsModal = true;
        });
      };

      /**
       * Add custom row in deduction fields modal window
       */
      vm.addCustomRow = function () {
        vm.deductionFields.push('');
      };

      /**
       * Update deduction fields
       * @return {Resource} response
       */
      vm.updateDeductionFields = function () {
        return AdminSettingsOptionsService.deductionField.update(vm.deductionFields);
      };

      /**
       * Open modal window to select updates
       */
      vm.openUpdatesModal = function () {
        var updates = AdminSettingsOptionsService.updates.getAll();

        $q.all([
          updates.$promise
        ]).then(function () {
          vm.updates = updates;
          vm.updatesModal = true;
        });
      };

      /**
       * Update whether to notigy client or not
       * @param {Int} Id
       * @param {Object} update
       * @return {Resource} response
       */
      vm.updateUpdates = function (id, update) {
        return AdminSettingsOptionsService.updates.udpate(update);
      };

      /**
       * Open modal window for vendors
       */
      vm.openAppraiserDesignationModal = function () {
        var vendors = AdminSettingsOptionsService.vendors.getAll();

        $q.all([
          vendors.$promise
        ]).then(function () {
          vm.vendors = vendors;
          vm.newVendor = {};
          vm.appraiserDesignationModal = true;
        });
      };

      /**
       * Crete a new vendor
       * @param {Object} newVendor
       * @param {Resource} response
       */
      vm.addVenor = function (newVendor) {

        var stored = AdminSettingsOptionsService.vendors.store(newVendor);

        $q.all([
          stored.$promise
        ]).then(function () {
          if (stored) {
            vm.vendors.push(stored);
            return stored;
          }
        });
      };

      /**
       * Update a vendor
       * @param {Int} key
       * @param {Object} aVendor
       * @return {Resource} response
       */
      vm.updateVendor = function (key, aVendor) {
        vm.editVendor[key] = false;
        return AdminSettingsOptionsService.vendors.update(aVendor);
      };

      /**
       * Delete a vendor
       * @param {Int} key
       * @param {Int} vendorId
       * @return {Resource} response
       */
      vm.deleteVendor = function (key, vendorId) {
        vm.vendors.remove(key);
        return AdminSettingsOptionsService.vendors.destroy(vendorId);
      };

      vm.openTermsAndConditionsModal = function () {
        vm.termsAndConditionsModal = true;
      };

      /**
       * Open modal window for terms and conditions
       */
      vm.openTermsAndConditionsModal = function () {
        var termsAndConditions = AdminSettingsOptionsService.termsAndConditions.getAll();

        $q.all([
          termsAndConditions.$promise
        ]).then(function () {
          vm.termsAndConditions = termsAndConditions.terms;
          vm.termsAndConditionsModal = true;
        });
      };

      /**
       * Update terms and conditions
       * @return {Resource} response
       */
      vm.saveTermsAndConditions = function () {
        return AdminSettingsOptionsService.termsAndConditions.udpate(vm.termsAndConditions);
      };

      /**
       * Open modal window for terms and conditions for appraiser login
       */
      vm.openAppraiserLoginTACModal = function () {
        var appraiserLoginTAC = AdminSettingsOptionsService.termsAndConditions.getAll();

        $q.all([
          appraiserLoginTAC.$promise
        ]).then(function () {
          vm.appraiserLoginTAC = appraiserLoginTAC.terms;
          vm.appraiserLoginTACModal = true;
        });
      };

      /**
       * Update terms and conditions for appraiser login
       * @return {Resource} response
       */
      vm.saveTermsAndConditions = function () {
        return AdminSettingsOptionsService.termsAndConditions.udpate(vm.appraiserLoginTAC);
      };

      return this;
    }
  ]);
