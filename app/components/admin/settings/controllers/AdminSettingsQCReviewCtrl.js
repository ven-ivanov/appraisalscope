'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminSettingsQCReviewCtrl
 * @description
 * # AdminSettingsQCReviewCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AdminSettingsQCReviewCtrl', ['AdminSettingsQCReviewService', '$q','$scope',
    function (AdminSettingsQCReviewService, $q, $scope) {
      var vm = this;
      var rows = AdminSettingsQCReviewService.reviewTemplates.getAll();
      var main = angular.element('#main-review').scope();
      main.status = 'preview';

      $q.all([
      	rows.$promise
      ]).then(function(){
        vm.tableData = rows;
      });

			// adding new review from save button
      vm.addNew = function(){
        if(angular.isUndefined(vm.newReview)){
          return false;
        }
        
      	var arr = angular.element('#reviewsTable').scope().tableCtrl.tableData;
      	var newElement = {name:vm.newReview,id:arr.length+1};
        var save = AdminSettingsQCReviewService.saveReviewTable();
        $q.all([
          save.$promise
          ]).then(function(){
            newElement.id = save.id;
            arr.push(newElement);
          });

      	return ;
      };

      vm.confirmDelete = function(key){
      	main.deleteKey = key;
        main.tableData = vm.tableData;
      	main.$broadcast('show-modal','confirm-delete');
      };

      vm.deleteReview = function() {     
        var arr = main.tableData;
        angular.forEach(arr,function(value,key){
          if(value.id === main.deleteKey){
            arr.splice(key,1);
            AdminSettingsQCReviewService.reviewID = value.id;
          }
        });
       
        main.$broadcast('hide-modal','confirm-delete');

      	return AdminSettingsQCReviewService.deleteReview();
     	};

     	vm.editReview = function(key){
          main.status = 'edit';
          angular.forEach(vm.tableData,function(value){
            if(value.id === key){
              main.qcReviewCtrl.editLabel = value.name;
            }
          });
     			var tables = AdminSettingsQCReviewService.reviewTables.getAll({'id':key});
     			var availableOptions = AdminSettingsQCReviewService.availableElements.getAll();

     			$q.all([
     				availableOptions.$promise,
     				tables.$promise
     			]).then(function(){
     				main.elementOptions = availableOptions;
     				vm.reviewTables = tables;
            // Store on service
            AdminSettingsQCReviewService.editTableData = tables;
            
     			});
     	};

     	vm.cloneReview = function(key){
     		var arr = vm.tableData;

        angular.forEach(arr,function(value){
          if(value.id === key){
            var newElement = {name:value.name+' clone',id:arr.length+1};
            AdminSettingsQCReviewService.reviewTableId = key;
            var cloneID = AdminSettingsQCReviewService.cloneReview();
            $q.all([
              cloneID.$promise
              ]).then(function(){
                newElement.id = cloneID.id;
                arr.push(newElement);
              });
          }
        });
        
     		return ;
     	};

     	vm.closeEdit = function(){
        main.status = 'preview';          
     	};

     	vm.generete = function() {
        main.newReviewTable = true;
     		vm.modalLabel = main.elementOptions[vm.addElement];
     		vm.reviewLabel = '';
     		if(parseInt(vm.addElement) > 3) {

     			vm.addChoises = [{'id':1,'title':''}];
     			switch(parseInt(vm.addElement)){
     				case 0:
            main.newTableType = '0';
            break;
            case 1:
            main.newTableType = 'input';
            break;
            case 4:
            main.newTableType = 'dropdown';
            break;
            case 5:
            main.newTableType = 'dropdown-multiselect';
            break;
     			}
     		} else {
     			vm.addChoises = false;
     		}

     		if(!angular.isUndefined(vm.addElement) && vm.addElement !== null) {

     			if(vm.addElement === '0' || vm.addElement === '1') {
     				vm.oneLine = true;
     				vm.multyLine = false;

     				if(vm.addElement === '0') {

     					vm.modalLabel = '';
     				}
     			} else {
     				vm.oneLine = false;
     				vm.multyLine = true;
     			}

          AdminSettingsQCReviewService.reviewTables.id = vm.editID;
          $scope.$broadcast('show-modal','genereted');
     		}
     	};

     	vm.addChoise = function(){
     		var newID = vm.addChoises[vm.addChoises.length-1].id+1;
     		vm.addChoises.push({'id':newID,'title':''});
     	};

     	vm.removeChoise = function(key){
     		vm.addChoises.splice(key,1);
     	};

     	vm.addEditTable = function(){
        var editTable = angular.element('#reviewTables').scope();
        var table = editTable.tableCtrl;

        if(main.newReviewTable === true) {
          AdminSettingsQCReviewService.reviewTable = {'dataArray':vm.addChoises,'title':vm.reviewLabel,type:main.newTableType,status:'E'};
          table.tableData.push(AdminSettingsQCReviewService.reviewTable);
          table.specialCellsConfig.push({id:table.tableData.length+1,options:vm.addChoises,type:main.newTableType});
          AdminSettingsQCReviewService.saveReviewTable();
          $scope.$broadcast('hide-modal','genereted'); 
        } else {
          AdminSettingsQCReviewService.reviewTable = {'dataArray':vm.addChoises,'label':vm.reviewLabel,'id':vm.tableID};
          AdminSettingsQCReviewService.updateReviewTable();
          $scope.$broadcast('hide-modal','genereted');
        }
     	};

     	vm.changeReviewName = function(){
          AdminSettingsQCReviewService.review = {'id':vm.editID,'new_name':main.editLabel};
        	AdminSettingsQCReviewService.updateReviewName();
     	};

      vm.heading = [
        {
          'label': 'No.',
          'data' : 'id'
        },
        {
          'label': 'Name',
          'data' : 'name'
        },
        {
          'label': 'Edit',
          'linkLabel': 'Edit',
          'fn'   : 'edit'
        },
        {
          'label': 'Delete',
          'linkLabel': 'Delete',
          'fn' : 'delete'
        },
        {
          'label': 'Clone',
          'linkLabel': 'Clone',
          'fn' : 'clone'
        },
      ];

      vm.linkFn = function (id, fnName) {
        switch(fnName){
          case 'clone':
            vm.cloneReview(id);
            break;

          case 'delete':
            vm.confirmDelete(id);
            break;

          case 'edit':
            vm.editReview(id);
            break;
        }
      };

      vm.reset = function(){
        vm.newReview = '';
      };
    }
  ]);