'use strict';

var app = angular.module('frontendApp');

app.controller('AdminSettingsQcReviewEditTableCtrl',
  ['$scope', 'AdminSettingsQCReviewService',

 function ($scope, AdminSettingsQCReviewService) {
  var vm = this;
  var main = angular.element('#main-review').scope();

  $scope.$watchCollection(function () {
    return AdminSettingsQCReviewService.editTableData;
  }, function (newVal) {
    if (angular.isUndefined(newVal)) {
      return;
    }

    vm.specialCellsConfig = [];
    angular.forEach(newVal, function (record) {
      record.statusTitle = record.title + ' [' + record.status + ']';
      record.options = [];

      if(record.elementType.indexOf('dropdown') !== false && record.dataArray.length > 0)
      {
        angular.forEach(record.dataArray,function(value){
          record.options.push({value: value});
        });
      }
      var spec = {
        id:record.id,
        type:record.elementType,
        options: record.options,
        settings: {
          scrollableHeight: '150px',
          scrollable: true,
          displayProp: 'value'
        }
      };
      console.log(spec);
      vm.specialCellsConfig.push(spec);
      console.log(vm.specialCellsInit);
      vm.specialCellsInit = {};
      //$scope.compile();

    });
    // Create table data
    vm.tableData = newVal;
    console.log(vm.specialCellsConfig);
  });

  vm.heading = [
    {
      label: 'Title',
      data: 'statusTitle'
    },
    {
      label: 'Status',
      fn: 'status',
      linkLabel: 'Status'
    },
    {
      label: 'Edit',
      fn: 'edit',
      linkLabel: 'Edit'
    },
    {
      label: 'Delete',
      fn: 'delete',
      linkLabel: 'Delete'
    },
    {
      label: 'spetial',
      special: true
    }
  ];

  $scope.$watchCollection(function () {
    return vm.tableInputs;
  }, function (newVal) {
    if (angular.isUndefined(newVal)) {
      return;
    }
    console.log('**************TABLE INPUTS CHANGED**********');
    console.log(newVal);
  });
  $scope.$watchCollection(function () {
    return vm.multiSelect;
  }, function (newVal) {
    if (angular.isUndefined(newVal)) {
      return;
    }
    console.log('**************INPUT CHANGED**********');
    console.log(newVal);
  });

  vm.deleteReviewTable = function(key){
    AdminSettingsQCReviewService.deleteReviewTable();

    angular.forEach(vm.tableData, function (record, id) {
      if (record.id === key) {
        AdminSettingsQCReviewService.editTableData.splice(id,1);
      }
    });

  };

  vm.linkFn = function (id, fnName) {
    console.log(id);
    console.log(fnName);
    
    AdminSettingsQCReviewService.reviewTableId = id;

    switch(fnName) {
      case 'delete':
        vm.deleteReviewTable(id);
        break;

      case 'edit':
        vm.editReviewTable(id);
        break;

      case 'status':
        vm.changeTableStatus(id);
        break;
    }
  };

  vm.editReviewTable = function(id){
    var row;
    main.newReviewTable = false;
    angular.forEach(vm.tableData, function (record) {
      if (record.id === id) {
        row = record;
      }
    });
    main.qcReviewCtrl.reviewLabel = row.title;
    main.qcReviewCtrl.addChoises = [];

    if(row.elementType === 'input' || row.elementType === '0') {
      main.qcReviewCtrl.oneLine = true;

      if(row.type === '0') {
        main.qcReviewCtrl.modalLabel = '';
      }
    } else{
      main.qcReviewCtrl.oneLine = false;
    }

    if(angular.isArray(row.dataArray) && row.elementType.indexOf('dropdown') !== -1) {

      main.qcReviewCtrl.modalLabel = row.elementType;
      main.qcReviewCtrl.tableID = row.id;

      angular.forEach(row.dataArray,function(value,key){
        main.qcReviewCtrl.addChoises.push({'id':key,'title':value});
      });

    } else {
      main.qcReviewCtrl.addChoises = false;
    }
    console.log(row.elementType);
    main.$broadcast('show-modal','genereted');
  };

  vm.changeTableStatus = function(key){
    angular.forEach(vm.tableData, function (record,ind) {
      if (record.id === key) {
        if(record.status !== 'D') {
          record.status = 'D';
        } else {
          record.status = 'E';
        }
        vm.tableData[ind].statusTitle = record.title + ' [' + record.status + ']';
        AdminSettingsQCReviewService.reviewTable = record;
        AdminSettingsQCReviewService.updateReviewTable();
      }
    });
  };

}]);