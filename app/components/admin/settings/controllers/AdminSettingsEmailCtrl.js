'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminSettingsInsctructionsCtrl
 * @description
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AdminSettingsEmailCtrl', [
  	'AdminSettingsEmailService',
    '$q',

    function (AdminSettingsEmailService, $q) { 
     
      var vm = this;
      var clientTemplates = AdminSettingsEmailService.clientTemplates.getAll(); 
      var appraiserTemplates = AdminSettingsEmailService.appraiserTemplates.getAll(); 
      var borrowerContactTemplates = AdminSettingsEmailService.borrowerContactTemplates.getAll(); 
    	
      $q.all([       
        clientTemplates.$promise,      
        appraiserTemplates.$promise,      
        borrowerContactTemplates.$promise      
      ]).then(function () {         
        vm.clientTemplates = clientTemplates;
        vm.appraiserTemplates = appraiserTemplates;
        vm.borrowerContactTemplates = borrowerContactTemplates;
      });

       /**
       * Open modal for edit Template
       */
      vm.openEditTemplateModal = function(row, type) {

        vm.currentTemplate = {};
        vm.currentTemplate.id = row.id;
        vm.currentTemplate.title = row.title;
        vm.currentTemplate.from = row.from;
        vm.currentTemplate.template = row.template;
        vm.currentTemplate.subject = row.subject;
        vm.currentTemplate.type = type;        
        
        vm.setEditTemplateModal = true;
      };

      /**
       * Update a template     
       * @param {Object} template
       * @return {Resource} response
       */
      vm.updateTemplate = function() {
        var success, fail, templates;
        success = function(){
          for (var i = templates.length - 1; i >= 0; i--) {
            if(templates[i].id === vm.currentTemplate.id) {
              templates[i] = vm.currentTemplate;
            }
          }
          vm.setEditTemplateModal = false;
        };

        // query failed.
        fail = function(){ };

        switch(vm.currentTemplate.type) {
          case 'client':
              templates = vm.clientTemplates;
              return AdminSettingsEmailService.clientTemplates.update(vm.currentTemplate, success, fail).$promise;
          case 'borrow':
              templates = vm.borrowerContactTemplates;
              return AdminSettingsEmailService.borrowerContactTemplates.update(vm.currentTemplate, success, fail).$promise;
          case 'appraiser':
              templates = vm.appraiserTemplates;
              return AdminSettingsEmailService.appraiserTemplates.update(vm.currentTemplate, success, fail).$promise;
        }        
      };

      return this;     
    }
  ]);
