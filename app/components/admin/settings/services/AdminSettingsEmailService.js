'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AdminSettingsEmailService
 * @description
 * # AdminSettingsEmailService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AdminSettingsEmailService', [
    '$resource', 'API_PREFIX',

    function ($resource, API_PREFIX) {

      var prefix = API_PREFIX() + '/v2.0/admin/settings/email/';

      var AdminSettingsEmailService = {};

      AdminSettingsEmailService.clientTemplates = $resource(prefix + 'client-templates', {}, {
        getAll: {method: 'GET', isArray: true},
        update: {method: 'PUT'}       
      }); 

      AdminSettingsEmailService.appraiserTemplates = $resource(prefix + 'appraiser-templates', {}, {
        getAll: {method: 'GET', isArray: true},
        update: {method: 'PUT'}       
      });
      
      AdminSettingsEmailService.borrowerContactTemplates = $resource(prefix + 'borrower-contact-templates', {}, {
        getAll: {method: 'GET', isArray: true},
        update: {method: 'PUT'}       
      });

      return AdminSettingsEmailService;
    }
  ]);