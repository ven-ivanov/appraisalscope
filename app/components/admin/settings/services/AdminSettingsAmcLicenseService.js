'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AdminSettingsAmcLicenseService
 * @description
 * # AdminSettingsOptionsService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AdminSettingsAmcLicenseService', [
    '$resource', 'API_PREFIX',

    function ($resource, API_PREFIX) {

      var prefix = API_PREFIX() + '/v2.0/admin/settings/amcLicense/';

      return {
        adminAmcLicense: $resource(prefix + 'amcLicense', {}, {
          getAll: {method: 'GET', isArray: true},
          update: {method: 'PUT'} 
        })
      };
    }
  ]);