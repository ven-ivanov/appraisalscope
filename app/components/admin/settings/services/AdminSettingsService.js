'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AdminSettingsService
 * @description
 * # AdminSettingsService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AdminSettingsService', [
    '$resource', 'API_PREFIX',

    function ($resource, API_PREFIX) {

      var file = API_PREFIX() + '/v2.0/admin/settings/file';
      var invoice = API_PREFIX() + '/v2.0/admin/settings/invoice';

      var AdminSettingsService = {};

      //File resource
      AdminSettingsService.file = $resource(file, {}, {
        getAll: {method: 'GET', isArray: false},
        store : {method: 'POST', isArray: false}
      });

      //Invoice resource
      AdminSettingsService.invoice = $resource(invoice, {}, {
        getAll: {method: 'GET', isArray: false},
        store: {method: 'POST', isArray: false}
      });

      return AdminSettingsService;
    }
  ]);
