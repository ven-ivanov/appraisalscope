'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AdminSettingsEmailService
 * @description
 * # AdminSettingsEmailService
 * Factory in the frontendApp.
 *
 * @todo The context of this file looks totally messed up. Need to fix at some point. -- Logan 6/22/15
 */
angular.module('frontendApp')
  .factory('AdminSettingsQCReviewService', [
    '$resource', 'API_PREFIX',

    function ($resource, API_PREFIX) {
      var AdminSettingsQCReviewService = {};
      var prefix = API_PREFIX() + '/v2.0/admin/settings/qcReview/';
      this.reviewTableId = null;
      this.reviewID = null;
      this.reviewTable = {};

      AdminSettingsQCReviewService.reviewTemplates = $resource(prefix + 'appraisal-qc-review', {}, {
        getAll: {method: 'GET', isArray: true},
        update: {method: 'PUT'},
        destroy: {method: 'DELETE'},
        store: {method: 'POST'}
      });

      AdminSettingsQCReviewService.reviewClone = $resource(prefix + 'clone-qc-review', {}, {
      	store: {method: 'POST'}
      });

      AdminSettingsQCReviewService.availableElements = $resource(prefix+'available-elements', {},{
      	getAll: {method: 'GET', isArray: true}
      });

      AdminSettingsQCReviewService.reviewTables = $resource(prefix+'review-tables',{},{
      	getAll: {method: 'GET', isArray: true},
      	store:  {method: 'POST'},
      	destroy: {method: 'DELETE'},
        update: {method: 'PUT'}
      });

      AdminSettingsQCReviewService.singleTable = $resource(prefix+'single-table', {}, {
      	getCur: {method: 'GET', isArray: true},
      	update: {method: 'PUT'}
      });

      // For storing the edit table information
      AdminSettingsQCReviewService.editTableData = [];

      // function AdminSettingsQCReviewService(){

      // }

      AdminSettingsQCReviewService.saveReviewTable = function(){
        return this.reviewTables.store(this.reviewTable);
      };

      AdminSettingsQCReviewService.deleteReviewTable = function(){
        return this.reviewTables.destroy({id:this.reviewTableId});
      };

      AdminSettingsQCReviewService.updateReviewTable = function(){
        return this.reviewTables.update(this.reviewTable);
      };

      AdminSettingsQCReviewService.cloneReview = function(){
        return this.reviewClone.store({id:this.reviewTableId});
      };

      AdminSettingsQCReviewService.updateReviewName = function(){
        return this.reviewTemplates.update(this.review);
      };

      AdminSettingsQCReviewService.deleteReview = function(){
        this.reviewTemplates.destroy({id:this.reviewID});
      };

      return AdminSettingsQCReviewService;
    }
  ]);