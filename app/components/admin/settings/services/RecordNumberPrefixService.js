'use strict';

/**
 * @ngdoc service
 * @name frontendApp.RecordNumberPrefix
 * @description
 * # RecordNumberPrefix
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('RecordNumberPrefixService', [

    function () {

      /**
       * RecordNumber prototype.
       *
       * @type {Function}
       * @api public
       */
      function NewRecordNumber(){
        this.label = undefined;
        this.startAt = undefined;
        this.nextValue = undefined;
        this.prefix = 1;
      }

      function RecordNumberPrefixService(opts){

        /**
         * Holds new RecordNumber
         *
         * @type {Object}
         * @api public
         */
        this.recordNumber = {};

        /**
         * Holds table data.
         *
         * @type {Array}
         * @api public
         */
        this.tableData = [];

        /**
         * Holds api resource
         *
         * @type {Object}
         * @api public
         */
        this.apiResource = opts.apiResource || {};

        /**
         * Holds isLoading
         *
         * @type {Boolean}
         * @api public
         */
        this.isLoading = false;

        /**
         * Holds tableState
         *
         * @type {Object}
         * @api public
         */
        this.tableState = {};

        /**
         * Per page options
         *
         * @type {Array}
         * @api public
         */
        this.perPageOptions = [
          {value: 5,innerHTML: '5'},
          {value: 10, innerHTML: '10'},
          {value: 15, innerHTML: '15'},
          {value: 20, innerHTML: '20'}
        ];

        /**
         * Holds how many rows per page
         *
         * @type {Number}
         * @api public
         */
        this.perPage = opts.perPage || 10;

        this.resetNewRecordNumber();
      }

      /**
       * Used with as-submit - resetting form after successful submission.
       *
       * @api public
       */
      RecordNumberPrefixService.prototype.autoClear = function(){
        this.resetNewRecordNumber();
      };

      /**
       * reset new RecordNumber.
       *
       * @api public
       */
      RecordNumberPrefixService.prototype.resetNewRecordNumber = function(){
        this.recordNumber = new NewRecordNumber();
      };

      /**
       * Save RecordNumber details
       *
       * @api public
       */
      RecordNumberPrefixService.prototype.saveNewRecordNumber = function(){
        var success, fail, self = this;

        // assign startAt to nextValue.
        this.recordNumber.nextValue = this.recordNumber.startAt;

        // query succeeded.
        success = function(response){

          // removing last array entry
          self.tableData.pop();

          // adding created file to first array cell
          self.tableData.unshift(response);

          // rebuilding no property.
          self.rebuildNumber();
        };

        // query failed.
        fail = function(){ };

        // sending query.
        return this.apiResource.store(this.recordNumber, success, fail).$promise;
      };


      /**
       * loading table data from server
       *
       * @param {Object} tableState
       * @api public
       */
      RecordNumberPrefixService.prototype.loadDataFromServer = function(tableState){

        // this function is being invoked twice in a row.
        if(this.isLoading) {
          return;
        }

        var params = {}, success, fail, start, number, self = this, tableData;

        // setting pagination start and per page
        start = tableState.pagination.start || 0;
        number = tableState.pagination.number || 10;

        // setting orderBy and order for sorting to sent to server
        if(tableState.sort.predicate){
          params.orderBy = tableState.sort.predicate;
          params.order   = tableState.sort.reverse? 'DESC': 'ASC';
        }

        // setting perPage and page to send to server
        params.perPage = number;
        params.page = Math.round(start / this.perPage + 1) || 1;

        // setting loading true to indicate some action happened
        this.isLoading = true;

        // query succeeded.
        success = function(response){
          console.log(response);
          tableData = [];
          response.data.map(function(row){
            var i = tableData.push(row);
            row.no = i+start;
          });
          self.tableData = tableData;
          tableState.pagination.numberOfPages = response.meta.pagination.total_pages; // jshint ignore:line
          self.isLoading = false;
        };

        // query failed.
        fail = function(){ self.isLoading = false; };

        // sending query.
        this.apiResource.getAll(params,success,fail);

        this.tableState = tableState;
      };

      /**
       * re build the property no for all rows.
       *
       * @api public
       * */
      RecordNumberPrefixService.prototype.rebuildNumber = function(){
        var i = 1, self = this;
        this.tableData.map(function(row){
          row.no = self.tableState.pagination.start+i;
          i++;
        });
      };

      return RecordNumberPrefixService;
    }
  ]);