'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AdminSettingsEmailService
 * @description
 * # AdminSettingsEmailService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AdminSettingsNotificationService', [
    '$resource', 'API_PREFIX',

    function ($resource, API_PREFIX) {

      var url = API_PREFIX() + '/v2.0/admin/notifications/:id'; 

      var notifications = $resource(url, {
        'id': '@id'
      }, {
      	getAll: {method: 'GET'},
      	store:  {method: 'POST'},
      	destroy: {method: 'DELETE'},
        update: {method: 'PUT'}
      });

      
      var notificationsService = {
        listing: [], //array of all notifications

        /** 
         * Find specified notifiaction in array
         * @param {Int} id
         * @return {Object} notification
         */
        findById: function (id) {
          var notification = null;

          angular.forEach(notificationsService.listing, function (val) {

            if(val.id === id) {
              notification = val;
              return;
            }
          });

          return notification;
        },

        /** 
         * Get all notifications
         * @return {Response} promise
         */
        getAll: function () {
          return notifications.getAll().$promise.then(function (res) {
            notificationsService.listing = res.data;

            return notificationsService.listing;
          });
        },

        /** 
         * Delete specified notification
         * @param {Int} id
         * @return {Response} promise
         */
        destroy: function (id) {

          return notifications.destroy({id: id}).$promise.then(function () {
            var notification = notificationsService.findById(id);
            var index = notificationsService.listing.indexOf(notification);

            notificationsService.listing.splice(index, 1);
          });
        },

        /** 
         * Update specified notification
         * @param {Object} notification
         * @return {Response} promise
         */
        update: function (notification) {

          return notifications.update({id: notification.id}, notification).$promise.then(function () {
            var oldNotification = notificationsService.findById(notification.id);
            var index = notificationsService.listing.indexOf(oldNotification);

            notificationsService.listing[index] = notification;
          });
        },

        /** 
         * Store new notification
         * @param {Object} notification
         * @return {Response} promise
         */
        store: function (notification) {
          return notifications.store(notification).$promise.then(function (res) {

            notificationsService.listing.push(res.data);
          });
        }
      };

      return notificationsService;
    }
  ]);