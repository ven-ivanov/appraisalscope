'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AdminSettingsInstructionsService
 * @description
 * # AdminSettingsOptionsService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AdminSettingsInstructionsService', [
    '$resource', 'API_PREFIX',

    function ($resource, API_PREFIX) {

      var prefix = API_PREFIX() + '/v2.0/admin/settings/instructions/';      

      var AdminSettingsInstructionsService = {};

      AdminSettingsInstructionsService.adminInstructionsDocuments = $resource(prefix + 'documents', {}, {
        getAll: {method: 'GET', isArray: true},
        update: {method: 'PUT'},
        store: {method: 'POST', isArray: false},
        destroy: {method: 'DELETE'}
      });     

      AdminSettingsInstructionsService.adminInstructionsForClient = $resource(prefix + 'instructions-for-clients', {}, {
        getAll: {method: 'GET', isArray: true},
        update: {method: 'PUT'},
        store: {method: 'POST', isArray: false},
        destroy: {method: 'DELETE'}
      });

      AdminSettingsInstructionsService.adminStates = $resource(API_PREFIX() + '/v2.0/handbook/states', {}, {
        getAll: {method: 'GET', isArray: false}      
      });
      
      AdminSettingsInstructionsService.adminJobTypes = $resource(API_PREFIX() + '/v2.0/admin/job-type', {}, {
        getAll: {method: 'GET', isArray: true}      
      });

      AdminSettingsInstructionsService.adminClientCompanies = $resource(API_PREFIX() + '/v2.0/client/companies', {}, {
        getAll: {method: 'GET', isArray: false}      
      });

      AdminSettingsInstructionsService.adminAssign = $resource(prefix + 'assign', {}, {
        update: {method: 'PUT'},     
      });

      return AdminSettingsInstructionsService;
    }
  ]);