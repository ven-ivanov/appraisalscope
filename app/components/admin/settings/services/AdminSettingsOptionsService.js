'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AdminSettingsOptionsService
 * @description
 * # AdminSettingsOptionsService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AdminSettingsOptionsService', [
    '$resource', 'API_PREFIX',

    function ($resource, API_PREFIX) {

      var prefix = API_PREFIX() + '/v2.0/admin/settings/options/';
      var jobType = API_PREFIX() + '/v2.0/admin/job-type/';

      var AdminSettingsOptionsService= {};

      AdminSettingsOptionsService.adminOptions = $resource(prefix + 'admin-options', {}, {
        getAll: {method: 'GET', isArray: false}
      });

      AdminSettingsOptionsService.availableOption = $resource(prefix + 'available-option', {}, {
        getAll: {method: 'GET', isArray: false}
      });

      AdminSettingsOptionsService.dueDateJobType = $resource(prefix + 'due-date-job-type', {}, {
        getAll: {method: 'GET', isArray: true},
        update: {method: 'PUT'}
      });

      AdminSettingsOptionsService.appraisalForm = $resource(prefix + 'appraisal-form', {}, {
        getAll: {method: 'GET', isArray: true},
        update: {method: 'PUT'}
      });

      AdminSettingsOptionsService.revisionType = $resource(prefix + 'revision-type', {}, {
        getAll: {method: 'GET', isArray: true},
        update: {method: 'PUT'},
        store: {method: 'POST', isArray: false},
        destroy: {method: 'DELETE'}
      });

      AdminSettingsOptionsService.localJobTypes = $resource(jobType, {}, {
        getAll: {method: 'GET', isArray: true},
        update: {method: 'PUT'}
      });

      AdminSettingsOptionsService.mercuryJobTypes = $resource(jobType, {}, {
        getAll: {method: 'GET', isArray: true}
      });

      AdminSettingsOptionsService.turnTimeRatings = $resource(prefix + 'turn-time-rating', {}, {
        getAll: {method: 'GET', isArray: false},
        update: {method: 'PUT'}
      });

      AdminSettingsOptionsService.manualSortOrder = $resource(prefix + 'manual-sort-order', {}, {
        getAll: {method: 'GET', isArray: true},
        update: {method: 'PUT'}
      });

      AdminSettingsOptionsService.autoSortOrder = $resource(prefix + 'auto-sort-order', {}, {
        getAll: {method: 'GET', isArray: true},
        update: {method: 'PUT'}
      });

      AdminSettingsOptionsService.deductionField = $resource(prefix + 'deduction-field', {}, {
        getAll: {method: 'GET', isArray: true},
        update: {method: 'PUT'}
      });

      AdminSettingsOptionsService.updates = $resource(prefix + 'updates', {}, {
        getAll: {method: 'GET', isArray: true},
        update: {method: 'PUT'}
      });

      AdminSettingsOptionsService.vendors = $resource(prefix + 'vendor', {}, {
        getAll: {method: 'GET', isArray: true},
        update: {method: 'PUT'},
        store: {method: 'POST', isArray: false},
        destroy: {method: 'DELETE'}
      });

      AdminSettingsOptionsService.termsAndConditions = $resource(prefix + 'terms-and-conditions', {}, {
        getAll: {method: 'GET', isArray: false},
        update: {method: 'PUT'}
      });

      return AdminSettingsOptionsService;
    }
  ]);