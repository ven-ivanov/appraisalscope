'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AdminSettingsPredefinedMessagesService
 * @description
 * # AdminSettingsOptionsService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AdminSettingsPredefinedMessagesService', [
    '$resource', 'API_PREFIX',

    function ($resource, API_PREFIX) {

      var prefix = API_PREFIX() + '/v2.0/admin/settings/predefinedMessages/';      

      var AdminSettingsPredefinedMessagesService = {};

      AdminSettingsPredefinedMessagesService.PredefinedMessages = $resource(prefix + 'predefinedMessage', {}, {
        getAll: {method: 'GET', isArray: false},
        update: {method: 'PUT'},
        destroy: {method: 'DELETE'},
        store: {method: 'POST', isArray: false}
      });

      AdminSettingsPredefinedMessagesService.PredefinedMessagesTypes = $resource(prefix + 'predefinedTypes', {}, {
        getAll: {method: 'GET', isArray: true}       
      });
      
      return AdminSettingsPredefinedMessagesService;
    }
  ]);