'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AdminSettingsUCDPService
 * @description
 * # AdminSettingsOptionsService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AdminSettingsUCDPService', [
    '$resource', 'API_PREFIX',

    function ($resource, API_PREFIX) {

      var prefix = API_PREFIX() + '/v2.0/admin/settings/ucdp/';      

      var AdminSettingsUCDPService = {};

      AdminSettingsUCDPService.adminUCDP = $resource(prefix + 'ucdp', {}, {
        getAll: {method: 'GET', isArray: false},
        update: {method: 'PUT'} 
      });

      return AdminSettingsUCDPService;
    }
  ]);