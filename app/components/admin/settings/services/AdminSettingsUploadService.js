'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AdminSettingsUploadService
 * @description
 * # AdminSettingsUploadService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AdminSettingsUploadService', [
    '$resource', 'API_PREFIX', 'FileUploader',

    function ($resource, API_PREFIX, FileUploader) {

      var clientImport = API_PREFIX() + '/v2.0/admin/settings/files/client-import/';
      var userImport = API_PREFIX() + '/v2.0/admin/settings/files/user-import/';
      var uploadDocument = API_PREFIX() + '/v2.0/admin/settings/files/document/';

      var additionalDocType = API_PREFIX() + '/v2.0/admin/settings/document-upload/additional-doc-type';
      var systemDocs = API_PREFIX() + '/v2.0/admin/settings/document-upload/system-documents';
      var templates = API_PREFIX() + '/v2.0/admin/settings/document-upload/templates';
      var sysTemplates = API_PREFIX() + '/v2.0/admin/settings/document-upload/system-templates';
      var templateTags = API_PREFIX() + '/v2.0/admin/settings/document-upload/tags';

      var AdminSettingsUploadService = {};

      AdminSettingsUploadService.additionalDocType = $resource(additionalDocType, {}, {
        getAll: {method: 'GET', isArray: false},
        update: {method: 'PUT'},
        destroy: {method: 'DELETE'}
      });

      AdminSettingsUploadService.systemDocs = $resource(systemDocs, {}, {
        getAll: {method: 'GET', isArray: false},
        destroy: {method: 'DELETE'}
      });

      AdminSettingsUploadService.templates = $resource(templates, {}, {
        getAll: {method: 'GET', isArray: false},
        update: {method: 'PUT'},
        destroy: {method: 'DELETE'}
      });

      AdminSettingsUploadService.sysTemplates = $resource(sysTemplates, {}, {
        getAll: {method: 'GET', isArray: false},
        update: {method: 'PUT'}
      });

      AdminSettingsUploadService.templateTags = $resource(templateTags, {}, {
        getAll: {method: 'GET', isArray: false}
      });

      /**
			 * Return rules for client CSV file
			 */
     	AdminSettingsUploadService.clientImportRules = function () {
     		return ['Company Name', 'Contact First Name', 'Contact Last Name', 'Email', 'Address', 'City', 'State', 'Zip','Phone'];
     	};

     	/**
			 * Return rules for user CSV file
			 */
     	AdminSettingsUploadService.userImportRules = function () {
     		return ['Company Name', 'First Name', 'Last Name', 'Username', 'Password', 'Email', 'User Type'];
     	};

			/**
			 * Upload client CSV that need to be imported
			 */
			AdminSettingsUploadService.importClient = function (file) {
			  return FileUploader.upload({
			    url: clientImport,
			    method: 'PUT',
			    file: file
			  });
			};

			/**
			 * Upload user CSV that need to be imported
			 */
			AdminSettingsUploadService.importUser = function (file) {
			  return FileUploader.upload({
			    url: userImport,
			    method: 'PUT',
			    file: file
			  });
			};

			/**
			 * Upload document
			 */
			AdminSettingsUploadService.uploadDoc = function (file) {
			  return FileUploader.upload({
			    url: uploadDocument,
			    method: 'PUT',
			    file: file
			  });
			};

     	return AdminSettingsUploadService;
    }
  ]);