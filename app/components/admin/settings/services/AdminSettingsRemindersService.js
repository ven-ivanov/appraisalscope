'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AdminSettingsRemindersService
 * @description
 * # AdminSettingsRemindersService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AdminSettingsRemindersService', [
    '$resource', 'API_PREFIX',

    function ($resource, API_PREFIX) {

      var availableOptions = API_PREFIX() + '/v2.0/admin/settings/reminders/options';
      var reminders = API_PREFIX() + '/v2.0/admin/settings/reminders/reminders';
      var appraisalForms = API_PREFIX() + '/v2.0/admin/settings/reminders/appraisal-forms/:id';
      var scheduler = API_PREFIX() + '/v2.0/admin/settings/reminders/scheduler';

      var AdminSettingsRemindersService = {};

      AdminSettingsRemindersService.availableOptions = $resource(availableOptions, {}, {
        getAll: {method: 'GET', isArray: false}
      });

      AdminSettingsRemindersService.reminders = $resource(reminders, {}, {
        getAll: {method: 'GET', isArray: false},
        update: {method: 'PUT'}
      });

      AdminSettingsRemindersService.appraisalForms = $resource(appraisalForms, {}, {
        getAll: {method: 'GET', isArray: true},
        update: {method: 'PUT'}
      });

      AdminSettingsRemindersService.scheduler = $resource(scheduler, {}, {
        getAll: {method: 'GET', isArray: false},
        update: {method: 'PUT'}
      });

      return AdminSettingsRemindersService;
    }
  ]);