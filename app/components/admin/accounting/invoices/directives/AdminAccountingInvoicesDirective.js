'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Accounting Invoices view
 */
app.directive('adminAccountingInvoices',
['DirectiveInheritanceService',
function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/accounting/invoices/directives/partials/invoice.html',
    controller: 'AdminAccountingInvoicesCtrl',
    controllerAs: 'tableCtrl',
    scope: true,
    link: function (scope) {
      // Import shared methods
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);