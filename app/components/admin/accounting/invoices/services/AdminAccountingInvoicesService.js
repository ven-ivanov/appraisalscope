'use strict';

var app = angular.module('frontendApp');

app.factory('AdminAccountingInvoiceService', ['$q', 'AdminAccountingDataService', 'AsDataService',
function ($q, AdminAccountingDataService, AsDataService) {
  var Service = {
    safeData: {
      // Invoices
      invoice: {},
      // Selected appraisal
      selectedAppraisal: '',
      // Selected records
      selectedRecords: [],
      // Company IDs of selected records
      companiesForEmail: {},
      // Table definition
      heading: [
        // Invoice number
        {
          label: 'Invoice Number',
          data: 'invoiceNumber',
          uc: true,
          isStatic: false
        },
        // Client name
        {
          label: 'Client',
          data: 'clientName'
        },
        // Generated date
        {
          label: 'Generated date',
          data: 'generatedDate'
        },
        // Invoice description
        {
          label: 'Description',
          data: 'description'
        },
        // Invoice amount
        {
          label: 'Amount',
          data: 'invoiceAmount',
          grandTotal: 'invoiceAmountGrandTotal',
          pageTotal: 'invoiceAmountPageTotal'
        },
        // Amount paid
        {
          label: 'Amount Paid',
          data: 'amountPaid',
          grandTotal: 'amountPaidGrandTotal',
          pageTotal: 'amountPaidPageTotal'
        },
        // Balance due
        {
          label: 'Balance Due',
          data: 'balanceDue',
          grandTotal: 'balanceDueGrandTotal',
          pageTotal: 'balanceDuePageTotal'
        },
        // Invoice status
        {
          label: 'Status',
          data: 'invoiceStatus'
        },
        // Multiselect
        {
          label: '',
          noSort: true,
          checkbox: true
        }
      ],
      // Transformers
      transformers: {
        table: {
          invoiceNumber: {
            source: 'payment.receivable.invoice.number'
          },
          clientName: {
            source: 'client.name'
          },
          generatedDate: {
            source: 'payment.receivable.invoice.createdAt',
            filter: 'date'
          },
          description: {
            source: 'payment.receivable.invoice.description'
          },
          invoiceAmount: {
            source: 'payment.receivable.amount.total',
            filter: 'currency'
          },
          amountPaid: {
            filter: 'currency',
            source: 'payment.receivable.amount.paid'
          },
          balanceDue: {
            filter: 'currency',
            source: 'payment.receivable.amount.due'
          },
          invoiceStatus: {
            // Determine payment status
            source: function (data) {
              var amount = data.payment.receivable.amount;
              if (!amount.due) {
                return 'Paid';
              } else if (!amount.paid) {
                return 'Unpaid';
              } else {
                return 'Partial payment';
              }
            }
          }
        },
        totals: {
          invoiceAmountGrandTotal: {
            filter: 'currency',
            source: 'summary.receivable.amount.total.total'
          },
          invoiceAmountPageTotal: {
            filter: 'currency',
            source: 'summary.receivable.amount.total.pageTotal'
          },
          amountPaidGrandTotal: {
            filter: 'currency',
            source: 'summary.receivable.amount.paid.total'
          },
          amountPaidPageTotal: {
            filter: 'currency',
            source: 'summary.receivable.amount.paid.pageTotal'
          },
          balanceDueGrandTotal: {
            filter: 'currency',
            source: 'summary.receivable.amount.due.total'
          },
          balanceDuePageTotal: {
            filter: 'currency',
            source: 'summary.receivable.amount.due.pageTotal'
          }
        }
      }
    },
    displayData: {
      // Invoices
      invoice: [],
      // Paid status filter
      paidStatusFilter: ['paid', 'unpaid', 'partial'],
      // Format (for emailing, etc)
      format: 'PDF',
      // Format options
      formatOptions: AdminAccountingDataService.displayData.formatOptions,
      // Email recipients
      emailRecipients: {},
      // Email options
      emailOptions: {}
    },
    transformers: {

    },
    /**
     * Update display on new page
     */
    pageLoadCallback: function (response) {
      // Update table
      Service.hashData(response.data, 'invoice');
      Service.formatData();
    },
    /**
     * Update which paid statuses should be included in the current result set
     * @param type
     */
    updatePaidStatus: function (type) {
      // Current status
      var paidStatusFilter = Service.displayData.paidStatusFilter;
      // Filter
      var filterIndex = paidStatusFilter.indexOf(type);
      // If the item isn't currently active, add it
      if (filterIndex === -1) {
        paidStatusFilter.push(type);
        // Remove item if currently active
      } else {
        paidStatusFilter.splice(filterIndex, 1);
      }
      // Update the table listing
      return Service.request('init', {page: 1, status: paidStatusFilter.join(',')});
    },
    /**
     * Delete selected invoices
     */
    deleteInvoices: function () {
      var records = Service.safeData.selectedRecords;
      // Send request
      return Service.request('deleteInvoices', {appraisals: records})
      .then(function () {
        angular.forEach(records, function (record) {
          // Delete and update table
          delete Service.safeData.invoice[record];
          Service.formatSingle(parseInt(record));
        });
      });
    },
    /**
     * Retrieve clients and emails
     *
     * @todo Still a work in progress
     * @link https://github.com/ascope/manuals/issues/290
     */
    getClientEmails: function () {
      Service.safeData.selectedCompanyIds = {};
      Service.displayData.emailOptions = {};
      // Get client company owners of each invoiced appraisal
      Service.getSelectedCompanies();
      // Get members
      return Service.request('members').then(function (response) {
        // Iterate and order into companies
        Lazy(response.data)
        .filter(Service.filterByClientCompany)
        .each(Service.getEmailOptions);
      });
    },
    /**
     * Retrieve company IDs from selected appraisals
     */
    getSelectedCompanies: function () {
      // Companies for emailing
      Service.safeData.companiesForEmail = {};
      var companiesForEmail = Service.safeData.companiesForEmail;
      var invoices = Service.safeData.invoice;
      var selectedAppraisals = Service.safeData.selectedRecords;
      // Iterate selected appraisals
      Lazy(selectedAppraisals)
      .each(function (appraisalId) {
        // Push each company into an object, with company ID as key and selected appraisals as array
        if (angular.isUndefined(companiesForEmail[invoices[appraisalId].client.id])) {
          companiesForEmail[invoices[appraisalId].client.id] = [parseInt(appraisalId)];
        } else {
          companiesForEmail[invoices[appraisalId].client.id].push(parseInt(appraisalId));
        }
        return invoices[appraisalId];
      });
    },
    /**
     * Filter members by target client companies
     */
    filterByClientCompany: function (record) {
      var companiesForEmail = Service.safeData.companiesForEmail;
      // Users
      if (angular.isDefined(record.company)) {
        return angular.isDefined(companiesForEmail[record.company.id]);
      } else {
        return angular.isDefined(companiesForEmail[record.id]);
      }
    },
    /**
     * Create email options from member records
     * @param record
     */
    getEmailOptions: function (record) {
      var emailOptions = Service.displayData.emailOptions;
      // Company
      if (angular.isUndefined(record.company)) {
        // Create initial object for this company
        if (!emailOptions[record.id]) {
          emailOptions[record.id] = {
            companyName: record.name,
            email: record.email,
            model: [],
            additionalRecipient: '',
            remarks: '',
            appraisals: angular.copy(Service.safeData.companiesForEmail[record.id]),
            users: []
          };
        }
        // User
      } else {
        // Create initial object
        if (!emailOptions[record.company.id]) {
          emailOptions[record.company.id] = {
            users: [{
              label: record.name,
              id: record.id
            }],
            companyName: record.company.name,
            email: record.company.email,
            model: [],
            additionalRecipient: '',
            remarks: '',
            appraisals: angular.copy(Service.safeData.companiesForEmail[record.company.id])
          };
        } else {
          // Push
          emailOptions[record.company.id].users.push({id: record.id, label: record.name});
        }
      }
    },
    /**
     * Send invoice email
     */
    sendEmail: function () {
      var body = {entries: [], format: Service.displayData.format};
      // Iterate options and create request body
      Lazy(Service.displayData.emailOptions)
      .each(function (record) {
        body.entries.push({
          appraisal: record.appraisals,
          receivers: Lazy(record.model).pluck('id').toArray(),
          email: record.additionalRecipient,
          remarks: record.remarks
        });
        return record;
      });
      // Send request
      return Service.request('sendEmail', body);
    },
    /**
     * Download invoices
     *
     * @todo This will likely change
     * @link https://github.com/ascope/manuals/issues/291
     */
    downloadInvoices: function () {
      // Post request
      return Service.request('downloadInvoices', {
        appraisals: Service.safeData.selectedRecords,
        format: Service.displayData.format
      });
    }
    ///**
    // * Export invoices
    // * @param params
    // */
    //exportInvoices: function (params) {
    //  return $http.post(API_PREFIX() + '/v2.0/admin/accounting/export-invoices/', params);
    //},
  };

  Service.request = AsDataService.request.bind(Service, 'AccountingResourceService', 'invoice');
  Service.loadRecords = AsDataService.loadRecords.bind(Service, {resourceService: 'AccountingResourceService', type: 'invoice'});
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'invoice');
  Service.formatSingle = AsDataService.formatSingle.bind(Service, 'invoice');
  Service.transformData = AsDataService.transformData.bind(Service);
  // Go to page
  Service.getRequestedPage = AdminAccountingDataService.getRequestedPage.bind(Service, Service.pageLoadCallback);
  // Change grouping
  Service.changeGrouping = AdminAccountingDataService.changeGrouping.bind(Service);
  // Formatting table
  Service.beforeInitFormat = AdminAccountingDataService.beforeInitFormat.bind(Service);
  // Get page and grand totals
  Service.getTotals = AdminAccountingDataService.getTotals.bind(Service);

  return Service;
}]);