describe('AdminAccountingInvoiceService', function () {
  var scope, httpBackend, Service, adminAccountingDataService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminAccountingDataService', AdminAccountingDataServiceMock);
    });
  });

  beforeEach(inject(function (AdminAccountingInvoiceService, $rootScope, $httpBackend, AdminAccountingDataService) {
    Service = AdminAccountingInvoiceService;
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    adminAccountingDataService = AdminAccountingDataService;

    spyOn(Service, 'hashData');
    spyOn(Service, 'formatData');
    spyOn(Service, 'request').and.callThrough();
    spyOn(Service, 'formatSingle');

    httpBackend.whenDELETE(/invoice/).respond();
    httpBackend.whenGET(/members/).respond({data: [1, 2]});
  }));

  describe('pageLoadCallback', function () {
    beforeEach(function () {
      Service.pageLoadCallback({data: []});
    });

    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should format data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('updatePaidStatus', function () {
    describe('remove', function () {
      beforeEach(function () {
        Service.displayData.paidStatusFilter = ['all', 'some'];
        Service.updatePaidStatus('some');
      });

      it('should update the paid status filter', function () {
        expect(Service.displayData.paidStatusFilter).toEqual(['all']);
      });
      it('should make the right request', function () {
        expect(Service.request).toHaveBeenCalledWith('init', {
          page: '?page=1',
          status: '&filter[payment][receivable][status]=all',
          grouping: ''
        });
      });
    });

    describe('add', function () {
      beforeEach(function () {
        Service.displayData.paidStatusFilter = ['all', 'some'];
        Service.updatePaidStatus('another');
      });

      it('should update the paid status filter', function () {
        expect(Service.displayData.paidStatusFilter).toEqual(['all', 'some', 'another']);
      });
      it('should make the right request', function () {
        expect(Service.request).toHaveBeenCalledWith('init', {
          page: '?page=1',
          status: '&filter[payment][receivable][status]=all,some,another',
          grouping: ''
        });
      });
    });
  });

  describe('deleteInvoices', function () {
    beforeEach(function () {
      Service.safeData.invoice = {
        1: {},
        2: {},
        3: {}
      };
      Service.safeData.selectedRecords = ['1', '2'];
      Service.deleteInvoices();
      httpBackend.flush();
      scope.$digest();
    });

    it('should make the right request', function () {
      expect(Service.request).toHaveBeenCalledWith('deleteInvoices', Object({appraisals: ['1', '2']}));
    });
    it('should update safe data', function () {
      expect(Service.safeData.invoice).toEqual({3: {}});
    });
    it('should format deleted records', function () {
      expect(Service.formatSingle).toHaveBeenCalled();
    });
  });

  describe('getClientEmails', function () {
    beforeEach(function () {
      spyOn(Service, 'getSelectedCompanies');
      Service.filterByClientCompany = function () {
        return true;
      };
      spyOn(Service, 'filterByClientCompany').and.callThrough();
      spyOn(Service, 'getEmailOptions');
      Service.getClientEmails();
      httpBackend.flush();
      scope.$digest();
    });

    it('should make the right request', function () {
      expect(Service.request).toHaveBeenCalledWith('members');
    });
    it('should call filter by client company', function () {
      expect(Service.filterByClientCompany).toHaveBeenCalled();
    });
    it('should call getEmailOptions', function () {
      expect(Service.getEmailOptions).toHaveBeenCalled();
    });
  });

  describe('getSelectedCompanies', function () {
    beforeEach(function () {
      Service.safeData.invoice = {
        1: {client: {id: 2}},
        2: {client: {id: 3}}
      };
      Service.safeData.selectedRecords = ['1', '2'];
      Service.getSelectedCompanies();
    });

    it('should not error out', function () {
      expect(Service.safeData.companiesForEmail).toEqual({
        2: [1],
        3: [2]
      });
    });
  });

  describe('filterByClientCompany', function () {
    var inputRecord, result;
    describe('user', function () {
      describe('found', function () {
        beforeEach(function () {
          inputRecord = {company: {id: 1}};
          Service.safeData.companiesForEmail = {1: {}};
          result = Service.filterByClientCompany(inputRecord);
        });

        it('should return true', function () {
          expect(result).toEqual(true);
        });
      });

      describe('not found', function () {
        beforeEach(function () {
          inputRecord = {company: {id: 1}};
          Service.safeData.companiesForEmail = {2: {}};
          result = Service.filterByClientCompany(inputRecord);
        });

        it('should return true', function () {
          expect(result).toEqual(false);
        });
      });
    });

    describe('company', function () {
      describe('found', function () {
        beforeEach(function () {
          inputRecord = {id: 1};
          Service.safeData.companiesForEmail = {1: {}};
          result = Service.filterByClientCompany(inputRecord);
        });

        it('should return true', function () {
          expect(result).toEqual(true);
        });
      });

      describe('not found', function () {
        beforeEach(function () {
          inputRecord = {id: 1};
          Service.safeData.companiesForEmail = {2: {}};
          result = Service.filterByClientCompany(inputRecord);
        });

        it('should return true', function () {
          expect(result).toEqual(false);
        });
      });
    });
  });

  describe('getEmailOptions', function () {
    var record;
    describe('company', function () {
      describe('new', function () {
        beforeEach(function () {
          Service.displayData.emailOptions = {};
          Service.safeData.companiesForEmail = {1: [1, 2]};
          record = {
            id: 1,
            name: 'Company',
            email: 'co@co.com'
          };
          Service.getEmailOptions(record);
        });

        it('should set a new company into email options', function () {
          expect(Service.displayData.emailOptions).toEqual({
            1: {
              companyName: 'Company',
              email: 'co@co.com',
              model: [],
              additionalRecipient: '',
              remarks: '',
              appraisals: [1, 2],
              users: []
            }
          });
        });
      });

      describe('old', function () {
        beforeEach(function () {
          Service.displayData.emailOptions = {1: {}};
          Service.safeData.companiesForEmail = {1: [1, 2]};
          record = {
            id: 1,
            name: 'Company',
            email: 'co@co.com'
          };
          Service.getEmailOptions(record);
        });

        it('should ignore a company that is already in the recipients object', function () {
          expect(Service.displayData.emailOptions).toEqual({1: {}});
        });
      });
    });

    describe('user', function () {
      describe('new', function () {
        beforeEach(function () {
          Service.displayData.emailOptions = {};
          Service.safeData.companiesForEmail = {1: [1, 2]};
          record = {
            id: 7,
            company: {
              id: 1,
              name: 'Co Co',
              email: 'co@co.com'
            },
            name: 'User',
            email: 'us@er.com'
          };
          Service.getEmailOptions(record);
        });

        it('should set a new company into email options', function () {
          expect(Service.displayData.emailOptions).toEqual({
            1: {
              users: [{
                label: 'User',
                id: 7
              }],
              companyName: 'Co Co',
              email: 'co@co.com',
              model: [],
              additionalRecipient: '',
              remarks: '',
              appraisals: [1, 2]
            }
          });
        });

        it('should add a user after one has already been added, without recreating company', function () {
          record = {
            id: 9,
            company: {
              id: 1,
              name: 'Co Co',
              email: 'co@co.com'
            },
            name: 'User2',
            email: 'us2@er.com'
          };
          Service.getEmailOptions(record);
          expect(Service.displayData.emailOptions).toEqual({
            1: {
              users: [{
                label: 'User',
                id: 7
              }, {
                id: 9,
                label: 'User2'
              }],
              companyName: 'Co Co',
              email: 'co@co.com',
              model: [],
              additionalRecipient: '',
              remarks: '',
              appraisals: [1, 2]
            }
          });
        });
      });
    });
  });

  describe('sendEmail', function () {
    beforeEach(function () {
      Service.displayData.emailOptions = {
        1: {
          users: [{
            label: 'User',
            id: 7
          }, {
            id: 9,
            label: 'User2'
          }],
          companyName: 'Co Co',
          email: 'co@co.com',
          model: [{id: 101}, {id: 201}],
          additionalRecipient: 'someone@else.com',
          remarks: 'A remark.',
          appraisals: [1, 2]
        },
        2: {
          users: [{
            label: 'User3',
            id: 44
          }, {
            id: 88,
            label: 'User4'
          }],
          companyName: 'Co Co2',
          email: 'co2@co.com',
          model: [{id: 100}, {id: 200}],
          additionalRecipient: 'someone@other.com',
          remarks: 'This is too!',
          appraisals: [3, 4]
        }
      };
      Service.displayData.format = 'pdf';
      Service.sendEmail();
    });

    it('should build the body correctly', function () {
      expect(Service.request.calls.argsFor(0)[1]).toEqual({
        entries: [{
          appraisal: [1, 2],
          receivers: [101, 201],
          email: 'someone@else.com',
          remarks: 'A remark.'
        }, {
          appraisal: [3, 4],
          receivers: [100, 200],
          email: 'someone@other.com',
          remarks: 'This is too!'
        }],
        format: 'pdf'
      });
    });
  });

  describe('downloadInvoices', function () {
    beforeEach(function () {
      Service.safeData.selectedRecords = [1,2];
      Service.displayData.format = 'pdf';
      Service.downloadInvoices();
    });

    it('should make the right request', function () {
      expect(Service.request.calls.argsFor(0)).toEqual([ 'downloadInvoices', { appraisals: [ 1, 2 ], format: 'pdf' } ]);
    });
  });
});