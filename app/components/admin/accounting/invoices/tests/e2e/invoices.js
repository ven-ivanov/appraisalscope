var backend = null,
    helpers = new global.UserHelper(),
    formHelper = new global.FormHelper(),
    loginHelper = new global.LoginHelper(),
    urlHelper = new global.UrlHelper(),
    helper = new global.Helper(),
    adminAccountingHelper = new global.AdminAccountingHelper();

describe('Admin Accounting -- Invoices', function () {
  // Allow GET requests
  beforeEach(function () {
    backend = new global.HttpBackend(browser);
    backend.whenGET(/.*/).passThrough();
  });

  // Set browser to maximum size
  beforeEach(function () {
    browser.driver.manage().window().maximize();
  });
  // Clear backend after each test
  afterEach(function() {
    backend.clear();
  });

  it('should allow the user to login', function () {
    // Login as admin
    loginHelper.login('admin', backend, urlHelper);
  });

  it('should go to the admin accounting section', function () {
    urlHelper.switchTab(2);
  });

  it('should visit the invoices subtab', function () {
    element(by.repeater('tab in adminAccountingTabsCtrl.tabs track by $index').row(2)).click();
    urlHelper.testUrl('accounting/invoices/1');
  });

  describe('delete invoices', function () {
    it('should delete selected invoices', function () {
      // The first invoice should be for appraisal 1
      adminAccountingHelper.matchCell(0, /1/);
      // Select the first 2 records
      $$('as-table tbody tr').get(0).$('input').click();
      $$('as-table tbody tr').get(1).$('input').click();
      // Click delete
      $$('.list-inline').get(4).$$('li').get(1).click();
      // Close the modal
      $('#delete-success').$('button').click();
      // Check to see if the table has been updated
      adminAccountingHelper.matchCell(0, /3/);
      // All checkboxes should now be unchecked
      $$('as-table tbody tr input').each(function (input) {
        expect(input.getAttribute('checked')).toBeFalsy();
      });
    });
  });

  describe('email', function () {
    it('should show error modal if no invoices are checked', function () {
      $$('.list-inline').get(4).$$('li').get(3).click();
      helper.browserWait($('#select-invoices').isDisplayed, function (displayed) {
        return displayed;
      });
      $('#select-invoices button').click();
    });

    it('should have two sections when two invoices are selected', function () {
      // Select first two invoices - now 3 and 4
      $$('as-table tbody tr').get(0).$('input').click();
      $$('as-table tbody tr').get(1).$('input').click();
      // Click email button
      $$('.list-inline').get(4).$$('li').get(3).click();
      // Wait for modal window to show
      helper.browserWait($('#email-invoice').isDisplayed, function (displayed) {
        return displayed;
      });
      // Make sure there are two sections
      var emailSections = element.all(by.repeater('(key, value) in tableCtrl.displayData.emailRecipients track by $index'));
      expect(emailSections.count()).toBe(2);
    });

    it('should not allow the user to send emails if recipients are not selected for each section', function () {
      // Initially disabled
      $('#email-invoice button[type="submit"]').click();
      // Make sure the "select at least one recipient per invoice" modal is shown
      helper.elementShown($('#email-failure-select'));
      // Hide modal
      $('#email-failure-select button').click();
      helper.elementHidden($('#email-failure-select'));
      // Show the first multi-select
      $$('#email-invoice .dropdown-toggle').get(0).click();
      // Click select all for the first dropdown multi-select
      $$('.dropdown-menu').get(2).$$('li').get(0).$('a').click();
      // Close the dropdown
      $$('#email-invoice .dropdown-toggle').get(0).click();
      // Check the submit button is still disabled
      // Initially disabled
      $('#email-invoice button[type="submit"]').click();
      // Make sure the "select at least one recipient per invoice" modal is shown
      helper.elementShown($('#email-failure-select'));
      // Hide modal
      $('#email-failure-select button').click();
      helper.elementHidden($('#email-failure-select'));
    });

    it('should allow the user to send the email when sections are complete', function () {
      // Open the second dropdown
      $$('#email-invoice .dropdown-toggle').get(1).click();
      // Click a single email
      $$('.dropdown-menu').get(3).$$('li').get(5).$('a').click();
      // Close second dropdown
      $$('#email-invoice .dropdown-toggle').get(1).click();
      // Check submit button
      // Initially disabled
      $('#email-invoice button[type="submit"]').click();
      // Make sure success modal is shown
      helper.elementShown($('#email-success'));
    });
  });
});
