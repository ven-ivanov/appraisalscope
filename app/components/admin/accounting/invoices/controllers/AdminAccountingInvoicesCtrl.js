'use strict';

var app = angular.module('frontendApp');

app.controller('AdminAccountingInvoicesCtrl',
['$scope', 'AdminAccountingDataService', 'AdminAccountingCtrlInheritanceService', 'AdminAccountingConfigService',
 'AdminAccountingInvoiceService', 'ngProgress', function (
$scope, AdminAccountingDataService, AdminAccountingCtrlInheritanceService, AdminAccountingConfigService,
AdminAccountingInvoiceService, ngProgress) {
  var vm = this;
  var Service = AdminAccountingInvoiceService;
  // Set up reference to current page
  vm.page = AdminAccountingDataService.page;
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;

  /**
   * Inherit from mock controller prototype
   */
  AdminAccountingCtrlInheritanceService.createCtrl.call(this, $scope, Service, 'invoice');

  /**
   * Get search models
   */
  AdminAccountingConfigService.getInvoicesSearchModels.call(this);

  /**
   * Individual selects for this view
   */
  _.assign(vm.selects, AdminAccountingConfigService.getInvoicesSelects());

  // Table definition
  vm.heading = vm.safeData.heading;

  /**
   * Update filter for paid status
   */
  vm.updatePaidStatusFilter = AdminAccountingInvoiceService.updatePaidStatus;

  /**
   * Disable a button due to no invoices being selected
   */
  vm.disableButtonNoneSelected = function () {
    return !vm.safeData.selectedRecords.length;
  };

  /**
   * Confirm delete invoices
   */
  vm.deleteConfirm = function () {
    $scope.$broadcast('show-modal', 'delete-confirm');
  };

  /**
   * Delete invoices
   */
  vm.deleteInvoices = function () {
    // Delete invoices
    AdminAccountingInvoiceService.deleteInvoices()
    .then(function () {
      // Hide confirm
      $scope.$broadcast('hide-modal', 'delete-confirm');
      // Show success modal, uncheck checkboxes
      $scope.$broadcast('show-modal', 'delete-success');
      // Uncheck all boxes
      vm.multiSelect = {};
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'delete-failure');
    });
  };

  /**
   * Disable send email modal
   */
  vm.sendEmailDisabled = function () {
    // Disable if invalid
    var disabled = vm.emailForm.$invalid;
    if (disabled) {
      return disabled;
    }
    // Else, disable if no emails selected
    angular.forEach(vm.displayData.emailOptions, function (client) {
      // No items selected in dropdown and no additional recipient
      if (!client.model.length && !client.additionalRecipient) {
        disabled = true;
      }
    });
    return disabled;
  };

  /**
   * Email invoices
   *
   * @todo Work in progress
   * @link https://github.com/ascope/manuals/issues/290
   */
  vm.emailConfirm = function () {
    // Get email recipients
    AdminAccountingInvoiceService.getClientEmails()
    .then(function () {
      // Show modal with selected invoices to send email
      $scope.$broadcast('show-modal', 'email-invoice');
    });
  };

  /**
   * Send email
   */
  vm.sendEmail = function () {
    AdminAccountingInvoiceService.sendEmail()
    .then(function () {
      // Show success
      $scope.$broadcast('hide-modal', 'email-invoice');
      $scope.$broadcast('show-modal', 'email-success');
    })
    .catch(function () {
      // Failure
      $scope.$broadcast('show-modal', 'email-failure');
    })
    .finally(function () {
      ngProgress.complete();
    });
  };

  /**
   * Download invoices
   */
  vm.downloadInvoices = function () {
    ngProgress.start();
    // Get download link
    AdminAccountingInvoiceService.downloadInvoices()
    .then(function (response) {
      // Trigger download
      $scope.triggerDownload(response.data.url);
    })
    .catch(function () {
      // Show failure modal
      $scope.$broadcast('show-modal', 'download-failure');
    })
    .finally(function () {
      ngProgress.complete();
    });
  };

  /**
   * Export invoices
   */
  vm.exportInvoices = function () {
    // Start loading bar
    ngProgress.start();
    // Get all search boxes
    var searchParams = AdminAccountingDataService.removeUnusedParams(vm.searchModels);
    // Send request
    AdminAccountingInvoiceService.exportInvoices({
      searchParams: searchParams,
      paidStatus: vm.displayData.paidStatusFilter,
      groupBy: vm.groupBy,
      order: vm.currentSort
    }).then(function (response) {
      // Trigger download
      $scope.triggerDownload(response.data.link);
    }, function () {
      // Trigger error modal
      $scope.$broadcast('show-modal', 'export-failure');
    }).finally(function () {
      ngProgress.complete();
    });
  };
}]);