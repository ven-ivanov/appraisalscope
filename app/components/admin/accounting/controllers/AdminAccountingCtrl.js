'use strict';

var app = angular.module('frontendApp');

/**
 * Admin accounting ctrl
 */
app.controller('AdminAccountingCtrl',
['$scope', '$stateParams', 'AdminAccountingDataService', '$state',
function ($scope, $stateParams, AdminAccountingDataService, $state) {
  var vm = this;

  /**
   * Redirect to unpaid if not tab is selected
   */
  if ($stateParams.activeTab === '') {
    $state.go('main.accounting', {activeTab: 'unpaid', page: 1});
    return;
  }

  // Initialize data
  vm.rowData = [];

  /**
   * Directives which should be loaded based on the selected tab
   */
  var directives = {
    unpaid: 'admin-accounting-unpaid',
    paid: 'admin-accounting-paid',
    invoices: 'admin-accounting-invoices',
    'appraiser-unpaid': 'admin-accounting-appraiser-unpaid',
    'pending-ach': 'admin-accounting-pending-ach',
    'appraiser-paid': 'admin-accounting-appraiser-paid',
    quickbooks: 'admin-accounting-quickbooks',
    commissions: 'admin-accounting-commissions',
    'aging-balance': 'admin-accounting-aging-balance',
    'transaction-summary': 'admin-accounting-transaction-summary'
  };

  // Set active tab
  AdminAccountingDataService.tab = vm.activeTab = $scope.activeTab = $stateParams.activeTab;
  // Set directive that should be loaded
  $scope.directive = directives[$scope.activeTab];
  // Set page for display
  AdminAccountingDataService.page.displayPage = parseInt($stateParams.page);
}]);