'use strict';

var app = angular.module('frontendApp');

app.factory('AccountingCtrlInheritanceImmutableService', [function () {
  var Service = {
    inherit: function ($scope, service, type) {
      var vm = this;
      /**
       * Watch for new records to populate the table
       */
      $scope.$watch(function () {
        return service.displayData[type];
      }, function (newVal) {
        if (angular.isUndefined(newVal) || !Immutable.Iterable.isIterable(newVal)) {
          return;
        }
        vm.tableData = newVal.toJS();
        vm.rowData = vm.tableData.slice();
      });
    }
  };
  return Service;
}]);