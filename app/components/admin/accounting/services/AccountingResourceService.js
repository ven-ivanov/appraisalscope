'use strict';

var app = angular.module('frontendApp');

/**
 * Service for constructing $resource URLs, parameters, methods, etc
 */
app.factory('AccountingResourceService',
['$resource', 'API_PREFIX', 'RequestMethodsService', '$stateParams',
 function ($resource, API_PREFIX, RequestMethodsService, $stateParams) {

   /**
    * Page and grouping
    * @param params Object
    *  page Int Page number
    *  grouping String Grouping (client, appraiser, all)
    *  status String Comma separates list of statuses
    * @returns String
    */
   var pageGroup = function (params) {
     params = params || {};
     // Get page
     params.page = params.page || parseInt($stateParams.page);
     // Make sure it's a valid page
     params.page = angular.isNumber(params.page) ? '?page=' + params.page : '?page=1';
     // Group data appropriately
     params.grouping = angular.isString(params.grouping) && params.grouping.length ? '&groupBy=' + params.grouping : '';
     // Status
     params.status = angular.isString(params.status) && params.status.length ? '&filter[payment][receivable][status]=' + params.status : '';
     return params.page + params.grouping + params.status;
   };

   /**
    * Accounting resources
    */
   var Service = {
     // Standard request URLs
     requests: {
       // Receivable unpaid
       receivableUnpaidQueue: '/v2.0/appraisals/queues/receivable-unpaid/:appraisalId/',
       receivablePayment: '/v2.0/appraisals/:appraisalId/payment/receivable/',
       // Receivable paid
       receivablePaidQueue: '/v2.0/appraisals/queues/receivable-paid/:appraisalId/',
       // Invoices queue
       invoicesQueue: '/v2.0/appraisals/queues/invoices/',
       // Payable unpaid queue
       payableUnpaidQueue: '/v2.0/appraisals/queues/payable-unpaid/',
       // Payable paid queue
       payablePaidQueue: '/v2.0/appraisals/queues/payable-paid/',
       // Payable payment
       payablePayment: '/v2.0/appraisals/:appraisalId/payment/payable/',
       // Pending ACH
       pendingAch: '/v2.0/appraisals/queues/pending-ach/',
       // Commissions
       commissions: '/v2.0/appraisals/queues/commissions/',
       // Aging
       aging: '/v2.0/agings/',
       // Transaction
       transaction: '/v2.0/appraisals/queues/transactions/'
     },
     // Standard params
     params: {
       // Params placeholder
       todo: function () {
         return {

         };
       }
     },
     /**
      * Unpaid view
      */
     unpaid: {
       /**
        * Data load
        */
       init: function (params) {
         return RequestMethodsService.getRequest(Service.requests.receivableUnpaidQueue + pageGroup(params));
       },
       /**
        * Mark a single appraisal as paid
        * @param params
        *
        * @todo This is not the real endpoint
        */
       markPaid: function (params) {
         return RequestMethodsService.post(Service.requests.receivablePayment + 'pay', params.url, params.body);
       },
       /**
        * Pay multiple appraisals
        *
        * @todo This is not the real endpoint
        */
       payMulti: function (appraisals, body) {
         return RequestMethodsService.post(Service.requests.receivablePayment + 'pay', {}, _.assign(appraisals, body));
       },
       /**
        * Generate invoice for one or more appraisals
        *
        * @todo Waiting on backend to accept multiple invoices
        * @link https://github.com/ascope/manuals/issues/271
        */
       generateInvoice: function (params) {
         return RequestMethodsService.post(Service.requests.receivablePayment + 'invoice', params);
       },
       /**
        * @todo This is fake, waiting on backend
        * @link https://github.com/ascope/manuals/issues/273
        */
       checkInvoiced: function (appraisalIds) {
         return RequestMethodsService.getRequest(Service.requests.receivablePayment + 'invoice/check', {appraisalId: appraisalIds});
       }
     },
     /**
      * Paid view
      */
     paid: {
       /**
        * Data load
        */
       init: function (params) {
         return RequestMethodsService.getRequest(Service.requests.receivablePaidQueue + pageGroup(params));
       },
       /**
        * Change status of appraisal to unpaid
        */
       markUnpaid: function (appraisalId) {
         return RequestMethodsService.patch(Service.requests.receivablePayment, appraisalId);
       },
       /**
        * Rebill appraisal
        */
       rebill: function (appraisalId, params) {
         return RequestMethodsService.post(Service.requests.receivablePayment + 'rebill', appraisalId, params);
       },
       /**
        * Refund appraisal
        */
       refund: function (appraisalId, params) {
         return RequestMethodsService.post(Service.requests.receivablePayment + 'refund', appraisalId, params);
       }
     },
     /**
      * Invoices view
      */
     invoice: {
       /**
        * Data load
        */
       init: function (params) {
         return RequestMethodsService.getRequest(Service.requests.invoicesQueue + pageGroup(params));
       },
       /**
        * Delete selected invoices
        * @param params Object Selected appraisal IDs as array
        */
       deleteInvoices: function (params) {
         return RequestMethodsService.deleteRequest(Service.requests.receivablePayment + 'invoice', {}, params);
       },
       /**
        * Download selected invoices
        * @param params Appraisals, format
        *
        * @todo This is incomplete on the backend
        */
       downloadInvoices: function (params) {
         return RequestMethodsService.getRequest(Service.requests.receivablePayment +
                                                 'invoice/download?appraisals=[' +
                                                 params.appraisals.join(',') + ']&format=' +
                                                 params.format.toLowerCase(), {}, params);
       },
       /**
        * Get client members to retrieve companies and employees
        *
        * @todo This is going to change
        * @link https://github.com/ascope/manuals/issues/290
        */
       members: function () {
         return RequestMethodsService.getRequest('/v2.0/client/members');
       },
       /**
        * Email invoices
        */
       sendEmail: function (body) {
         return RequestMethodsService.post(Service.requests.receivablePayment + 'invoice/email', {}, body);
       }
     },
     /**
      * Appraiser unpaid view
      */
     appraiserUnpaid: {
       /**
        * Data load
        * @param params (page, grouping)
        */
       init: function (params) {
         return RequestMethodsService.getRequest(Service.requests.payableUnpaidQueue + pageGroup(params));
       },
       /**
        * Pay appraiser
        * @param params
        */
       payAppraiser: function (params) {
         return RequestMethodsService.post(Service.requests.payablePayment + 'pay', params.url, params.body);
       }
     },
     /**
      * Appraiser paid view
      */
     appraiserPaid: {
       /**
        * Data load
        */
       init: function (params) {
         return RequestMethodsService.getRequest(Service.requests.payablePaidQueue + pageGroup(params));
       },
       /**
        * Mark appraisal as unpaid
        * @param params
        */
       markUnpaid: function (params) {
         return RequestMethodsService.patch(Service.requests.payablePayment, params.url, params.body);
       }
     },
     /**
      * Pending ACH
      */
     pendingAch: {
       /**
        * Data load
        * @param params (page, grouping)
        */
       init: function (params) {
         return RequestMethodsService.getRequest(Service.requests.pendingAch + pageGroup(params));
       }
     },
     /**
      * Quickbooks view
      */
     quickbooks: {

     },
     /**
      * Commissions view
      */
     commissions: {
       /**
        * Data load
        * @param params (page, grouping)
        */
       init: function (params) {
         return RequestMethodsService.getRequest(Service.requests.commissions + pageGroup(params));
       },
       /**
        * Delete commissions
        */
       deleteCommissions: function (id) {
         return RequestMethodsService.deleteRequest(Service.requests.commissions + id);
       }
     },
     /**
      * Aging view
      */
     aging: {
       init: function (params) {
         // Receivable or payable
         var type = angular.isObject(params) && params.grouping === 'appraisers' ? 'appraisers' : 'clients';
         return RequestMethodsService.getRequest(Service.requests.aging + type + '/' + pageGroup(params));
       }
     },
     /**
      * Transaction view
      */
     transaction: {
       /**
        * Data load
        * @param params (page, grouping)
        */
       init: function (params) {
         return RequestMethodsService.getRequest(Service.requests.transaction + pageGroup(params));
       }
     }
   };
   return Service;
 }]);