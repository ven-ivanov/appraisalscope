// jshint ignore: start
// @todo Remove jshint cheat above once this solidifies
'use strict';

var app = angular.module('frontendApp');

app.factory('AdminAccountingCtrlInheritanceService',
['AdminAccountingDataService', 'HandbookService', 'HANDBOOK_CATEGORIES', '$stateParams', '$timeout', '$q', 'ngProgress',
 'AccountingCtrlInheritanceImmutableService', function (
AdminAccountingDataService, HandbookService, HANDBOOK_CATEGORIES, $stateParams, $timeout, $q, ngProgress,
AccountingCtrlInheritanceImmutableService) {
  return {
    /**
     * Mock prototyping without violating global scope
     *
     * Common items that controllers in the admin accounting section can inherit
     */
    createCtrl: function ($scope, service, type) {
      var vm = this;
      // Group by
      vm.groupBy = 'All';
      // Records per page select element
      vm.selects = {};

      /**
       * Initiate data load on load
       */
      vm.init = function () {
        service.loadRecords().then(function () {
          // Get page and grand totals
          if (angular.isFunction(service.getTotals)) {
            service.getTotals();
          }
        });
      };

      /**
       * Function for switching pages
       */
      vm.getRequestedPage = service.getRequestedPage;

      /**
       * Submit a single payment
       */
      vm.markPaid = function () {
        // Pay single record
        service.markPaid()
        .then(function () {
          $scope.$broadcast('hide-modal', 'pay-single-modal');
          $scope.$broadcast('show-modal', 'pay-single-successful');
          // Uncheck all multiselect
          AdminAccountingDataService.uncheckRecords(vm.multiSelect);
        })
        .catch(function () {
          // Display failure modal
          $scope.$broadcast('hide-modal', 'pay-single-modal');
          $scope.$broadcast('show-modal', 'pay-single-failure');
        });
      };

      /**
       * Show the pay multi modal
       */
      vm.showPayMultiModal = function () {
        // Clear the modal when it's closed
        $scope.clearModalOnClose('pay-multi-modal', 'payMulti', ['number', 'date']);
        // Make sure we have records selected
        if (service.safeData.selectedRecords.length) {
          // Get balance due value
          service.getBalanceDue();
          // Display modal
          $scope.$broadcast('show-modal', 'pay-multi-modal');
          // Show "please select records" modal
        } else {
          $scope.$broadcast('show-modal', 'select-records');
        }
      };
      /**
       * Pay multi
       */
      vm.payMulti = function () {
        // Post to API
        service.payMulti()
        .then(function () {
          // Show success modal
          $scope.$broadcast('hide-modal', 'pay-multi-modal');
          $scope.$broadcast('show-modal', 'pay-multi-successful');
        })
        .finally(function () {
          // Uncheck all multiselect
          AdminAccountingDataService.uncheckRecords(vm.multiSelect);
        })
        .catch(function () {
          // Show failure modal
          $scope.$broadcast('hide-modal', 'pay-multi-modal');
          $scope.$broadcast('show-modal', 'pay-multi-failure');
        });
      };

      /**
       * Create exported document, then trigger download
       *
       * @todo Waiting on backend to create the export functionality
       */
      vm.exportData = function () {
        ngProgress.start();
        // Create search params
        var params = {
          groupBy: vm.groupBy
        };
        // Remove empty search parameters
        params = AdminAccountingDataService.removeUnusedParams(params, vm.searchModels);
        // Create the download link
        AdminAccountingDataService.getExportDataLink(params)
        .then(function (response) {
          // Trigger download
          $scope.triggerDownload(response.data);
        })
        .catch(function () {
          // On error, show modal to let the user know
          $scope.$broadcast('show-modal', 'export-data-error');
        })
        .finally(function () {
          ngProgress.complete();
        });
      };

      /**
       * Import checks modal
       */
      vm.importChecksConfirm = function () {
        $scope.$broadcast('show-modal', 'import-checks-modal');
      };
      /**
       * Import checks
       *
       * @todo Waiting on backend
       * @link https://github.com/ascope/manuals/issues/258
       */
      vm.importChecks = function () {

      };

      /**
       * Watch data for page and grand totals
       */
      $scope.$watchCollection(function () {
        return service.displayData.meta;
      }, function (newVal) {
        if (angular.isArray(newVal) || !angular.isObject(newVal) || !Object.keys(newVal).length) {
          return;
        }
        // Set grand total and page total
        vm.grandTotal = vm.pageTotal = newVal;
        // Make sure we get pagination data
        if (angular.isObject(newVal.pagination)) {
          // Get pagination data
          vm.pages = angular.isNumber(newVal.pagination.totalPages) ? newVal.pagination.totalPages : null;
        } else {
          throw Error('Unable to retrieve pagination data');
        }
      });

      /**
       * Watch for new records to populate the table
       */
      if (!service.immutable) {
        $scope.$watchCollection(function () {
          return service.displayData[type];
        }, function (newVal) {
          if (angular.isUndefined(newVal)) {
            return;
          }
          vm.tableData = newVal;
          vm.rowData = vm.tableData.slice();
        });
      } else {
        // Immutable inheritance
        AccountingCtrlInheritanceImmutableService.inherit.call(vm, $scope, service, type);
      }

      /**
       * Change grouping
       */
      $scope.$watch(function () {
        return vm.groupBy;
      }, function (newVal, oldVal) {
        // Change grouping
        if (!angular.equals(newVal, oldVal)) {
          // Display all data
          if (newVal === 'All') {
            service.changeGrouping(null);
            // If a particular grouping is used
          } else {
            service.changeGrouping(newVal.toLowerCase());
          }
          // Always go back to page 1
          vm.page.displayPage = 1;
        }
      });

      /**
       * Keep reference to multiselect on service
       */
      $scope.$watchCollection(function () {
        return vm.multiSelect;
      }, function (newVal) {
        if (angular.isUndefined(newVal)) {
          return;
        }
        // Keep reference to selected records on service
        service.safeData.selectedRecords = _.map(newVal, function (record, key) {
          if (record) {
            return key;
          }
          // Remove de-selected items
        }).filter(function (record) {
          return record;
        });
      });

      /**
       * Init the entire controller
       */
      vm.init();

      return;

      // The highest numbered page that is displayed currently
      vm.pagesDisplayed = 1;
      // Current sorting
      vm.currentSort = {
        type: 'sNo',
        reverse: false
      };
      // The number of records to display at any given time
      vm.recordsToDisplay = 30;
      // Number of pages of data for this view
      vm.pages = 0;

      /**
       * Retrieve data when it's ready
       */
      AdminAccountingDataService.registerCallback(function () {
        vm.getRowData();
      });

      /**
      * Get data needed for this view
      */
      vm.getRowData = function () {
        // Get totals for this set of data
        vm.pageTotal = AdminAccountingDataService.getPageTotal(AdminAccountingDataService.safeData, totals);
        // Set row data/table data
        vm.rowData = AdminAccountingDataService.data;
        vm.tableData = [].concat(vm.rowData);
      };

      /**
       * Update selected records on multi-select click
       */
      $scope.$watchCollection(function () {
        return vm.multiSelect;
      }, function (newVal) {
        vm.selectedRecords = [];
        angular.forEach(newVal, function (select, key) {
          if (select) {
            vm.selectedRecords.push(parseInt(key));
          }
        });
      });

      /**
       * Retrieve states for dropdown
       */
      HandbookService.query({category: HANDBOOK_CATEGORIES.states}).$promise
      .then(function (data) {
        vm.selects.state = data.data;
      });

      /**
       * Retrieve the data of the next page
       */
      vm.nextPage = function () {
        // Whether to change URL
        var changeUrl = !arguments.length || arguments[0];
        return $q(function (resolve, reject) {
          // Whether to slice existing data
          var sliceData = false;
          /**
           * @todo Update when we have a real backend
           */
          if (AdminAccountingDataService.paginationInProgress || vm.groupBy !== 'All') {
            return;
          }
          AdminAccountingDataService.page.page = AdminAccountingDataService.page.page + 1;
          //// Set pagination in progress
          //AdminAccountingDataService.paginationInProgress = true;
          // Get next page data
          AdminAccountingDataService.getPage(AdminAccountingDataService.page.page)
          .then(function (data) {
            // Get number of pages displayed
            vm.pagesDisplayed = AdminAccountingDataService.pagesDisplayed(vm.pagesDisplayed);
            // Don't allow more than 30 records to be in memory at any given time
            if (data.data.length + AdminAccountingDataService.safeData.length > vm.recordsToDisplay) {
              // Slice safe data
              sliceData = true;
              AdminAccountingDataService.safeData = AdminAccountingDataService.safeData.slice(data.data.length - 1);
            }
            // Create safe data copy
            AdminAccountingDataService.safeData = AdminAccountingDataService.safeData.concat(JSON.parse(JSON.stringify(data.data)));
            // Apply filters to the new data
            AdminAccountingDataService.applyFilters(data.data).then(function () {
              // Slice display data
              if (sliceData) {
                // Remove top rows when loading in more data than we want to display
                vm.rowData = vm.rowData.slice(data.data.length);
                // Handle scroll position when slicing data
                $scope.scrollOnLoad(data.data);
              }
              // Concat the filtered data
              vm.rowData = vm.rowData.concat(data.data);
              // Set table data
              AdminAccountingDataService.setData(vm.rowData);
              // Recalculate page totals from unfiltered data
              vm.pageTotal = AdminAccountingDataService.getPageTotal(AdminAccountingDataService.safeData, totals);
              // Allow pagination to resume
              AdminAccountingDataService.paginationInProgress = false;
              // Transition to the correct page without reloading state
              if (changeUrl) {
                AdminAccountingDataService.changeUrl(AdminAccountingDataService.page.page);
              }
              resolve(data.data);
            });
          })
          .catch(function () {
            reject();
          });
        });
      };

      /**
       * Retrieve previous page on scroll up
       */
      vm.prevPage = function () {
        // Whether to change URL on load
        var changeUrl = !arguments.length || arguments[0];
        return $q(function (resolve, reject) {
          var sliceData = false;
          // Don't go back beyond page 1
          if (AdminAccountingDataService.paginationInProgress || vm.groupBy !== 'All' ||
              AdminAccountingDataService.page.page - vm.pagesDisplayed < 1) {
            return;
          }
          // Set pagination in progress
          AdminAccountingDataService.paginationInProgress = true;
          // Get next page data
          AdminAccountingDataService.getPage(AdminAccountingDataService.page.page - vm.pagesDisplayed).then(function (data) {
            // if we're at the max number of pages, decrement top page by 1
            if (vm.pagesDisplayed === AdminAccountingDataService.maxPages) {
              AdminAccountingDataService.page.page = AdminAccountingDataService.page.page - 1;
            }
            // Get number of pages displayed
            vm.pagesDisplayed = AdminAccountingDataService.pagesDisplayed(vm.pagesDisplayed);
            // Don't allow more than 40 records to be in memory at any given time
            if (data.data.length + AdminAccountingDataService.safeData.length > vm.recordsToDisplay) {
              // Slice safe data
              sliceData = true;
              AdminAccountingDataService.safeData = AdminAccountingDataService.safeData.slice(0, vm.recordsToDisplay);
            }
            // Create safe data copy, place new data onto beginning
            AdminAccountingDataService.safeData = JSON.parse(JSON.stringify(data.data)).concat(AdminAccountingDataService.safeData);
            // Apply filters to the new data
            AdminAccountingDataService.applyFilters(data.data).then(function () {
              // Slice display data
              if (sliceData) {
                // Remove top rows when loading in more data than we want to display
                vm.rowData = vm.rowData.slice(0, vm.rowData.length - data.data.length);
                // Handle scroll position when slicing data
                $scope.scrollOnLoad(data.data, 'up');
              }
              // Concat the filtered data
              vm.rowData = data.data.concat(vm.rowData);
              // Set table data
              AdminAccountingDataService.setData(vm.rowData);
              // Recalculate page totals from unfiltered data
              vm.pageTotal = AdminAccountingDataService.getPageTotal(AdminAccountingDataService.safeData, totals);
              // Allow pagination to resume
              AdminAccountingDataService.paginationInProgress = false;
              // Transition to the correct page without reloading state
              if (changeUrl) {
                AdminAccountingDataService.changeUrl(AdminAccountingDataService.page);
              }
              resolve(data.data);
            });
          })
          .catch(function () {
            reject();
          });
        });
      };

      /**
       * Retrieve the current page if a specific page is chosen
       */
      vm.getCurrentPage = function (page) {
        return $q(function (resolve, reject) {
          // Hold reference to this page
          AdminAccountingDataService.page.page = page;
          // Set pagination in progress
          AdminAccountingDataService.paginationInProgress = true;
          // Get the requested page
          AdminAccountingDataService.getPage(page)
          .then(function (data) {
            // Only a single page should be displayed
            vm.pagesDisplayed = 1;
            // Set safe data
            AdminAccountingDataService.safeData = JSON.parse(JSON.stringify(data.data));
            // Apply filters
            AdminAccountingDataService.applyFilters(data.data)
            .then(function () {
              // Concat the filtered data
              vm.rowData = data.data;
              // Set table data
              AdminAccountingDataService.setData(vm.rowData);
              // Recalculate page totals from unfiltered data
              vm.pageTotal = AdminAccountingDataService.getPageTotal(AdminAccountingDataService.safeData, totals);
              // Allow pagination to resume
              AdminAccountingDataService.paginationInProgress = false;
              // Transition to the correct page without reloading state
              AdminAccountingDataService.changeUrl(AdminAccountingDataService.page.page);
              // Resolve promise
              resolve();
            });
          })
          .catch(function () {
            reject();
          });
        });
      };

      /**
       * Resort data
       * @param sortParam
       */
      vm.sort = function (sortParam) {
        // New sorting method
        if (vm.currentSort.type !== sortParam) {
          vm.currentSort.type = sortParam;
          vm.currentSort.reverse = false;
          // Reverse current method
        } else {
          vm.currentSort.reverse = !vm.currentSort.reverse;
        }
        AdminAccountingDataService.sort(vm.currentSort)
        .then(function (sortedData) {
          // Filter returned data
          return AdminAccountingDataService.applyFilters(sortedData);
        })
        .then(function (sortedData) {
          vm.rowData = sortedData;
          // Enable pagination
          AdminAccountingDataService.paginationInProgress = false;
        });
      };

      /**
       * Reset each of the search inputs
       */
      vm.clear = function () {
        angular.forEach(vm.searchModels, function (searchVal, searchKey) {
          // Numeric (drop downs)
          if (angular.isNumber(searchVal)) {
            vm.searchModels[searchKey] = 0;
            // String (user input)
          } else if (angular.isString(searchVal)) {
            vm.searchModels[searchKey] = '';
            // Date (datepicker)
          } else if (angular.isDate(searchVal)) {
            vm.searchModels[searchKey] = new Date();
            // Unexpected value
          } else {
            throw Error('Something went wrong');
          }
        });
      };

      /**
       * Search function
       */
      vm.search = function () {
        AdminAccountingDataService.search(AdminAccountingDataService.tab, vm.searchModels)
        .then(function (response) {
          console.log(response);
        })
        .catch(function (err) {
          console.log(err);
        });
      };

      /**
       * Upload CSV of checks to import
       * @param file
       */
      vm.uploadCsv = function (file) {
        // This is triggered on change, so ensure that a file is being input
        if (!file) {
          return;
        }
        // Import checks via upload service
        AdminAccountingDataService.importChecks(file).promise
        .then(function () {
          // Eventual response
          $scope.$broadcast('hide-modal', 'import-checks-modal');
          $scope.$broadcast('show-modal', 'import-checks-successful');
        })
        .catch(function () {
          // This is going to be an error until we have the backend
          $scope.$broadcast('hide-modal', 'import-checks-modal');
          $scope.$broadcast('show-modal', 'import-checks-failure');
        });
      };

      /**
       * Fetch next page on load to prevent the user from being stuck on a single page
       * The problem is when the user loads a page on full-screen, it could potentially take up less than the entire
       * screen width, in which case they can't scroll to load more results
       */
      $timeout(function () {
        // Retrieve next two pages
        vm.nextPage(false)
        .then(function () {
          vm.nextPage(false);
        });
      }, 500);
    }
  };
}]);