'use strict';

var app = angular.module('frontendApp');

app.factory('AdminAccountingConfigService', [function () {
  return {
    /**
     * Return an object with all of the table data column headers
     *
     * @link - https://github.com/ascope/manuals/blob/master/Developer's%20Guide/Guidelines/Front-end/components/as-table.md
     *
     * @returns Object
     */
    getModelHeaders: function () {
      return {
        // S Number: File number?
        sNo: {
          label: 'S Number',
          data: 'sNo',
          filter: 'number'
        },
        // Appraiser name
        appraiser: {
          label: 'Appraiser',
          data: 'appraiserName'
        },
        // Appraisal company
        appraisalCompany: {
          label: 'Appraisal Company',
          data: 'appraisalCompany'
        },
        // State
        state: {
          label: 'State',
          data: 'state'
        },
        // File number
        file: {
          label: 'File Number',
          data: 'file',
          uc: true
        },
        // Client name
        client: {
          label: 'Client',
          data: 'client'
        },
        // Borrower name
        borrower: {
          label: 'Borrower',
          data: 'borrower'
        },
        // Borrower address
        address: {
          label: 'Address',
          data: 'address'
        },
        // Order date
        orderDate: {
          label: 'Order Date',
          data: 'orderDate',
          filter: 'date:M/d/yyyy'
        },
        // Completed data
        completedDate: {
          label: 'Completed Date',
          data: 'completedDate',
          filter: 'date:M/d/yyyy'
        },
        // Client fee
        clientFee: {
          label: 'Client Fee',
          data: 'clientFee',
          filter: 'currency'
        },
        // Amount paid
        amountPaid: {
          label: 'Amount Paid',
          data: 'amountPaid',
          filter: 'currency'
        },
        // Balance due -- not static, can be updated
        balanceDue: {
          label: 'Balance Due',
          data: 'balanceDue',
          filter: 'currency',
          isStatic: false
        },
        // Application fee
        appFee: {
          label: 'Application Fee',
          data: 'appFee',
          filter: 'currency'
        },
        // P/L Amount
        plAmount: {
          label: 'P/L Amount',
          data: 'plAmount',
          filter: 'currency'
        },
        // Invoice number
        invoiceNumber: {
          label: 'Invoice Number',
          data: 'invoiceNumber',
          uc: true,
          isStatic: false
        },
        // Check number
        checkNumber: {
          label: 'Check Number',
          data: 'checkNumber',
          uc: true
        },
        // Appraisal paid date
        paidDate: {
          label: 'Paid Date',
          data: 'paidDate',
          filter: 'date:M/d/yyyy'
        },
        // Invoice amount
        invoiceAmount: {
          label: 'Invoice Amount',
          data: 'invoiceAmount',
          filter: 'currency'
        },
        // Invoice status
        invoiceStatus: {
          label: 'Invoice Status',
          data: 'invoiceStatus',
          uc: true
        },
        // Generated date
        generatedDate: {
          label: 'Generated Date',
          data: 'generatedDate',
          filter: 'date:M/d/yyyy'
        },
        invoiceDescription: {
          label: 'Invoice Description',
          data: 'invoiceDescription'
        },
        salesPerson: {
          label: 'Sales Person',
          data: 'salesPerson'
        },
        jobType: {
          label: 'Job Type',
          data: 'jobType'
        },
        commission: {
          label: 'Commission',
          data: 'commission',
          filter: 'currency'
        },
        /**
         * Quickbooks
         */
        qbQueueId: {
          label: 'Queue ID',
          data: 'qbQueueId'
        },
        qbAction: {
          label: 'QB Action',
          data: 'qbAction'
        },
        qbIdent: {
          label: 'Identity',
          data: 'qbIdent'
        },
        qbReqData: {
          label: 'Request Data',
          data: 'qbReqData'
        },
        qbPriority: {
          label: 'Priority',
          data: 'qbPriority'
        },
        qbStatus: {
          label: 'Status',
          data: 'qbStatus'
        },
        qbMsg: {
          label: 'Message',
          data: 'qbMsg'
        },
        action: {
          label: 'Action',
          data: 'action'
        },
        // Current value for aging balance table
        current: {
          label: 'Current',
          data: 'current',
          filter: 'currency'
        },
        over30: {
          label: 'Over 30',
          data: 'over30',
          filter: 'currency'
        },
        over60: {
          label: 'Over 60',
          data: 'over60',
          filter: 'currency'
        },
        over90: {
          label: 'Over 90',
          data: 'over90',
          filter: 'currency'
        },
        /**
         * Transaction Summary Placeholder
         */
        orderedFor: {
          label: 'Ordered For',
          data: 'orderedFor'
        },
        transactionDate: {
          label: 'Transaction Date',
          data: 'transactionDate',
          filter: 'date:M/d/yyyy'
        },
        referenceNumber: {
          label: 'Reference Number',
          data: 'referenceNumber'
        },
        creditCardNumber: {
          label: 'Credit Card #',
          data: 'creditCardNumber'
        },
        paymentType: {
          label: 'Payment Type',
          data: 'paymentType'
        },
        credit: {
          label: 'Credit',
          data: 'credit',
          filter: 'currency'
        },
        debit: {
          label: 'Debit',
          data: 'debit',
          filter: 'currency'
        },
        transactionRemarks: {
          label: 'Remarks',
          data: 'transactionRemarks'
        },
        // Standard multi-select checkbox
        'checkbox:multi-select': {
          label: 'Multi-select',
          noSort: true,
          checkbox: true
        },
        // Mark as paid link function
        'fn:mark-as-paid': {
          label: 'Mark as Paid',
          noSort: true,
          linkLabel: 'Mark as Paid',
          fn: 'markAsPaid'
        },
        // Mark as paid link function
        'fn:mark-as-unpaid': {
          label: 'Mark as Unpaid',
          noSort: true,
          linkLabel: 'Mark as unpaid',
          fn: 'markAsUnpaid'
        },
        // Change status link function
        'fn:change-status': {
          label: 'Change Status',
          noSort: true,
          linkLabel: 'Change Status',
          fn: 'changeStatus'
        },
        // Pay appraiser link function
        'fn:pay-appraiser': {
          label: 'Action',
          noSort: true,
          linkLabel: 'Pay',
          fn: 'payAppraiser'
        }
      };
    },
    /**
     * Client branch select values
     */
    clientBranch: [
      {
        value: ''
      },
      {
        value: 'Branch 1'
      },
      {
        value: 'Branch 2'
      }
    ],
    /**
     * Appraiser company select values
     */
    appraiserCompany: [
      {
        value: ''
      },
      {
        value: 'Company 1'
      },
      {
        value: 'Company2'
      }
    ],
    /**
     * Payment status select values
     */
    paymentStatus: [
      {
        value: ''
      },
      {
        value: 'Partial Payment'
      },
      {
        value: 'Authorized'
      },
      {
        value: 'Invoiced'
      },
      {
        value: 'Payment Info Saved'
      }
    ],
    /**
     * Appraisal status select values
     */
    appraisalStatus: [
      {
        value: ''
      },
      {
        value: 'Incomplete'
      },
      {
        value: 'Complete'
      }
    ],
    /**
     * Days select values
     */
    days: [
      {
        value: ''
      },
      {
        value: '30 days'
      },
      {
        value: '60 days'
      },
      {
        value: '90 days'
      }
    ],
    /**
     * Month select values
     */
    months: [
      {
        value: ''
      },
      {
        value: 'January'
      },
      {
        value: 'February'
      },
      {
        value: 'March'
      },
      {
        value: 'April'
      },
      {
        value: 'May'
      },
      {
        value: 'June'
      },
      {
        value: 'July'
      },
      {
        value: 'August'
      },
      {
        value: 'September'
      },
      {
        value: 'October'
      },
      {
        value: 'November'
      },
      {
        value: 'December'
      }
    ],
    /**
     * Year select values
     */
    years: [
      {
        value: ''
      },
      {
        value: '2000'
      },
      {
        value: '2001'
      },
      {
        value: '2002'
      },
      {
        value: '2003'
      },
      {
        value: '2004'
      },
      {
        value: '2005'
      },
      {
        value: '2006'
      },
      {
        value: '2007'
      },
      {
        value: '2008'
      },
      {
        value: '2009'
      },
      {
        value: '2010'
      },
      {
        value: '2011'
      },
      {
        value: '2012'
      },
      {
        value: '2013'
      },
      {
        value: '2014'
      },
      {
        value: '2015'
      },
      {
        value: '2016'
      },
      {
        value: '2017'
      },
      {
        value: '2018'
      }
    ],
    /**
     * Group by select values
     */
    groupBy: [
      {
        value: 'All'
      },
      {
        value: 'Client'
      },
      {
        value: 'Appraiser'
      }
    ],
    format: [
      {
        value: 'PDF'
      },
      {
        value: 'XLS'
      },
      {
        value: 'Both'
      }
    ],
    /**
     * Unpaid view select configuration
     * @returns {{Object}}
     */
    getUnpaidSelects: function () {
      return {
        clientBranch: this.clientBranch,
        appraiserCompany: this.appraiserCompany,
        paymentStatus: this.paymentStatus,
        appraisalStatus: this.appraisalStatus,
        days: this.days,
        month: this.months,
        year: this.years,
        groupBy: this.groupBy
      };
    },
    /**
     * Unpaid view search models
     */
    getUnpaidSearchModels: function () {
      this.searchModels = {
        borrowerName: '',
        address: '',
        fileNumber: '',
        invoiceNumber: '',
        client: '',
        // Client branches
        clientBranch: '',
        // Appraiser
        appraiser: '',
        // Appraiser companies
        appraiserCompany: '',
        // Payment status,
        paymentStatus: '',
        // Appraisal Status
        appraisalStatus: '',
        // State
        state: '',
        // Days (how many days old)
        days: '',
        // Month
        month: '',
        // Years
        year: '',
        // Datepicker from
        dateFrom: '',
        // Datepicker to
        dateTo: ''
      };
    },
    /**
     * Get paid view select values
     */
    getPaidSelects: function () {
      return {
        clientBranch: this.clientBranch,
        appraiserCompany: this.appraiserCompany,
        appraisalStatus: this.appraisalStatus,
        days: this.days,
        month: this.months,
        year: this.years,
        groupBy: this.groupBy
      };
    },
    /**
     * Paid view search models
     */
    getPaidSearchModels: function () {
      this.searchModels = {
        borrowerName: '',
        fileNumber: '',
        invoiceNumber: '',
        client: '',
        clientBranch: '',
        appraiser: '',
        appraiserCompany: '',
        paymentStatus: '',
        appraisalStatus: '',
        state: '',
        days: '',
        month: '',
        year: '',
        dateFrom: '',
        dateTo: ''
      };
    },
    /**
     * Get paid view select values
     */
    getInvoicesSelects: function () {
      return {
        clientBranch: this.clientBranch,
        appraiserCompany: this.appraiserCompany,
        groupBy: this.groupBy
      };
    },
    /**
     * Paid view search models
     */
    getInvoicesSearchModels: function () {
      this.searchModels = {
        invoiceNumber: '',
        checkNumber: '',
        client: '',
        clientBranch: '',
        appraiserCompany: '',
        dateFrom: '',
        dateTo: ''
      };
    },
    /**
     * Appraiser unpaid select boxes
     */
    getAppraiserUnpaidSelects: function () {
      return {
        clientBranch: this.clientBranch,
        appraiserCompany: this.appraiserCompany,
        paymentStatus: this.paymentStatus,
        appraisalStatus: this.appraisalStatus,
        days: this.days,
        month: this.months,
        year: this.years,
        groupBy: this.groupBy
      };
    },
    /**
     * Appraiser Unpaid search models
     */
    getAppraiserUnpaidSearchModels: function () {
      this.searchModels = {
        borrowerName: '',
        address: '',
        fileNumber: '',
        client: '',
        // Client branches
        clientBranch: '',
        // Appraiser
        appraiser: '',
        // Appraiser companies
        appraiserCompany: '',
        // Payment status,
        paymentStatus: '',
        // Appraisal Status
        appraisalStatus: '',
        // State
        state: '',
        // Days (how many days old)
        days: '',
        // Month
        month: '',
        // Years
        year: '',
        // Datepicker from
        dateFrom: '',
        // Datepicker to
        dateTo: ''
      };
    },
    /**
     * Pending ACH select box values
     */
    getPendingAchSelects: function () {
      return {
        clientBranch: this.clientBranch,
        appraiserCompany: this.appraiserCompany,
        appraisalStatus: this.appraisalStatus,
        days: this.days,
        month: this.months,
        year: this.years,
        groupBy: this.groupBy
      };
    },
    /**
     * Pending ACH search models
     */
    getPendingAchSearchModels: function () {
      this.searchModels = {
        borrowerName: '',
        address: '',
        fileNumber: '',
        client: '',
        // Client branches
        clientBranch: '',
        // Appraiser
        appraiser: '',
        // Appraiser companies
        appraiserCompany: '',
        // Appraisal Status
        appraisalStatus: '',
        // State
        state: '',
        // Days (how many days old)
        days: '',
        // Month
        month: '',
        // Years
        year: '',
        // Datepicker from
        dateFrom: '',
        // Datepicker to
        dateTo: ''
      };
    },
    /**
     * Appraiser paid select box values
     */
    getAppraiserPaidSelects: function () {
      return {
        clientBranch: this.clientBranch,
        appraiserCompany: this.appraiserCompany,
        paymentStatus: this.paymentStatus,
        appraisalStatus: this.appraisalStatus,
        days: this.days,
        month: this.months,
        year: this.years,
        groupBy: this.groupBy
      };
    },
    /**
     * Appraiser paid search models
     */
    getAppraiserPaidSearchModels: function () {
      this.searchModels = {
        borrowerName: '',
        address: '',
        fileNumber: '',
        checkNumber: '',
        client: '',
        // Client branches
        clientBranch: '',
        // Appraiser
        appraiser: '',
        // Appraiser companies
        appraiserCompany: '',
        // Payment status
        paymentStatus: '',
        // Appraiser status
        appraisalStatus: '',
        // State
        state: '',
        // Days (how many days old)
        days: '',
        // Month
        month: '',
        // Years
        year: '',
        // Datepicker from
        dateFrom: '',
        // Datepicker to
        dateTo: ''
      };
    },
    /**
     * Appraiser paid select box values
     */
    getCommissionsSelects: function () {
      return {
        clientBranch: this.clientBranch,
        appraiserCompany: this.appraiserCompany,
        paymentStatus: this.paymentStatus,
        appraisalStatus: this.appraisalStatus,
        days: this.days,
        month: this.months,
        year: this.years,
        groupBy: this.groupBy
      };
    },
    /**
     * Appraiser paid search models
     */
    getCommissionsSearchModels: function () {
      this.searchModels = {
        borrowerName: '',
        address: '',
        fileNumber: '',
        checkNumber: '',
        client: '',
        // Client branches
        clientBranch: '',
        // Appraiser
        appraiser: '',
        // Appraiser companies
        appraiserCompany: '',
        // Payment status
        paymentStatus: '',
        // Appraiser status
        appraisalStatus: '',
        // State
        state: '',
        // Days (how many days old)
        days: '',
        // Month
        month: '',
        // Years
        year: '',
        // Datepicker from
        dateFrom: '',
        // Datepicker to
        dateTo: ''
      };
    },
    /**
     * Aging balance search models
     */
    getAgingBalanceSearchModels: function () {
      this.searchModels = {
        client: '',
        // Appraiser
        appraiser: '',
        // Appraiser companies
        appraiserCompany: ''
      };
    },
    /**
     * Aging balance selects
     */
    getAgingBalanceSelects: function () {
      return {
        appraiserCompany: this.appraiserCompany,
        // How to group aging data
        agingGroup: [
          {
            value: 'Accounts Receivable'
          },
          {
            value: 'Accounts Payable'
          }
        ]
      };
    },
    /**
     * Transaction summary search models
     */
    getTransactionSummarySearchModels: function () {
      this.searchModels = {
        fileNumber: '',
        client: '',
        clientUser: '',
        invoiceNumber: '',
        appraiser: '',
        appraiserCompany: '',
        dateFrom: '',
        dateTo: ''
      };
    },
    /**
     * Transaction summary select boxes
     */
    getTransactionSummarySelects: function () {
      return {
        appraiserCompany: this.appraiserCompany,
        // How to group aging data
        transactionSummaryGrouping: [
          {
            value: 'Accounts Receivable'
          },
          {
            value: 'Accounts Payable'
          }
        ]
      };
    },
    /**
     * Quickbooks select box options
     */
    getQuickbooksSelects: function () {
      return {
        // Request status
        requestStatus: [
          {
            value: 'In Queue'
          },
          {
            value: 'Success'
          },
          {
            value: 'Error'
          }
        ],
        // Request action
        requestAction: [
          {
            value: 'all'
          },
          {
            value: 'vendor add'
          },
          {
            value: 'customer add'
          },
          {
            value: 'bill add'
          }
        ],
        // Sync invoice
        syncInvoice: [
          {
            value: 'On completion'
          },
          {
            value: 'On first payment OR completion'
          }
        ],
        // Sync CC payments
        syncCcPayments: [
          {
            value: 'On'
          },
          {
            value: 'Off'
          }
        ],
        // Sync check payments
        syncCheckPayments: [
          {
            value: 'On'
          },
          {
            value: 'Off'
          }
        ],
        // Sync appraiser checks
        syncAppraiserChecks: [
          {
            value: 'On'
          },
          {
            value: 'Off'
          }
        ],
        // Sync 1099
        sync1099: [
          {
            value: 'On'
          },
          {
            value: 'Off'
          }
        ]
      };
    }
  };
}]);