'use strict';

var app = angular.module('frontendApp');

app.factory('AdminAccountingDataService',
['$http', '$q', 'FileUploader', 'API_PREFIX', '$timeout', 'AdminAccountingConfigService', 'traverse', '$filter',
 '$state', '$stateParams', 'AccountingResourceService', 'AsDataService', 'AsUrlService', function (
$http, $q, FileUploader, API_PREFIX, $timeout, AdminAccountingConfigService, traverse, $filter, $state, $stateParams,
AccountingResourceService, AsDataService, AsUrlService) {
  var dataServiceCallbacks = [],
      headers = AdminAccountingConfigService.getModelHeaders();

  var Service = {
    /**
     * Hold reference to the current tab for child controllers
     */
    tab: '',
    // Current page
    page: {},
    /**
     * Hold data for use between controllers
     */
    data: [],
    safeData: {

    },
    // Common display items
    displayData: {
      formatOptions: [
        {
          value: 'PDF'
        },
        {
          value: 'XLS'
        },
        {
          value: 'Both'
        }
      ]
    },
    // Max number of pages to display
    //maxPages: 3,
    /**
     * Set pagination in progress or not
     * @param inProgress
     */
    //paginationInProgress: false,
    /**
     * Retrieve a request page
     */
    getRequestedPage: function (callback, page) {
      var service = this;
      if (arguments.length === 1 && angular.isUndefined(page)) {
        page = callback;
        callback = angular.noop;
      }
      return service.request('init', {page: page, grouping: service.safeData.grouping})
      .then(function (response) {
        // Extract meta data
        if (angular.isDefined(response.meta)) {
          service.safeData.meta = angular.copy(response.meta);
        }
        // Callback to update table
        if (angular.isFunction(callback)) {
          callback(response);
        }
        // Update URL to reflect current page
        AsUrlService.changeUrl({page: page});
      })
      .then(function () {
        // Update totals
        if (angular.isFunction(service.getTotals)) {
          service.getTotals();
        }
      });
    },
    /**
     * Group table data
     *
     * @todo Waiting on backend
     * @link https://github.com/ascope/manuals/issues/265
     */
    changeGrouping: function (grouping) {
      var service = this;
      // Update grouping
      service.safeData.grouping = grouping;
      // Retrieve the requested page
      return service.getRequestedPage(1);
    },
    /**
     * Specify the right transformer
     */
    beforeInitFormat: function () {
      var service = this;
      // Use table transformers
      service.transformers = service.safeData.transformers.table;
    },
    /**
     * Get and format grand totals
     */
    getTotals: function () {
      var service = this;
      // Use totals transformers
      service.transformers = service.safeData.transformers.totals;
      // Generate totals
      AsDataService.formatPlainObjectData.call(service, 'meta');
      // Switch back to table transformers
      service.transformers = service.safeData.transformers.table;
    },
    /**
     * Below here is under threat of refactoring
     */
    /**
     * Observer pattern callback registry
     * @param callback
     */
    registerCallback: function (callback) {
      dataServiceCallbacks = [callback];
    },
    /**
     * Run all registered callbacks
     */
    notifyObservers: function () {
      angular.forEach(dataServiceCallbacks, function (callback) {
        callback();
      });
    },
    /**
     * Initial table data load
     */
    getTableData: function () {
      var page = $stateParams.page, tab = $stateParams.activeTab;
      // Make sure not to paginate yet
      Service.paginationInProgress = true;
      return $http.get(API_PREFIX() + '/v2.0/admin/accounting/data/' + page + '/' + tab)
      .then(function (data) {
        // Create a completely separate copy of the data for calculations
        var dataCopy = JSON.parse(JSON.stringify(data));
        Service.safeData = dataCopy.data;
        // Apply filters, then pass on the data
        Service.applyFilters(data.data).then(function () {
          // Set data, trigger callback
          Service.setData(data.data);
          // Allow pagination
          Service.paginationInProgress = false;
        });
      });
    },
    /**
     * Set data and then notify all observers
     * @param data
     */
    setData: function (data) {
      Service.data = data.slice();
      Service.notifyObservers();
    },
    /**
     * Filter data before passing it to the table
     *
     * @todo Use AsTableService implementation of applyFilters
     *
     * @param data
     * @returns {*}
     */
    applyFilters: function (data) {
      return $q(function (resolve) {
        // Iterate rows
        angular.forEach(data, function (row, key) {
          // Iterate cells
          angular.forEach(row, function (thisRow, rowKey) {
            // Only run if we have a header, and the data is not going to change. If the data is going to be updated,
            // let Angular handle filtering in the view
            if (angular.isDefined(headers[rowKey]) && angular.isDefined(headers[rowKey].filter) &&
                (!angular.isDefined(headers[rowKey].isStatic) || headers[rowKey].isStatic)) {
              // Filter with args
              if (headers[rowKey].filter.indexOf(':') !== -1) {
                var filterArgs = headers[rowKey].filter.split(':');
                data[key][rowKey] = $filter(filterArgs[0])(data[key][rowKey], filterArgs[1]);
              // Filter without args
              } else {
                data[key][rowKey] = $filter(headers[rowKey].filter)(data[key][rowKey]);
              }
            }
          });
        });
        return resolve(data);
      });
    },
    /**
     * Retrieve headers to be used in the current table view
     * @param requestedHeaders
     * @returns {Array}
     */
    getHeaders: function (requestedHeaders) {
      var usedHeaders = [];
      // Iterate headers, and search for existing data points
      angular.forEach(requestedHeaders, function (header) {
        if (headers[header]) {
          usedHeaders.push(headers[header]);
        } else {
          // If no header exists, use it as a label only
          usedHeaders.push({
            label: header,
            data: null
          });
        }
      });
      return usedHeaders;
    },
    /**
     * Retrieve sorters for each table view
     * @param sorters
     */
    getSorters: function (sorters) {
      var sorterObject = {};
      angular.forEach(sorters, function (sorter) {
        // Throw an error if an invalid sorter is requested
        if (!headers[sorter]) {
          throw Error('Could not find data to sort');
        }
        sorterObject[sorter] = function (value) {
          return value[sorter];
        };
      });
      return sorterObject;
    },
    /**
     * Calculate totals for this set of data
     * @param rowData
     * @param columnsToCalculate
     * @param limit
     * @returns {*}
     */
    getPageTotal: function (rowData, columnsToCalculate, limit) {
      // If nothing to work with, return empty object
      if (!rowData.length) {
        return {};
      }
      if (limit) {
        rowData = rowData.slice(0, limit);
      } else {
        // Reset totals (for infinite scroll)
        angular.forEach(columnsToCalculate, function (total, key) {
          columnsToCalculate[key] = 0;
        });
      }
      // Iterate row data and recalculate page total
      angular.forEach(rowData, function (thisRow) {
        // Calculate page totals
        angular.forEach(thisRow, function (cell, rowKey) {
          // See if this data point is one that will be totaled
          if (columnsToCalculate.hasOwnProperty(rowKey) && cell) {
            // Make sure to not process strings
            if (typeof cell === 'string') {
              cell = parseFloat(cell);
            }
            // Add to column total
            columnsToCalculate[rowKey] = columnsToCalculate[rowKey] + cell;
          }
        });
      });
      return columnsToCalculate;
    },
    /**
     * Keep track of how many pages are currently being displayed
     */
    pagesDisplayed: function (pages) {
      return pages === Service.maxPages ? Service.maxPages : pages + 1;
    },
    /**
     * POST parameters, retrieve export link
     */
    getExportDataLink: function (params) {
      return $http.post(API_PREFIX() + '/v2.0/admin/accounting/retrieve-export-data/', params);
    },
    /**
     * Retrieve the grand total for the view
     */
    getGrandTotal: function (view) {
      return $http.get(API_PREFIX() + '/v2.0/admin/accounting/grand-total/' + view);
    },
    /**
     * Retrieve data from the next page
     * @param page
     */
    getPage: function (page) {
      return $http.get(API_PREFIX() + '/v2.0/admin/accounting/data/' + page);
    },
    /**
     * Change URL without changing state
     * @param page
     */
    changeUrl: function (page) {
      $state.transitionTo('.', {page: page}, { location: true, inherit: true, relative: $state.$current, notify: false });
    },
    /**
     * The total number of pages for this view
     */
    getPages: function (view) {
      return $http.get(API_PREFIX() + '/v2.0/admin/accounting/pages/' + view);
    },
    /**
     * Perform a search for specific data
     * @param view
     * @param searchParams
     */
    search: function (view, searchParams) {
      // Remove unused search parameters
      searchParams = Service.removeUnusedParams(searchParams);
      // POST a search request
      return $http.post(API_PREFIX() + '/v2.0/admin/accounting/search/', searchParams);
    },
    /**
     * Resort data set
     * @param sortParams
     */
    sort: function (sortParams) {
      // Don't allow pagination
      Service.paginationInProgress = true;
      var dataService = this;
      return $q(function (resolve) {
        // Retrieve table data and sort it -- For now, just the first page
        $http.get(API_PREFIX() + '/v2.0/admin/accounting/data/1').then(function (data) {
          // Simulate sorting from the server
          resolve(dataService.fakeSorting(data.data, sortParams.type, sortParams.reverse));
        });
      });
    },
    /**
     * Simulate server side sorting
     * @param data
     * @param prop
     * @param asc
     * @returns {*}
     */
    fakeSorting: function (data, prop, asc) {
      return data.sort(function (a, b) {
        if (!asc) {
          return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
        } else {
          return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
        }
      });
    },
    /**
     * Retrieve grouped data
     */
    changeGroup: function (grouping) {
      return $http.get(API_PREFIX() + '/v2.0/admin/accounting/sort/1/' + grouping);
    },
    /**
     * Update appraisals after an invoice has been generated
     * @param invoiceData
     *
     * @todo Deprecated
     */
    updateAppraisalsWithInvoice: function (invoiceData) {
      var that = this;
      // Traverse each node and update if it's an invoice
      traverse(Service.data).forEach(function (node) {
        if (that.nodeIsAppraisal(node) && invoiceData.appraisalIds.indexOf(node.id) !== -1) {
          node.invoiceId = invoiceData.invoiceId;
          node.invoiceDescription = invoiceData.invoiceDescription;
          node.invoiceStatus = invoiceData.invoiceStatus;
          node.invoiceAmount = invoiceData.invoiceAmount;
          node.invoiceNumber = invoiceData.invoiceNumber;
          this.update(node);
        }
      });
      Service.setData(Service.data);
    },
    /**
     * Remove unused parameters
     * @param params
     * @param searchModels
     * @returns {*}
     */
    removeUnusedParams: function (params, searchModels) {
      if (searchModels) {
        params = angular.element().extend(params, searchModels);
      }
      var actualParams = {};
      // Remove empty search parameters
      angular.forEach(params, function (param, key) {
        if (param) {
          actualParams[key] = param;
        }
      });
      return actualParams;
    },
    /**
     *
     * @param tableData
     * @returns {*}
     */
    filterRecursive: function (tableData) {
      var that = this;
      return tableData.filter(function (dataRow) {
        if (dataRow === null) {
          return;
        }
        // If this is a tree structure, then call recursively
        if (dataRow.data) {
          dataRow.data = that.filterRecursive(dataRow.data);
          if (dataRow.data.length) {
            return dataRow;
          }
          // If flat object, evaluate normally
        } else if (dataRow) {
          return dataRow;
        }
      });
    },
    /**
     * Check if a node being traversed is an appraisal record
     * @param node
     * @returns {boolean|*}
     */
    nodeIsAppraisal: function (node) {
      return angular.isObject(node) && node.hasOwnProperty('id') && angular.isNumber(node.id);
    },
    /**
     * Remove appraisal from list
     *
     * NOTE: The requested appraisals are set to null, rather than removed from tree. They are recursively filtered from
     * the tree after being set to null. The reason for this is that setting them directly to null interferes with smart table
     * updates. -- Logan 4/19/15
     *
     * @param id
     */
    removeAppraisal: function (id) {
      var that = this;
      // Make sure the correct argument is used
      if (!angular.isArray(id) && !angular.isNumber(id)) {
        throw Error('removeAppraisal requires a number or array of numbers as the argument');
      }
      var tableData = traverse(Service.data).map(function (node) {
        // If it's a matching appraisal, remove it from the tree
        if (that.nodeIsAppraisal(node)) {
          // If an array of appraisals if passed in, remove each
          if (angular.isArray(id) && id.indexOf(node.id) !== -1) {
            this.update(null);
            // If a single one is passed in, remove that
          } else if (angular.isNumber(id) && node.id === id) {
            this.update(null);
          }
        }
      });
      // Set the new data
      Service.setData(Service.filterRecursive(tableData));
    },
    /**
     * Retrieve a single appraisal from table data
     * @param appraisalId
     * @returns {*}
     */
    getSingleAppraisal: function (appraisalId) {
      var that = this;
      var sought = null;
      // Find the node being sought after
      traverse(Service.data).forEach(function (node) {
        if (that.nodeIsAppraisal(node) && node.id === appraisalId) {
          sought = node;
        }
      });
      return sought;
    },
    /**
     * Remove paid records from table
     * @param paidRow
     * @param property
     * @param payAmount
     * @returns {*}
     */
    updatePaidRecords: function (paidRow, property, payAmount) {
      var that = this;
      var tableData = traverse(Service.data).map(function (node) {
        // If this is an appraisal record, update it accordingly
        if (that.nodeIsAppraisal(node) && paidRow.indexOf(node.id) !== -1) {
          // Remove those are completely paid
          if (!payAmount) {
            this.update(null);
          }
          // If there is a payment amount, check to make sure that the balance was paid before removing
          if (parseFloat(payAmount) >= node[property]) {
            return null;
            // Otherwise decrement the amount due
          } else {
            node[property] = node[property] - parseFloat(payAmount);
          }
        }
      });
      // Filter out unused now-empty array items
      Service.setData(Service.filterRecursive(tableData));
    },
    /**
     * Uncheck the model for all selected records
     * @param multiSelect
     * @param records
     */
    uncheckRecords: function (multiSelect, records) {
      // Uncheck a subset of multi-select items
      if (records) {
        records.forEach(function (invoice) {
          multiSelect[invoice.toString()] = false;
        });
        // Uncheck all multiselect
      } else {
        angular.forEach(multiSelect, function (select, key) {
          multiSelect[key] = false;
        });
      }
    },
    /**
     * Upload CSV of checks that need to be imported
     */
    importChecks: function (file) {
      return FileUploader.upload({
        url: API_PREFIX() + '/v2.0/admin/accounting/import-checks/',
        method: 'PUT',
        file: file
      });
    },
    /**
     * Get the record currently being selected (or de-selected). To be used in $scope.$watchCollection
     * @param newArray
     * @param old
     * @returns {*}
     */
    difference: function (newArray, old) {
      var larger = old, smaller = newArray;
      // Return null if arrays are the same length
      if (newArray.length === old.length) {
        return null;
      }
      // Determine which is larger (old val or new val)
      if (newArray.length > old.length) {
        larger = newArray;
        smaller = old;
      }
      // If we have a single record, return
      if (smaller.length === 0 && larger.length === 1) {
        return larger[0];
      }
      // Filter to find the value being added or removed from selected records
      return larger.filter(function(i) {
        return smaller.indexOf(i) === -1;
      })[0];
    },
    /**
     * Get the values for each of the tabs for the subsections
     */
    getTabValues: function () {
      // Cached
      if (Service.safeData.tabValues) {
        return $q(function (resolve) {
          resolve({data: Service.safeData.tabValues});
        });
      }
      // Request values
      return $http.get(API_PREFIX() + '/v2.0/admin/accounting/tab-values')
      .then(function (response) {
        Service.safeData.tabValues = angular.copy(response.data);
        return response;
      });
    },
    /**
     * Remove an appraisal from the table (immutable data)
     */
    removeAppraisalImmutable: function (type, appraisal) {
      var service = this;
      // Remove from table
      service.safeData[type] = service.safeData[type].delete(appraisal);
      service.formatSingleImmutable(parseInt(appraisal));
      // Remove selected record
      service.safeData.selectedAppraisal = null;
    },
    /**
     * Update display on new page
     */
    pageLoadCallback: function (type, response) {
      var service = this;
      // Update table
      service.hashDataImmutable(response.data, type);
      service.formatDataImmutable();
    }
  };

  // Inherit requests
  Service.request = AsDataService.request.bind(Service, 'AccountingResourceService');
  return Service;
}]);
