describe('AdminAccountingAppraiserUnpaidService', function () {
  var scope, httpBackend, Service, adminAccountingDataService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminAccountingDataService', AdminAccountingDataServiceMock);
    });
  });

  beforeEach(inject(function (AdminAccountingAppraiserUnpaidService, AdminAccountingDataService, $httpBackend, $rootScope) {
    scope = $rootScope.$new();
    Service= AdminAccountingAppraiserUnpaidService;
    adminAccountingDataService = AdminAccountingDataService;
    httpBackend = $httpBackend;

    spyOn(Service, 'hashDataImmutable');
    spyOn(Service, 'formatDataImmutable');
    spyOn(Service, 'formatSingleImmutable');
    spyOn(Service, 'request').and.callThrough();

    Service.safeData.appraiserUnpaid = Immutable.fromJS({
      1: {
        id: 1,
        ach: true,
        payment: {payable: {fee: 101.00}}
      },
      2: {
        id: 2,
        ach: false,
        payment: {payable: {fee: 202.00}}
      }
    });
    // Selected appraisal
    Service.safeData.selectedAppraisal = '1';

    httpBackend.whenPOST(/pay/).respond();
  }));

  describe('beforeInitFormat', function () {
    beforeEach(function () {
      spyOn(Service, 'determineAch');
      Service.beforeInitFormat({});
    });

    it('should determine ACH values', function () {
      expect(Service.determineAch).toHaveBeenCalled();
    });
    it('should set transformers correctly', function () {
      expect(Service.transformers.clientName).toEqual({ source: 'client.name' });
    });
  });

  describe('determineAch', function () {
    var records;
    beforeEach(function () {
      records = [{
        payment: {
          receivable: {
            profile: {
              bank: {
                account: '1',
                name: 'Bank',
                routing: '2'
              }
            }
          }
        }
      }, {
        payment: {
          receivable: {
            profile: {
              bank: null
            }
          }
        }
      },
        {
          payment: {
            receivable: {
              profile: {
                bank: {
                  account: '1',
                  name: 'Bank'
                }
              }
            }
          }
        }];
      Service.determineAch(records);
    });

    it('should set ACH to true on the first record', function () {
      expect(records[0].ach).toEqual(true);
    });
    it('should set ACH to false on the second record', function () {
      expect(records[1].ach).toEqual(false);
    });
    it('should set ACH to false on the third record', function () {
      expect(records[2].ach).toEqual(false);
    });
  });

  describe('pageLoadCallback', function () {
    beforeEach(function () {
      spyOn(Service, 'determineAch');
      Service.pageLoadCallback({data: [1,2]});
    });

    it('should determine ACH', function () {
      expect(Service.determineAch).toHaveBeenCalled();
    });
    it('should hash data', function () {
      expect(Service.hashDataImmutable).toHaveBeenCalled();
    });
    it('should format data', function () {
      expect(Service.formatDataImmutable).toHaveBeenCalled();
    });
  });

  describe('achExists', function () {
    var response;
    beforeEach(function () {
      response = Service.achExists();
    });
    it('should report that ACH does exist', function () {
      expect(response).toEqual(true);
    });
  });

  describe('payAppraiserAch', function () {
    beforeEach(function () {
      Service.safeData.selectedAppraisal = 1;
      spyOn(Service, 'removePaidAppraisal');
      Service.payAppraiserAch();
      httpBackend.flush();
      scope.$digest();
    });

    it('should make the request correctly', function () {
      expect(Service.request).toHaveBeenCalledWith('payAppraiser', Object({
        url: Object({appraisalId: 1}),
        body: Object({method: 'ach'})
      }));
    });
    it('should remove the paid appraisals', function () {
      expect(Service.removePaidAppraisal).toHaveBeenCalled();
    });
  });

  describe('markPaid', function () {
    beforeEach(function () {
      spyOn(Service, 'removePaidAppraisal');
      Service.safeData.selectedAppraisal = 1;
      Service.displayData.paySingle = {number: '55', date: '07/10/2012'};
      Service.markPaid();
      httpBackend.flush();
      scope.$digest();
    });

    it('should make the right request', function () {
      expect(Service.request.calls.argsFor(0)).toEqual(['payAppraiser', {
        url: {appraisalId: 1},
        body: {
          method: 'check',
          number: '55',
          date: '2012-07-10T00:00:00.000Z'
        }
      }]);
    });
    it('should remove paid appraisals', function () {
      expect(Service.removePaidAppraisal).toHaveBeenCalled();
    });
  });

  describe('getBalanceDue', function () {
    beforeEach(function () {
      Service.displayData.payMulti = {amountDue: 100};
      Service.safeData.selectedRecords = ['1', '2'];
      Service.getBalanceDue();
    });

    it('should calculate balance due', function () {
      expect(Service.displayData.payMulti.amountDue).toEqual(303);
    });
  });

  describe('payMulti', function () {
    beforeEach(function () {
      Service.safeData.selectedRecords = ['1', '2'];
      Service.displayData.payMulti = {number: 'check', date: '07/14/2010'};
      spyOn(Service, 'removePaidAppraisal');
    });
    describe('ach', function () {
      beforeEach(function () {
        Service.payMulti(true);
        httpBackend.flush();
        scope.$digest();
      });

      it('should make the right request', function () {
        expect(Service.request).toHaveBeenCalledWith('payAppraiser', {
          url: {},
          body: {
            method: 'ach',
            appraisals: ['1', '2']
          }
        });
      });
      it('should remove all paid appraisals', function () {
        expect(Service.removePaidAppraisal.calls.count()).toEqual(2);
      });
    });

    describe('non-ach', function () {
      beforeEach(function () {
        Service.payMulti();
        httpBackend.flush();
        scope.$digest();
      });

      it('should make the right request', function () {
        expect(Service.request).toHaveBeenCalledWith('payAppraiser', {
          url: {},
          body: {
            method: 'check',
            appraisals: ['1', '2'],
            number: 'check',
            date: '2010-07-14T00:00:00.000Z'
          }
        });
      });
      it('should remove all paid appraisals', function () {
        expect(Service.removePaidAppraisal.calls.count()).toEqual(2);
      });
    });
  });

  describe('payMultiAch', function () {
    beforeEach(function () {
      spyOn(Service, 'removePaidAppraisal');
      Service.safeData.selectedRecords = ['1', '2'];
      Service.payMultiAch();
      httpBackend.flush();
      scope.$digest();
    });

    it('should make the right request', function () {
      expect(Service.request).toHaveBeenCalledWith('payAppraiser', {
        url: {},
        body: {
          method: 'ach',
          appraisals: ['1', '2']
        }
      });
    });
    it('should remove all paid appraisals', function () {
      expect(Service.removePaidAppraisal.calls.count()).toEqual(2);
    });
  });
});