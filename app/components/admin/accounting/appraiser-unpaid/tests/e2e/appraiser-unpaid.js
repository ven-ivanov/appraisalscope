var backend = null,
helpers = new global.UserHelper(),
formHelper = new global.FormHelper(),
loginHelper = new global.LoginHelper(),
urlHelper = new global.UrlHelper(),
helper = new global.Helper(),
adminAccountingHelper = new global.AdminAccountingHelper();

describe('Admin Accounting -- Invoices', function () {
  // Allow GET requests
  beforeEach(function () {
    backend = new global.HttpBackend(browser);
    backend.whenGET(/.*/).passThrough();
  });

  // Set browser to maximum size
  beforeEach(function () {
    browser.driver.manage().window().maximize();
  });
  // Clear backend after each test
  afterEach(function() {
    backend.clear();
  });

  it('should allow the user to login', function () {
    // Login as admin
    loginHelper.login('admin', backend, urlHelper);
  });

  it('should go to the admin accounting section', function () {
    urlHelper.switchTab(2);
  });

  it('should visit the invoices subtab', function () {
    element(by.repeater('tab in adminAccountingTabsCtrl.tabs track by $index').row(2)).click();
    urlHelper.testUrl('accounting/invoices/1');
  });
});
