'use strict';

var app = angular.module('frontendApp');

app.factory('AdminAccountingAppraiserUnpaidService',
['AsDataService', 'AdminAccountingDataService', function (AsDataService, AdminAccountingDataService) {
  var Service = {
    // Handle data as immutable
    immutable: true,
    safeData: {
      // Data
      appraiserUnpaid: {},
      // Selected appraisal
      selectedAppraisal: '',
      // Selected records
      selectedRecords: [],
      // Table definition
      heading: [
        // File number
        {
          label: 'File Number',
          data: 'file'
        },
        // Client name
        {
          label: 'Client',
          data: 'clientName'
        },
        // Borrower name
        {
          label: 'Borrower',
          data: 'borrower'
        },
        // Borrower address
        {
          label: 'Address',
          data: 'address'
        },
        // Order date
        {
          label: 'Order Date',
          data: 'createdAt'
        },
        // Completed date
        {
          label: 'Completed Date',
          data: 'completedAt'
        },
        // Client fee
        {
          label: 'Client Fee',
          data: 'clientFee',
          grandTotal: 'clientFeeGrandTotal',
          pageTotal: 'clientFeePageTotal'
        },
        // Appraiser fee
        {
          label: 'Appraiser Fee',
          data: 'appFee',
          grandTotal: 'appraiserFeeGrandTotal',
          pageTotal: 'appraiserFeePageTotal'
        },
        // P/L Amount
        {
          label: 'P/L Amount',
          data: 'plAmount',
          grandTotal: 'plAmountGrandTotal',
          pageTotal: 'plAmountPageTotal'
        },
        // Pay appraiser
        {
          label: 'Action',
          noSort: true,
          linkLabel: {false: 'No ACH', true: 'Pay'},
          data: 'ach',
          isStatic: false,
          fn: 'payAppraiser'
        },
        // Mark as paid
        {
          label: 'Mark as Paid',
          noSort: true,
          linkLabel: 'Mark as Paid',
          fn: 'markAsPaid'
        },
        // Standard multi-select checkbox
        {
          label: '',
          noSort: true,
          checkbox: true
        }
      ],
      // Transformers
      transformers: {
        // Table
        table: {
          clientName: {
            source: 'client.name'
          },
          borrower: {
            source: 'property.contacts.borrower.fullName'
          },
          address: {
            source: 'property.location.address1'
          },
          createdAt: {
            filter: 'date'
          },
          completedAt: {
            filter: 'date'
          },
          clientFee: {
            filter: 'currency',
            source: 'payment.receivable.fee'
          },
          appFee: {
            filter: 'currency',
            source: 'payment.payable.fee'
          },
          plAmount: {
            filter: 'currency',
            source: 'payment.amount.pl'
          }
        },
        // Page and grand totals
        totals: {
          clientFeeGrandTotal: {
            filter: 'currency',
            source: 'summary.receivable.fee.total'
          },
          clientFeePageTotal: {
            filter: 'currency',
            source: 'summary.receivable.fee.pageTotal'
          },
          appraiserFeeGrandTotal: {
            filter: 'currency',
            source: 'summary.payable.fee.total'
          },
          appraiserFeePageTotal: {
            filter: 'currency',
            source: 'summary.payable.fee.pageTotal'
          },
          plAmountGrandTotal: {
            filter: 'currency',
            source: 'summary.amount.pl.total'
          },
          plAmountPageTotal: {
            filter: 'currency',
            source: 'summary.amount.pl.pageTotal'
          }
        }
      }
    },
    displayData: {
      // Records
      appraiserUnpaid: [],
      // Pay single modal
      paySingle: {
        number: '',
        date: ''
      },
      // Pay multi modal
      payMulti: {
        amountDue: '',
        date: '',
        number: ''
      }
    },
    transformers: {},
    /**
     * Determine ACH and set transformers on load
     */
    beforeInitFormat: function (records) {
      // Determine ACH value for each record
      Service.determineAch(records);
      // Use table transformers
      Service.transformers = Service.safeData.transformers.table;
    },
    /**
     * Determine whether record has ACH or not
     * @param records
     */
    determineAch: function (records) {
      Lazy(records)
      .pluck('payment')
      .pluck('receivable')
      .pluck('profile')
      .each(function (record, key) {
        if (!angular.isObject(record.bank)) {
          records[key].ach = false;
        } else {
          records[key].ach = !!(record.bank.account && record.bank.name && record.bank.routing);
        }
      });
    },
    /**
     * Update display on new page
     */
    pageLoadCallback: function (response) {
      // Determine ACH value
      Service.determineAch(response.data);
      // Update table
      Service.hashDataImmutable(response.data, 'appraiserUnpaid');
      Service.formatDataImmutable();
    },
    /**
     * Determine is a given record has an ACH value
     */
    achExists: function () {
      return Service.safeData.appraiserUnpaid.getIn([Service.safeData.selectedAppraisal, 'ach']);
    },
    /**
     * Pay single appraisal
     */
    payAppraiserAch: function () {
      var appraisal = Service.safeData.selectedAppraisal;
      return Service.request('payAppraiser', {url: {appraisalId: appraisal}, body: {method: 'ach'}})
      .then(function () {
        Service.removePaidAppraisal(parseInt(appraisal));
      });
    },
    /**
     * Pay a single appraisal
     */
    markPaid: function () {
      var appraisal = Service.safeData.selectedAppraisal;
      var paySingle = Service.displayData.paySingle;
      // Make request
      return Service.request('payAppraiser', {
        url: {appraisalId: appraisal},
        body: {method: 'check', number: paySingle.number, date: new Date(paySingle.date + ' UTC').toISOString()}
      })
      .then(function () {
        Service.removePaidAppraisal(parseInt(appraisal));
      });
    },
    /**
     * Get amount due
     */
    getBalanceDue: function () {
      // Get records
      var selectedRecords = Service.safeData.selectedRecords;
      // Reset total
      Service.displayData.payMulti.amountDue = 0;
      // Calculate total appraiser fee due
      Service.displayData.payMulti.amountDue = Service.safeData.appraiserUnpaid
      .filter(function (record) {
        return selectedRecords.indexOf(String(record.get('id'))) !== -1;
      })
      .reduce(function (current, next) {
        return parseInt(current.getIn(['payment', 'payable', 'fee'])) +
               parseInt(next.getIn(['payment', 'payable', 'fee']));
      });
    },
    /**
     * Pay multi
     */
    payMulti: function (ach) {
      var appraisals = Service.safeData.selectedRecords;
      var payMulti = Service.displayData.payMulti;
      var body;
      // Pay multi via ACH
      if (ach) {
        body = {method: 'ach', appraisals: appraisals};
      // Pay multi non-ACH
      } else {
        body = {
          method: 'check',
          appraisals: appraisals,
          number: payMulti.number,
          date: new Date(payMulti.date + ' UTC').toISOString()
        };
      }
      // Make request
      return Service.request('payAppraiser', {
        url: {},
        body: body
      })
      .then(function () {
        // Remove all paid appraisals
        angular.forEach(appraisals, function (appraisal) {
          Service.removePaidAppraisal(parseInt(appraisal));
        });
      });
    },
    /**
     * Pay ACH
     */
    payMultiAch: function () {
      var appraisals = Service.safeData.selectedRecords;
      // Make request
      return Service.request('payAppraiser', {
        url: {},
        body: {method: 'ach', appraisals: appraisals}
      })
      .then(function () {
        // Remove all paid appraisals
        angular.forEach(appraisals, function (appraisal) {
          Service.removePaidAppraisal(parseInt(appraisal));
        });
      });
    }
  };

  Service.request = AsDataService.request.bind(Service, 'AccountingResourceService', 'appraiserUnpaid');
  Service.loadRecords = AsDataService.loadRecords.bind(Service, {
    resourceService: 'AccountingResourceService',
    type: 'appraiserUnpaid'
  });
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.hashDataImmutable = AsDataService.hashDataImmutable.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'appraiserUnpaid');
  Service.formatDataImmutable = AsDataService.formatDataImmutable.bind(Service, 'appraiserUnpaid');
  Service.formatSingle = AsDataService.formatSingle.bind(Service, 'appraiserUnpaid');
  Service.formatSingleImmutable = AsDataService.formatSingleImmutable.bind(Service, 'appraiserUnpaid');
  Service.transformData = AsDataService.transformData.bind(Service);
  Service.transformDataImmutable = AsDataService.transformDataImmutable.bind(Service);
  // Go to page
  Service.getRequestedPage = AdminAccountingDataService.getRequestedPage.bind(Service, Service.pageLoadCallback);
  // Change grouping
  Service.changeGrouping = AdminAccountingDataService.changeGrouping.bind(Service);
  // Get page and grand totals
  Service.getTotals = AdminAccountingDataService.getTotals.bind(Service);
  // Uncheck multiselect
  Service.uncheckRecords = AdminAccountingDataService.uncheckRecords.bind(null);
  // Remove paid appraisal
  Service.removePaidAppraisal = AdminAccountingDataService.removeAppraisalImmutable.bind(Service, 'appraiserUnpaid');

  return Service;
}]);