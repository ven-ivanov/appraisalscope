'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Accounting - Appraiser Unpaid view
 */
app.directive('adminAccountingAppraiserUnpaid',
['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/accounting/appraiser-unpaid/directives/partials/appraiser-unpaid.html',
    controller: 'AdminAccountingAppraiserUnpaidCtrl',
    controllerAs: 'tableCtrl',
    scope: true,
    link: function (scope) {
      // Import shared methods
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);