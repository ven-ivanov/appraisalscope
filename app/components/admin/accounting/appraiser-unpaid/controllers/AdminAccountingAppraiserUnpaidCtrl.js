'use strict';

var app = angular.module('frontendApp');

app.controller('AdminAccountingAppraiserUnpaidCtrl',
['$scope', 'AdminAccountingDataService', 'AdminAccountingCtrlInheritanceService',
 'AdminAccountingAppraiserUnpaidService', 'AdminAccountingConfigService',
function (
$scope, AdminAccountingDataService, AdminAccountingCtrlInheritanceService, AdminAccountingAppraiserUnpaidService,
AdminAccountingConfigService) {

  var vm = this;
  var Service = AdminAccountingAppraiserUnpaidService;
  // Set up reference to current page
  vm.page = AdminAccountingDataService.page;
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;

  /**
   * Inherit from mock controller prototype
   */
  AdminAccountingCtrlInheritanceService.createCtrl.call(this, $scope, Service, 'appraiserUnpaid');

  /**
   * Get select boxes
   */
  AdminAccountingConfigService.getAppraiserUnpaidSearchModels.call(this);

  /**
   * Individual selects for this view
   */
  _.assign(vm.selects, AdminAccountingConfigService.getAppraiserUnpaidSelects());
  /**
   * Table heading
   */
  vm.heading = Service.safeData.heading;

  /**
   * Disable buttons when no records are selected
   * @returns {boolean}
   */
  vm.disableNoRecordsSelected = function () {
    return !vm.safeData.selectedRecords.length;
  };

  /**
   * Disable pay single modal submit
   */
  vm.paySingleDisabled = function () {
    return vm.paySingleForm.$invalid;
  };

  /**
   * Pay multi ACH confirmation
   */
  vm.payMultiAchConfirm = function () {
    $scope.$broadcast('show-modal', 'pay-ach-confirm');
  };

  /**
   * Pay multi ACH button function
   */
  vm.payMultiAch = function () {
    Service.payMulti(true)
    .then(function () {
      // Success modal
      $scope.$broadcast('hide-modal', 'pay-ach-confirm');
      $scope.$broadcast('show-modal', 'pay-ach-success');
      // uncheck checked records
      Service.uncheckRecords(vm.multiSelect);
    })
    .catch(function () {
      // Failure modal
      $scope.$broadcast('show-modal', 'pay-ach-failure');
    });
  };

  /**
   * Disable submit on pay multi modal
   */
  vm.payMultiDisabled = function () {
    return vm.payMultiForm.$invalid;
  };

  /**
   * Link functions for appraiser unpaid view
   * @param id
   * @param fn
   */
  vm.linkFn = function (id, fn) {
    vm.safeData.selectedAppraisal = id;
    /**
     * Mark as paid function
     */
    if (fn === 'markAsPaid') {
      $scope.$broadcast('show-modal', 'pay-single-modal');
      // Clear modal on close
      $scope.clearModalOnClose('pay-single-modal', 'paySingle', ['number', 'date']);
      /**
       * Pay appraiser function
       */
    } else if (fn === 'payAppraiser') {
      // Determine if record has ACH available
      if (!Service.achExists()) {
        $scope.$broadcast('show-modal', 'no-ach');
        return;
      }
      // ACH available, continue with pay
      $scope.$broadcast('show-modal', 'pay-single-confirm');
    }
  };

  /**
   * Pay single
   */
  vm.payAppraiser = function () {
    // Submit request
    Service.payAppraiserAch()
    .then(function () {
      // Display success modal
      $scope.$broadcast('hide-modal', 'pay-single-confirm');
      $scope.$broadcast('show-modal', 'pay-single-successful');
      // Uncheck checked records
      Service.uncheckRecords(vm.multiSelect);
    })
    .catch(function () {
      // Display failure modal
      $scope.$broadcast('show-modal', 'pay-single-failure');
    });
  };

  /**
   * Import checks
   *
   * @todo Waiting on backend
   */
  vm.importChecksConfirm = function () {
    $scope.$broadcast('show-modal', 'import-checks-modal');
  };
}]);
