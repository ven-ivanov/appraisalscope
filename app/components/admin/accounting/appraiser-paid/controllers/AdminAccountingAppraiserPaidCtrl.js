'use strict';

var app = angular.module('frontendApp');

app.controller('AdminAccountingAppraiserPaidCtrl',
['$scope', 'AdminAccountingDataService', 'AdminAccountingCtrlInheritanceService', 'AdminAccountingConfigService',
 'AdminAccountingAppraiserPaidService', function (
$scope, AdminAccountingDataService, AdminAccountingCtrlInheritanceService, AdminAccountingConfigService,
AdminAccountingAppraiserPaidService) {

  var vm = this;
  var Service = AdminAccountingAppraiserPaidService;
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;
  // Set up reference to current page
  vm.page = AdminAccountingDataService.page;

  /**
   * Inherit from mock controller prototype
   */
  AdminAccountingCtrlInheritanceService.createCtrl.call(this, $scope, Service, 'appraiserPaid');

  /**
   * Get search models
   */
  AdminAccountingConfigService.getAppraiserPaidSearchModels.call(this);

  /**
   * Individual selects for this view
   */
  _.assign(vm.selects, AdminAccountingConfigService.getAppraiserPaidSelects());

  // Table heading
  vm.heading = Service.safeData.heading;

  /**
   * Link function
   */
  vm.linkFn = function (id) {
    // Store selected appraisal
    Service.safeData.selectedAppraisal = id;
    // Show confirmation modal
    $scope.$broadcast('show-modal', 'mark-unpaid');
  };

  /**
   * Submit mark as unpaid invoice
   */
  vm.markUnpaid = function () {
    // Send request
    AdminAccountingAppraiserPaidService.markUnpaid()
    .then(function () {
      // Show success modal
      $scope.$broadcast('hide-modal', 'mark-unpaid');
      $scope.$broadcast('show-modal', 'mark-unpaid-success');
    })
    .catch(function () {
      $scope.$broadcast('hide-modal', 'mark-unpaid');
      $scope.$broadcast('show-modal', 'mark-unpaid-failure');
    });
  };
}]);