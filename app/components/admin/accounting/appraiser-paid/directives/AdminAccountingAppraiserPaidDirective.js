'use strict';

var app = angular.module('frontendApp');

app.directive('adminAccountingAppraiserPaid',
['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/accounting/appraiser-paid/directives/partials/appraiser-paid.html',
    controller: 'AdminAccountingAppraiserPaidCtrl',
    controllerAs: 'tableCtrl',
    scope: true,
    link: function (scope) {
      // Inherit shared methods
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);