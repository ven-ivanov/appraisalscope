'use strict';

var app = angular.module('frontendApp');

app.factory('AdminAccountingAppraiserPaidService', ['AdminAccountingDataService', 'AsDataService', function (AdminAccountingDataService, AsDataService) {
  var Service = {
    // Handle records as immutable
    immutable: true,
    safeData: {
      appraiserPaid: {},
      // Selected appraisal
      selectedAppraisal: '',
      // Selected records
      selectedRecords: [],
      // Table definition
      heading: [
        // File number
        {
          label: 'File Number',
          data: 'file'
        },
        // Client name
        {
          label: 'Client',
          data: 'clientName'
        },
        // Borrower name
        {
          label: 'Borrower',
          data: 'borrower'
        },
        // Borrower address
        {
          label: 'Address',
          data: 'address'
        },
        // Order date
        {
          label: 'Order Date',
          data: 'createdAt'
        },
        // Completed date
        {
          label: 'Completed Date',
          data: 'completedAt'
        },
        // Paid date
        {
          label: 'Paid date',
          data: 'paidDate'
        },
        // Client fee
        {
          label: 'Client Fee',
          data: 'clientFee',
          grandTotal: 'clientFeeGrandTotal',
          pageTotal: 'clientFeePageTotal'
        },
        // Appraiser fee
        {
          label: 'Appraiser Fee',
          data: 'appFee',
          grandTotal: 'appraiserFeeGrandTotal',
          pageTotal: 'appraiserFeePageTotal'
        },
        // P/L Amount
        {
          label: 'P/L Amount',
          data: 'plAmount',
          grandTotal: 'plAmountGrandTotal',
          pageTotal: 'plAmountPageTotal'
        },
        {
          label: 'Check Number',
          data: 'checkNumber'
        },
        // Mark as unpaid
        {
          label: 'Mark as unpaid',
          noSort: true,
          linkLabel: 'Mark as unpaid',
          fn: 'markAsUnpaid'
        }
      ],
      // Transformers
      transformers: {
        // Table
        table: {
          clientName: {
            source: 'client.name'
          },
          borrower: {
            source: 'property.contacts.borrower.fullName'
          },
          address: {
            source: 'property.location.address1'
          },
          createdAt: {
            filter: 'date'
          },
          completedAt: {
            filter: 'date'
          },
          paidDate: {
            source: 'payment.payable.paidAt',
            filter: 'date'
          },
          clientFee: {
            filter: 'currency',
            source: 'payment.receivable.fee'
          },
          appFee: {
            filter: 'currency',
            source: 'payment.payable.fee'
          },
          plAmount: {
            filter: 'currency',
            source: 'payment.amount.pl'
          },
          checkNumber: {
            source: 'payment.payable.profile.check.number'
          }
        },
        // Page and grand totals
        totals: {
          clientFeeGrandTotal: {
            filter: 'currency',
            source: 'summary.receivable.fee.total'
          },
          clientFeePageTotal: {
            filter: 'currency',
            source: 'summary.receivable.fee.pageTotal'
          },
          appraiserFeeGrandTotal: {
            filter: 'currency',
            source: 'summary.payable.fee.total'
          },
          appraiserFeePageTotal: {
            filter: 'currency',
            source: 'summary.payable.fee.pageTotal'
          },
          plAmountGrandTotal: {
            filter: 'currency',
            source: 'summary.amount.pl.total'
          },
          plAmountPageTotal: {
            filter: 'currency',
            source: 'summary.amount.pl.pageTotal'
          }
        }
      }
    },
    displayData: {
      // Data
      appraiserPaid: []
    },
    // Transformers
    transformers: {},
    /**
     * Mark appraisal as appraiser unpaid
     */
    markUnpaid: function () {
      var appraisalId = Service.safeData.selectedAppraisal;
      return Service.request('markUnpaid', {url: {appraisalId: Service.safeData.selectedAppraisal}, body: {paid: false}})
      .then(function () {
        Service.removeAppraisal(appraisalId);
      });
    }
  };

  Service.beforeInitFormat = AdminAccountingDataService.beforeInitFormat.bind(Service);
  Service.request = AsDataService.request.bind(Service, 'AccountingResourceService', 'appraiserPaid');
  Service.loadRecords = AsDataService.loadRecords.bind(Service, {
    resourceService: 'AccountingResourceService',
    type: 'appraiserPaid'
  });
  Service.hashDataImmutable = AsDataService.hashDataImmutable.bind(Service);
  Service.formatDataImmutable = AsDataService.formatDataImmutable.bind(Service, 'appraiserPaid');
  Service.formatSingleImmutable = AsDataService.formatSingleImmutable.bind(Service, 'appraiserPaid');
  Service.transformDataImmutable = AsDataService.transformDataImmutable.bind(Service);
  Service.transformData = AsDataService.transformData.bind(Service);
  // Go to page
  Service.pageLoadCallback = AdminAccountingDataService.pageLoadCallback.bind(Service, 'appraiserPaid');
  Service.getRequestedPage = AdminAccountingDataService.getRequestedPage.bind(Service, Service.pageLoadCallback);
  // Change grouping
  Service.changeGrouping = AdminAccountingDataService.changeGrouping.bind(Service);
  // Get page and grand totals
  Service.getTotals = AdminAccountingDataService.getTotals.bind(Service);
  // Remove appraisal from table
  Service.removeAppraisal = AdminAccountingDataService.removeAppraisalImmutable.bind(Service, 'appraiserPaid');

  return Service;
}]);