describe('AdminAccountingAppraiserPaidService', function () {
  var scope, httpBackend, Service, adminAccountingDataService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminAccountingDataService', AdminAccountingDataServiceMock);
    });
  });

  beforeEach(inject(function (AdminAccountingAppraiserPaidService, AdminAccountingDataService, $rootScope, $httpBackend) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    Service = AdminAccountingAppraiserPaidService;
    adminAccountingDataService = AdminAccountingDataService;

    spyOn(Service, 'hashDataImmutable');
    spyOn(Service, 'formatDataImmutable');

    httpBackend.whenPATCH(/.*/).respond();
  }));

  describe('markUnpaid', function () {
    beforeEach(function () {
      spyOn(Service, 'removeAppraisal');
      spyOn(Service, 'request').and.callThrough();
      Service.safeData.selectedAppraisal = '1';
      Service.markUnpaid();
      httpBackend.flush();
      scope.$digest();
    });

    it('should make the right request', function () {
      expect(Service.request).toHaveBeenCalledWith('markUnpaid', { url: { appraisalId: '1' }, body: { paid: false } });
    });
    it('should remove the selected appraisal', function () {
      expect(Service.removeAppraisal).toHaveBeenCalledWith('1');
    });
  });
});