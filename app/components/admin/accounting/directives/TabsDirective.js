'use strict';

var app = angular.module('frontendApp');

app.directive('adminAccountingTabs', [function () {
  return {
    restrict: 'E',
    templateUrl: '/components/admin/accounting/directives/partials/tabs.html',
    controller: 'AdminAccountingTabsCtrl',
    controllerAs: 'adminAccountingTabsCtrl'
  };
}]);