'use strict';

var app = angular.module('frontendApp');

app.directive('importChecks', [function () {
  return {
    templateUrl: '/components/admin/accounting/directives/partials/import-checks.html'
  };
}]);