'use strict';

var app = angular.module('frontendApp');

app.directive('payMulti', [function () {
  return {
    templateUrl: '/components/admin/accounting/directives/partials/pay-multi.html'
  };
}]);