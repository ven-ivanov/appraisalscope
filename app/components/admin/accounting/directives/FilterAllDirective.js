'use strict';

var app = angular.module('frontendApp');

app.directive('filterAll', [function () {
  return {
    templateUrl: '/components/admin/accounting/directives/partials/filter-all.html'
  };
}]);