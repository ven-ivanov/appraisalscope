'use strict';

var app = angular.module('frontendApp');

app.controller('AdminAccountingTabsCtrl',
['$stateParams', '$scope', 'AdminAccountingDataService', function ($stateParams, $scope, AdminAccountingDataService) {
  var vm = this;
  /**
   * Available tabs for the admin accounting state
   * @type {{name: string, value: number, tab: string}[]}
   */
  vm.tabs =
  [
    {name: 'unpaid', tab: 'unpaid', page: 1},
    {name: 'paid', tab: 'paid', page: 1},
    {name: 'invoices', tab: 'invoices', page: 1},
    {name:'appraiser unpaid', tab: 'appraiser-unpaid', page: 1},
    {name:'pending ach', tab: 'pending-ach', page: 1},
    {name:'appraiser paid', tab: 'appraiser-paid', page: 1},
    {name:'quickbooks', tab: 'quickbooks', page: 1},
    {name:'commissions', tab: 'commissions', page: 1},
    {name:'aging balance', tab: 'aging-balance', page: 1},
    {name:'transaction summary', tab: 'transaction-summary', page: 1}
  ];

  /**
   * Get tab values
   */
  AdminAccountingDataService.getTabValues().then(function (response) {
    angular.forEach(vm.tabs, function (tab) {
      tab.value = response.data[tab.tab];
    });
  });

  /**
   * Set active class on currently active tab
   * @param tab
   * @returns {string}
   */
  vm.tabClass = function (tab) {
    if (tab.tab === $scope.activeTab) {
      return 'col-xs-6 active';
    }
    return 'col-xs-6';
  };
}]);