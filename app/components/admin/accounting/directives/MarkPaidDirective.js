'use strict';

var app = angular.module('frontendApp');

app.directive('markPaid', [function () {
  return {
    templateUrl: '/components/admin/accounting/directives/partials/mark-paid.html',
    link: function (scope, element, attrs) {
      // Conditionally hide the amount input
      scope.hide = attrs.hide;
    }
  };
}]);