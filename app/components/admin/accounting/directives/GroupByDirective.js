'use strict';

var app = angular.module('frontendApp');

/**
 * Admin accounting views group by functionality
 */
app.directive('groupBy', [function () {
  return {
    templateUrl: '/components/admin/accounting/directives/partials/group-by.html',
    transclude: true
  };
}]);