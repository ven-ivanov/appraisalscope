'use strict';

/**
 * Mocking AdminAccountingDataService
 * @param $http
 * @param $q
 */
var AdminAccountingDataServiceMock = function ($http, $q, $filter) {
  // Hold observer callbacks
  var dataServiceCallbacks = [];

  return {
    data: [],
    safeData: [],
    displayData: {
      formatOptions: {}
    },
    // Current page
    page: {
      page: 1
    },
    // Pagination currently taking place
    paginationInProgress: false,
    maxPages: 3,
    /**
     * Register observers
     * @param callback
     */
    registerCallback: function (callback) {
      dataServiceCallbacks.push(callback);
    },
    /**
     * Notify all registered observers
     */
    notifyObservers: function () {
      angular.forEach(dataServiceCallbacks, function (callback) {
        callback();
      });
    },
    /**
     * Mock table data
     * @returns {*}
     */
    getTableData: function (tab) {
      // Pagination in progress
      this.paginationInProgress = true;
      return $http.get('/v2.0/admin/accounting/data/1/' + tab);
    },
    /**
     * Store table data and notify observers
     */
    setData: function (data) {
      this.data = data;
      this.notifyObservers();
    },
    /**
     * Mock apply filters
     * @returns {*}
     */
    applyFilters: function (data) {
      return $q(function (resolve) {
        angular.forEach(data, function (thisData) {
          // Apply filters
          if (angular.isDefined(thisData.filter)) {
            thisData.data = $filter(thisData.filter)(thisData.data);
          }
        });
        resolve(data);
      });
    },
    /**
     * Retrieve grand total
     * @returns {{}}
     */
    getGrandTotal: function () {
      return $q(function (resolve) {
        resolve({});
      });
    },
    /**
     * Get headers
     * @returns {*}
     */
    getHeaders: function () {
      return $q(function (resolve) {
        resolve({});
      });
    },
    getSorters: function () {
      return $q(function (resolve) {
        resolve({});
      });
    },
    getPageTotal: function () {
      return $q(function (resolve) {
        resolve({});
      });
    },
    getBalanceDue: function () {
      return $q(function (resolve) {
        resolve({});
      });
    },
    changeGroup: function () {
      return $q(function (resolve) {
        resolve({});
      });
    },
    getPage: function () {
      return $q(function (resolve) {
        resolve({data: {
          sNo: 2,
          client: 'Next page client'
        }});
      });
    },
    sort: function () {
      return $q(function (resolve) {
        resolve({});
      });
    },
    payMulti: function () {

    },
    getPages: function (view) {
      return $http.get('/v2.0/admin/accounting/pages/' + view);
    },
    pagesDisplayed: function (pages) {
      return pages === this.maxPages ? this.maxPages : pages + 1;
    },
    // We won't be changing URL during testing
    changeUrl: function () {
      return '';
    },
    getRequestedPage: function () {

    },
    changeGrouping: function () {

    },
    beforeInitFormat: function () {

    },
    getTotals: function () {

    },
    uncheckRecords: function () {

    },
    removeAppraisalImmutable: function () {

    },
    pageLoadCallback: function () {

    }
  }
};