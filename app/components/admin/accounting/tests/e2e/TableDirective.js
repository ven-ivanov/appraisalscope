'use strict';

var backend = null,
helper = new global.Helper(),
urlHelper = new global.UrlHelper(),
loginHelper = new global.LoginHelper(),
adminAccountingHelper = new global.AdminAccountingHelper();

describe('Admin accounting unpaid view', function () {
  // Allow GET requests
  beforeEach(function () {
    backend = new global.HttpBackend(browser);
    backend.whenGET(/.*/).passThrough();
  });

  // Make sure the browser isn't too big, so infinite scroll isn't triggered
  beforeEach(function () {
    browser.driver.manage().window().maximize();
  });
  // Clear backend after each test
  afterEach(function() {
    backend.clear();
  });

  it('should allow the user to login', function () {
    // Login as admin
    loginHelper.login('admin', backend, urlHelper);
  });

  it('should go to the admin accounting section', function () {
    urlHelper.switchTab(2);
  });

  it('should begin on the unpaid tab', function () {
    urlHelper.testUrl('accounting/unpaid/1');
  });

  describe('table rows', function () {
    it('should load twenty rows (pages 1 and 2) initially', function () {
      var rows = element.all(by.repeater('data in tableCtrl.tableData | filter:tableCtrl.filterAll track by $index'));
      expect(rows.count()).toBe(20);
    });
  });

  describe('infinite load', function () {
    it('should do infinite loading when the user scrolls down', function () {
      // Rows plus totals
      browser.executeScript('window.scrollTo(0,document.body.scrollHeight);').then(function () {
        expect(element.all(by.repeater('data in tableCtrl.tableData | filter:tableCtrl.filterAll track by $index')).count()).toBe(30);
      })
    });
  });

  describe('filtering', function () {
    it('should filter date columns according to date', function () {
      adminAccountingHelper.matchCell(5, /.*?\/.*?\/.*/);
    });

    it('should filter currency columns according to currency', function () {
      adminAccountingHelper.matchCell(7, /\$\d*\.\d*/);
    });

    it('should make uppercase columns uppercase', function () {
      adminAccountingHelper.matchCell(1, /[0-9A-Z]*/);
    });

    it('should continue to filter data on infinite load', function () {
      browser.executeScript('window.scrollTo(0,document.body.scrollHeight);').then(function () {
        adminAccountingHelper.matchCell(5, /.*?\/.*?\/.*/, 25);
        adminAccountingHelper.matchCell(7, /\$\d*\.\d*/, 25);
        adminAccountingHelper.matchCell(1, /[0-9A-Z]*/, 25);
      })
    });

    it('should filter data on sort', function () {
      // Scroll down to hide the headroom area
      browser.executeScript('window.scrollTo(0,document.body.scrollHeight);').then(function () {
        $$('thead tr').get(2).$$('th').get(0).click();
        adminAccountingHelper.matchCell(5, /.*?\/.*?\/.*/);
        adminAccountingHelper.matchCell(7, /\$\d*\.\d*/);
        adminAccountingHelper.matchCell(1, /[0-9A-Z]*/);
      });
    });
  });

  describe('page total', function () {
    it('should have the page total title in the first cell', function () {
      expect($$('[page-total] td').get(0).getText()).toBe('Page Total');
    });

    it('should display the page total for columns which specify it', function () {
      expect($$('[page-total] td').get(7).getText()).toMatch(/\$\d*/);
    });
  });

  describe('grand total', function () {
    it('should have the grant total title in the first cell', function () {
      expect($$('[grand-total] td').get(0).getText()).toBe('Grand Total');
    });

    it('should display the grand total for columns which specify it', function () {
      expect($$('[grand-total] td').get(7).getText()).toMatch(/\$\d*/);
    });
  });

  /**
  * @todo
  */
  describe('change grouping', function () {
    it('should change the display to display data by company', function () {

    });
  });

  describe('sorting', function () {
    it('should allow the user to sort by column', function () {
      // Reorder
      var sorter = $$('thead tr').get(2).$$('th').get(0);
      sorter.click();
      // Make sure the first data cell is 1
      expect($$('as-table tbody tr').get(0).$$('td').get(0).getText()).toBe('1');
      sorter.click();
      // Should be reversed
      expect($$('as-table tbody tr').get(0).$$('td').get(0).getText()).toBe('10');
      // Go back to normal ordering
      sorter.click();
      expect($$('as-table tbody tr').get(0).$$('td').get(0).getText()).toBe('1');
    });
  });

  describe('manual pagination', function () {
    it('should have a list of page numbers displayed', function () {
      var pageNumbers = element.all(by.repeater('page in pageNumbers track by $index'));
      // The first page should be 1
      expect(pageNumbers.get(0).getText()).toBe('1');
      // Click page 10
      pageNumbers.get(9).click();
      // Expect that the row number of the top element is 81 (page 9: 81-90, page 10: 91-100, page 11: 101-110)
      adminAccountingHelper.matchCell(0, /81/);
      // Expect that the last row number is 110
      adminAccountingHelper.matchCell(0, /110/, 29);
      // Check that the URL is correct
      urlHelper.testUrl('accounting/unpaid/10');
      // Check that scrolling down produces the expected result
      browser.executeScript('window.scrollTo(0,document.body.scrollHeight);').then(function () {
        // First row should be 91
        adminAccountingHelper.matchCell(0, /91/);
        // Last row should be 120
        adminAccountingHelper.matchCell(0, /120/, 29);
      });
      // Check that scrolling up produces the expected result
      browser.executeScript('window.scrollTo(0,0);').then(function () {
        // First row should be 91
        adminAccountingHelper.matchCell(0, /81/);
        // Last row should be 120
        adminAccountingHelper.matchCell(0, /110/, 29);
      });
    });
  });

  describe('load on page', function () {
    it('should display the correct data set when loading a URL requesting a specific page', function () {
      browser.get('/#/accounting/unpaid/10');
      // Expect that the row number of the top element is 81 (page 9: 81-90, page 10: 91-100, page 11: 101-110)
      adminAccountingHelper.matchCell(0, /81/);
      // Expect that the last row number is 110
      adminAccountingHelper.matchCell(0, /110/, 29);
      // Check that the URL is correct
      urlHelper.testUrl('accounting/unpaid/10');
      // Check that scrolling down produces the expected result
      browser.executeScript('window.scrollTo(0,document.body.scrollHeight);').then(function () {
        // First row should be 91
        adminAccountingHelper.matchCell(0, /91/);
        // Last row should be 120
        adminAccountingHelper.matchCell(0, /120/, 29);
      });
      // Check that scrolling up produces the expected result
      browser.executeScript('window.scrollTo(0,0);').then(function () {
        // First row should be 91
        adminAccountingHelper.matchCell(0, /81/);
        // Last row should be 120
        adminAccountingHelper.matchCell(0, /110/, 29);
      });
    });
  });
});
