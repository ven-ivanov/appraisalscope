'use strict';

describe('AdminAccountingDataService', function () {
  var scope, Service, httpBackend, tableData, dataColumns, headers, treeData, asDataService;

  beforeEach(function () {
    module('frontendApp');
  });

  beforeEach(inject(function ($rootScope, AdminAccountingDataService, $httpBackend, AsDataService) {
    // Get a new scope
    scope = $rootScope.$new();
    // Inject data service
    Service = AdminAccountingDataService;
    // Httpbackend for handling requests
    httpBackend = $httpBackend;
    asDataService = AsDataService;
    tableData = [{
                   id: 1, appFee: 300.50, clientFee: 400.50, balanceDue: 100.50
                 }, {
                   id: 2, appFee: 100.00, clientFee: 200.00, balanceDue: 200.50
                 }];
    // Respond with this fake data when retrieving page1
    httpBackend.whenGET(/.*/).respond(tableData);
    // Get table data
    //Service.getTableData('unpaid').then(function (data) {
    //  // Set data
    //  Service.setData(data.data);
    //});
    //httpBackend.flush();
    //scope.$digest();
  }));

  xdescribe('getTableData()', function () {
    it('should retrieve the table data', function () {
      expect(angular.equals(Service.data, tableData)).toBe(true);
    });
  });

  xdescribe('applyFilters()', function () {
    var data;
    // Filter data
    beforeEach(function () {
      data = Service.data;
      Service.applyFilters(data);
    });

    it('should apply filters to application fee and client fee in each record', function () {
      expect(data[0].appFee).toBe('$300.50');
      expect(data[1].appFee).toBe('$100.00');
      expect(data[0].clientFee).toBe('$400.50');
      expect(data[1].clientFee).toBe('$200.00');
    });

    it('should not apply filters to balance due, since it is a dynamic field', function () {
      expect(data[0].balanceDue).toBe(100.5);
      expect(data[1].balanceDue).toBe(200.5);
    });
  });

  xdescribe('getHeaders()', function () {
    it('should use a non-existant column as a non-data column', function () {
      dataColumns = ['IDontExist'];
      headers = Service.getHeaders(dataColumns);
      // Expect 1 non-data column
      expect(headers.length).toBe(1);
      expect(angular.equals(headers[0], {label: 'IDontExist', data: null})).toBe(true);
    });

    it('should return the proper table headers', function () {
      dataColumns = ['appFee', 'clientFee'];
      headers = Service.getHeaders(dataColumns);
      // Make sure 2 were returned
      expect(headers.length).toBe(2);
      // Check them individually
      expect(angular.equals(headers[0], {label: 'Application Fee', data: 'appFee', filter: 'currency'})).toBe(true);
      expect(angular.equals(headers[1], {label: 'Client Fee', data: 'clientFee', filter: 'currency'})).toBe(true);
    });
  });

  xdescribe('getSorters()', function () {
    it('should throw an error if an invalid sorter is requested', function () {
      dataColumns = ['FAKE_FAKE_FAKE'];
      expect( function(){ Service.getSorters(dataColumns) } ).toThrow(new Error('Could not find data to sort'));
    });

    it('should return the proper headers', function () {
      dataColumns = ['appFee', 'clientFee'];
      var sorters = Service.getSorters(dataColumns);
      angular.forEach(sorters, function (sorter) {
        expect(angular.isFunction(sorter)).toBe(true);
      });
    });
  });

  xdescribe('getPageTotal()', function () {
    it('should retrieve totals for the current data set', function () {
      var pageTotal = Service.getPageTotal(Service.data, {
        appFee: 0, clientFee: 0
      });
      expect(pageTotal.appFee).toBe(400.5);
      expect(pageTotal.clientFee).toBe(600.5);
    });

    it('should calculate a subset of the current data set', function () {
      var pageTotal = Service.getPageTotal(Service.data, {
        appFee: 0, clientFee: 0
      }, 1);
      expect(pageTotal.appFee).toBe(300.5);
      expect(pageTotal.clientFee).toBe(400.5);
    });
  });

  xdescribe('updatePaidRecords()', function () {
    it('should remove paid items from the table data', function () {
      // Remove paid records
      Service.updatePaidRecords([1]);
      // Get updated data
      var modifiedTableData = Service.data;
      expect(modifiedTableData.length).toBe(1);
      expect(angular.equals(modifiedTableData, [{id: 2, appFee: 100, clientFee: 200, balanceDue: 200.5}])).toBe(true);
    });

    it('should update partially paid records', function () {
      Service.updatePaidRecords([1], 'balanceDue', 50.50);
      var tableData = Service.data;
      expect(tableData.length).toBe(2);
      expect(angular.equals(tableData, [{id: 1, appFee: 300.5, clientFee: 400.5, balanceDue: 50},
                                        {id: 2, appFee: 100, clientFee: 200, balanceDue: 200.5}])).toBe(true);
    });

    it('should remove records recursively', function () {
      treeData = [
        {
          client: 'Mr. Client',
          data: [
            {
              id: 1, appFee: 300.50, clientFee: 400.50, balanceDue: 100.50
            },
            {
              id: 2, appFee: 100.00, clientFee: 200.00, balanceDue: 200.50
            }
          ]
        }
      ];
      // Update the data set such that recursion is necessary
      Service.setData(treeData);
      Service.updatePaidRecords([1]);
      expect(angular.equals(Service.data, [
        {"client":"Mr. Client","data":[
          {"id":2,"appFee":100,"clientFee":200,"balanceDue":200.5}
        ]}
      ])).toBe(true);
    });
  });

  xdescribe('updateAppraisalsWithInvoice()', function () {
    it('should update appraisals with invoice info', function () {
      Service.updateAppraisalsWithInvoice({
        appraisalIds: [1],
        invoiceId: 1,
        invoiceDescription: 'This is an invoice',
        invoiceStatus: 'Unpaid',
        invoiceAmount: 'a billion',
        invoiceNumber: 'whatever'
      });

      expect(angular.equals(Service.data, [
        {
          id: 1,
          appFee: 300.5,
          clientFee: 400.5,
          balanceDue: 100.5,
          invoiceId: 1,
          invoiceDescription: 'This is an invoice',
          invoiceStatus: 'Unpaid',
          invoiceAmount: 'a billion',
          invoiceNumber: 'whatever'
        }, {
          id: 2, appFee: 100, clientFee: 200, balanceDue: 200.5
        }])).toBe(true);
    });
  });

  xdescribe('removeUnusedParams()', function () {
    it('should remove parameters which are not used, and leave the rest along', function () {
      var params = Service.removeUnusedParams({
        blah: null,
        bleh: 'hey there',
        other: null,
        me: 'me'
      });

      expect(angular.equals(params, {bleh: 'hey there', me: 'me'})).toBe(true);
    });
  });

  xdescribe('removeAppraisal()', function () {
    it('should remove appraisals by id non-recursively', function () {
      Service.removeAppraisal(1);
      expect(angular.equals(Service.data,
      [{"id":2,"appFee":100,"clientFee":200,"balanceDue":200.5}])).toBe(true);
    });

    it('should remove appraisals by id recursively', function () {
      treeData = [
        {
          client: 'Mr. Client',
          data: [
            {
              id: 1
            },
            {
              id: 2
            }
          ]
        },
        {
          client: 'Mrs. Client',
          data: [
            {
              id: 1
            },
            {
              id: 3
            }
          ]
        }
      ];
      // Set the actual data as the tree
      Service.setData(treeData);
      Service.removeAppraisal(1);
      expect(angular.equals(Service.data, [
        {"client": "Mr. Client", "data": [{"id": 2}]},
        {"client": "Mrs. Client","data": [{"id": 3}]}
      ])).toBe(true);
    });
  });

  xdescribe('filterRecursive()', function () {
    it('should recursively filter out data items which have been deleted from the tree, regardless of depth',
    function () {
      // Set one to null, as remove appraisal would do
      treeData[0].data[0] = null;
      // Update the data to use a tree structure
      var filteredData = Service.filterRecursive(treeData);
      expect(angular.equals([
        {"client": "Mr. Client", "data": [{"id": 2}]},
        {"client": "Mrs. Client", "data": [{"id": 1}, {"id": 3}]}
      ],
      filteredData)).toBe(true);
    });
  });

  xdescribe('nodeIsAppraisal()', function () {
    it('should let me know if a record is an appraisal or not', function () {
      expect(Service.nodeIsAppraisal({something: 'something', id: 1})).toBe(true);
      expect(Service.nodeIsAppraisal({something: 'something', somethingElse: 1})).toBe(false);
    });

    it('should not note a record as an appraisal without a numeric appraisal id', function () {
      expect(Service.nodeIsAppraisal({something: 'something', id: '1'})).toBe(false);
    });
  });

  xdescribe('getSingleAppraisal()', function () {
    it('should return an appraisal if it exists', function () {
      expect(angular.equals(Service.getSingleAppraisal(1),
      {id: 1, appFee: 300.5, clientFee: 400.5, balanceDue: 100.5})).toBe(true);
      expect(angular.equals(Service.getSingleAppraisal(2),
      {id: 2, appFee: 100, clientFee: 200, balanceDue: 200.5})).toBe(true);
    });

    it('should return null if the appraisal doesnt exist', function () {
      expect(Service.getSingleAppraisal(3)).toBeNull();
    });
  });

  xdescribe('uncheckRecords()', function () {
    var multiSelect;
    beforeEach(function () {
      multiSelect = {
        0: true,
        1: true,
        2: true
      }
    });

    it('should set all multiselect models to false', function () {
      Service.uncheckRecords(multiSelect);
      expect(angular.equals({0: false, 1: false, 2: false}, multiSelect)).toBe(true);
    });

    it('should set a subset of multiselect models to false', function () {
      Service.uncheckRecords(multiSelect, [0, 1]);
      expect(angular.equals({0: false, 1: false, 2: true}, multiSelect)).toBe(true);
    });
  });

  xdescribe('difference()', function () {
    var newArray, oldArray;
    beforeEach(function () {
      oldArray = [1, 2, 3];
    });
    it('should return the new array item when adding to an array', function () {
      newArray = [1, 2, 3, 4];
      expect(Service.difference(newArray, oldArray)).toBe(4);
      // Set new array to old
      oldArray = [];
      newArray = [1];
      expect(Service.difference(newArray, oldArray)).toBe(1);
    });

    it('should return the removed array item when removing from array', function () {
      newArray = [1, 2];
      expect(Service.difference(newArray, oldArray)).toBe(3);
      newArray = [];
      oldArray = [1];
      expect(Service.difference(newArray, oldArray)).toBe(1);
    });

    it('should return nothing if arrays are the same', function () {
      expect(Service.difference([1], [1])).toBe(null);
    });
  });

  xdescribe('getBalanceDue()', function () {
    it('should increase and decrease the balance when selecting or deselecting records', function () {
      this.balanceDue = 0;
      Service.getBalanceDue.call(this, [1], [], 'balanceDue');
      expect(this.balanceDue).toBe(100.5);
      Service.getBalanceDue.call(this, [1, 2], [1], 'balanceDue');
      expect(this.balanceDue).toBe(301);
      Service.getBalanceDue.call(this, [1], [1, 2], 'balanceDue');
      expect(this.balanceDue).toBe(100.5);
    });
  });

  describe('transformer methods', function () {
    var service;
    beforeEach(function () {
      service = this;
      service.transformers = {};
      service.safeData = {
        transformers: {
          totals: {
            clientFeeGrandTotal: {
              filter: 'currency',
              source: 'summary.receivable.fee.total'
            }
          },
          table: {
            clientName: {
              source: 'client.name'
            }
          }
        }
      };
      spyOn(asDataService, 'formatPlainObjectData');
    });

    describe('beforeInitFormat', function () {
      beforeEach(function () {
        Service.beforeInitFormat.call(service);
      });

      it('should set the right transformers', function () {
        expect(service.transformers.clientName).toEqual({ source: 'client.name' });
      });
      it('should leave the transformer template alone', function () {
        expect(Object.keys(service.safeData.transformers.table)).toEqual(['clientName']);
      });
    });

    describe('getTotals', function () {
      beforeEach(function () {
        Service.getTotals.call(service);
      });

      it('should set the correct transformers, and then return to the table transformers', function () {
        expect(service.transformers.clientName).toEqual({ source: 'client.name' });
      });
      it('should format as plain object', function () {
        expect(asDataService.formatPlainObjectData).toHaveBeenCalledWith('meta');
      });
    });
  });

  describe('removeAppraisalImmutable', function () {
    beforeEach(function () {
      this.formatSingleImmutable = jasmine.createSpy('formatSingleImmutable');
      this.safeData = {test: Immutable.fromJS({1: {id: 1}, 2: {id: 2}})};
      this.safeData.selectedAppraisal = '1';
      Service.removeAppraisalImmutable.call(this, 'test', '1');
    });

    it('should update safeData', function () {
      expect(this.safeData.test.toJS()).toEqual({ 2: { id: 2 } });
    });
    it('should call formatSingleImmutable', function () {
      expect(this.formatSingleImmutable).toHaveBeenCalled();
    });
    it('should remove selected appraisal', function () {
      expect(this.safeData.selectedAppraisal).toEqual(null);
    });
  });

  describe('pageLoadCallback', function () {
    beforeEach(function () {
      this.hashDataImmutable = jasmine.createSpy('hashDataImmutable');
      this.formatDataImmutable = jasmine.createSpy('formatDataImmutable');
      Service.pageLoadCallback.call(this, 'test', {data: [1,2]});
    });

    it('should hash immutable', function () {
      expect(this.hashDataImmutable).toHaveBeenCalledWith([1,2], 'test');
    });
    it('should format immutable', function () {
      expect(this.formatDataImmutable).toHaveBeenCalled();
    });
  });
});