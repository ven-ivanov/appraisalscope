'use strict';

xdescribe('AdminAccountingCtrl', function () {
  var scope, controller, httpBackend, tableData, stateParams, adminAccountingDataService;

  // Set table data
  tableData = [
    {
      sNo: 1,
      client: 'Some Client'
    },
    {
      data: 100,
      filter: 'currency'
    }
  ];

  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminAccountingDataService', AdminAccountingDataServiceMock);
    });
  });

  beforeEach(inject(function ($rootScope, $controller, $httpBackend, AdminAccountingDataService) {
    // Create a new scope
    scope = $rootScope.$new();
    // Catch backend requests (for getting fake data)
    httpBackend = $httpBackend;
    adminAccountingDataService = AdminAccountingDataService;
    // Respond when retrieving initial table data
    httpBackend.whenGET(/admin\/accounting\/data\/1/).respond(tableData);
    // Set state params
    stateParams = {activeTab: 'unpaid'};
    // Instantiate controller in it
    controller = $controller('AdminAccountingCtrl', {$scope: scope, $stateParams: stateParams});
    // Spy on applyFilters
    spyOn(adminAccountingDataService, 'applyFilters').and.callThrough();
  }));

  it('should show when pagination is in progress', function () {
    expect(adminAccountingDataService.paginationInProgress).toBe(true);
    // Flush backend
    httpBackend.flush();
    // Run digest to resolve promises
    scope.$digest();
    expect(adminAccountingDataService.paginationInProgress).toBe(false);
  });

  describe('controller properties', function () {
    beforeEach(function () {
      // Flush backend
      httpBackend.flush();
      // Run digest to resolve promises
      scope.$digest();
    });

    it('should have three properties set on the controller', function () {
      expect(Object.keys(controller).length).toBe(3);
    });

    it('should immediately set the table data on controller instantiation', function () {
      expect(adminAccountingDataService.data).toEqual([{sNo: 1, client: 'Some Client'}, {data: '$100.00', filter: 'currency'}]);
    });

    it('should set the active tab according to $stateParams', function () {
      expect(controller.activeTab).toBe('unpaid');
    });

    it('should apply filters to the table data as soon as it is retrieved', function () {
      expect(adminAccountingDataService.applyFilters).toHaveBeenCalled();
    });
  });

  describe('get table data', function () {
    beforeEach(function () {
      // Flush backend
      httpBackend.flush();
      // Run digest to resolve promises
      scope.$digest();
    });
    it('should store the unaltered "safe" data', function () {
      expect(adminAccountingDataService.safeData).toEqual(tableData);
    });

    it('should apply filters and store the filtered data for display', function () {
      // Make sure that applyFilters() was called
      expect(adminAccountingDataService.applyFilters).toHaveBeenCalled();
      // Make sure that filtered data is stored for display
      expect(adminAccountingDataService.data).toEqual([{sNo: 1, client: 'Some Client'}, {data: '$100.00', filter: 'currency'}]);
    });

    it('should not have altered the safe data', function () {
      expect(adminAccountingDataService.safeData).toEqual(tableData);
    });
  });
});