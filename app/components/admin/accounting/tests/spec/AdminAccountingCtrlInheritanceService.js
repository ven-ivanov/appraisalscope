'use strict';

/**
 * Since this service is intended explicitly for controller inheritance, I'm going to instantiate a controller
 * and test the functionality there
 */
xdescribe('AdminAccountingCtrlInheritanceService', function () {
  var scope, controller, adminAccountingDataService, httpBackend, stateParams;
  
  var tableData = [
    {
      sNo: 1,
      client: 'Some Client'
    }
  ];

  beforeEach(function () {
    module('frontendApp', function ($provide) {
      // Use AdminAccountingDataServiceMock mock
      $provide.factory('AdminAccountingDataService', AdminAccountingDataServiceMock);
    });
  });
  beforeEach(inject(function ($rootScope, $controller, $httpBackend, AdminAccountingDataService) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    adminAccountingDataService = AdminAccountingDataService;
    // Respond when retrieving initial table data
    httpBackend.whenGET(/admin\/accounting\/data\/1/).respond(tableData);
    // Ignore states
    httpBackend.whenGET(/states/).respond({});
    // Respond with pages
    httpBackend.whenGET(/pages/).respond({pages: 2});
    // Set state params
    stateParams = {activeTab: 'unpaid'};
  }));

  describe('setup', function () {
    var handbookService;
    beforeEach(inject(function ($controller, HandbookService) {
      handbookService = HandbookService;
      spyOn(adminAccountingDataService, 'registerCallback').and.callThrough();
      spyOn(adminAccountingDataService, 'getGrandTotal').and.callThrough();
      spyOn(handbookService, 'query').and.callThrough();
      // Instantiate controller
      controller = $controller('AdminAccountingUnpaidCtrl', {$scope: scope, $stateParams: stateParams});
      httpBackend.flush();
      scope.$digest();
    }));
    it('should immediate register an observer with the data service', function () {
      expect(adminAccountingDataService.registerCallback).toHaveBeenCalled();
    });

    it('should notify observers when data is set', function () {
      spyOn(controller, 'getRowData');
      adminAccountingDataService.setData({});
      expect(controller.getRowData).toHaveBeenCalled();
    });

    it('should immediately request this views grand total', function () {
      expect(adminAccountingDataService.getGrandTotal).toHaveBeenCalled();
    });

    it('should immediately query states from the handbook', function () {
      expect(handbookService.query).toHaveBeenCalled();
    });
  });

  describe('functionality', function () {
    beforeEach(inject(function ($controller) {
      // Instantiate controller
      controller = $controller('AdminAccountingUnpaidCtrl', {$scope: scope, $stateParams: stateParams});
      httpBackend.flush();
      scope.$digest();
    }));

    it('should watch multiselect and keep selectedRecords up to date', function () {
      // Select one
      controller.multiSelect = {
        1: true
      };
      scope.$digest();
      expect(controller.selectedRecords).toEqual([1]);
      // Select another
      controller.multiSelect = {
        1: true,
        2: true
      };
      scope.$digest();
      expect(controller.selectedRecords).toEqual([1, 2]);
      // Deselect one
      controller.multiSelect = {
        1: true,
        2: false
      };
      scope.$digest();
      expect(controller.selectedRecords).toEqual([1]);
    });

    it('should initiate requests on changing grouping', function () {
      spyOn(adminAccountingDataService, 'changeGroup').and.callThrough();
      spyOn(adminAccountingDataService, 'getTableData').and.callThrough();
      // Select one
      controller.groupBy = 'Whatever';
      scope.$digest();
      expect(adminAccountingDataService.changeGroup).toHaveBeenCalledWith('whatever');
      // Change back to all
      controller.groupBy = 'All';
      scope.$digest();
      expect(adminAccountingDataService.getTableData).toHaveBeenCalled();
    });

    describe('nextPage()', function () {
      it('should not run the function is pagination is in progress', function () {
        spyOn(adminAccountingDataService, 'getPage');
        adminAccountingDataService.paginationInProgress = true;
        controller.nextPage();
        expect(adminAccountingDataService.getPage).not.toHaveBeenCalled();
      });

      it('should make a request to get the next page', function () {
        expect(adminAccountingDataService.page.page).toEqual(1);
        spyOn(adminAccountingDataService, 'getPage').and.callThrough();
        controller.nextPage();
        expect(adminAccountingDataService.getPage).toHaveBeenCalledWith(2);
      });

      it('should update data and safe data', function () {
        spyOn(adminAccountingDataService, 'applyFilters').and.callThrough();
        // Get initial table data
        adminAccountingDataService.getTableData().then(function (data) {
          adminAccountingDataService.setData(data.data);
          adminAccountingDataService.paginationInProgress = false;
          // Set safe data
          adminAccountingDataService.safeData = JSON.parse(JSON.stringify(adminAccountingDataService.data));
          controller.nextPage();
        }).then(function () {
          // Here is where filters are applied
        }).then(function () {
          expect([{sNo: 1, client: 'Some Client'}, {sNo: 2, client: 'Next page client'}]).toEqual(adminAccountingDataService.data);
          expect([{sNo: 1, client: 'Some Client'}, {sNo: 2, client: 'Next page client'}]).toEqual(adminAccountingDataService.safeData);
          // Make sure the filtering function was called
          expect(adminAccountingDataService.applyFilters).toHaveBeenCalled();
        });
        httpBackend.flush();
        scope.$digest();
      });
    });

    describe('sort()', function () {
      it('should apply filters to sorted data', function () {
        spyOn(adminAccountingDataService, 'applyFilters').and.callThrough();
        adminAccountingDataService.getTableData().then(function (data) {
          adminAccountingDataService.setData(data.data);
          controller.sort();
        }).then(function () {
          expect(adminAccountingDataService.applyFilters).toHaveBeenCalled();
        });
        httpBackend.flush();
        scope.$digest();
      });
    });
  });
});
