describe('AdminAccountingCommissionsService', function () {
  var scope, Service, adminAccountingDataService, httpBackend, $q;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminAccountingDataService', AdminAccountingDataServiceMock);
    });
  });

  beforeEach(inject(function (AdminAccountingCommissionsService, $rootScope, AdminAccountingDataService, $httpBackend, _$q_) {
    scope = $rootScope.$new();
    $q = _$q_;
    httpBackend = $httpBackend;
    Service = AdminAccountingCommissionsService;
    adminAccountingDataService = AdminAccountingDataService;

    spyOn(Service, 'request').and.callThrough();
    spyOn(adminAccountingDataService, 'removeAppraisalImmutable');

    httpBackend.whenDELETE(/.*/).respond();
  }));

  describe('deleteRecords', function () {
    var multiSelect;
    beforeEach(function () {
      multiSelect = {1: true, 2: false, 3: true};
      Service.safeData.selectedRecords = ['1', '2'];
      Service.deleteRecords(multiSelect);
      httpBackend.flush();
      scope.$digest();
    });

    it('should make the right requests', function () {
      expect(Service.request.calls.count()).toEqual(2);
    });
    it('should request deletion of the first', function () {
      expect(Service.request.calls.argsFor(0)).toEqual(['deleteCommissions', '1']);
    });
    it('should request deletion of the second', function () {
      expect(Service.request.calls.argsFor(1)).toEqual(['deleteCommissions', '2']);
    });
    it('should uncheck all multiselect', function () {
      expect(multiSelect).toEqual({1: false, 2: false, 3: false});
    });
    it('should remove each record', function () {
      expect(adminAccountingDataService.removeAppraisalImmutable.calls.count()).toEqual(2);
    });
    it('should parse int for removing records', function () {
      expect(adminAccountingDataService.removeAppraisalImmutable.calls.argsFor(0)).toEqual([ 'commissions', 1 ]);
    });
  });
});