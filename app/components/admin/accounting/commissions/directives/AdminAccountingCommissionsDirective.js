'use strict';

var app = angular.module('frontendApp');

app.directive('adminAccountingCommissions', [function () {
  return {
    templateUrl: '/components/admin/accounting/commissions/directives/partials/commissions.html',
    controller: 'AdminAccountingCommissionsCtrl',
    controllerAs: 'tableCtrl',
    scope: true
  };
}]);