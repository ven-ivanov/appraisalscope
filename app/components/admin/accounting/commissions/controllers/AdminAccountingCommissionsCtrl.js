'use strict';

var app = angular.module('frontendApp');

app.controller('AdminAccountingCommissionsCtrl',
['$scope', 'AdminAccountingDataService', 'AdminAccountingCtrlInheritanceService', 'AdminAccountingConfigService',
 'AdminAccountingCommissionsService', function (
$scope, AdminAccountingDataService, AdminAccountingCtrlInheritanceService, AdminAccountingConfigService,
AdminAccountingCommissionsService) {

  var vm = this;
  var Service = AdminAccountingCommissionsService;
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;
  // Set up reference to current page
  vm.page = AdminAccountingDataService.page;

  /**
   * Inherit from mock controller prototype
   */
  AdminAccountingCtrlInheritanceService.createCtrl.call(this, $scope, Service, 'commissions');

  /**
   * Get search models
   */
  AdminAccountingConfigService.getCommissionsSearchModels.call(this);

  /**
   * Individual selects for this view
   */
  _.assign(vm.selects, AdminAccountingConfigService.getCommissionsSelects());
  // Table definition
  vm.heading = Service.safeData.heading;

  /**
   * Delete records
   */
  vm.deleteRecordsConfirm = function () {
    // Show confirmation modal
    $scope.$broadcast('show-modal', 'confirm-delete');
  };

  /**
   * Disable delete button
   */
  vm.deleteDisabled = function () {
    return !Service.safeData.selectedRecords.length;
  };

  /**
   * Perform record deletion
   */
  vm.deleteRecords = function () {
    // Perform record deletion
    AdminAccountingCommissionsService.deleteRecords(vm.multiSelect)
    .then(function () {
      // Hide modal
      $scope.$broadcast('hide-modal', 'confirm-delete');
    })
    .catch(function () {
      // Show failure modal
      $scope.$broadcast('hide-modal', 'confirm-delete');
      $scope.$broadcast('show-modal', 'delete-failure');
    });
  };
}]);