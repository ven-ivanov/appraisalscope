'use strict';

var app = angular.module('frontendApp');

app.factory('AdminAccountingCommissionsService',
['AdminAccountingDataService', 'AsDataService', '$q', function (AdminAccountingDataService, AsDataService, $q) {
  var Service = {
    immutable: true,
    safeData: {
      // Data
      commissions: {},
      // Selected appraisal
      selectedAppraisal: '',
      // Selected records
      selectedRecords: [],
      // Table definitions
      heading: [
        // File number
        {
          label: 'File Number',
          data: 'file'
        },
        // Salesperson
        {
          label: 'Salesperson',
          data: 'salesPerson'
        },
        // Client name
        {
          label: 'Client',
          data: 'clientName'
        },
        // Job type
        {
          label: 'Job Type',
          data: 'jobTypeLabel'
        },
        // Borrower name
        {
          label: 'Borrower',
          data: 'borrower'
        },
        // Borrower address
        {
          label: 'Address',
          data: 'address'
        },
        // Order date
        {
          label: 'Order Date',
          data: 'createdAt'
        },
        // Completed date
        {
          label: 'Completed Date',
          data: 'completedAt'
        },
        // Client fee
        {
          label: 'Client Fee',
          data: 'clientFee',
          grandTotal: 'clientFeeGrandTotal',
          pageTotal: 'clientFeePageTotal'
        },
        // Appraiser fee
        {
          label: 'Appraiser Fee',
          data: 'appFee',
          grandTotal: 'appraiserFeeGrandTotal',
          pageTotal: 'appraiserFeePageTotal'
        },
        // P/L Amount
        {
          label: 'P/L Amount',
          data: 'plAmount',
          grandTotal: 'plAmountGrandTotal',
          pageTotal: 'plAmountPageTotal'
        },
        // Commission
        {
          label: 'Commission',
          data: 'commission',
          grandTotal: 'commissionGrandTotal',
          pageTotal: 'commissionPageTotal'
        },
        // Standard multi-select checkbox
        {
          label: '',
          noSort: true,
          checkbox: true
        }
      ],
      transformers: {
        // Table data transformers
        table: {
          salesPerson: {
            source: 'salesperson.name'
          },
          clientName: {
            source: 'client.name'
          },
          jobTypeLabel: {
            source: 'jobType.label'
          },
          borrower: {
            source: 'property.contacts.borrower.fullName'
          },
          address: {
            source: 'property.location.address1'
          },
          createdAt: {
            filter: 'date'
          },
          completedAt: {
            filter: 'date'
          },
          clientFee: {
            filter: 'currency',
            source: 'payment.receivable.fee'
          },
          appFee: {
            filter: 'currency',
            source: 'payment.payable.fee'
          },
          plAmount: {
            filter: 'currency',
            source: 'payment.amount.pl'
          },
          commission: {
            filter: 'currency',
            source: 'payment.amount.commission'
          }
        },
        // Totals data transformers
        totals: {
          clientFeeGrandTotal: {
            filter: 'currency',
            source: 'summary.receivable.fee.total'
          },
          clientFeePageTotal: {
            filter: 'currency',
            source: 'summary.receivable.fee.pageTotal'
          },
          appraiserFeeGrandTotal: {
            filter: 'currency',
            source: 'summary.payable.fee.total'
          },
          appraiserFeePageTotal: {
            filter: 'currency',
            source: 'summary.payable.fee.pageTotal'
          },
          plAmountGrandTotal: {
            filter: 'currency',
            source: 'summary.amount.pl.total'
          },
          plAmountPageTotal: {
            filter: 'currency',
            source: 'summary.amount.pl.pageTotal'
          },
          commissionGrandTotal: {
            filter: 'currency',
            source: 'summary.amount.commission.total'
          },
          commissionPageTotal: {
            filter: 'currency',
            source: 'summary.amount.commission.pageTotal'
          }
        }
      }
    },
    displayData: {
      // Data
      commissions: []
    },
    transformers: {},
    /**
     * Perform commissions records deletion
     */
    deleteRecords: function (multiSelect) {
      var appraisalIds = Service.safeData.selectedRecords;
      var requests = [];
      // Delete as many records as are selected
      angular.forEach(appraisalIds, function (appraisalId) {
        requests.push(Service.request('deleteCommissions', appraisalId));
      });
      // After all requests, delete from table
      return $q.all(requests)
      .then(function () {
        // Uncheck multiselect
        angular.forEach(multiSelect, function (value, key) {
          multiSelect[key] = false;
        });
      })
      .then(function () {
        // Delete each record
        angular.forEach(appraisalIds, function (appraisalId) {
          AdminAccountingDataService.removeAppraisalImmutable.call(Service, 'commissions', parseInt(appraisalId));
        });
      });
    }
  };

  Service.beforeInitFormat = AdminAccountingDataService.beforeInitFormat.bind(Service);
  Service.request = AsDataService.request.bind(Service, 'AccountingResourceService', 'commissions');
  Service.loadRecords = AsDataService.loadRecords.bind(Service, {
    resourceService: 'AccountingResourceService',
    type: 'commissions'
  });
  Service.hashDataImmutable = AsDataService.hashDataImmutable.bind(Service);
  Service.formatDataImmutable = AsDataService.formatDataImmutable.bind(Service, 'commissions');
  Service.formatSingleImmutable = AsDataService.formatSingleImmutable.bind(Service, 'commissions');
  Service.transformDataImmutable = AsDataService.transformDataImmutable.bind(Service);
  Service.transformData = AsDataService.transformData.bind(Service);
  // Go to page
  Service.pageLoadCallback = AdminAccountingDataService.pageLoadCallback.bind(Service, 'commissions');
  Service.getRequestedPage = AdminAccountingDataService.getRequestedPage.bind(Service, Service.pageLoadCallback);
  // Change grouping
  Service.changeGrouping = AdminAccountingDataService.changeGrouping.bind(Service);
  // Get page and grand totals
  Service.getTotals = AdminAccountingDataService.getTotals.bind(Service);
  // Remove appraisal from table
  Service.removeAppraisal = AdminAccountingDataService.removeAppraisalImmutable.bind(Service, 'commissions');

  return Service;
}]);