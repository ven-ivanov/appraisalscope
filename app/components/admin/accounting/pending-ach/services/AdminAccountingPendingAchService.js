'use strict';

var app = angular.module('frontendApp');

app.factory('AdminAccountingPendingAchService',
['AsDataService', 'AdminAccountingDataService', function (AsDataService, AdminAccountingDataService) {
  var Service = {
    // Handle records as immutable
    immutable: true,
    safeData: {
      pendingAch: {},
      // meta data immutable
      meta: '',
      transformers: {
        table: {
          clientName: {
            source: 'client.name'
          },
          borrower: {
            source: 'property.contacts.borrower.fullName'
          },
          address: {
            source: 'property.location.address1'
          },
          createdAt: {
            filter: 'date'
          },
          completedAt: {
            filter: 'date'
          },
          clientFee: {
            filter: 'currency',
            source: 'payment.receivable.fee'
          },
          appFee: {
            filter: 'currency',
            source: 'payment.payable.fee'
          },
          plAmount: {
            filter: 'currency',
            source: 'payment.amount.pl'
          }
        },
        // Page and grand totals
        totals: {
          clientFeeGrandTotal: {
            filter: 'currency',
            source: 'summary.receivable.fee.total'
          },
          clientFeePageTotal: {
            filter: 'currency',
            source: 'summary.receivable.fee.pageTotal'
          },
          appraiserFeeGrandTotal: {
            filter: 'currency',
            source: 'summary.payable.fee.total'
          },
          appraiserFeePageTotal: {
            filter: 'currency',
            source: 'summary.payable.fee.pageTotal'
          },
          plAmountGrandTotal: {
            filter: 'currency',
            source: 'summary.amount.pl.total'
          },
          plAmountPageTotal: {
            filter: 'currency',
            source: 'summary.amount.pl.pageTotal'
          }
        }
      },
      heading: [
        // File number
        {
          label: 'File Number',
          data: 'file'
        },
        // Client name
        {
          label: 'Client',
          data: 'clientName'
        },
        // Borrower name
        {
          label: 'Borrower',
          data: 'borrower'
        },
        // Borrower address
        {
          label: 'Address',
          data: 'address'
        },
        // Order date
        {
          label: 'Order Date',
          data: 'createdAt'
        },
        // Completed date
        {
          label: 'Completed Date',
          data: 'completedAt'
        },
        // Client fee
        {
          label: 'Client Fee',
          data: 'clientFee',
          grandTotal: 'clientFeeGrandTotal',
          pageTotal: 'clientFeePageTotal'
        },
        // Appraiser fee
        {
          label: 'Appraiser Fee',
          data: 'appFee',
          grandTotal: 'appraiserFeeGrandTotal',
          pageTotal: 'appraiserFeePageTotal'
        },
        // P/L Amount
        {
          label: 'P/L Amount',
          data: 'plAmount',
          grandTotal: 'plAmountGrandTotal',
          pageTotal: 'plAmountPageTotal'
        }
      ]
    },
    displayData: {
      pendingAch: [],
      meta: ''
    },
    transformers: {},
    /**
     * Update display on new page
     */
    pageLoadCallback: function (response) {
      // Update table
      Service.hashDataImmutable(response.data, 'pendingAch');
      Service.formatDataImmutable();
    }
  };

  Service.request = AsDataService.request.bind(Service, 'AccountingResourceService', 'pendingAch');
  Service.loadRecords = AsDataService.loadRecords.bind(Service, {
    resourceService: 'AccountingResourceService',
    type: 'pendingAch'
  });
  Service.beforeInitFormat = AdminAccountingDataService.beforeInitFormat.bind(Service);
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.hashDataImmutable = AsDataService.hashDataImmutable.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'pendingAch');
  Service.formatDataImmutable = AsDataService.formatDataImmutable.bind(Service, 'pendingAch');
  Service.formatSingle = AsDataService.formatSingle.bind(Service, 'pendingAch');
  Service.transformData = AsDataService.transformData.bind(Service);
  Service.transformDataImmutable = AsDataService.transformDataImmutable.bind(Service);
  // Go to page
  Service.getRequestedPage = AdminAccountingDataService.getRequestedPage.bind(Service, Service.pageLoadCallback);
  // Change grouping
  Service.changeGrouping = AdminAccountingDataService.changeGrouping.bind(Service);
  // Get page and grand totals
  Service.getTotals = AdminAccountingDataService.getTotals.bind(Service);
  // Uncheck multiselect
  Service.uncheckRecords = AdminAccountingDataService.uncheckRecords.bind(null);

  return Service;
}]);