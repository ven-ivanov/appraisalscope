'use strict';

var app = angular.module('frontendApp');

app.controller('AdminAccountingPendingAchCtrl',
['$scope', 'AdminAccountingDataService', 'AdminAccountingCtrlInheritanceService', 'AdminAccountingConfigService',
 'AdminAccountingPendingAchService', function (
$scope, AdminAccountingDataService, AdminAccountingCtrlInheritanceService, AdminAccountingConfigService,
AdminAccountingPendingAchService) {

  var vm = this;
  var Service = AdminAccountingPendingAchService;
  // Set up reference to current page
  vm.page = AdminAccountingDataService.page;

  /**
   * Inherit from mock controller prototype
   */
  AdminAccountingCtrlInheritanceService.createCtrl.call(vm, $scope, Service, 'pendingAch');

  /**
   * Get select boxes
   */
  AdminAccountingConfigService.getPendingAchSearchModels.call(this);

  /**
   * Individual selects for this view
   */
  _.assign(vm.selects, AdminAccountingConfigService.getPendingAchSelects());

  // Table definition
  vm.heading = Service.safeData.heading;
}]);