'use strict';

var app = angular.module('frontendApp');

app.directive('adminAccountingPendingAch', [function () {
  return {
    templateUrl: '/components/admin/accounting/pending-ach/directives/partials/pending-ach.html',
    controller: 'AdminAccountingPendingAchCtrl',
    controllerAs: 'tableCtrl',
    scope: true
  };
}]);