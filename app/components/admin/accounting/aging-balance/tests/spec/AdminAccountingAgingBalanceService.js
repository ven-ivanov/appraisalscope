describe('AdminAccountingAgingBalanceService', function () {
  var scope, httpBackend, adminAccountingDataService, Service, asDataService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminAccountingDataService', AdminAccountingDataServiceMock);
    });
  });

  beforeEach(inject(function (
  AdminAccountingDataService, AdminAccountingAgingBalanceService, $httpBackend, $rootScope, AsDataService) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    adminAccountingDataService = AdminAccountingDataService;
    Service = AdminAccountingAgingBalanceService;
    asDataService = AsDataService;

    httpBackend.whenGET(/.*/).respond({data: []});

    spyOn(Service, 'getTotals');
    spyOn(asDataService, 'loadRecords').and.callThrough();
  }));

  describe('changeGrouping', function () {
    var response;
    var changeGroup = function (type) {
      Service.changeGrouping(type).then(function (res) {
        response = res;
      });
      httpBackend.flush();
      scope.$digest();
    };
    describe('client', function () {
      beforeEach(function () {
        changeGroup('clients');
      });

      it('should make the right request', function () {
        expect(asDataService.loadRecords).toHaveBeenCalled();
      });
      it('should get totals', function () {
        expect(Service.getTotals).toHaveBeenCalled();
      });
      it('should return the proper table definition', function () {
        expect(response[0].label).toEqual('Client');
      });
    });

    describe('appraiser', function () {
      beforeEach(function () {
        changeGroup('appraisers');
      });

      it('should return the proper table definition', function () {
        expect(response[0].label).toEqual('Appraiser');
      });
    });
  });
});