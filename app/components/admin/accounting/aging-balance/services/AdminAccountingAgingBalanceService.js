'use strict';

var app = angular.module('frontendApp');

app.factory('AdminAccountingAgingBalanceService',
['AdminAccountingDataService', 'AsDataService', function (AdminAccountingDataService, AsDataService) {
  var Service = {
    immutable: true,
    safeData: {
      // Data
      aging: {},
      // Grouping
      grouping: 'clients',
      // Table headings
      headings: {
        // Receivable
        receivable: [
          // Client name
          {
            label: 'Client',
            data: 'clientName'
          },
          // Current
          {
            label: 'Current',
            data: 'current',
            grandTotal: 'currentGrandTotal',
            pageTotal: 'currentPageTotal'
          },
          // Over 30
          {
            label: 'Over 30',
            data: 'over30',
            grandTotal: 'over30GrandTotal',
            pageTotal: 'over30PageTotal'
          },
          // Over 60
          {
            label: 'Over 60',
            data: 'over60',
            grandTotal: 'over60GrandTotal',
            pageTotal: 'over60PageTotal'
          },
          // Over 90
          {
            label: 'Over 90',
            data: 'over90',
            grandTotal: 'over90GrandTotal',
            pageTotal: 'over90PageTotal'
          },
          // Balance
          {
            label: 'Balance',
            data: 'balance',
            grandTotal: 'balanceGrandTotal',
            pageTotal: 'balancePageTotal'
          }
        ],
        // Payable
        payable: [
          // Appraiser
          {
            label: 'Appraiser',
            data: 'appraiserName'
          },
          // Appraiser company
          {
            label: 'Appraiser company',
            data: 'appraiserCompany'
          },
          // State
          {
            label: 'State',
            data: 'appraiserState'
          },
          // Current
          {
            label: 'Current',
            data: 'current',
            grandTotal: 'currentGrandTotal',
            pageTotal: 'currentPageTotal'
          },
          // Over 30
          {
            label: 'Over 30',
            data: 'over30',
            grandTotal: 'over30GrandTotal',
            pageTotal: 'over30PageTotal'
          },
          // Over 60
          {
            label: 'Over 60',
            data: 'over60',
            grandTotal: 'over60GrandTotal',
            pageTotal: 'over60PageTotal'
          },
          // Over 90
          {
            label: 'Over 90',
            data: 'over90',
            grandTotal: 'over90GrandTotal',
            pageTotal: 'over90PageTotal'
          },
          // Balance
          {
            label: 'Balance',
            data: 'balance',
            grandTotal: 'balanceGrandTotal',
            pageTotal: 'balancePageTotal'
          }
        ]
      },
      transformers: {
        // Table data
        table: {
          clientName: {
            source: 'client.name'
          },
          current: {
            filter: 'currency'
          },
          over30: {
            filter: 'currency'
          },
          over60: {
            filter: 'currency'
          },
          over90: {
            filter: 'currency'
          },
          balance: {
            filter: 'currency'
          },
          appraiserName: {
            source: 'appraiser.fullName'
          },
          appraiserCompany: {
            source: 'appraiser.company.name'
          },
          appraiserState: {
            source: 'appraiser.certification.certificate.coverage.state.code'
          }
        },
        // Totals data
        totals: {
          currentGrandTotal: {
            filter: 'currency',
            source: 'summary.current.total'
          },
          currentPageTotal: {
            filter: 'currency',
            source: 'summary.current.pageTotal'
          },
          over30GrandTotal: {
            filter: 'currency',
            source: 'summary.over30.total'
          },
          over30PageTotal: {
            filter: 'currency',
            source: 'summary.over30.pageTotal'
          },
          over60GrandTotal: {
            filter: 'currency',
            source: 'summary.over60.total'
          },
          over60PageTotal: {
            filter: 'currency',
            source: 'summary.over60.pageTotal'
          },
          over90GrandTotal: {
            filter: 'currency',
            source: 'summary.over90.total'
          },
          over90PageTotal: {
            filter: 'currency',
            source: 'summary.over90.pageTotal'
          },
          balanceGrandTotal: {
            filter: 'currency',
            source: 'summary.balance.total'
          },
          balancePageTotal: {
            filter: 'currency',
            source: 'summary.balance.pageTotal'
          }
        }
      }
    },
    displayData: {
      // Data
      aging: [],
      // Grouping either by receivable or payable
      groupingOptions: [{id: 'clients', title: 'Accounts Receivable'}, {id: 'appraisers', title: 'Accounts Payable'}]
    },
    transformers: {},
    /**
     * Change grouping between receivable and payable
     */
    changeGrouping: function (type) {
      // Retrieve first page of updated grouping
      return AsDataService.loadRecords.call(Service, {
        resourceService: 'AccountingResourceService',
        type: 'aging',
        params: {
          grouping: type
        }
      })
      .then(function () {
        // Refresh totals
        Service.getTotals();
      })
      .then(function () {
        // Accounts receivable
        if (type === 'clients') {
          return Service.safeData.headings.receivable;
          // Accounts payable
        } else if (type === 'appraisers') {
          return Service.safeData.headings.payable;
        }
      });
    }
  };

  Service.beforeInitFormat = AdminAccountingDataService.beforeInitFormat.bind(Service);
  Service.request = AsDataService.request.bind(Service, 'AccountingResourceService', 'aging');
  Service.loadRecords = AsDataService.loadRecords.bind(Service, {
    resourceService: 'AccountingResourceService',
    type: 'aging',
    params: {
      grouping: 'clients'
    }
  });
  Service.hashDataImmutable = AsDataService.hashDataImmutable.bind(Service);
  Service.formatDataImmutable = AsDataService.formatDataImmutable.bind(Service, 'aging');
  Service.formatSingleImmutable = AsDataService.formatSingleImmutable.bind(Service, 'aging');
  Service.transformDataImmutable = AsDataService.transformDataImmutable.bind(Service);
  Service.transformData = AsDataService.transformData.bind(Service);
  // Go to page
  Service.pageLoadCallback = AdminAccountingDataService.pageLoadCallback.bind(Service, 'aging');
  Service.getRequestedPage = AdminAccountingDataService.getRequestedPage.bind(Service, Service.pageLoadCallback);
  // Get page and grand totals
  Service.getTotals = AdminAccountingDataService.getTotals.bind(Service);

  return Service;
}]);