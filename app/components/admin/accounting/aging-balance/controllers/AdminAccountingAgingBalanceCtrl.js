'use strict';

var app = angular.module('frontendApp');

app.controller('AdminAccountingAgingBalanceCtrl',
['$scope', 'AdminAccountingDataService', 'AdminAccountingConfigService', 'AdminAccountingCtrlInheritanceService',
 'AdminAccountingAgingBalanceService', function (
$scope, AdminAccountingDataService, AdminAccountingConfigService, AdminAccountingCtrlInheritanceService,
AdminAccountingAgingBalanceService) {

  var vm = this;
  var Service = AdminAccountingAgingBalanceService;
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;
  // Set up reference to current page
  vm.page = AdminAccountingDataService.page;

  /**
   * Inherit from mock controller prototype
   */
  AdminAccountingCtrlInheritanceService.createCtrl.call(this, $scope, Service, 'aging');

  /**
   * Get select boxes
   */
  AdminAccountingConfigService.getAgingBalanceSearchModels.call(this);

  /**
   * Individual selects for this view
   */
  _.assign(vm.selects, AdminAccountingConfigService.getAgingBalanceSelects());

  // Begin on receivable
  vm.heading = Service.safeData.headings.receivable;

  /**
   * Switch between accounts receivable and accounts payable
   */
  $scope.$watch(function () {
    return vm.safeData.grouping;
  }, function (newVal, oldVal) {
    // Don't run on load
    if (angular.equals(oldVal, newVal)) {
      return;
    }
    if (newVal === 'appraisers') {
      // Table column headings
      Service.changeGrouping('appraisers')
      .then(function (response) {
        // Change heading and update table
        vm.heading = response;
        $scope.recompileTable();
      });
    } else if (newVal === 'clients') {
      // Retrieve table definition
      Service.changeGrouping('clients')
      .then(function (response) {
        // Change heading and update table
        vm.heading = response;
        $scope.recompileTable();
      });
    }
  });
}]);