'use strict';

var app = angular.module('frontendApp');

app.directive('adminAccountingAgingBalance', ['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/accounting/aging-balance/directives/partials/aging-balance.html',
    controller: 'AdminAccountingAgingBalanceCtrl',
    controllerAs: 'tableCtrl',
    scope: true,
    link: function (scope) {
      // Expose compile table
      DirectiveInheritanceService.inheritDirective(scope);
    }
  };
}]);