'use strict';

var app = angular.module('frontendApp');

app.directive('adminAccountingUnpaid', ['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/accounting/unpaid/directives/partials/unpaid.html',
    controller: 'AdminAccountingUnpaidCtrl',
    controllerAs: 'tableCtrl',
    scope: true,
    link: function (scope) {
      // Import shared methods
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);