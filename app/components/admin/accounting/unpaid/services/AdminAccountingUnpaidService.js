'use strict';

var app = angular.module('frontendApp');

app.factory('AdminAccountingUnpaidService',
['AsDataService', 'AdminAccountingDataService', function (AsDataService, AdminAccountingDataService) {
  var Service = {
    safeData: {
      // Raw data
      unpaid: {},
      // Meta data
      meta: {},
      // Grand totals
      grandTotal: {},
      // Data grouping
      grouping: null,
      // Selected appraisal
      selectedAppraisal: '',
      // Selected records via multiselect
      selectedRecords: [],
      // Group by
      groupBy: '',
      // Table heading
      heading: [
        // File number
        {
          label: 'File Number',
          data: 'file',
          uc: true
        },
        // Client name
        {
          label: 'Client',
          data: 'clientName'
        },
        // Borrower name
        {
          label: 'Borrower',
          data: 'borrower'
        },
        // Borrower address
        {
          label: 'Address',
          data: 'address'
        },
        // Order date
        {
          label: 'Order Date',
          data: 'createdAt'
        },
        // Completed data
        {
          label: 'Completed Date',
          data: 'completedAt'
        },
        // Client fee
        {
          label: 'Client Fee',
          data: 'clientFee',
          grandTotal: 'clientFeeGrandTotal',
          pageTotal: 'clientFeePageTotal'
        },
        // Amount paid
        {
          label: 'Amount Paid',
          data: 'amountPaid',
          grandTotal: 'amountPaidGrandTotal',
          pageTotal: 'amountPaidPageTotal'
        },
        // Balance due
        {
          label: 'Balance Due',
          data: 'balanceDue',
          grandTotal: 'balanceDueGrandTotal',
          pageTotal: 'balanceDuePageTotal'
        },
        // Appraiser fee
        {
          label: 'Appraiser Fee',
          data: 'appFee',
          grandTotal: 'appraiserFeeGrandTotal',
          pageTotal: 'appraiserFeePageTotal'
        },
        // P/L Amount
        {
          label: 'P/L Amount',
          data: 'plAmount',
          grandTotal: 'plAmountGrandTotal',
          pageTotal: 'plAmountPageTotal'
        },
        // Invoice number
        {
          label: 'Invoice Number',
          data: 'invoiceNumber'
        },
        // Mark as paid link function
        {
          label: 'Mark as Paid',
          noSort: true,
          linkLabel: 'Mark as Paid',
          fn: 'markAsPaid'
        },
        // Standard multi-select checkbox
        {
          label: '',
          noSort: true,
          checkbox: true
        }
      ],
      transformers: {
        // Page and grand totals
        totals: {
          clientFeeGrandTotal: {
            filter: 'currency',
            source: 'summary.receivable.fee.total'
          },
          clientFeePageTotal: {
            filter: 'currency',
            source: 'summary.receivable.fee.pageTotal'
          },
          amountPaidGrandTotal: {
            filter: 'currency',
            source: 'summary.receivable.amount.paid.total'
          },
          amountPaidPageTotal: {
            filter: 'currency',
            source: 'summary.receivable.amount.paid.pageTotal'
          },
          balanceDueGrandTotal: {
            filter: 'currency',
            source: 'summary.receivable.amount.due.total'
          },
          balanceDuePageTotal: {
            filter: 'currency',
            source: 'summary.receivable.amount.due.pageTotal'
          },
          appraiserFeeGrandTotal: {
            filter: 'currency',
            source: 'summary.payable.fee.total'
          },
          appraiserFeePageTotal: {
            filter: 'currency',
            source: 'summary.payable.fee.pageTotal'
          },
          plAmountGrandTotal: {
            filter: 'currency',
            source: 'summary.amount.pl.total'
          },
          plAmountPageTotal: {
            filter: 'currency',
            source: 'summary.amount.pl.pageTotal'
          }
        },
        // Table
        table: {
          clientName: {
            source: 'client.name'
          },
          borrower: {
            source: 'property.contacts.borrower.fullName'
          },
          address: {
            source: 'property.location.address1'
          },
          createdAt: {
            filter: 'date'
          },
          completedAt: {
            filter: 'date'
          },
          clientFee: {
            filter: 'currency',
            source: 'payment.receivable.fee'
          },
          amountPaid: {
            filter: 'currency',
            source: 'payment.receivable.amount.paid'
          },
          balanceDue: {
            filter: 'currency',
            source: 'payment.receivable.amount.due'
          },
          appFee: {
            filter: 'currency',
            source: 'payment.payable.fee'
          },
          plAmount: {
            filter: 'currency',
            source: 'payment.amount.pl'
          },
          invoiceNumber: {
            source: 'payment.receivable.invoice.number'
          }
        }
      }
    },
    displayData: {
      // Formatted records
      unpaid: [],
      // Formatted meta
      meta: [],
      // Pay single modal
      paySingle: {
        method: 'check',
        amount: '',
        number: '',
        date: ''
      },
      // Pay multi modal
      payMulti: {
        method: 'check',
        number: '',
        date: '',
        amountDue: 0
      },
      // Generate invoice modal
      generateInvoice: {
        description: '',
        alreadyInvoiced: []
      }
    },
    transformers: {},
    /**
     * Update display on new page
     */
    pageLoadCallback: function (response) {
      // Update table
      Service.hashData(response.data, 'unpaid');
      Service.formatData();
    },
    /**
     * Get balance due for all selected records
     */
    getBalanceDue: function () {
      var balanceDue = 0;
      var unpaidRecords = Service.safeData.unpaid;
      // Iterate selected records
      angular.forEach(Service.safeData.selectedRecords, function (recordId) {
        balanceDue = balanceDue + parseFloat(unpaidRecords[recordId].payment.receivable.amount.due);
      });
      // Update balance due display
      Service.displayData.payMulti.amountDue = balanceDue;
    },
    /**
     * Pay multi
     */
    payMulti: function () {
      // Selected records
      var selectedRecords = Service.safeData.selectedRecords;
      // Make request
      return Service.request('payMulti', {appraisals: selectedRecords}, Service.displayData.payMulti)
      .then(function () {
        // Remove from table
        angular.forEach(selectedRecords, function (appraisalId) {
          delete Service.safeData.unpaid[appraisalId];
          // Reformat just the updated record
          Service.formatSingle(parseInt(appraisalId));
        });
      });
    },
    /**
     * Pay a single appraisal
     */
    markPaid: function () {
      var appraisalId = Service.safeData.selectedAppraisal;
      return Service.request('markPaid', {
        url: {appraisalId: appraisalId},
        body: Service.displayData.paySingle
      }).then(function (response) {
        // Delete
        if (response.payment.receivable.amount.due <= 0) {
          delete Service.safeData.unpaid[appraisalId];
          // Update record
        } else {
          Service.safeData.unpaid[appraisalId] = response;
        }
        // Reformat just the updated record
        Service.formatSingle(appraisalId);
      });
    },
    /**
     * Generate invoices
     *
     * @todo This is fake
     */
    generateInvoice: function () {
      // appraisalId
      return Service.request('generateInvoice', {appraisalId: Service.safeData.selectedRecords.join(',')});
    },
    /**
     * Determine if invoices have already been created for the selected appraisals
     *
     * @todo This is fake, and may be unnecessary
     * @link https://github.com/ascope/manuals/issues/273
     */
    checkGeneratedInvoices: function () {
      return Service.request('checkInvoiced', Service.safeData.selectedRecords.join(','));
    },
    /**
     * Add invoices to selected appraisals
     * @param response DB response from invoice creation
     *
     * @todo Waiting on backend to see which property corresponds with invoices
     */
    updateAppraisalsWithInvoice: function () {

    }
  };

  Service.request = AsDataService.request.bind(Service, 'AccountingResourceService', 'unpaid');
  Service.loadRecords = AsDataService.loadRecords.bind(Service, {resourceService: 'AccountingResourceService', type: 'unpaid'});
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'unpaid');
  Service.formatSingle = AsDataService.formatSingle.bind(Service, 'unpaid');
  Service.transformData = AsDataService.transformData.bind(Service);
  // Go to page
  Service.getRequestedPage = AdminAccountingDataService.getRequestedPage.bind(Service, Service.pageLoadCallback);
  // Change grouping
  Service.changeGrouping = AdminAccountingDataService.changeGrouping.bind(Service);
  // Formatting table
  Service.beforeInitFormat = AdminAccountingDataService.beforeInitFormat.bind(Service);
  // Get page and grand totals
  Service.getTotals = AdminAccountingDataService.getTotals.bind(Service);

  return Service;
}]);