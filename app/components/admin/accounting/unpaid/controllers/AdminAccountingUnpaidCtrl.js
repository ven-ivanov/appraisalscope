'use strict';

var app = angular.module('frontendApp');

app.controller('AdminAccountingUnpaidCtrl',
['$scope', 'AdminAccountingDataService', 'AdminAccountingCtrlInheritanceService', 'AdminAccountingConfigService',
 'HandbookService', 'HANDBOOK_CATEGORIES', 'AdminAccountingUnpaidService', function (
$scope, AdminAccountingDataService, AdminAccountingCtrlInheritanceService, AdminAccountingConfigService,
HandbookService, HANDBOOK_CATEGORIES, AdminAccountingUnpaidService) {

  var vm = this;
  var Service = AdminAccountingUnpaidService;
  // Safe and display data
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;
  // Balance due
  vm.balanceDue = 0;
  // Set up reference to current page
  vm.page = AdminAccountingDataService.page;

   /**
    * Inherit common controller items for admin accounting view
    */
  AdminAccountingCtrlInheritanceService.createCtrl.call(vm, $scope, Service, 'unpaid');

  /**
   * Get select boxes
   */
  AdminAccountingConfigService.getUnpaidSearchModels.call(vm);

  /**
   * Individual selects for this view
   */
  _.assign(vm.selects, AdminAccountingConfigService.getUnpaidSelects());

  // Table definition
  vm.heading = Service.safeData.heading;

  /**
   * Pay a single appraisal
   */
  vm.linkFn = function (id) {
    // Hold reference to appraisal
    vm.safeData.selectedAppraisal = id;
    // Display modal
    $scope.$broadcast('show-modal', 'pay-single-modal');
    // Clear modal on close
    $scope.clearModalOnClose('pay-single-modal', 'paySingle', ['amount', 'number', 'date']);
  };

  /**
   * Disable the pay single modal until validation passes
   */
  vm.paySingleDisabled = function () {
    return !vm.paySingleForm.$valid;
  };

  /**
   * Display the generate invoice modal
   */
  vm.generateInvoice = function () {
    // If no records have been selected, display info modal
    if (!Service.safeData.selectedRecords.length) {
      $scope.$broadcast('show-modal', 'select-records');
      return;
    }
    // Clear the modal when it is being closed
    $scope.clearModalOnClose('generate-invoice-modal', 'generateInvoice', ['description']);
    // Check to see if the selected appraisals already have invoices
    Service.checkGeneratedInvoices().then(function (response) {
      // Unselect the appraisals which already have invoices
      AdminAccountingDataService.uncheckRecords(vm.multiSelect, response.invoiced);
      // Handle response from generating invoice
      vm.generateInvoiceResponse(response);
    });
  };

  /**
   * Handle response after submitting a generate invoice request
   */
  vm.generateInvoiceResponse = function (response) {
    // Invoices have been created for all appraisals
    if (!response.notInvoiced.length) {
      $scope.$broadcast('show-modal', 'all-have-invoices');
      // Invoices have already been created for a subset
    } else if (response.notInvoiced.length && response.invoiced.length) {
      // Show invoice modal, and let the user know some already have invoices
      $scope.$broadcast('show-modal', 'generate-invoice-modal');
      // Let the user know which appraisals have already received invoices
      vm.displayData.generateInvoice.alreadyInvoiced = response.invoiced;
      // All selected appraisals need invoices
    } else {
      $scope.$broadcast('show-modal', 'generate-invoice-modal');
    }
  };

  /**
   * Generate invoices
   */
  vm.submitInvoice = function () {
    // Generate invoices
    Service.generateInvoice().then(function (response) {
      // Hide modal
      $scope.$broadcast('hide-modal', 'generate-invoice-modal');
      // Show success modal
      $scope.$broadcast('show-modal', 'invoices-successful');
      // Update appraisals with invoice information
      Service.updateAppraisalsWithInvoice(response.data);
    }, function () {
      // Error handling
      $scope.$broadcast('show-modal', 'invoices-failure');
    }).finally(function () {
      // Uncheck all multi-select boxes
      AdminAccountingDataService.uncheckRecords(vm.multiSelect, null);
    });
  };
}]);
