'use strict';

var backend = null,
    helper = new global.Helper(),
    urlHelper = new global.UrlHelper(),
    loginHelper = new global.LoginHelper(),
    adminAccountingHelper = new global.AdminAccountingHelper();

describe('Admin accounting unpaid view', function () {
  // Allow GET requests
  beforeEach(function () {
    backend = new global.HttpBackend(browser);
    backend.whenGET(/.*/).passThrough();
  });

  // Set browser to maximum size
  beforeEach(function () {
    browser.driver.manage().window().maximize();
  });
  // Clear backend after each test
  afterEach(function() {
    backend.clear();
  });

  it('should allow the user to login', function () {
    // Login as admin
    loginHelper.login('admin', backend, urlHelper);
  });

  it('should go to the admin accounting section', function () {
    urlHelper.switchTab(2);
  });

  it('should begin on the unpaid tab', function () {
    urlHelper.testUrl('accounting/unpaid/1');
  });

  describe('generate invoice', function () {
    it('should display an error modal when trying to generate invoice with no appraisals selected', function () {
      adminAccountingHelper.clickFunctionButton(2);
      browser.wait(protractor.until.elementIsVisible($('#select-records')));
      expect($('#select-records').isDisplayed()).toBeTruthy();
      // Close the modal
      $('#select-records button').click();
    });

    it('should not let the user create invoices for appraisals which already have invoices', function () {
      adminAccountingHelper.checkRecord(2);
      adminAccountingHelper.clickFunctionButton(2);
      browser.wait(protractor.until.elementIsVisible($('#all-have-invoices')));
      expect($('#all-have-invoices').isDisplayed()).toBeTruthy();
      // Close modal
      $('#all-have-invoices button').click();
    });

    it('should deselect appraisals which already have invoices', function () {
      adminAccountingHelper.checkRecord(0);
      adminAccountingHelper.checkRecord(1);
      // Trigger invoice
      adminAccountingHelper.clickFunctionButton(2);
      // Invoice 1 should be checked
      adminAccountingHelper.getCheckStatus(0).then(function (checked) {
        expect(checked).toBe('true');
      });
      // Invoice 2 should not be checked
      adminAccountingHelper.getCheckStatus(1).then(function (checked) {
        expect(checked).toBe(null);
      });
      // It should let you know that an invoice is already created for 2
      expect($$('#generate-invoice-modal .text-uppercase').get(2).isDisplayed()).toBe(true);
      // One should be shown
      expect(element.all(by.repeater('invoice in alreadyInvoiced track by $index')).count()).toBe(1);
      // It should tell us that appraisal 2 has an invoice
      expect($$('#generate-invoice-modal .divider.col-12').get(2).$$('ol li').get(0).getText()).toBe('Appraisal 2');
    });

    it('should ensure form is filled out before allowing submissions', function () {
      // Make sure submit button is disabled
      expect($('#generate-invoice-modal button[type="submit"]').getAttribute('disabled')).toBe('true');
      // Type invoice description
      $('#generate-invoice-modal textarea').sendKeys('Description');
      // Submit
      $('#generate-invoice-modal button[type="submit"]').click();
      // Wait for modal to be hidden
      browser.wait(protractor.until.elementIsNotVisible($('#generate-invoice-modal')));
      // Check success modal is shown
      browser.wait(protractor.until.elementIsVisible($('#invoices-successful')));
      // Click OK
      $('#invoices-successful button').click();
      browser.wait(protractor.until.elementIsNotVisible($('#invoices-successful')));
      // Make sure invoice 1 isn't checked anymore
      adminAccountingHelper.getCheckStatus(0).then(function (checked) {
        expect(checked).toBe(null);
      });
      // Check invoice number is updated
      adminAccountingHelper.getRecord(0).$$('td').get(12).getText().then(function (text) {
        expect(text.trim()).toBe('INVOICE1');
      });
    });
  });

  describe('pay multi', function () {
    it('should not let the user pay multi until records are selected', function () {
      adminAccountingHelper.clickFunctionButton(3);
      // Show error modal
      helper.elementShown($('#select-records'));
      $('#select-records button').click();
      helper.elementHidden($('#select-records'));
    });

    it('should allow the user to pay multi after invoices selected', function () {
      var firstFile, secondFile;
      // Get file number of first two
      adminAccountingHelper.getRecord(1).$$('td').get(1).getText().then(function (text) {
        firstFile = text.trim();
      });
      adminAccountingHelper.getRecord(1).$$('td').get(1).getText().then(function (text) {
        secondFile = text.trim();
      });
      // Select first two
      adminAccountingHelper.checkRecord(0);
      adminAccountingHelper.checkRecord(1);
      adminAccountingHelper.clickFunctionButton(3);
      // Make sure modal shown
      helper.elementShown($('#pay-multi-modal'));
      // Make sure you can import checks
      $$('#pay-multi-modal button').get(0).click();
      helper.elementShown($('#import-checks-modal'));
      // Close import checks
      $$('#import-checks-modal button').get(1).click();
      helper.elementHidden($('#import-checks-modal'));
      // Make sure submit is disabled
      expect($('#pay-multi-modal button[type="submit"]').getAttribute('disabled')).toBe('true');
      // Input check number
      $('#pay-multi-check-number').sendKeys('11111');
      // Input date
      $('#pay-multi-date').sendKeys('04/15/2015');
      // Submit
      $('#pay-multi-modal button[type="submit"]').click();
      /**
       * SMART TABLE IS FAILING TO UPDATE IN PROTRACTOR -- NEED TO EXPLORE AND DETERMINE WHY -- LOGAN
       */
    });
  });
});
