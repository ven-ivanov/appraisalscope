describe('AdminAccountingUnpaidService', function () {
  var Service, adminAccountingDataService, scope, httpBackend, asDataService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminAccountingDataService', AdminAccountingDataServiceMock);
    });
  });

  beforeEach(inject(function (AdminAccountingUnpaidService, $rootScope, $httpBackend, AdminAccountingDataService, AsDataService) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    Service = AdminAccountingUnpaidService;
    adminAccountingDataService = AdminAccountingDataService;
    asDataService = AsDataService;

    spyOn(asDataService, 'formatPlainObjectData');
    spyOn(Service, 'formatData');
    spyOn(Service, 'hashData');
    spyOn(Service, 'formatSingle');
    spyOn(Service, 'request').and.callThrough();

    Service.safeData.selectedRecords = [1,2];
    Service.safeData.unpaid = {1: {payment: {receivable: {amount: {due: '100.00'}}}}, 2: {payment: {receivable: {amount: {due: '0.10'}}}}, 3: {payment: {receivable: {amount: {due: '100.00'}}}}};
  }));

  describe('pageLoadCallback', function () {
    beforeEach(function () {
      Service.pageLoadCallback({data: []});
    });

    it('should hash data', function () {
      expect(Service.hashData).toHaveBeenCalled();
    });
    it('should format data', function () {
      expect(Service.formatData).toHaveBeenCalled();
    });
  });

  describe('getBalanceDue', function () {
    beforeEach(function () {
      Service.getBalanceDue();
    });

    it('should set the amount due as a total of the selected records', function () {
      expect(Service.displayData.payMulti.amountDue).toEqual(100.1);
    });
  });

  describe('payMulti', function () {
    beforeEach(function () {
      httpBackend.whenPOST(/receivable\/pay/).respond();
      Service.payMulti();
      httpBackend.flush();
      scope.$digest();
    });

    it('should delete selected records', function () {
      expect(Service.safeData.unpaid).toEqual({ 3: { payment: {receivable: { amount: { due: '100.00' } }} } });
    });
    it('should format each record individually', function () {
      expect(Service.formatSingle.calls.count()).toEqual(2);
    });
  });

  describe('markPaid', function () {
    describe('paid completely', function () {
      beforeEach(function () {
        httpBackend.whenPOST(/receivable\/pay/).respond({payment: {receivable: {amount: {due: 0}}}});
        Service.safeData.selectedAppraisal = 1;
        Service.markPaid();
        httpBackend.flush();
        scope.$digest();
      });

      it('should completely delete a paid record', function () {
        expect(Service.safeData.unpaid).toEqual({ 2: { payment: { receivable: {amount: { due: '0.10' }} } }, 3: { payment: { receivable: {amount: { due: '100.00' }} } } });
      });
      it('should format single', function () {
        expect(Service.formatSingle).toHaveBeenCalled();
      });
    });

    describe('partial payment', function () {
      beforeEach(function () {
        httpBackend.whenPOST(/receivable\/pay/).respond({payment: {receivable: {amount: {due: 10}}}});
        Service.safeData.selectedAppraisal = 1;
        Service.markPaid();
        httpBackend.flush();
        scope.$digest();
      });

      it('should completely delete a paid record', function () {
        expect(Service.safeData.unpaid[1].payment.receivable.amount.due).toEqual(10);
      });
      it('should format single', function () {
        expect(Service.formatSingle).toHaveBeenCalled();
      });
    });
  });

  describe('generateInvoice', function () {
    beforeEach(function () {
      Service.generateInvoice();
    });

    it('should make the request with the right params', function () {
      expect(Service.request).toHaveBeenCalledWith('generateInvoice', { appraisalId: '1,2' });
    });
  });

  describe('checkGeneratedInvoices', function () {
    beforeEach(function () {
      Service.checkGeneratedInvoices();
    });

    it('should make the request with the right params', function () {
      expect(Service.request).toHaveBeenCalledWith('checkInvoiced', '1,2');
    });
  });

  describe('updateAppraisalsWithInvoice', function () {

  });
});