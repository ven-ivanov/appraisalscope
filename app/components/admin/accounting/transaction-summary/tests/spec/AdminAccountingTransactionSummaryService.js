describe('AdminAccountingTransactionSummaryService', function () {
  var scope, Service, adminAccountingDataService, httpBackend;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminAccountingDataService', AdminAccountingDataServiceMock);
    });
  });

  beforeEach(inject(function (
  AdminAccountingTransactionSummaryService, AdminAccountingDataService, $httpBackend, $rootScope) {
    scope = $rootScope.$new();
    Service = AdminAccountingTransactionSummaryService;
    adminAccountingDataService = AdminAccountingDataService;
    httpBackend = $httpBackend;
  }));

  describe('beforeInitFormat', function () {
    beforeEach(function () {
      spyOn(Service, 'addIdValue');
      Service.beforeInitFormat();
    });

    it('should set transformers to table', function () {
      expect(Service.transformers[0]).toEqual();
    });
    it('should add ID value to each record', function () {
      expect(Service.addIdValue).toHaveBeenCalled();
    });
  });

  describe('addIdValue', function () {
    var data;
    beforeEach(function () {
      data = [{transaction: {id: 1}}, {transaction: {id: 2}}];
      Service.addIdValue(data);
    });

    it('should add ID value to each record', function () {
      expect(data).toEqual([ { transaction: { id: 1 }, id: 1 }, { transaction: { id: 2 }, id: 2 } ]);
    });
  });

  describe('pageLoadCallback', function () {
    beforeEach(function () {
      spyOn(Service, 'addIdValue');
      spyOn(Service, 'hashDataImmutable');
      spyOn(Service, 'formatDataImmutable');
      Service.pageLoadCallback({data: []});
    });

    it('should add ID value', function () {
      expect(Service.addIdValue).toHaveBeenCalled();
    });
    it('should hash data', function () {
      expect(Service.hashDataImmutable).toHaveBeenCalled();
    });
    it('should format data', function () {
      expect(Service.formatDataImmutable).toHaveBeenCalled();
    });
  });
});