'use strict';

var app = angular.module('frontendApp');

app.directive('adminAccountingTransactionSummary',
['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/accounting/transaction-summary/directives/partials/transaction-summary.html',
    controller: 'AdminAccountingTransactionSummaryCtrl',
    controllerAs: 'tableCtrl',
    scope: true,
    link: function (scope) {
      // Expose compile table
      DirectiveInheritanceService.inheritDirective(scope);
    }
  };
}]);