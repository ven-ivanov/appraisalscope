'use strict';

var app = angular.module('frontendApp');

app.controller('AdminAccountingTransactionSummaryCtrl',
['$scope', 'AdminAccountingDataService', 'AdminAccountingConfigService', 'AdminAccountingCtrlInheritanceService',
 'AdminAccountingTransactionSummaryService', function (
$scope, AdminAccountingDataService, AdminAccountingConfigService, AdminAccountingCtrlInheritanceService,
AdminAccountingTransactionSummaryService) {

  var vm = this;
  var Service = AdminAccountingTransactionSummaryService;
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;
  // Set up reference to current page
  vm.page = AdminAccountingDataService.page;
  // Default to receivable grouping
  vm.safeData.grouping = 'receivable';

  /**
   * Inherit from mock controller prototype
   */
  AdminAccountingCtrlInheritanceService.createCtrl.call(this, $scope, Service, 'transaction');

  /**
   * Get select boxes
   */
  AdminAccountingConfigService.getTransactionSummarySearchModels.call(this);

  /**
   * Individual selects for this view
   */
  _.assign(vm.selects, AdminAccountingConfigService.getTransactionSummarySelects());

  // Initial table definition
  vm.heading = Service.safeData.headings.receivable;

  /**
   * Switch between accounts receivable and accounts payable
   */
  $scope.$watch(function () {
    return vm.safeData.grouping;
  }, function (newVal, oldVal) {
    // Don't run on load
    if (angular.equals(oldVal, newVal)) {
      return;
    }
    // Change heading to payable or receivable
    vm.heading = Service.changeGrouping(newVal);
    // Update table
    $scope.recompileTable();
    // Refresh totals
    Service.getTotals();
  });
}]);