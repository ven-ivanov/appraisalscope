'use strict';

var app = angular.module('frontendApp');

/**
 * @todo Waiting on updates from the backend
 * @link https://github.com/ascope/manuals/issues/324
 */

app.factory('AdminAccountingTransactionSummaryService', ['AsDataService', 'AdminAccountingDataService', function (AsDataService, AdminAccountingDataService) {
  var Service = {
    immutable: true,
    safeData: {
      // Data
      transaction: {},
      // Grouping
      grouping: 'receivable',
      // Table definitions
      headings: {
        // Receivable
        receivable: [
          // File number
          {
            label: 'File Number',
            data: 'file'
          },
          // Client name
          {
            label: 'Client',
            data: 'clientName'
          },
          // Ordered for
          {
            label: 'Ordered for',
            data: 'orderedFor'
          },
          // Transaction date
          {
            label: 'Transaction date',
            data: 'transactionDate'
          },
          // Reference number
          {
            label: 'Reference number',
            data: 'referenceNumber'
          },
          // CC number
          {
            label: 'CC Number',
            data: 'ccNumber'
          },
          // Payment type
          {
            label: 'Payment type',
            data: 'paymentType'
          },
          // Credit
          {
            label: 'Credit',
            data: 'credit'
          },
          // Debit
          {
            label: 'Debit',
            data: 'debit'
          },
          // Transaction remarks
          {
            label: 'Remarks',
            data: 'transactionRemarks'
          }
        ],
        // Payable
        payable: [
        // appraiser
          {
            label: 'Appraiser',
            data: ''
          },
          // appraisalCompany
          {
            label: 'Appraisal Company',
            data: 'appraisalCompany'
          },
          // transactionDate
          {
            label: 'Transaction date',
            data: 'transactionDate'
          },
          // checkNumber
          {
            label: 'Check number',
            data: ''
          },
          // appFee
          {
            label: 'Appraiser fee',
            data: 'appFee',
            grandTotal: 'appraiserFeeGrandTotal',
            pageTotal: 'appraiserFeePageTotal'
          }
        ]
      },
      transformers: {
        table: {
          file: {
            source: 'appraisal.file'
          },
          clientName: {
            source: 'appraisal.client.name'
          },
          transactionDate: {
            source: 'transaction.createdAt',
            filter: 'date'
          },
          referenceNumber: {
            source: 'transaction.reference'
          },
          ccNumber: {
            source: 'transaction.ccn'
          },
          paymentType: {
            source: 'transaction.type'
          },
          credit: {
            source: 'transaction.credit',
            filter: 'currency'
          },
          debit: {
            source: 'transaction.debit',
            filter: 'currency'
          },
          transactionRemarks: {
            source: 'transaction.remarks'
          },
          appraisalCompany: {
            source: 'appraisal.client.name'
          },
          appFee: {
            filter: 'currency',
            source: 'appraisal.payment.payable.fee'
          }
        },
        totals: {
          appraiserFeeGrandTotal: {
            filter: 'currency',
            source: 'summary.payable.fee.total'
          },
          appraiserFeePageTotal: {
            filter: 'currency',
            source: 'summary.payable.fee.pageTotal'
          },
        }
      }
    },
    displayData: {
      // Data
      transaction: [],
      // Grouping options
      groupingOptions: [{id: 'receivable', title: 'Accounts Receivable'}, {id: 'payable', title: 'Accounts Payable'}]
    },
    transformers: {},
    /**
     * Set transformers, apply ID to each transaction
     */
    beforeInitFormat: function (response) {
      // Use table transformers
      Service.transformers = Service.safeData.transformers.table;
      // Add transaction ID to each response object
      Service.addIdValue(response);
    },
    /**
     * Add ID to each response object
     * @param response
     */
    addIdValue: function (response) {
      Lazy(response)
      .each(function (value, key) {
        response[key].id = value.transaction.id;
      });
    },
    /**
     * Handle loading new page
     * @param response
     */
    pageLoadCallback: function (response) {
      // Add ID value to each
      Service.addIdValue(response.data);
      // Update table
      Service.hashDataImmutable(response.data, 'transaction');
      Service.formatDataImmutable();
    },
    /**
     * Change grouping
     * @param type Receivable or payable
     */
    changeGrouping: function (type) {
      return type === 'payable' ? Service.safeData.headings.payable : Service.safeData.headings.receivable;
    }
  };

  Service.request = AsDataService.request.bind(Service, 'AccountingResourceService', 'transaction');
  Service.loadRecords = AsDataService.loadRecords.bind(Service, {
    resourceService: 'AccountingResourceService',
    type: 'transaction'
  });
  Service.hashDataImmutable = AsDataService.hashDataImmutable.bind(Service);
  Service.formatDataImmutable = AsDataService.formatDataImmutable.bind(Service, 'transaction');
  Service.formatSingleImmutable = AsDataService.formatSingleImmutable.bind(Service, 'transaction');
  Service.transformDataImmutable = AsDataService.transformDataImmutable.bind(Service);
  Service.transformData = AsDataService.transformData.bind(Service);
  // Go to page
  Service.getRequestedPage = AdminAccountingDataService.getRequestedPage.bind(Service, Service.pageLoadCallback);
  // Get page and grand totals
  Service.getTotals = AdminAccountingDataService.getTotals.bind(Service);

  return Service;
}]);