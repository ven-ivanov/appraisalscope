'use strict';

var app = angular.module('frontendApp');

app.factory('AdminAccountingPaidService', ['AdminAccountingDataService', 'AsDataService',
function (AdminAccountingDataService, AsDataService) {
  var Service = {
    immutable: true,
    // Safe
    safeData: {
      // Paid appraisals
      paid: {},
      // Selected appraisal
      selectedAppraisal: '',
      // Table definition
      heading: [
        {
          label: 'File Number',
          data: 'file'
        },
        // Client name
        {
          label: 'Client',
          data: 'clientName'
        },
        // Borrower name
        {
          label: 'Borrower',
          data: 'borrower'
        },
        // Borrower address
        {
          label: 'Address',
          data: 'address'
        },
        // Order date
        {
          label: 'Order Date',
          data: 'createdAt'
        },
        // Completed date
        {
          label: 'Completed Date',
          data: 'completedAt'
        },
        // Appraisal paid date
        {
          label: 'Paid Date',
          data: 'paidDate'
        },
        // Client fee
        {
          label: 'Client Fee',
          data: 'clientFee',
          grandTotal: 'clientFeeGrandTotal',
          pageTotal: 'clientFeePageTotal'
        },
        // Appraiser fee
        {
          label: 'Appraiser Fee',
          data: 'appFee',
          grandTotal: 'appraiserFeeGrandTotal',
          pageTotal: 'appraiserFeePageTotal'
        },
        // P/L Amount
        {
          label: 'P/L Amount',
          data: 'plAmount',
          grandTotal: 'plAmountGrandTotal',
          pageTotal: 'plAmountPageTotal'
        },
        // Check number
        {
          label: 'Check Number',
          data: 'checkNumber',
          uc: true
        },
        // Invoice number
        {
          label: 'Invoice Number',
          data: 'invoiceNumber',
          uc: true,
          isStatic: false
        },
        // Change status link function
        {
          label: 'Change Status',
          noSort: true,
          linkLabel: 'Change Status',
          fn: 'changeStatus'
        }
      ],
      // Transformers
      transformers: {
        // Table
        table: {
          clientName: {
            source: 'client.name'
          },
          borrower: {
            source: 'property.contacts.borrower.fullName'
          },
          address: {
            source: 'property.location.address1'
          },
          createdAt: {
            filter: 'date'
          },
          completedAt: {
            filter: 'date'
          },
          paidDate: {
            filter: 'date',
            source: 'payment.receivable.paidAt'
          },
          clientFee: {
            filter: 'currency',
            source: 'payment.receivable.fee'
          },
          // Appraiser fee
          appFee: {
            filter: 'currency',
            source: 'payment.payable.fee'
          },
          // P/L Amount
          plAmount: {
            filter: 'currency',
            source: 'payment.amount.pl'
          },
          // Check number
          checkNumber: {
            source: 'payment.payable.profile.check.number'
          },
          // Invoice number
          invoiceNumber: {
            source: 'payment.receivable.invoice.number'
          }
        },
        // Page, grand totals
        totals: {
          clientFeeGrandTotal: {
            filter: 'currency',
            source: 'summary.receivable.fee.total'
          },
          clientFeePageTotal: {
            filter: 'currency',
            source: 'summary.receivable.fee.pageTotal'
          },
          appraiserFeeGrandTotal: {
            filter: 'currency',
            source: 'summary.payable.fee.total'
          },
          appraiserFeePageTotal: {
            filter: 'currency',
            source: 'summary.payable.fee.pageTotal'
          },
          plAmountGrandTotal: {
            filter: 'currency',
            source: 'summary.amount.pl.total'
          },
          plAmountPageTotal: {
            filter: 'currency',
            source: 'summary.amount.pl.pageTotal'
          }
        }
      }
    },
    // Display
    displayData: {
      // Table data
      paid: [],
      // Change status modal
      changeStatus: {
        amount: '',
        cc: {
          number: '',
          // Display
          expiration: '',
          // Writing
          expireAt: {
            year: '',
            month: ''
          },
          code: ''
        }
      },
      // Tab that the change status modal is on
      changeStatusTab: ''
    },
    // Transformers
    transformers: {},
    /**
     * Get charge amount for change status modal
     * @param id
     */
    initChangeStatus: function (id) {
      // Hold reference to the selected appraisal
      Service.safeData.selectedAppraisal = id;
      // Get charge amount for display
      Service.displayData.changeStatus.amount = Service.safeData.paid[id].payment.receivable.fee;
    },
    /**
     * Mark appraisal as unpaid
     * @returns {*}
     */
    markUnpaid: function () {
      var appraisal = Service.safeData.selectedAppraisal;
      return Service.request('markUnpaid', {appraisalId: appraisal})
      .then(function () {
        // Delete and update table
        delete Service.safeData.paid[appraisal];
        Service.formatSingle(appraisal);
      });
    },
    /**
     * Rebill appraisal
     * @returns {*}
     */
    rebill: function () {
      // Get expiration input
      var expiration = Service.displayData.changeStatus.cc.expiration.split('/');
      // Expiration
      var ccExpire = Service.displayData.changeStatus.cc.expireAt;
      // Separate month and year
      ccExpire.month = expiration[0];
      ccExpire.year = expiration[1];
      return Service.request('rebill', {appraisalId: Service.safeData.selectedAppraisal}, Service.displayData.changeStatus);
    },
    /**
     * Issue refund for appraisal
     * @returns {*}
     */
    refund: function () {
      return Service.request('refund', {appraisalId: Service.safeData.selectedAppraisal},
        {amount: Service.displayData.changeStatus.amount});
    }
  };

  Service.request = AsDataService.request.bind(Service, 'AccountingResourceService', 'paid');
  Service.loadRecords = AsDataService.loadRecords.bind(Service, {resourceService: 'AccountingResourceService', type: 'paid'});
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.hashDataImmutable = AsDataService.hashDataImmutable.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'paid');
  Service.formatDataImmutable = AsDataService.formatDataImmutable.bind(Service, 'paid');
  Service.formatSingle = AsDataService.formatSingle.bind(Service, 'paid');
  Service.formatSingleImmutable = AsDataService.formatSingleImmutable.bind(Service, 'paid');
  Service.transformData = AsDataService.transformData.bind(Service);
  Service.transformDataImmutable = AsDataService.transformDataImmutable.bind(Service);
  // Go to page
  Service.getRequestedPage = AdminAccountingDataService.getRequestedPage.bind(Service,
  AdminAccountingDataService.pageLoadCallback.bind(Service, 'paid'));
  // Change grouping
  Service.changeGrouping = AdminAccountingDataService.changeGrouping.bind(Service);
  // Formatting table
  Service.beforeInitFormat = AdminAccountingDataService.beforeInitFormat.bind(Service);
  // Get page and grand totals
  Service.getTotals = AdminAccountingDataService.getTotals.bind(Service);

  return Service;
}]);