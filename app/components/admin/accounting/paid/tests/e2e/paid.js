'use strict';

var backend = null,
    helpers = new global.UserHelper(),
    formHelper = new global.FormHelper(),
    urlHelper = new global.UrlHelper(),
    loginHelper = new global.LoginHelper(),
    adminAccountingHelper = new global.AdminAccountingHelper();

describe('Admin accounting unpaid view', function () {
  // Allow GET requests
  beforeEach(function () {
    backend = new global.HttpBackend(browser);
    backend.whenGET(/.*/).passThrough();
    backend.whenPATCH(/.*/).respond('');
  });

  // Set browser to maximum size
  beforeEach(function () {
    browser.driver.manage().window().maximize();
  });
  // Clear backend after each test
  afterEach(function() {
    backend.clear();
  });

  it('should allow the user to login', function () {
    // Login as admin
    loginHelper.login('admin', backend, urlHelper);
  });

  it('should go to the admin accounting section', function () {
    urlHelper.switchTab(2);
  });

  it('should allow the user to visit the paid subtab', function () {
    element(by.repeater('tab in adminAccountingTabsCtrl.tabs track by $index').row(1)).click();
    urlHelper.testUrl('accounting/paid/1');
  });

  it('should disply an error modal when the user tries to export data', function () {
    expect($('.export').isDisplayed()).toBeTruthy();
    $('.export').click();
    expect($('#export-data-error').isDisplayed()).toBeTruthy();
    $('#export-data-error .cancel-btn').click();
  });

  describe('Change Status', function () {
    var formElements, tabs;

    it('should have the first table element be appraisal 1', function () {
      adminAccountingHelper.matchCell(0, /1/);
    });

    it('should display "change status" modal window when clicking "change status" in table', function () {
      $$('.table-function').get(0).click();
      // Wait for the form elements to be shown
      browser.wait(protractor.until.elementIsVisible($('#change-status-modal')));
      expect($('#change-status-modal').isDisplayed()).toBeTruthy();
    });

    it('should allow the user to change tabs in the modal', function () {
      tabs = $$('#change-status-modal .form-group.col-md-4');
      // Rebill tab
      tabs.get(1).click();
      expect($$('#change-status-modal .form-group').get(4).$('input').getAttribute('placeholder')).toBe('100.00');
      // Refund tab
      tabs.get(2).click();
      expect($$('#change-status-modal .form-group').count()).toBe(6);
      // Go back to unpaid tab
      tabs.get(0).click();
      expect($$('#change-status-modal input').count()).toBe(0);
    });

    it('should set appraisal to unpaid and remove it from the table', function () {
      // Check that there are 0 form elements displayed at first
      formElements = $$('#change-status-modal input');
      expect(formElements.count()).toBe(0);
      // Click the button
      $('#change-status-modal .signin-btn').click();
      browser.wait(protractor.until.elementIsVisible($('#set-unpaid-success')));
      // Close the success modal
      $('#set-unpaid-success button').click();
      // Check to make sure that item 1 was removed
      adminAccountingHelper.matchCell(0, /2/);
    });

    it('should allow the user to rebill', function () {
      // Reopen the change status modal
      $$('.table-function').get(0).click();
      browser.wait(protractor.until.elementIsVisible($('#change-status-modal')));
      expect($('#change-status-modal').isDisplayed()).toBeTruthy();
      // Get tabs
      tabs = $$('#change-status-modal .form-group.col-md-4');
      // Get the help blocks for validation
      var helpBlocks = $$('#change-status-modal .help-block');
      tabs.get(1).click();
      formElements = $$('#change-status-modal input');
      // One form element should be present
      expect(formElements.count()).toBe(1);
      // Clear and set amount
      formElements.get(0).clear().then(function () {
        // Amount
        formHelper.sendKeysAndCheck(formElements.get(0), 'aaaaaaa');
        // Click away
        $('#change-status-modal h4').click();
        // Check validation is shown
        expect(helpBlocks.get(0).isDisplayed()).toBeTruthy();
        // Clear first
        formElements.get(0).clear().then(function () {
          // Send valid amount
          formHelper.sendKeysAndCheck(formElements.get(0), '100.00');
          // Make sure validation is gone
          expect(helpBlocks.get(0).isDisplayed()).toBeFalsy();
          $$('#change-status-modal button').get(0).click();
          // Make sure the valid rebill modal is shown
          browser.wait(protractor.until.elementIsVisible($('#rebill-success')));
          expect($('#rebill-success').isDisplayed()).toBeTruthy();
        });
      });
    });
  });
});
