describe('AdminAccountingPaidService', function () {
  var Service, adminAccountingDataService, httpBackend, scope;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminAccountingDataService', AdminAccountingDataServiceMock);
    });
  });

  beforeEach(inject(function (AdminAccountingPaidService, AdminAccountingDataService, $rootScope, $httpBackend) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    Service = AdminAccountingPaidService;
    adminAccountingDataService = AdminAccountingDataService;

    spyOn(Service, 'hashData');
    spyOn(Service, 'formatData');
    spyOn(Service, 'formatSingle');
    spyOn(Service, 'request').and.callThrough();

    httpBackend.whenPOST(/.*/).respond();
    httpBackend.whenPATCH(/.*/).respond();
  }));

  describe('initChangeStatus', function () {
    beforeEach(function () {
      Service.safeData.paid[1] = {payment: {receivable: {fee: 100}}};
      Service.initChangeStatus(1);
    });

    it('should set selected appraisal', function () {
      expect(Service.safeData.selectedAppraisal).toEqual(1);
    });
    it('should set payment amount input value to complete amount by default', function () {
      expect(Service.displayData.changeStatus.amount).toEqual(100);
    });
  });

  describe('change status', function () {
    beforeEach(function () {
      Service.safeData.selectedAppraisal = 1;
      Service.safeData.paid = {1: {payment: {receivable: {fee: 100}}}, 2: {payment: {receivable: {fee: 200}}}};
    });

    describe('markUnpaid', function () {
      beforeEach(function () {
        Service.markUnpaid();
        httpBackend.flush();
        scope.$digest();
      });

      it('should delete appraisal from table', function () {
        expect(Service.safeData.paid[1]).toBeUndefined();
      });
      it('should format single', function () {
        expect(Service.formatSingle).toHaveBeenCalled();
      });
    });

    describe('rebill', function () {
      beforeEach(function () {
        Service.displayData.changeStatus.amount = 100.00;
        Service.displayData.changeStatus.cc.expiration = '12/2014';
        Service.displayData.changeStatus.cc.number = '6';
        Service.displayData.changeStatus.cc.code = '187';
        Service.rebill();
      });

      it('should request a rebill', function () {
        expect(Service.request.calls.argsFor(0)[0]).toEqual('rebill');
      });
      it('should select the right appraisal', function () {
        expect(Service.request.calls.argsFor(0)[1].appraisalId).toEqual(1);
      });
      it('should send the right parameters', function () {
        expect(Service.request.calls.argsFor(0)[2]).toEqual({ amount: 100.00, cc: Object({ number: '6', expiration: '12/2014', expireAt: Object({ year: '2014', month: '12' }), code: '187' }) });
      });
    });

    describe('refund', function () {
      beforeEach(function () {
        Service.displayData.changeStatus.amount = 200.00;
        Service.refund();
      });

      it('should request a refund', function () {
        expect(Service.request.calls.argsFor(0)[0]).toEqual('refund');
      });
      it('should select the right appraisal', function () {
        expect(Service.request.calls.argsFor(0)[1].appraisalId).toEqual(1);
      });
      it('should send the right parameters', function () {
        expect(Service.request.calls.argsFor(0)[2]).toEqual({ amount: 200 });
      });
    });
  });
});