'use strict';

var app = angular.module('frontendApp');

app.controller('AdminAccountingPaidCtrl',
['$scope', 'AdminAccountingDataService', 'AdminAccountingCtrlInheritanceService', 'AdminAccountingConfigService',
 'AdminAccountingPaidService', function (
$scope, AdminAccountingDataService, AdminAccountingCtrlInheritanceService, AdminAccountingConfigService,
AdminAccountingPaidService) {
  var vm = this;
  var Service = AdminAccountingPaidService;
  // Set up reference to current page
  vm.page = AdminAccountingDataService.page;
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;

  /**
   * Inherit from mock controller prototype
   */
  AdminAccountingCtrlInheritanceService.createCtrl.call(this, $scope, Service, 'paid');

  /**
   * Get search models
   */
  AdminAccountingConfigService.getPaidSearchModels.call(this);

  /**
   * Individual selects for this view
   */
  _.assign(vm.selects, AdminAccountingConfigService.getPaidSelects());

  // Table definition
  vm.heading = vm.safeData.heading;

  /**
   * Change status tab config
   */
    // Tab config
  vm.changeStatusTabConfig = {
    unpaid: 'Unpaid',
    rebill: 'Rebill',
    refund: 'Refund'
  };
  // Tab config array (for ordering)
  vm.changeStatusTabs = [
    'unpaid',
    'rebill',
    'refund'
  ];
  // Selected tab
  vm.selectedChangeStatusTab = 'unpaid';

  /**
  * Change status form submit functions
  */
  vm.changeStatus = function () {
    switch (vm.selectedChangeStatusTab) {
      case 'unpaid':
        // mark unpaid
        vm.setUnpaid();
        break;
      case 'rebill':
        vm.rebill();
        break;
      // Refund tab function
      case 'refund':
        vm.refund();
        break;
      default:
        throw Error('Could not determine change status tab');
    }
  };

  /**
   * Set appraisal to unpaid
   */
  vm.setUnpaid = function () {
    AdminAccountingPaidService.markUnpaid()
    .then(function () {
      // Show success modal
      $scope.$broadcast('hide-modal', 'change-status-modal');
      $scope.$broadcast('show-modal', 'set-unpaid-success');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'set-unpaid-error', true);
    });
  };
  /**
   * Rebill
   */
  vm.rebill = function () {
    AdminAccountingPaidService.rebill()
    .then(function () {
      $scope.$broadcast('hide-modal', 'change-status-modal');
      $scope.$broadcast('show-modal', 'rebill-success');
    })
    .catch(function () {
      $scope.$broadcast('show-modal', 'rebill-error', true);
    });
  };
  /**
   * Refund
   */
  vm.refund = function () {
    AdminAccountingPaidService.refund()
    .then(function () {
      $scope.$broadcast('hide-modal', 'change-status-modal');
      $scope.$broadcast('show-modal', 'refund-success');
    }, function () {
      $scope.$broadcast('show-modal', 'refund-error', true);
    });
  };

  /**
  * Change status
  * @param id
  */
  vm.linkFn = function (id) {
    // Get charge amount input value
    Service.initChangeStatus(id);
    // Show change status modal
    $scope.$broadcast('show-modal', 'change-status-modal');
    // Reset modal on close
    $scope.resetTabOnClose();
  };
}]);