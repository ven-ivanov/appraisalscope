'use strict';

var app = angular.module('frontendApp');

app.directive('adminAccountingPaid',
['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/accounting/paid/directives/partials/paid.html',
    controller: 'AdminAccountingPaidCtrl',
    controllerAs: 'tableCtrl',
    scope: true,
    link: function (scope, element, attrs, controller) {
      // Import shared methods
      DirectiveInheritanceService.inheritDirective.call(null, scope);

      /**
       * Change back to unpaid tab on close
       */
      scope.resetTabOnClose = function () {
        angular.element('#change-status-modal').on('hidden.bs.modal', function () {
          controller.displayData.changeStatusTab = 'unpaid';
        });
      };

      /**
       * Change tab in the change status modal window
       */
      scope.changeStatusTab = function (tab) {
        controller.displayData.changeStatusTab = tab;
      };
    }
  };
}]);