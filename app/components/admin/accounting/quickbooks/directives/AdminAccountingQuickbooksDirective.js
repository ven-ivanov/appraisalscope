'use strict';

var app = angular.module('frontendApp');

app.directive('adminAccountingQuickbooks', [function () {
  return {
    templateUrl: '/components/admin/accounting/quickbooks/directives/partials/quickbooks.html',
    controller: 'AdminAccountingQuickbooksCtrl',
    controllerAs: 'tableCtrl',
    scope: true
  };
}]);