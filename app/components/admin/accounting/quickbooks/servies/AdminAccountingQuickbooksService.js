'use strict';

var app = angular.module('frontendApp');

/**
 * Service for Quickbooks view of Admin Accounting tab
 */
app.factory('AdminAccountingQuickbooksService',
['AdminAccountingDataService', 'AsDataService', function (AdminAccountingDataService, AsDataService) {
  var Service = {
    immutable: true,
    safeData: {
      // Data
      quickbooks: {},
      // Table definition
      heading: [
        {
          label: 'Queue ID',
          data: 'queueId'
        },
        {
          label: 'QB Action',
          data: 'qbAction'
        },
        {
          label: 'Identity',
          data: 'identity'
        },
        {
          label: 'Request Data',
          data: 'reqData'
        },
        {
          label: 'Priority',
          data: 'priority'
        },
        {
          label: 'Status',
          data: 'status'
        },
        {
          label: 'Message',
          data: 'message'
        },
        {
          label: 'Action',
          data: 'action'
        }
      ]
    },
    displayData: {
      // Data
      quickbooks: []
    },
    transformers: {

    },
    ///**
    // * Initiate a sync to Quickbooks request
    // * @param method
    // */
    //syncToQb: function (method) {
    //  return $http.post(API_PREFIX() + '/v2.0/admin/accounting/sync-to-qb', {method: method});
    //},
    ///**
    // * Initiate a check sync request to Quickbooks
    // */
    //syncChecks: function () {
    //  return $http.post(API_PREFIX() + '/v2.0/admin/accounting/sync-to-qb/checks');
    //},
    ///**
    // * Retrieve Quickbooks settings on page load
    // */
    //getQbSettings: function () {
    //  return $http.get(API_PREFIX() + '/v2.0/admin/accounting/qb-settings');
    //},
    ///**
    // * Update Quickbooks settings
    // */
    //saveQbSettings: function (settings) {
    //  return $http.post(API_PREFIX() + '/v2.0/admin/accounting/qb-settings', settings);
    //},
    ///**
    // * Filter Quickbooks data
    // */
    //updateQbData: function (requestStatus, requestAction) {
    //  return $http.get(API_PREFIX() + '/v2.0/admin/accounting/data/1/requestStatus/' + requestStatus + '/requestAction/' + requestAction);
    //}
  };

  Service.beforeInitFormat = AdminAccountingDataService.beforeInitFormat.bind(Service);
  Service.request = AsDataService.request.bind(Service, 'AccountingResourceService', 'quickbooks');
  Service.loadRecords = AsDataService.loadRecords.bind(Service, {
    resourceService: 'AccountingResourceService',
    type: 'quickbooks'
  });
  Service.hashDataImmutable = AsDataService.hashDataImmutable.bind(Service);
  Service.formatDataImmutable = AsDataService.formatDataImmutable.bind(Service, 'quickbooks');
  Service.formatSingleImmutable = AsDataService.formatSingleImmutable.bind(Service, 'quickbooks');
  Service.transformDataImmutable = AsDataService.transformDataImmutable.bind(Service);
  Service.transformData = AsDataService.transformData.bind(Service);
  // Go to page
  Service.getRequestedPage = AdminAccountingDataService.getRequestedPage.bind(Service, Service.pageLoadCallback);
  // Change grouping
  Service.changeGrouping = AdminAccountingDataService.changeGrouping.bind(Service);
  // Remove appraisal from table
  Service.removeAppraisal = AdminAccountingDataService.removeAppraisalImmutable.bind(Service, 'quickbooks');

  return Service;
}]);