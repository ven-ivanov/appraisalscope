'use strict';

var app = angular.module('frontendApp');

app.controller('AdminAccountingQuickbooksCtrl',
['$scope', 'AdminAccountingDataService', 'AdminAccountingCtrlInheritanceService', 'AdminAccountingConfigService',
 'AdminAccountingQuickbooksService', 'ngProgress', function (
$scope, AdminAccountingDataService, AdminAccountingCtrlInheritanceService, AdminAccountingConfigService,
AdminAccountingQuickbooksService, ngProgress) {

  var vm = this;
  var Service = AdminAccountingQuickbooksService;
  vm.safeData = Service.safeData;
  vm.displayData = Service.displayData;
  // Set up reference to current page
  vm.page = AdminAccountingDataService.page;
  // Request status
  vm.requestStatus = 'In Queue';
  // Request action
  vm.requestAction = 'all';

  /**
   * Inherit from mock controller prototype
   */
  AdminAccountingCtrlInheritanceService.createCtrl.call(this, $scope, Service, 'quickbooks');

  /**
   * Individual selects for this view
   */
  _.assign(vm.selects, AdminAccountingConfigService.getQuickbooksSelects());

  // Table heading
  vm.heading = Service.safeData.heading;

  /**
   * Retrieve Quickbooks settings on page load
   */
  AdminAccountingQuickbooksService.getQbSettings()
  .then(function (response) {
    vm.qbSettings = response.data;
  })
  .catch(function () {
    // @todo Error handling
    vm.qbSettings = {};
  });

  /**
   * Change the method via which syncing occurs
   * @param method
   */
  vm.changeSyncMethod = function (method) {
    vm.qbSettings.syncMethod = method;
  };

  /**
   * Watch both request status and request action, change table data accordingly
   */
  $scope.$watchGroup([function () {
    return vm.requestStatus;
  }, function () {
    return vm.requestAction;
  }], function (newVal, oldVal) {
    // Don't run on page load
    if (angular.equals(newVal, oldVal)) {
      return;
    }
    ngProgress.start();
    /**
     * Update data based on filtering
     */
    AdminAccountingQuickbooksService.updateQbData(newVal[0].replace(' ', '_'), newVal[1].replace(' ', '_')).then(function (response) {
      AdminAccountingDataService.setData(response.data);
    }).finally(function () {
      ngProgress.complete();
    });
  });

  /**
   * Send a request to perform a sync to Quickbooks
   * @param method
   */
  vm.syncToQb = function (method) {
    // Start loading bar
    ngProgress.start();
    var requestType = 'syncToQb';
    if (method === 'checks') {
      requestType = 'syncChecks';
    }
    // Initiate sync request
    AdminAccountingQuickbooksService[requestType](method).then(function () {
      // Success modal
      $scope.$broadcast('show-modal', 'qb-sync-success');
    }, function () {
      // Failure modal
      $scope.$broadcast('show-modal', 'qb-sync-failure');
    }).finally(function () {
      ngProgress.complete();
    });
  };

  /**
   * Update Quickbooks settings
   */
  vm.saveQbSettings = function () {
    // Start progress bar
    ngProgress.start();
    // Save settings
    AdminAccountingQuickbooksService.saveQbSettings(vm.qbSettings).then(function () {
      $scope.$broadcast('show-modal', 'qb-save-success');
    }, function () {
      $scope.$broadcast('show-modal', 'qb-save-failure');
    }).finally(function () {
      // Complete progress bar
      ngProgress.complete();
    });
  };
}]);