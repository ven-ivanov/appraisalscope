/**
 * Created by Venelin on 6/23/2015.
 */
'use strict';
var app = angular.module('frontendApp');
/**
 * @ngdoc function
 * @name frontendApp.controller:AdminStaffAdminRuleProfileCtrl
 * @description
 * # AdminStaffAdminRuleProfileCtrl
 *
 * Controller of the frontendApp
 */
app.controller('AdminStaffAdminRuleProfileCtrl',
  [
    '$scope',
    'ngProgress',
    'AdminStaffCtrlInheritanceService',
    'AdminStaffService',
    'AdminStaffAdminRuleProfileService',
    function ($scope,ngProgress,AdminStaffCtrlInheritanceService, AdminStaffService,AdminStaffAdminRuleProfileService) {
      var vm = this, // This service name is just getting out of hand with the line breaks
      Service = AdminStaffAdminRuleProfileService;

      // Inherit common functions
      AdminStaffCtrlInheritanceService.createCtrl.call(vm, Service, $scope, 'adminRuleProfile');
      // Directory details
      vm.displayData = Service.displayData;
      // Safe data
      vm.safeData = Service.safeData;
      //by default create new profile
      vm.selectedAction = Service.selectedAction;
      //init selected profile
      vm.selectedProfile = Service.selectedProfile;

      //Reference to details on the currently selected admin rule profile
      //displayData.details

      /**
       * Initiate data and state
       */
      vm.init = function () {
        // Set state on load
        // Service.setStateOnLoad($stateParams, vm);
        // Keep reference to which section we're on
        AdminStaffService.section = 'adminRuleProfile';


        /*
        @TODO formatData is not working properly
         */
        Service.getAdminRuleProfiles();
      };
      /**
       * Submit Profile either create new / edit
       */
      vm.submitProfile = function() {
        //get action
        if(vm.selectedAction === 'Create'){
          vm.newProfile();
        } else if (vm.selectedAction === 'Update') {
          if (angular.isDefined(vm.selectedProfile) && (vm.selectedProfile !== null)) {
            //update Profile
            vm.editProfile();
          }
        }
      };
      /**
       * select profile change...
       */
      $scope.$watchCollection(function () {
        return vm.displayData.details.profile;
      }, function (newVal) {
         //new Val is profile ID
         // vm.selectedProfile = angular.copy(newVal);
        if(angular.isDefined(newVal)) {
          vm.selectedProfile = vm.safeData.profiles[newVal.id];
          AdminStaffService.profileId = newVal.id;
        }
        if(angular.isDefined(AdminStaffService.profileId) && AdminStaffService.profileId !== 0){
          //companies...
          Service.getCompanies();
          //states
          Service.getStates();
          //job Type
          Service.getJobType();
          //paid Status
          Service.getPaidStatus();
          //process Status
          Service.getProcessStatus();
        }

      });
      /**
       * These are for deleting admin and reviewer rule sub data deletions on table
       */
      /**
       * Delete company
       */
      vm.deleteCompany =  function() {
        Service.deleteCompany()
          .then(function () {
            $scope.$broadcast('hide-modal', 'admin-rule-profile-delete-company-confirm');
          })
          .catch(function () {
            $scope.$broadcast('show-modal', 'delete-company-failure', true);
          });
      };
      /**
       * Delete Job Type
       */
      vm.deleteJobType = function() {
        Service.deleteJobType()
          .then(function () {
            $scope.$broadcast('hide-modal', 'admin-rule-profile-delete-job-type-confirm');
          })
          .catch(function () {
            $scope.$broadcast('show-modal', 'delete-job-type-failure', true);
          });
      };
      /**
       * Delete state
       */
      vm.deleteState = function() {
        Service.deleteState()
          .then(function () {
            $scope.$broadcast('hide-modal', 'admin-rule-profile-delete-state-confirm');
          })
          .catch(function () {
            $scope.$broadcast('show-modal', 'delete-state-failure', true);
          });
      };
      /**
       * Delete Process Status
       */
      vm.deleteProcessStatus = function() {
        Service.deleteProcessStatus()
          .then(function () {
            $scope.$broadcast('hide-modal', 'admin-rule-profile-delete-process-status-confirm');
          })
          .catch(function () {
            $scope.$broadcast('show-modal', 'delete-process-status-failure', true);
          });
      };
      /**
       * Delete Paid Status
       */
      vm.deletePaidStatus = function() {
        Service.deletePaidStatus()
          .then(function () {
            $scope.$broadcast('hide-modal', 'admin-rule-profile-delete-paid-status-confirm');
          })
          .catch(function () {
            $scope.$broadcast('show-modal', 'delete-paid-status-failure', true);
          });
      };
      // Init controller load
      vm.init();

    }]);
