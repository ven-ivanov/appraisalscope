
'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Rule Profile / Location for profile
 */
app.controller('AdminStaffAdminRuleProfileLocationCtrl',
  ['AdminStaffAdminRuleProfileService', 'AsTableService', '$scope', '$q', 'AdminStaffTableCtrlInheritanceService',
    function (AdminStaffAdminRuleProfileService, AsTableService, $scope, $q, AdminStaffTableCtrlInheritanceService) {
      var vm = this;
      /**
       * @TODO
       */
      var heading =  [
        /*
         * D
         */
        {
          label: 'D',
          linkLabel: '[D]',
          fn: 'deleteState'
        },
        /*
         * [C]
         */
        {
          label: 'C',
          linkLabel: '[C]',
          fn: 'selectCounty'
        },
        {
          label: 'Location',
          data: 'name'
        },
        /*
         * [Round Robin]
         */
        {
          label: 'RB',
          linkLabel: '[Round Robin]',
          fn: 'roundRobin'
        }
      ];
      vm.page = {displayPage: 1};
      // Filter table by property
      vm.filterByProp = {};
      // Filter table by property
      vm.filterByProp = {};

      /**
       * Link functions
        */
        vm.linkFn = function (id, fnName) {
          // Keep reference to selected staff
        vm.displayData.selectedStateId = id;
        if(fnName === 'deleteState'){
          vm.deleteStateModal(id);
        }else if(fnName === 'roundRobin') {
          vm.roundRobin(id);
        }else if(fnName === 'selectCounty') {
          vm.selectCounty(id);
        }
      };

      vm.deleteStateModal = function () {
        $scope.$parent.$broadcast('show-modal','admin-rule-profile-delete-state');
      };
      vm.roundRobin = function() {

      };
      vm.selectCounty = function() {
        $scope.$parent.$broadcast('show-modal','admin-rule-profile-select-county');
      };
      // Inherit table controller methods
      AdminStaffTableCtrlInheritanceService.inherit.call(vm, $scope, AdminStaffAdminRuleProfileService,heading, 'states');

    }]);
