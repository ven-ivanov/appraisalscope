
'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Rule Profile / Job Type for profile
 */
app.controller('AdminStaffAdminRuleProfileJobTypeCtrl',
  ['AdminStaffAdminRuleProfileService', 'AsTableService', '$scope', '$q', 'AdminStaffTableCtrlInheritanceService',
    function (AdminStaffAdminRuleProfileService, AsTableService, $scope, $q, AdminStaffTableCtrlInheritanceService) {
      var vm = this;
      /**
       * @TODO
       */
      var heading =  [
        /*
         * D
         */
        {
          label: 'D',
          linkLabel: '[D]',
          fn: 'deleteJobType'
        },
        {
          label: 'Job Type',
          data: 'label'
        },
        /*
         * [Round Robin]
         */
        {
          label: 'RB',
          linkLabel: '[Round Robin]',
          fn: 'roundRobin'
        }
      ];
      vm.page = {displayPage: 1};
      // Filter table by property
      vm.filterByProp = {};


      /**
       * Link functions
       */
      vm.linkFn = function (id, fnName) {
        // Keep reference to selected staff
        vm.displayData.selectedJobTypeId = id;
        if(fnName === 'deleteJobType'){
          vm.deleteJobTypeModal();
        }else if(fnName === 'roundRobin') {
          vm.roundRobin(id);
        }
      };

      vm.deleteJobTypeModal = function () {
        $scope.$parent.$broadcast('show-modal','admin-rule-profile-delete-job-type');
      };
      vm.roundRobin = function() {

      };


      // Inherit table controller methods
      AdminStaffTableCtrlInheritanceService.inherit.call(vm, $scope, AdminStaffAdminRuleProfileService,heading, 'jobType');

    }]);
