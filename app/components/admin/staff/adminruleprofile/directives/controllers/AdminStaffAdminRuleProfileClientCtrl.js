
'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Rule Profile / Clients for profile
 */
app.controller('AdminStaffAdminRuleProfileClientCtrl',
  ['AdminStaffAdminRuleProfileService', 'AsTableService', '$scope', '$q', 'AdminStaffTableCtrlInheritanceService',
    function (AdminStaffAdminRuleProfileService, AsTableService, $scope, $q, AdminStaffTableCtrlInheritanceService) {
      var vm = this;
      /**
       * @TODO
       */
      var heading = [
        /*
         * D
         */
        {
          label: 'D',
          linkLabel: '[D]',
          fn: 'deleteClient'
        },
        /*
         * [B]
         */
        {
          label: 'B',
          linkLabel: '[B]',
          fn: 'selectBranches'
        },
        /*
         * [BK]
         */
        {
          label: 'BK',
          linkLabel: '[BK]',
          fn: 'selectBrokers'
        },
        /*
         * Client
         */
        {
          label: 'Client',
          data: 'name'
        },
        /*
         * Round Robin
         */
        {
          label: 'RB',
          linkLabel: '[Round Robin]',
          fn: 'roundRobin'
        },
        /*
         * Use Location Rule
         */
        {
          label: 'LR',
          linkLabel: '[use location rule]',
          // When 'location rule' click, it deletes client
          fn: 'useLocationRule'
        }
      ];

      // Safe data and table heading
      vm.safeData = AdminStaffAdminRuleProfileService.safeData;

      //vm.heading = AdminStaffAdminRuleProfileService.safeData.heading;

      vm.page = {displayPage: 1};

      // Filter table by property
      vm.filterByProp = {};
      // Inherit table controller methods
      /**
       * Link functions
       */
      vm.linkFn = function (id, fnName) {
        // Keep reference to selected staff
        vm.displayData.selectedCompanyId = id;
        if (fnName === 'selectBranches') {
          vm.selectBranches(id);
        } else if (fnName === 'selectBrokers') {
          vm.selectBrokers(id);
        } else if (fnName === 'deleteClient') {
          vm.deleteClientModal(id);
        } else if (fnName === 'roundRobin') {
          vm.roundRobin(id);
        } else if (fnName === 'useLocationRule') {
          vm.useLocationRule(id);
        }

      };
      //select branches
      vm.selectBranches = function () {
        $scope.$parent.$broadcast('show-modal', 'admin-rule-profile-add-branch');
      };
      vm.selectBrokers = function () {
        $scope.$parent.$broadcast('show-modal', 'admin-rule-profile-add-broker');
      };
      vm.deleteClientModal = function () {
        $scope.$parent.$broadcast('show-modal', 'admin-rule-profile-delete-company-confirm');
      };
      vm.roundRobin = function () {

      };
      vm.useLocationRule = function () {

      };

      AdminStaffTableCtrlInheritanceService.inherit.call(vm, $scope, AdminStaffAdminRuleProfileService,heading, 'companies');

    }]);
