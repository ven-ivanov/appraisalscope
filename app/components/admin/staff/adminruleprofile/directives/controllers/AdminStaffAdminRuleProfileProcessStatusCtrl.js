
'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Rule Profile / Process Status for profile
 */
app.controller('AdminStaffAdminRuleProfileProcessStatusCtrl',
  ['AdminStaffAdminRuleProfileService', 'AsTableService', '$scope', '$q', 'AdminStaffTableCtrlInheritanceService',
    function (AdminStaffAdminRuleProfileService, AsTableService, $scope, $q, AdminStaffTableCtrlInheritanceService) {
      var vm = this;
      /**
       * @TODO
       */
      var heading =  [
        /*
         * D
         */
        {
          label: 'D',
          linkLabel: '[D]',
          fn: 'deleteProcessStatus'
        },
        {
          label: 'Process Status',
          data:  'label'
      },
        /*
         * [Round Robin]
         */
      {
        label: 'RB',
        linkLabel: '[Round Robin]',
        fn: 'roundRobin'
      }
      ];
      vm.page = {displayPage: 1};
      // Filter table by property
      vm.filterByProp = {};
      // Filter table by property
      vm.filterByProp = {};

      /**
       * Link functions
       */
      vm.linkFn = function (id, fnName) {
        // Keep reference to selected staff
        vm.displayData.selectedProcessStatusId = id;
        if(fnName === 'deleteProcessStatus'){
          vm.deleteProcessStatusModal();
        }else if(fnName === 'roundRobin') {
          vm.roundRobin(id);
        }
      };

      vm.deleteProcessStatusModal = function () {
        $scope.$parent.$broadcast('show-modal','admin-rule-profile-delete-process-status');
      };
      vm.roundRobin = function() {

      };
      // Inherit table controller methods
      AdminStaffTableCtrlInheritanceService.inherit.call(vm, $scope, AdminStaffAdminRuleProfileService,heading, 'processStatus');

    }]);
