/**
 * Created by Venelin on 6/11/2015.
 */
'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffAdminRuleProfile',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/staff/adminruleprofile/directives/partials/admin-rule-profile.html',
    controller:'AdminStaffAdminRuleProfileCtrl',
    controllerAs:'adminStaffAdminRuleProfileCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
