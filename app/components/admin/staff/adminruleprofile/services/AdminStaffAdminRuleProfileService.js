/**
 * Created by Venelin on 6/23/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Staff - AdminStaffAdminRuleProfileService
 */
app.factory('AdminStaffAdminRuleProfileService',
  ['API_PREFIX', 'AdminStaffService','AdminStaffResourceService','AsDataService', function (API_PREFIX, AdminStaffService,AdminStaffResourceService,AsDataService) {
    var Service = {
      // Safe data
      safeData: {
        //Admin Rule profiles
        profiles: [],
        //table configuration
        heading:[],
        //companies to add,
        companies: [],
        //states to add
        states: [],
        //process status
        processStatus: [],
        //Job Type,
        jobType: [],
        //Paid Status
        paidStatus:[]
      },
      // Display data
      displayData: {
        //Admin Rule profiles
        profiles: [],
        //new profile
        newProfile : {},
        // Details,
        details: {},
        //table configuration
        heading:[],
        //companies to add,
        companies: [],
        //states to add
        states: [],
        //process status
        processStatus: [],
        //Job Type,
        jobType: [],
        //Paid Status
        paidStatus:[],
        //contentTitle
        contentTitle: '',
        //selected action
        selectedAction: 'Create',
        //selected profile
        selectedProfile: {},
        //selected Company Id
        selectedCompanyId: 0,
        //selected branch Id
        selectedBranchId: 0,
        //selected broker Id
        selectedBrokerId: 0,
        //selected job type Id
        selectedJobTypeId: 0,
        //selected paid status Id
        selectedPaidStatusId: 0,
        //selected process status Id
        selectedProcessStatusId:0,
        //selected state Id
        selectedStateId:0
      },
      // Data transformations
      transformers: {
        // Registered data
        registered: {
          filter: 'date'
        }
      },
      /**
       * Request wrapper
       */
      request: function (requestFn, params) {
        // Get $resource configuration
        return AdminStaffResourceService.adminRuleProfile[requestFn](params);
      },
      /**
       * Retrieve admin rule profiles for selected ones
       */
      getAdminRuleProfiles: function () {
        return Service.request('init')
          .then(function (response) {
            // Hash profiles
            Service.hashData(response.data, 'profiles');
            //Service.formatData('profiles');
            /**
             * @TODO formatData is not syncing safeData and displayData
             */
            Service.displayData.profiles = response.data;
          });
      }
    };
    /**
     * Inherit from AdminStaffService
     */
    Service.loadRecords = AdminStaffService.loadRecords.bind(Service, 'adminRuleProfile', 'profiles');
    Service.hashData = AsDataService.hashData.bind(Service);
    Service.formatData = AsDataService.formatData.bind(Service, 'adminRuleProfile');
    Service.formatCompaniesData = AsDataService.formatData.bind(Service, 'companies');
    Service.formatStatesData = AsDataService.formatData.bind(Service, 'states');
    Service.formatPaidStatusData = AsDataService.formatData.bind(Service, 'paidStatus');
    Service.formatProcessStatusData = AsDataService.formatData.bind(Service, 'processStatus');
    Service.formatJobTypeData = AsDataService.formatData.bind(Service, 'jobType');
    Service.transformData = AsDataService.transformData.bind(Service);
    // Get record details
   /* Service.getRecordDetails =
      AdminStaffService.getRecordDetails.bind(Service, 'adminRuleProfile',
          Service.profiles);*/
    // Update record
    //Service.updateRecord = AdminStaffService.updateRecord.bind(Service, 'reviewerRuleProfile');

    //Profile Management
    Service.newProfile = AdminStaffService.newProfile.bind(Service,'adminRuleProfile');
    Service.updateProfile = AdminStaffService.updateProfile.bind(Service,'adminRuleProfile');
    Service.deleteProfile = AdminStaffService.deleteProfile.bind(Service,'adminRuleProfile');

    //Company Management
    Service.addCompanies = AdminStaffService.addCompanies.bind(Service);
    Service.getCompanies = AdminStaffService.getCompanies.bind(Service);
    Service.deleteCompany = AdminStaffService.deleteCompany.bind(Service);

    //State Management
    Service.getStates = AdminStaffService.getStates.bind(Service);
    Service.addStates = AdminStaffService.addStates.bind(Service);
    Service.deleteState = AdminStaffService.deleteState.bind(Service);

    //Paid Status Management
    Service.getPaidStatus = AdminStaffService.getPaidStatus.bind(Service);
    Service.addPaidStatus = AdminStaffService.addPaidStatus.bind(Service);
    Service.deletePaidStatus = AdminStaffService.deletePaidStatus.bind(Service);

    //Process Status Management
    Service.getProcessStatus = AdminStaffService.getProcessStatus.bind(Service);
    Service.addStatus = AdminStaffService.addStatus.bind(Service);
    Service.deleteProcessStatus = AdminStaffService.deleteProcessStatus.bind(Service);

    //Job Type Management
    Service.getJobType = AdminStaffService.getJobType.bind(Service);
    Service.addJobTypes = AdminStaffService.addJobTypes.bind(Service);
    Service.deleteJobType = AdminStaffService.deleteJobType.bind(Service);

    return Service;
  }]);

