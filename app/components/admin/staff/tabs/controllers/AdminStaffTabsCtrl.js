'use strict';

var app = angular.module('frontendApp');

/**
 * Tabs for Admin Staff section
 * Inherited by Venelin
 */
app.controller('AdminStaffTabsCtrl',
['$stateParams', '$scope', 'AdminStaffService', function ($stateParams, $scope, AdminStaffService) {
  var vm = this;
  /**
   * Available tabs for the admin users state
   * @type {{name: string, value: number, tab: string}[]}
   */
  vm.tabs =
  [
    {name: 'staff',value: 'staff', tab: 'staff', page: '/', query: ''},
    {name: 'admin-rule-profile',value: 'admin rule profile', tab: 'admin-rule-profile', page: '/', query: ''},
    {name: 'reviewer-rule-profile', value: 'reviewer rule profile', tab: 'reviewer-rule-profile', page: '/', query: ''},
    {name: 'realec-failure-orders', value: 'realec failure orders', tab: 'realec-failure-orders', page: '/', query: ''},
    {name: 'mercury-failure-orders',value: 'mercury failure orders', tab: 'mercury-failure-orders', page: '/', query: ''},
  ];

  // Keep any query parameters previously set by loading a record
  AdminStaffService.setTabs(vm.tabs);

  /**
   * Get tab values
   */
  AdminStaffService.getTabValues(vm.tabs);
  /**
   * Set active class on currently active tab
   * @param tab
   * @returns {string}
   */
  vm.tabClass = function (tab) {
    return AdminStaffService.getTabClass(tab, $scope.activeTab);
  };
}]);
