'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users tabs directive
 */
app.directive('adminStaffTabs', [function () {
  return {
    restrict: 'E',
    templateUrl: '/components/admin/staff/tabs/directives/partials/tabs.html',
    controller: 'AdminStaffTabsCtrl',
    controllerAs: 'adminStaffTabsCtrl'
  };
}]);
