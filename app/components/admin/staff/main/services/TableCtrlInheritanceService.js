'use strict';

var app = angular.module('frontendApp');

/**
 * Inherit common table controller functions for each sub-view main staff table
 * This to follow the guideline suggested by Logan
 * Staff has tabs,of each has sub table
 */
app.factory('AdminStaffTableCtrlInheritanceService', ['AdminStaffService','AdminStaffStaffService','DomLoadService',
  function (AdminStaffService, AdminStaffStaffService ,DomLoadService) {
    return {
      // Inherit
      inherit: function ($scope, Service, heading, type, multiSelect, loadParams) {
        var vm = this;
        // Safe data
        vm.safeData = Service.safeData;

        //display data
        vm.displayData = Service.displayData;
        /**
         * I do believe I should actually be defining the table on the service - this is here for
         * variable arguments until I get it switched over
         */
        if (!angular.isObject(heading)) {
          loadParams = type;
          type = heading;
          vm.heading = vm.safeData.heading;
        } else {
          // Set table heading
          vm.safeData.heading = vm.heading = heading;
        }

        /**
         * Init data load
         */
        vm.init = function () {
          Service.loadRecords(loadParams);
        };

        /**
         * Watch table data
         */
        $scope.$watchCollection(function () {
          return Service.displayData[type];
        }, function (newVal) {
          if (!angular.isArray(newVal) || !newVal.length) {
            return;
          }
          // Table data
          vm.tableData = newVal;
          vm.rowData = vm.tableData.slice();
          if (multiSelect) {
            // Check the boxes which are currently active
            vm.multiSelect = Service.checkGroups(newVal);
          }
        });

        // If multiselect is being used in the table, watch those values
        if (multiSelect) {
          /**
           * Watch which records are checked, and update the backend
           */
          $scope.$watchCollection(function () {
            return vm.multiSelect;
          }, function (newVal, oldVal) {
            // Don't write on load
            if (angular.isUndefined(newVal) || angular.isUndefined(oldVal)) {
              return;
            }
            // Write to the backend

            // We have to deal with individual backend operation for group action
           /* Service.updatePermissions(type, newVal)
              .catch(function () {
                $scope.$emit('show-modal', 'general-update-failure', true);
              });*/
          });
        }

        /**
         * Delete Staff Modal
         */
        vm.deleteStaffModal = function () {
          // Wait for modal, then display it
          DomLoadService.load().then(function () {
            $scope.$parent.$broadcast('show-modal', 'delete-staff-confirm');
          });
        };
        /**
         * Edit Staff Modal
         */
        vm.updateStaffModal = function () {
          // Wait for modal, then display it
          DomLoadService.load().then(function () {
            $scope.$parent.$broadcast('show-modal', 'new-staff');
          });
        };
        // Init data loading
        vm.init();
      }
    };
  }]);
