'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff base data service
 * Inherited from Admin Users section by Venelin 6/12/15
 *
 */
app.factory('AdminStaffService',
['$http','API_PREFIX', '$state', '$filter', '$injector', '$resource','$q', function (
$http, API_PREFIX,  $state, $filter,  $injector, $resource,$q
) {
  var AdminStaffService = {
    // profile currently being worked with
    profileId: 0,
    // branch currently being worked with
    branchId: 0,
    // broker currently being worked with
    brokerId :0,
    // company profile currently being worked with
    companyId: 0,
    // staff currently being worked with
    staffId: 0,
    // For holding safe data when mixing sections (such as viewing user details from within company view)
    safeData: {
      staff: {}
    },
    /**
     * Watch for changes to the current record
     */
    detachUpdateWatcher: '',
    attachUpdateWatcher: function ($scope, service) {
      var vm = this, $timeout, ngProgress, REQUEST_DELAY;
      // Update staff info when typing
      AdminStaffService.detachUpdateWatcher = $scope.$watchCollection(function () {
        return service.displayData.details;
      }, function (newVal, oldVal) {
        if (angular.isUndefined(newVal) || angular.isUndefined(oldVal) || angular.equals(newVal, oldVal) || !Object.keys(oldVal).length) {
          return;
        }
        // Get dependencies
        $timeout = $injector.get('$timeout');
        ngProgress = $injector.get('ngProgress');
        REQUEST_DELAY = $injector.get('REQUEST_DELAY');
        if (vm.typing) {
          $timeout.cancel(vm.typing);
        }
        // Wait and then make the request
        vm.typing = $timeout(function () {
          ngProgress.start();
          // Update record
          service.updateRecord(newVal)
          .finally(function () {
            ngProgress.complete();
          });
          vm.typing = null;
        }, REQUEST_DELAY.ms);
      });
    },
    /**
     * Retrieve count for each tab
     * @param tabs
     */
    getTabValues: function (tabs) {
      angular.forEach(tabs, function(){
        //tab.value = 1001; // jsut in c  ase, we 're not using this value
      });
    },
    /**
     * Set active class on active tab
     * @returns {string}
     */
    getTabClass: function (tab, activeTab) {
      if (tab.tab === activeTab) {
        return 'col-md-3 active';
      }
      return 'col-md-3';
    },
    /**
     * Change URL to maintain state
     */
    changeUrl: function (state) {
      $state.transitionTo('.', state, {location: true, inherit: true, relative: $state.$current, notify: false});
    },
    /**
     * Set ID val for hashdata
     * @param idVal
     * @returns {*}
     */
    setIdVal: function (idVal) {
      // If idVal isn't set,  orif a callback is passed as the third argument, return 'id'
      if (angular.isUndefined(idVal) || angular.isFunction(idVal)) {
        return 'id';
      }
      // Return value for idVal
      return idVal;
    },
    /**
     * Retrieve a transformer key by value
     *
     * @todo - This creates a potential conflict in case there are duplicate values
     *
     * @param transformer
     * @param value
     * @returns {string}
     */
    getTransformerKeyByValue: function (transformer, value) {
      for (var prop in transformer) {
        if (transformer.hasOwnProperty(prop) && transformer[prop] === value) {
          return prop;
        }
      }
      return value;
    },
    /**
     * Get record details
     */
    getRecordDetails: function (type, callback, recordId) {
      var service = this;
      // Variable argument length
      if (angular.isUndefined(recordId) && (angular.isNumber(callback) || !isNaN(parseInt(callback)))) {
        recordId = callback;
        callback = null;
      }
      // Ensure that transform data is defined
      if (!angular.isFunction(service.transformData)) {
        throw Error('getRecordDetails requires transformData to be defined');
      }
      return $q(function (resolve) {
        var state = {};
        // Set state based on type
        state[type] =  recordId;
        // Keep reference to inspector ID
        service[type + 'Id'] = recordId;
        // Keep ID on AdminStaffService
        AdminStaffService.id = recordId;
        // Store records -- This is likely redundant and will be refactored out
        if(angular.isDefined(service.safeData[type][recordId])){
          AdminStaffService.record = angular.copy(service.safeData[type][recordId]);
        }

        // Keep type, if it's already been defined
        //record is 'undefined at the moment, need to check.
        //AdminStaffService.record._type = angular.isDefined(AdminStaffService.record._type) ? AdminStaffService.record._type : type;
        // Change URL
        AdminStaffService.changeUrl(state);
        // Keep reference to record on tab change
        AdminStaffService.persistUrlsOnRecordChange(recordId, type);
        // Don't continue if the record doesn't exist
        if (angular.isUndefined(service.safeData[type]) || angular.isUndefined(service.safeData[type][recordId])) {
          state[type] = '';
          // Reset URL
          return AdminStaffService.changeUrl(state);
        }
        // Retrieve appraiser
        var record = JSON.parse(JSON.stringify(service.safeData[type][recordId]));
        // Transform as necessary
        service.transformData(record);
        // Set to details
        service.displayData.details = record;
        // Callback
        if (callback) {
          var response = callback();
          // Async callback
          if (angular.isDefined(response) && angular.isDefined(response.then)) {
            return response.then(function () {
              resolve();
            });
          }
        }
        resolve();
      });
    },
    /**
     * Keep reference to which record is selected when switching tabs.
     *
     * If client 1 is being examined, and then switch to appraiser tab, when switching back to client, load 1
     */
    persistUrlsOnRecordChange: function (recordId, type) {
      angular.forEach(AdminStaffService.tabs, function (tab) {
        if (tab.tab === type) {
          tab.query = '?' + type + '=' + recordId;
        }
      });
    },
    /**
     * Update record
     */
    updateRecordOld: function (type, details) {
      var service = this;
      // Update safe data
      // @todo Need to update safe data without including transformed values
      //service.safeData[type][service.displayData.details.id] = details;
      // Make request
      return $http.patch(API_PREFIX() + '/v2.0/admin/staff/' + type + '/' + service.displayData.details.id, details);
    },
    /**
     * Update record
     */
    updateRecord: function (type, details) {
      var ResourceService = $injector.get('AdminStaffResourceService'),
        fn = details._type ? details._type : AdminStaffService._type;
      // Convert dash to uppercase letter
      if (fn.indexOf('-') !== -1) {
        fn = fn.split('-').map(function (word, key) {
          // Don't capitalize first word
          if (!key) {
            return word;
          }
          // Capitalize all others
          return word.charAt(0).toUpperCase() + word.slice(1);
        }).join('');
      }
      fn = fn + 'UpdateDetails';
      // Write to backend
      return ResourceService[type][fn](details);
    },
    /**
     * Check that we have a valid number being passed in
     */
    checkValidNumber: function (data) {
      var valid = true;
      angular.forEach(data, function (input, key) {
        if (isNaN(Number(input))) {
          valid = false;
          // Replace with regex
          data[key] = input.replace(/[^\d.]*/g, '');
        }
      });
      return valid;
    },
    /**
     * Determine service for initializing a component
     */
    getComponentService: function () {
      switch (AdminStaffService.section) {
        case 'admin-rule-profile':
          return $injector.get('AdminStaffAdminRuleProfileService');
        case 'mercury-failure-orders':
          return $injector.get('AdminStaffMercuryFailOrdersService');
        case 'realec-failure-orders':
          return $injector.get('AdminStaffRealEcFailureOrdersService');
        case 'reviewer-ruler-profile':
          return $injector.get('AdminStaffReviewerRulerProfileService');
        case 'staff':
          return $injector.get('AdminStaffStaffService');
        default:
          throw Error('Section must be specified for certification');
      }
    },
    /**
     * Keep any query parameters previously set
     */
    setTabs: function (tabs) {
      // Keep any query params
      angular.forEach(AdminStaffService.tabs, function (tab, key) {
        if (tab.query) {
          tabs[key].query = tab.query;
        }
      });
      AdminStaffService.tabs = tabs;
    },
    /**
     * Construct the table with the necessary columns hidden and shown
     * @param hide Comma separated list of columns to hide
     * @param show Comma separated list of columns to show
     * @returns {Array}
     */
    getDirectiveTableColumns: function (hide, show) {
      var service = this, hideItems, showItems, heading = [];
      // If items are hidden, only include those that are not
      if (hide) {
        hideItems = hide.split(',');
        angular.forEach(service.safeData.columns, function (item) {
          if (hideItems.indexOf(item.id) === -1) {
            heading.push(item);
          }
        });
        // Otherwise include all items
      } else {
        heading = service.safeData.columns;
      }
      // If additional columns are to be conditionally shown, find them here
      if (show) {
        showItems = show.split(',');
        angular.forEach(service.safeData.additionalColumns, function (item) {
          if (showItems.indexOf(item.id) !== -1) {
            heading.push(item);
          }
        });
      }
      return heading;
    },
    /**
     * Register record (create new admin staff)
     */
    registerRecord: function (type, safeData) {
      var service = this;
      // Default to type
      safeData = safeData || type;
      // Make request
      return $resource(API_PREFIX() + '/v2.0/admin/staff/:id/register',
      {id: AdminStaffService.id, type: type})
      .save().$promise
      .then(function () {
        // Set to registered
        service.safeData[safeData][AdminStaffService.id].registeredStatus = AdminStaffService.registeredStatusValues.registered;
      });
    },
    /**
     * Retrieve data on load
     */
    loadRecords: function (type, hashValue, params, callback) {
      // Account for only two parameters
      if (angular.isObject(hashValue)) {
        params = hashValue;
        hashValue = null;
      }
      // Function as second param
      if (angular.isFunction(hashValue)) {
        callback = hashValue;
        hashValue = null;
      }
      var service = this,
      formatCallback = service.formatCallback || angular.noop,
      request;
      // Default to null
      params = params || null;
      // Default to type
      hashValue = hashValue || type;

      // Get resource service
      var ResourceService = $injector.get('AdminStaffResourceService');
      // If we have a function on resource service, use that
      if (angular.isDefined(ResourceService[type]) && angular.isFunction(ResourceService[type].init)) {
        request = ResourceService[type].init(params);
      } else {
        request = $resource(API_PREFIX() + '/v2.0/admin/staff/:type/', {type: type}).query(params).$promise;
      }
      // Load records
      request
        .then(function (response) {
          // Real API requests will contain a data parameter, if I need the rest of the object for any reason this can be changed later
          if (angular.isArray(response.data)) {
            response = response.data;
          }
          // Hash
          service.hashData(response, hashValue);
          /**
           * @todo I need to make sure that the correct type is bound to all instances of format data
           * or else correct this
           */
            // Format
          service.formatData(formatCallback);
          //throw Error();
        }).then(function () {
          // If callback
          if (angular.isDefined(callback)) {
            callback();
          }
        });
    },
    /**
     * Registered status values
     */
    registeredStatusValues: {
      // Registered statuses
      uninvited: 0,
      invited: 1,
      registered: 2
    },
    /**
     * Retrieve values for registered status for a user/company
     */
    getRegisteredStatus: function () {
      var statusValues = {};
      statusValues[AdminStaffService.registeredStatusValues.uninvited] = 0;
      statusValues[AdminStaffService.registeredStatusValues.invited] = 1;
      statusValues[AdminStaffService.registeredStatusValues.registered] = 2;
      return statusValues;
    },
    /**
     * Create new staff
     */
    newStaff: function (newRecord) {

      var Service = this;//, newRecord = AdminStaffStaffService.displayData.details;
      //by default we're dealing with staff
      var type = 'staff';
      return Service.request('newStaff',newRecord)
        // Add company to table on success
        .then(function (response) {
          // Set ID for new company
          newRecord.id = response.data.id;
          newRecord.type = 'staff';
          // Add to table
          Service.safeData[type][newRecord.id] = newRecord;
          // Recreate table
          Service.formatData();
        });
    },
    /**
     * update Staff
     */
    updateStaff: function (type) {
      var Service = this, newRecord = Service.displayData.details;
      return Service.request('updateStaff', {
        id: newRecord.id
      }, newRecord)
        // Add Staff to table on success
        .then(function (response) {
          // Format raw record
          if (angular.isFunction(Service.formatRawRecords)) {
            Service.formatRawRecords(response);
          }
          // Add to table
          Service.safeData[type][response.id] = response;
          // Recreate table
          Service.formatData();
        });
    },
    /**
     * Delete selected staff
     */
    deleteStaff: function () {
      // Make request
      var Service = this;
      /**
       * @TODO need to mange selected staff ID
       */
      return Service.request('deleteStaff', {
        staffId: Service.displayData.selectedStaff
      })
        .then(function () {
          // Remove the deleted company or user
          delete Service.safeData.staff[Service.displayData.selectedStaff];
          // Recreate display data
          Service.formatData();
          // Reset URL
          //AdminStaffService.changeUrl({client: ''});
          // Hold reference to deleted type
          Service.displayData.deletedType = Service.displayData.type;
          // Clear details
          // Service.displayData.details = null;
          // Clear record
          //AdminStaffService.record = null;
        });
    },
    /**
     * Create new profile
     */
    newProfile: function (type) {
      var Service = this, newRecord = Service.selectedProfile;
      return Service.request('newProfile', {
        name: newRecord.name
      })
        // Add Profile to table on success
        .then(function (response) {
          // Format raw record
          if (angular.isFunction(Service.formatRawRecords)) {
            Service.formatRawRecords(response);
          }
          // Add to table
          Service.safeData[type][response.id] = response.data;
          // Recreate table
          Service.formatData();
        });
    },
    /**
     * update profile (admin rule/ reviewer rule)
     */
    updateProfile: function (type) {
      var Service = this, newRecord = Service.selectedProfile;
      Service.displayData.newProfile = Service.selectedProfile;
      return Service.request('updateProfile', {
        id: newRecord.id
      }, {
        name: newRecord.name
      })
        // Add Profile to table on success
        .then(function (response) {
          // Format raw record
          if (angular.isFunction(Service.formatRawRecords)) {
            Service.formatRawRecords(response);
          }
          // Add to table
          Service.safeData[type][response.id] = response.data;
          // Recreate table
          Service.formatData();
        });
    },
    /**
     * Delete selected profile
     */
    deleteProfile: function () {
      // Make request
      var Service = this;
      return Service.request('deleteProfile')
        .then(function () {
          // Remove the deleted company or user
          delete Service.safeData.profiles[AdminStaffService.id];
          // Recreate display data
          Service.formatData();
          // Reset URL
          AdminStaffService.changeUrl({client: ''});
          // Hold reference to deleted type
          Service.displayData.deletedType = Service.displayData.type;
          // Clear details
          Service.displayData.details = null;
          // Clear record
          AdminStaffService.record = null;
        });
    },
    /**
     * Retrieve companies
     */
    getCompanies: function () {
      var Service = this;
      return Service.request('companies')
        .then(function (response) {
          // Hash companies
          Service.hashData(response.data, 'companies');
          Service.formatCompaniesData();
        });
    },
    /**
     * Retrieve process status
     */
    getProcessStatus: function () {
      var Service = this;
      return Service.request('processStatus')
        .then(function (response) {
          // Hash settings
          Service.hashData(response.data, 'processStatus');
          Service.formatProcessStatusData();
        });
    },
    /**
     * Retrieve states
     */
    getStates: function () {
      var Service = this;
      return Service.request('states')
        .then(function (response) {
          // Hash settings
          Service.hashData(response.data, 'states');
          Service.formatStatesData();
        });
    },
    /**
     * Retrieve job Type
     */
    getJobType: function () {
      var Service = this;
      return Service.request('jobType')
        .then(function (response) {
          // Hash settings
          Service.hashData(response.data, 'jobType');
          Service.formatJobTypeData();
        });
    },
    /**
     * Retrieve paid Status
     */
    getPaidStatus: function () {
      var Service = this;
      return Service.request('paidStatus')
        .then(function (response) {
          // Hash settings
          Service.hashData(response.data, 'paidStatus');
          Service.formatPaidStatusData();
        });
    },
    /**
     * Add Clients (Companies)
     */
    addCompanies: function(){
      var service = this, newRecord = service.displayData.newCompany;
      var type = 'company';
      return service.request('newCompany', newRecord)
        // Add Profile to table on success
        .then(function (response) {
          // Format raw record
          if (angular.isFunction(service.formatRawRecords)) {
            service.formatRawRecords(response);
          }
          // Add to table
          service.safeData[type][response.id] = response;
          // Recreate table
          service.formatData();
        });
    },
    /**
     * Add Paid Status
     */
    addPaidStatus: function() {
      var service = this, newRecord = service.displayData.newPaidStatus;
      var type = 'paidStatus';
      return service.request('newPaidStatus', newRecord)
        // Add Profile to table on success
        .then(function (response) {
          // Format raw record
          if (angular.isFunction(service.formatRawRecords)) {
            service.formatRawRecords(response);
          }
          // Add to table
          service.safeData[type][response.id] = response;
          // Recreate table
          service.formatData();
        });
    },
    /**
     * Add Process Status
     */
    addStatus: function() {
      var service = this, newRecord = service.displayData.newProcessStatus;
      var type = 'processStatus';
      return service.request('newProcessStatus', newRecord)
        // Add Profile to table on success
        .then(function (response) {
          // Format raw record
          if (angular.isFunction(service.formatRawRecords)) {
            service.formatRawRecords(response);
          }
          // Add to table
          service.safeData[type][response.id] = response;
          // Recreate table
          service.formatData();
        });
    },
    /**
     * Add States
     */
    addStates: function() {
      var service = this, newRecord = service.displayData.newStates;
      var type = 'states';
      return service.request('newStates', newRecord)
        // Add Profile to table on success
        .then(function (response) {
          // Format raw record
          if (angular.isFunction(service.formatRawRecords)) {
            service.formatRawRecords(response);
          }
          // Add to table
          service.safeData[type][response.id] = response;
          // Recreate table
          service.formatData();
        });
    },
    /**
     * Add Job Type
     */
    addJobTypes: function(){
      var service = this, newRecord = service.displayData.newJobTypes;
      var type = 'jobType';
      return service.request('newJobTypes', newRecord)
        // Add Profile to table on success
        .then(function (response) {
          // Format raw record
          if (angular.isFunction(service.formatRawRecords)) {
            service.formatRawRecords(response);
          }
          // Add to table
          service.safeData[type][response.id] = response;
          // Recreate table
          service.formatData();
        });
    },
    /**
     * Delete company
     */
    deleteCompany: function () {
      var Service = this;
      return Service.request('deleteCompany', {
        companyId: Service.displayData.selectedCompanyId,
        profileId: AdminStaffService.profileId
      })
        .then(function () {
          // Delete from safe data
          delete Service.safeData.companies[Service.displayData.selectedCompanyId];
          // Update display
          Service.formatCompaniesData();
        });
    },
    /**
     * Delete job type
     */
    deleteJobType: function () {
      var Service = this;
      return Service.request('deleteJobType', {
        jobTypeId: Service.displayData.selectedJobTypeId,
        profileId: AdminStaffService.profileId
      })
        .then(function () {
          // Delete from safe data
          delete Service.safeData.jobType[Service.displayData.selectedJobTypeId];
          // Update display
          Service.formatJobTypeData();
        });
    },
    /**
     * Delete Process Status
     */
    deleteProcessStatus: function () {
      var Service = this;
      return Service.request('deleteProcessStatus', {
        statusId: Service.displayData.selectedProcessStatusId,
        profileId: AdminStaffService.profileId
      })
        .then(function () {
          // Delete from safe data
          delete Service.safeData.processStatus[Service.displayData.selectedProcessStatusId];
          // Update display
          Service.formatProcessStatusData();
        });
    },
    /**
     * Delete Paid Status
     */
    deletePaidStatus: function () {
      var Service = this;
      return Service.request('deletePaidStatus', {
        statusId: Service.displayData.selectedPaidStatusId,
        profileId: AdminStaffService.profileId
      })
        .then(function () {
          // Delete from safe data
          delete Service.safeData.paidStatus[Service.displayData.selectedPaidStatusId];
          // Update display
          Service.formatPaidStatusData();
        });
    },
    /**
     * Delete state
     */
    deleteState: function () {
      var Service = this;
      return Service.request('deleteState', {
        stateId: Service.displayData.selectedStateId,
        profileId: AdminStaffService.profileId
      })
        .then(function () {
          // Delete from safe data
          delete Service.safeData.states[Service.displayData.selectedStateId];
          // Update display
          Service.formatStatesData();
        });
    },
    /**
     * Check the checkboxes for the records which are already active
     */
    checkGroups: function (data) {
      var result = {};
      angular.forEach(data, function (val) {
        if (val.active) {
          result[val.id] = true;
        }
      });
      return result;
    },
    /**
     * General toggle settings
     */
    toggleSetting: function (type) {
      if (angular.isUndefined(type)) {
        throw Error('Setting is undefined');
      }
      // Update auto add status
      var Service = this;
      Service.displayData[type] = !Service.displayData[type];
      // Write to backend
      var permissions = new (Service.permissionsRequest())({val: Service.displayData[type]});
      permissions.permissionType = type;
      return permissions.$updatePermissions();
    }
  };

  return AdminStaffService;
}]);
