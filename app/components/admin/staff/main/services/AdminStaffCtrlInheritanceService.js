'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users controller inheritance service
 */
app.factory('AdminStaffCtrlInheritanceService',
['ngProgress', '$timeout', 'REQUEST_DELAY', 'DomLoadService', 'AdminStaffService',
function ( ngProgress, $timeout, REQUEST_DELAY, DomLoadService, AdminStaffService) {
  return {
    createCtrl: function (service, $scope, type) {
      var vm = this;
      // Sync with service
      vm.safeData = service.safeData;
      vm.displayData = service.displayData;
      vm.searchTrackId = service.searchTrackId;
      /**
       * Active button text
       */
      vm.activeButtonText = function () {
        return vm.displayData.details.active ? 'Active' : 'Inactive';
      };
      /**
       * Toggle active/inactive
       */
      vm.toggleActive = function () {
        // Detach watcher
        AdminStaffService.detachUpdateWatcher();
        // Update record
        vm.displayData.details.active = !vm.displayData.details.active;
        // Make request
        service.toggleSetting('isActive')
          .catch(function () {
            // Reset safe data
            service.safeData[type][AdminStaffService.id].active = !service.safeData[type][AdminStaffService.id].active;
            // Reset display data
            service.displayData.details.active = !service.displayData.details.active;
            // Show failure modal
            $scope.$broadcast('show-modal', 'update-' + type + '-failure');
          })
          .finally(function () {
            // Reattach watcher
            AdminStaffService.attachUpdateWatcher.call(vm, $scope, service);
          });
      };
      /**
       * Update details when typing
       */
     // AdminStaffService.attachUpdateWatcher.call(vm, $scope, service);
      /**
       * Display appraiser details
       * @param type View type
       */
      vm.showAdditionalDetails = function (type) {
        // Compile additional details
        $scope.$broadcast('compile-directive', type + '-additional-details');
        // Display modal
        DomLoadService.load().then(function () {
          // Ensure that a valid tab is displayed
          if (typeof vm.checkDetailsPaneTab === 'function') {
            vm.checkDetailsPaneTab();
          }
          // Change to staff so it can be displayed on load
          AdminStaffService.changeUrl({detailsPane: AdminStaffService.currentDetailsTab || 'staff'});
          // Display modal
          $scope.$broadcast('show-modal', type + '-additional-details');
          // Change URL on close
          vm.onAdditionalDetailsClose(type);
        });
      };
      /**
       * Whether the new staff is registered
       *
       * @todo
       */
      vm.recordRegistered = function () {
        var view;
        view = 'staff';
        // Return staff registration status
        if (angular.isDefined(service.safeData[view][AdminStaffService.id])) {
          return service.safeData[view][AdminStaffService.id].registeredStatus;
        }
      };
      /**
       * Register staff
       */
      vm.registerConfirm = function () {
        $scope.$broadcast('show-modal', 'register-record');
      };
      /**
       * Register new staff
       */
      vm.registerRecord = function () {
        service.registerRecord()
          .then(function () {
            $scope.$broadcast('hide-modal', 'register-record');
            $scope.$broadcast('show-modal', 'register-record-success');
          })
          .catch(function () {
            $scope.$broadcast('show-modal', 'register-record-failure');
          });
      };
      /**
       * Submit new staff modal
       */
      vm.newStaff = function () {
        // Submit new company
        service.newStaff().then(function () {
          // Update table ctrl service
        })
          .catch(function () {
            // Show failure
          })
          .finally(function () {
          });
      };
      /**
       * New Profile for Admin and Reviewer Rule
       */
      vm.newProfile = function () {
        // Submit new company
        service.newProfile().then(function () {
          // Show success
          $scope.$broadcast('hide-modal', 'new-profile');
          $scope.$broadcast('show-modal', 'new-profile-successful');
        }, function () {
          // Show failure
          $scope.$broadcast('hide-modal', 'new-profile');
          $scope.$broadcast('show-modal', 'new-profile-failure');
        }).finally(function () {
        });
      };
      /**
       * Edit Profile for Admin and Reviwer Rule
       */
      vm.editProfile = function () {
        // Submit new company
        service.updateProfile().then(function () {
          // Show success
          $scope.$broadcast('hide-modal', 'new-profile');
          $scope.$broadcast('show-modal', 'new-profile-successful');
        }, function () {
          // Show failure
          $scope.$broadcast('hide-modal', 'new-profile');
          $scope.$broadcast('show-modal', 'new-profile-failure');
        }).finally(function () {
        });
      };
      /**
       * Delete profile
       */
      vm.deleteProfile = function () {
        // Submit new company
        service.deleteProfile().then(function () {
          // Show success
          $scope.$broadcast('hide-modal', 'delete-profile-confirm');
          $scope.$broadcast('show-modal', 'delete-success');
        }, function () {
          // Show failure
          $scope.$broadcast('hide-modal', 'delete-profile-confirm');
          $scope.$broadcast('show-modal', 'delete-failure');
        }).finally(function () {
        });
      };
      /**
       * New Profile Modal
       */
      vm.newProfileModal = function () {
        vm.displayData.contentTitle = 'New Profile';
        vm.selectedAction = 'Create';
        vm.selectedProfile = {};
        // Wait for modal, then display it
        DomLoadService.load().then(function () {
          $scope.$broadcast('show-modal', 'new-profile');
        });
        // Kill modal on close
        var closed = $scope.$on('modal-closed-new-profile', function () {
          vm.showNewProfile = false;
          closed();
        });
      };
      /**
       * Update Profile Modal
       */
      vm.updateProfileModal = function () {
        vm.displayData.contentTitle = 'Edit Profile';
        vm.selectedAction = 'Update';
        // Wait for modal, then display it
        DomLoadService.load().then(function () {
          $scope.$broadcast('show-modal', 'new-profile');
        });
        // Kill modal on close
        var closed = $scope.$on('modal-closed-new-profile', function () {
          vm.showNewProfile = false;
          closed();
        });
      };
      /**
       * Delete Profile Modal
       */
      vm.deleteProfileModal = function () {
        // Wait for modal, then display it
        DomLoadService.load().then(function () {
          $scope.$broadcast('show-modal', 'delete-profile-confirm');
        });
      };
      /**
       * Staff CRUD modal
       */
      /**
       * New Profile Modal
       */
      vm.newProfileModal = function () {
        vm.displayData.contentTitle = 'New Profile';
        vm.selectedAction = 'Create';
        vm.selectedProfile = {};
        // Wait for modal, then display it
        DomLoadService.load().then(function () {
          $scope.$broadcast('show-modal', 'new-profile');
        });
        // Kill modal on close
        var closed = $scope.$on('modal-closed-new-profile', function () {
          vm.showNewProfile = false;
          closed();
        });
      };
      /**
       * New Staff Modal
       */
      vm.newStaffModal = function () {
        // Wait for modal, then display it
        if (!vm.displayData.details) {
          vm.displayData.contentTitle = 'Create New Staff';
          vm.details = null;
        } else {
          vm.displayData.contentTitle = 'Edit Staff';
        }
        DomLoadService.load().then(function () {
          $scope.$broadcast('show-modal', 'new-staff');
        });
        // Kill modal on close
        var closed = $scope.$on('modal-closed-new-staff', function () {
          vm.showNewStaff = false;
          closed();
        });
      };
      /**
       * New Staff Modal
       */
      vm.updateStaffModal = function () {
        // Wait for modal, then display it
        vm.displayData.contentTitle = 'Create New Staff';
        vm.details = {};
        DomLoadService.load().then(function () {
          $scope.$broadcast('show-modal', 'new-staff');
        });
        // Kill modal on close
        var closed = $scope.$on('modal-closed-new-staff', function () {
          vm.showNewStaff = false;
          closed();
        });
      };
      /**
       * Create new staff
       */
      vm.newStaff = function () {
        // Submit new company
        service.newStaff().then(function () {
          // Show success
          $scope.$broadcast('hide-modal', 'new-staff');
          $scope.$broadcast('show-modal', 'new-staff-success');
        }, function () {
          // Show failure
          $scope.$broadcast('hide-modal', 'new-staff');
          $scope.$broadcast('show-modal', 'new-staff-failure');
        }).finally(function () {
        });
      };
      /**
       * Create new staff
       */
      vm.updateStaff = function () {
        // Submit new company
        service.editStaff().then(function () {
          // Show success
          $scope.$broadcast('hide-modal', 'new-staff');
          $scope.$broadcast('show-modal', 'new-staff-success');
        }, function () {
          // Show failure
          $scope.$broadcast('hide-modal', 'new-staff');
          $scope.$broadcast('show-modal', 'new-staff-failure');
        }).finally(function () {
        });
      };

      /**
       * Delete selected staff
       */
      vm.deleteStaff = function () {
        service.deleteStaff()
          .then(function () {
            // Hide confirmation modal
            $scope.$broadcast('hide-modal', 'delete-staff-confirm');
            // Show success modal
            $scope.$broadcast('show-modal', 'delete-staff-success');
          }, function () {
            // Show failure modal
            $scope.$broadcast('hide-modal', 'delete-staff-confirm');
            $scope.$broadcast('show-modal', 'delete-staff-failure', true);
          }).finally(function () {
            // Complete ngProgress
          });
      };
      /**
       * Rule Profiles section
       * Add companies
       */
      vm.addCompaniesToRuleProfile = function () {
        // Submit new company info
        service.addCompanies(service.displayData.multiSelect)
          .then(function () {
            // Show success
            // Hash profiles
             /*service.hashData(response.data, 'companies');
             service.displayData.companies = response.data;
             service.formatData();*/
          }, function () {
            // Show failure
          }).finally(function () {
          });
      };
      /**
       * Rule Profiles section
       * Add states
       */
      vm.addStatesToRuleProfile = function () {
        // Submit new state info
        service.addStates(service.displayData.multiSelect)
          .then(function (data) {
            // Show success
            service.safeData[type] = data;
          }, function () {
            // Show failure
          }).finally(function () {
          });
      };
      /**
       * Rule Profiles section
       * Add states
      */
      vm.addJobTypesToRuleProfile = function () {
        // Submit job type info
        service.addJobTypes(service.displayData.multiSelect)
          .then(function (data) {
          // Show success.
          service.safeData[type] =  data;
        }, function () {
          // Show failure
        }).finally(function () {
        });
      };
      /**
       * Rule Profiles section
       * Add paid status
       */
      vm.addPaidStatusToRuleProfile = function () {
        // Submit paid status info
        service.addPaidStatus(service.displayData.multiSelect)
          .then(function (data) {
            // Show success
            service.safeData[type] = data;
          }, function () {
            // Show failure
          }).finally(function () {
          });
      };
      /**
       * Rule Profiles section
       * Add Process Status
       */
      vm.addProcessStatusToRuleProfile = function () {
        // Submit new process status info
        service.addProcessStatus(service.displayData.multiSelect)
          .then(function (data) {
            // Show success
            service.safeData[type] = data;
          }, function () {
            // Show failure
          }).finally(function () {
          });
      };
    }
  };
}]);
