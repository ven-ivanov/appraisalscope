'use strict';

var app = angular.module('frontendApp');

/**
 * Service for constructing $resource URLs, parameters, methods, etc
 */
app.factory('AdminStaffResourceService', ['$resource', 'API_PREFIX', 'HandbookService', 'HANDBOOK_CATEGORIES', 'AdminStaffService', function ($resource, API_PREFIX,HandbookService, HANDBOOK_CATEGORIES, AdminStaffService) {

  /**
   * Perform a query, expect array response
   * @TODO Fix this JSHint error and remove the ignore:line
   */
  var query = function (url, params) {  // jshint ignore:line
    return $resource(API_PREFIX() + url).query(params).$promise;
  };

  /**
   * Get request, non-array expected
   */
  var getRequest = function (url, params) {
    return $resource(API_PREFIX() + url ).get(params).$promise;
};

  /**
   * General post request
   */
  var post = function (url, params, body, headers) {
    if (!headers) {
      return new ($resource(API_PREFIX() + url, params))(body).$save();
    }
    // Specify put request with potential custom headers
    return new ($resource(API_PREFIX() + url, params, {
      postWithHeaders: {method: 'POST', headers: headers}
    }))(body).$postWithHeaders();
  };

  /**
   * General PUT request
   * @TODO Fix this JSHint error and remove the ignore:line
   */
  var put = function (url, params, body, headers) { // jshint ignore:line
    // Specify put request with potential custom headers
    return new ($resource(API_PREFIX() + url, params, {
      update: {method: 'PUT', headers: headers}
    }))(body).$update();
  };

  /**
   * Update request via POST
   */
  var patch = function (url, params, body, headers) {
    return new ($resource(API_PREFIX() + url, params, {
      update: {method: 'PATCH', headers: headers}
    }))(body).$update();
  };

  /**
   * Post with array body
   * @TODO Fix this JSHint error and remove the ignore:line
   */
  var postArray = function (url, params, body) { // jshint ignore:line
    return new ($resource(API_PREFIX() + url, params, {
      postArray: {
        method: 'POST',
        transformRequest: function (data) {
          var thisData = angular.copy(data), response = [];
          // Iterate and send as array
          angular.forEach(thisData, function (item) {
            response.push(item);
          });
          return JSON.stringify(response);
        }
        }
    }))(body).$postArray();
  };

  /**
   * Initiate a delete request
   */
  var deleteRequest = function (url, params, headers) {
    return new ($resource(API_PREFIX() + url, params, {
      doDelete: {method: 'DELETE', headers: headers}
    }))().$doDelete();
  };
  /**
   * AdminStaffResourceService
   */
  var Service = {
    // General resource object
    resource: {type: '', url: '', params: '', methods: ''},
    //Standard request URLS throughout section
    requests: {
      companies: '/v2.0/client/companies/',
      processStatus:'/v2.0/admin/process-status',
      jobType:'/v2.0/admin/admin-rule-profile/:profileId/job-type',
      paidStatus:'/v2.0/admin/paid-status'

    },
    /**
     * Staff sub-view
     */
    staff: {
      params: {
        // staff params
        staff: function () {
          return {
            staffId: AdminStaffService.id
          };
        }
      },
      // Standard request URLs
      requests: {
        staff: '/v2.0/admin/staff/:staffId/',
        loggedInLogs:'/v2.0/admin/staff/:staffId/logged-in-logs',
        branches: '/v2.0/admin/staff/:companyId/branches',
        brokers: '/v2.0/admin/staff/:companyId/brokers'
      },
      /**
       * Initial data load
       */
      init: function (params) {
        return getRequest('/v2.0/admin/staff', params);
      },
      /**
       * Delete a staff
       */
      deleteStaff: function (params) {
        return deleteRequest(Service.staff.requests.staff, params);
      },
      /**
       * Change active status for user
       */
      changeActiveStatus: function (body) {
        return patch(Service.staff.requests.staff, Service.staff.params.staff(), body);
      },
      /**
       * create new staff
       * @param params
       */
      newStaff: function (params) {
          return post(Service.staff.requests.staff, {}, params);
      },
      /**
       * update Staff
       * @param params
       */
      updateStaff: function (params, body) {
        return patch(Service.staff.requests.staff, params,body);
      },
      /**
       * get staff login log
       */
      getStaffLoggedinLog: function(){
        return getRequest(Service.staff.requests.loggedInLogs, Service.staff.params.staff());
      },
      /**
       * branches
       */
      branches: function( params) {
        return getRequest(Service.staff.requests.branches, params);
      },
      updateBranches: function(params, body) {
        return patch(Service.staff.requests.branches, params,body);
      },
      /**
       * brokers
       */
      brokers: function(params) {
        return getRequest(Service.staff.requests.brokers, params);
      },
      updateBrokers: function(params, body) {
        return patch(Service.staff.requests.brokers, params,body);
      }
    },
    /**
     * Staff permissions
     */
    permissions: {
      params: {
        // staff params
        staff: function () {
          return {
            staffId: AdminStaffService.id
          };
        }
      },
      // Standard request URLs
      requests: {
        staff: '/v2.0/admin/staff/permissions/',
        email: '/v2.0/admin/staff/email-permissions/',
        tabs: '/v2.0/admin/staff/tabs-permissions/'
      },
      /**
       * Initial data load
       */
      init: function (params) {
        return getRequest('/v2.0/admin/staff/', params);
      },
      /**
       * get staff permissions
       */
      staff: function (params) {
        return getRequest(Service.permissions.requests.staff, params);
      },
      /**
       * get email permissions
       */
      email: function (params) {
        return getRequest(Service.permissions.requests.email, params);
      },
      /**
       * get staff permissions
       */
      tabs: function (params) {
        return getRequest(Service.permissions.requests.tabs, params);
      }
    },
    /**
     * Mercury Failure Orders sub-view
     */
    mercuryFailureOrders: {
      params: {
        // staff params
        orders: function () {
          return {
            trackingId: AdminStaffService.trackingId
          };
        }
      },
      // Standard request URLs
      requests: {
        orders: '/v2.0/admin/mercury-failure-orders/search/:trackId'
      },
      /**
       * Initial data load
       */
      init: function (params) {
        return getRequest('/v2.0/admin/mercury-failure-orders', params);
      },
      /**
       * process Order
       */
      processOrder: function (body) {
        return patch(Service.mercuryFailureOrders.requests.orders, Service.mercuryFailureOrders.params.orders(), body);
      },
      /**
       * search order
       */
      searchOrder: function(trackId) {
        return getRequest(Service.mercuryFailureOrders.requests.orders,{trackId: trackId} );
      }
    },
    /**
     * RealEc Failure Orders sub-view
     */
    realecFailureOrders: {
      params: {
        // staff params
        orders: function () {
          return {
            transactionId: AdminStaffService.transactionId,
            uniqueId:AdminStaffService.uniqueId
          };
        }
      },
      // Standard request URLs
      requests: {
        orders: '/v2.0/admin/realec-failure-orders/search/:transactionId/:uniqueId'
      },
      /**
       * Initial data load
       */
      init: function (params) {
        return getRequest('/v2.0/admin/realec-failure-orders', params);
      },
      /**
       * process Order
       */
      processOrder: function (body) {
        return patch(Service.realecFailureOrders.requests.orders, Service.realecFailureOrders.params.orders(), body);
      },
      /**
       * search order
       */
      searchOrder: function(body) {
        return getRequest(Service.realecFailureOrders.requests.orders, Service.realecFailureOrders.params.orders(), body);
      }
    },
    /**
     *Admin Rule Profiles sub-view
     */
    adminRuleProfile: {
      params: {
          profile: function (){

            //return {profileId: AdminStaffService.profileId};
            return {profileId: 3};
          },
          company: function() {
            return {companyId: AdminStaffService.companyId};
          }
      },
      // Standard request URLs
      requests: {
        profiles: '/v2.0/admin/admin-rule-profile/',
        companies: '/v2.0/admin/admin-rule-profile/:profileId/companies',
        states: '/v2.0/admin/admin-rule-profile/:profileId/states',
        processStatus: '/v2.0/admin/admin-rule-profile/:profileId/process-status',
        paidStatus: '/v2.0/admin/admin-rule-profile/:profileId/paid-status',
        jobType: '/v2.0/admin/admin-rule-profile/:profileId/job-type',
        branches: '/v2.0/admin/admin-rule-profile/:profileId/branches',
        brokers: '/v2.0/admin/admin-rule-profile/:profileId/brokers'
      },
      /**
       * Initial data load
       */
      init: function (params) {
        return getRequest('/v2.0/admin/admin-rule-profile', params);
      },
      /**
       * get profiles
       */
      profiles: function(params){
        return getRequest(Service.adminRuleProfile.requests.profiles, params);
      },
      /**
       * companies
       */
      companies: function(){
        return getRequest(Service.adminRuleProfile.requests.companies, Service.adminRuleProfile.params.profile());
      },
      /**
       * delete company
       */
      deleteCompany: function(params) {
        return deleteRequest(Service.adminRuleProfile.requests.companies+'/:companyId',params);
      },
      /**
       * states
       */
      states: function(){
        return getRequest(Service.adminRuleProfile.requests.states, Service.adminRuleProfile.params.profile());
      },
      /**
       * delete state
       */
      deleteState: function(params) {
        return deleteRequest(Service.adminRuleProfile.requests.states+'/:stateId',params);
      },
      /**
       * branches
       */
      branches: function() {
          return getRequest(Service.adminRuleProfile.requests.branches, Service.adminRuleProfile.params.profile());
      },
      /**
       * Update branches for a company for a profile
       */
      updateBranches:  function(params, body) {
        return put(Service.adminRuleProfile.requests.branches, params, body);
      },
      /**
       * brokers
       */
      brokers: function(params) {
         return getRequest(Service.adminRuleProfile.requests.branches, params);
      },
      /**
       * Update brokers for a company for a profile
       */
      updateBrokers:  function(params, body) {
        return put(Service.adminRuleProfile.requests.brokers, params, body);
      },
      /**
       * process status
       */
      processStatus: function(){
        return getRequest(Service.adminRuleProfile.requests.processStatus, Service.adminRuleProfile.params.profile());
      },
      /**
       * delete process status
       */
      deleteProcessStatus: function(params) {
        return deleteRequest(Service.adminRuleProfile.requests.processStatus+'/:statusId',params);
      },
      /**
       * paid status
       */
      paidStatus: function(){
        return getRequest(Service.adminRuleProfile.requests.paidStatus, Service.adminRuleProfile.params.profile());
      },
      /**
       * delete paid status
       */
      deletePaidStatus: function(params) {
        return deleteRequest(Service.adminRuleProfile.requests.paidStatus+'/:statusId',params);
      },
      /**
       * job type
       */
      jobType: function(){
        return getRequest(Service.adminRuleProfile.requests.jobType, Service.adminRuleProfile.params.profile());
      },
      /**
       * delete job type
       */
      deleteJobType: function(params) {
        return deleteRequest(Service.adminRuleProfile.requests.jobType+'/:jobTypeId',params);
      },
      /**
       * New profile
       */
      newProfile: function(params) {
        return put(Service.adminRuleProfile.requests.profiles,  params);
      },
      /**
      * update profile
      */
      updateProfile: function(params, body) {
        return put(Service.adminRuleProfile.requests.profiles, params, body);
      },
      /**
       * Delete profile
       */
      deleteProfile: function(params) {
        return deleteRequest(Service.adminRuleProfile.requests.profiles, params);
      },
      /**
       * Add companies
       */
      addCompanies: function(params, body) {
        return put(Service.adminRuleProfile.requests.companies, Service.adminRuleProfile.params.profile(), body);
      },
      /**
       * Add Job Types
       */
      addJobTypes: function(params, body) {
        return put(Service.adminRuleProfile.requests.jobType, Service.adminRuleProfile.params.profile(), body);
      },
      /**
       * Add States
       */
      addStates: function(params, body) {
        return put(Service.adminRuleProfile.requests.states, Service.adminRuleProfile.params.profile(), body);
      },
      /**
       * Add Paid Status
       */
      addPaidStatus: function(params, body) {
        return put(Service.adminRuleProfile.requests.paidStatus, Service.adminRuleProfile.params.profile(), body);
      },
      /**
       * Add Process status
       */
      addProcessStatus: function(params, body) {
        return put(Service.adminRuleProfile.requests.processStatus, Service.adminRuleProfile.params.profile(), body);
      }
    },
    /**
     *Admin Rule Profiles sub-view
     */
      reviewerRuleProfile: {
      params: {
        profile: function (){
          return {profileId : AdminStaffService.profileId};
        },
        company: function() {
          return {companyId : AdminStaffService.companyId};
        }
      },
      // Standard request URLs
      requests: {
        profiles: '/v2.0/admin/reviewer-rule-profile/',
        companies: '/v2.0/admin/reviewer-rule-profile/:profileId/companies',
        states: '/v2.0/admin/reviewer-rule-profile/:profileId/states',
        processStatus: '/v2.0/admin/reviewer-rule-profile/:profileId/process-status',
        paidStatus: '/v2.0/admin/reviewer-rule-profile/:profileId/paid-status',
        jobType: '/v2.0/admin/reviewer-rule-profile/:profileId/job-type',
        branches: '/v2.0/admin/reviewer-rule/:profileId/branches',
        brokers: '/v2.0/admin/reviewer-rule/:profileId/brokers'
      },
      /**
       * Initial data load
       */
      init: function (params) {
        return getRequest('/v2.0/admin/reviewer-rule-profile', params);
      },
      /**
       * get profiles
       */
      profiles: function(params){
        return getRequest(Service.reviewerRuleProfile.requests.profiles, params);
      },
      /**
       * companies
       */
      companies: function(){
        return getRequest(Service.reviewerRuleProfile.requests.companies, Service.reviewerRuleProfile.params.profile());
      },
      /**
       * delete company
       */
      deleteCompany: function(params) {
        return deleteRequest(Service.reviewerRuleProfile.requests.companies+'/:companyId',params);
      },
      /**
       * branches
       */
      branches: function() {
        return getRequest(Service.reviewerRuleProfile.requests.branches, Service.reviewerRuleProfile.params.profile());
      },
      /**
       * Update branches for a company for a profile
       */
      updateBranches:  function(params, body) {
        return put(Service.reviewerRuleProfile.requests.branches, Service.reviewerRuleProfile.params.profile(), body);
      },
      /**
       * brokers
       */
      brokers: function() {
        return getRequest(Service.reviewerRuleProfile.requests.brokers, Service.reviewerRuleProfile.params.profile());
      },
      /**
       * Update brokers for a company for a profile
       */
      updateBrokers:  function(params, body) {
        return put(Service.reviewerRuleProfile.requests.brokers, params, body);
      },
      /**
       * states
       */
      states: function(){
        return getRequest(Service.reviewerRuleProfile.requests.states, Service.reviewerRuleProfile.params.profile());
      },
      /**
       * delete state
       */
      deleteState: function(params) {
        return deleteRequest(Service.reviewerRuleProfile.requests.states+'/:stateId',params);
      },
      /**
       * process status
       */
      processStatus: function(){
        return getRequest(Service.reviewerRuleProfile.requests.processStatus, Service.reviewerRuleProfile.params.profile());
      },
      /**
       * delete process status
       */
      deleteProcessStatus: function(params) {
        return deleteRequest(Service.reviewerRuleProfile.requests.processStatus+'/:statusId',params);
      },
      /**
       * paid status
       */
      paidStatus: function(){
        return getRequest(Service.reviewerRuleProfile.requests.paidStatus, Service.reviewerRuleProfile.params.profile());
      },
      /**
       * delete paid status
       */
      deletePaidStatus: function(params) {
        return deleteRequest(Service.reviewerRuleProfile.requests.paidStatus+'/:statusId',params);
      },
      /**
       * job type
       */
      jobType: function(){
        return getRequest(Service.reviewerRuleProfile.requests.jobType, Service.reviewerRuleProfile.params.profile());
      },
      /**
       * delete job type
       */
      deleteJobType: function(params) {
        return deleteRequest(Service.reviewerRuleProfile.requests.jobType+'/:jobTypeId',params);
      },
      /**
       * New profile
       */
      newProfile: function(params) {
        return put(Service.adminRuleProfile.requests.profiles,  params);
      },
      /**
       * update profile
       */
      updateProfile: function(params, body) {
        return put(Service.adminRuleProfile.requests.profiles, params, body);
      },
      /**
       * Delete profile
       */
      deleteProfile: function(params) {
        return deleteRequest(Service.reviewerRuleProfile.requests.profiles, params);
      },
      /**
       * Add companies
       */
      addCompanies: function(params, body) {
        return put(Service.reviewerRuleProfile.requests.companies, params, body);
      },
      /**
       * Add Job Types
       */
      addJobTypes: function(params, body) {
        return put(Service.reviewerRuleProfile.requests.jobType, params, body);
      },
      /**
       * Add States
       */
      addStates: function(params, body) {
        return put(Service.reviewerRuleProfile.requests.states, params, body);
      },
      /**
       * Add Paid Status
       */
      addPaidStatus: function(params, body) {
        return put(Service.reviewerRuleProfile.requests.paidStatus, params, body);
      },
      /**
       * Add Process status
       */
      addProcessStatus: function(params, body) {
        return put(Service.reviewerRuleProfile.requests.processStatus, params, body);
      }
    },

    /**
     * Companies
     */
      companies :{
        /**
         * get companies(clients)
         */
        init:function(params) {
          return getRequest(Service.requests.companies, params);
        }
      },
    /**
     * Process status for profiles
     */
    processStatus: {
      /**
       * get process status
       */
      init: function(params){
        return getRequest(Service.requests.processStatus, params);
      }
    },
    /**
     * States for Profiles
     */
      states: {
        params:{
        },
          /**
           * get states
           */
          init: function() {
              //return getRequest(Service.requests.states, params);
              return HandbookService.query({category: HANDBOOK_CATEGORIES.states}).$promise;
          }
      },
    /**
     * Job Type
     */
      jobTypes: {
      params:{
        profileId : 1
      },
      /**
       * get job Type
       */
      init: function(){
        return getRequest(Service.requests.jobType, Service.jobTypes.params);
      }
    },
    /**
     * Paid Status
     */
    paidStatus: {
      /**
       * get paid status
       */
      init: function(params){
        return getRequest(Service.requests.paidStatus, params);
      }
    }
  };
  return Service;
}]);
