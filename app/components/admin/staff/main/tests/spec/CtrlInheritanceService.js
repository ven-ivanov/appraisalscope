xdescribe('CtrlInheritanceService', function () {
  var controller, scope, ctrlInheritanceService, AdminStaffService;

  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AdminStaffService', AdminStaffServiceMock);
    });
  });

  /**
   * I need to test this service as a controller, since it's explicitly meant to provide controller methods
   */
  beforeEach(inject(function ($controller, $rootScope, CtrlInheritanceService, AdminStaffService) {
    scope = $rootScope.$new();
    controller = $controller('AdminStaffCtrl', {$scope: scope});
    ctrlInheritanceService = CtrlInheritanceService;
    adminStaffService = AdminStaffService;

    // Inherit
    ctrlInheritanceService.createCtrl.call(controller, adminStaffService, scope, 'appraiser');
  }));

  describe('staff', function () {
    xit('should retrieve staff admin on load');
  });

  //@Todo need to add more test as new features are added

});
