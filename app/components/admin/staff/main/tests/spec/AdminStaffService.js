xdescribe('AdminStaffService', function () {
  var scope, adminStaffService, httpBackend, that, data, asTableService, domLoadService;
  beforeEach(function () {
    module('frontendApp', function ($provide) {
      $provide.factory('AsTableService', AsTableServiceMock);
      $provide.factory('DomLoadService', DomLoadServiceMock);
    });
  });

  beforeEach(inject(function ($rootScope, AdminStaffService, $httpBackend, AsTableService, DomLoadService) {
    scope = $rootScope.$new();
    adminStaffService = AdminStaffService;
    httpBackend = $httpBackend;
    asTableService = AsTableService;
    domLoadService = DomLoadService;

    that = this;
    // Tabs
    that.tabs = [{tab: 'fakeTab'}, {tab: 'otherFakeTab'}];

    // Caller service
    this.safeData = {
      test: {},
      heading: []
    };
    this.displayData = {
      test: []
    };
    this.transformers = {
      test: {
        'Test 1': 'blah'
      },
      id: {
        filter: 'currency'
      }
    };

    // Data to be hashed
    data = [{id: 1, test: 'Test 1'}, {id: 2, test: 'Test 2'}];

    // Spies
    spyOn(AsTableService, 'applyFilters').and.callThrough();
    spyOn(JSON, 'stringify').and.callThrough();
    spyOn(JSON, 'parse').and.callThrough();

    // http
    httpBackend.whenGET(/staff/).respond({userName:'test',email:'test@gmail.com'});
  }));

  describe('getTabValues', function () {
    beforeEach(function () {
      //tab management on staff section, - we have only 4 fixed tabs
      //AdminStaffService.getTabValues(that.tabs);
      scope.$digest();
    });

    it('should set tab values', function () {
      expect(that.tabs).toEqual([{
                                   tab: 'fakeTab'
                                 }, {
                                   tab: 'otherFakeTab'
                                 }]);
    });
  });

  describe('getTabClass', function () {
    var response;
    it('should set the active tab to active', function () {
      response = adminStaffService.getTabClass({tab: 'fakeTab'}, 'fakeTab');
      expect(response).toEqual('col-md-3 active');
    });
    it('should set non-active tabs to normal class', function () {
      response = adminStaffService.getTabClass({tab: 'fakeTab'}, 'NOT THIS ONE');
      expect(response).toEqual('col-md-3');
    });
  });

  describe('setIdVal', function () {
    var response;
    it('should set ID to specified ID', function () {
      response = adminStaffService.setIdVal('Blah');
      expect(response).toEqual('Blah');
    });
    it('should set ID to id if none specified', function () {
      response = adminStaffService.setIdVal();
      expect(response).toEqual('id');
    });
  });

  describe('hashData', function () {
    beforeEach(function () {
      that.idVal = 'testIdVal';
    });

    it('should throw an error if only data is passed in', function () {
      try {
        adminStaffService.hashData.call(this, data);
      } catch (e) {
        expect(e.message).toEqual('Data and type must be passed in to hashData');
      }
    });
    it('should set safe data if only data and type are passed in', function () {
      adminStaffService.hashData.call(this, data, 'test');
      expect(this.safeData.test).toEqual({
        1: {
          id: 1,
          test: 'Test 1'
        },
        2: {
          id: 2,
          test: 'Test 2'
        }
      });
    });
    it('should break references', function () {
      adminStaffService.hashData.call(this, data, 'test');
      expect(JSON.stringify).toHaveBeenCalled();
      expect(JSON.parse).toHaveBeenCalled();
    });
    describe('callback', function () {
      beforeEach(function () {
        this.callback = function () {};
        spyOn(this, 'callback');
      });
      it('should call the callback when it is the third parameter', function () {
        adminStaffService.hashData.call(this, data, 'test', this.callback);
        expect(this.callback).toHaveBeenCalled();
      });
      it('should call the callback when it is the fourth parameter', function () {
        adminStaffService.hashData.call(this, data, 'test', 'idVal', this.callback);
        expect(this.callback).toHaveBeenCalled();
      });
    });
  });

  describe('with data', function () {
    beforeEach(function () {
      adminStaffService.hashData.call(this, data, 'test');
    });

    describe('formatData', function () {
      beforeEach(function () {
        this.transformData = jasmine.createSpy('transformData');
      });

      it('should throw an error if type is not specified', function () {
        try {
          adminStaffService.formatData.call(this);
        } catch (e) {
          expect(e.message).toEqual('formatData requires a type to be specified');
        }
      });
      describe('all parameters', function () {
        beforeEach(function () {
          adminStaffService.formatData.call(this, 'test');
        });

        it('should break references', function () {
          expect(JSON.stringify).toHaveBeenCalled();
          expect(JSON.parse).toHaveBeenCalled();
        });
        it('should create display data', function () {
          expect(this.displayData.test).toEqual([{
             id: 1,
             test: 'Test 1'
           }, {
             id: 2,
             test: 'Test 2'
           }]);
        });
        it('should call transform data', function () {
          expect(this.transformData).toHaveBeenCalled();
        });
        it('should call apply filters', function () {
          expect(asTableService.applyFilters).toHaveBeenCalled();
        });
      });

      describe('with callback', function () {
        beforeEach(function () {
          this.callback = jasmine.createSpy('callback');
          adminStaffService.formatData.call(this, 'test', this.callback);
          scope.$digest();
        });

        it('should call the callback when one is defined', function () {
          expect(this.callback).toHaveBeenCalled();
        });
      });
    });

    describe('formatPlainObjectData', function () {
      it('should throw an error if not given type', function () {
        try {
          adminStaffService.formatPlainObjectData.call(this);
        } catch (e) {
          expect(e.message).toEqual('Type must be passed to formatPlainObject');
        }
      });

      beforeEach(function () {
        this.transformData = function(){};
        spyOn(this, 'transformData');
        this.safeData.test2 = {
          test1: 'Test1',
          currency: 100
        };
        this.safeData.heading = [];
        adminStaffService.formatPlainObjectData.call(this, 'test2');
      });

      it('should call transformData', function () {
        expect(this.transformData).toHaveBeenCalled();
      });
      it('should render display data', function () {
        expect(this.displayData.test2).toEqual({ test1: 'Test1', currency: 100 });
      });
      it('should call applyFilters', function () {
        expect(asTableService.applyFilters).toHaveBeenCalled();
      });
    });

    describe('transformData', function () {
      it('should apply both normal transformations and $filter transformations', function () {
        adminStaffService.transformData.call(this, this.safeData.test[1]);
        expect(this.safeData.test).toEqual({
          1: {
            id: '$1.00',
            test: 'blah'
          },
          2: {
            id: 2,
            test: 'Test 2'
          }
        });
      });

      it('should be able to undo transformations', function () {
        adminStaffService.transformData.call(this, this.safeData.test[1]);
        //expect(this.safeData.test).toEqual();
        adminStaffService.transformData.call(this, this.safeData.test[1], true);
        expect(this.safeData.test).toEqual({
          1: {
            id: '$1.00',
            test: 'Test 1'
          },
          2: {
            id: 2,
            test: 'Test 2'
          }
        });
      });
    });

    describe('getTransformerKeyByValue', function () {
      var transformer, value, result;
      beforeEach(function () {
        transformer = {test: 'blah'};
        value = 'blah';
        result = adminStaffService.getTransformerKeyByValue(transformer, value);
      });

      it('should find the appropriate property', function () {
        expect(result).toEqual('test');
      });
    });

    // @todo Update
    describe('getRecordDetails', function () {
      beforeEach(function () {
        this.getRecordDetails = adminStaffService.getRecordDetails.bind(this, 'test');
        this.transformData = adminStaffService.transformData.bind(this);
        spyOn(adminStaffService, 'changeUrl');
        spyOn(this, 'transformData');
      });

      describe('no callback', function () {
        beforeEach(function () {
          this.getRecordDetails(null, 1);
        });
        it('should change URL to the appropriate state', function () {
          expect(adminStaffService.changeUrl).toHaveBeenCalledWith({test: 1});
        });
        it('should set the ID of the parameter', function () {
          expect(this.testId).toEqual(1);
        });
        it('should call transformData', function () {
          expect(this.transformData).toHaveBeenCalled();
        });
        it('should set displayData', function () {
          expect(this.displayData).toEqual({ test: [  ], details: { id: 1, test: 'Test 1' } });
        });
      });
    });


    describe('getDirectiveTableColumns', function () {
      xit('should be defined');
    });

    describe('setTabs', function () {
      xit('should be defined');
    });

    describe('registerRecord', function () {
      xit('should be defined');
    });

    describe('attachUpdateWatcher', function () {
      xit('should attach and detach watchers');
    });

    describe('loadRecords', function () {
      xit('should load records on view load');
    });
  });
});
