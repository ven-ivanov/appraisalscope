// Mock AdminStaffService
var AdminStaffServiceMock = function ($q) {
  var Mock = {
    id: 1,
    section: 'staff',
    changeUrl: function () {

    },
    /**
     * ID val for hash data
     * @param idVal
     * @returns {*}
     */
    setIdVal: function (idVal) {
      // If idVal isn't set, or if a callback is passed as the third argument, return 'id'
      if (angular.isUndefined(idVal) || angular.isFunction(idVal)) {
        return 'id';
      }
      // Return value for idVal
      return idVal;
    },
    /**
     * Data handling
     *
     * This needs to work pretty much like how it does in the real service, or else it's going to make testing a pain
     */
    hashData: function (data, type, idVal, callback) {
      var that = this;
      // Default to id
      idVal = Mock.setIdVal(idVal);
      // Break references
      data = JSON.parse(JSON.stringify(data));
      // Store safe data
      angular.forEach(data, function (record) {
        that.safeData[type][record[idVal]] = record;
      });
      // Partial application
      if (arguments.length !== 4 && angular.isFunction(arguments[arguments.length - 1])) {
        callback = arguments[arguments.length - 1];
      }
      // Callback
      if (angular.isDefined(callback)) {
        callback();
      }
    },
    formatData: function (type, callback) {
      if (callback) {
        callback();
      }
    },
    transformData: function () {

    },
    getStateTransformer: function () {
      return {'AK': 'Arkansas'}
    },
    formatPlainObjectData: function() {

    },
    getTransformerKeyByValue:function() {

    },
    getRecordDetails: function() {

    },
    updateRecord  :function(){

    },
    checkValidNumber : function() {

    },
    setTabs: function() {

    },
    getDirectiveTableColumns : function () {

    },
    registerRecords: function() {

    },
    loadRecords : function() {

    },
  };

  return Mock;
};
