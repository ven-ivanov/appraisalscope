/**
 * Resource service mock
 */
var AdminStaffResourceServiceMock = function ($q) {
  return {
    staff: function () {
        return {
          $update: function () {
            return $q(function (resolve) {
              resolve();
            });
          },
          query: function () {
            return $q(function (resolve) {
              resolve();
            });
          }
        }
    },
    adminRuleProfile: function () {
      return {
        $update: function () {
          return $q(function (resolve) {
            resolve();
          });
        },
        query: function () {
          return $q(function (resolve) {
            resolve();
          });
        }
      }
    },
    reviewerRuleProfile: function () {
      return {
        $update: function () {
          return $q(function (resolve) {
            resolve();
          });
        },
        query: function () {
          return $q(function (resolve) {
            resolve();
          });
        }
      }
    },
    realEcFailureOrders: function () {
      return {
        $update: function () {
          return $q(function (resolve) {
            resolve();
          });
        },
        query: function () {
          return $q(function (resolve) {
            resolve();
          });
        }
      }
    }
  }
};
