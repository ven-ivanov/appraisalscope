'use strict';

/**
 * Base controller of the Admin Users section
 */
var app = angular.module('frontendApp');

app.controller('AdminStaffCtrl',
['$scope', 'AdminStaffService', '$stateParams','$state', function ($scope, AdminStaffService, $stateParams,$state) {
  var vm = this;

  if ($stateParams.activeTab === '') {
    $state.go('main.staff', {
      activeTab: 'staff'
    });

    return;
  }

  /**
   * Directives for each individual section
   */
  var directives = {
    staff: 'admin-staff-staff',
    'admin-rule-profile': 'admin-staff-admin-rule-profile',
    'reviewer-rule-profile': 'admin-staff-reviewer-rule-profile',
    'realec-failure-orders':'admin-staff-realec-failure-orders',
    'mercury-failure-orders':'admin-staff-mercury-failure-orders'
  };
  // Set active tab
  AdminStaffService.tab = vm.activeTab = $scope.activeTab = $stateParams.activeTab;

  // Set directive to compile
  $scope.directive = directives[$scope.activeTab];
  // Set page
  AdminStaffService.page = vm.page = $scope.page = $stateParams.page ? parseInt($stateParams.page) : null;
  //Entire States part should be followed
  /*Detailed explanation about buttons and sub views will be followed  */
}]);
