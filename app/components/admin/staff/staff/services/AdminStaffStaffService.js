/**
 * Created by Venelin on 6/16/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Admin User - Amc service
 */
app.factory('AdminStaffStaffService',
['API_PREFIX', 'AdminStaffService','AdminStaffAdminRuleProfileService','AdminStaffReviewerRuleProfileService','AsDataService',
  'AdminStaffResourceService', function (API_PREFIX, AdminStaffService,AdminStaffAdminRuleProfileService,AdminStaffReviewerRuleProfileService,AsDataService, AdminStaffResourceService) {
  var Service = {
    /**
     * @Todo need to manage selected staff id
     */
    // Safe data
    safeData: {
      //staff data
      staff: [],
      //table configuration
      heading: [
        {
          label: 'S.No',
          data: 'id'
        },
        {
          label: 'Name',
          data: 'name'
        },
        {
          label: 'Username',
          data: 'username'
        },
        {
          label: 'Email',
          data: 'email'
        },
        {
          label: 'Sales Person',
          data: 'isSalesPerson',
          type: 'boolean',
          filter: 'boolYesNo',
          checkbox: true
        },
        {
          label: 'Reviewer',
          data: 'reviewerRuleProfile',
          type: 'reviewer-rule-profile',
          noSort: true,
         /* checkbox: true,*/
          dropdown: true,
          selectOptions: [{value: 'percentage'}, {value: 'flat'}]
        },
        {
          //column heading is 'emails'
          label: 'Emails',
          data: 'emailPermission',
          // Displays the text "Email" in each column cell
          linkLabel: 'Email',
          // When "email" is clicked in any column, it calls tableCtrl.linkFn(id, functionName)
          fn: 'editEmailPermission',
          // By defining the data and setting the function to optional, it will only display in rows for
          // which there is data in the pay attribute
          optionalFn: true
        },
        {
          label: 'Rule',
          type: 'rule-profile',
          data: 'rule',
          dropdown: true,
          selectOptions: [{value: 'percentage'}, {value: 'flat'}]
        },
        {
          label: 'Vacation',
          data: 'isVacation',
          type: 'boolean',
          filter: 'boolYesNo',
          checkbox: true
        },
        {
          label: 'Tabs',
          // Displays the text "tabs" in each column cell
          linkLabel: 'Tabs',
          // When "Pay" is clicked in any column, it calls tableCtrl.linkFn(id, functionName)
          fn: 'editTabsPermission'
        },
        {
          label: 'Permission',
          data: 'permission',
          // Displays the text "permission" in each column cell
          linkLabel: 'Permission',
          // When "permission" is clicked in any column, it calls tableCtrl.linkFn(id, functionName)
          fn: 'editPermissions'
          // By defining the data and setting the function to optional, it will only display in rows for
          // which there is data in the pay attribute
          //optionalFn: true
        },
        {
          label: 'Log',
          type: 'action',
          data: 'log',
          // Displays the text "log" in each column cell
          linkLabel: 'log',
          // When "Log" is clicked in any column, it calls tableCtrl.linkFn(id, functionName)
          fn: 'showStaffLog'
        },
        {
          label: 'Edit',
          type: 'action',
          // Displays the text "edit" in each column cell
          linkLabel: 'edit',
          // When "edit" is clicked in any column, it calls tableCtrl.linkFn(id, functionName)
          fn: 'editStaffDetails'
        },
        {
          label: 'Delete',
          type: 'action',
          // Displays the text "delete" in each column cell
          linkLabel: 'delete',
          // When "delete" is clicked in any column, it calls tableCtrl.linkFn(id, functionName)
          fn: 'deleteStaff'
        }
      ]
    },
    // Display data
    displayData: {
      //staff data
      staff: [],
      //details
      details : {},
      //content title
      contentTitle: '',
      //selected staff
      selectedStaff: 0
    },
    // Data transformations
    transformers: {
      // Registered data
      registered: {
        filter: 'date'
      }
    },
    /**
     * Watch for changes to the current record
     */
    detachUpdateWatcher: '',
    attachUpdateWatcher: function ($scope, service) {
      //var vm = this;
      // Update staff info when typing
      AdminStaffService.detachUpdateWatcher = $scope.$watchCollection(function () {
        return service.displayData.details;
      }, function (newVal, oldVal) {
        if (angular.isUndefined(newVal) || angular.isUndefined(oldVal) || angular.equals(newVal, oldVal) || !Object.keys(oldVal).length) {
          return;
        }
        /*if (vm.typing) {
          $timeout.cancel(vm.typing);
        }
        // Wait and then make the request
        vm.typing = $timeout(function () {
          ngProgress.start();
          // Update record
          service.updateRecord(newVal)
            .finally(function () {
              ngProgress.complete();
            });
          vm.typing = null;
        }, REQUEST_DELAY.ms);*/
      });
    },
    /**
     * Request wrapper
     */
    request: function (requestFn, params) {
      // Get $resource configuration
      return AdminStaffResourceService.staff[requestFn](params);
    },
    /**
     * Retrieve settings
     */
    getStaff: function () {
      return AdminStaffResourceService.staff('init').then(function (response) {
        // Real API requests will contain a data parameter, if I need the rest of the object for any reason this can be changed later
        if (angular.isArray(response.data)) {
          response = response.data;
        }
        // Hash settings
        Service.hashData(response, 'staff');
      });
    },
    /**
     * Retrieve Rule(Admin) drop down and set values
     */
    getAdminRules: function() {
      var ruleColumnIndex = 7;
      return AdminStaffAdminRuleProfileService.request('init').then(function (response) {
        //load rule select box
        Service.safeData.heading[ruleColumnIndex].selectOptions = _.map(response.data,function(data){
          return {value: data.name, id: data.id};
        });
      });
    },
    /**
     * Retrieve Rule(Admin) dropdown and set values
     */
    getReviewerRules: function() {
      var reviewerColumnIndex = 5;
      //call service
      return AdminStaffReviewerRuleProfileService.request('init').then(function (response) {
        //load rule select box
        Service.safeData.heading[reviewerColumnIndex].selectOptions = _.map(response.data,function(data){
          return {value: data.name, id: data.id};
        });
      });
    }
  };

  /**
   * Inherit from AdminStaffService
   */
  Service.loadRecords = AdminStaffService.loadRecords.bind(Service, 'staff');
  Service.hashData = AsDataService.hashData.bind(Service);
  Service.formatData = AsDataService.formatData.bind(Service, 'staff');
  Service.transformData = AsDataService.transformData.bind(Service);
  // Get record details
  Service.getRecordDetails =
  /*AdminStaffService.getRecordDetails.bind(Service, 'staff',
  Service.getStaff);*/
    //we don't need to speicfy callback param for staff
  AdminStaffService.getRecordDetails.bind(Service, 'staff',
    null);

  // Update record
  //Service.updateRecord = AdminStaffService.updateRecord.bind(Service, 'staff');

  //Staff management
  Service.newStaff = AdminStaffService.newStaff.bind(Service,'staff');
  Service.updateStaff = AdminStaffService.updateStaff.bind(Service,'staff');
  Service.deleteStaff = AdminStaffService.deleteStaff.bind(Service,'staff');

  return Service;
}]);
