'use strict';

var app = angular.module('frontendApp');

/**
 * Search table for clients and users
 */
app.controller('AdminStaffStaffTableCtrl',
  ['AdminStaffStaffService', 'AsTableService', '$scope', '$q', 'AdminStaffTableCtrlInheritanceService', function (AdminStaffStaffService, AsTableService, $scope, $q, AdminStaffTableCtrlInheritanceService) {
    var vm = this;
    /**
     * @TODO
     */
    vm.page = {displayPage: 1};

    // Filter table by property
    vm.filterByProp = {};

    // Inherit table controller methods
    AdminStaffTableCtrlInheritanceService.inherit.call(vm, $scope, AdminStaffStaffService, 'staff');

    /**
     * Show record details
     */
    vm.rowFn = function (id) {
      // Detach watcher
      if (angular.isFunction(AdminStaffStaffService.detachUpdateWatcher)) {
        AdminStaffStaffService.detachUpdateWatcher();
      }
      // Get details
      AdminStaffStaffService.getRecordDetails(id).then(function () {
        // Attach update watcher once the record is retrieved
        AdminStaffStaffService.attachUpdateWatcher.call(vm, $scope, AdminStaffStaffService);
      });
    };
    /**
     * Edit staff details
     */
    vm.editStaffDetails = function () {
      //update selected staff id;
    };
    /**
     * Log details
     */
    vm.showStaffLog = function () {
      //update selected staff id;
      $scope.$parent.$broadcast('show-modal', 'admin-staff-logged-in-log');
    };

    /**
     * Edit staff
     */
    vm.editStaff = function () {
      //update selected staff id;
      vm.updateStaffModal();
    };
    /**
     * Emails action
     */
    vm.editEmailPermission = function () {
      //update selected staff id;
      $scope.$parent.$broadcast('show-modal', 'admin-staff-edit-email-permission');
    };
    /**
     * Tabs action
     */
    vm.editTabPermission = function () {
      //update selected staff id;
      $scope.$parent.$broadcast('show-modal', 'admin-staff-edit-tabs-permission');
    };
    /**
     * Permissions action
     */
    vm.editPermissions = function () {
      //update selected staff id;
      $scope.$parent.$broadcast('show-modal', 'admin-staff-edit-permission');
    };
    /**
     * Link functions
     */
    vm.linkFn = function (id, fnName) {
      // Keep reference to selected staff
      vm.displayData.selectedStaff = id;

      if (fnName === 'editPermissions') {
        vm.editPermissions(id);
      } else if (fnName === 'editTabsPermission') {
        vm.editTabPermission(id);
      } else if (fnName === 'editEmailPermission') {
        vm.editEmailPermission(id);
      } else if (fnName === 'deleteStaff') {
        vm.deleteStaffModal(id);
      } else if (fnName === 'showStaffLog') {
        vm.showStaffLog(id);
      } else if (fnName === 'editStaffDetails') {
        vm.editStaff(id);
      }
    };
  }]);
