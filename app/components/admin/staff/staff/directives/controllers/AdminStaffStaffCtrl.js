/**
 * Created by Venelin on 6/16/2015.
 */
'use strict';
var app = angular.module('frontendApp');
/**
 * @ngdoc function
 * @name frontendApp.controller:AdminStaffCtrl
 * @description
 * # AdminStaffStaffCtrl
 *
 * Controller of the frontendApp
 */
app.controller('AdminStaffStaffCtrl',
  [
    '$q',
    '$scope',
    'AdminStaffCtrlInheritanceService',
    'AdminStaffService',
    'AdminStaffStaffService',
    function ($q,$scope,AdminStaffCtrlInheritanceService, AdminStaffService,AdminStaffStaffService){
      var vm = this;
      // Display data
      vm.displayData = AdminStaffStaffService.displayData;
      // Safe data
      vm.safeData = AdminStaffStaffService.safeData;

      // Inherit common controller attributes
      AdminStaffCtrlInheritanceService.createCtrl.call(vm, AdminStaffStaffService, $scope, 'staff');

      // Reference to details on the currently selected staff(admin)
      vm.details = AdminStaffStaffService.details;

      vm.tabClass = function (tab) {
        return AdminStaffService.getDetailsTabClass(tab, vm.displayData.tab);
      };
      // Start off on staff tab
      AdminStaffService.currentDetailsTab = vm.currentDetailsTab;

      /**
       * Here we're dealing with all actions on staff tab
       */

      /**
       * Load table data on load
       */
      vm.init = function () {
        // Keep reference to which section we're on
        AdminStaffService.section = 'staff';
        // Set state on load based on query parameters
       // AdminStaffStaffService.setStateOnLoad('staff', $stateParams, vm);

        //load Admin Rules for Rule dropdown
        AdminStaffStaffService.getAdminRules();

        //load Reviewer Rules for Reviewer dropdown
        AdminStaffStaffService.getReviewerRules();
      };

      /**
       * Submit Profile either create new / edit
       */
      vm.submitStaff = function () {
        //get action
        if (angular.isDefined(vm.displayData.details) && (vm.displayData.details !== null)) {
          //update staff info
          vm.updateStaff();
        } else {
          //create new staff
          vm.newStaff();
        }
      };
      // Initiate controller
      vm.init();
    }]);
