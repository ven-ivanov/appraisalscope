/**
 * Created by Venelin on 6/11/2015.
 */
'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffStaff', ['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/staff/staff/directives/partials/staff.html',
    controller: 'AdminStaffStaffCtrl',
    controllerAs: 'adminStaffStaffCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
