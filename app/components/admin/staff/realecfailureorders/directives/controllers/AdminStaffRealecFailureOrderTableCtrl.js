/**
 * Created by Venelin on 6/22/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Search table for clients and users
 */
app.controller('AdminStaffRealecFailureOrderTableCtrl',
  ['AdminStaffRealecFailureOrderService', 'AsTableService', '$scope', '$q', 'AdminStaffTableCtrlInheritanceService',
    function (AdminStaffRealecFailureOrderService, AsTableService, $scope, $q, AdminStaffTableCtrlInheritanceService) {
      var vm = this;

      var heading =[
        {
          label: 'S.No',
          data: 'id'
        },
        {
          label: 'Transaction ID',
          data: 'transactionId'
        },
        {
          label: 'Unique ID',
          data: 'uniqueId'
        }, {
          label: 'Failure Reason',
          data:  'failureReason'
        },
        {
          label: 'Requested Date',
          data: 'requestDate'
        },
        {
          label: 'Action',
          type: 'action',
          linkLabel: 'Process',
          fn: 'processRealecOrder'
        }
      ];
      /**
       * @TODO
       */
      vm.page = {displayPage: 1};

      // Filter table by property
      vm.filterByProp = {};

      // Inherit table controller methods
      AdminStaffTableCtrlInheritanceService.inherit.call(vm, $scope, AdminStaffRealecFailureOrderService, heading, 'realecFailureOrders');

      /**
       * Link functions
       */
      vm.linkFn = function(id, fnName){
        if(fnName === 'processRealecOrder'){
          $scope.$parent.$broadcast('show-modal','process-order-confirm');
        }
      };
    }]);

