/**
 * Created by Venelin on 6/19/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Search table for clients and users
 */
app.controller('AdminStaffRealecFailureOrderCtrl',
  ['AdminStaffService','AdminStaffRealecFailureOrderService', 'AsTableService', '$scope', '$q', 'AdminStaffCtrlInheritanceService',
    function (AdminStaffService,AdminStaffRealecFailureOrderService, AsTableService, $scope, $q, AdminStaffCtrlInheritanceService) {
      var vm = this, // This service name is just getting out of hand with the line breaks
        Service = AdminStaffRealecFailureOrderService;
      // Inherit common functions
      AdminStaffCtrlInheritanceService.createCtrl.call(vm, Service, $scope, 'realecFailureOrders');
      // Directory details
      vm.displayData = Service.displayData;
      // Safe data
      vm.safeData = Service.safeData;

      /**
       * Initiate data and state
       */
      vm.init = function () {
        // Set state on load
        // Service.setStateOnLoad($stateParams, vm);
        // Keep reference to which section we're on
        AdminStaffService.section = 'realecFailureOrders';
      };
      /**
       * Process Mercury Order
       */
      vm.processOrder = function(orderId) {
        //process order
        AdminStaffRealecFailureOrderService.processOrder(orderId)
          .then(function () {
            $scope.$broadcast('hide-modal', 'process-order-confirm');
          })
          .catch(function () {
            $scope.$broadcast('show-modal', 'process-order-failure', true);
          });
      };
      /**
       * Search
       */
      vm.search = function() {
        AdminStaffRealecFailureOrderService.searchOrder();
      };
      // Init controller load
      vm.init();
    }]);

