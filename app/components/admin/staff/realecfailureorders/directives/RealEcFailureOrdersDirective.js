/**
 * Created by Venelin on 6/16/2015.
 */
'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffRealecFailureOrders', ['DirectiveInheritanceService',function (DirectiveInheritanceService) {

  return {
    templateUrl: '/components/admin/staff/realecfailureorders/directives/partials/realec-failure-orders.html',
    controller:'AdminStaffRealecFailureOrderCtrl',
    controllerAs:'adminStaffRealecFailureOrderCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
