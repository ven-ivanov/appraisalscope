/**
 * Created by Venelin on 6/19/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Staff - AdminStaffRealEcFailureOrderService
 */
app.factory('AdminStaffRealecFailureOrderService',
  ['API_PREFIX', 'AdminStaffService','AsDataService', 'AdminStaffResourceService', function (API_PREFIX, AdminStaffService,AsDataService, AdminStaffResourceService) {
    var Service = {
      // Safe data
      safeData: {
        //realec FailureOrders data
        realecFailureOrders: [],
        heading: []
      },
      // Display data
      displayData: {
        //realec FailureOrders data
        realecFailureOrders: [],
        details : []
      },
      //SEARCH info
      searchTransactionId: '',
      searchUniqueId: '',
      // Data transformations
      transformers: {
        // Registered data
        registered: {
          filter: 'date'
        }
      },
      /**
       * Request (mercuryFailureOrder, orderId)
       */
      request: function(requestFn, urlParams, bodyParams) {
        //Service.getRequestType();
        //get $resource configuration
        return AdminStaffResourceService.realecFailureOrders[requestFn](Service.safeData.requestType, urlParams, bodyParams);
      },
      /**
       * Retrieve settings
       */
      getrealecFailureOrders: function () {
        return Service.request('init').then(function (response) {
            // Hash settings
            Service.hashData(response.data, 'realecFailureOrders');
          });
      },
      /**
       * Process Mercury order
       */
      processOrder:function(orderId, params){
        //process Order
        return Service.request('processOrder', {orderId: orderId}, params);
      },
      /**
       * Search Order
       */
      searchOrder:function() {
        return Service.request('searchOrder',{transactionId: Service.searchTransactionId, uniqueId: Service.searchUniqueId})
          .then(function(response){
            Service.hashData(response.data, 'realecFailureOrders');
          });
      }
    };

    /**
     * Inherit from AdminStaffService
     */
    Service.loadRecords = AdminStaffService.loadRecords.bind(Service, 'realecFailureOrders');
    Service.hashData = AsDataService.hashData.bind(Service);
    Service.formatData = AsDataService.formatData.bind(Service, 'realecFailureOrders');
    Service.transformData = AsDataService.transformData.bind(Service);
    // Get record details
    Service.getRecordDetails =
      AdminStaffService.getRecordDetails.bind(Service, 'realecFailureOrders',
        Service.getrealecFailureOrders);
    // Set state
   // Service.setStateOnLoad = AdminStaffService.setStateOnLoad.bind(Service, 'realecFailureOrders');

    // Update record
    Service.updateRecord = AdminStaffService.updateRecord.bind(Service, 'realecFailureOrders');

    return Service;
  }]);
