'use strict';

var app = angular.module('frontendApp');

/**
 * Admin staff Broker
 *
 */
app.controller('AdminStaffProfileBrokerCtrl', ['AdminStaffProfilesCtrlInheritanceService','$scope',function (AdminStaffProfilesCtrlInheritanceService, $scope) {
      var vm = this;
      // Heading
      vm.heading = [
        // Whether currently active
        {
          label: 'Active',
          data: 'isActive',
          noSort: true,
          checkbox: true
        },
        // Name
        {
          label: 'Branch',
          data: 'name',
          noSort: true
        },
        //ID
        {
          label: 'ID',
          data: 'id',
          noSort: true
        }
      ];

      // Inherit common table functionality
      AdminStaffProfilesCtrlInheritanceService.inherit.call(vm, $scope, 'broker', true);
    }]);

