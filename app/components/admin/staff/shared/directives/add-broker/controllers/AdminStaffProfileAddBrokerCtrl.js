'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff Profile - Add broker
 */
app.controller('AdminStaffProfileAddBrokerCtrl',
  ['$scope','AdminStaffProfilesBrokerService' , 'AdminStaffService','AdminStaffCtrlInheritanceService', 'ngProgress',
    function ($scope,AdminStaffProfilesBrokerService , AdminStaffService,AdminStaffCtrlInheritanceService, ngProgress) {
      /**
       * @TODO
       */
      //vm point to this isolated directive controller and its scope
      var vm = this;
      // Safe and display data
      vm.safeData = AdminStaffProfilesBrokerService.safeData;
      vm.displayData = AdminStaffProfilesBrokerService.displayData;

      // Inherit common controller attributes
      AdminStaffCtrlInheritanceService.createCtrl.call(vm, AdminStaffProfilesBrokerService, $scope, 'broker');
      /**
       * Get initial data on load
       */
      vm.init = function () {
        // Table data
        AdminStaffProfilesBrokerService.getBrokerData()
          .then(function() {
            //init brokers list
            AdminStaffProfilesBrokerService.initSelectedBrokers();
            //now attach watcher
            vm.attachWatch();
          });
      };

      /**
       * Update top level dropdowns on backend
       */
      vm.attachWatch = function () {
        $scope.$watch(function () {
       return vm.safeData.broker;
       }, function (newVal, oldVal) {
          if (angular.isUndefined(oldVal) || angular.equals(oldVal, newVal)) {
            return;
          }
        }, true);
       };

       /**
       * Brokers
       */
      $scope.$watchCollection(function () {
        return vm.broker;
      }, function (newVal) {
        // If we have an order, then write it
        if (angular.isArray(newVal) && newVal.length) {
          ngProgress.start();
          AdminStaffProfilesBrokerService.addBroker(newVal, 'broker').then(function () {
            $scope.$broadcast('hide-modal', 'add-broker');
          }).finally(function () {
            ngProgress.complete();
            // Reset array
            $scope.broker = [];
          }).catch(function () {
            // Show failure
            $scope.$broadcast('show-modal', 'add-broker', true);
          });
        }
      });

      //add Broker
      vm.addBrokerModal = function() {
        $scope.$broadcast('show-modal', 'admin-rule-profile-add-broker', true);
      };
      // Initiate data
      vm.init();
    }]);
