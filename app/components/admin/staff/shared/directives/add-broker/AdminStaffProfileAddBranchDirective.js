'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffProfileAddBroker',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {scope: true,
    bindToController: {
      broker: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/add-broker/partials/broker.html',
    controller:'AdminStaffProfileAddBrokerCtrl',
    controllerAs:'adminStaffProfileAddBrokerCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
