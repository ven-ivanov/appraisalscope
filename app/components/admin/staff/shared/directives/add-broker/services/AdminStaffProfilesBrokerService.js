'use strict';

var app = angular.module('frontendApp');

app.factory('AdminStaffProfilesBrokerService',
  ['AdminStaffService', 'AdminStaffResourceService', 'AsDataService', function (AdminStaffService, AdminStaffResourceService, AsDataService) {
    var Service = {
      safeData: {
        // Table data
        broker: {},
        // selected list of broker
        selectedBroker: []
      },
      displayData: {
        // Table data
        broker: []
      },
      /**
       * Request (either company, user, or branch)
       */
      request: function (requestFn, urlParams, bodyParams) {
        var type='adminRuleProfile';
        // Get resource configuration
        if(type === 'adminRuleProfile') {
          return AdminStaffResourceService.adminRuleProfile[requestFn](urlParams, bodyParams);
        }
        else if (type === 'reviewerRuleProfile'){
          return AdminStaffResourceService.reviewerRuleProfile[requestFn](urlParams, bodyParams);
        }
      },
      /**
       * Get auto assignment data
       */
      getBrokerData: function () {
        return Service.request('brokers').then(function (response) {
          // Make a simple hash for manipulation
          Service.hashData(response.data, 'broker');
          Service.formatData();
        });
      },
       /**
       * Initialize brokers
       *
       * @todo Waiting on the backend
       */
      initSelectedBrokers: function () {
        angular.forEach(Service.safeData.broker, function (item) {
          // Find the approved list
          if (angular.isDefined(item.selectedBroker)) {
            angular.forEach(item.selectedBroker, function (list) {
              Service.safeData.selectedBroker.push(list);
            });
          }
        });
        // Sort by order
        Service.safeData.selectedBroker.sort(Service.sortByOrder);
      },
      /**
       * Write updates to backend
       * @param values
       */
      addBroker: function (values) {
        return Service.request('addBroker', values);
      }
    };

    // Inherit
    Service.hashData = AsDataService.hashData.bind(Service);
    Service.formatData = AsDataService.formatData.bind(Service, 'broker');
    Service.transformData = AsDataService.transformData.bind(Service);
    Service.getRecordDetails =
      AdminStaffService.getRecordDetails.bind(Service, 'broker',
        null);
    Service.checkGroups = AdminStaffService.checkGroups.bind(Service);
    return Service;
  }]);
