'use strict';

var app = angular.module('frontendApp');

app.factory('AdminStaffSetFeesByBrokerService',
  ['AdminStaffService', 'AdminStaffResourceService', 'AsDataService', function (AdminStaffService, AdminStaffResourceService, AsDataService) {
    var Service = {
      safeData: {
        // Table data
        brokers: [],
        jobTypes: [],

        // selected list of brokers
        selectedBrokers: [],
        selectedJobTypes: []
      },
      displayData: {
        // Table data
        brokers: [],
        jobTypes: [],
        //multi select from table ctrl
        multiSelect: {},
        companyId: 0
      },
      /**
       * Request (either company, user, or broker)
       */
      request: function (requestFn, urlParams, bodyParams) {
        // Get resource configuration
        return AdminStaffResourceService.staff[requestFn](urlParams, bodyParams);
      },
      /**
       * Get auto assignment data
       */
      getBrokersData: function () {
        return Service.request('brokers',{companyId: Service.displayData.companyId})
          .then(function (response) {
            // Make a simple hash for manipulation
            Service.hashData(response.data, 'brokers');
            Service.formatData();
          });
      },
      /**
       * Sort the approved forms by order
       * @param a
       * @param b
       * @returns {number}
       */
      sortByOrder: function (a, b) {
        return (a.order < b.order) ? -1 : 1;
      },
      /**
       * Initialize job types
       *
       * @todo Waiting on the backend
       */
      initSelectedBrokers: function () {
        angular.forEach(Service.safeData.brokers, function (item) {
          // Find the approved list
          if (angular.isDefined(item.selectedBrokers)) {
            angular.forEach(item.selectedBrokers, function (list) {
              Service.safeData.selectedBrokers.push(list);
            });
          }
        });
        // Sort by order
        Service.safeData.selectedBrokers.sort(Service.sortByOrder);
      },
      /**
       * Write updates to backend
       * @param values
       */
      setCommission: function (values) {
        return Service.request('addBrokers', values);
      }
    };

    // Inherit
    Service.hashData = AsDataService.hashData.bind(Service);
    Service.formatData = AsDataService.formatData.bind(Service, 'brokers');
    Service.transformData = AsDataService.transformData.bind(Service);
    Service.getRecordDetails =
      AdminStaffService.getRecordDetails.bind(Service, 'brokers',
        null);

    Service.checkGroups = AdminStaffService.checkGroups.bind(Service);
    return Service;
  }]);
