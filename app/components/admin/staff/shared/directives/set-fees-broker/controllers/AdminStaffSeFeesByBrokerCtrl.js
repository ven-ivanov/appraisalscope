'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff Set Commission
 */
app.controller('AdminStaffSetFeesByBrokerCtrl',
  ['$scope','AdminStaffSetFeesByBrokerService' , 'AdminStaffService','AdminStaffCommissionCtrlInheritanceService',
    function ($scope,AdminStaffSetFeesByBrokerService , AdminStaffService,AdminStaffCommissionCtrlInheritanceService) {
      /**
       * @TODO
       */
      //vm point to this isolated directive controller and its scope
      var vm = this;
      // Safe and display data
      vm.safeData = AdminStaffSetFeesByBrokerService.safeData;
      vm.displayData = AdminStaffSetFeesByBrokerService.displayData;
      // Inherit common controller attributes
      AdminStaffCommissionCtrlInheritanceService.createCtrl.call(vm, AdminStaffSetFeesByBrokerService,$scope,  'brokers');
      //add commission
      vm.addFeesBranchModal = function() {
        $scope.$broadcast('show-modal', 'set-fees-branch-modal', true);
      };
      //add Job Type
      vm.addFeesBrokerModal = function() {
        $scope.$parent.$broadcast('show-modal', 'set-fees-broker-modal', true);
      };
      /**
       * Get initial data on load
       */
      vm.init = function () {
        // Table data
        AdminStaffSetFeesByBrokerService.getBrokersData()
          .then(function() {
            //init job types list
            AdminStaffSetFeesByBrokerService.initSelectedBrokers();
            //now attach watcher
            vm.attachWatch();
          });
      };
      // Initiate data
      vm.init();
    }]);
