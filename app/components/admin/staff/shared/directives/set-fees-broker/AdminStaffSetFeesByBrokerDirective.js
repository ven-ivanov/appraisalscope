'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffSetFeesByBroker',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {scope: true,
    bindToController: {
      jobType: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/set-fees-broker/partials/set-fees-broker.html',
    controller:'AdminStaffSetFeesByBrokerCtrl',
    controllerAs:'adminStaffSetFeesByBrokerCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
