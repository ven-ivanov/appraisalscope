'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffRecalcCommissionBranch',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {scope: true,
    bindToController: {
      jobType: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/recalc-commission-branch/partials/recalc-commission-branch.html',
    controller:'AdminStaffRecalcCommissionBranchCtrl',
    controllerAs:'adminStaffRecalcCommissionBranchCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
