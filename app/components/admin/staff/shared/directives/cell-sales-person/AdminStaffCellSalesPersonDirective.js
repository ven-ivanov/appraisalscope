'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffCellSalesPerson',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    scope: true,
    bindToController: {
      company: '=',
      service: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/cell-sales-pee/partials/partials.html',
    controller:'AdminStaffCellSalesPersonCtrl',
    controllerAs:'adminStaffCellSalesPersonCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
