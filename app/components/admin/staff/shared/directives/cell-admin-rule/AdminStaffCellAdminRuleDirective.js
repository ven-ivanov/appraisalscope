'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffCellAdminRule',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    scope: true,
    bindToController: {
      company: '=',
      service: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/cell-admin-rule/partials/partials.html',
    controller:'AdminStaffCellAdminRuleCtrl',
    controllerAs:'adminStaffCellAdminRuleCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);

