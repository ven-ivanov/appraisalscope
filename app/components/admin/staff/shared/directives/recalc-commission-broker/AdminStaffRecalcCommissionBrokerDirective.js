'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffRecalcCommissionBroker',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {scope: true,
    bindToController: {
      jobType: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/recalc-commission-broker/partials/recalc-commission-broker.html',
    controller:'AdminStaffRecalcCommissionBrokerCtrl',
    controllerAs:'adminStaffRecalcCommissionBrokerCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
