'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff Set Commission
 */
app.controller('AdminStaffRecalcCommissionBrokerCtrl',
  ['$scope','AdminStaffRecalcCommissionBrokerService' , 'AdminStaffService','AdminStaffCtrlInheritanceService',
    function ($scope,AdminStaffRecalcCommissionBrokerService , AdminStaffService,AdminStaffCtrlInheritanceService) {
      /**
       * @TODO
       */
      //vm point to this isolated directive controller and its scope
      var vm = this;
      // Safe and display data
      vm.safeData = AdminStaffRecalcCommissionBrokerService.safeData;
      vm.displayData = AdminStaffRecalcCommissionBrokerService.displayData;

      // Inherit common controller attributes
      AdminStaffCtrlInheritanceService.createCtrl.call(vm, AdminStaffRecalcCommissionBrokerService, $scope, 'commission');
      /**
       * Get initial data on load
       */
      vm.init = function () {
        // Table data
        AdminStaffRecalcCommissionBrokerService.getJobTypesData()
          .then(function () {
            //init job types list
            AdminStaffRecalcCommissionBrokerService.initSelectedJobTypes();
            //now attach watcher
            vm.attachWatch();
          });
      };

      /**
       * Update top level dropdowns on backend
       */
      vm.attachWatch = function () {
        $scope.$watch(function () {
          return vm.safeData.commission;
        }, function (newVal, oldVal) {
          if (angular.isUndefined(oldVal) || angular.equals(oldVal, newVal)) {
            return;
          }
          /**
           * @todo have to implement recalc commission
           */
        }, true);
      };

        /**
       * Job Type
       */
        $scope.$watchCollection(function () {
          return vm.commission;
        }, function (newVal) {
          // If we have an order, then write it
          if (angular.isArray(newVal) && newVal.length) {
            AdminStaffRecalcCommissionBrokerService.setCommission(newVal, 'commissions')
              .then(function () {
                $scope.$broadcast('hide-modal', 'recalc-commission-company');
              }).finally(function () {
                // Reset array
                $scope.commission = [];
              }).catch(function () {
                // Show failure
                $scope.$broadcast('show-modal', 'recalc-commission-company', true);
              });
          }
        });

      //add Job Type
      vm.addRecalcCommissionModal = function() {
        $scope.$broadcast('show-modal', 'recalc-commission-company', true);
      };
      // Initiate data
      vm.init();
    }]);
