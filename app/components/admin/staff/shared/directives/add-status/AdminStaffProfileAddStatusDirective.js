/**
 * Created by Venelin on 6/26/2015.
 */
'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffProfileAddStatus',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    scope: true,
    bindToController: {
      status: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/add-status/partials/add-status.html',
    controller:'AdminStaffProfileAddStatusCtrl',
    controllerAs:'adminStaffProfileAddStatusCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);

