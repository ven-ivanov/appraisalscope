/**
 * Created by Venelin on 7/3/2015.
 */

'use strict';

var app = angular.module('frontendApp');

app.factory('AdminStaffProfilesProcessStatusService',
  ['AdminStaffService', 'AdminStaffResourceService', 'AsDataService', function (AdminStaffService, AdminStaffResourceService, AsDataService) {
    var Service = {
      safeData: {
        // Table data
        processStatus: {},
        // selected list of paid status
        selectedStatus: []
      },
      displayData: {
        // Table data
        processStatus: [],
        //multi select from table ctrl
        multiSelect: {}
      },
      /**
       * Request (status)
       */
      request: function (requestFn, urlParams, bodyParams) {
        // Get resource configuration
        return AdminStaffResourceService.processStatus[requestFn](urlParams, bodyParams);
      },
      /**
       * Get auto assignment data
       */
      getStatusData: function () {
        return Service.request('init').then(function (response) {
          // Make a simple hash for manipulation
          Service.hashData(response.data, 'processStatus');
          Service.formatData();
        });
      },
      /**
       * Sort the approved forms by order
       * @param a
       * @param b
       * @returns {number}
       */
      sortByOrder: function (a, b) {
        if (a.order < b.order) {
          return -1;
        }
        if (a.order > b.order) {
          return 1;
        }
        return 0;
      },
      /**
       * Initialize status
       * @todo Waiting on the backend
       */
      initSelectedStatus: function () {
        angular.forEach(Service.safeData.status, function (item) {
          // Find the approved list
          if (angular.isDefined(item.selectedStatus)) {
            angular.forEach(item.selectedStatus, function (list) {
              Service.safeData.selectedStatus.push(list);
            });
          }
        });
        // Sort by order
        Service.safeData.selectedStatus.sort(Service.sortByOrder);
      },
      /**
       * Write updates to backend
       * @param values
       */
      addProcessStatus: function (values) {
        return Service.request('addProcessStatus', values);
      }
    };

    // Inherit
    Service.hashData = AsDataService.hashData.bind(Service);
    Service.formatData = AsDataService.formatData.bind(Service, 'processStatus');
    Service.transformData = AsDataService.transformData.bind(Service);

    Service.getRecordDetails =
      AdminStaffService.getRecordDetails.bind(Service, 'processStatus',
        null);
    Service.checkGroups = AdminStaffService.checkGroups.bind(Service);
    return Service;
  }]);
