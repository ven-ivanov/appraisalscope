/**
 * Created by Venelin on 6/27/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff Profile - Add Status
 */
app.controller('AdminStaffProfileAddStatusCtrl',
  ['$scope', 'AdminStaffProfilesProcessStatusService' , 'AdminStaffService','AdminStaffCtrlInheritanceService', 'ngProgress',
    function ($scope,AdminStaffProfilesProcessStatusService, AdminStaffService,AdminStaffCtrlInheritanceService, ngProgress) {
      //vm point to this isolated directive controller and its scope
      var vm = this;
      // Safe and display data
      vm.safeData = AdminStaffProfilesProcessStatusService.safeData;
      vm.displayData = AdminStaffProfilesProcessStatusService.displayData;
      // Inherit common controller attributes
      AdminStaffCtrlInheritanceService.createCtrl.call(vm, AdminStaffProfilesProcessStatusService, $scope, 'processStatus');


      /**
       * Get initial data on load
       */
      vm.init = function () {
        // Table data
        AdminStaffProfilesProcessStatusService.getStatusData()
          .then(function() {
            //init states list
            AdminStaffProfilesProcessStatusService.initSelectedStatus();
            //now attach watcher
            vm.attachWatch();
          });
      };

      /**
       * Update top level dropdowns on backend
       */
      vm.attachWatch = function () {
        $scope.$watch(function () {
          return vm.status;
        }, function (newVal, oldVal) {
          if (angular.isUndefined(oldVal) || angular.equals(oldVal, newVal)) {
            return;
          }
        }, true);
      };

      /**
       * Watch status
       */
      $scope.$watchCollection(function () {
        return vm.safeData.status;
      }, function (newVal) {
        // If we have an order, then write it
        if (angular.isArray(newVal) && newVal.length) {
          ngProgress.start();
          AdminStaffProfilesProcessStatusService.addProcessStatus(newVal, 'status').then(function () {
            $scope.$broadcast('hide-modal', 'add-status');
          }).finally(function () {
            ngProgress.complete();
            // Reset array
            $scope.status = [];
          }).catch(function () {
            // Show failure
            $scope.$broadcast('show-modal', 'add-status', true);
          });
        }
      });
      //add process status
      vm.addProcessStatusModal = function() {
        $scope.$broadcast('show-modal', 'admin-rule-profile-add-status', true);
      };
      vm.allProcessStatusSelected = '';
      // Initiate data
      vm.init();
    }]);
