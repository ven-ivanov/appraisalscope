/**
 * Created by Venelin on 7/2/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff Companies - Companies - Groups
 */
app.controller('AdminStaffCompaniesCtrl', ['AdminStaffProfilesCtrlInheritanceService','$scope',function (AdminStaffProfilesCtrlInheritanceService, $scope) {
      var vm = this;
      // Heading
      vm.heading = [
        // Whether currently active
        {
          label: 'Active',
          data: 'active',
          noSort: true,
          checkbox: true
        },
        // Name
        {
          label: 'Company',
          data: 'name',
          noSort: true
        }
      ];

      // Inherit common table functionality
      AdminStaffProfilesCtrlInheritanceService.inherit.call(vm, $scope, 'companies', true);
    }]);
