/**
 * Created by Venelin on 6/26/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff - Add Company
 */
app.controller('AdminStaffProfileAddCompanyCtrl',
  ['$scope','AdminStaffProfilesCompanyService' , 'AdminStaffCtrlInheritanceService','AdminStaffService', 'ngProgress',
    function ($scope,AdminStaffProfilesCompanyService,AdminStaffCtrlInheritanceService, AdminStaffService, ngProgress) {
      //vm point to this isolated directive controller and its scope
      /**
       * @TODO
       *
       * */

      var vm = this;

      // Safe and display data
      vm.safeData = AdminStaffProfilesCompanyService.safeData;
      vm.displayData = AdminStaffProfilesCompanyService.displayData;


      // Inherit common controller attributes
      AdminStaffCtrlInheritanceService.createCtrl.call(vm, AdminStaffProfilesCompanyService, $scope, 'companies');
      /**
       * Get initial data on load        // Table data

       */
      vm.init = function () {
        AdminStaffProfilesCompanyService.getCompaniesData()
          .then(function() {
            //init states list
            AdminStaffProfilesCompanyService.initSelectedCompanies();
            //now attach watcher
            vm.attachWatch();
          });
      };

      /**
       * Update top level dropdowns on backend
       */
      vm.attachWatch = function () {
        $scope.$watch(function () {
          return vm.safeData.companies;
        }, function (newVal, oldVal) {
          if (angular.isUndefined(oldVal) || angular.equals(oldVal, newVal)) {
            return;
          }
        }, true);
      };

      /**
       * Approved list order
       */
      $scope.$watchCollection(function () {
        return vm.companies;
      }, function (newVal) {
        // If we have an order, then write it
        if (angular.isArray(newVal) && newVal.length) {
          ngProgress.start();
          AdminStaffProfilesCompanyService.addCompanies(newVal, 'companies').then(function () {
            $scope.$broadcast('hide-modal', 'admin-rule-profile-add-company');
          }).finally(function () {
            ngProgress.complete();
            // Reset array
            $scope.companies = [];
          }).catch(function () {
            // Show failure
            $scope.$broadcast('show-modal', 'admin-rule-profile-add-company', true);
          });
        }
      });
      //add Companies Modal
      vm.addCompaniesModal = function() {
         $scope.$broadcast('show-modal', 'admin-rule-profile-add-company', true);
      };

      // Initiate data
      vm.init();
    }]);
