/**
 * Created by Venelin on 6/26/2015.
 */
'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffProfileAddCompany',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    scope: true,
    bindToController: {
      company: '=',
      service: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/add-company/partials/add-company.html',
    controller:'AdminStaffProfileAddCompanyCtrl',
    controllerAs:'adminStaffProfileAddCompanyCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
