'use strict';

var app = angular.module('frontendApp');

app.factory('AdminStaffSetCommissionBranchService',
  ['AdminStaffService', 'AdminStaffResourceService', 'AsDataService', function (AdminStaffService, AdminStaffResourceService, AsDataService) {
    var Service = {
      safeData: {
        // Table data
        jobTypes: [],
        commissions: {},
        // selected list of commissions
        selectedCommissions: [],
        selectedJobTypes: []
      },
      displayData: {
        // Table data
        jobTypes: [],
        commissions: [],
        selectedCommissions: [],
        selectedJobTypes: [],
        //multi select from table ctrl
        multiSelect: {}
      },
      /**
       * Request (either company, user, or branch)
       */
      request: function (requestFn, urlParams, bodyParams) {
        // Get resource configuration
        return AdminStaffResourceService.jobTypes[requestFn](urlParams, bodyParams);
      },
      /**
       * Get auto assignment data
       */
      getJobTypesData: function () {
        return Service.request('init')
          .then(function (response) {
            // Make a simple hash for manipulation
            Service.hashData(response.data, 'jobTypes');
            Service.formatData();
          });
      },
      /**
       * Initialize job types
       *
       * @todo Waiting on the backend
       */
      initSelectedJobTypes: function () {
        angular.forEach(Service.safeData.jobTypes, function (item) {
          // Find the approved list
          if (angular.isDefined(item.selectedJobTypes)) {
            angular.forEach(item.selectedJobTypes, function (list) {
              Service.safeData.selectedJobTypes.push(list);
            });
          }
        });
        // Sort by order
        Service.safeData.selectedJobTypes.sort(Service.sortByOrder);
      },
      /**
       * Sort the approved forms by order
       * @param a
       * @param b
       * @returns {number}
       */
      sortByOrder: function (a, b) {
        return (a.order < b.order) ? -1 : 1;
      },
      /**
       * Write updates to backend
       * @param values
       */
      setCommission: function (values) {
        return Service.request('addCommissions', values);
      }
    };

    // Inherit
    Service.hashData = AsDataService.hashData.bind(Service);
    Service.formatData = AsDataService.formatData.bind(Service, 'jobTypes');
    Service.transformData = AsDataService.transformData.bind(Service);
    Service.getRecordDetails =
      AdminStaffService.getRecordDetails.bind(Service, 'jobTypes',
        null);

    Service.checkGroups = AdminStaffService.checkGroups.bind(Service);
    return Service;
  }]);
