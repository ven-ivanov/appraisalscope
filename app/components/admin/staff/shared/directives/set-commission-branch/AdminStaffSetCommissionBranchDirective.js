'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffSetCommissionBranch',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {scope: true,
    bindToController: {
      jobType: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/set-commission-branch/partials/set-commission-branch.html',
    controller:'AdminStaffSetCommissionBranchCtrl',
    controllerAs:'adminStaffSetCommissionBranchCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
