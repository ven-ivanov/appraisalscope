'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff Set Commission
 */
app.controller('AdminStaffSetCommissionBranchCtrl',
  ['$scope','AdminStaffSetCommissionBranchService' , 'AdminStaffService','AdminStaffCommissionCtrlInheritanceService',
    function ($scope,AdminStaffSetCommissionBranchService , AdminStaffService,AdminStaffCommissionCtrlInheritanceService) {
      /**
       * @TODO
       */
      //vm point to this isolated directive controller and its scope
      var vm = this;
      // Safe and display data
      vm.safeData = AdminStaffSetCommissionBranchService.safeData;
      vm.displayData = AdminStaffSetCommissionBranchService.displayData;
      vm.modalTitle = 'set-commission-branch-modal';
      // Inherit common controller attributes - commission ctrl
      AdminStaffCommissionCtrlInheritanceService.createCtrl.call(vm, AdminStaffSetCommissionBranchService, $scope, 'jobTypes');


      //add Job Type
      vm.addCommissionModal = function() {
        $scope.$broadcast('show-modal',vm.modalTitle, true);
      };
      // Initiate data
      vm.init();
    }]);
