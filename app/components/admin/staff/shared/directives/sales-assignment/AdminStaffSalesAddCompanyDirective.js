'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffSaleAddAssignment',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    scope: true,
    bindToController: {
      company: '=',
      service: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/sales-assignment/partials/add-assignment.html',
    controller:'AdminStaffSalesAssignmentCtrl',
    controllerAs:'adminStaffSalesAssignmentCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
