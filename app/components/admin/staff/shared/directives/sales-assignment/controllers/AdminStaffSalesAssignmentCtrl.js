'use strict';

var app = angular.module('frontendApp');

app.controller('AdminStaffSalesAssignmentCtrl',
  ['$scope','AdminStaffSalesAssignmentService' , 'AdminStaffCommissionCtrlInheritanceService',
    function ($scope,AdminStaffSalesAssignmentService,AdminStaffCommissionCtrlInheritanceService) {
      //vm point to this isolated directive controller and its scope
      /**
       * @TODO
       *
       * */

      var vm = this;

      // Safe and display data
      vm.safeData = AdminStaffSalesAssignmentService.safeData;
      vm.displayData = AdminStaffSalesAssignmentService.displayData;
      vm.showCompanies = false;


      // Inherit common controller attributes
      AdminStaffCommissionCtrlInheritanceService.createCtrl.call(vm, AdminStaffSalesAssignmentService, $scope, 'companies');
      //add Assignment Modal
      vm.addAssignmentModal = function() {
         $scope.$broadcast('show-modal', 'staff-add-assignment', true);
      };
      // Toggle companies
      vm.toggleCompanies = function() {
        vm.showCompanies = !vm.showCompanies;
      };
      /**
       * Get initial data on load
       */
      vm.init = function () {
        AdminStaffSalesAssignmentService.getCompaniesData()
          .then(function() {
            //init states list
            AdminStaffSalesAssignmentService.initSelectedCompanies();
            //attach watach
            vm.attachWatch();
          });
      };
      // Initiate data
      vm.init();
     }]);
