
'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff Companies - Companies - Groups
 */
app.controller('AdminStaffSalesAssignmentTableCtrl',
  ['AdminStaffAssignmentCtrlInheritanceService', 'AdminStaffSalesAssignmentService', '$scope',
    function (AdminStaffAssignmentCtrlInheritanceService, AdminStaffSalesAssignmentService, $scope) {
      var vm = this;
      // Heading
      vm.heading = [
        // Whether currently active
        {
          label: 'Active',
          data: 'active',
          noSort: true,
          checkbox: true
        },
        // Name
        {
          label: 'Company',
          data: 'name',
          noSort: true
        },
        //set fees by branch
        {
          label: 'Set Fees By Branch',
          fn: 'setFeesByBranch',
          type: 'action',
          linkLabel: 'Set Fees By Branch'
        },
        //set fees by broker
        {
          label: 'Set Fees By Broker',
          fn: 'setFeesByBroker',
          type: 'action',
          linkLabel: 'Set Fees By Broker'
        },
        //re-calc commission
        {
          label: 'Re-calculate Commissions',
          fn: 'recalcCommissions',
          type: 'action',
          linkLabel: 'Re-calculate commissions'
        }
      ];

      /**
       * Link functions
       */
      vm.linkFn = function(id, fnName){
        AdminStaffSalesAssignmentService.displayData.selectedCompanies.push(id);
         if(fnName === 'setFeesByBranch') {
          $scope.$parent.$broadcast('show-modal','set-fees-branch-modal', true);
        } else if(fnName === 'setFeesByBroker') {
          $scope.$parent.$broadcast('show-modal','set-fees-broker-modal', true);
        } else if(fnName === 'recalcCommissions'){
           $scope.$parent.$broadcast('show-modal','recalc-commission-company', true);
         }


      };

      // Inherit common table functionality
      AdminStaffAssignmentCtrlInheritanceService.inherit.call(vm, $scope, AdminStaffSalesAssignmentService, 'companies');
    }]);
