/**
 * Created by Venelin on 7/3/2015.
 */
'use strict';

var app = angular.module('frontendApp');

app.factory('AdminStaffSalesAssignmentService',
  ['AdminStaffService', 'AdminStaffResourceService', 'AsDataService', function (AdminStaffService, AdminStaffResourceService, AsDataService) {
    var Service;
    Service = {
      safeData: {
        // Table data
        companies: [],
        // selected list of companies
        selectedCompanies: []
      },
      displayData: {
        // Table data
        companies: [],
        selectedCompanies: [],
        //multi select from table ctrl
        multiSelect: []
      },

      /**
       * Request (either company, user, or branch)
       */
      request: function (requestFn, urlParams, bodyParams) {
        // Get resource configuration
        return AdminStaffResourceService.adminRuleProfile[requestFn](urlParams, bodyParams);
      },
      /**
       * Get auto assignment data
       */
      getCompaniesData: function () {
        return Service.request('init')
          .then(function (response) {
            // Make a simple hash for manipulation
            console.log('--------------selected companies');
            console.log(response);
            Service.hashData(response.data, 'companies');
            Service.formatData();

          });
      },
      /**
       * Sort the approved forms by order
       * @param a
       * @param b
       * @returns {number}
       */
      sortByOrder: function (a, b) {
        return (a.order < b.order) ? -1 : 1;
      },
      /**
       * Initialize approved list
       *
       * @todo Waiting on the backend
       */
      initSelectedCompanies: function () {
        angular.forEach(Service.safeData.companies, function (item) {
          // Find the approved list
          if (angular.isDefined(item.selectedCompanies)) {
            angular.forEach(item.selectedCompanies, function (list) {
              Service.safeData.selectedCompanies.push(list);
            });
          }
        });
        // Sort by order
        Service.safeData.selectedCompanies.sort(Service.sortByOrder);
      },
      /**
       * Write updates to backend
       * @param values
       */
      addCompanies: function (values) {
        return Service.request('addCompanies', null, values);
      }
    };

    // Inherit
    Service.hashData = AsDataService.hashData.bind(Service);
    Service.formatData = AsDataService.formatData.bind(Service, 'companies');
    Service.transformData = AsDataService.transformData.bind(Service);

    Service.getRecordDetails =
      AdminStaffService.getRecordDetails.bind(Service, 'companies',
        null);

    Service.checkGroups = AdminStaffService.checkGroups.bind(Service);
    return Service;
  }]);

