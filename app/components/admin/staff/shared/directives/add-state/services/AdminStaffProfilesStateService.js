/**
 * Created by Venelin on 7/3/2015.
 */
'use strict';

var app = angular.module('frontendApp');

app.factory('AdminStaffProfilesStateService',
  ['AdminStaffService', 'AdminStaffResourceService', 'AsDataService', function (AdminStaffService, AdminStaffResourceService, AsDataService) {
    var Service = {
      safeData: {
        // Table data
        states: {},
        // selected list of paid status
        selectedStates: []
      },
      displayData: {
        // Table data
        states: [],
        //multi select from table ctrl
        multiSelect: {}
      },
      /**
       * Request (company)
       */
      request: function (requestFn, urlParams, bodyParams) {
        // Get resource configuration
        return AdminStaffResourceService.states[requestFn](urlParams, bodyParams);
      },
      /**
       * Get auto assignment data
       */
      getStatesData: function () {
        return Service.request('init').then(function (response) {
          // Make a simple hash for manipulation
          Service.hashData(response.data, 'states');
          Service.formatData();
        });
      },
      /**
       * Sort the approved forms by order
       * @param a
       * @param b
       * @returns {number}
       */
      sortByOrder: function (a, b) {
        if (a.order < b.order) {
          return -1;
        }
        if (a.order > b.order) {
          return 1;
        }
        return 0;
      },
      /**
       * Initialize approved list
       *
       * @todo Waiting on the backend
 .      */
      initSelectedStates: function () {
        angular.forEach(Service.safeData.states, function (item) {
          // Find the approved list
          if (angular.isDefined(item.selectedStates)) {
            angular.forEach(item.selectedStates, function (list) {
              Service.safeData.selectedStates.push(list);
            });
          }
        });
        // Sort by order
        Service.safeData.selectedStates.sort(Service.sortByOrder);
      },
      /**
       * Write updates to backend
       * @param values
       */
      addStates: function (values) {
        return Service.request('addStates', values);
      }
    };

    // Inherit
    Service.hashData = AsDataService.hashData.bind(Service);
    Service.formatData = AsDataService.formatData.bind(Service, 'states');
    Service.transformData = AsDataService.transformData.bind(Service);

    Service.getRecordDetails =
      AdminStaffService.getRecordDetails.bind(Service, 'states',
        null);
    Service.checkGroups = AdminStaffService.checkGroups.bind(Service);
    return Service;
  }]);
