/**
 * Created by Venelin on 6/26/2015.
 */
'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffProfileAddState',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    scope: true,
    bindToController: {
      states: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/add-state/partials/add-state.html',
    controller:'AdminStaffProfileAddStateCtrl',
    controllerAs:'adminStaffProfileAddStateCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);

