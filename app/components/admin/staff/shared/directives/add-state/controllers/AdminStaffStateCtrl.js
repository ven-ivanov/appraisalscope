/**
 * Created by Venelin on 7/2/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff State - States
 */

app.controller('AdminStaffStateCtrl', ['$scope','AdminStaffProfilesStateService','AdminStaffProfilesCtrlInheritanceService',function ($scope,AdminStaffProfilesStateService,AdminStaffProfilesCtrlInheritanceService) {
      var vm = this;
      // Heading
      vm.heading = [
        // Whether currently active
        {
          label: 'Active',
          data: 'active',
          noSort: true,
          checkbox: true
        },
        // Name
        {
          label: 'State',
          data: 'name',
          noSort: true
        }
      ];

      // Inherit common table functionality
     AdminStaffProfilesCtrlInheritanceService.inherit.call(vm, $scope, 'states', true);
    }]);

