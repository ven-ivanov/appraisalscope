/**
 * Created by Venelin on 6/27/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff Profile - Add State
 */
app.controller('AdminStaffProfileAddStateCtrl',
  ['$scope', 'AdminStaffService','AdminStaffProfilesStateService','AdminStaffCtrlInheritanceService', 'ngProgress',
    function ($scope, AdminStaffService,AdminStaffProfilesStateService,AdminStaffCtrlInheritanceService, ngProgress) {
      //vm point to this isolated directive controller and its scope
      var vm = this;
      // Safe and display data
      vm.safeData = AdminStaffProfilesStateService.safeData;
      vm.displayData = AdminStaffProfilesStateService.displayData;

      // Inherit common controller attributes
      AdminStaffCtrlInheritanceService.createCtrl.call(vm, AdminStaffProfilesStateService, $scope, 'states');

      /**
       * Get initial data on load
       */
      vm.init = function () {
        // Table data populated
        AdminStaffProfilesStateService.getStatesData()
          .then(function() {
            //init states list
            AdminStaffProfilesStateService.initSelectedStates();
            //now attach watcher
            vm.attachWatch();
          });
      };

      /**
          * Update top level dropdowns on backend
       */
      vm.attachWatch = function () {
        $scope.$watch(function () {
          return vm.safeData.states;
        }, function (newVal, oldVal) {
          if (angular.isUndefined(oldVal) || angular.equals(oldVal, newVal)) {
            return;
          }
        }, true);
      };

      /**
       * States
       */
      $scope.$watchCollection(function () {
        return vm.safeData.states;
      }, function (newVal) {
        // If we have an order, then write it
        if (angular.isArray(newVal) && newVal.length) {
          ngProgress.start();
          AdminStaffProfilesStateService.addStates(newVal, 'states').then(function () {
            $scope.$broadcast('hide-modal', 'add-state');
          }).finally(function () {
            ngProgress.complete();
            // Reset array
            $scope.states = [];
          }).catch(function () {
            // Show failure
            $scope.$broadcast('show-modal', 'add-state', true);
          });
        }
      });

      //add state
      vm.addStatesModal = function() {
        $scope.$broadcast('show-modal', 'admin-rule-profile-add-state', true);
      };
      // Initiate data
      vm.init();
    }]);
