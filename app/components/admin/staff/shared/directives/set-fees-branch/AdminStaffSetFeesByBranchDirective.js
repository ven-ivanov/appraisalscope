'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffSetFeesByBranch',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {scope: true,
    bindToController: {
      jobType: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/set-fees-branch/partials/set-fees-branch.html',
    controller:'AdminStaffSetFeesByBranchCtrl',
    controllerAs:'adminStaffSetFeesByBranchCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
