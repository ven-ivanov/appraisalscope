'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff Set Commission
 */
app.controller('AdminStaffSetFeesByBranchCtrl',
  ['$scope','AdminStaffSetFeesByBranchService' , 'AdminStaffService','AdminStaffCommissionCtrlInheritanceService',
    function ($scope,AdminStaffSetFeesByBranchService , AdminStaffService,AdminStaffCommissionCtrlInheritanceService) {
      /**
       * @TODO
       */
      //vm point to this isolated directive controller and its scope
      var vm = this;
      // Safe and display data
      vm.safeData = AdminStaffSetFeesByBranchService.safeData;
      vm.displayData = AdminStaffSetFeesByBranchService.displayData;
      // Inherit common controller attributes
      AdminStaffCommissionCtrlInheritanceService.createCtrl.call(vm,AdminStaffSetFeesByBranchService,$scope,'branches');
      //add commission
      vm.addFeesBranchModal = function() {
        $scope.$broadcast('show-modal', 'set-fees-branch-modal', true);
      };
      /**
       * Get initial data on load
       */
      vm.init = function () {
        // Table data
        AdminStaffSetFeesByBranchService.getBranchesData()
          .then(function() {
            //init job types list
            AdminStaffSetFeesByBranchService.initSelectedBranches();
            //now attach watcher
            vm.attachWatch();
          });
      };

      // Initiate data
      vm.init();
    }]);
