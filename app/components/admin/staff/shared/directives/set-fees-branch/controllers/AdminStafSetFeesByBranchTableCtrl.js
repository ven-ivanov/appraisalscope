'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff set fees by branch
 */
app.controller('AdminStaffSetFeesByBranchTableCtrl',
  ['AdminStaffAssignmentCtrlInheritanceService','AdminStaffSetFeesByBranchService','$scope',
    function (AdminStaffAssignmentCtrlInheritanceService,AdminStaffSetFeesByBranchService, $scope) {
      var vm = this;
      // Heading
      vm.heading = [
        // Whether currently active
        {
          label: '',
          data: 'commission',
          noSort: true,
          checkbox: true
        },
        {
          label: 'S.No',
          data: 'id'
        },
        // Name
        {
          label: 'Branch Name',
          data: 'name',
          noSort: true
        },
        {
          label: 'Set Commission',
          fn: 'setCommission',
          type: 'action',
          linkLabel: 'Set Commission'
        },
        //re-calc commission
        {
          label: 'Re-calculate Commissions',
          fn: 'reCalcCommission',
          type: 'action',
          linkLabel: 'Re-calculate Commissions'
        }
      ];

    //special config for input
      vm.speicalCellsConfig = [
        {
          id: 2,
          type: 'input'
        }
      ];

      /**
       * Link functions
       */
      vm.linkFn = function(id, fnName){
        AdminStaffSetFeesByBranchService.displayData.selectedCompanies.push(id);
        if(fnName === 'setCommission') {
          $scope.$parent.$broadcast('show-modal','set-commission-branch-modal', true);
        } else if(fnName === 'reCalcCommission') {
          $scope.$parent.$broadcast('show-modal','recalc-commission-branch-modal', true);
        }
      };
      // Inherit common table functionality
      AdminStaffAssignmentCtrlInheritanceService.inherit.call(vm, $scope, AdminStaffSetFeesByBranchService,'branches', true);
    }]);

