'use strict';

var app = angular.module('frontendApp');

app.factory('AdminStaffSetFeesByBranchService',
  ['AdminStaffService', 'AdminStaffResourceService', 'AsDataService', function (AdminStaffService, AdminStaffResourceService, AsDataService) {
    var Service = {
      safeData: {
        // Table data
        branches: [],
        jobTypes: [],
        // selected list of branches
        selectedBranches: [],
        selectedJobTypes: []
      },
      displayData: {
        // Table data
        branches: [],
        jobTypes: [],
        //multi select from table ctrl
        multiSelect: {},
        companyId: 0
      },
      /**
       * Request (either company, user, or branch)
       */
      request: function (requestFn, urlParams, bodyParams) {
        // Get resource configuration
        return AdminStaffResourceService.staff[requestFn](urlParams, bodyParams);
      },
      /**
       * Get auto assignment data
       */
      getBranchesData: function () {
        return Service.request('branches',{companyId: Service.displayData.companyId})
          .then(function (response) {
            // Make a simple hash for manipulation
            console.log('---------------------');
            console.log(response);
            Service.hashData(response.data, 'branches');
            Service.formatData();
          });
      },
      /**
       * Sort the approved forms by order
       * @param a
       * @param b
       * @returns {number}
       */
      sortByOrder: function (a, b) {
        return (a.order < b.order) ? -1 : 1;
      },
      /**
       * Initialize job types
       *
       * @todo Waiting on the backend
       */
      initSelectedBranches: function () {
        angular.forEach(Service.safeData.branches, function (item) {
          // Find the approved list
          if (angular.isDefined(item.selectedBranches)) {
            angular.forEach(item.selectedBranches, function (list) {
              Service.safeData.selectedBranches.push(list);
            });
          }
        });
        // Sort by order
        Service.safeData.selectedBranches.sort(Service.sortByOrder);
      },
      /**
       * Write updates to backend
       * @param values
       */
      setCommission: function (values) {
        return Service.request('addBranches', values);
      }
    };

    // Inherit
    Service.hashData = AsDataService.hashData.bind(Service);
    Service.formatData = AsDataService.formatData.bind(Service, 'branches');
    Service.transformData = AsDataService.transformData.bind(Service);
    Service.getRecordDetails =
      AdminStaffService.getRecordDetails.bind(Service, 'branches',
        null);

    Service.checkGroups = AdminStaffService.checkGroups.bind(Service);
    return Service;
  }]);
