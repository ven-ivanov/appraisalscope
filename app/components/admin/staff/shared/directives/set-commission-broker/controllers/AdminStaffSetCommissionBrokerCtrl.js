'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff Set Commission
 */
app.controller('AdminStaffSetCommissionBrokerCtrl',
  ['$scope','AdminStaffSetCommissionBrokerService' , 'AdminStaffService','AdminStaffCommissionCtrlInheritanceService',
    function ($scope,AdminStaffSetCommissionBrokerService , AdminStaffService,AdminStaffCommissionCtrlInheritanceService) {
      /**
       * @TODO
       */
      //vm point to this isolated directive controller and its scope
      var vm = this;
      // Safe and display data
      vm.safeData = AdminStaffSetCommissionBrokerService.safeData;
      vm.displayData = AdminStaffSetCommissionBrokerService.displayData;
      vm.modalTitle = 'set-commission-broker-modal';
      // Inherit common controller attributes - commission ctrl
      AdminStaffCommissionCtrlInheritanceService.createCtrl.call(vm, AdminStaffSetCommissionBrokerService, $scope, 'commission');

      //add Job Type
      vm.addCommissionModal = function() {
        $scope.$broadcast('show-modal',vm.modalTitle, true);
      };
      // Initiate data
      vm.init();
    }]);
