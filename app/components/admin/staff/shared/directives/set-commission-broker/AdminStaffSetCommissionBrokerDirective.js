'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffSetCommissionBroker',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {scope: true,
    bindToController: {
      jobType: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/set-commission-broker/partials/set-commission-broker.html',
    controller:'AdminStaffSetCommissionBrokerCtrl',
    controllerAs:'adminStaffSetCommissionBrokerCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
