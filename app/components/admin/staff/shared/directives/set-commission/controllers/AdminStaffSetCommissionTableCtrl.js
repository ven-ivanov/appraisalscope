'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff set commission for companies and branch
 */
app.controller('AdminStaffSetCommissionTableCtrl',
  ['AdminStaffAssignmentCtrlInheritanceService','AdminStaffSetCommissionService','$scope',
    function (AdminStaffAssignmentCtrlInheritanceService,AdminStaffSetCommissionService, $scope) {
      var vm = this;
      // Heading
      vm.heading = [
        // Whether currently active
        // Name
        {
          label: 'Job Type',
          data: 'label',
          noSort: true
        },
        {
          label: 'Commission Type',
          dropdown: true,
          data: 'commission_type',
          selectOptions: [{value: 'Flat Fee'}, {value: 'Percentage'}]
        },
        {
          label: 'Commission',
          data: 'commission',
          input: true
        }
      ];

    //special config for input
      vm.speicalCellsConfig = [
        {
          id: 2,
          type: 'input'
        }
      ];
      // Inherit common table functionality
      AdminStaffAssignmentCtrlInheritanceService.inherit.call(vm, $scope, AdminStaffSetCommissionService,'jobTypes');
    }]);

