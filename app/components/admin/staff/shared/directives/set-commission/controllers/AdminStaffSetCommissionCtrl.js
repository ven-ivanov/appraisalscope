'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff Set Commission
 */
app.controller('AdminStaffSetCommissionCtrl',
  ['$scope','AdminStaffSetCommissionService' , 'AdminStaffService','AdminStaffCommissionCtrlInheritanceService',
    function ($scope,AdminStaffSetCommissionService , AdminStaffService,AdminStaffCommissionCtrlInheritanceService) {
      /**
       * @TODO
       */
      //vm point to this isolated directive controller and its scope
      var vm = this;
      // Safe and display data
      vm.safeData = AdminStaffSetCommissionService.safeData;
      vm.displayData = AdminStaffSetCommissionService.displayData;
      vm.modalTitle =  'set-commission-company';
      // Inherit common controller attributes
      AdminStaffCommissionCtrlInheritanceService.createCtrl.call(vm, AdminStaffSetCommissionService, $scope, 'jobTypes');

      //add Job Type
      vm.addCommissionModal = function() {
        $scope.$broadcast('show-modal',vm.modalTitle, true);
      };
      // Initiate data
      vm.init();
    }]);
