'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffSetCommission',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {scope: true,
    bindToController: {
      jobType: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/set-commission/partials/set-commission.html',
    controller:'AdminStaffSetCommissionCtrl',
    controllerAs:'adminStaffSetCommissionCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
