'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffRecalcCommission',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {scope: true,
    bindToController: {
      jobType: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/recalc-commission/partials/recalc-commission.html',
    controller:'AdminStaffRecalcCommissionCtrl',
    controllerAs:'adminStaffRecalcCommissionCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
