'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff Set Commission
 */
app.controller('AdminStaffRecalcCommissionCtrl',
  ['$scope','AdminStaffRecalcCommissionService' , 'AdminStaffService','AdminStaffRecalcCommissionsCtrlInheritanceService',
    function ($scope,AdminStaffRecalcCommissionService , AdminStaffService,AdminStaffRecalcCommissionsCtrlInheritanceService) {
      /**
       * @TODO
       */
      //vm point to this isolated directive controller and its scope
      var vm = this;
      // Safe and display data
      vm.safeData = AdminStaffRecalcCommissionService.safeData;
      vm.displayData = AdminStaffRecalcCommissionService.displayData;

      // Inherit common controller attributes
      AdminStaffRecalcCommissionsCtrlInheritanceService.createCtrl.call(vm,$scope,AdminStaffRecalcCommissionService, 'jobTypes');


      //add Job Type
      vm.addRecalcCommissionModal = function() {
        $scope.$broadcast('show-modal', 'recalc-commission-company', true);
      };
      // Initiate data
      vm.init();
    }]);
