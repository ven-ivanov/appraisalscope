'use strict';

var app = angular.module('frontendApp');

app.controller('AdminStaffLoggedInLogCtrl',
  ['$scope', 'AdminStaffLoggedInLogService',function ($scope, AdminStaffLoggedInLogService) {
    var vm = this;
    // Keep reference to display data
    vm.displayData = AdminStaffLoggedInLogService.displayData;
    // Safe data
    vm.safeData = AdminStaffLoggedInLogService.safeData;
    /**
     * Data Load
     */
    vm.init = function() {
      AdminStaffLoggedInLogService.getStaffLoggedInLog();
    };
    //load Data
    vm.init();
  }]);
