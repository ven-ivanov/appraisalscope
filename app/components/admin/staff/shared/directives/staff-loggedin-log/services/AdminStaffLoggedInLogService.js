'use strict';

var app = angular.module('frontendApp');
/**
 * @todo This endpoint doesn't exist yet.
 */
app.factory('AdminStaffLoggedInLogService',
  ['$http', 'API_PREFIX', 'AdminStaffService', '$resource', 'AdminStaffResourceService','AsDataService',
    function ($http, API_PREFIX, AdminStaffService, $resource, AdminStaffResourceService,AsDataService) {
      var Service = {
        safeData:{
          loggedInLogs: []
          // Heading
        },
        displayData:{
          loggedInLogs: []
        },
        /**
         * User AdminStaffResourceService to general resource requests
         */
        request: function (requestType, params) {
          params = params || null;
          // Get $resource configuration
          return AdminStaffResourceService.staff[requestType](params);
        },
        /**
         * Retrieve  staff logged in logs
         */
        getStaffLoggedInLog: function () {
          // Make request
          Service.request('getStaffLoggedinLog')
            .then(function (response) {
              //hash data
              Service.hashData(response.data, 'loggedInLogs');
              Service.formatData();
            });
        }
      };
      // Inherit
      Service.loadRecords = AdminStaffService.loadRecords.bind(Service, 'loggedInLogs');
      Service.hashData = AsDataService.hashData.bind(Service);
      Service.formatData = AsDataService.formatData.bind(Service,'loggedInLogs');
      Service.transformData = AsDataService.transformData.bind(Service);
      return Service;
      }]);

