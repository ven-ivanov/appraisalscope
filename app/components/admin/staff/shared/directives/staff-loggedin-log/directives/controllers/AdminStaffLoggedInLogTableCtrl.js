'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff - Logged In Log
 */
app.controller('AdminStaffLoggedInLogTableCtrl',
  ['$scope', 'AdminStaffLoggedInLogService', 'AdminStaffProfilesCtrlInheritanceService',
    function ($scope, AdminStaffLoggedInLogService, AdminStaffProfilesCtrlInheritanceService) {
      var vm = this;

      vm.heading = [{
        label: 'Logged Time',
          data: 'logged_time',
          noSort: true
      }];

      // Inherit common table functionality
      AdminStaffProfilesCtrlInheritanceService.inherit.call(vm, $scope, 'loggedInLogs', false);
    }]);
