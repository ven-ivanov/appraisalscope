'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users client sub-tab
 */
app.directive('adminStaffLoggedInLog', ['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/staff/shared/directives/staff-loggedin-log/directives/partials/staff-log.html',
    controller: 'AdminStaffLoggedInLogCtrl',
    controllerAs: 'adminStaffLoggedInLogCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);


