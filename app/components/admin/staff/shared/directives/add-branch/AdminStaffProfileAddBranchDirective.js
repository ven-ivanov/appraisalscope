'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffProfileAddBranch',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {scope: true,
    bindToController: {
      branch: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/add-branch/partials/branch.html',
    controller:'AdminStaffProfileAddBranchCtrl',
    controllerAs:'adminStaffProfileAddBranchCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
