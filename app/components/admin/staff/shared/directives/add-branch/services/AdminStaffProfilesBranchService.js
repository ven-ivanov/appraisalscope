'use strict';

var app = angular.module('frontendApp');

app.factory('AdminStaffProfilesBranchService',
  ['AdminStaffService', 'AdminStaffResourceService', 'AsDataService', function (AdminStaffService, AdminStaffResourceService, AsDataService) {
    var Service = {
      safeData: {
        // Table data
        branch: {},
        // selected list of branch
        selectedBranch: []
      },
      displayData: {
        // Table data
        branch: []
      },
      /**
       * Request (either company, user, or branch)
       */
      request: function (requestFn, urlParams, bodyParams) {
        var type='adminRuleProfile';
        // Get resource configuration
        if(type === 'adminRuleProfile') {
          return AdminStaffResourceService.adminRuleProfile[requestFn](urlParams, bodyParams);
        }else if (type === 'reviewerRuleProfile'){
          return AdminStaffResourceService.reviewerRuleProfile[requestFn](urlParams, bodyParams);
        }
      },
      /**
       * Get auto assignment data
       */
      getBranchData: function () {
        return Service.request('branches').then(function (response) {
          // Make a simple hash for manipulation
          Service.hashData(response.data, 'branch');
          Service.formatData();
       });
       },
      /**
       * Sort the approved forms by order
       * @param a
       * @param b
        }
        return 0;
      },
      /**
       * Initialize branches
       *
       */
      initSelectedBranches: function () {
        angular.forEach(Service.safeData.branch, function (item) {
          // Find the approved list
          if (angular.isDefined(item.selectedBranch)) {
            angular.forEach(item.selectedBranch, function (list) {
              Service.safeData.selectedBranch.push(list);
            });
          }
        });
        // Sort by order
        Service.safeData.selectedBranch.sort(Service.sortByOrder);
      },
      /**
       * Write updates to backend
       * @param values
       */
      addBranch: function (values) {
        return Service.request('addBranch', values);
      }
    };

    // Inherit
    Service.hashData = AsDataService.hashData.bind(Service);
    Service.formatData = AsDataService.formatData.bind(Service, 'branch');
    Service.transformData = AsDataService.transformData.bind(Service);
    Service.getRecordDetails =
      AdminStaffService.getRecordDetails.bind(Service, 'branch',
        null);
    Service.checkGroups = AdminStaffService.checkGroups.bind(Service);
    return Service;
  }]);
