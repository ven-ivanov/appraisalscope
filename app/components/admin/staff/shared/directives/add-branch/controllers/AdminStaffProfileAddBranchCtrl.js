'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff Profile - Add branch
 */
app.controller('AdminStaffProfileAddBranchCtrl',
  ['$scope','AdminStaffProfilesBranchService' , 'AdminStaffService','AdminStaffCtrlInheritanceService',
    function ($scope,AdminStaffProfilesBranchService , AdminStaffService,AdminStaffCtrlInheritanceService) {
      /**
       * @
       * Need to be completed with Mock APIsTODO
       */
      //vm point to this isolated directive controller and its scope
      var vm = this;
      // Safe and display data
      vm.safeData = AdminStaffProfilesBranchService.safeData;
      vm.displayData = AdminStaffProfilesBranchService.displayData;

      // Inherit common controller attributes
      AdminStaffCtrlInheritanceService.createCtrl.call(vm, AdminStaffProfilesBranchService, $scope, 'branch');
      /**
       * Get initial data on load
       */
      vm.init = function () {
        // Table data
        AdminStaffProfilesBranchService.getBranchData()
          .then(function () {
            //init job types list
            AdminStaffProfilesBranchService.initSelectedBranches();
            //now attach watcher
            vm.attachWatch();
          });
      };

      /**
       * Update top level dropdowns on backend
       */
      vm.attachWatch = function () {
        $scope.$watch(function () {
          return vm.safeData.branch;
        }, function (newVal, oldVal) {
          if (angular.isUndefined(oldVal) || angular.equals(oldVal, newVal)) {
            return;
          }
        }, true);
      };

      /**
       * Branch
       */
      $scope.$watchCollection(function () {
        return vm.branch;
      }, function (newVal) {
        // If we have an order, then write it
        if (angular.isArray(newVal) && newVal.length) {
          AdminStaffProfilesBranchService.addBranch(newVal, 'branch').then(function () {
            $scope.$broadcast('hide-modal', 'add-branch');
          }).finally(function () {
            // Reset array
            $scope.branch = [];
          }).catch(function () {
            // Show failure
            $scope.$broadcast('show-modal', 'add-branch', true);
          });
        }
      });

      //add branch
      vm.addBranchModal = function() {
        $scope.$broadcast('show-modal', 'admin-rule-profile-add-branch', true);
      };
      // Initiate data
      vm.init();
    }]);
