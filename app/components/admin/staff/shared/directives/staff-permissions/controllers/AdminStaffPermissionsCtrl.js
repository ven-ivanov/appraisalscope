'use strict';

var app = angular.module('frontendApp');

app.controller('AdminStaffPermissionsCtrl',
  ['$scope', 'AdminStaffPermissionsService', 'ngProgress', function ($scope, AdminStaffPermissionsService, ngProgress) {
    var vm = this;
    // Keep reference to display data
    vm.displayData = AdminStaffPermissionsService.displayData;
    // Safe data
    vm.safeData = AdminStaffPermissionsService.safeData;

    /**
     * Get data on load
     */
    vm.init = function () {
      //init default permissions , email permissions, tabs permissions...
      // Table data populated
      AdminStaffPermissionsService.getStaffPermission();
     };

    /**
     * Edit staff permission
     */
    vm.editPermission = function () {
      ngProgress.start();
      AdminStaffPermissionsService.editPermission()
        .then(function () {
          $scope.$broadcast('hide-modal', 'admin-staff-edit-permission');
        })
        .finally(function () {
          ngProgress.complete();
        })
        .catch(function () {
          //failure modal should exist
          $scope.$broadcast('show-modal', 'admin-staff-edit-permission-failure', true);
        });
    };

    // Init data
    vm.init();
  }]);
