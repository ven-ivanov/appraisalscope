'use strict';

var app = angular.module('frontendApp');

app.controller('AdminStaffTabsPermissionsCtrl',
  ['$scope', 'AdminStaffTabsPermissionsService', 'ngProgress', function ($scope, AdminStaffTabsPermissionsService, ngProgress) {
    var vm = this;
    // Keep reference to display data
    vm.displayData = AdminStaffTabsPermissionsService.displayData;
    // Safe data
    vm.safeData = AdminStaffTabsPermissionsService.safeData;

    // Construct the heading based on the non-hidden items
    //vm.heading = AdminStaffPermissionsService.getDirectiveTableColumns($scope.hide, $scope.show);

    // Keep reference on service
    //AdminStaffPermissionsService.safeData.heading = vm.heading;

    /**
     * Get data on load
     */
    vm.init = function () {
      //init default permissions , email permissions, tabs permissions...
      // Table data populated
      AdminStaffTabsPermissionsService.getStaffTabsPermission();
     };

    /**
     * Edit tabs permission
     */
    vm.editTabsPermission = function () {
      ngProgress.start();
      AdminStaffTabsPermissionsService.editTabsPermission()
        .then(function () {
          $scope.$broadcast('hide-modal', 'admin-staff-edit-tabs-permission');
        })
        .finally(function () {
          ngProgress.complete();
        })
        .catch(function () {
          //failure modal should exist
          $scope.$broadcast('show-modal', 'admin-staff-edit-tabs-permission-failure', true);
        });
    };
    // Init data
    vm.init();
  }]);
