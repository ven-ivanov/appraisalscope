'use strict';

var app = angular.module('frontendApp');

app.controller('AdminStaffEmailPermissionsCtrl',
  ['$scope', 'AdminStaffEmailPermissionsService', 'ngProgress', function ($scope, AdminStaffEmailPermissionsService, ngProgress) {
    var vm = this;
    // Keep reference to display data
    vm.displayData = AdminStaffEmailPermissionsService.displayData;
    // Safe data
    vm.safeData = AdminStaffEmailPermissionsService.safeData;

    /**
     * Get data on load
     */
    vm.init = function () {
      //init default email permissions...
      // Table data populated
      AdminStaffEmailPermissionsService.getStaffEmailPermission();
     };

     /**
     * Edit email permission
     */
    vm.editEmailPermission = function () {
      ngProgress.start();
      AdminStaffEmailPermissionsService.editEmailPermission()
        .then(function () {
          $scope.$broadcast('hide-modal', 'admin-staff-edit-email-permission');
        })
        .finally(function () {
          ngProgress.complete();
        })
        .catch(function () {
          //failure modal should exist
          $scope.$broadcast('show-modal', 'admin-staff-edit-email-permission-failure', true);
        });
    };

    // Init data
    vm.init();
  }]);
