'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users client sub-tab
 */
app.directive('adminStaffEmailPermission', ['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/staff/shared/directives/staff-permissions/directives/partials/email-permissions.html',
    controller: 'AdminStaffEmailPermissionsCtrl',
    controllerAs: 'adminStaffEmailPermissionsCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);

