'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users client sub-tab
 */
app.directive('adminStaffTabsPermission', ['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/staff/shared/directives/staff-permissions/directives/partials/tabs-permissions.html',
    controller: 'AdminStaffTabsPermissionsCtrl',
    controllerAs: 'adminStaffTabsPermissionsCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);

