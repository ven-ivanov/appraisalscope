'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff - Email Permissions
 */
app.controller('AdminStaffEmailPermissionsTableCtrl',
  ['$scope', 'AdminStaffEmailPermissionsService' , 'AdminStaffPermissionsCtrlInheritanceService',
    function ($scope, AdminStaffEmailPermissionsService,  AdminStaffPermissionsCtrlInheritanceService) {
      var vm = this;
      // Heading
      vm.heading = [// Name
        {
          label: 'Name',
          data: 'name',
          noSort: true
        }, // Whether currently active
        {
          label: 'Active',
          data: 'active',
          noSort: true,
          checkbox: true
        }];

      // Inherit common table functionality
      AdminStaffPermissionsCtrlInheritanceService.inherit.call(vm, $scope, AdminStaffEmailPermissionsService,'emailPermissions');
    }]);
