'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Users client sub-tab
 */
app.directive('adminStaffPermission', ['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/staff/shared/directives/staff-permissions/directives/partials/staff-permissions.html',
    controller: 'AdminStaffPermissionsCtrl',
    controllerAs: 'adminStaffPermissionsCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);

