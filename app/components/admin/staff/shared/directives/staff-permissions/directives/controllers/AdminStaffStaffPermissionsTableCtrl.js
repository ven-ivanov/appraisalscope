'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff - Email Permissions
 */
app.controller('AdminStaffStaffPermissionsTableCtrl',
  ['$scope', 'AdminStaffPermissionsService', 'AdminStaffPermissionsCtrlInheritanceService',
    function ($scope, AdminStaffPermissionsService, AdminStaffPermissionsCtrlInheritanceService) {
      var vm = this;
      // Heading
      vm.heading = [// Name
        {
          label: 'Name',
          data: 'name',
          noSort: true
        }, // Whether currently active
        {
          label: 'Active',
          data: 'active',
          noSort: true,
          checkbox: true
        }];

      // Inherit common table functionality
      AdminStaffPermissionsCtrlInheritanceService.inherit.call(vm, $scope,AdminStaffPermissionsService, 'permissions', true);
    }]);
