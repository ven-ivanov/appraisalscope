'use strict';

var app = angular.module('frontendApp');

/**
 * @todo This endpoint doesn't exist yet.
 *
 * @link https://github.com/ascope/manuals/issues/41
 */
app.factory('AdminStaffPermissionsService',
  ['$http', 'API_PREFIX', 'AdminStaffService', '$resource', 'AdminStaffResourceService','AsDataService',
    function ($http, API_PREFIX, AdminStaffService, $resource, AdminStaffResourceService,AsDataService) {
      var Service = {
        safeData: {
          permissions: [],
          selectedPermissions: []
        },
        displayData: {
          permissions: []
        },
        /**
         * User AdminStaffResourceService to general resource requests
         * @param requestType String Function to call in resource service
         * @param params String Argument passed into resource service
         */
        request: function (requestType, params) {
          params = params || null;
          // Get $resource configuration
          return AdminStaffResourceService.permissions[requestType](params);
        },
        /**
         * Retrieve  permission for the current staff
         */
        getStaffPermission: function () {
          // Make request
          Service.request('staff').then(function(response){
            //hash data
            Service.hashData(response.data, 'permissions');
            Service.formatData();
            Service.initSelectedPermissions();
          });
        },
        /**
         * Initialize selected permissions
         */
        initSelectedPermissions: function () {
          angular.forEach(Service.safeData.permissions, function (item) {
            // Find the permissions
            if (angular.isDefined(item.selectedPermissions)) {
              angular.forEach(item.selectedPermissions, function (list) {
                Service.safeData.selectedPermissions.push(list);
              });
            }
          });
          // Sort by order
          Service.safeData.selectedPermissions.sort(Service.sortByOrder);
        },
        /**
         * Update staff permissions @backend
         */
        updateStaffPermissions: function (type, update) {
          // Update permissions
          var permissions = new (Service.permissionsRequest())(update);
          permissions.permissionType = type + '-permissions';
          return permissions.$updatePermissions();
        }
      };
      // Inherit
      Service.hashData = AsDataService.hashData.bind(Service);
      Service.formatData = AsDataService.formatData.bind(Service,'permissions');
      Service.transformData = AsDataService.transformData.bind(Service);

      Service.toggleSetting = AdminStaffService.toggleSetting.bind(Service, 'permissions');
      Service.checkGroups = AdminStaffService.checkGroups.bind(Service);
      return Service;
    }]);
