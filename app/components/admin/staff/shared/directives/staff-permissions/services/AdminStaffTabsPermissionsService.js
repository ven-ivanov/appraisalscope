'use strict';

var app = angular.module('frontendApp');

/**
 * @todo This endpoint doesn't exist yet.
 *
 */
app.factory('AdminStaffTabsPermissionsService',
  ['$http', 'API_PREFIX', 'AdminStaffService', '$resource', 'AdminStaffResourceService','AsDataService',
    function ($http, API_PREFIX, AdminStaffService, $resource, AdminStaffResourceService,AsDataService) {
      var Service = {
        safeData: {
          tabsPermissions: [],
          selectedTabsPermissions: []
        },
        displayData: {
          tabsPermissions: []
        },
        /**
         * User AdminStaffResourceService to general resource requests
         * @param requestType String Function to call in resource service
         * @param params String Argument passed into resource service
         */
        request: function (requestType, params) {
          params = params || null;
          // Get $resource configuration
          return AdminStaffResourceService.permissions[requestType](params);
        },
        /**
         * Retrieve tabs permission for the current staff
         */
        getStaffTabsPermission: function () {
          // Make request
          Service.request('tabs').then(function(response){
            //hash data
            Service.hashData(response.data, 'tabsPermissions');
            Service.formatData();
            Service.initSelectedTabsPermissions();
          });
        },
        /**
         * Initialize selected permissions
         */
        initSelectedTabsPermissions: function () {
          angular.forEach(Service.safeData.tabsPermissions, function (item) {
            // Find the permissions
            if (angular.isDefined(item.selectedTabsPermissions)) {
              angular.forEach(item.selectedTabsPermissions, function (list) {
                Service.safeData.selectedTabsPermissions.push(list);
              });
            }
          });
          // Sort by order
          Service.safeData.selectedTabsPermissions.sort(Service.sortByOrder);
        }
      };
      // Inherit
      Service.hashData = AsDataService.hashData.bind(Service);
      Service.formatData = AsDataService.formatData.bind(Service,'tabsPermissions');
      Service.transformData = AsDataService.transformData.bind(Service);

      Service.toggleSetting = AdminStaffService.toggleSetting.bind(Service, 'tabsPermissions');
      Service.checkGroups = AdminStaffService.checkGroups.bind(Service);
      return Service;
    }]);
