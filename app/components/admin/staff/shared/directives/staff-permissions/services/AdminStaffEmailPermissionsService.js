'use strict';

var app = angular.module('frontendApp');

/**
 * @todo This endpoint doesn't exist yet.
 */
app.factory('AdminStaffEmailPermissionsService',
  ['$http', 'API_PREFIX', 'AdminStaffService', '$resource', 'AdminStaffResourceService','AsDataService',
    function ($http, API_PREFIX, AdminStaffService, $resource, AdminStaffResourceService,AsDataService) {
      var Service = {
        safeData: {
          emailPermissions: [],
          selectedEmailPermissions: []
        },
        displayData: {
          emailPermissions: []
        },
        /**
         * User AdminStaffResourceService to general resource requests
         * @param requestType String Function to call in resource service
         * @param params String Argument passed into resource service
         */
        request: function (requestType, params) {
          params = params || null;
          // Get $resource configuration
          return AdminStaffResourceService.permissions[requestType](params);
        },
        /**
         * Retrieve email permission for the current staff
         */
        getStaffEmailPermission: function () {
          // Make request
          Service.request('email')
            .then(function (response) {
              //hash data
              Service.hashData(response.data, 'emailPermissions');
              Service.formatData();
              Service.initSelectedEmailPermissions();
            });
        },
        /**
         * Initialize selected permissions
         */
        initSelectedEmailPermissions: function () {
          angular.forEach(Service.safeData.emailPermissions, function (item) {
            // Find the permissions
            if (angular.isDefined(item.selectedEmailPermissions)) {
              angular.forEach(item.selectedEmailPermissions, function (list) {
                Service.safeData.selectedEmailPermissions.push(list);
              });
            }
          });
          // Sort by order
          Service.safeData.selectedEmailPermissions.sort(Service.sortByOrder);
        },
        /**
         * Check the checkboxes for the records which are already active
         */
        checkGroups: function (data) {
          var result = {};
          angular.forEach(data, function (val) {
            if (val.active) {
              result[val.id] = true;
            }
          });
          return result;
        },
        /**
         * Set mail settings for the selected staff
         * @param id Record ID
         * @param type is staff
         */
        setMailSettings: function (id, type) {
          var safeData = Service.safeData[type + 'Mail'];
          safeData.selections = [];
          // Reset if no user is selected
          if (id === 0) {
            return;
          }
          // Get the selected user's permissions
          angular.forEach(safeData[type + 's'], function (user) {
            if (user.id === id) {
              angular.forEach(user.data, function (permission, type) {
                if (permission) {
                  safeData.selections.push(type);
                }
              });
            }
          });
        },
        /**
         * Set a single permissions on click
         */
        setSingleMailPermission: function (permission, type) {
          var recordId = Service.safeData[type].selectedUser;
          // Add or remove the selected permission item
          if (Service.safeData[type].selections.indexOf(permission) === -1) {
            Service.safeData[type].selections.push(permission);
          } else {
            Service.safeData[type].selections.splice(Service.safeData[type].selections.indexOf(permission), 1);
          }
          // Write to DB
          return Service.updatePermissions(type + '-mail', {selections: Service.safeData[type].selections, recordId: recordId});
        }
      };
      // Inherit
      Service.hashData = AsDataService.hashData.bind(Service);
      Service.formatData = AsDataService.formatData.bind(Service,'emailPermissions');
      Service.transformData = AsDataService.transformData.bind(Service);

      Service.toggleSetting = AdminStaffService.toggleSetting.bind(Service, 'emailPermissions');
      Service.checkGroups = AdminStaffService.checkGroups.bind(Service);
      return Service;
    }]);
