'use strict';

var app = angular.module('frontendApp');

/**
 * Inherit common methods for staff permissions tables
 */
app.factory('AdminStaffPermissionsCtrlInheritanceService',
  ['AdminStaffPermissionsService', function (AdminStaffPermissionsService) {
    return {
      inherit: function ($scope,Service, type, multiSelect) {
        var vm = this;

        // Safe data
        vm.safeData = Service.safeData;
        /**
         * Watch user documents and display table when we have the documents
         */
        $scope.$watchCollection(function () {
          return Service.displayData[type];
        }, function (newVal) {
          if (!angular.isArray(newVal)) {
            return;
          }
          // Set table data
          vm.tableData = newVal;
          vm.rowData = newVal.slice();
          if (multiSelect) {
            // Check the boxes which are currently active
            vm.multiSelect = Service.checkGroups(newVal);
          }
        });

        // If multiselect is being used in the table, watch those values
        if (multiSelect) {
          /**
           * Watch which records are checked, and update the backend
           */
          $scope.$watchCollection(function () {
            return vm.multiSelect;
          }, function (newVal, oldVal) {
            // Don't write on load
            if (angular.isUndefined(newVal) || angular.isUndefined(oldVal)) {
              return;
            }
            // Write to the backend
          /*  Service.updatePermissions(type, newVal)
              .catch(function () {
                $scope.$emit('show-modal', 'general-update-failure', true);
              });*/
          });
        }
        /**
         * Show record details
         */
        vm.rowFn = function (id) {
          // Detach watcher
          if (angular.isFunction(AdminStaffPermissionsService.detachUpdateWatcher)) {
            AdminStaffPermissionsService.detachUpdateWatcher();
          }
          // Get details
          Service.getRecordDetails(id).then(function () {
            // Attach update watcher once the record is retrieved
            AdminStaffPermissionsService.attachUpdateWatcher.call(vm, $scope, Service);
          });
        };

      }
    };
  }]);
