/**
 * Created by Venelin on 7/3/2015.
 */
'use strict';

var app = angular.module('frontendApp');

app.factory('AdminStaffProfilesPaidStatusService',
  ['AdminStaffService', 'AdminStaffResourceService', 'AsDataService', function (AdminStaffService, AdminStaffResourceService, AsDataService) {
    var Service = {
      safeData: {
        // Table data
        paidStatus: {},
        // selected list of paid status
        selectedPaidStatus: []
      },
      displayData: {
        // Table data
        paidStatus: [],
        //multi select from table ctrl
        multiSelect: {}
      },
      /**
       * Request (company)
       */
      request: function (requestFn, urlParams, bodyParams) {
        // Get resource configuration
        return AdminStaffResourceService.paidStatus[requestFn](urlParams, bodyParams);
      },
      /**
       * Get auto assignment data
       */
      getPaidStatusData: function () {
        return Service.request('init').then(function (response) {
          // Make a simple hash for manipulation
          Service.hashData(response.data, 'paidStatus');
          Service.formatData();
        });
      },
      /**
       * Sort the approved forms by order
       * @param a
       * @param b
       * @returns {number}
       */
      sortByOrder: function (a, b) {
        if (a.order < b.order) {
          return -1;
        }
        if (a.order > b.order) {
          return 1;
        }
        return 0;
      },
      /**
       * Initialize selected paid status
       *
       * @todo Waiting on the backend
       */
      initPaidStatus: function () {
        angular.forEach(Service.safeData.paidStatus, function (item) {
          // Find the approved list
          if (angular.isDefined(item.selectedPaidStatus)) {
            angular.forEach(item.selectedPaidStatus, function (list) {
              Service.safeData.selectedPaidStatus.push(list);
            });
          }
        });
        // Sort by order
        Service.safeData.selectedPaidStatus.sort(Service.sortByOrder);
      },
      /**
       * Write updates to backend
       * @param values
       */
      addPaidStatus: function (values) {
        return Service.request('addPaidStatus', values);
      }
    };

    // Inherit
    Service.hashData = AsDataService.hashData.bind(Service);
    Service.formatData = AsDataService.formatData.bind(Service, 'paidStatus');
    Service.transformData = AsDataService.transformData.bind(Service);

    Service.getRecordDetails =
      AdminStaffService.getRecordDetails.bind(Service, 'paidStatus',
        null);

    Service.checkGroups = AdminStaffService.checkGroups.bind(Service);
    return Service;
  }]);

