/**
 * Created by Venelin on 6/26/2015.
 */
'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffProfileAddPaidStatus',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    scope: true,
    bindToController: {
      paidStatus: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/add-paidstatus/partials/add-paid-status.html',
    controller:'AdminStaffProfileAddPaidStatusCtrl',
    controllerAs:'adminStaffProfileAddPaidStatusCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);

