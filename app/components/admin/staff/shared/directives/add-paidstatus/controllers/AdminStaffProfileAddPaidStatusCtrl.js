/**
 * Created by Venelin on 6/27/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff Profile - Add Paid Status
 */
app.controller('AdminStaffProfileAddPaidStatusCtrl',
  ['$scope', 'AdminStaffProfilesPaidStatusService','AdminStaffCtrlInheritanceService', 'AdminStaffService', 'ngProgress',
    function ($scope,AdminStaffProfilesPaidStatusService,AdminStaffCtrlInheritanceService, AdminStaffService, ngProgress) {
      //vm point to this isolated directive controller and its scope
      var vm = this;
      // Safe and display data
      var Service = vm.service;
      vm.safeData = AdminStaffProfilesPaidStatusService.safeData;
      vm.displayData = AdminStaffProfilesPaidStatusService.displayData;

      // Inherit common controller attributes
      AdminStaffCtrlInheritanceService.createCtrl.call(vm, AdminStaffProfilesPaidStatusService, $scope, 'paidStatus');
      /**
       * Get initial data on load
       */
      vm.init = function () {
        // Table data populated
        AdminStaffProfilesPaidStatusService.getPaidStatusData()
          .then(function() {
            //init states list
            AdminStaffProfilesPaidStatusService.initPaidStatus();
            //now attach watcher
            vm.attachWatch();
          });
      };

      /**
       * Update top level dropdowns on backend
       */
      vm.attachWatch = function () {
        $scope.$watch(function () {
          return vm.safeData.paidStatus;
        }, function (newVal, oldVal) {
          if (angular.isUndefined(oldVal) || angular.equals(oldVal, newVal)) {
            return;
          }
        }, true);
      };

      /**
       * Watch paid status
       */
      $scope.$watchCollection(function () {
        return vm.paidStatus;
      }, function (newVal) {
        // If we have an order, then write it
        if (angular.isArray(newVal) && newVal.length) {
          ngProgress.start();
          Service.addPaidStatus(newVal, 'paidStatus').then(function () {
            $scope.$broadcast('hide-modal', 'add-paid-status');
          }).finally(function () {
            ngProgress.complete();
            // Reset array
            $scope.paidStatus = [];
          }).catch(function () {
            // Show failure
            $scope.$broadcast('show-modal', 'add-paid-status', true);
          });
        }
      });
      //add paid status
      vm.addPaidStatusModal = function() {
        $scope.$broadcast('show-modal', 'admin-rule-profile-add-paid-status', true);
      };
      // Initiate data
      vm.init();
    }]);
