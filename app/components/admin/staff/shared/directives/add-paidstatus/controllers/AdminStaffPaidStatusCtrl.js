/**
 * Created by Venelin on 7/2/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff Companies - Companies - Groups
 */

app.controller('AdminStaffPaidStatusCtrl', ['AdminStaffProfilesCtrlInheritanceService','$scope',function (AdminStaffProfilesCtrlInheritanceService, $scope) {
      var vm = this;
      // Heading
      vm.heading = [
        // Whether currently active
        {
          label: 'Active',
          data: 'active',
          noSort: true,
          checkbox: true
        },
        // Name
        {
          label: 'Status',
          data: 'label',
          noSort: true
        }
      ];

      // Inherit common table functionality
     AdminStaffProfilesCtrlInheritanceService.inherit.call(vm, $scope, 'paidStatus', true);
    }]);
