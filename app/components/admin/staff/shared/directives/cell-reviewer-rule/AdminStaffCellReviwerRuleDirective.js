'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffCellReviewerRule',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {
    scope: true,
    bindToController: {
      company: '=',
      service: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/cell-reviewer-rule/partials/partials.html',
    controller:'AdminStaffCellReviewerRuleCtrl',
    controllerAs:'adminStaffCellReviewerRuleCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
