/**
 * Created by Venelin on 7/3/2015.
 */
'use strict';

var app = angular.module('frontendApp');

app.factory('AdminStaffProfilesJobTypeService',
  ['AdminStaffService', 'AdminStaffResourceService', 'AsDataService', function (AdminStaffService, AdminStaffResourceService, AsDataService) {
    var Service = {
      safeData: {
        // Table data
        jobTypes: {},
        // selected list of jobTypes
        selectedJobTypes: []
      },
      displayData: {
        // Table data
        jobTypes: [],
        //multi select from table ctrl
        multiSelect: {}
      },
      /**
       * Request (either company, user, or branch)
       */
      request: function (requestFn, urlParams, bodyParams) {
        // Get resource configuration
        return AdminStaffResourceService.jobTypes[requestFn](urlParams, bodyParams);
      },
      /**
       * Get auto assignment data
       */
      getJobTypesData: function () {
        return Service.request('init').then(function (response) {
          // Make a simple hash for manipulation
          Service.hashData(response, 'jobTypes');
          Service.formatData();
        });
      },
      /**
       * Sort the approved forms by order
       * @param a
       * @param b
        }
        return 0;
      },
      /**
       * Initialize job types
       *
       * @todo Waiting on the backend
       */
      initSelectedJobTypes: function () {
        angular.forEach(Service.safeData.jobTypes, function (item) {
          // Find the approved list
          if (angular.isDefined(item.selectedJobTypes)) {
            angular.forEach(item.selectedJobTypes, function (list) {
              Service.safeData.selectedJobTypes.push(list);
            });
          }
        });
        // Sort by order
        Service.safeData.selectedJobTypes.sort(Service.sortByOrder);
      },
      /**
       * Write updates to backend
       * @param values
       */
      addJobTypes: function (values) {
        return Service.request('addJobTypes', values);
      }
    };

    // Inherit
    Service.hashData = AsDataService.hashData.bind(Service);
    Service.formatData = AsDataService.formatData.bind(Service, 'jobTypes');
    Service.transformData = AsDataService.transformData.bind(Service);
    Service.getRecordDetails =
      AdminStaffService.getRecordDetails.bind(Service, 'jobTypes',
        null);

    Service.checkGroups = AdminStaffService.checkGroups.bind(Service);
    return Service;
  }]);
