/**
 * Created by Venelin on 6/27/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Admin Staff Profile - Add Job Type
 */
app.controller('AdminStaffProfileAddJobTypeCtrl',
  ['$scope','AdminStaffProfilesJobTypeService' , 'AdminStaffService','AdminStaffCtrlInheritanceService', 'ngProgress',
    function ($scope,AdminStaffProfilesJobTypeService , AdminStaffService,AdminStaffCtrlInheritanceService, ngProgress) {
      /**
       * @TODO
       */
      //vm point to this isolated directive controller and its scope
      var vm = this;
      // Safe and display data
      vm.safeData = AdminStaffProfilesJobTypeService.safeData;
      vm.displayData = AdminStaffProfilesJobTypeService.displayData;

      // Inherit common controller attributes
      AdminStaffCtrlInheritanceService.createCtrl.call(vm, AdminStaffProfilesJobTypeService, $scope, 'jobType');
      /**
       * Get initial data on load
       */
      vm.init = function () {
        // Table data
        AdminStaffProfilesJobTypeService.getJobTypesData()
          .then(function() {
            //init job types list
            AdminStaffProfilesJobTypeService.initSelectedJobTypes();
            //now attach watcher
            vm.attachWatch();
          });
      };

      /**
       * Update top level dropdowns on backend
       */
      vm.attachWatch = function () {
        $scope.$watch(function () {
          return vm.safeData.jobType;
        }, function (newVal, oldVal) {
          if (angular.isUndefined(oldVal) || angular.equals(oldVal, newVal)) {
            return;
          }
        }, true);
        };

        /**
       * Job Type
       */
      $scope.$watchCollection(function () {
        return vm.jobType;
      }, function (newVal) {
        // If we have an order, then write it
        if (angular.isArray(newVal) && newVal.length) {
          ngProgress.start();
          AdminStaffProfilesJobTypeService.addJobType(newVal, 'jobType').then(function () {
            $scope.$broadcast('hide-modal', 'add-job-type');
          }).finally(function () {
            ngProgress.complete();
            // Reset array
            $scope.jobType = [];
          }).catch(function () {
            // Show failure
            $scope.$broadcast('show-modal', 'add-job-type', true);
          });
        }
      });

      //add Job Type
      vm.addJobTypeModal = function() {
        $scope.$broadcast('show-modal', 'admin-rule-profile-add-job-type', true);
      };
      // Initiate data
      vm.init();
    }]);
