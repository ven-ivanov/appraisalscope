/**
 * Created by Venelin on 6/26/2015.
 */
'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffProfileAddJobType',['DirectiveInheritanceService', function (DirectiveInheritanceService) {
  return {scope: true,
    bindToController: {
      jobType: '='
    },
    templateUrl: '/components/admin/staff/shared/directives/add-jobtype/partials/add-job-type.html',
    controller:'AdminStaffProfileAddJobTypeCtrl',
    controllerAs:'adminStaffProfileAddJobTypeCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
