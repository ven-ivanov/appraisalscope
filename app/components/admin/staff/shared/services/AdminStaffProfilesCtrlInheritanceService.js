/**
 * Created by Venelin on 7/2/2015.
 */

'use strict';

var app = angular.module('frontendApp');

/**
 * Inherit common methods for user permissions tables
 */
app.factory('AdminStaffProfilesCtrlInheritanceService', ['AdminStaffProfilesStateService','AdminStaffProfilesCompanyService',
  'AdminStaffProfilesJobTypeService','AdminStaffProfilesPaidStatusService','AdminStaffProfilesProcessStatusService','AdminStaffLoggedInLogService',
  function (AdminStaffProfilesStateService,AdminStaffProfilesCompanyService,
  AdminStaffProfilesJobTypeService,AdminStaffProfilesPaidStatusService,AdminStaffProfilesProcessStatusService,AdminStaffLoggedInLogService) {
    return {
      inherit: function ($scope, type, multiSelect) {
        var vm = this;
        /**
         * Watch user documents and display table when we have the documents
           */
        /**
         * @Todo Watch Collection
         */
          var services = {
          'companies': AdminStaffProfilesCompanyService,
          'states': AdminStaffProfilesStateService,
          'paidStatus': AdminStaffProfilesPaidStatusService,
          'processStatus': AdminStaffProfilesProcessStatusService,
          'jobTypes': AdminStaffProfilesJobTypeService,
          'loggedInLogs':AdminStaffLoggedInLogService
        };
        var Service = services[type] || AdminStaffProfilesStateService;

        //watch collection to update table data --important
        $scope.$watchCollection(function () {
          return Service.displayData[type];
        }, function (newVal) {
          if (!angular.isArray(newVal)) {
            return;
          }
          // Set table data
          vm.tableData = newVal;
          vm.rowData = newVal.slice();

          if (multiSelect) {
            // Check the boxes which are currently active
            vm.multiSelect = Service.checkGroups(newVal);
          }
        });

        // If multi select is being used in the table, watch those values
        if (multiSelect) {
          /**
           * Watch which records are checked, and update the backend
           */
          $scope.$watchCollection(function () {
            return vm.multiSelect;
          }, function (newVal, oldVal) {
            // Don't write on load
            if (angular.isUndefined(newVal) || angular.isUndefined(oldVal)) {
              return;
            }
            //update parent service multiselect
            Service.displayData.multiSelect = newVal;
          });
        }
      }
    };
  }]);
