'use strict';

var app = angular.module('frontendApp');

/**
 * Inherit common methods for user permissions tables
 */
app.factory('AdminStaffAssignmentCtrlInheritanceService', [
  function () {
    return {
      inherit: function ($scope, Service, type, multiSelect) {
        var vm = this;
        /**
         * Watch user documents and display table when we have the documents
         */
          // Safe data
        vm.safeData = Service.safeData;
        //display data
        vm.displayData = Service.displayData;

        /**
         * Update top level dropdowns on backend
         */
        vm.attachWatch = function () {
          $scope.$watch(function () {
            return vm.safeData[type];
          }, function (newVal, oldVal) {
            if (angular.isUndefined(oldVal) || angular.equals(oldVal, newVal)) {
              return;
            }
            /**
             * @todo have to implement recalc commission
             */
          }, true);
        };

        //watch collection to update table data --important
        $scope.$watchCollection(function () {
          return Service.displayData[type];
        }, function (newVal) {
          if (!angular.isArray(newVal)) {
            return;
          }
          // Set table data
          vm.tableData = newVal;
          vm.rowData = newVal.slice();

          if (multiSelect) {
            // Check the boxes which are currently active
            vm.multiSelect = Service.checkGroups(newVal);
          }
        });

        // If multi select is being used in the table, watch those values
        if (multiSelect) {
          /**
           * Watch which records are checked, and update the backend
           */
          $scope.$watchCollection(function () {
            return vm.multiSelect;
          }, function (newVal, oldVal) {
            // Don't write on load
            if (angular.isUndefined(newVal) || angular.isUndefined(oldVal)) {
              return;
            }
            //update parent service multiselect
            Service.displayData.multiSelect = newVal;
          });
        }
      }
    };
  }]);
