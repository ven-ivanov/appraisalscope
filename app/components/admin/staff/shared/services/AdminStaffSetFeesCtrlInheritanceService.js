'use strict';

var app = angular.module('frontendApp');

/**
 * Inherit common methods for set fees
 */
app.factory('AdminStaffSetFeesCtrlInheritanceService', [
  function () {
    return {
      createCtrl: function ($scope, Service, type, multiSelect) {
        /**
         * @TODO
         */
        //vm point to this isolated directive controller and its scope
        var vm = this;
        // Safe and display data
        vm.safeData = Service.safeData;
        vm.displayData = Service.displayData;



        /**
         * Update top level dropdowns on backend
         */
        vm.attachWatch = function () {
          $scope.$watch(function () {
            return vm.safeData[type];
          }, function (newVal, oldVal) {
            if (angular.isUndefined(oldVal) || angular.equals(oldVal, newVal)) {
              return;
            }
            /**
             * @todo have to implement commission
             */
          }, true);
        };

        /**
         * commission
         */
        $scope.$watchCollection(function () {
          return vm.commission;
        }, function (newVal) {
          // If we have an order, then write it
          if (angular.isArray(newVal) && newVal.length) {
            Service.setCommission(newVal, 'commissions')
              .then(function () {
                $scope.$broadcast('hide-modal', 'set-fees-branch');
              }).finally(function () {
                // Reset array
                vm.commission = [];
              }).catch(function () {
                // Show failure
                $scope.$broadcast('show-modal', 'set-fees-branch', true);
              });
          }
        });

        //if multi select
        if(multiSelect){

        }
      }
    };
  }]);
