'use strict';

var app = angular.module('frontendApp');

/**
 * Inherit common methods for set fees
 */
app.factory('AdminStaffRecalcCommissionsCtrlInheritanceService',[
  function () {
    return {
      createCtrl: function ($scope, Service, type, multiSelect) {
        /**
         * @TODO
         */
        //vm point to this isolated directive controller and its scope
        var vm = this;
        vm.safeData = Service.safeData;
        vm.displayData = Service.displayData;
        vm.attachWatch = function () {
          $scope.$watch(function () {
            return vm.safeData[type];
          }, function (newVal, oldVal) {
            if (angular.isUndefined(oldVal) || angular.equals(oldVal, newVal)) {
              return;
            }
            /**
             * @todo have to implement recalc commission
             */
          }, true);
        };
        //watch collection to update table data --important
        $scope.$watchCollection(function () {
          return Service.displayData[type];
        }, function (newVal) {
          if (!angular.isArray(newVal)) {
            return;
          }
          // Set table data
          vm.tableData = newVal;
          vm.rowData = newVal.slice();

          if (multiSelect) {
            // Check the boxes which are currently active
            vm.multiSelect = Service.checkGroups(newVal);
          }
        });

        //we don't have multi select
        /**
         * Get initial data on load
         */
        vm.init = function () {
          // Table data
          Service.getJobTypesData()
            .then(function () {
              //init job types list
              Service.initSelectedJobTypes();
              //now attach watcher
              vm.attachWatch();
            });
        };
        type = 'commissions';
        multiSelect = true;
      }
    };
  }]);
