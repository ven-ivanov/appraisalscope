/**
 * Created by Venelin on 6/23/2015.
 */
/**
 * Created by Venelin on 6/19/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Staff - AdminStaffAdminReviewerRuleProfileService
 */
app.factory('AdminStaffReviewerRuleProfileService',
  ['AdminStaffService','AdminStaffResourceService','AsDataService', function ( AdminStaffService,AdminStaffResourceService, AsDataService) {
    var Service = {
      // Safe data
      safeData: {
        //Reviewer Rule profiles
        profiles: [],
        //table configuration
        heading:[],
        //companies to add,
        companies: [],
        //location to add
        states: [],
        //process status
        processStatus: [],
        //Job Type,
        jobType: [],
        //Paid Status
        paidStatus:[]
      },
      // Display data
      displayData: {
        //new Profile
        newProfile: {},
        //Reviewer Rule profiles
        profiles: [],
        //details
        details : [],
        //table configuration
        heading: [],
        //companies to add,
        companies: [],
        //location to add
        states: [],
        //process status
        processStatus: [],
        //Job Type,
        jobType: [],
        //Paid Status
        paidStatus:[],
        //contentTitle
        contentTitle: '',
        //selected action
        selectedAction: 'Create',
        //selected profile
        selectedProfile: {},
        //selected Company Id
        selectedCompanyId: 0,
        //selected branch Id
        selectedBranchId: 0,
        //selected broker Id
        selectedBrokerId: 0,
        //selected job type Id
        selectedJobTypeId: 0,
        //selected paid status Id
        selectedPaidStatusId: 0,
        //selected process status Id
        selectedProcessStatusId:0,
        //selected state Id
        selectedStateId:0
      },
      // Data transformations
      transformers: {
        // Registered data
        registered: {
          filter: 'date'
        }
      },
      /**
       * Request wrapper
       */
      request: function (requestFn, params) {
        // Get $resource configuration
        return AdminStaffResourceService.reviewerRuleProfile[requestFn](params);
      },
      /**
       * Retrieve admin rule profiles
       */
      getReviewerRuleProfiles: function () {
        return Service.request('init')
          .then(function (response) {
            // Hash profiles
            Service.hashData(response.data, 'profiles');
            //Service.formatData('profiles');
            Service.displayData.profiles = response.data;
          });
      }
    };

    /**
     * Inherit from AdminStaffService
     */
    Service.loadRecords = AdminStaffService.loadRecords.bind(Service, 'reviewerRuleProfile');
    Service.hashData = AsDataService.hashData.bind(Service);
    Service.formatData = AsDataService.formatData.bind(Service, 'reviewerRuleProfile');
    Service.formatCompaniesData = AsDataService.formatData.bind(Service, 'companies');
    Service.formatStatesData = AsDataService.formatData.bind(Service, 'states');
    Service.formatPaidStatusData = AsDataService.formatData.bind(Service, 'paidStatus');
    Service.formatProcessStatusData = AsDataService.formatData.bind(Service, 'processStatus');
    Service.formatJobTypeData = AsDataService.formatData.bind(Service, 'jobType');
    Service.transformData = AsDataService.transformData.bind(Service);
    // Get record details
    Service.getRecordDetails =
      AdminStaffService.getRecordDetails.bind(Service, 'reviewerRuleProfile',
        Service.getReviewerRuleProfiles);
    // Update record
    //Service.updateRecord = AdminStaffService.updateRecord.bind(Service, 'reviewerRuleProfile');

    //Profile Management
    Service.newProfile = AdminStaffService.newProfile.bind(Service,'reviewerRuleProfile');
    Service.updateProfile = AdminStaffService.updateProfile.bind(Service,'reviewerRuleProfile');
    Service.deleteProfile = AdminStaffService.deleteProfile.bind(Service,'reviewerRuleProfile');

    //Company Management
    Service.addCompanies = AdminStaffService.addCompanies.bind(Service);
    Service.getCompanies = AdminStaffService.getCompanies.bind(Service);
    Service.deleteCompany = AdminStaffService.deleteCompany.bind(Service);

    //State Management
    Service.getStates = AdminStaffService.getStates.bind(Service);
    Service.addStates = AdminStaffService.addStates.bind(Service);
    Service.deleteState = AdminStaffService.deleteState.bind(Service);

    //Paid Status Management
    Service.getPaidStatus = AdminStaffService.getPaidStatus.bind(Service);
    Service.addPaidStatus = AdminStaffService.addPaidStatus.bind(Service);
    Service.deletePaidStatus = AdminStaffService.deletePaidStatus.bind(Service);

    //Process Status Management
    Service.getProcessStatus = AdminStaffService.getProcessStatus.bind(Service);
    Service.addStatus = AdminStaffService.addStatus.bind(Service);
    Service.deleteProcessStatus = AdminStaffService.deleteProcessStatus.bind(Service);

    //Job Type Management
    Service.getJobType = AdminStaffService.getJobType.bind(Service);
    Service.addJobTypes = AdminStaffService.addJobTypes.bind(Service);
    Service.deleteJobType = AdminStaffService.deleteJobType.bind(Service);

    return Service;
  }]);
