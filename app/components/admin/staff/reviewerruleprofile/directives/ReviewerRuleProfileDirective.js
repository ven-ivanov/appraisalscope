/**
 * Created by Venelin on 6/11/2015.
 */
'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffReviewerRuleProfile', ['DirectiveInheritanceService',function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/staff/reviewerruleprofile/directives/partials/reviewer-rule-profile.html',
    controller:'AdminStaffReviewerRuleProfileCtrl',
    controllerAs:'adminStaffReviewerRuleProfileCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective.call(null, scope);
    }
  };
}]);
