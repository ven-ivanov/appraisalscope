/**
 * Created by Venelin on 6/23/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Search table for clients that has a profile
 */
app.controller('AdminStaffReviewerRuleProfileLocationCtrl',
  ['AdminStaffReviewerRuleProfileService', 'AsTableService', '$scope', '$q', 'AdminStaffTableCtrlInheritanceService',
    function (AdminStaffReviewerRuleProfileService, AsTableService, $scope, $q, AdminStaffTableCtrlInheritanceService) {
      var vm = this;
      /**
       * @TODO
       */
      var heading =  [
        /*
         * D
         */
        {
          label: 'D',
          linkLabel: '[D]',
          fn: 'deleteState'
        },
        /*
         * [C]
         */
        {
          label: 'C',
          linkLabel: '[C]',
          fn: 'selectCounty'
        },
        {
          label: 'Location',
          data: 'name'
        },
        /*
         * [Round Robin]
         */
        {
          label: 'RB',
          linkLabel: '[Round Robin]',
          fn: 'roundRobin'
        }
      ];
      vm.page = {displayPage: 1};

      // Filter table by property
      vm.filterByProp = {};

      /**
       * Link functions
       */
      vm.linkFn = function (id, fnName) {
        // Keep reference to selected staff
        vm.displayData.selectedStateId = id;
        if(fnName === 'deleteState'){
          vm.deleteStateModal();
        }else if(fnName === 'roundRobin') {
          vm.roundRobin(id);
        }else if(fnName === 'selectCounty') {
          vm.selectCounty(id);        }

      };

      vm.deleteStateModal = function () {
        $scope.$parent.$broadcast('show-modal','reviewer-rule-profile-delete-state-confirm');
      };
      vm.roundRobin = function() {

      };
      vm.selectCounty = function() {
        $scope.$parent.$broadcast('show-modal','reviewer-rule-profile-delete-stconfirm');
      };

      // Inherit table controller methods
      AdminStaffTableCtrlInheritanceService.inherit.call(vm, $scope, AdminStaffReviewerRuleProfileService,heading, 'states');

    }]);
