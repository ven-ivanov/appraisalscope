/**
 * Created by Venelin on 6/23/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Search table for clients that has a profile
 */
app.controller('AdminStaffReviewerRuleProfilePaidStatusCtrl',
  ['AdminStaffReviewerRuleProfileService', 'AsTableService', '$scope', '$q', 'AdminStaffTableCtrlInheritanceService',
    function (AdminStaffReviewerRuleProfileService, AsTableService, $scope, $q, AdminStaffTableCtrlInheritanceService) {
      var vm = this;
      /**
       * @TODO
       */
      var heading =  [
        /*
         * D
         */
        {
          label: 'D',
          linkLabel: '[D]',
          fn: 'deletePaidStatus'
        },
        {
          label: 'Paid Status',
          data: 'label'
        },
        /*
         * [Round Robin]
         */
        {
          label: 'RB',
          linkLabel: '[Round Robin]',
          fn: 'roundRobin'
        }
      ];
      vm.page = {displayPage: 1};

      // Filter table by property
      vm.filterByProp = {};

      /**
       * Link functions
       */
      vm.linkFn = function (id, fnName) {
        // Keep reference to selected staff
        vm.displayData.selectedPaidStatusId = id;
        if(fnName === 'deletePaidStatus'){
          vm.deletePaidStatusModal();
        }else if(fnName === 'roundRobin') {
          vm.roundRobin(id);
        }
      };

      vm.deletePaidStatusModal = function () {
        $scope.$parent.$broadcast('show-modal','reviewer-rule-profile-delete-paid-status-confirm');
      };
      vm.roundRobin = function() {

      };
      // Inherit table controller methods
      AdminStaffTableCtrlInheritanceService.inherit.call(vm, $scope, AdminStaffReviewerRuleProfileService,heading, 'paidStatus');

    }]);
