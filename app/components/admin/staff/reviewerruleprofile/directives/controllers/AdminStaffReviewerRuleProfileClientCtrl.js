'use strict';

var app = angular.module('frontendApp');

/**
 * Search table for clients that has a profile
 */
app.controller('AdminStaffReviewerRuleProfileClientCtrl',
  ['AdminStaffReviewerRuleProfileService', 'AsTableService', '$scope', '$q', 'AdminStaffTableCtrlInheritanceService',
    function (AdminStaffReviewerRuleProfileService, AsTableService, $scope, $q, AdminStaffTableCtrlInheritanceService) {
      var vm = this;
      /**
       * @TODO
       */
      var heading =  [
        /*
         * D
         */
        {
          label: 'D',
          linkLabel: '[D]',
          fn: 'deleteClient'
        },
        /*
         * [B]
         */
        {
          label: 'B',
          linkLabel: '[B]',
          fn: 'selectBranches'
        },
        /*
         * [BK]
         */
        {
          label: 'BK',
          linkLabel: '[BK]',
          fn: 'selectBrokers'
        },
        {
          label: 'Client',
          data: 'name'
        },
        /*
         * Round Robin
         */
        {
          label: 'RB',
          linkLabel: '[Round Robin]',
          fn: 'roundRobin'
        },
        /*
         * Use Location Rule
         */
        {
          label: 'LR',
          linkLabel: '[use location rule]',
          // When 'location rule' click, it deletes client
          fn: 'useLocationRule'
        }
      ];
      vm.page = {displayPage: 1};

      // Filter table by property
      vm.filterByProp = {};
      /**
       * Link functions
       */
      vm.linkFn = function (id, fnName) {
        // Keep reference to selected staff
        vm.displayData.selectedCompanyId = id;
        if(fnName === 'selectBranches'){
          vm.selectBranches(id);
        }else if(fnName === 'selectBrokers') {
          vm.selectBrokers(id);
        }else if(fnName === 'deleteClient') {
          vm.deleteCompanyModal();
        }else if(fnName === 'roundRobin') {
          vm.roundRobin(id);
        }else if(fnName === 'useLocationRule') {
          vm.useLocationRule(id);
        }
      };
      //select branches
      vm.selectBranches = function() {
        $scope.$parent.$broadcast('show-modal','reviewer-rule-profile-add-branch');
      };
      //select brokers
      vm.selectBrokers = function() {
        $scope.$parent.$broadcast('show-modal','reviewer-rule-profile-add-broker');
      };      //delete company

      vm.deleteCompanyModal = function () {
        $scope.$parent.$broadcast('show-modal','reviewer-rule-profile-delete-company-confirm');
      };
      //round robin
      vm.roundRobin = function() {

      };
      // use location rule
      vm.useLocationRule = function() {

      };

      // Inherit table controller methods
      AdminStaffTableCtrlInheritanceService.inherit.call(vm, $scope, AdminStaffReviewerRuleProfileService,heading, 'companies');

    }]);
