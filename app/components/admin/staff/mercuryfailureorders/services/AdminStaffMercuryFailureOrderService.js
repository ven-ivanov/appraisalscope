/**
 * Created by Venelin on 6/19/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Staff - AdminStaffMercuryFailureOrderService
 */
app.factory('AdminStaffMercuryFailureOrderService',
  ['API_PREFIX', 'AdminStaffService','AdminStaffResourceService','AsDataService', function (API_PREFIX, AdminStaffService,AdminStaffResourceService,AsDataService) {
    var Service = {
      // Safe data
      safeData: {
        //mercuryFailureOrders data
        mercuryFailureOrders: [],
        //table configuration
        heading:[]
      },
      //search track id
      searchTrackId: '',
      // Type under which to make requests
      requestType: '',
      // Display data
      displayData: {
        //staff data
        mercuryFailureOrders: [],
        details : []
      },
      // Data transformations
      transformers: {
        // Registered data
        registered: {
          filter: 'date'
        }
      },
      /**
       * Request (mercuryFailureOrder, orderId)
       */
      request: function(requestFn, urlParams, bodyParams) {
        //Service.getRequestType();
        //get $resource configuration
        //return AdminStaffResourceService.mercuryFailureOrders[requestFn](Service.safeData.requestType, urlParams, bodyParams);
        return AdminStaffResourceService.mercuryFailureOrders[requestFn](urlParams, bodyParams);
      },
      /**
       * Retrieve settings
       */
      getMercuryFailureOrders: function () {
        return Service.request('init').then(function (response) {
            // Hash settings
            Service.hashData(response.data, 'mercuryFailureOrders');
          });
      },
      /**
       * Process Mercury order
       */
      processOrder:function(orderId, params){
        //process Order
        return Service.request('processOrder', {orderId: orderId}, params);
      },
      /**
       * Search orders
       */
      searchOrder: function(){
        Service.displayData.mercuryFailureOrders = [];
        return Service.request('searchOrder', Service.searchTrackId)
          .then(function(response){
            Service.hashData(response.data, 'mercuryFailureOrders');
          });
      }
    };

    /**
     * Inherit from AdminStaffService
     */
    Service.loadRecords = AdminStaffService.loadRecords.bind(Service, 'mercuryFailureOrders');
    Service.hashData = AsDataService.hashData.bind(Service);
    Service.formatData = AsDataService.formatData.bind(Service, 'mercuryFailureOrders');
    Service.transformData = AsDataService.transformData.bind(Service);
    // Get record details
    Service.getRecordDetails =
      AdminStaffService.getRecordDetails.bind(Service, 'mercuryFailureOrders',
        Service.getMercuryFailureOrders);
    // Set state
   // Service.setStateOnLoad = AdminStaffService.setStateOnLoad.bind(Service, 'mercuryFailureOrders');

    // Update record
    Service.updateRecord = AdminStaffService.updateRecord.bind(Service, 'mercuryFailureOrders');

    return Service;
  }]);
