/**
 * Created by Venelin on 6/16/2015.
 */
'use strict';

var app = angular.module('frontendApp');

app.directive('adminStaffMercuryFailureOrders', ['DirectiveInheritanceService',function (DirectiveInheritanceService) {
  return {
    templateUrl: '/components/admin/staff/mercuryfailureorders/directives/partials/mercury-failure-orders.html',
    controller:'AdminStaffMercuryFailureOrderCtrl',
    controllerAs:'adminStaffMercuryFailureOrderCtrl',
    link: function (scope) {
      // Inherit trigger download method
      DirectiveInheritanceService.inheritDirective(scope);
    }
  };
}]);
