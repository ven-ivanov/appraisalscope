/**
 * Created by Venelin on 6/19/2015.
 */
'use strict';
var app = angular.module('frontendApp');
/**
 * @ngdoc function
 * @name frontendApp.controller:AdminStaffMercuryFailureOrderCtrl
 * @description
 * # AdminStaffMercuryFailureOrderCtrl
 *
 * Controller of the frontendApp
 */
app.controller('AdminStaffMercuryFailureOrderCtrl',
  [
    '$scope',
    'AdminStaffCtrlInheritanceService',
    'AdminStaffService',
    'AdminStaffMercuryFailureOrderService',
    function ($scope,AdminStaffCtrlInheritanceService, AdminStaffService,AdminStaffMercuryFailureOrderService) {
      var vm = this, // This service name is just getting out of hand with the line breaks
       Service = AdminStaffMercuryFailureOrderService;
      // Inherit common functions
      AdminStaffCtrlInheritanceService.createCtrl.call(vm, Service, $scope, 'mercuryFailureOrders');
      // Directory details
      vm.displayData = Service.displayData;
      // Safe data
      vm.safeData = Service.safeData;

      //searchID
      vm.searchTrackId = Service.searchTrackId;
      /**
       * Initiate data and state
       */
      vm.init = function () {
        // Set state on load
       // Service.setStateOnLoad($stateParams, vm);
        // Keep reference to which section we're on
        AdminStaffService.section = 'mercuryFailureOrders';
      };
      /**
       * Process Mercury Order
       */
      vm.processOrder = function(orderId) {
        //process order
        AdminStaffMercuryFailureOrderService.processOrder(orderId)
          .then(function () {
            $scope.$broadcast('hide-modal', 'process-order-confirm');
          })
          .catch(function () {
            $scope.$broadcast('show-modal', 'process-order-failure', true);
          });
      };
      /**
       * Search by tracking ID
       */
      vm.search = function() {
        AdminStaffMercuryFailureOrderService.searchOrder();
      };
      /**
       * Clear search
       */
      vm.clear = function() {
        //just call init
        vm.init();
      };
      // Init controller load
      vm.init();
    }]);
