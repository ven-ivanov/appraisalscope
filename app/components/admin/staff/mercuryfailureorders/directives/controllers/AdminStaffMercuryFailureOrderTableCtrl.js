/**
 * Created by Venelin on 6/22/2015.
 */
'use strict';

var app = angular.module('frontendApp');

/**
 * Search table for clients and users
 */
app.controller('AdminStaffMercuryFailureOrderTableCtrl',
  ['AdminStaffMercuryFailureOrderService', 'AsTableService', '$scope', '$q', 'AdminStaffTableCtrlInheritanceService',
    function (AdminStaffMercuryFailureOrderService, AsTableService, $scope, $q, AdminStaffTableCtrlInheritanceService) {
      var vm = this;
      /**
       * @TODO
       */
      var heading =  [
        {
          label: 'S.No',
          data: 'id'
        },
        {
          label: 'Tracking ID',
          data: 'trackingId'
        },
        {
          label: 'Failure Reason',
          data:  'failureReason'
        },
        {
          label: 'Requested Date',
          data: 'requestDate'
        },
        {
          label: 'Action',
          type: 'action',
          linkLabel: 'Process',
          fn: 'processMercuryOrder'
        }
      ];
      vm.page = {displayPage: 1};

      // Filter table by property
      vm.filterByProp = {};

      // Inherit table controller methods
      AdminStaffTableCtrlInheritanceService.inherit.call(vm, $scope, AdminStaffMercuryFailureOrderService,heading, 'mercuryFailureOrders');

      /**
       * Link functions
       */
      vm.linkFn = function(id, fnName){
        if(fnName === 'processMercuryOrder'){
            $scope.$parent.$broadcast('show-modal','process-order-confirm');
        }
      };

    }]);


