'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.directive:appraisalFormField
 * @description
 *
 */

app.directive('appraisalFormField', [
  '$compile', 'APPRAISAL_FORM_FIELDS', 'SharedElementGeneratorService',

  function($compile, APPRAISAL_FORM_FIELDS, SharedElementGeneratorService) {

    /**
     * Get options from the config constant.
     *
     * @param {String} key
     * @returns {Object}
     */
    function getConfig(key) {
      if (typeof key === 'undefined') {
        console.log('appraisalFormField Directive: form-model attribute is required');
        return {};
      }

      // First two parts of the key are not needed. Throw them away
      key = key.split('.');
      key.shift();
      key.shift();
      key = key.join('.');

      if (typeof APPRAISAL_FORM_FIELDS[key] === 'undefined') {
        console.log('appraisalFormField Directive: No config found for key: '+ key);
        return {};
      }

      var config = APPRAISAL_FORM_FIELDS[key];

      // Assign default values for missing options
      config.label       = typeof config.label       !== 'undefined'? config.label:       'Missing Label';
      config.type        = typeof config.type        !== 'undefined'? config.type:        'input';
      config.model       = typeof config.model       !== 'undefined'? config.model:       'missing';
      config.placeholder = typeof config.placeholder !== 'undefined'? config.placeholder: '';
      config.change      = typeof config.change      !== 'undefined'? config.change: '';

      return APPRAISAL_FORM_FIELDS[key];
    }

    return {
      compile: function(element, attrs) {
        var fieldModal = attrs.fieldModel;
        var config     = getConfig(fieldModal);
        var HTML       = '';

        switch (config.type) {
          case 'input':
            HTML = SharedElementGeneratorService.input({
              'id': fieldModal.replace('.', '-'),
              'type': 'text',
              'class': 'form-control',
              'ng-model': fieldModal,
              'placeholder': config.placeholder
            });
            break;
          case 'disabled':
            HTML = SharedElementGeneratorService.div('{{'+ fieldModal +'}}', {
              'class': 'no-edit'
            });
            break;
          case 'select':
            HTML = SharedElementGeneratorService.select(config.placeholder, {
              'as-select': '',
              'ng-options': config.options,
              'ng-model': fieldModal,
              'ng-change': config.change
            });
            break;
        }

        if (HTML !== '') {
          var label = SharedElementGeneratorService.label(config.label, {
            'for': fieldModal.replace('.', '-')
          });

          element.html(label + HTML);
        }
      }
    };
  }
]);
