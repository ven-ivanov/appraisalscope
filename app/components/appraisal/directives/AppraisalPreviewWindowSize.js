'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.directive:OrdersResizeDirective
 * @description
 *
 *
 */
app.directive('appraisalPreviewWindowSize', [
  '$window', 'AppraisalStateService',

  function($window, AppraisalStateService) {
    var window = angular.element($window);
    var PREVIEW_HEADER_HEIGHT  = 180;
    var SCROLL_ANIMATION_DELAY = 250;
    var scrollPositions        = {};

    return {
      link: function(scope, element) {
        // This directive should only work in preview mode
        if (AppraisalStateService.data.previewMode === false) {
          return false;
        }

        var previewWindowPosition = element.find('.fixed-position');

        // Set preview window height
        function updatePreviewSize() {
          previewWindowPosition.css('height', (window.innerHeight() - PREVIEW_HEADER_HEIGHT) +'px');
        }

        updatePreviewSize();

        window.bind('resize', updatePreviewSize);

        // Prevent page scrolling while the user is scrolling through the preview window
        previewWindowPosition.on('DOMMouseScroll mousewheel', function(ev) {
          var $this = angular.element(this),
            scrollTop = this.scrollTop,
            scrollHeight = this.scrollHeight,
            height = $this.height(),
            delta = ev.originalEvent.wheelDelta,
            up = delta > 0;

          var prevent = function() {
            ev.stopPropagation();
            ev.preventDefault();
            ev.returnValue = false;
            return false;
          };

          if (!up && -delta > scrollHeight - height - scrollTop) {
            // Scrolling down, but this will take us past the bottom.
            $this.scrollTop(scrollHeight);
            return prevent();
          } else if (up && delta > scrollTop) {
            // Scrolling up, but this will take us past the top.
            $this.scrollTop(0);
            return prevent();
          }
        });

        // When appraisals changes scroll position should be where we left it last time.
        // If no value is saved put it on top
        scope.$watch(AppraisalStateService.selectedAppraisal, function(appraisalId, previousAppraisal) {

          scrollPositions[previousAppraisal] = previewWindowPosition.scrollTop();

          if (typeof scrollPositions[appraisalId] === 'undefined') {
            previewWindowPosition.animate({ scrollTop: '0px'}, SCROLL_ANIMATION_DELAY);
          } else {
            previewWindowPosition.animate({ scrollTop: scrollPositions[appraisalId]+ 'px'}, SCROLL_ANIMATION_DELAY);
          }
        });
      }
    };
  }
]);
