'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.directive:sharedAppraisalMasonry
 * @description
 */
app.directive('appraisalMasonry', [
  'AppraisalStateService',

  function(AppraisalStateService) {
    return {
      compile: function(element, attrs) {
        AppraisalStateService.registerMasonryWatcher(function() {
          new Masonry(document.querySelector('#'+ attrs.id), {
            itemSelector: ['.masonrybox-two', '.masonrybox-one'],
            columnWidth: 5,
            gutter: 10
          });
        });
      }
    };
  }
]);
