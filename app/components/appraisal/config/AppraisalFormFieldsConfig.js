'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc constant
 * @name frontendApp.constant:ORDERS_PAGE_CONFIG
 * @description
 *
 * Configuration of different elements on the page for each user type
 */
app.constant('APPRAISAL_FORM_FIELDS', {
  // PROPERTY TYPE
  'property.type': {
    type: 'select',
    label: 'Property Type',
    options: 'item.option as item.label for item in mainCtrl.selectOptions.data.propertyTypes'
  },

  // LOT
  lot: {
    type: 'input',
    label: 'Lot'
  },

  // BUILT IN
  builtIn: {
    type: 'input',
    label: 'Built In'
  },

  // LAST SOLD
  lastSold: {
    type: 'input',
    label: 'Last Sold'
  },

  // SOLD FOR
  soldFor: {
    type: 'input',
    label: 'For'
  },

  // FLOOR SIZE
  floorSize: {
    type: 'input',
    label: 'Floor Size'
  },

  // BATHROOMS
  bathrooms: {
    type: 'input',
    label: 'Bathrooms'
  },

  // CLIENT
  'client.id': {
    type: 'select',
    label: 'Client',
    options: 'item.option as item.label for item in mainCtrl.selectOptions.data.clients',
    change: 'mainCtrl.appraisal.clientOnReport.companyId = null; mainCtrl.selectOptions.clientsOnReport(mainCtrl.appraisal.client.id)'
  },

  // CLIENT ON REPORT
  'clientOnReport.companyId': {
    type: 'select',
    label: 'Client Displayed On Report',
    options: 'item.option as item.label for item in mainCtrl.selectOptions.data.clientsOnReport'
  },

  // ORDERED FOR
  'owner.id': {
    type: 'input',
    label: 'Ordered For'
  },

  // SUBMITTED BY
  'creator.name': {
    type: 'disabled',
    label: 'Submitted By'
  },

  // AMC LICENCE
  'amc.licenses.document.file': {
    type: 'disabled',
    label: 'AMC Licence'
  },

  // LICENCE EXPIRATION DATE
  'amc.licenses.expireAt': {
    type: 'disabled',
    label: 'Licence Expiration Date'
  },

  // TOTAL SERVICES AND LENDERS TITLE INSURANCE
  totalServicesFee: {
    type: 'input',
    label: 'Total Services And Lenders Title Insurance'
  },

  // SETTLEMENT OR COSING FEE
  settlementOrCostFee: {
    type: 'input',
    label: 'Settlement Or Cosing Fee'
  },

  // LENDERS TITLE INSURANCE
  lendersTitleInsurance: {
    type: 'input',
    label: 'Lenders Title Insurance'
  },

  // JUDGMENT SEARCH
  judgmentSearch: {
    type: 'input',
    label: 'Judgment Search'
  },

  // TITLE EXAM
  titleExam: {
    type: 'input',
    label: 'Title Exam'
  },

  // ABSTRACT
  abstract: {
    type: 'input',
    label: 'Abstract'
  },

  // WIRE FEE
  wireFee: {
    type: 'input',
    label: 'Wire Fee'
  },

  // JOB TYPE
  'jobType.id': {
    type: 'input',
    label: 'Job Type'
  },

  // CLIENT FEE
  'payment.receivable.fee': {
    type: 'input',
    label: 'Client Fee'
  },

  // PAID AMOUNT
  'payment.receivable.amount.paid': {
    type: 'input',
    label: 'Paid Amount'
  },

  // AMOUNT DUE
  'payment.receivable.amount.due': {
    type: 'input',
    label: 'Amount Due'
  },

  // APPRAISER FEE
  'payment.payable.fee': {
    type: 'input',
    label: 'Appraiser Fee'
  },

  // PAID STATUS
  paidStatus: {
    type: 'input',
    label: 'Paid Status'
  },

  // PAID DATE
  'payment.payable.paidAt': {
    type: 'input',
    label: 'Paid Date'
  },

  // PAYMENT HISTORY
  paymentHistory: {
    type: 'input',
    label: 'Payment History'
  },

  // LAST UPDATE
  customStatus: {
    type: 'input',
    label: 'Last Update'
  },

  // PROCESS STATUS
  status: {
    type: 'input',
    label: 'Process Status'
  },

  // NOTIFIED USERS
  notifiedUsers: {
    type: 'input',
    label: 'Notified Users'
  },

  // STAFF USER
  'staff.id': {
    type: 'input',
    label: 'Staff User'
  },

  // FHA
  fha: {
    type: 'input',
    label: 'FHA'
  },

  // FILE NUMBER
  file: {
    type: 'input',
    label: 'File Number'
  },

  // LOAN NUMBER
  'loan.number': {
    type: 'input',
    label: 'Loan Number'
  },

  // LOAN AMOUNT
  'loan.amount': {
    type: 'input',
    label: 'Loan Amount'
  },

  // LOAN TYPE
  'loan.type': {
    type: 'input',
    label: 'Loan Type'
  },

  // REFERENCE NUMBER
  'loan.reference': {
    type: 'input',
    label: 'Reference Number'
  },

  // ORDER DATE
  createdAt: {
    type: 'disabled',
    label: 'Order Date'
  },

  // DUE DATE
  'dueDate.client': {
    type: 'input',
    label: 'Due Date'
  },

  // APPRAISER DUE DATE
  'dueDate.appraiser': {
    type: 'input',
    label: 'Appraiser Due Date'
  },

  // ASSIGNED DATE
  'assignment.assignedAt': {
    type: 'input',
    label: 'Assigned Date'
  },

  // ACCEPTED DATE
  'assignment.acceptedAt': {
    type: 'input',
    label: 'Accepted Date'
  },

  // INSPECTION DATE
  'inspection.startedAt': {
    type: 'input',
    label: 'Inspection Date'
  },

  // ESTIMATED COMPLETION DATE
  'inspection.estimatedCompletionDate': {
    type: 'input',
    label: 'E.C.D.'
  },

  // LEGAL
  'property.legal': {
    type: 'input',
    label: 'Legal'
  },

  // ADDRESS
  'property.location.address1': {
    type: 'input',
    label: 'Address'
  },

  // ADDITIONAL INFORMATION
  'property.location.address2': {
    type: 'input',
    label: 'Additional Information'
  },

  // CITY
  'property.location.city': {
    type: 'input',
    label: 'City'
  },

  // STATE
  'property.location.state.code': {
    type: 'input',
    label: 'State'
  },

  // ZIP
  'property.location.zip': {
    type: 'input',
    label: 'Zip'
  },

  // COUNTY
  'property.location.county': {
    type: 'input',
    label: 'County'
  },

  // OCCUPANCY
  'property.occupancy': {
    type: 'input',
    label: 'Occupancy'
  },

  // BEST PERSON TO CONTACT
  'property.personToContact': {
    type: 'input',
    label: 'Best Person To Contact'
  },

  // BORROWER FIRST NAME
  'property.contacts.borrower.firstName': {
    type: 'input',
    label: 'Borrower First Name'
  },

  // BORROWER MI
  'property.contacts.borrower.mi': {
    type: 'input',
    label: 'MI'
  },

  // BORROWER LAST NAME
  'property.contacts.borrower.lastName': {
    type: 'input',
    label: 'Borrower Last Name'
  },

  // BORROWER EMAIL
  'property.contacts.borrower.email': {
    type: 'input',
    label: 'Enter Borrower\'s Email To Expedite Scheduling'
  },

  // BORROWER ENTITY NAME
  borrowerEntityName: {
    type: 'input',
    label: 'Entity name'
  },

  // BORROWER WORK NUMBER
  'property.contacts.borrower.phones.work': {
    type: 'input',
    label: 'Work Number'
  },

  // BORROWER PHONE NUMBER
  'property.contacts.borrower.phones.home': {
    type: 'input',
    label: 'Phone Number'
  },

  // BORROWER CELLPHONE NUMBER
  'property.contacts.borrower.phones.cell': {
    type: 'input',
    label: 'Cellphone Number'
  },

  // CO-BORROWER FIRST NAME
  'property.contacts.coBorrower.firstName': {
    type: 'input',
    label: 'Co-borrower First Name'
  },

  // CO-BORROWER MI
  'property.contacts.coBorrower.mi': {
    type: 'input',
    label: 'MI'
  },

  // CO-BORROWER LAST NAME
  'property.contacts.coBorrower.lastName': {
    type: 'input',
    label: 'Co-borrower Last Name'
  },

  // CO-BORROWER EMAIL
  'property.contacts.coBorrower.email': {
    type: 'input',
    label: 'Enter Co-borrower\'s Email To Expedite Scheduling'
  },

  // CO-BORROWER WORK NUMBER
  'property.contacts.coBorrower.phones.work': {
    type: 'input',
    label: 'Work Number'
  },

  // CO-BORROWER PHONE NUMBER
  'property.contacts.coBorrower.phones.home': {
    type: 'input',
    label: 'Phone Number'
  },

  // CO-BORROWER CELLPHONE NUMBER
  'property.contacts.coBorrower.phones.cell': {
    type: 'input',
    label: 'Cellphone Number'
  },

  // OWNER FIRST NAME
  'property.contacts.owner.firstName': {
    type: 'input',
    label: 'Owner First Name'
  },

  // OWNER MI
  'property.contacts.owner.mi': {
    type: 'input',
    label: 'MI'
  },

  // OWNER LAST NAME
  'property.contacts.owner.lastName': {
    type: 'input',
    label: 'Owner Last Name'
  },

  // OWNER EMAIL
  'property.contacts.owner.email': {
    type: 'input',
    label: 'Enter Borrower\'s Email To Expedite Scheduling'
  },

  // OWNER WORK NUMBER
  'property.contacts.owner.phones.work': {
    type: 'input',
    label: 'Work Number'
  },

  // OWNER PHONE NUMBER
  'property.contacts.owner.phones.home': {
    type: 'input',
    label: 'Phone Number'
  },

  // OWNER CELLPHONE NUMBER
  'property.contacts.owner.phones.cel': {
    type: 'input',
    label: 'Cellphone Number'
  },

  // OTHER FIRST NAME
  'property.contacts.other.firstName': {
    type: 'input',
    label: 'First Name'
  },

  // OTHER MI
  'property.contacts.other.mi': {
    type: 'input',
    label: 'MI'
  },

  // OTHER LAST NAME
  'property.contacts.other.lastName': {
    type: 'input',
    label: 'Last Name'
  },

  // OTHER EMAIL
  'property.contacts.other.email': {
    type: 'input',
    label: 'Enter Email To Expedite Scheduling'
  },

  // OTHER WORK NUMBER
  'property.contacts.other.phones.work': {
    type: 'input',
    label: 'Work Number'
  },

  // OTHER PHONE NUMBER
  'property.contacts.other.phones.home': {
    type: 'input',
    label: 'Phone Number'
  },

  // OTHER CELLPHONE NUMBER
  'property.contacts.other.phones.cell': {
    type: 'input',
    label: 'Cellphone Number'
  },

  // INSTRUCTIONS
  instructions: {
    type: 'input',
    label: 'Instructions'
  },

  // ADDITIONAL COMMENTS
  additionalComments: {
    type: 'input',
    label: 'Additional Comments'
  },

  // STAFF NOTES
  staffNotes: {
    type: 'input',
    label: 'Staff Notes'
  }
});
