'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc constant
 * @name frontendApp.constant:ORDERS_SELECT_OPTIONS
 * @description
 *
 * Filter options for select fields that don't need to be loaded from the back end
 */
app.constant('APPRAISAL_SELECT_OPTIONS', {
  month: [
    {id: 1,  value: 'January'},
    {id: 2,  value: 'February'},
    {id: 3,  value: 'March'},
    {id: 4,  value: 'April'},
    {id: 5,  value: 'May'},
    {id: 6,  value: 'June'},
    {id: 7,  value: 'July'},
    {id: 8,  value: 'August'},
    {id: 9,  value: 'September'},
    {id: 10, value: 'October'},
    {id: 11, value: 'November'},
    {id: 12, value: 'December'}
  ],
  year: [
    {id: 2000, value: 2000},
    {id: 2001, value: 2001},
    {id: 2002, value: 2002},
    {id: 2003, value: 2003},
    {id: 2004, value: 2004},
    {id: 2005, value: 2005},
    {id: 2006, value: 2006},
    {id: 2007, value: 2007},
    {id: 2008, value: 2008},
    {id: 2009, value: 2009},
    {id: 2010, value: 2010},
    {id: 2011, value: 2011},
    {id: 2012, value: 2012},
    {id: 2013, value: 2013},
    {id: 2014, value: 2014},
    {id: 2015, value: 2015},
    {id: 2016, value: 2016},
    {id: 2017, value: 2017},
    {id: 2018, value: 2018},
    {id: 2018, value: 2019},
    {id: 2020, value: 2020}
  ],
  days: [
    {id: 30, value: '30 Days'},
    {id: 60, value: '30 Days'},
    {id: 90, value: '30 Days'}
  ],
  loanType: [
    {id: 'Refinance', value: 'Refinance'},
    {id: 'purchase',  value: 'Purchase'},
    {id: 'Other',     value: 'Other'}
  ],
  propertyTypes: [
    {option: 'single-family', label: 'Single Family'},
    {option: 'multi-family',  label: 'Multi Family'},
    {option: 'condominium',   label: 'Condominium'},
    {option: 'duplex',        label: 'Duplex'},
    {option: 'modular-home',  label: 'Modular Home'},
    {option: 'vacant-land',   label: 'Vacant Land'},
    {option: 'industrial',    label: 'Industrial'},
    {option: 'office',        label: 'Office'},
    {option: 'raw-land',      label: 'Raw Land'},
    {option: 'retail',        label: 'Retail'},
    {option: 'other',         label: 'Other'}
  ],
  paidStatus: [
    {id: '0,1',    value: 'Unpaid'},
    {id: '2',      value: 'Unpaid (Invoiced)'},
    {id: '3',      value: 'Paid (Credit Card)'},
    {id: 'check',  value: 'Paid (Check)'},
    {id: 'echeck', value: 'Paid (eCheck)'},
    {id: '5',      value: 'Unpaid (Authorized)'},
    {id: '6',      value: 'Unpaid (Invoice sent to borrower)'}
  ],
  status: [
    {id: 1,  value: 'New'},
    {id: 2,  value: 'Assigned'},
    {id: 3,  value: 'Accepted'},
    {id: 16, value: 'Accepted with Conditions'},
    {id: 4,  value: 'Declined'},
    {id: 9,  value: 'Inspection Scheduled'},
    {id: 13, value: 'Inspection Complete'},
    {id: 7,  value: 'Late'},
    {id: 10, value: 'On Hold'},
    {id: 11, value: 'Revision Sent'},
    {id: 14, value: 'Revision Received'},
    {id: 5,  value: 'Ready for Review'},
    {id: 12, value: 'Reviewed'},
    {id: 15, value: 'Request for bid'},
    {id: 6,  value: 'Completed'},
    {id: 8,  value: 'Canceled'}
  ]
});
