'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.service:AppraisalSelectOptionsService
 * @description
 *
 */
app.factory('AppraisalSelectOptionsService', [
  'APPRAISAL_SELECT_OPTIONS', 'SharedClientCompaniesService', 'SharedClientsOnReportService',

  function(APPRAISAL_SELECT_OPTIONS, SharedClientCompaniesService, SharedClientsOnReportService) {
    var CACHE_TTL = 900000;
    var cache = Immutable.Map();


    /**
     * Create a random expire time in a range around CACHE_TTL
     *
     * @returns {number}
     */
    function expireTime() {
      var max = Math.round(CACHE_TTL * 1.25);
      var min = Math.round(CACHE_TTL * 0.75);

      return Date.now() + Math.round(Math.random() * (max - min) + min);
    }

    var Service = {
      data: {
        propertyTypes: APPRAISAL_SELECT_OPTIONS.propertyTypes,
        clients: [],
        clientsOnReport: []
      },

      /**
       * All clients select options
       *
       * @returns {Array}
       */
      loadClients: function() {
        if (cache.get('clients.expiresAt', 0) < Date.now()) {
          var cacheData = Immutable.Map();

          SharedClientCompaniesService.getAll().then(function(result) {
            Lazy(result.data).each(function(client) {
              cacheData = cacheData.set(client.id, Immutable.fromJS({
                option: client.id,
                label: client.name
              }));
            });

            cache = cache.set('clients.data', cacheData)
                         .set('clients.expiresAt', expireTime());

            Service.data.clients = cacheData.toArray();
          });
        } else {
          Service.data.clients = cache.get('clients.data', []).toArray();
        }
      },

      /**
       * Load all select options when the page renders.
       */
      loadData: function() {
        Service.loadClients();
      },

      /**
       * All clients on report select options for a client
       *
       * @param clientId {*}
       */
      clientsOnReport: function(clientId) {
        console.log(clientId);
        if (typeof clientId === 'undefined' || !clientId) {
          return (Service.data.clientsOnReport = []);
        }

        if (cache.getIn(['clientsOnReport.expiresAt', clientId], 0) < Date.now()) {
          var cacheData = Immutable.Set();

          SharedClientsOnReportService.getAll(clientId).then(function(result) {
            Lazy(result.data).each(function(clientOnReport) {
              cacheData = cacheData.add(Immutable.fromJS({
                option: clientOnReport.companyId,
                label: cache.getIn(['clients.data', clientOnReport.companyId, 'label'], '')
              }));
            });

            cache = cache.setIn(['clientsOnReport.data', clientId], cacheData)
                         .setIn(['clientsOnReport.expiresAt', clientId], expireTime());

            Service.data.clientsOnReport = cacheData.toJS();
          });
        } else {
          Service.data.clientsOnReport = cache.getIn(['clientsOnReport.data', clientId], []).toJS();
        }
      }
    };

    return Service;
  }
]);
