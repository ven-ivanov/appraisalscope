'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.service:AppraisalOrderDetailsService
 * @description
 *
 */
app.factory('AppraisalOrderDetailsService', [

  function() {
    var Service = {

    };

    return {
      get: function() {
        return Service;
      }
    };
  }
]);
