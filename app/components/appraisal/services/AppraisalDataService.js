'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.service:AppraisalDataService
 * @description
 *
 */
app.factory('AppraisalDataService', [
  'SharedAppraisalApiService',

  function(SharedAppraisalApiService) {
    var CACHE_TTL = 600000;
    var cache     = {};
    var cacheMeta = [];

    function timeStamp() {
      return Date.now();
    }

    var Service = {

      /**
       * Insert appraisal into the cache
       * If the appraisal is not in the cache it will insert new object and return appraisalId
       * If the appraisal is in the cache it will update it without destroying the reference
       *
       * @param appraisal
       * @returns {Number}
       */
      insert: function(appraisal) {
        var appraisalId;

        if (typeof appraisal !== 'object' && Object.getPrototypeOf(appraisal) === Object.prototype) {
          throw Error('AppraisalCacheService: Appraisal for insert needs to be an object');
        }

        appraisalId = appraisal.id;

        // If this item is not already
        if (typeof cache[appraisalId] === 'undefined') {
          // Appraisal is not in the cache. Insert new
          cache[appraisalId]     = appraisal;
          cacheMeta[appraisalId] = {
            expires: timeStamp() + CACHE_TTL
          };

          return appraisalId;
        } else {
          // Update the cached object without loosing the reference
          angular.extend(cache[appraisalId], appraisal);

          // Update the expire time
          cacheMeta[appraisalId].expires = timeStamp() + CACHE_TTL;
          return null;
        }
      },

      /**
       * Insert multiple appraisals into cache
       *
       * @param {Array} appraisalCollection
       * @return {Array} List of appraisal IDs that were just inserted
       */
      insertCollection: function(appraisalCollection) {
        var appraisalIds = [];

        for (var key in appraisalCollection) {
          if (appraisalCollection.hasOwnProperty(key)) {
            Service.insert(appraisalCollection[key]);
            appraisalIds.push(appraisalCollection[key].id);
          }
        }

        return appraisalIds;
      },

      /**
       * Retrieve a single appraisal. DO NOT USE FOR FORMS!
       *
       * @param {Number} appraisalId
       * @return {Object}
       */
      get: function(appraisalId) {
        if (typeof cache[appraisalId] === 'undefined') {
          return Service.getFromServer(appraisalId);
        }

        return cache[appraisalId];
      },

      /**
       * Retrieve a collection of appraisals from an array of IDs
       *
       * @param {Array} appraisalIds
       * @return {Array}
       */
      getGroup: function(appraisalIds) {
        var appraisals = [];

        for (var key in appraisalIds) {
          if (appraisalIds.hasOwnProperty(key)) {
            appraisals.push(Service.get(appraisalIds[key]));
          }
        }

        return appraisals;
      },

      /**
       * If the appraisal is not in the cache get it from the server
       *
       * @param {Number} appraisalId
       */
      getFromServer: function(appraisalId) {
        SharedAppraisalApiService.get(appraisalId).then(function(result) {
          Service.insert(result);
        });
      },


      /**
       * Delete appraisal from cache
       *
       * @param {Number} appraisalId
       */
      delete: function(appraisalId) {
        if (typeof cache[appraisalId] !== 'undefined' && typeof cacheMeta[appraisalId] !== 'undefined') {
          delete cache[appraisalId];
          delete cacheMeta[appraisalId];

          return true;
        }

        return false;
      }
    };

    return Service;
  }
]);
