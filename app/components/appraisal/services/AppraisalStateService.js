'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.service:AppraisalStateService
 * @description
 */
app.factory('AppraisalStateService', [
  '$state', 'OrdersStateService',

  function($state, OrdersStateService) {
    var Service = {
      data: {
        previewMode: false,
        updateMasonry: null,
        displayTabs: {
          'order-details': true,
          'updates': true,
          'documents': true,
          'email-office': true,
          'notification-log': true,
          'qc-review': true,
          'request-revision': true
        }
      },

      /**
       * Controller triggered update of the page state
       */
      init: function() {
        var currentState = $state.current.name;

        if (currentState === 'main.orders') {
          Service.data.previewMode = true;
          Service.updateTabDisplay();
        } else if (currentState === 'main.appraisal') {
          Service.data.previewMode = false;

          // Temporary fix for showing all tabs on appraisal page
          for (var key in Service.data.displayTabs) {
            if (Service.data.displayTabs.hasOwnProperty(key)) {
              Service.data.displayTabs[key] = true;
            }
          }
        }
      },

      selectedAppraisal: function() {
        if (typeof $state.params.appraisalId !== 'undefined') {
          return $state.params.appraisalId;
        }

        return '';
      },

      /**
       * When a tab is changed update the display
       */
      updateTabDisplay: function() {
        var activeTab = OrdersStateService.activePreviewTab();

        if (activeTab === '') {
          return false;
        }

        // Hide all other tabs
        for (var key in Service.data.displayTabs) {
          if (Service.data.displayTabs.hasOwnProperty(key)) {
            Service.data.displayTabs[key] = false;
          }
        }

        Service.showTab(OrdersStateService.activePreviewTab());
      },

      /**
       * Change tab visibility
       *
       * @param tabId
       */
      showTab: function(tabId) {
        if (Service.data.displayTabs.hasOwnProperty(tabId)) {
          Service.data.displayTabs[tabId] = true;

          return true;
        }

        return false;
      },

      /**
       * Trigger update of Masonry. It is needed every tabe the size of one tab changes.
       */
      updateMasonry: function() {
        if (Service.data.updateMasonry === null || Service.data.previewMode === true) {
          return;
        }

        Service.data.updateMasonry();
      },

      /**
       * Allows the masonry directive to register it's callback for updates.
       *
       * @param callback
       */
      registerMasonryWatcher: function(callback) {
        Service.data.updateMasonry = callback;
      }
    };

    return Service;
  }
]);
