'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.controller:AppraisalMainCtrl
 * @description
 *
 */
app.controller('AppraisalMainCtrl', [
  '$scope', '$timeout', 'AppraisalStateService', 'OrdersStateService', 'AppraisalDataService',
  'AppraisalSelectOptionsService',

  function($scope, $timeout, AppraisalStateService, OrdersStateService, AppraisalDataService,
           AppraisalSelectOptionsService) {

    var MASONRY_UPDATE_DELAY  = 100;
    var GOOGLE_MAP_ZOOM_LEVEL = 18;
    var vm = this;

    AppraisalStateService.init();

    // For preview we need to rearrange some elements on the page
    vm.previewMode   = AppraisalStateService.data.previewMode;
    vm.displayTabs   = AppraisalStateService.data.displayTabs;
    vm.selectOptions = AppraisalSelectOptionsService;
    vm.mapOptions    = null;

    vm.selectOptions.loadData();

    vm.selectedContact = 'borrower';

    // If we are in preview mode watch for orders tab changes
    if (vm.previewMode) {
      $scope.$watch(OrdersStateService.activePreviewTab, function() {
        AppraisalStateService.updateTabDisplay();
      });
    }

    // Watch for appraisalId change and load the new appraisal data into the preview/details
    $scope.$watch(AppraisalStateService.selectedAppraisal, function (appraisalId) {
      vm.mapOptions = null;

      $timeout(function() {
        vm.appraisal = AppraisalDataService.get(appraisalId);

        if (vm.appraisal) {
          vm.selectOptions.clientsOnReport(vm.appraisal.client.id);

          console.log(vm.appraisal);

          vm.mapOptions = {
            center: new google.maps.LatLng(
              vm.appraisal.property.location.geo.latitude,
              vm.appraisal.property.location.geo.longitude
            ),
            zoom: GOOGLE_MAP_ZOOM_LEVEL
          };
        }

        AppraisalStateService.updateMasonry();
      }, AppraisalStateService.data.updateMasonry? MASONRY_UPDATE_DELAY: 0);
    });
  }
]);
