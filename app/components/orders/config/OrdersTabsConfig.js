'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc constant
 * @name frontendApp.constant:ORDERS_TABS_CONFIG
 * @description
 *
 * Configuration for each tab. It's id for URL, label to display to user and id of the summary number
 */
app.constant('ORDERS_TABS_CONFIG', {
  'all':               {label: 'All',               summaryId: 'all'},
  'all-open':          {label: 'All Open',          summaryId: 'open'},
  'pending':           {label: 'Pending',           summaryId: 'pending'},
  'completed':         {label: 'Completed',         summaryId: 'completed'},
  'canceled':          {label: 'Canceled',          summaryId: 'canceled'},
  'new':               {label: 'New',               summaryId: 'new'},
  'assigned':          {label: 'Assigned',          summaryId: 'assigned'},
  'unaccepted':        {label: 'Unaccepted',        summaryId: 'unaccepted'},
  'accepted':          {label: 'Accepted',          summaryId: 'accepted'},
  'scheduled':         {label: 'Scheduled',         summaryId: 'scheduled'},
  'due':               {label: 'Due',               summaryId: 'due'},
  'late':              {label: 'Late',              summaryId: 'late'},
  'on-hold':           {label: 'On Hold',           summaryId: 'onHold'},
  'updates':           {label: 'Updates',           summaryId: 'updates'},
  'ucdp':              {label: 'UCDP',              summaryId: 'ucdp'},
  'bids':              {label: 'Bids',              summaryId: 'bids'},
  'uw-conditions':     {label: 'UW Conditions',     summaryId: 'uwConditions'},
  'revision-sent':     {label: 'Revision Sent',     summaryId: 'revisionSent'},
  'revision-received': {label: 'Revision Received', summaryId: 'revisionReceived'},
  'ready-for-review':  {label: 'Ready for Review',  summaryId: 'readyForReview'},
  'reviewed':          {label: 'Reviewed',          summaryId: 'reviewed'}
});
