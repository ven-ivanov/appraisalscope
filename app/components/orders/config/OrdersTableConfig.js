'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc constant
 * @name frontendApp.constant:ORDERS_TABLE_CONFIG
 * @description
 *
 * Definition for each table column
 */
app.constant('ORDERS_TABLE_CONFIG', {
  rowNumber: {
    header: {
      label: 'Other'
    },
    row: {
      template: 'rowNumber.html'
    }
  },
  fileNumber: {
    header: {
      type:   'input',
      label:  'File#',
      model:  'searchCtrl.filters.file'
    },
    row: {
      display: '::appraisal.file'
    },
    sort: 'file'
  },
  loanNumber: {
    header: {
      label: 'Loan#'
    },
    row: {
      display: '::appraisal.loan.number'
    },
    sort: 'loan.number'
  },
  borrower: {
    header: {
      label: 'Borrower'
    },
    row: {
      display: '::appraisal.property.contacts.borrower.fullName'
    },
    sort: 'property.contacts.borrower.fullName'
  },
  address: {
    header: {
      label: 'address'
    },
    row: {
      display: '::appraisal.property.location.address1'
    },
    sort: 'property.location.address'
  },
  city: {
    header: {
      label: 'City'
    },
    row: {
      display: '::appraisal.property.location.city'
    },
    sort: 'property.location.city'
  },
  state: {
    header: {
      label: 'State'
    },
    row: {
      display: '::appraisal.property.location.state.name'
    },
    sort: 'property.location.state.name'
  },
  assigned: {
    header: {
      label: 'Assigned To'
    },
    row: {
      display: '::appraisal.assignment.assignee.fullName'
    },
    sort: 'assignment.appraiser.name'
  },
  client: {
    header: {
      label: 'Client'
    },
    row: {
      display: '::appraisal.client.name'
    },
    sort: 'client.name'
  },
  clientOnReport: {
    header: {
      label: 'Client On Report'
    },
    row: {
      display: '::appraisal.clientOnReport.company.name'
    },
    sort: 'clientOnReport.name'
  },
  dateOrdered: {
    header: {
      label: 'Order Date'
    },
    row: {
      display: '::appraisal.createdAt | asdate'
    },
    sort: 'createdAt'
  },
  dueDate: {
    header: {
      label: 'Due Date'
    },
    row: {
      display: '::appraisal.dueDate.client | asdate'
    },
    sort: 'dueDate.client'
  },
  status: {
    header: {
      label: 'Status'
    },
    row: {
      template: 'rowStatus.html'
    },
    sort: 'status'
  },
  queue: {
    header: {
      label: 'Queue'
    },
    row: {
      display: '::appraisal.status.value'
    },
    sort: 'status'
  },
  lastUpdate: {
    header: {
      label: 'Last Update'
    },
    row: {
      display: '::appraisal.customStatus'
    },
    sort: 'customStatus'
  },
  lastUpdateTime: {
    header: {
      label: 'Last Update Time'
    },
    row: {
      display: '::appraisal.updatedAt | asdate'
    },
    sort: 'updatedAt'
  },
  staff: {
    header: {
      label: 'Staff'
    },
    row: {
      display: '::appraisal.staff.name'
    },
    sort: 'staff.name'
  },
  referenceNumber: {
    header: {
      label: 'Reference#'
    },
    row: {
      display: '::appraisal.loan.reference'
    }
  },
  // @TODO Missing API. Don't forget to update sorting
  county: {
    header: {
      label: 'County'
    },
    row: {
      display: ''
    }
  },
  jobType: {
    header: {
      label: 'Job Type'
    },
    row: {
      display: '::appraisal.jobType.label'
    }
  },
  assignedDate: {
    header: {
      label: 'Assigned At'
    },
    row: {
      display: '::appraisal.assignment.assignedAt'
    },
    sort: 'assignment.assignedAt'
  },
  inspectionDate: {
    header: {
      label: 'Inspection Date'
    },
    row: {
      display: '::appraisal.inspection.startedAt | asdate'
    },
    sort: 'inspection.startedAt'
  },
  ecd: {
    header: {
      label: 'E.C.D.'
    },
    row: {
      display: '::appraisal.inspection.estimatedCompletionDate | asdate'
    },
    sort: 'inspection.ecd'
  },
  reason: {
    header: {
      label: 'Reason'
    },
    row: {
      template: 'rowReason.html'
    },
    sort: 'reason'
  },
  hours: {
    header: {
      label: 'Hours'
    },
    row: {

    }
  },
  reviewer: {
    header: {
      label: 'Reviewer'
    },
    row: {
      display: '::appraisal.reviewer.firstName +\' \'+ appraisal.reviewer.lastName'
    }
  },

  // Message tab fields
  messageSubmittedOn: {
    header: {
      label: 'Subbmited'
    },
    row: {
      display: ''
    }
  },

  // UW Tab fields
  uwRevisionSubmitted: {
    header: {
      label: 'Submitted'
    },
    row: {
      display: '::appraisal.revision.createdAt | asdate'
    }
  },
  uwRevisionType: {
    header: {
      label: 'Type'
    },
    row: {
      // @TODO Not sure about this one.
      display: '::appraisal.revision.content.name'
    }
  },

  // Revision tabs
  revisionRead: {
    header: {
      label: 'Revision Read'
    },
    row: {
      display: '::appraisal.assignment.revision.readAt | asdate'
    }
  },
  revisionUploaded: {
    header: {
      label: 'Uploaded'
    },
    row: {
      display: '::appraisal.assignment.revision.uploadedAt | asdate'
    }
  },
  revisionSubmitted: {
    header: {
      label: 'Revision submitted'
    },
    row: {
      display: '::appraisal.assignment.revision.createdAt | asdate'
    }
  },
  revisionType: {
    header: {
      label: 'Revision Type'
    },
    row: {
      display: '::appraisal.assignment.revision.type'
    }
  },
  reviewedDate: {
    header: {
      label: 'Reviewed Date'
    },
    row: {
      display: '::appraisal.reviewedAt | asdate'
    }
  },

  // UCDP Tab columns
  ucdpLender: {
    header: {
      label: 'Lender Name'
    },
    row: {
      display: '::appraisal.ucdpSubmission.lender'
    },
    sort: 'ucdpSubmission.lender'
  },
  ucdpOrderId: {
    header: {
      label: 'UCDP Order Id'
    },
    row: {
      display: '::appraisal.ucdpSubmission.orderId'
    },
    sort: 'ucdpSubmission.orderId'
  },
  ucdpSubmittedOn: {
    header: {
      label: 'Submitted On'
    },
    row: {
      display: '::appraisal.ucdpSubmission.submittedAt | asdate'
    },
    sort: 'ucdpSubmission.submittedAt'
  },
  ucdpLoanStatus: {
    header: {
      label: 'Loan Status'
    },
    row: {
      display: '::appraisal.ucdpSubmission.status'
    },
    sort: 'ucdpSubmission.status'
  },
  ucdpFannieMae: {
    header: {
      label: 'Fannie Mae'
    },
    // @TODO Needs a link
    row: {
      display: '::appraisal.ucdpSubmission.documents.fannieMae.file'
    }
  },
  ucdpFreddieMac: {
    header: {
      label: 'Freddie Mac'
    },
    // @TODO Needs a link
    row: {
      display: '::appraisal.ucdpSubmission.documents.freddieMac.file'
    }
  },

  // Bids tab fields
  totalBids: {
    header: {
      label: 'Totals Bids'
    },
    row: {
      display: '::appraisal.bidSummary.total'
    }
  },
  lowestBid: {
    header: {
      label: 'Lowest Bid Amount'
    },
    row: {
      display: '::appraisal.bidSummary.lowest'
    }
  },


  // Tab specific actions
  messagesAction: {
    header: {
      label: 'Action'
    },
    row: {
      template: 'rowMessagesAction.html'
    }
  },
  updatesAction: {
    header: {
      label: 'Action'
    },
    row: {

    }
  },
  ucdpAction: {
    header: {
      label: 'Action'
    },
    row: {

    }
  },
  bidAppraisers: {
    header: {
      label: 'Appraisers'
    },
    row: {

    }
  },
  uwAction: {
    header: {
      label: 'Action'
    },
    row: {

    }
  },
  revisionSentAction: {
    header: {
      label: 'Action'
    },
    row: {

    }
  },
  revisionReceivedAction: {
    header: {
      label: 'Action'
    },
    row: {

    }
  },
  ReadyForReviewerAction: {
    header: {
      label: 'Action'
    },
    row: {

    }
  },
  reviewedAction: {
    header: {
      label: 'Action'
    },
    row: {

    }
  }
});
