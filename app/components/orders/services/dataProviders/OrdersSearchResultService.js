'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.service:OrdersSearchResultService
 * @description
 *
 *
 */
app.factory('OrdersSearchResultService', [
  '$q', '$window', 'SharedAppraisalApiService', 'OrdersStateService', 'AppraisalQueuesApiService',
  'AppraisalDataService', 'OrdersConfigService',

  function($q, $window, SharedAppraisalApiService, OrdersStateService, AppraisalQueuesApiService,
           AppraisalDataService, OrdersConfigService) {

    var cache = Immutable.Map();

    /**
     * Return null if item is undefined, empty string or empty object
     *
     * @param {*} item
     */
    function checkEmpty(item) {
      if (typeof item === 'undefined') {
        return null;
      }

      if (item === '') {
        return null;
      }

      if (item !== null && typeof item === 'object' && Object.keys(item).length === 0) {
        return null;
      }

      return item;
    }

    /**
     * Remove keys from object that have null value
     *
     * @param {object} object
     * @returns {object|null}
     */
    function removeNullFromObject(object) {
      var childObject;

      for (var key in object) {
        if (!object.hasOwnProperty(key)) {
          continue;
        }

        childObject = object[key];

        if (typeof childObject === 'object') {
          childObject = removeNullFromObject(childObject);
        }

        if (checkEmpty(childObject) === null) {
          delete object[key];
        }
      }

      return checkEmpty(object);
    }

    /**
     * Create correctly formatted object for sending a request to the back end
     *
     * @param {number} page
     * @param {string} orderBy
     * @param {bool} orderDirection
     * @param {object} filters
     */
    function appraisalRequestObject(page, orderBy, orderDirection, filters) {
      filters = removeNullFromObject(filters);

      // Avoid errors if there are no filters
      if (filters === null) {
        filters = {};
      }

      var requestObject = {
        orderBy: checkEmpty(orderBy),
        orderDirection: checkEmpty(orderDirection),
        page: page,
        filter: {
          fha: null,
          dueDate: {
            client: null
          },
          completedAt: null,
          completedAtRange: {
            from: null,
            to: null
          },
          completedAtSince: {
            days: null,
            year: null,
            month: null
          },
          createdAt: null,
          createdAtRange: {
            from: null,
            to: null
          },
          createdAtSince: {
            days: null,
            year: null,
            month: null
          },
          owner: {
            fullName: null
          },
          creator: {
            fullName: null
          },
          file: null,
          property: {
            contacts: {
              borrower: {
                fullName: null
              }
            },
            location: {
              address: null,
              city: null,
              state: {
                code: null
              },
              zip: null,
              county: null
            },
            type: null
          },
          loan: {
            number: null,
            type: null,
            reference: null
          },
          client: {
            id: null,
            name: null
          },
          customStatus: null,
          payment: {
            receivable: {
              paidAt: null,
              fee: null
            },
            payable: {
              fee: null
            },
            fee: {
              management: null
            },
            state: null
          },
          jobType: {
            id: null
          },
          clientOnReport: {
            id: null
          },
          staff: {
            id: null
          },
          reviewer: {
            id: null
          },
          inspection: {
            startedAt: null,
            ecd: null
          },
          assignment: {
            assignedAt: null,
            acceptedAt: null,
            appraiser: {
              fullName: null
            }
          }
        }
      };

      return removeNullFromObject(requestObject);
    }

    var Service = {
      displayData: {
        appraisals: []
      },
      safeData: {
        appraisals: {},
        pagination: {}
      },
      requestData: {
        page: 1,
        orderBy: null,
        orderDirection: null,
        filters: {}
      },

      /**
       * When tab changes we need to load everything.
       * From cache or from the server
       */
      loadTabResults: function() {
        var activeTab  = OrdersStateService.activeTab();
        var cachedData = Service.getCached(activeTab);
        var oldLength  = Service.displayData.appraisals.length;

        Service.displayData.pagination = {};
        Service.displayData.appraisals = [];

        // Fake table with empty rows while we switch tab
        for (var i = 0; i < oldLength; i ++) {
          Service.displayData.appraisals.push({});
        }

        if (cachedData) {
          Service.requestData = cachedData.requestData;

          if (cachedData.displayData) {
            // Set items from cache without breaking the displayData reference
            Service.displayData.appraisals = cachedData.displayData.appraisals;
            Service.displayData.pagination = cachedData.displayData.pagination;
          } else {
            Service.getDataFromServer();
          }
        } else {
          Service.getDataFromServer();
        }

        AppraisalDataService.insertCollection(Service.displayData.appraisals);
      },

      /**
       * Request new set of data from the server.
       */
      getDataFromServer: function() {
        var appraisal;
        var activeTab     = OrdersStateService.activeTab();
        var reqData       = Service.requestData;

        var requestObject = appraisalRequestObject(
          reqData.page, reqData.orderBy, reqData.orderDirection, reqData.filters
        );

        Service.selectEndpoint(activeTab, requestObject).then(function(result){
          Service.displayData.pagination = result.meta.pagination;

          Service.displayData.appraisals = [];

          Lazy(result.data).each(function(dataRow, itemIndex) {
            // Some tabs require some transformation of data
            if (activeTab === 'messages') {
              appraisal = dataRow.appraisal;
              appraisal.message = dataRow.message;
            } else if (activeTab === 'updates') {
              appraisal = dataRow.appraisal;
              appraisal.update = dataRow.update;
            } else {
              appraisal = dataRow;
            }

            appraisal.index   = (Service.displayData.pagination.perPage * (Service.displayData.pagination.page - 1)) + itemIndex + 1;
            appraisal.status  = Service.getStatusById(appraisal.status);

            Service.displayData.appraisals.push(appraisal);
          });

          AppraisalDataService.insertCollection(Service.displayData.appraisals);

          Service.saveToCache(activeTab);
        });
      },

      /**
       * Get descriptive status object by Id
       *
       * @param id
       * @returns {object}
       */
      getStatusById: function(id) {
        var statusOptions = OrdersConfigService.selectOptions('status');
        var filteredSet = statusOptions.filter(function(current) {
          return (current.id === id);
        });
        return filteredSet[0];
      },

      /**
       * Select endpoint for current tab.
       *
       * @param activeTab
       * @param requestObject
       * @returns {object}
       */
      selectEndpoint: function(activeTab, requestObject) {
        requestObject.include = 'clientOnReport,amc';

        switch(activeTab) {
          case 'all-open':
            return AppraisalQueuesApiService.getOpen(requestObject);
          case 'new':
            return AppraisalQueuesApiService.getNew(requestObject);
          case 'assigned':
            return AppraisalQueuesApiService.getAssigned(requestObject);
          case 'unaccepted':
            return AppraisalQueuesApiService.getUnaccepted(requestObject);
          case 'accepted':
            return AppraisalQueuesApiService.getAccepted(requestObject);
          case 'scheduled':
            return AppraisalQueuesApiService.getScheduled(requestObject);
          case 'due':
            return AppraisalQueuesApiService.getDue(requestObject);
          case 'late':
            return AppraisalQueuesApiService.getLate(requestObject);
          case 'on-hold':
            return AppraisalQueuesApiService.getOnHold(requestObject);
          case 'messages':
            return AppraisalQueuesApiService.getMessages(requestObject);
          case 'updates':
            return AppraisalQueuesApiService.getUpdates(requestObject);
          case 'ucdp':
            return AppraisalQueuesApiService.getUCDP(requestObject);
          case 'bids':
            return AppraisalQueuesApiService.getBids(requestObject);
          case 'uw-conditions':
            return AppraisalQueuesApiService.getUwConditions(requestObject);
          case 'revision-sent':
            return AppraisalQueuesApiService.getRevisionSent(requestObject);
          case 'revision-received':
            return AppraisalQueuesApiService.getRevisionReceived(requestObject);
          case 'ready-for-review':
            return AppraisalQueuesApiService.getReadyForReview(requestObject);
          case 'reviewed':
            return AppraisalQueuesApiService.getReviewed(requestObject);
          // @TODO Missing API
          case 'canceled':
          case 'completed':
          case 'pending':

          case 'all':
          case 'super-search':
            return SharedAppraisalApiService.getAll(requestObject);
        }
      },

      /**
       * Set filters and reload the data
       *
       * @param {object} filters
       */
      setFilters: function(filters) {
        Service.requestData.filters        = filters;
        Service.requestData.orderBy        = null;
        Service.requestData.orderDirection = null;
        Service.requestData.page           = 1;

        Service.getDataFromServer();
      },
      /**
       * Change page and reload the data
       *
       * @param {number} page
       */
      setPage: function(page) {
        Service.requestData.page = page;
        Service.getDataFromServer();
      },

      /**
       * Change sorting filed and direction
       *
       * @param orderBy
       * @return {string}
       */
      setSort: function(orderBy) {
        if (Service.requestData.orderBy === orderBy) {
          if (Service.requestData.orderDirection === 'ASC') {
              Service.requestData.orderDirection = 'DESC';
            } else {
              Service.requestData.orderBy = null;
              Service.requestData.orderDirection = null;
            }
        } else {
          Service.requestData.orderBy = orderBy;
          Service.requestData.orderDirection  = 'ASC';
        }

        Service.requestData.page = 1;
        Service.getDataFromServer();

        return Service.requestData.orderBy;
      },

      /**
       * Retrieve cached result set
       *
       * @param activeTab
       * @returns {object}
       */
      getCached: function(activeTab) {
        var cacheData = cache.get(activeTab);

        if (cacheData) {
          return cacheData.toJS();
        }

        return null;
      },

      /**
       * Save results and request data to cache for tab.
       * It is faster to manage each tabs data individually instead of
       *
       * @param activeTab
       */
      saveToCache: function(activeTab) {
        cache.set(activeTab, {
          displayData: Service.displayData,
          requestData: Service.requestData
        });
      }
    };

    return Service;
  }
]);
