'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.service:OrdersConfigService
 * @description
 *
 *
 */
app.factory('OrdersConfigService', [
  'SessionService', 'ORDERS_PAGE_CONFIG', 'ORDERS_TABS_CONFIG', 'ORDERS_TABLE_CONFIG', 'APPRAISAL_SELECT_OPTIONS',

  function(SessionService, ORDERS_PAGE_CONFIG, ORDERS_TABS_CONFIG, ORDERS_TABLE_CONFIG, APPRAISAL_SELECT_OPTIONS) {

    /**
     * Get specific key in the page config
     *
     * @param configKey
     * @returns {object}
     */
    function getPageConfig(configKey) {
      if (typeof SessionService.user === 'undefined') {
        return null;
      }

      var config = ORDERS_PAGE_CONFIG[SessionService.user.typeName];
      if (typeof config === 'undefined') {
        console.log('OrdersConfigService: No config found for current user. User type: '+ SessionService.user.typeName);
        return null;
      }

      if (typeof config[configKey] === 'undefined') {
        console.log('OrdersConfigService: Invalid config key: '+ configKey);
        return null;
      }

      return config[configKey];
    }

    var Service = {
      /**
       * Get all table columns configs
       * @returns {Object}
       */
      tableColumns: function() {
        return getPageConfig('tableColumns');
      },

      /**
       * Get table columns configuration for a specific tab for the current user type
       *
       * @param tabId {string}
       * @returns {object}
       */
      tableColumnsForTab: function(tabId) {
        var tableColumnsConfig = getPageConfig('tableColumns');

        if (typeof tableColumnsConfig[tabId] === 'undefined') {
          console.log('OrdersConfigService: No table column config found for tabId: '+ tabId);
          return null;
        }

        return tableColumnsConfig[tabId];
      },

      /**
       * Get tab configuration for the current user type
       */
      tabs: function() {
        return getPageConfig('tabs');
      },

      /**
       * Retrieve configuration for a specific tab
       *
       * @param tabId
       */
      tabConfig: function(tabId) {
        if (typeof ORDERS_TABS_CONFIG[tabId] === 'undefined') {
          console.log('OrdersConfigService: No tab config found for tabId '+ tabId);
          return {};
        }

        return ORDERS_TABS_CONFIG[tabId];
      },

      /**
       * Get options for a table column.
       *
       * @param {string} columnId
       * @returns {object}
       */
      tableColumnConfig: function(columnId) {
        if (typeof ORDERS_TABLE_CONFIG[columnId] === 'undefined') {
          console.log('OrdersConfigService: No table column configuration for column id: '+ columnId);
          return null;
        }

        return ORDERS_TABLE_CONFIG[columnId];
      },

      /**
       * Give a list
       *
       * @param {string} optionId
       */
      selectOptions: function(optionId) {
        if (typeof APPRAISAL_SELECT_OPTIONS[optionId] === 'undefined') {
          console.log('OrdersConfigService: No select option for id: '+ optionId);
          return null;
        }

        return APPRAISAL_SELECT_OPTIONS[optionId];
      }
    };

    return Service;
  }
]);
