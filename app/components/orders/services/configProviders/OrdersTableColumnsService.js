'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.service:OrdersTableColumnsService
 * @description
 *
 *
 */
app.factory('OrdersTableColumnsService', [
  'SessionService', 'OrdersConfigService', 'OrdersStateService',

  function(SessionService, OrdersConfigService, OrdersStateService) {

    var Service= {
      displayData: {
        tableColumns: []
      },

      /**
       * Create the correct table configuration and trigger angular in order to display
       * the table to the user.
       */
      updateColumns: function() {
        var columnConfig, columnOptions;
        var activeTab      = OrdersStateService.activeTab();
        var tabTableConfig = OrdersConfigService.tableColumnsForTab(activeTab);

        // Reset
        Service.displayData.tableColumns.length = 0;

        for (var columnId in tabTableConfig) {
          columnOptions = tabTableConfig[columnId];
          columnConfig  = OrdersConfigService.tableColumnConfig(columnId);

          if (columnConfig === null || columnOptions.display === false) {
            continue;
          }

          // Allow column options to overwrite the sort
          if (typeof columnOptions.sort !== 'undefined') {
            columnConfig.sort = columnOptions.sort;
          }

          Service.displayData.tableColumns.push(
            columnConfig
          );
        }

      }
    };

    return Service;
  }
]);
