'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.service:OrdersTabsService
 * @description
 *
 *
 */
app.factory('OrdersTabsService', [
  '$window', 'AppraisalQueuesApiService', 'OrdersConfigService',

  function($window, AppraisalQueuesApiService, OrdersConfigService) {
    var storage = $window.localStorage;

    var RELOAD_TIMEOUT = 600000;
    var CACHE_KEY      = 'ordersTabsSummary';

    var Service = {
      displayData: {
        tabs: []
      },
      counters: {
        data: {},
        cacheExpire: 0
      },

      /**
       * Set the correct tabs configuration
       */
      updateTabs: function() {
        // On first page load if there is no data try loading the cache version
        if (Service.counters.cacheExpire === 0) {
          Service.counters = Service.loadFromCache();
        }

        // Trigger reload. It will not send a request if the timeout period hasn't expired
        Service.loadCounters();

        var tabConfig, tabSummary;
        var userTabsConfig = OrdersConfigService.tabs();

        // Remove old tabs from the array without destroying the reference
        Service.displayData.tabs.length = 0;

        // Convert the configuration arrays into something angular can display to the user
        for (var key in userTabsConfig) {
          tabConfig  = OrdersConfigService.tabConfig(userTabsConfig[key]);
          // If tab summary is set to false then there is no summary to be displayed
          tabSummary = tabConfig.summaryId !== false? Service.counters.data[tabConfig.summaryId]: '';

          Service.displayData.tabs.push({
            id: userTabsConfig[key],
            label: tabConfig.label,
            // tabSummary undefined means we are still waiting for the back end. Display 0 while we wait
            counter: typeof tabSummary !== 'undefined'? tabSummary: 0
          });
        }
      },

      /**
       * Checks to see if counter data is present and loads it if necessary. Also reload the data
       * after predefined set of time.
       *
       * It will trigger updateTabs() once the new data is loaded
       */
      loadCounters: function() {
        if (Service.counters.cacheExpire < Date.now() || Service.counters.data === null) {
          AppraisalQueuesApiService.getSummary().then(function(result) {
            Service.counters.cacheExpire = Date.now() + RELOAD_TIMEOUT;
            Service.counters.data        = result;

            Service.updateTabs();
            Service.saveToCache();
          });
        }
      },

      /**
       * Load counter values from the cache.
       *
       * @returns {object}
       */
      loadFromCache: function() {
        var data = storage.getItem(CACHE_KEY);

        if (typeof data === 'string') {
          return JSON.parse(data);
        }

        // If there is nothing in the cache return the old object
        return Service.counters;
      },

      /**
       * Save counters data into cache to avoid useless http requests
       */
      saveToCache: function() {
        storage.setItem(CACHE_KEY, JSON.stringify(Service.counters));
      }
    };

    Service.updateTabs();

    return Service;
  }
]);
