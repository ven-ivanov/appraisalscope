'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.service:OrdersUrlService
 * @description
 *
 *
 */
app.factory('OrdersStateService', [
  '$window', '$state', 'SessionService',

  function($window, $state, SessionService) {
    var user    = SessionService.user;
    var storage = $window.localStorage;
    var storageKey = 'ordersUrlParams';
    var previewTabCache = {};

    var Service = {
      params: {
        activeTab: null,
        appraisalId: null,
        activePreviewTab: null
      },

      /**
       * Update active tab param, update url and save the value to cache.
       *
       * @param param
       * @param value
       * @returns {boolean}
       */
      setParam: function(param, value) {
        if(typeof value === 'string') {
          Service.params[param] = value;

          Service.saveParamsToCache();

          Service.updateUrl();

          return true;
        }

        console.log('SharedOrdersUrlService: Invalid value provided for update. Value: ', param, value);
        return false;
      },

      /**
       * Retrieve one of the state parameters.
       *
       * @param {string} param
       * @returns {string}
       */
      getParamValue: function(param) {
        var cache;
        var value = '';

        if (Service.params[param] !== null) {
          // Sometimes the URL will not get updated. Force the update.
          Service.updateUrl();

          return Service.params[param];
        }

        cache = Service.getCachedParams();

        switch (true) {
          case typeof $state.params[param] !== 'undefined' && $state.params[param] !== null:
            value = $state.params[param];
            break;

          case typeof cache !== 'undefined' && typeof cache[param] !== 'undefined' && cache[param] !== '':
            value = cache[param];
            break;

          case param === 'activeTab' && (value === null || value === ''):
            value = Service.defaultActiveTab();
            break;
        }

        if (Service.params[param] !== value) {
          Service.setParam(param, value);
        }

        return value;
      },

      /**
       * Get current active tab.
       * @returns {string}
       */
      activeTab: function() {
        return Service.getParamValue('activeTab');
      },

      /**
       * Set current active tab
       *
       * @param {string} activeTab
       */
      setActiveTab: function(activeTab) {
        Service.setParam('activeTab', activeTab);
      },

      /**
       * Get currently selected appraisal
       *
       * @returns {string}
       */
      selectedAppraisal: function() {
        return Service.getParamValue('appraisalId');
      },

      /**
       * Set current selected appraisal
       *
       * @param {string} appraisalId
       */
      setSelectedAppraisal: function(appraisalId) {
        // If the appraisal is set to empty then remove the preview tab from the url
        if (appraisalId !== '') {
          Service.params.appraisalId = appraisalId;

          // if an appraisal is selected show the last tab user was on
          if (typeof previewTabCache[appraisalId] === 'undefined') {
            Service.setActivePreviewTab('order-details');
          } else {
            Service.setActivePreviewTab(previewTabCache[appraisalId]);
          }
        } else {
          Service.setParam('appraisalId', appraisalId);
        }
      },

      /**
       * Get current preview tab
       *
       * @returns {*|string}
       */
      activePreviewTab: function() {
        return Service.getParamValue('activePreviewTab');
      },

      /**
       * Update current preview tab.
       *
       * @param activePreviewTab
       */
      setActivePreviewTab: function(activePreviewTab) {
        Service.setParam('activePreviewTab', activePreviewTab);

        previewTabCache[Service.selectedAppraisal()] = activePreviewTab;
      },

      /**
       * Put all the Service.params into the URL bar
       */
      updateUrl: function() {
        var params = Service.params;

        // Set preview tab to empty if appraisalId is empty. Fixes performance when closing the preview
        if (params.appraisalId === '') {
          params.activePreviewTab = '';
        }

        // If the vars in memory are the same as the ones in the URL don't do anything
        if ((params.activeTab === null || params.activeTab === $state.params.activeTab) &&
            (params.appraisalId === null || params.appraisalId === $state.params.appraisalId) &&
            (params.activePreviewTab === null || params.activePreviewTab === $state.params.activePreviewTab)) {

          return null;
        }

        // Prevent triggering of state change if we are already in transition. Fixes a bug with changing main tabs
        if ($state.transition === null) {
          $state.transitionTo($state.current.name, params, {notify: false});
        }
      },

      /**
       * Get the default active tab for the current user type
       *
       * @returns {string}
       */
      defaultActiveTab: function() {
        if (user.isAdmin()) {
          return 'all';
        }

        if (user.isClient()) {
          return 'client';
        }

        console.log('SharedOrdersUrlService: Invalid user type. Unable to determine default active tab');
        return null;
      },

      /**
       * Save Service.params to cache so that the page state can be restored when switching main tabs.
       */
      saveParamsToCache: function() {
        storage.setItem(storageKey, JSON.stringify(Service.params));
      },

      /**
       * Retrieve cached params.
       * This function does not set the Service.params value. It only returns the object from cache
       */
      getCachedParams: function() {
        var data = storage.getItem(storageKey);

        if (typeof data === 'string') {
          return JSON.parse(data);
        }
      }
    };

    return Service;
  }
]);
