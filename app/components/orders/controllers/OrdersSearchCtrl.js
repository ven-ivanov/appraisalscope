'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.controller:OrdersSearchCtrl
 * @description
 *
 */
app.controller('OrdersSearchCtrl', [
  '$scope', 'OrdersStateService', 'OrdersTableColumnsService', 'OrdersSearchResultService',

  function($scope, OrdersStateService, OrdersTableColumnsService, OrdersSearchResultService) {
    var vm = this;

    vm.tableColumns = OrdersTableColumnsService.displayData.tableColumns;
    vm.tableData    = OrdersSearchResultService.displayData;
    vm.activeSort   = {};
    vm.filters      = {};

    /**
     * Change the page
     *
     * @param {number} page
     */
    vm.changePage = function(page) {
      OrdersSearchResultService.setPage(page);
    };

    /**
     * Change sorting order for results
     *
     * @param {string} sortId
     */
    vm.updateSort = function(sortId) {
      vm.activeSort = OrdersSearchResultService.setSort(sortId);
    };

    /**
     * Watch active tab and update the page if it changes.
     */
    $scope.$watch(OrdersStateService.activeTab, function() {
      OrdersTableColumnsService.updateColumns();
      OrdersSearchResultService.loadTabResults();

      vm.activeSort = OrdersSearchResultService.requestData.orderBy;
      vm.filters    = OrdersSearchResultService.requestData.filters;
    });

    $scope.$watch(function() { return vm.filters; }, function(filters) {
      OrdersSearchResultService.setFilters(filters);
    }, true);
  }
]);
