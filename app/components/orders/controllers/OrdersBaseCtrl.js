'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.controller:OrdersBaseCtrl
 * @description
 * # AdminOrdersCtrl
 *
 * Main controller for Orders page.
 */
app.controller('OrdersBaseCtrl', [
    '$scope', 'OrdersStateService', 'OrdersTabsService',

    function($scope, OrdersStateService, OrdersTabsService) {
      var vm = this;

      /**
       * Reconfigure the page to display data for the current tab
       */
      function updatePageData() {
        vm.activeTab         = OrdersStateService.activeTab();
        vm.selectedAppraisal = OrdersStateService.selectedAppraisal();
        vm.activePreviewTab  = OrdersStateService.activePreviewTab();
      }


      vm.tabs = OrdersTabsService.displayData.tabs;

      /**
       * Change selected queue tab.
       *
       * @param {string} activeTab
       */
      vm.changeTab = function(activeTab) {
        OrdersStateService.setActiveTab(activeTab);

        updatePageData();
      };

      /**
       * Change the selected appraisal.
       */
      vm.selectAppraisal = function(appraisalId) {
        OrdersStateService.setSelectedAppraisal(appraisalId);

        updatePageData();
      };

      /**
       * Change the current preview tab.
       *
       * @param activePreviewTab
       */
      vm.changePreviewTab = function(activePreviewTab) {
        OrdersStateService.setActivePreviewTab(activePreviewTab);

        updatePageData();
      };

      // Only run on view load
      updatePageData();
    }
  ]
);
