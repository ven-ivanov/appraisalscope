'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:modal
 * @description
 * Wrap provided html in bootrap modal
 *
 * Usage:
 * <modal title="">...</modal>
 */
angular.module('frontendApp')
  .directive('acceptAction', ['$compile', 'ModalInheritanceService', function ($compile, ModalInheritanceService) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {

        element.bind('click', function(e) {
          e.stopPropagation();

          if (attrs) {
          }

          ModalInheritanceService.inheritModal.call(this, scope, element, attrs);

          scope.appraisal = scope.$parent.appraisal;

          scope.showModal('row-action-confirmation', true);
        });
      }
    };
  }]);
