'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.directive:OrdersFormFields
 * @description
 *
 * Used by the table to generate header form fields.
 */
app.directive('ordersFormFields', [
  '$compile', 'SharedElementGeneratorService',

  function($compile, SharedElementGeneratorService) {

    return {
      scope: {
        fieldInfo: '='
      },
      link: function (scope, element) {
        var inputField = scope.fieldInfo;
        var html;

        // Generate a form field from the configuration object
        switch(inputField.type) {
          case 'input':
            html = SharedElementGeneratorService.input({
              'type': 'text',
              'class': 'form-control',
              'ng-model': inputField.model
            });
            break;
          case 'select':
            SharedElementGeneratorService.select('', {
              'class': 'form-control',
              'ng-model': inputField.model,
              'ng-options': inputField.options,
              'as-select': ''
            });
            break;
          case 'checkbox':
            html = SharedElementGeneratorService.input({
              'type': 'checkbox',
              'class': 'form-control',
              'ng-model': inputField.model,
              'checkable': ''
            });
            break;
        }

        if (typeof html !== 'undefined') {
          html = angular.element(html);

          $compile(html)(scope);

          element.html(html);
        }
      }
    };
  }
]);
