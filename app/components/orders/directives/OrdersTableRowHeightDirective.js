'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.directive:ordersTableRowConstantHeight
 * @description
 *
 * Forces table rows to be constant height
 */
app.directive('ordersTableRowConstantHeight', [
  function() {
    return {
      scope: {
        ordersTableData: '='
      },
      link: function(scope, element) {
        scope.$watch('ordersTableData', function() {
          var tableRows = element.find('tbody tr');
          var maxHeight = 0;
          var row;

          tableRows
            .each(function(){
              angular.element(this).height('auto');
            })
            .each(function() {
              row = angular.element(this);
              maxHeight = row.height() > maxHeight? row.height(): maxHeight;
            })
            .each(function() {
              angular.element(this).height(maxHeight);
            })
            .then(function(){
              maxHeight = 0;
            });

        }, true);
      }
    };
  }
]);
