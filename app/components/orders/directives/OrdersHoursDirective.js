'use strict';


/**
 * @ngdoc function
 * @name frontendApp.directive:OrdersPageHours
 * @description
 *
 */
angular.module('frontendApp')
  .directive('ordersPageHours', [function() {
    return {
      scope: {
        date: '='
      },
      link: function(scope, element) {
        // @TODO Improve this UNUSED directive

        if (typeof scope.date === 'undefined') {
          return;
        }

        function updateTime() {
          var string    = '';
          var timestamp = Math.floor((Date.now() - Date.parse(scope.date)) / 1000);

          var days    = Math.floor(timestamp / 86400);
          var hours   = Math.floor((timestamp - (days * 86400 ))/3600);
          var minutes = Math.floor((timestamp - (days * 86400 ) - (hours *3600 ))/60);
          var secs    = Math.floor((timestamp - (days * 86400 ) - (hours *3600 ) - (minutes*60)));

          hours   = hours < 10? '0'+ String(hours): hours;
          minutes = minutes < 10? '0'+ String(minutes): minutes;
          secs    = secs < 10? '0'+ String(secs): secs;

          string +=  days +' Days '+ hours +' H : '+ minutes +' M : '+ secs +' S';

          element.html(string);

          setTimeout(updateTime, 1000);
        }

        updateTime();
      }
    };
  }]
);
