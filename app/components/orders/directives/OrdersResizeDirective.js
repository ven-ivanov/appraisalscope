'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.directive:OrdersResizeDirective
 * @description
 *
 *
 */
app.directive('ordersResizeDirective', [
  '$window', 'OrdersStateService',

  function($window, OrdersStateService) {
    var WIDE_SCREEN_THRESHOLD = 1600;
    var ANIMATION_TIME        = 250;
    var window = angular.element($window);

    return {
      link: function(scope, element) {
        var searchTable    = element.find('#dashboardLeftArea');
        var previewWindow  = element.find('#dashboardRightArea');
        var fullScreen     = true;
        var firstLoad      = true;
        var previewOpen    = false;

        // Prevent flickering preview window while the page is loading
        previewWindow.hide();

        // Watch for changes to the appraisal and update the view
        scope.$watch(OrdersStateService.selectedAppraisal, function(selectedAppraisal) {
          var previewHeader  = element.find('.fixed-header');
          var previewContent = element.find('.fixed-position');

          // Force static width wile we animate. Fixes resizing problem while the animation is playing
          previewHeader.css('width', '670px');
          previewContent.css('width', '671.5px');

          // Don't animate the preview window when the page is loaded. Only on user interaction
          var animationDelay = firstLoad || fullScreen? 0: ANIMATION_TIME;

          searchTable.removeClass('col-lg-12 col-el-8 col-ml-7');

          previewWindow.animate({right: '-672px'}, previewOpen? animationDelay: 0, function() {
            previewOpen = false;
            previewWindow.hide();

            if (selectedAppraisal !== '' && selectedAppraisal !== null) {
              searchTable.addClass('col-lg-12 col-el-8 col-ml-7');
              previewWindow.show().animate({right: '0px'}, animationDelay, function() {
                  previewOpen = true;
                  previewHeader.css('width', '');
                  previewContent.css('width', '');
                });
            }
          });

          firstLoad = false;
        });

        /**
         * Set the elements position depending on the screen size.
         */
        function updatePageSize() {
          if (window.outerWidth() > WIDE_SCREEN_THRESHOLD) {
            // If there is room show table and preview side by side
            previewWindow.removeClass('sidebar col-lg-6');
            fullScreen = true;
          } else {
            // If there is not enough room the preview should float above the table
            previewWindow.addClass('sidebar col-lg-6');
            fullScreen = false;
          }
        }

        updatePageSize();

        window.bind('resize', updatePageSize);
      }
    };
  }
]);
