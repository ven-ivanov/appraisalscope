'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.directive:OrdersTableRow
 * @description
 *
 * Generate table row fields based on the table columns configuration.
 */
app.directive('ordersTableRow', [
  '$compile', 'SharedElementGeneratorService',

  function($compile, SharedElementGeneratorService) {
    var TEMPLATE_PREFIX = '/components/orders/views/searchTableFields/';

    return {
      scope: {
        columns: '='
      },
      link: function(scope, element) {
        scope.$watch('columns', function() {
          var key, column;
          var html = '';

          for (key in scope.columns) {
            column = scope.columns[key].row;

            /**
             * Depending on field type either load a template file, show the data or if
             * there is no config just add a empty field
             */
            if (typeof column.template !== 'undefined') {
              html += SharedElementGeneratorService.td('', {
                'ng-include': '\''+ TEMPLATE_PREFIX + column.template + '\''
              });
            } else if (typeof column.display !== 'undefined') {
              html += SharedElementGeneratorService.td(
                '{{'+ column.display +'}}'
              );
            } else {
              html += SharedElementGeneratorService.td();
            }
          }

          html = $compile(angular.element(html))(scope.$parent);

          element.html(html);
        });
      }
    };
  }
]);
