'use strict';

var backend = null,
helpers = new global.UserHelper(),
urlHelper = new global.UrlHelper();

describe('Appraisal Scope Front-end App Auth Scenarios', function() {

  beforeEach(function () {
    backend = new global.HttpBackend(browser);
  });

  afterEach(function() {
    backend.clear();
  });

  describe('unauthenticated', function () {
    describe('urls', function () {
      // Allow requests
      beforeEach(function () {
        backend.whenGET(/.*/).passThrough();
        backend.whenPOST(/.*/).passThrough();
      });

      it('should redirect / to /auth/sign-in', function() {
        browser.get('/#/auth/sign-out');
        browser.wait(protractor.until.elementIsVisible($('.auth-page-wrapper')));
        browser.get('/index.html');
        browser.getLocationAbsUrl().then(function(url) {
          expect(url).toBe('/auth/sign-in');
        });
      });

      it('should allow unauthenticated users to access the sign in forms', function() {
        browser.get('/#/auth/sign-in');
        urlHelper.testUrl('auth/sign-in');
      });

      it('should allow the user to access the client sign up form', function () {
        browser.get('/#/auth/client-sign-up');
        urlHelper.testUrl('auth/client-sign-up');
      });

      it('should allow the user to access the appraiser sign up form', function () {
        browser.get('/#/auth/appraiser-sign-up');
        urlHelper.testUrl('auth/appraiser-sign-up');
      });
    });
  });
});
