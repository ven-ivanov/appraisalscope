'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AuthTabsCtrl
 * @description
 * # AuthTabsCtrl
 * Controller of the frontendApp to manage tab changing on auth pages
 */
angular.module('frontendApp')
  .controller('AuthTabsCtrl', ['$scope', '$state', function ($scope, $state) {

    function setActiveTab(stateName){
      $scope.activeTab = null;
      $scope.tabs.forEach(function (tab) {
        if (tab.stateName === stateName) {
          $scope.activeTab = tab;
        }
      });
    }

    function initialize() {

      $scope.tabs = [
        {stateName: 'auth.sign-in', label: 'Sign In'},
        {stateName: 'auth.client-sign-up', label: 'Client Sign Up'},
        {stateName: 'auth.appraiser-sign-up.step-1', label: 'Appraiser Sign Up'}
      ];

      setActiveTab($state.current.name);

      $scope.selectTab = function (tab) {
        $scope.activeTab = tab;
        $state.go(tab.stateName);
      };

      $scope.$on('$stateChangeSuccess', function () {
        setActiveTab($state.current.name);
      });
    }

    initialize();

  }]);
