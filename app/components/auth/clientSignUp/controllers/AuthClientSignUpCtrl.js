'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AuthClientSignUpCtrl
 * @description
 * # AuthClientSignUpCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AuthClientSignUpCtrl', [
    '$state', 'AuthClientSignUpService', 'HandbookService', 'HANDBOOK_CATEGORIES', '$q',

    function ($state, AuthClientSignUpService, HandbookService, HANDBOOK_CATEGORIES, $q) {

      var vm = this;

      vm.dataLoaded = false;

      var states = HandbookService.query({category: HANDBOOK_CATEGORIES.states});
      var accountTypes = HandbookService.query({category: HANDBOOK_CATEGORIES.accountTypes});

      /**
        * Get data for select boxes
        */
      $q.all([
        states.$promise,
        accountTypes.$promise
      ]).then(function () {
        vm.states = states;
        vm.accountTypes = accountTypes;
        vm.dataLoaded = true;
      });

      /**
        * Save the form
        */
      vm.save = function () {
        return AuthClientSignUpService.save(vm.client)
          .success(function () {
            $state.transitionTo('auth.sign-in');
          });
      };

    }
  ]);
