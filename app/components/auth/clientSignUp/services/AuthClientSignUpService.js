'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AuthClientSignUpService
 * @description
 * # AuthClientSignUpService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
.factory('AuthClientSignUpService', [
    '$http', 'API_PREFIX', function ($http, API_PREFIX) {

  var clientSignUp = {};

	/**
	  * Save client data
	  */
  clientSignUp.save = function (clientData) {
    return $http.post(API_PREFIX() + '/v2.0/auth/client', clientData);
  };

  return clientSignUp;
}]);
