'use strict';

describe('Controller: AuthClientSignUpCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var AuthClientSignUpCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthClientSignUpCtrl = $controller('AuthClientSignUpCtrl', {
      $scope: scope
    });
  }));
});
