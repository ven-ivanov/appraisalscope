'use strict';

/**
 * @author Logan Etherton<logan@loganswalk.com>
 */

var backend = null,
helpers = new global.UserHelper(),
formHelper = new global.FormHelper(),
urlHelper = new global.UrlHelper();

describe('Appraisal Scope Front-end App Auth Scenarios', function() {

  var formElements;

  beforeEach(function () {
    backend = new global.HttpBackend(browser);
  });

  afterEach(function() {
    backend.clear();
  });

  describe('unauthenticated', function () {
    describe('client sign up form', function () {
      beforeEach(function () {
        // When querying the account types, respond with these two
        backend.whenGET(/account-types/).respond({
          data: [
            {
              id: 1,
              title: 'Mortgage Broker'
            },
            {
              id: 2,
              title: 'Lender'
            }
          ]
        });
        // When querying states, respond with these two
        backend.whenGET(/states/).respond({
          data: [
            {
              id: 1,
              code: 'AL',
              title: 'Alabama'
            },
            {
              id: 2,
              code: 'AK',
              title: 'Alaska'
            }
          ]
        });
        backend.whenGET(/.*/).passThrough();
        // Respond with nothing, since that's what the client sign up form currently does
        backend.whenPOST(/auth\/client/).respond('');
      });

      it('should have all of the necessary form fields', function () {
        // Go to client sign up form
        browser.get('/#/auth/client-sign-up');
        // Get all elements in form
        formElements = element.all(by.css('.form-group input, select[as-select], textarea'));
        // Verify there are 15 elements
        expect(formElements.count()).toBe(15);
      });

      it('should allow the user to enter a company name', function () {
        formHelper.testStandardFormInputBox({
          sanityString: 'name="user_company_name" id="inputCompanyName".*?ng-model="client.user_company_name"',
          sendKeyValue: 'Test Company',
          labelText: '',
          formElements: formElements
        });
      });

      it('should prevent the user from submitting the form with incomplete form items', function () {
        formHelper.assertNumberVisibleItems(0, '.help-block');
        // Try to submit
        $('button').click();
        // Get count of visible help items
        formHelper.assertNumberVisibleItems(9, '.help-block');
      });

      it('should allow the user to enter an email address', function () {
        formHelper.testStandardFormInputBox({
          sanityString: 'name="email" id="inputEmail".*?ng-model="client.email"',
          sendKeyValue: 'test@test.com',
          labelText: '*EMAIL ADDRESS'
        });
      });

      it('should allow the user to select an account type', function () {
        formHelper.checkDropDown({
          sanityString: 'name="user_account_type_id" id="inputAccountType".*?ng-model="client.user_account_type_id"',
          labelText: '*ACCOUNT TYPE',
          labelDepth: 2,
          optionText: 'Lender',
          optionValue: '1'
        });
        // need to switch back to mortgage broker to see the next field
        formHelper.checkDropDown({
          sanityString: 'name="user_account_type_id" id="inputAccountType".*?ng-model="client.user_account_type_id"',
          labelText: '*ACCOUNT TYPE',
          labelDepth: 2,
          optionText: 'Mortgage Broker',
          optionValue: '0',
          elementNumber: 2
        });
      });

      it('should allow the user to enter the estimated number of orders per month', function () {
        formHelper.testStandardFormInputBox({
          sanityString: 'name="eo_amount" id="inputAmountOfOrders".*?ng-model="client.eo_amount"',
          sendKeyValue: '1000',
          labelText: 'ESTIMATED NUMBER OF ORDERS PER MONTH'
        });
      });

      it('should allow the user to enter a list of wholesalers', function () {
        formHelper.testStandardFormInputBox({
          sanityString: 'name="wholesalers" id="inputWholesalers".*?ng-model="client.wholesalers"',
          sendKeyValue: 'Test Wholesaler',
          labelText: '*WHOLESALERS'
        });
      });

      it('should allow the user to enter their first name', function () {
        formHelper.testStandardFormInputBox({
          sanityString: 'name="first_name" id="inputFirstName".*?ng-model="client.first_name"',
          sendKeyValue: 'FirstName',
          labelText: '*FIRST NAME'
        });
      });

      it('should allow the user to enter their last name', function () {
        formHelper.testStandardFormInputBox({
          sanityString: 'name="last_name" id="inputLastName".*?ng-model="client.last_name"',
          sendKeyValue: 'LastName',
          labelText: '*LAST NAME'
        });
      });

      it('should allow the user to enter their address', function () {
        formHelper.testStandardFormInputBox({
          sanityString: 'name="address1" id="inputAddress".*?ng-model="client.address1"',
          sendKeyValue: '1111 Test St.',
          labelText: '*ADDRESS'
        });
      });

      it('should allow the user to enter an additional address item', function () {
        formHelper.testStandardFormInputBox({
          sanityString: 'name="address2" id="inputAddAddress".*?ng-model="client.address2"',
          sendKeyValue: 'Suite Test',
          labelText: 'ADDITIONAL ADDRESS'
        });
      });

      it('should allow the user to enter their city', function () {
        formHelper.testStandardFormInputBox({
          sanityString: 'name="city" id="inputCity".*?ng-model="client.city"',
          sendKeyValue: 'Test City',
          labelText: '*CITY'
        });
      });

      it('should allow the user to enter their zip code', function () {
        formHelper.testStandardFormInputBox({
          sanityString: 'name="zip" id="inputZIP".*?ng-model="client.zip"',
          sendKeyValue: '45209',
          labelText: '*ZIP'
        });
      });

      it('should allow the user to select their state', function () {
        formHelper.checkDropDown({
          sanityString: 'name="state" id="inputState".*?ng-model="client.state"',
          labelText: '*STATE',
          labelDepth: 2,
          optionText: 'Alaska',
          optionValue: '1'
        });
      });

      it('should allow the user to enter their phone number', function () {
        formHelper.testStandardFormInputBox({
          sanityString: 'name="phone" id="inputPhone".*?ng-model="client.phone"',
          sendKeyValue: '555-555-5555',
          labelText: '*PHONE'
        });
      });

      it('should allow the user to enter a phone extension', function () {
        formHelper.testStandardFormInputBox({
          sanityString: 'name="phone_ext".*?id="inputPhoneExt" ng-model="client.phone_ext"',
          sendKeyValue: '55'
        });
      });

      it('should allow the user to enter a fax number', function () {
        formHelper.testStandardFormInputBox({
          sanityString: 'name="fax" id="inputFax".*?ng-model="client.fax"',
          sendKeyValue: '555-555-5555',
          labelText: 'FAX'
        });
      });

      it('should allow the user to submit the form successfully', function () {
        // Try to submit
        $('button').click();
        urlHelper.testUrl('auth/sign-in');
      });

      it('should remove the set of form elements currently being examined', function () {
        formHelper.resetFormElements();
      });
    });
  });
});
