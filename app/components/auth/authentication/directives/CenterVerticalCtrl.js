'use strict';

var app = angular.module('frontendApp');

app.controller('CenterVerticalCtrl', ['$scope', '$state', 'AUTH_SCREEN_SIZES', function ($scope, $state, AUTH_SCREEN_SIZES) {
  var vm = this;

  /**
   * Detect if the viewport is large enough for absolute positioning
   * @returns {boolean}
   */
  var viewportLarge = function () {
    return angular.element(window).height() >= AUTH_SCREEN_SIZES.heightLarge && angular.element(window).width() >= AUTH_SCREEN_SIZES.width;
  };

  /**
   * Trigger absolute positioning only on correct state with large enough screen size
   */
  $scope.$on('$stateChangeStart', function(event, toState){
    // Check state and ensure that the browser window is tall enough
    if ((toState.name === 'auth.sign-in' || toState.name === 'auth.reset-password') && (viewportLarge())) {
      vm.setAbsolute = true;
      return;
    }
    vm.setAbsolute = false;
  });
  // Check state and ensure that the browser window is tall enough
  if (($state.current.name === 'auth.sign-in' || $state.current.name === 'auth.reset-password') && (viewportLarge())) {
      vm.setAbsolute = true;
  }
}]);