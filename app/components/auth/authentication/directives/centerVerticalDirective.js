'use strict';

var app = angular.module('frontendApp');

app.directive('centerVertical', ['AUTH_SCREEN_SIZES', function (AUTH_SCREEN_SIZES) {
  return {
    restrict: 'A',
    controller: 'CenterVerticalCtrl',
    controllerAs: 'centerVerticalCtrl',
    link: function (scope, element, attrs, controller) {
      var documentHeight, documentWidth;
      // Trigger on resize
      angular.element(window).resize(function () {
        documentHeight = angular.element(window).height();
        documentWidth = angular.element(window).width();
        // If the screen gets too small, remove absolute positioning
        if ((documentHeight < AUTH_SCREEN_SIZES.heightSmall || documentWidth < AUTH_SCREEN_SIZES.width) && controller.setAbsolute) {
          scope.$digest(controller.setAbsolute = false);
        // If the viewport is large enough, add absolute positioning
        } else if ((documentHeight > AUTH_SCREEN_SIZES.heightLarge && documentWidth >= AUTH_SCREEN_SIZES.width) && !controller.setAbsolute) {
          scope.$digest(controller.setAbsolute = true);
        }
      });
    }
  };
}]);