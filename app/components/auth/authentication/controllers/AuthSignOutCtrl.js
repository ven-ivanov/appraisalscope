'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AuthSignOutCtrl
 * @description
 * # AuthSignOutCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AuthSignOutCtrl', [
    '$scope', 'AuthService', '$state',
    function ($scope, AuthService, $state) {
      AuthService.logout();
      $state.transitionTo('auth.sign-in');
    }]);
