'use strict';

var app = angular.module('frontendApp');

/**
 * @ngdoc function
 * @name frontendApp.controller:AuthSignInCtrl
 * @description
 * # AuthSignInCtrl
 * Controller of the frontendApp
 */

app.controller('AuthSignInCtrl', [
  '$scope', '$rootScope', '$state', 'SessionService', 'AUTH_EVENTS', 'AuthService',

  function ($scope, $rootScope, $state, SessionService, AUTH_EVENTS, AuthService) {
    if (typeof SessionService.user !== 'undefined') {
      if (SessionService.user.typeName === 'admin') {
        $state.transitionTo('main.dashboard');
      } else {
        $state.transitionTo('main.orders');
      }
    }

    $scope.authenticating = false;
    $scope.invalidCredentials = false;

    $scope.credentials = {
      username: '',
      password: ''
    };

    $scope.login = function (credentials) {
      $scope.authenticating = true;

      AuthService.login(credentials).then(function (user) {
        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
        $scope.setCurrentUser(user);

        $scope.invalidCredentials = false;

        // Redirect based on type
        $state.transitionTo(AuthService.getRedirectStateOnLogin(user.type));
      }, function () {
        $scope.authenticating = false;
        $scope.invalidCredentials = true;
        $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
      });
    };
  }
]);
