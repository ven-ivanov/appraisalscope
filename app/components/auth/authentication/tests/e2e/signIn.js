'use strict';

/**
 * @author Logan Etherton<logan@loganswalk.com>
 */

var backend = null,
helpers = new global.UserHelper(),
formHelper = new global.FormHelper(),
urlHelper = new global.UrlHelper();

describe('Appraisal Scope Front-end App Auth Scenarios', function() {

  var formElements;

  beforeEach(function () {
    backend = new global.HttpBackend(browser);
  });

  afterEach(function() {
    backend.clear();
  });

  describe('unauthenticated', function () {
    describe('sign in form', function () {
      // Allow requests
      beforeEach(function () {
        backend.whenGET(/.*/).passThrough();
        // Catch the sign in request
        backend.whenPOST(/auth\/sign-in/).respond(function (method, url, data, headers) {
          return [401, {}];
        });
        backend.whenPOST(/.*/).passThrough();
      });

      it('should have 2 form elements visible', function () {
        browser.get('/#/auth/sign-in');
        // Get form elements
        formElements = element.all(by.css('.form-group input'));
        // Get count of form elements
        expect(formElements.count()).toBe(2);
        // Both should be visible
        formHelper.assertNumberVisibleItems(2, '.form-group input')
      });

      it('should allow the user to input their username', function () {
        formHelper.testStandardFormInputBox({
          sanityString: 'name="username" id="inputUsername".*?ng-model="credentials.username"',
          sendKeyValue: 'testUser',
          labelText: 'USERNAME',
          formElements: formElements
        });
      });

      it('should allow users to enter their password', function () {
        formHelper.testStandardFormInputBox({
          sanityString: 'name="password" id="inputPassword".*?ng-model="credentials.password"',
          sendKeyValue: 'password',
          labelText: 'PASSWORD'
        });
      });

      it('should allow the user to try to sign in after the inputs have been completed', function () {
        $('button').click();
        formHelper.assertNumberVisibleItems(0, 'div.alert.alert-danger');
      });

      it('should remove the set of form elements currently being examined', function () {
        formHelper.resetFormElements();
      });
    });
  });
});
