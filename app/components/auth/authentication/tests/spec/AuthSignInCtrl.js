'use strict';

describe('Controller: AuthSignInCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var AuthSignInCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthSignInCtrl = $controller('AuthSignInCtrl', {
      $scope: scope
    });
  }));
});
