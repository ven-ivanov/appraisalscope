'use strict';

describe('Controller: AuthSignOutCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var AuthSignOutCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthSignOutCtrl = $controller('AuthSignOutCtrl', {
      $scope: scope
    });
  }));

  it('should remove token from localStorage', function () {
    expect(localStorage.getItem('token')).toBe(null);
  });
});
