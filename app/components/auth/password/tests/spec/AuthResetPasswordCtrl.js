'use strict';

describe('Controller: AuthResetPasswordCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var AuthResetPasswordCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthResetPasswordCtrl = $controller('AuthResetPasswordCtrl', {
      $scope: scope
    });
  }));
});
