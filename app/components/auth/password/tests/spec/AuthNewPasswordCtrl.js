'use strict';

describe('Controller: AuthNewPasswordCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var AuthNewPasswordCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthNewPasswordCtrl = $controller('AuthNewPasswordCtrl', {
      $scope: scope
    });
  }));
});
