'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AuthResetPasswordCtrl
 * @description
 * # AuthResetPasswordCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AuthResetPasswordCtrl', function ($scope, RecoverService) {
    $scope.request = function (email) {
      $scope.sending = true;

      return RecoverService.remind(email).then(function () {
        $scope.emailDispatched = true;
      }, function () {
        $scope.sending = false;
        $scope.unrecognizedEmail = true;
      });
    };
  });
