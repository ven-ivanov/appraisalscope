'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AuthNewPasswordCtrl
 * @description
 * # AuthNewPasswordCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AuthNewPasswordCtrl', function ($scope, $stateParams, ValidatePasswordChangeTokenService, PasswordChangeService) {
    $scope.token = $stateParams.token;
    $scope.tokenValidated = false;
    $scope.invalidToken = false;
    $scope.credentials = {
      email: '',
      password: '',
      passwordConfirmation: '',
      token: $scope.token
    };

    ValidatePasswordChangeTokenService.validate($scope.token).then(function () {
      $scope.tokenValidated = true;
    }, function () {
      $scope.invalidToken = true;
    });

    $scope.reset = function (credentials) {
      $scope.processing = true;

      PasswordChangeService.reset(credentials).then(function () {
        $scope.passwordChanged = true;
      }, function () {
        $scope.invalidRequest = true;
        $scope.processing = false;
      });
    };

  });
