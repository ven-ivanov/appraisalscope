'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AuthAppraiserSignUpUploadService
 * @description
 * # AuthAppraiserSignUpUploadService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AuthAppraiserSignUpUploadService', [
    '$resource', 'API_PREFIX', 'FileUploader',

    function ($resource, API_PREFIX, FileUploader) {

      var prefix = API_PREFIX() + '/v2.0/auth/appraiser-sign-up/';

      var coverageLicenseUpload = prefix + 'coverage-license';
      var uploadLicense = prefix + 'license-upload';
      var uploadResume = prefix + 'resume-upload';

      return {
        coverageLicenseUpload: function (file) {
          return FileUploader.upload({
            url: coverageLicenseUpload,
            method: 'POST',
            file: file
          });
        },
        uploadLicense: function (file) {
          return FileUploader.upload({
            url: uploadLicense,
            method: 'POST',
            file: file
          });
        },
        uploadResume: function (file) {
          return FileUploader.upload({
            url: uploadResume,
            method: 'POST',
            file: file
          });
        }
      };
    }
  ]);