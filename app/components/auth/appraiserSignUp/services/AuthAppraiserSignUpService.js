'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AuthAppraiserSignUpService
 * @description
 * # AuthAppraiserSignUpService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AuthAppraiserSignUpService', [
    '$resource', 'API_PREFIX',

    function ($resource, API_PREFIX) {

      var prefix = API_PREFIX() + '/v2.0/auth/appraiser-sign-up/';

      var licensedUsers = prefix + 'licensed-users';
      var settings = prefix + 'settings';
      var appraisalForms = prefix + 'appraisal-forms';
      var counties = prefix + 'counties';
      var zipCodes = prefix + 'zip-codes';
      var saveForm = prefix + 'save';

      return $resource(prefix, {}, {
        getLicensedUsers: {method: 'GET', url: licensedUsers},// get list of licensed users
        getSignUpSettings: {method: 'GET', url: settings},// get sign up system settings
        getAppraisalForms: {method: 'GET', url: appraisalForms},// get list of appraisal forms
        getCounties: {method: 'GET', url: counties},// get list of counties
        getZipCodes: {method: 'GET', url: zipCodes},// get list of zipcodes

        saveForm: {method: 'POST', url: saveForm} // save registraion form
      });
    }
  ]);