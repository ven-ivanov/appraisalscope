'use strict';

describe('Controller: AuthAppraiserSignUpW9FormCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var AuthAppraiserSignUpW9FormCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthAppraiserSignUpW9FormCtrl = $controller('AuthAppraiserSignUpW9FormCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    // expect(scope.awesomeThings.length).toBe(3);
    expect(true).toBe(true);
  });
});
