'use strict';

describe('Controller: AuthAppraiserSignUpAppraisalFormsCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var AuthAppraiserSignUpAppraisalFormsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthAppraiserSignUpAppraisalFormsCtrl = $controller('AuthAppraiserSignUpAppraisalFormsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    // expect(scope.awesomeThings.length).toBe(3);
    expect(true).toBe(true);
  });
});
