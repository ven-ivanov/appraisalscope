'use strict';

describe('Controller: AuthAppraiserSignUpDocumentUploadCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var AuthAppraiserSignUpDocumentUploadCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthAppraiserSignUpDocumentUploadCtrl = $controller('AuthAppraiserSignUpDocumentUploadCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    // expect(scope.awesomeThings.length).toBe(3);
    expect(true).toBe(true);
  });
});
