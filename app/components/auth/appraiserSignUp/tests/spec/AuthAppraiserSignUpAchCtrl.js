'use strict';

describe('Controller: AuthAppraiserSignUpAchCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var AuthAppraiserSignUpAchCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthAppraiserSignUpAchCtrl = $controller('AuthAppraiserSignUpAchCtrl', {
      $scope: scope
    });
  }));
});
