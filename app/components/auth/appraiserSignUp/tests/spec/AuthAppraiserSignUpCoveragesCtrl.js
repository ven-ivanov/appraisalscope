'use strict';

describe('Controller: AuthAppraiserSignUpCoveragesCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var AuthAppraiserSignUpCoveragesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthAppraiserSignUpCoveragesCtrl = $controller('AuthAppraiserSignUpCoveragesCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    // expect(scope.awesomeThings.length).toBe(3);
    expect(true).toBe(true);
  });
});
