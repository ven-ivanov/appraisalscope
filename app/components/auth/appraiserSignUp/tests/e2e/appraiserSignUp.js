/**
 * Disabled since the appraiser sign up has changed since I wrote this -- Logan
 */
//'use strict';
//
//var backend = null,
//helpers = new global.UserHelper(),
//formHelper = new global.FormHelper();
//
//describe('Appraisal Scope Front-end App Auth Scenarios', function() {
//
//  var formElements;
//
//  beforeEach(function () {
//    backend = new global.HttpBackend(browser);
//  });
//
//  afterEach(function() {
//    backend.clear();
//  });
//
//  describe('unauthenticated', function () {
//    describe('appraiser sign up form', function () {
//      // Allow requests through
//      beforeEach(function () {
//        // State response
//        backend.whenGET(/states/).respond({
//          data: [
//            {
//              'id': 1,
//              'title': 'Alabama',
//              'code': 'AL'
//            },
//            {
//              'id': 2,
//              'title': 'Alaska',
//              'code': 'AK'
//            }
//          ]
//        });
//        // License response
//        backend.whenGET(/licensed-users\?licenseNumber/).respond({
//          data: [
//            {
//              'id': 1,
//              'last_name': 'Tester',
//              'first_name': 'Testy',
//              'email': 'testy@tester.com',
//              'company_name': 'Test Company'
//            }
//          ]
//        });
//        // Certification response
//        backend.whenGET(/primary-licenses/).respond({
//          data: [
//            {'name': 'Licensed Appraiser', 'title': 'Licensed Appraiser'},
//            {'name': 'Certified Appraiser', 'title': 'Certified Appraiser'},
//            {'name': 'General Appraiser', 'title': 'General Appraiser'}
//          ]
//        });
//        backend.whenGET(/.*/).passThrough();
//        backend.whenPOST(/.*/).passThrough();
//      });
//
//      it('should have all of the necessary form elements', function () {
//        browser.get('/#/auth/appraiser-sign-up');
//        // Get all elements in form
//        formElements = formHelper.getFormItems('.form-group input, select[as-select], textarea');
//        // Verify there are 32 elements
//        expect(formElements.count()).toBe(32);
//        // This is a protractor quirk - the visible items are not being counted correctly
//        formHelper.assertNumberVisibleItems(28, 'input, select[as-select], textarea');
//      });
//
//      it('should allow the user to enter their first name', function () {
//        // Check this input
//        formHelper.testStandardFormInputBox({
//          formElements: formElements,
//          sanityString: 'name="first_name" id="inputFirstName".*?ng-model="appraiser.first_name"',
//          sendKeyValue: 'FirstName',
//          labelText: '*FIRST NAME'
//        });
//      });
//
//      /**
//       * @TODO This cannot be completed yet, since the validation does not prevent the user from switching pages
//       */
//        //it('should prevent the user from submitting the form with incomplete form items', function () {
//        //  // @TODO
//        //  formHelper.assertNumberVisibleItems(0, '.help-block');
//        //  // Try to submit
//        //  $('button').click();
//        //  // Get count of visible help items
//        //  formHelper.assertNumberVisibleItems(9, '.help-block');
//        //});
//
//      it('should allow the user to enter their last name', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="last_name" id="inputLastName".*?ng-model="appraiser.last_name"',
//          sendKeyValue: 'LastName',
//          labelText: '*LAST NAME'
//        });
//      });
//
//      it('should allow the user to enter their email address', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="email" id="inputEmail".*?ng-model="appraiser.email"',
//          sendKeyValue: 'test@test.com',
//          labelText: '*EMAIL'
//        });
//      });
//
//      it('should allow the user to enter their company name', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="user_company_name" id="inputCompanyName".*?ng-model="appraiser.user_company_name"',
//          sendKeyValue: 'Test Company',
//          labelText: '*COMPANY NAME'
//        });
//      });
//
//      it('should allow the user to select the state in which they are certified', function () {
//        formHelper.checkDropDown({
//          sanityString: 'name="certified_state" id="inputCertifiedState".*?ng-model="appraiser.certified_state"',
//          labelText: '*CERTIFIED STATE',
//          labelDepth: 2,
//          optionText: 'Alaska',
//          optionValue: '1'
//        });
//      });
//
//      it('should allow the user to enter their license number', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="license_number" id="inputLicenseNumber".*?ng-model="appraiser.license_number"',
//          sendKeyValue: 'Testy Tester',
//          labelText: '*LICENSE NUMBER',
//          labelDepth: 2
//        });
//      });
//
//      it('should populate the relevant fields when a user is selected from the license dropdown', function () {
//        // Select the response
//        element.all(by.repeater('(key, user) in licensedUsers')).then(function (licenseResponse) {
//          licenseResponse[0].click();
//          // First name should now be Testy
//          expect(formElements.get(0).getAttribute('value')).toBe('Testy');
//          // Last name should now be Tester
//          expect(formElements.get(1).getAttribute('value')).toBe('Tester');
//          // Email should now be testy@tester.com
//          expect(formElements.get(2).getAttribute('value')).toBe('testy@tester.com');
//          // Company should now be Test Company
//          expect(formElements.get(3).getAttribute('value')).toBe('Test Company');
//        });
//      });
//
//      it('should allow the user to select a username', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="username" id="inputUsername".*?ng-model="appraiser.username"',
//          sendKeyValue: 'testusername',
//          labelText: '*USERNAME'
//        });
//      });
//
//      it('should allow the user to enter a password', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="password" id="inputPassword".*?ng-model="appraiser.password"',
//          sendKeyValue: 'password',
//          labelText: '*PASSWORD'
//        });
//      });
//
//      it('should allow the user to confirm their password', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="confirm_password" id="inputConfirmPassword".*?ng-model="appraiser.confirm_password"',
//          sendKeyValue: 'password',
//          labelText: '*CONFIRM PASSWORD'
//        });
//      });
//
//      it('should not allow the user to proceed if password and confirm do not match', function () {
//        // Clear the input and reenter the password
//        formHelper.clearInput(8);
//        // Change the password so it is not correct
//        formHelper.testStandardFormInputBox({
//          sendKeyValue: 'badpassword',
//          elementNumber: 8
//        });
//        // Click something else
//        formElements.get(0).click();
//        // Check for validation
//        formHelper.assertNumberVisibleItems(1, '.help-block');
//        // Clear it again
//        formHelper.clearInput(8);
//        // Change back and continue
//        formHelper.testStandardFormInputBox({
//          sendKeyValue: 'password',
//          elementNumber: 8
//        });
//      });
//
//      it('should allow the user to enter their address', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="address1" id="inputAddress1".*?ng-model="appraiser.address1"',
//          sendKeyValue: '1000 Test Ave.',
//          labelText: '*ADDRESS'
//        });
//      });
//
//      it('should allow the user to enter an additional address field', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="address2" id="inputAddress2".*?ng-model="appraiser.address2"',
//          sendKeyValue: 'Apt 88',
//          labelText: 'ADDITIONAL ADDRESS'
//        });
//      });
//
//      it('should allow the user to enter their city', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="city" id="inputCity".*?ng-model="appraiser.city"',
//          sendKeyValue: 'Cincinnati',
//          labelText: '*CITY'
//        });
//      });
//
//      it('should allow the user to enter their zip code', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="zip" id="inputZIP".*?ng-model="appraiser.zip"',
//          sendKeyValue: '45209',
//          labelText: '*ZIP'
//        });
//      });
//
//      it('should allow the user to select their state', function () {
//        formHelper.checkDropDown({
//          sanityString: 'name="state" id="inputState".*?ng-model="appraiser.state"',
//          labelText: '*STATE',
//          labelDepth: 2,
//          optionText: 'Alaska',
//          optionValue: '1'
//        });
//      });
//
//      it('should allow the user to enter the address for assignments', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="assignments_address" id="inputAssignmentsAddress".*?ng-model="appraiser.assignments_address"',
//          sendKeyValue: '100 Assignment Drive'
//        });
//      });
//
//      it('should allow the user to enter an additional address for assignments', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="add_assignments_address" id="inputAddAssignmentsAddress".*?ng-model="appraiser.add_assignments_address"',
//          sendKeyValue: 'Additional Apt 8'
//        });
//      });
//
//      it('should allow the user to enter the city for assignments', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="assignments_city" id="inputAssignmentsCity".*?ng-model="appraiser.assignments_city"',
//          sendKeyValue: 'City City'
//        });
//      });
//
//      it('should allow the user to select the state for assignments', function () {
//        formHelper.checkDropDown({
//          sanityString: 'name="assignments_state" id="inputAssignmentsState".*?ng-model="appraiser.assignments_state"',
//          labelText: '*STATE FOR ASSIGNMENTS',
//          labelDepth: 2,
//          optionText: 'Alaska',
//          optionValue: '1'
//        });
//      });
//
//      it('should allow the user to enter the zip for assignments', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="assignments_zip" id="inputAssignmentsZIP".*?ng-model="appraiser.assignments_zip"',
//          sendKeyValue: '45209'
//        });
//      });
//
//      it('should allow the user to enter their cell phone number', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="cell_phone" id="inputCellPhone".*?ng-model="appraiser.cell_phone"',
//          sendKeyValue: '513-555-5555'
//        });
//      });
//
//      it('should allow the user to enter their office phone number', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="phone" id="inputPhone".*?ng-model="appraiser.phone"',
//          sendKeyValue: '513-555-9999'
//        });
//      });
//
//      it('should allow the user to enter an office phone extension', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="phone_ext" id="inputOfficePhoneExt".*?ng-model="appraiser.office_phone_ext"',
//          sendKeyValue: '55'
//        });
//      });
//
//      it('should allow the user to enter their fax number', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="fax" id="inputFax".*?ng-model="appraiser.fax"',
//          sendKeyValue: '555-999-4255'
//        });
//      });
//
//      it('should allow the user to enter the years they have been licensed', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="years_licensed" id="inputYearsLicensed".*?ng-model="appraiser.years_licensed"',
//          sendKeyValue: '100'
//        });
//      });
//
//      it('should allow the user to enter their license expiration date', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="license_expiration_date" id="inputLicenseExpirationDate".*?ng-model="appraiser.license_expiration_date"',
//          sendKeyValue: '01/19/1999'
//        });
//      });
//
//      it('should allow the user to enter the E&O/claim amount', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="eo_amount" id="inputEOAmount".*?ng-model="appraiser.eo_amount"',
//          sendKeyValue: '10000'
//        });
//      });
//
//      it('should allow users to enter the E&O/aggregate amount', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="eo_aggregate_amount" id="inputEOAggregatesAmount".*?ng-model="appraiser.eo_aggregate_amount"',
//          sendKeyValue: '10000'
//        });
//      });
//
//      it('should allow users to enter the E&O expiration date', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="eo_expiration_date" id="inputEOExpirationDate".*?ng-model="appraiser.eo_expiration_date"',
//          sendKeyValue: '01/01/2016'
//        });
//      });
//
//      it('should allow the user to enter the E&O carrier', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="eo_carrier" id="inputEOCarrier".*?ng-model="appraiser.eo_carrier"',
//          sendKeyValue: 'Test Carrier'
//        });
//      });
//
//      it('should allow the user to enter their certifications', function () {
//        formHelper.checkDropDown({
//          sanityString: 'name="certifications" id="inputCertifications".*?ng-model="appraiser.certifications"',
//          labelDepth: 2,
//          optionText: 'Certified Appraiser',
//          optionValue: '1'
//        });
//      });
//
//      /**
//       * @TODO Complete these tests. The form items are not currently correctly on the form.
//       */
//        //it('should present the user with a checkbox for other certifications', function () {
//        //  // Basic sanity check
//        //  // Verify writing
//        //  // Check wording
//        //});
//        //
//        //it('should present the user with a checkbox for Certified Relocation Professional', function () {
//        //  // Basic sanity check
//        //  // Verify writing
//        //  // Check wording
//        //});
//
//      it('should remove the set of form elements currently being examined', function () {
//        formHelper.resetFormElements();
//      });
//    });
//
//    describe('appraiser sign up form page 2', function () {
//      // Allow requests through
//      beforeEach(function () {
//        backend.whenGET(/.*/).passThrough();
//        backend.whenPOST(/.*/).passThrough();
//      });
//
//      // @TODO THIS WILL CHANGE ONCE THE VALIDATION IS IN PLACE FOR PAGE 1 OF APPRAISER SIGN UP
//      it('should allow me to go to the second page', function () {
//        browser.get('/#/auth/appraiser-sign-up');
//        $('button').click();
//      });
//
//      it('should have all of the necessary form elements', function () {
//        formElements = formHelper.getFormItems('.form-group input, select[as-select], textarea');
//        expect(formElements.count()).toBe(58);
//      });
//
//      it('should allow the user to enter their name as it appears on the tax return', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="name" id="inputName".*?ng-model="w9.name"',
//          sendKeyValue: 'Testy Tester',
//          labelText: 'NAME (AS SHOWN ON YOUR INCOME TAX RETURN)',
//          elementNumber: 32,
//          formElements: formElements
//        });
//      });
//
//      it('should allow the user to enter a business name', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="business_name" id="inputBusinessName".*?ng-model="w9.business_name"',
//          sendKeyValue: 'Tester Corp.',
//          labelText: 'BUSINESS NAME/DISREGARDED ENTITY NAME, IF DIFFERENT FROM ABOVE'
//        });
//      });
//
//      /**
//       * @TODO Need clarification - next 3 items
//       */
//      it('should have seven checkboxes for company type', function () {
//        // Individual/sole pro
//        var taxClassificationCheckboxes = formHelper.getFormItems('[ng-click^="selectTaxClassification"]');
//        expect(taxClassificationCheckboxes.count()).toBe(7);
//      });
//
//      it('should allow the user to select only a single company type', function () {
//        // @TODO Not functional on form yet. Not even sure if this is the expected functionality
//      });
//
//      it('should allow the user to enter an "other" classification', function () {
//
//      });
//
//      it('should allow the user to enter the tax classification', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="lim_li_comp" id="inputLimLiComp".*?ng-model="w9.lim_li_comp"',
//          sendKeyValue: 'C',
//          labelText: 'ENTER THE TAX CLASSIFICATION',
//          elementNumber: 42
//        });
//      });
//
//      it('should only allow the user to enter C, D, or P into the tax classification input', function () {
//        // Clear input
//        formHelper.clearInput(42);
//        // Enter invalid entry
//        formHelper.testStandardFormInputBox({
//          sendKeyValue: 'R',
//          elementNumber: 42
//        });
//        // Click elsehwere
//        formElements.get(41).click().then(function () {
//          // Check that validation error occurred
//          formHelper.assertNumberVisibleItems(1, '.help-block');
//          // Make it a good value again
//          formHelper.clearInput(42);
//          formHelper.testStandardFormInputBox({
//            sendKeyValue: 'C',
//            elementNumber: 42
//          });
//        })
//      });
//
//      it('should allow the user to enter exempt payee code', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="exempt_payee_code" id="inputExemptPayeeCode".*?ng-model="w9.exempt_payee_code"',
//          sendKeyValue: '?????',
//          labelText: 'EXEMPT PAYEE CODE (IF ANY)'
//        });
//      });
//
//      it('should allow the user to enter exemption from FATCA reporting code', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="reporting_code" id="inputReportingCode".*?ng-model="w9.reporting_code"',
//          sendKeyValue: '????',
//          labelText: 'EXEMPTION FROM FATCA REPORTING CODE (IF ANY)'
//        });
//      });
//
//      it('should allow the user to list account numbers', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="acc_nos" id="inputAccNos".*?ng-model="w9.acc_nos"',
//          sendKeyValue: '????',
//          labelText: 'LIST ACCOUNT NUMBERS'
//        });
//      });
//
//      it('should allow the user to enter an address', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="address" id="inputAddress".*?ng-model="w9.address"',
//          sendKeyValue: '1919 Las Vegas Drive',
//          labelText: 'ADDRESS (INCLUDING APARTMENT OR SUITE NUMBER)'
//        });
//      });
//
//      it('should allow the user to enter a city', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="city" id="inputCityw9".*?ng-model="w9.city"',
//          sendKeyValue: 'Chicago',
//          labelText: 'CITY'
//        });
//      });
//
//      it('should allow the user to enter a state code', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="state" id="inputStatew9".*?ng-model="w9.state"',
//          sendKeyValue: 'OH',
//          labelText: 'STATE'
//        });
//      });
//
//      it('should not allow the user to enter an invalid state code', function () {
//        // @TODO - The form currently does allow this
//      });
//
//      it('should allow the user to enter a zip', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="zip" id="inputZipw9".*?ng-model="w9.zip"',
//          sendKeyValue: '45209',
//          labelText: 'ZIP CODE'
//        });
//      });
//
//      it('should allow the user to enter the requester name', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="requester_name" id="inputRequesterName".*?ng-model="w9.requester_name"',
//          sendKeyValue: 'Mr. Requester',
//          labelText: 'REQUESTER NAME'
//        });
//      });
//
//      it('should allow the user to enter an address', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="requester_address" id="inputRequesterAddress".*?ng-model="w9.requester_address"',
//          sendKeyValue: '1120 Requester Ave.',
//          labelText: 'REQUESTER ADDRESS'
//        });
//      });
//
//      it('should allow the user to enter an EIN or tax ID', function () {
//        // Invalid
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="ein" id="inputEin".*?ng-model="w9.ein"',
//          sendKeyValue: 'aaaaaaaa',
//          labelText: 'EMPLOYER IDENTIFICATION NUMBER (TAX ID)'
//        });
//        // Trigger validation error
//        formElements.get(51).click().then(function () {
//          // Check that validation error occurred
//          formHelper.assertNumberVisibleItems(1, '.help-block');
//          // Clear and write something valid
//          formHelper.clearInput(52);
//          formHelper.testStandardFormInputBox({
//            sendKeyValue: '12-1234567',
//            elementNumber: 52
//          });
//        });
//      });
//
//      it('should allow the user to enter an SSN', function () {
//        // Invalid
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="ssn" id="inputSSN".*?ng-model="w9.ssn"',
//          sendKeyValue: 'aaaaaaaa',
//          labelText: 'SSN'
//        });
//        // Trigger validation error
//        formElements.get(52).click().then(function () {
//          // Check that validation error occurred
//          formHelper.assertNumberVisibleItems(1, '.help-block');
//          // Clear and write something valid
//          formHelper.clearInput(53);
//          formHelper.testStandardFormInputBox({
//            sendKeyValue: '123-45-6789',
//            elementNumber: 53
//          });
//        });
//      });
//
//      it('should allow the user to cross out item 2 (not subject to backup withholding)', function () {
//        formHelper.testCheckbox(54);
//      });
//
//      it('should allow the user to sign the form', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="signature" id="inputSignature".*?ng-model="w9.signature" ',
//          sendKeyValue: 'MY SIGNATURE',
//          labelText: 'SIGNATURE OF U.S. PERSON'
//        });
//      });
//
//      it('should allow the user to initial the form', function () {
//        formHelper.testStandardFormInputBox({
//          sanityString: 'name="app_ini" id="inputAppIni".*?ng-model="w9.app_ini"',
//          sendKeyValue: 'MLE',
//          labelText: 'APPLICANT INITIALS'
//        });
//      });
//
//      it('should allow the user to input the date', function () {
//        // @TODO It currently does not allow the user to input the date...
//      });
//    });
//  });
//});
