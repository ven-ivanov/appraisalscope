'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AuthAppraiserSignUpDocumentUploadCtrl
 * @description
 * # AuthAppraiserSignUpDocumentUploadCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AuthAppraiserSignUpDocumentUploadCtrl', [
    '$scope', '$state', 'AuthAppraiserSignUpUploadService',

    function ($scope, $state, AuthAppraiserSignUpUploadService) {
      
      var vm = this;
      var baseCtrl = $scope.baseCtrl;

      /**
       * Upload appraiser license file
       * @param {Object} file
       */
      vm.uploadLicense = function (file) {
        // This is triggered on change, so ensure that a file is being input
        if (!file) {
          return;
        }

        // Import csv via upload service
        AuthAppraiserSignUpUploadService.uploadLicense(file).promise.then(function (response) { // jshint ignore:line
          // Eventual response
          $scope.$broadcast('show-modal', 'upload-license-successful');
        }, function (err) { // jshint ignore:line
          vm.uploadError = err.data;
          $scope.$broadcast('show-modal', 'upload-license-failure');
        });
      };

      /**
       * Upload appraiser resume
       * @param {Object} file
       */
      vm.uploadResume = function (file) {
        // This is triggered on change, so ensure that a file is being input
        if (!file) {
          return;
        }

        // Import csv via upload service
        AuthAppraiserSignUpUploadService.uploadResume(file).promise.then(function (response) { // jshint ignore:line
          // Eventual response
          $scope.$broadcast('show-modal', 'upload-resume-successful');
        }, function (err) { // jshint ignore:line
          vm.uploadError = err.data;
          $scope.$broadcast('show-modal', 'upload-resume-failure');
        });
      };

      /**
       * If ach form is required in system settings proceed to ach page if not save the form
       */
      vm.save = function () {
        if(baseCtrl.settings.requireAch) {
          $state.transitionTo('auth.appraiser-sign-up.step-6');
        } else {
          baseCtrl.saveForm();
        }
      };

      /**
       * Get back to previos page
       */
      vm.back = function () {
        $state.transitionTo('auth.appraiser-sign-up.step-4');
      };
    }
  ]);