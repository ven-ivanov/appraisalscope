'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AuthAppraiserSignUpBaseCtrl
 * @description
 * # AuthAppraiserSignUpBaseCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AuthAppraiserSignUpBaseCtrl', [
    'AuthAppraiserSignUpService', '$state',

    function (AuthAppraiserSignUpService, $state) {

      var vm = this;

      vm.userSelected = false;

      //create appraiser object if not created and proceed to first step
      if(!vm.appraiser) {
        vm.appraiser = {
          accountInformation: {},
          w9: {},
          coverages: {}
        };
        $state.transitionTo('auth.appraiser-sign-up.step-1');
      }

      //get system settings
      AuthAppraiserSignUpService.getSignUpSettings().$promise.then(function (res) {
        vm.settings = res.data;
        vm.dataLoaded = true;
      });

      /**
       * Save registration form
       */
      vm.saveForm = function () {
        AuthAppraiserSignUpService.saveForm(vm.appraiser).$promise.then(
          //success
          function () {

          },
          //error
          function () {

          }
        );
      };

      /**
       * Search for users by license number and certified state
       */
      vm.autoSearch = function () {
        var license = {
          licenseNumber: vm.appraiser.accountInformation.licenseNumber,
          state: vm.appraiser.accountInformation.certifiedState
        };

        vm.loadingUsers = true;
        vm.openAutoSearch = true;

        if(license.licenseNumber && license.state){
          AuthAppraiserSignUpService.getLicensedUsers(license).$promise.then(function (res) {

            vm.licensedUsers = res.data || null; //set licensed users
            vm.loadingUsers = false; //users loading ended
          });
        } else {
          vm.licensedUsers = null;
          vm.loadingUsers = false;
        }
      };

      /**
       * Select user from autoSearch dropdown
       */
      vm.selectUser = function (user) {

        //should be changed when statuses are provided from backend
        if(user) {
          vm.appraiser.accountInformation = user;
          vm.openAutoSearch = false;
          vm.userSelected = true;
        }
      };
    }
  ]);
