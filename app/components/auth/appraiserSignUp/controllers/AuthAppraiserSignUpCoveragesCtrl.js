'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AuthAppraiserSignUpCoveragesCtrl
 * @description
 * # AuthAppraiserSignUpCoveragesCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AuthAppraiserSignUpCoveragesCtrl', [
    '$scope', '$state',

    function ($scope, $state) {
      
      var vm = this;
      var baseCtrl = $scope.baseCtrl;

      vm.dataLoaded = true;

      /**
       * Go to new coverage page
       * @param {String} state
       */
      vm.addNewCoverage = function () {
        $state.transitionTo('auth.appraiser-sign-up.step-3-add-coverage');
      };

      /**
       * Go to coverage edit page
       * @param {String} state
       */
      vm.editCoverage = function (state) {
        $state.transitionTo('auth.appraiser-sign-up.step-3-edit-coverage', {state: state});
      };

      /**
       * Delete specified coverage
       * @param {String} state
       */
      vm.deleteCoverage = function (state) {
        delete baseCtrl.appraiser.coverages[state];
      };

      /**
       * Proceed to next step
       */
      vm.save = function () {
        $state.transitionTo('auth.appraiser-sign-up.step-4');
      };

      /**
       * Get back to previos page
       */
      vm.back = function () {
        $state.transitionTo('auth.appraiser-sign-up.step-2');
      };
    }
  ]);
