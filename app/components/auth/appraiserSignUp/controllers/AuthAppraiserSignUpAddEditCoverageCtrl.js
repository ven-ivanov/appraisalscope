'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AuthAppraiserSignUpAddEditCoverageCtrl
 * @description
 * # AuthAppraiserSignUpAddEditCoverageCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AuthAppraiserSignUpAddEditCoverageCtrl', [
    '$scope', '$state', '$stateParams', 'HandbookService', 'HANDBOOK_CATEGORIES', '$q', 'AuthAppraiserSignUpService', 'AuthAppraiserSignUpUploadService',

    function ($scope, $state, $stateParams, HandbookService, HANDBOOK_CATEGORIES, $q, AuthAppraiserSignUpService, AuthAppraiserSignUpUploadService) {

      var vm = this;
      var baseCtrl = $scope.baseCtrl;

      baseCtrl.appraiser.coverages = baseCtrl.appraiser.coverages ? baseCtrl.appraiser.coverages : {};
      vm.editState = false;
      vm.selectAll = {};

      //return if the certified state is not selected
      if(!baseCtrl.appraiser.accountInformation.certifiedState) {
        return;
      }

      //fill coverage form if in edit state
      if($state.current.name === 'auth.appraiser-sign-up.step-3-edit-coverage') {

        //if there is no coverage with specified state create one
        if(!baseCtrl.appraiser.coverages[$stateParams.state]) {
          vm.coverage = {
            state: $stateParams.state,
            licenseNumber: baseCtrl.appraiser.accountInformation.licenseNumber,
            licenseExp: baseCtrl.appraiser.accountInformation.licenseExp,
            certifications: baseCtrl.appraiser.accountInformation.certifications
          };
        } else {
          vm.coverage = baseCtrl.appraiser.coverages[$stateParams.state]; //else get that coverage
        }

        vm.editState = true;
      }

      //get states and primary licenses for slect boxes
      var states = HandbookService.query({category: HANDBOOK_CATEGORIES.states});
      var primaryLicenses = HandbookService.query({category: HANDBOOK_CATEGORIES.primaryLicenses});
        
      $q.all([
        states.$promise,
        primaryLicenses.$promise
      ]).then(function () {
        //filter states excluding used ones
        vm.usedStates = Object.keys(baseCtrl.appraiser.coverages);

        vm.states = [];
        angular.forEach(states.data, function (state) {
          if(vm.usedStates.indexOf(state.code) === -1 || state.code === $stateParams.state) {
            vm.states.push(state);
          }
        });
        
        vm.primaryLicenses = primaryLicenses.data;
        vm.dataLoaded = true;
      });

      //if in edit state
      if(vm.editState) {
        //get all countis for specifed state
        var counties = AuthAppraiserSignUpService.getCounties({state: vm.coverage.state});
        
        counties.$promise.then(function (res) {
          vm.counties = res.data;   
          vm.coverage.zipCodes = vm.coverage.zipCodes ? vm.coverage.zipCodes : {};
        });
        
      } else {
        //if not in edit state create new coverage object
        vm.coverage = { zipCodes: {}};

        /**
         * Selects a state
         * @param {String} state
         */
        vm.selectState = function(state) {
          if(!state) {
            return;
          }
          //get countis for selcted state
          var counties = AuthAppraiserSignUpService.getCounties({state: state});
          
          counties.$promise.then(function (res) {
            vm.counties = res.data;
            
            vm.selectAll.counties = false; //uncheck "select all counties" checkbox
          });
        };
      }

      /**
       * Selects a county
       * @param {String} county
       * @param {Boolean} checked
       */
      vm.selectCounty = function(county, checked) {
        //if checked get all zipCodes for the county
        if(checked) {
          //get all zipCodes
          var zipCodes = AuthAppraiserSignUpService.getZipCodes({state: vm.coverage.state, county: county});
          
          zipCodes.$promise.then(function (res) {
            vm.coverage.zipCodes[county] = {};

            angular.forEach(res.data, function (zipCode) {
              vm.coverage.zipCodes[zipCode.county][zipCode.zipCode] = zipCode.zipCode;
            });

            //if all counties are selected check "select all counties" checkbox
            if(Object.keys(vm.coverage.zipCodes).length === vm.counties.length) {
              vm.selectAll.counties = true;
            }
          });
        } else {
          //else delete county from coverage
          delete vm.coverage.zipCodes[county];
          vm.selectAll.counties = false;
        }
      };

      /**
       * Selects all counties
       * @param {Boolean} checked
       */
      vm.selectAllCounties = function(checked) {
        //if checked get all zipCodes for all counties
        if(checked) {

          //get all zipCodes
          var zipCodes = AuthAppraiserSignUpService.getZipCodes({state: vm.coverage.state});
          
          zipCodes.$promise.then(function (res) {

            angular.forEach(res.data, function (zipCode) {
              //create county object if not created yet
              if(typeof vm.coverage.zipCodes[zipCode.county] === 'undefined') {
                vm.coverage.zipCodes[zipCode.county] = {};
              }
              vm.coverage.zipCodes[zipCode.county][zipCode.zipCode] = zipCode.zipCode;
            });
          });
        } else {
          //else empty covearge zipcodes
          vm.coverage.zipCodes = {};
        }

        //check all county checkboxes
        angular.forEach(vm.counties, function (county) {
          county.val = checked;
        });
      };

      /**
       * Show zip codes modal window for specified county
       * @param {String} county
       * @param {Int} countyKey
       */
      vm.showZipCodes = function(county, countyKey) {
        //get all zipCodes
        var zipCodes = AuthAppraiserSignUpService.getZipCodes({state: vm.coverage.state, county: county});
        
        zipCodes.$promise.then(function (res) {
          vm.selectedZipCodes = res.data;
          vm.zipModalShow = true;
          vm.selectedCounty = {
            county: county,
            key: countyKey
          };

          //check if all zipCodes are selected
          vm.selectAll.zipCodes = Object.keys(vm.coverage.zipCodes[county]).length === vm.selectedZipCodes.length;
        });

      };

      /**
       * Select a zip code
       * @param {String} county
       * @param {String} zipCode
       * @param {Boolean} countyKey
       */
      vm.selectZipCode = function (county, zipCode, checked) {
        //if zipCode is checked
        if(checked) {
          //check county
          vm.counties[vm.selectedCounty.key].val = true;
          
          //check if all counties are selected
          vm.selectAll.counties = Object.keys(vm.coverage.zipCodes).length === vm.counties.length;

          //add zipCode to coverage
          vm.coverage.zipCodes[county][zipCode] = zipCode;

          //check if all zipCodes are selected
          vm.selectAll.zipCodes = Object.keys(vm.coverage.zipCodes[county]).length === vm.selectedZipCodes.length;

        } else {
          //uncheck "select all zipCodes"
          vm.selectAll.zipCodes = false;
          //delete specified zipCode
          delete vm.coverage.zipCodes[county][zipCode];
          
          //delete whole county if there is no selected zipCodes anymore
          if (!Object.keys(vm.coverage.zipCodes[county]).length) {
            vm.coverage.zipCodes[county] = {};
            vm.counties[vm.selectedCounty.key].val = false;
            vm.selectAll.counties = false;
          }
        }
      };

      /**
       * Select all zipCodes
       * @param {String} county
       * @param {Boolean} countyKey
       */
      vm.selectAllZipCodes = function (county, checked) {
        //add all zipCode to coverage
        angular.forEach(vm.selectedZipCodes, function (zipCode) {
          zipCode.val = checked;
          if (checked) {
            vm.coverage.zipCodes[county][zipCode.zipCode] = zipCode.zipCode;
          }
        });

        if(!checked) {
          //delete whole county
          vm.coverage.zipCodes[county] = {};
          vm.counties[vm.selectedCounty.key].val = false;
          vm.selectAll.counties = false;
        } else {
          vm.counties[vm.selectedCounty.key].val = true;

          //check if all counties are selected
          if(Object.keys(vm.coverage.zipCodes).length === vm.counties.length) {
            vm.selectAll.counties = true;
          }
        }
      };

      /**
       * Upload license file
       * @param {Object} file
       */
      vm.uploadLicense = function (file) {
        // This is triggered on change, so ensure that a file is being input
        if (!file) {
          return;
        }

        AuthAppraiserSignUpUploadService.coverageLicenseUpload(file).promise.then(function (response) { // jshint ignore:line
          // Eventual response
          $scope.$broadcast('show-modal', 'upload-license-successful');
        }, function (err) { // jshint ignore:line
          vm.uploadError = err.data;
          $scope.$broadcast('show-modal', 'upload-license-failure');
        });
      };

      /**
       * Save coverage and go to next page
       */
      vm.save = function () {
        baseCtrl.appraiser.coverages[vm.coverage.state] = vm.coverage;
        baseCtrl.appraiser.coverages[vm.coverage.state].counties = Object.keys(vm.coverage.zipCodes);

        $state.transitionTo('auth.appraiser-sign-up.step-3-coverages');
      };

      /**
       * Get back to previos page
       */
      vm.back = function () {
        //if there is no coverage get to step 2, else go to coverages page
        if(!baseCtrl.appraiser.coverages[baseCtrl.appraiser.accountInformation.certifiedState]) {
          $state.transitionTo('auth.appraiser-sign-up.step-2');
        } else {
          $state.transitionTo('auth.appraiser-sign-up.step-3-coverages');
        }
      };
    }
  ]);
