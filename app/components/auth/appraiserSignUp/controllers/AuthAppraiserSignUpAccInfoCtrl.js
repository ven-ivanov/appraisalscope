'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AuthAppraiserSignUpAccInfoCtrl
 * @description
 * # AuthAppraiserSignUpAccInfoCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AuthAppraiserSignUpAccInfoCtrl', [
    '$state', 'HandbookService', 'HANDBOOK_CATEGORIES', '$q',
    
    function ($state, HandbookService, HANDBOOK_CATEGORIES, $q) {
      
      var vm = this;

      vm.dataLoaded = true;
      vm.openAutoSearch = false;
      vm.loadingUsers = true;

      var states = HandbookService.query({category: HANDBOOK_CATEGORIES.states});
      var accountTypes = HandbookService.query({category: HANDBOOK_CATEGORIES.accountTypes});
      var primaryLicenses = HandbookService.query({category: HANDBOOK_CATEGORIES.primaryLicenses});

      /**
       * Get select boxes data
       */
      $q.all([
        states.$promise,
        accountTypes.$promise,
        primaryLicenses.$promise
      ]).then(function () {
        vm.states = states;
        vm.accountTypes = accountTypes;
        vm.primaryLicenses = primaryLicenses;
        vm.dataLoaded = true;
      });

      /**
       * Save account information
       */
      vm.save = function () {
        $state.transitionTo('auth.appraiser-sign-up.step-2');
      };

    }
  ]);
