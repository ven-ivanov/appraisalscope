'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AuthAppraiserSignUpAchCtrl
 * @description
 * # AuthAppraiserSignUpAchCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AuthAppraiserSignUpAchCtrl', [
    '$scope', '$state',

    function ($scope, $state) {
      
      var vm = this;
      var baseCtrl = $scope.baseCtrl;

      vm.dataLoaded = true;

      /**
       * Call registraion save function from base controller
       */
      vm.save = function () {
        baseCtrl.saveForm();
      };

      /**
       * Get back to previos page
       */
      vm.back = function () {
        $state.transitionTo('auth.appraiser-sign-up.step-5');
      };
    }
  ]);
