'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AuthAppraiserSignUpW9FormCtrl
 * @description
 * # AuthAppraiserSignUpW9FormCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AuthAppraiserSignUpW9FormCtrl', [
    '$state', '$scope',

    function ($state, $scope) {
      var vm = this;
      var baseCtrl = $scope.baseCtrl;

      /**
       * Validate if only one of tax classifications is selected
       * @param {Array} taxClassifications
       * @return {Booelan} isValid
       */
      vm.validateTaxClassifications =  function (taxClassifications) {
        var seleted = false;
        angular.forEach(taxClassifications, function (v) {
          if(v === true) {
            seleted = true; 
            return;
          }
        });
        if(!seleted) {
          vm.taxClassificationError = true;
          return false;
        }

        vm.taxClassificationError = false;
        return true;
      };

      /**
       * Save w9 form if there is no errors
       * @param {Object} coverages
       * @param {String} certifiedState
       */
      vm.save = function (coverages, certifiedState) {
        if(vm.taxClassificationError) {
          return;
        }

        if(!coverages[certifiedState]) {
          $state.transitionTo('auth.appraiser-sign-up.step-3-edit-coverage', {state: certifiedState});
        } else {
          $state.transitionTo('auth.appraiser-sign-up.step-3-coverages');
        }
      };

      /**
       * Get back to previos page
       */
      vm.back = function () {
        $state.transitionTo('auth.appraiser-sign-up.step-1');
      };

      /**
       * Select a tax classification
       * @param {String} key
       */
      vm.selectTaxClassification = function (key) {
        angular.forEach(baseCtrl.appraiser.w9.taxClassification, function (v, k) {
          baseCtrl.appraiser.w9.taxClassification[k] = false;
        });
        baseCtrl.appraiser.w9.taxClassification[key] = true;
      };

      vm.dataLoaded = true;
    }
  ]);
