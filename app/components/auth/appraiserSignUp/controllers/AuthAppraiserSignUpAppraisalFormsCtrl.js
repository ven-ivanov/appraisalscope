'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AuthAppraiserSignUpAppraisalFormsCtrl
 * @description
 * # AuthAppraiserSignUpAppraisalFormsCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('AuthAppraiserSignUpAppraisalFormsCtrl', [
    '$scope', '$state', 'AuthAppraiserSignUpService',

    function ($scope, $state, AuthAppraiserSignUpService) {

      var vm = this;

      AuthAppraiserSignUpService.getAppraisalForms().$promise.then(function (res) {
        vm.jobTypes = res.data;
        vm.dataLoaded = true;
      });

      /**
       * Get to next page
       */
      vm.save = function () {
        $state.transitionTo('auth.appraiser-sign-up.step-5');
      };

      /**
       * Get back to previos page
       */
      vm.back = function () {
        $state.transitionTo('auth.appraiser-sign-up.step-3-coverages');
      };

    }
  ]);
