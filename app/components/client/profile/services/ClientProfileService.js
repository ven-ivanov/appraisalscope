'use strict';

/**
 * @ngdoc service
 * @name frontendApp.ClientProfileService
 * @description
 * # ClientProfileService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('ClientProfileService', [
    '$resource', 'API_PREFIX',

    function ($resource, API_PREFIX) {
      return $resource(API_PREFIX() + '/v2.0/clients/profile/:userId', {}, {
        update: {
          method: 'PUT'
        }
      });
    }
  ]);
