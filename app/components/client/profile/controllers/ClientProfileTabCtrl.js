'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:ClientProfileTabCtrl
 * @description
 * # ClientProfileTabCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('ClientProfileTabCtrl', ['$scope', '$location', function ($scope, $location) {

  	var vm = this;

    vm.tabs = [
      {link: 'main.profile.show', label: 'Preview Profile'},
      {link: 'main.profile.edit', label: 'Edit Your Profile'}
    ];

    vm.setSelectedTab = function (tab) {
      $scope.selectedTab = tab;
    };

    vm.tabClass = function (tab) {
      if ($scope.selectedTab === tab) {
        return 'active';
      } else {
        return '';
      }
    };

    /**
     *		Set selected tab on initial load
     */
    var label = $location.url().split('/')[4];

    angular.forEach(vm.tabs,function (element, index) {
      var thisLabel = element.link.split('.')[3];

      if (label === thisLabel) {
      	vm.setSelectedTab(vm.tabs[index]);
        return;
      }
    });
  }]);
