'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AppraiserProfileCtrl
 * @description
 * # AppraiserProfileCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('ClientProfileCtrl', [
    '$scope', '$state', 'SessionService', 'HandbookService', 'HANDBOOK_CATEGORIES', '$q', 'ClientProfileService',

    function ($scope, $state, SessionService, HandbookService, HANDBOOK_CATEGORIES, $q, ClientProfileService) {

      var vm = this;

      var states = HandbookService.query({category: HANDBOOK_CATEGORIES.states});
      var accountTypes = HandbookService.query({category: HANDBOOK_CATEGORIES.accountTypes});
      var profile = ClientProfileService.get({userId: SessionService.user.id});

      /*
       * Waits for all queries to be processed.
       */
      $q.all([
        states.$promise,
        accountTypes.$promise,
        profile.$promise
      ]).then(function () {
        vm.states = states;
        vm.accountTypes = accountTypes;
        vm.profile = profile;
        vm.state = profile.state;
        vm.dataLoaded = true;
      });

      vm.update = function () {
        return ClientProfileService.update({userId: SessionService.user.id}, profile, function () {
          delete profile.password;
        }).$promise;
      };
    }
  ]);
