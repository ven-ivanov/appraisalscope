'use strict';

/**
 * @ngdoc service
 * @name frontendApp.ClientContactUsService
 * @description
 * # ClientContactUsService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('ClientContactUsService', [
    '$resource', 'API_PREFIX',

    function ($resource, API_PREFIX) {
      return $resource(API_PREFIX() + '/v2.0/company/contactus/',{},{
      	getAll:{method:'GET'}
      });
    }
  ]);
