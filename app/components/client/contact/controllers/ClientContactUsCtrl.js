'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller: ClientContactusCtrl
 * @description
 * # ClientContactusCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp' )
.controller('ClientContactUsCtrl', [
   '$scope', 'ClientContactUsService', 'SessionService', '$q',

   function ($scope, ClientContactUsService, SessionService, $q) {

     var contacts = ClientContactUsService.getAll();
     var vm = this;

     $q.all([
       contacts.$promise
     ]).then(function () {
       vm.dataLoaded = true;
       vm.contactData = contacts.data;
     });

  }
]);
