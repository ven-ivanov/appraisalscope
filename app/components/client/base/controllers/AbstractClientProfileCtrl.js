'use strict';

angular.module('frontendApp')
.controller('AbstractClientProfileCtrl', ['$scope', function ($scope) {
  $scope.$on('child-state-init', function (event, state) {
    $scope.state = state;
  });
}]);