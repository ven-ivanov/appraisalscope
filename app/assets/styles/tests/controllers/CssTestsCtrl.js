/**
 * Created by dvabuzyarov on 6/1/15.
 */
'use strict';

var app = angular.module('frontendApp');

app.controller('CssTestsCtrl',
  [function () {

    var vm = this;
    vm.buttons = [
      {icon:'close',   label:'close'},
      {icon:'cancel',  label:'cancel'},
      {icon:'apply',   label:'apply'},
      {icon:'pin',     label:'view on map'},
      {icon:'back',    label:'back to step 1'},
      {icon:'bell',    label:'add notification'},
      {icon:'company', label:'new company'},
      {icon:'details', label:'user details'},
      {icon:'file',    label:'generate invoice'},
      {icon:'money',   label:'pay multi'},
      {icon:'upload',  label:'import checks'},
      {icon:'download',label:'export'},
      {icon:'plus',     label:'new appraisal'},
      {icon:'all',     label:'view all'},
      {icon:'money2',  label:'view all'},
      {icon:'user',    label:'view all'},
      {icon:'settings', label:'settings'},
      {icon:'search', label:'search'}
    ];

    vm.buttons_themes = [
      'grey-theme',
      'green-theme',
      'blue-theme'
    ];

  }]);
