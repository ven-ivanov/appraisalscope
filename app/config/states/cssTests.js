/**
 * Created by dvabuzyarov on 6/1/15.
 */
'use strict';

/**
 * states for e2e testing
 */
angular.module('frontendApp')
  .config(['$stateProvider', 'ROLES', function ($stateProvider, ROLES) {
    $stateProvider
      .state('css-tests', {
        url: '/tests/css',
        templateUrl: '/tests/css/partials/css-tests.html',
        controller: 'CssTestsCtrl',
        controllerAs: 'vm',
        data: {
          authorizedTypes: [ROLES.all]
        }
      });
  }]);
