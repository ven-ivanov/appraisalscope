'use strict';

var app = angular.module('frontendApp');
/**
 * All the shared states are placed here
 */

app.config([
  '$stateProvider', '$urlRouterProvider', 'ROLES',

  function ($stateProvider, $urlRouterProvider, ROLES) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('main', {
        url: '',
        controller: 'MainCtrl',
        templateUrl: '/shared/components/base/views/base.html',
        data: {
          authorizedTypes: [ROLES.all]
        }
      })

      // Orders Page
      .state('main.orders', {
        url: '/orders/:activeTab/:appraisalId/:activePreviewTab/',
        templateUrl: '/components/orders/views/base.html',
        params:  {
          activeTab: {
            value: null
          },
          appraisalId: {
            value: '',
            squash: true
          },
          activePreviewTab: {
            value: '',
            squash: true
          }
        }
      })

      // Appraisal Details Page
      .state('main.appraisal', {
        url: '/appraisal/:appraisalId',
        templateUrl: '/components/appraisal/views/base.html'
      })

      // Admin Dashboard
      .state('main.dashboard', {
        url: '/dashboard',
        templateUrl: '/components/admin/dashboard/views/dashboard.html',
        controller: 'AdminDashboardCtrl'
      })
      .state('main.dashboard.accounting', {
        url: '/accounting',
        templateUrl: '/components/admin/dashboard/views/dashboard-accounting.html',
        controller: 'AdminDashboardAccountingCtrl'
      })
      .state('main.dashboard.activities', {
        url: '/activities',
        templateUrl: '/components/admin/dashboard/views/dashboard-activities.html',
        controller: 'AdminDashboardActivitesCtrl'
      })

      // Admin settings
      .state('main.settings', {
        url: '/settings',
        templateUrl: '/components/admin/settings/views/base.html',
        controller: 'AdminSettingsTabsCtrl'
      })
      .state('main.settings.invoice', {
        url: '/invoice',
        templateUrl: '/components/admin/settings/views/invoice.html',
        controller: 'AdminSettingsInvoiceCtrl'
      })
      .state('main.settings.companyInformation', {
        url: '/companyInformation',
        templateUrl: '/components/admin/settings/views/companyInformation.html',
        controller: 'AdminSettingsCompanyInformationCtrl'
      })
      .state('main.settings.file', {
        url: '/file',
        templateUrl: '/components/admin/settings/views/file.html',
        controller: 'AdminSettingsFileCtrl'
      })
      .state('main.settings.reminders', {
        url: '/reminders',
        templateUrl: '/components/admin/settings/views/reminders.html',
        controller: 'AdminSettingsRemindersCtrl'
      })
      .state('main.settings.options', {
        url: '/options',
        templateUrl: '/components/admin/settings/views/options.html',
        controller: 'AdminSettingsOptionsCtrl'
      })
      .state('main.settings.instructions', {
        url: '/instructions',
        templateUrl: '/components/admin/settings/views/instructions.html',
        controller: 'AdminSettingsInstructionsCtrl'
      })
      .state('main.settings.email', {
        url: '/email',
        templateUrl: '/components/admin/settings/views/email.html',
        controller: 'AdminSettingsEmailCtrl'
      })
      .state('main.settings.amcLicense', {
        url: '/amcLicense',
        templateUrl: '/components/admin/settings/views/amcLicense.html',
        controller: 'AdminSettingsAmcLicenseCtrl'
      })
      .state('main.settings.ucdp', {
        url: '/ucdp',
        templateUrl: '/components/admin/settings/views/ucdp.html',
        controller: 'AdminSettingsUCDPCtrl'
      })
      .state('main.settings.predefinedMessages', {
        url: '/predefinedMessages',
        templateUrl: '/components/admin/settings/views/predefinedMessages.html',
        controller: 'AdminSettingsPredefinedMessagesCtrl'
      })
      .state('main.settings.documentUpload', {
        url: '/documentUpload',
        templateUrl: '/components/admin/settings/views/documentUpload.html',
        controller: 'AdminSettingsDocumentUploadCtrl'
      })
      .state('main.settings.importUser', {
        url: '/importUser',
        templateUrl: '/components/admin/settings/views/importUser.html',
        controller: 'AdminSettingsImportUserCtrl'
      })
      .state('main.settings.importClient', {
        url: '/importClient',
        templateUrl: '/components/admin/settings/views/importClient.html',
        controller: 'AdminSettingsImportClientCtrl'
      })
      .state('main.settings.qcReview', {
        url: '/qcReview',
        templateUrl: '/components/admin/settings/views/qcReview.html',
        controller: 'AdminSettingsQCReviewCtrl'
      })
      .state('main.settings.notification', {
        url: '/notification',
        templateUrl: '/components/admin/settings/views/notification.html',
      })

      .state('main.accounting',{
        url: '/accounting/:activeTab/:page',
        templateUrl: '/components/admin/accounting/views/accounting.html',
        controller: 'AdminAccountingCtrl',
        controllerAs: 'adminAccountingCtrl'
      })



    /**
     * Placeholder states
     */
      .state('main.users',{
      url: '/users/:activeTab/:recordId/:details/:page',
        templateUrl: '/components/admin/users/main/views/users.html',
        controller: 'AdminUsersCtrl',
        controllerAs: 'adminUsersBaseCtrl'
      })

      .state('main.reports',{
        url: '/reports/:activeTab?list-reports&statistics&create-new&edit-report&generate-view',
        templateUrl: '/components/admin/reports/views/reports-new.html',
        controller: 'AdminReportsCtrl',
        controllerAs: 'adminReportsCtrl'
      })
      .state('main.staff',{
        url: '/staff/:activeTab?staff&admin-rule-profile&reviewer-rule-profile&realec-failure-orders&mercury-failure-orders',
        templateUrl: '/components/admin/staff/main/views/staff.html',
        controller: 'AdminStaffCtrl',
        controllerAs:'adminStaffCtrl'
      })

      // Admin Job Types
      .state('main.jobType',{
        abstract: true,
        url: '/jobType',
        templateUrl: '/components/admin/jobType/views/base.html',
        controller: 'AdminJobTypeMainCtrl'
      })
      .state('main.jobType.listing',{
        url: '/listing',
        templateUrl: '/components/admin/jobType/views/jobType.html',
        controller: 'AdminJobTypeCtrl'
      })
      .state('main.jobType.jobTypeList',{
        url: '/jobTypeList',
        templateUrl: '/components/admin/jobType/views/jobTypeList.html',
        controller: 'AdminJobTypeListCtrl'
      })
      .state('main.jobType.jobTypeMapping',{
          url: '/jobTypeMapping',
          templateUrl: '/components/admin/jobType/views/jobTypeMapping.html',
          controller: 'AdminJobTypeCtrl'
      })
      .state('main.jobType.setFees',{
        url: '/setFees/:jobTypeListId?jobTypeId',
        templateUrl: '/components/admin/jobType/views/setFeesByLocation.html',
        controller: 'AdminSetFeesByLocation'
      })
      .state('main.jobType.import',{
        url: '/import',
        templateUrl: '/components/admin/jobType/views/importFee.html',
        controller: 'AdminImportFeeCtrl'
      })

      // Client Profile
      .state('main.profile', {
        abstract: true,
        url: '/profile',
        templateUrl: '/components/client/profile/views/base.html',
        controller: 'AbstractClientProfileCtrl'
      })
      .state('main.profile.show', {
        url: '/show',
        templateUrl: '/components/client/profile/views/show.html',
        controller: 'ClientProfileCtrl'
      })
      .state('main.profile.edit', {
        url: '/edit',
        templateUrl: '/components/client/profile/views/edit.html',
        controller: 'ClientProfileCtrl'
      })

      .state('main.contactus', {
        url: '/contactus',
        templateUrl: '/components/client/contact/views/contact-us.html',
        controller: 'ClientContactUsCtrl'
      });
  }
]);
