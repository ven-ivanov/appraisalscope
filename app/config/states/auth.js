'use strict';

/**
 * All the authentication related state are placed here
 */
angular.module('frontendApp')
  .config(['$stateProvider', 'ROLES', function ($stateProvider, ROLES) {
    $stateProvider
      // --------------
      // Authentication
      // --------------
      .state('auth', {
        abstract: true,
        url: '',
        templateUrl: '/components/auth/base/views/base.html',
        controller: 'AuthTabsCtrl',
        data: {
          authorizedTypes: [ROLES.all]
        }
      })
      .state('auth.sign-in', {
        url: '/sign-in',
        templateUrl: '/components/auth/authentication/views/sign-in.html',
        controller: 'AuthSignInCtrl'
      })
      .state('auth.sign-out', {
        url: '/sign-out',
        templateUrl: '/components/auth/authentication/views/sign-out.html',
        controller: 'AuthSignOutCtrl'
      })
      .state('auth.reset-password', {
        url: '/reset-password',
        templateUrl: '/components/auth/password/views/reset-password.html',
        controller: 'AuthResetPasswordCtrl'
      })
      .state('auth.new-password', {
        url: '/new-password/:token',
        templateUrl: '/components/auth/password/views/new-password.html',
        controller: 'AuthNewPasswordCtrl'
      })
      .state('auth.client-sign-up', {
        url: '/client-sign-up',
        templateUrl: '/components/auth/clientSignUp/views/client-sign-up.html',
        controller: 'AuthClientSignUpCtrl'
      })
      .state('auth.unauthorized', {
        url: '/unauthorized',
        templateUrl: '/components/auth/authentication/views/unauthorized.html'
      })


      // --------------
      // Appraiser Sign Up
      // --------------
      .state('auth.appraiser-sign-up', {
        abstract: true,
        url: '/appraiser-sign-up',
        templateUrl: '/components/auth/appraiserSignUp/views/base.html',
        controller: 'AuthAppraiserSignUpBaseCtrl'
      })
      .state('auth.appraiser-sign-up.step-1', {
        url: '/step-1',
        templateUrl: '/components/auth/appraiserSignUp/views/step-1-account-information.html',
        controller: 'AuthAppraiserSignUpAccInfoCtrl'
      })
      .state('auth.appraiser-sign-up.step-2', {
        url: '/step-2',
        templateUrl: '/components/auth/appraiserSignUp/views/step-2-w9-form.html',
        controller: 'AuthAppraiserSignUpW9FormCtrl'
      })
      .state('auth.appraiser-sign-up.step-3-coverages', {
        url: '/step-3-coverages',
        templateUrl: '/components/auth/appraiserSignUp/views/step-3-coverages.html',
        controller: 'AuthAppraiserSignUpCoveragesCtrl'
      })
      .state('auth.appraiser-sign-up.step-3-add-coverage', {
        url: '/step-3-add-edit-coverage',
        templateUrl: '/components/auth/appraiserSignUp/views/step-3-add-edit-coverage.html',
        controller: 'AuthAppraiserSignUpAddEditCoverageCtrl'
      })
      .state('auth.appraiser-sign-up.step-3-edit-coverage', {
        url: '/step-3-add-edit-coverage/:state',
        templateUrl: '/components/auth/appraiserSignUp/views/step-3-add-edit-coverage.html',
        controller: 'AuthAppraiserSignUpAddEditCoverageCtrl'
      })
      .state('auth.appraiser-sign-up.step-4', {
        url: '/step-4',
        templateUrl: '/components/auth/appraiserSignUp/views/step-4-appraisal-forms.html',
        controller: 'AuthAppraiserSignUpAppraisalFormsCtrl'
      })
      .state('auth.appraiser-sign-up.step-5', {
        url: '/step-5',
        templateUrl: '/components/auth/appraiserSignUp/views/step-5-document-upload.html',
        controller: 'AuthAppraiserSignUpDocumentUploadCtrl'
      })
      .state('auth.appraiser-sign-up.step-6', {
        url: '/step-6',
        templateUrl: '/components/auth/appraiserSignUp/views/step-6-ach-info.html',
        controller: 'AuthAppraiserSignUpAchCtrl'
      });
  }]);
