'use strict';

/**
 * @ngdoc overview
 * @name frontendApp
 * @description
 * # frontendApp
 *
 * Main module of the application.
 */
angular
  .module('frontendApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ui.router',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap.showErrors',
    'smart-table',
    'as-grid',
    'as-tree-view',
    'lrDragNDrop',
    'ng-sortable',
    'xeditable',
    'angularFileUpload',
    'ascopeShared',
    'ngProgress',
    'ngTraverse',
    'angularjs-dropdown-multiselect',
    'ngCkeditor',
    'ngTagsInput',
    'ui.tree',
    'ui.map'
  ]).config(['$httpProvider', '$locationProvider', '$urlMatcherFactoryProvider',
     function ($httpProvider, $locationProvider, $urlMatcherFactoryProvider) {
       // Case insensitive states
       $urlMatcherFactoryProvider.caseInsensitive(true);
       // Continue to match if a trailing slash is accidentally left in
       $urlMatcherFactoryProvider.strictMode(false);

       // html 5 mode getting rid of #
       $locationProvider.html5Mode(true);

       $httpProvider.interceptors.push(['$injector', function ($injector) {
         return $injector.get('AuthInterceptorService');
       }]);
     }])
  .run(function(editableOptions) {
    // bootstrap3 theme. Can be also 'bs2', 'default'
    editableOptions.theme = 'bs3';
  });
