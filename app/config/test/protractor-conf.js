exports.config = {
  allScriptsTimeout: 11000,

  specs: [
    '../../../app/**/e2e/*.js'
  ],

  capabilities: {
    'browserName': 'chrome'
  },

  chromeOnly: true,

  baseUrl: 'http://clientx.appraisalscope.local:9001/',

  framework: 'jasmine',

  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000
  },

  onPrepare: function() {
    // At this point, global variable 'protractor' object will be set up, and
    // globals from the test framework will be available. For example, if you
    // are using Jasmine, you can add a reporter with:
    //     jasmine.getEnv().addReporter(new jasmine.JUnitXmlReporter(
    //         'outputdir/', true, true));
    global.registeredUsers = [];
    global.UserHelper = require('./e2e/helpers/userHelper');
    global.FormHelper = require('./e2e/helpers/formHelper');
    global.UrlHelper = require('./e2e/helpers/urlHelper');
    global.HttpBackend = require('httpbackend');
    global.LoginHelper = require('./e2e/helpers/loginHelper');
    global.Helper = require('./e2e/helpers/helper');
    global.AdminAccountingHelper = require('./e2e/helpers/adminAccountingHelper');

    // Get output of console.log, so we can see what's going on
    afterEach(function() {
      browser.manage().logs().get('browser').then(function(browserLog) {
        if (browserLog.length) {
          console.log('log: ' + require('util').inspect(browserLog));
        }
      });
    });
  }
};
