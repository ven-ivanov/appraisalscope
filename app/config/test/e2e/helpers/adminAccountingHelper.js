'use strict';

module.exports = function () {
  return {
    /**
     * Get reference to a specific record
     * @param record
     * @returns {*}
     */
    getRecord: function (record) {
      return $$('as-table tbody tr').get(record);
    },
    /**
     * Select a table record
     */
    checkRecord: function (record) {
      this.getRecord(record).$('input').click();
    },
    /**
     * Get checked status of checkbox
     */
    getCheckStatus: function (record) {
      return $$('as-table tbody tr').get(record).$('input').getAttribute('checked');
    },
    /**
     * Click a function button above the table
     */
    clickFunctionButton: function (button) {
      $$('.dashboard-search').get(1).$$('button').get(button).click();
    },
    /**
     * Match a cell's text again regex
     * @param cellNumber
     * @param regex
     */
    matchCell: function (cellNumber, regex) {
      var row = 0;
      // Set a row
      if (arguments.length === 3) {
        row = arguments[2];
      }
      expect($$('as-table tbody tr').get(row).$$('td').get(cellNumber).getText()).toMatch(regex);
    }
  };
};