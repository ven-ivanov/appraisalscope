'use strict';

module.exports = function () {
  return {
    /**
     * Blocking wait for a promise to resolve before continuing
     *
     * Note that testFn must return a truth value
     *
     * @param promiseFn
     * @param testFn
     */
    browserWait: function (promiseFn, testFn) {
      browser.wait(function () {
        var deferred = protractor.promise.defer();
        promiseFn().then(function (data) {
          deferred.fulfill(testFn(data));
        });
        return deferred.promise;
      });
    },
    /**
     * Wait until element is shown
     * @param selector
     */
    elementShown: function (selector) {
      browser.wait(protractor.until.elementIsVisible(selector));
    },
    /**
     * Wait until element is hidden
     * @param selector
     */
    elementHidden: function (selector) {
      browser.wait(protractor.until.elementIsNotVisible(selector));
    }
  };
};