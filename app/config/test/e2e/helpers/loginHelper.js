'use strict';

module.exports = function () {
  return {
    /**
     * Login to the application
     * @param userType
     */
    login: function (userType, backend, urlHelper) {
      // Login admin
      if (userType === 'admin') {
        // Allow the sign in request to complete successfully
        backend.whenPOST(/.*/).passThrough();
        // Sign in
        browser.get('/#/auth/sign-out');
        browser.wait(protractor.until.elementIsVisible($('.auth-page-wrapper')));
        // Complete sign in elements
        var formElements = element.all(by.css('.form-group input'));
        expect(formElements.count()).toBe(2);
        formElements.get(0).sendKeys('admin');
        formElements.get(1).sendKeys('a');
        // Sign in
        $('button').click();
      }
    }
  };
};