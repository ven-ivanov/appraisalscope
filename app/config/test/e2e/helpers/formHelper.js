'use strict';

module.exports = function () {
  var labelDepth = 1,
      elementNumber = 0,
      formElements;
  return {
    // Set to 1 if the label is a sibling of the input element, 2 if a sibling of the parent, etc.
    get labelDepth() {
      return labelDepth;
    },
    // The element in the collection of elements currently being examined
    get elementNumber() {
      return elementNumber;
    },
    set elementNumber(value) {
      elementNumber = value;
    },
    // Collection of form elements being examined
    get formElements() {
      return formElements;
    },
    set formElements(value) {
      formElements = value;
    },
    /**
     * Assert that there are a specific number of visible items
     * @param elementCount
     */
    assertNumberVisibleItems: function (elementCount, selector) {
      var elementDisplayedCount = 0;
      element.all(by.css(selector)).each(function (item) {
        item.isDisplayed().then(function (isVisible) {
          if (isVisible) {
            elementDisplayedCount++;
          }
        });
      }).then(function () {
        expect(elementDisplayedCount).toBe(elementCount);
      });
    },
    /**
     * Get all form elements on a page which conform to the CSS selector
     * @param selector
     * @returns {*}
     */
    getFormItems: function (selector) {
      return element.all(by.css(selector));
    },
    /**
     * Reset set of form elements for the next set of elements being tested
     */
    resetFormElements: function () {
      this.formElements = null;
      this.elementNumber = 0;
    },
    /**
     * Very basic sanity checking for each form element
     */
    htmlSanityCheck: function (element, matcher) {
      // Sanity check
      var regex = RegExp(matcher);
      expect(element.getOuterHtml()).toMatch(regex);
    },
    /**
     * Basic test to send keys and check that the input item is written to
     */
    sendKeysAndCheck: function (element, inputText) {
      element.sendKeys(inputText);
      expect(element.getAttribute('value')).toBe(inputText);
    },
    /**
     * Verify that the label appears as it is supposed to be
     * @param elem
     * @param labelText
     * @param levels
     */
    checkLabel: function (elem, labelText, levels) {
      levels = levels || 1;
      // Create the number of parent levels to go up, defaulting to 1
      var parentXpath = Array.apply(null, Array(levels)).map(function(){return '../'}).join('');
      parentXpath = parentXpath.substr(0, parentXpath.length - 1);
      // Try to get by ID
      elem.getAttribute('id').then(function (elementId) {
        if (elementId) {
          return expect(element(by.id(elementId))
          .element(by.xpath(parentXpath))
          .element(by.css('label'))
          .getText())
          .toBe(labelText);
        }
        // Try to get by model
        elem.getAttribute('ng-model').then(function (modelName) {
          if (modelName) {
            return expect(element(by.model(modelName))
            .element(by.xpath(parentXpath))
            .element(by.css('label'))
            .getText())
            .toBe(labelText);
          }
          // If still unable to get, throw error
          throw Error('Unable to verify text');
        });
      });
    },
    /**
     * Sets number of element being processed, as well as the collection of form elements itself
     */
    setFormElementCollectionAttributes: function (parameters) {
      // Set only when passed in
      if (parameters.elementNumber) {
        this.elementNumber = parameters.elementNumber;
      }
      // Set once from parameters, until explicitly reset
      this.formElements = this.formElements || parameters.formElements;
      if (typeof this.formElements === 'undefined' || !this.formElements) {
        throw Error('Make sure to pass in a collection of form elements at least once per test set');
      }
    },
    /**
     * Check a series of common attributes to form elements, such as html via RegExp and label, if necessary
     * @param parameters
     * @returns {*}
     */
    checkFormElementLabelAndHtml: function (parameters) {
      // Label default defaults to 1
      var thisLabelDepth = parameters.labelDepth || this.labelDepth;
      // The item to check defaults to the next item incrementally
      var input = this.formElements.get(this.elementNumber);
      // Basic sanity check
      if (parameters.sanityString) {
        this.htmlSanityCheck(input, parameters.sanityString);
      }
      // If a label text was passed in, check the label
      if (parameters.labelText) {
        // Make sure the wording is right
        this.checkLabel(input, parameters.labelText, thisLabelDepth);
      }
      return input;
    },
    /**
     * Standard function to check basic form inputs
     *
     * @param parameters Object used to set the various parameters, each of which are detailed below:
     *    elementNumber The element number when all elements are retrieves from the form
     *    sanityString RegExp to check outer html (typically id, name, and model suffice)
     *    sendKeyValue Value to write in input
     *    labelText Wording of the label
     *    labelDepth Set to 1 if the label is a sibling of the input element, 2 if a sibling of the parent, etc.
     */
    testStandardFormInputBox: function (parameters) {
      this.setFormElementCollectionAttributes(parameters);
      // Check the attributes common to form elements
      var input = this.checkFormElementLabelAndHtml(parameters);
      // Make sure we can write
      this.sendKeysAndCheck(input, parameters.sendKeyValue);
      // Increment element number
      this.elementNumber++;
    },
    /**
     * Clear an input field
     */
    clearInput: function (element) {
      var input;
      // Select by number of a collection of elements
      if (typeof element === 'number') {
        input = this.formElements.get(element);
      // CSS selector
      } else if (typeof element === 'string') {
        input = element(by.css(element));
      }
      input.clear();
    },
    /**
     * Test a form select element
     * @param parameters
     */
    checkDropDown: function (parameters) {
      this.setFormElementCollectionAttributes(parameters);
      // Check the attributes common to form elements
      var input = this.checkFormElementLabelAndHtml(parameters);
      // Select an option from the select item and check value
      input.getAttribute('id').then(function (id) {
        // Select Alaska and check for value
        element(by.id(id)).element(by.cssContainingText('option', parameters.optionText)).click();
        expect(input.getAttribute('value')).toBe(parameters.optionValue);
      });
      // Increment element number
      this.elementNumber++;
    },
    /**
     * Verify that checkboxes which are supposed to be available actually are
     * @param element
     */
    testCheckbox: function (element) {
      var input;
      // Select by number of a collection of elements
      if (typeof element === 'number') {
        input = this.formElements.get(element);
        // CSS selector
      } else if (typeof element === 'string') {
        input = element(by.css(element));
      }
      input.click().then(function () {
        expect(input.isSelected()).toBeTruthy();
      });
      // Increment element number
      this.elementNumber++;
    }
  }
};