// Karma configuration
// http://karma-runner.github.io/0.12/config/configuration-file.html
// Generated on 2014-11-19 using
// generator-karma 0.8.3

module.exports = function(config) {
  'use strict';

  config.set({
    // enable / disable watching file and executing tests whenever any file changes
    //autoWatch: true,

    // base path, that will be used to resolve files and exclude
    basePath: '../',

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: [
      '../../bower_components/jquery/dist/jquery.js',
      '../../bower_components/angular/angular.js',
      '../../bower_components/angular-animate/angular-animate.js',
      '../../bower_components/angular-cookies/angular-cookies.js',
      '../../bower_components/angular-resource/angular-resource.js',
      '../../bower_components/angular-sanitize/angular-sanitize.js',
      '../../bower_components/angular-touch/angular-touch.js',
      '../../bower_components/angular-ui-router/release/angular-ui-router.js',
      '../../bower_components/angular-ui-map/ui-map.js',
      '../../bower_components/angular-ui-utils/ui-utils.js',
      '../../bower_components/angular-bootstrap-show-errors/src/showErrors.js',
      '../../bower_components/angular-smart-table/dist/smart-table.min.js',
      '../../bower_components/angular-mocks/angular-mocks.js',
      // drag n drop
      '../../bower_components/lr-drag-n-rop/lrDragNDrop.js',
      '../../bower_components/Sortable/Sortable.js',
      '../../bower_components/Sortable/ng-sortable.js',
      // xeditable
      '../../bower_components/angular-xeditable/dist/js/xeditable.js',
      // File upload
      '../../bower_components/ng-file-upload/angular-file-upload-all.js',
      // ngProgress
      '../../bower_components/ngprogress/build/ngProgress.js',
      // moment
      '../../bower_components/moment/moment.js',
      // datetimepicker
      '../../bower_components/eonasdan-bootstrap-datetimepicker/**/*.js',
      // ngTraverse
      '../../bower_components/ngTraverse/ngTraverse.js',
      // Angular dropdown multi-select
      '../../bower_components/angularjs-dropdown-multiselect/src/angularjs-dropdown-multiselect.js',
      // ngCkeditor
      '../../bower_components/ckeditor/ckeditor.js',
      // ngCkeditor
      '../../bower_components/ng-ckeditor/ng-ckeditor.js',
      // bind polyfill, since we're using an old version of phantom through the karma phantom launch
      '../../bower_components/bind-polyfill/index.js',
      // Lodash
      '../../bower_components/lodash/lodash.js',
      // Tags input
      '../../bower_components/ng-tags-input/ng-tags-input.js',
      // UI Tree
      '../../bower_components/angular-ui-tree/dist/angular-ui-tree.js',
      // Lazy
      '../../bower_components/lazy.js/lazy.js',
      // Export data table to excel file
      '../../bower_components/FileSaver.js/FileSaver.js',
      // Immutable.js
      '../../bower_components/immutable/dist/immutable.js',
      // app/config files
      '*.js',
      './states/*.js',
      // Ascope shared
      '../../bower_components/ascope-shared/init.js',
      '../../bower_components/ascope-shared/directives/**/*.js',
      '../../bower_components/ascope-shared/filters/**/*.js',
      '../../bower_components/ascope-shared/packages/**/*.js',
      '../../bower_components/ascope-shared/services/**/*.js',
      // Shared
      '../shared/**/components/**/*.js',
      '../shared/**/constants/**/*.js',
      '../shared/**/directives/**/*.js',
      '../shared/**/filters/**/*.js',
      '../shared/**/services/**/*.js',
      // Components
      '../components/**/controllers/**/*.js',
      '../components/**/directives/**/*.js',
      '../components/**/services/**/*.js',
      // Test files
      '../**/tests/**/mocks/**/*.js',
      '../**/tests/**/spec/**/*.js',
      // Browser trigger
      './test/browserTrigger.js',
      // Load HTML files
      '../**/*.html'
    ],

    preprocessors: {
      '../**/*.html': ['ng-html2js']
    },

    /**********************************************************
     * Until the pull request is accepted, this will only work with my repo version of karma-ng-html2js
     * https://github.com/loganetherton/karma-ng-html2js-preprocessor-1
     **********************************************************/
    ngHtml2JsPreprocessor: {
      /**
       * For me, I'm starting here:
       * /public/appraisalscope/frontend/app/components/admin/accounting/directives/partials/table-order.html
       * And need to end up here:
       * /components/admin/accounting/directives/partials/tabs.html
       * If we don't end up with the exact path, this preprocessor fails. This should make it work -- Logan
       */
      stripPrefix: '.*?/app/',
      prependPrefix: '/',

      // the name of the Angular module to create
      moduleName: 'frontendTemplates'
    },

    // list of files / patterns to exclude
    exclude: [],

    // web server port
    port: 9999,

    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: [
      'PhantomJS'
    ],

    // Which plugins to enable
    plugins: [
      'karma-phantomjs-launcher',
      'karma-jasmine',
      //'ng-html2js',
      'karma-ng-html2js-preprocessor'
    ],

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false,

    colors: true,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,

    // Uncomment the following lines if you are using grunt's server to run the tests
    // proxies: {
    //   '/': 'http://localhost:9000/'
    // },
    // URL root prevent conflicts with the site root
    // urlRoot: '_karma_'

    browserDisconnectTimeout: 10000,
    browserDisconnectTolerance: 1,
    browserNoActivityTimeout: 60000
  });
};
