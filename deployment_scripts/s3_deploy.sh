#!/bin/bash

# Path of where we keep all the built files that are ready to upload
DIST_PATH=$1
S3_BUCKET=$2

# Make sure we have all the options we need!
if [ -z "$DIST_PATH" ]; then
	echo "Dist folder path is not defined"
	exit
fi

if [ -z ${DIST_PATH#*/} ]; then
	echo "Dist folder doesn't need a trailing slash"
	exit
fi

if [ -z "$S3_BUCKET" ]; then
	echo "S3 Bucket is not defined"
	exit
fi

if [ -z ${S3_BUCKET#*/} ]; then
	echo "S3 Bucket doesn't need a trailing slash"
	exit
fi

# All the steps needed to make the file production ready
processFile() {
	# Remove dist/ from file path
	finalName=${1#*/}
	tempFile=".tmp/tmpGzipFile.${1##*.}"
	
	# index.html needs different max-age header
	if [ "$finalName" = "index.html" ]; then
		# Expire after 5 minutes
		maxAge=300
	else
		# Expire after 7 days
		maxAge=604800
	fi
	
	# Compress the file and save the output into a tmp file
	gzip -c -9 $1 > $tempFile
	
	# Upload the  file to S3
	aws s3 cp $tempFile s3://"$S3_BUCKET"/"$finalName" --acl="public-read" --content-encoding="gzip" --cache-control="max-age=$maxAge"
	
	# Delete the tmp file
	rm $tempFile
	
	echo File "$finalName" uploaded to S3	
}

# Loop through all the files in the dist folder and upload them to S3
uploadFilesRecursevly() {
	for i in "$1"/*; do
		if [ -d "$i" ]; then
			uploadFilesRecursevly "$i"
		elif [ -f "$i" ]; then
			echo "Processng File: $i"
			processFile $i
		fi
	done
}

# Rock & Roll
uploadFilesRecursevly $DIST_PATH
