# Contribution Guidelines

Please go through the following documents and their related resources before contributing to this repository:

- [Codebase structure](https://github.com/ascope/ascopev2/wiki/Codebase-structure)
- [Coding conventions](https://github.com/ascope/ascopev2/wiki/Coding-conventions)
- [Commenting and documenting](https://github.com/ascope/ascopev2/wiki/Commenting-and-documenting)
